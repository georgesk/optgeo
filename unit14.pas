{  This file is part of OptGeo, optical simulation software
    Copyright (C) 2004  Jean-Marie Biansan
        Contact: jeanmarie.biansan@free.fr
    Web site: http://jeanmarie.biansan.free.fr/logiciel.html
 From version 1.25, OptGeo is distribued under the terms of the
 GNU General Public License.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA}

    unit Unit14;

{$mode objfpc}{$H+}

interface

uses
  LCLIntf, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, LResources,LCLType,UnitScaleFont,UChaines;

type

  { Tsaisieangle }

  Tsaisieangle = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    GroupBox1: TGroupBox;
    StaticText1: TLabel;
    StaticText2: TLabel;
    editx1: TEdit;
    edity1: TEdit;
    GroupBox2: TGroupBox;
    StaticText3: TLabel;
    StaticText4: TLabel;
    editx2: TEdit;
    edity2: TEdit;
    boutonsup: TBitBtn;
    GroupBox3: TGroupBox;
    StaticText5: TLabel;
    StaticText6: TLabel;
    editcx: TEdit;
    editcy: TEdit;
    Image1: TImage;
    log1: TLabel;
    log2: TLabel;
    log3: TLabel;
    log4: TLabel;
    log5: TLabel;
    log6: TLabel;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private  encreation:boolean;
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

var
  saisieangle: Tsaisieangle;
   di_x1,di_x2,di_y1,di_y2,di_cx,di_cy:extended;
implementation

uses unit222;


procedure Tsaisieangle.BitBtn1Click(Sender: TObject);
begin
try
di_x1:=strtofloat(editx1.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce13),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;

 try
di_x2:=strtofloat(editx2.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce14),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;

 try
di_y1:=strtofloat(edity1.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce15),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;

 try
di_y2:=strtofloat(edity2.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce16),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;

 try
di_cx:=strtofloat(editcx.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce8),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;

 try
di_cy:=strtofloat(editcy.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce9),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;


 ReTaillePangle(Listeangle,nombreangle,nombreangle+1);

 inc(nombreangle);
if not
(Listeangle[-1+nombreangle].create(di_cx,di_cy,di_x1,di_y1,di_x2,di_y2
)) then begin
ReTaillePangle(Listeangle,nombreangle,nombreangle-1);
dec(nombreangle);
application.messagebox(pchar(rsPointsTropPr3),
pchar(rsattention),mb_ok);

end else
self.modalresult:=mrOk; 
Rafraichit;



end;

procedure Tsaisieangle.FormCreate(Sender: TObject);
begin
   encreation:=true;
   BITBTN1.CAPTION
// msgctxt ';TSAISIEANGLE.BITBTN1.CAPTION';
:=rsOK;


BITBTN2.CAPTION
// msgctxt ';TSAISIEANGLE.BITBTN2.CAPTION';
:=rsAnnuler;


BOUTONSUP.CAPTION
:=rsSupprimerCet11;


CAPTION
:=rsModification2;


GROUPBOX1.CAPTION
// msgctxt ';TSAISIEANGLE.GROUPBOX1.CAPTION';
:=rsCoordonnEsDu3;


GROUPBOX2.CAPTION
// msgctxt ';TSAISIEANGLE.GROUPBOX2.CAPTION';
:=rsCoordonnEsDu4;


GROUPBOX3.CAPTION
:=rsCoordonnEsDe4;


LOG1.CAPTION
// msgctxt ';TSAISIEANGLE.LOG1.CAPTION';
:=rsLog1;


LOG2.CAPTION
// msgctxt ';TSAISIEANGLE.LOG2.CAPTION';
:=rsLog2;


LOG3.CAPTION
// msgctxt ';TSAISIEANGLE.LOG3.CAPTION';
:=rsLog3;


LOG4.CAPTION
// msgctxt ';TSAISIEANGLE.LOG4.CAPTION';
:=rsLog4;


LOG5.CAPTION
// msgctxt ';TSAISIEANGLE.LOG5.CAPTION';
:=rsLog5;


LOG6.CAPTION
// msgctxt ';TSAISIEANGLE.LOG6.CAPTION';
:=rsLog6;


STATICTEXT1.CAPTION
// msgctxt ';TSAISIEANGLE.STATICTEXT1.CAPTION';
:=rsX;


STATICTEXT2.CAPTION
// msgctxt ';TSAISIEANGLE.STATICTEXT2.CAPTION';
:=rsY;


STATICTEXT3.CAPTION
// msgctxt ';TSAISIEANGLE.STATICTEXT3.CAPTION';
:=rsX;


STATICTEXT4.CAPTION
// msgctxt ';TSAISIEANGLE.STATICTEXT4.CAPTION';
:=rsY;


STATICTEXT5.CAPTION
// msgctxt ';TSAISIEANGLE.STATICTEXT5.CAPTION';
:=rsX;


STATICTEXT6.CAPTION
// msgctxt ';TSAISIEANGLE.STATICTEXT6.CAPTION';
:=rsY;


end;

procedure Tsaisieangle.FormShow(Sender: TObject);
begin
 // if encreation then begin scalefont(self); encreation:=false; end;
end;

initialization
  {$i Unit14.lrs}


end.
