 {  This file is part of OptGeo, optical simulation software
    Copyright (C) 2004  Jean-Marie Biansan
        Contact: jeanmarie.biansan@free.fr
    Web site: http://jeanmarie.biansan.free.fr/logiciel.html
 From version 1.25, OptGeo is distribued under the terms of the
 GNU General Public License.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA}


    unit saisieoe;

{$mode objfpc}{$H+}

interface

uses
  LCLIntf, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Spin, Colorbox, ExtCtrls, Buttons,unit222, LResources,LCLType,UnitScaleFont,
  UChaines;

type

  { Tsaisieoeil }

  Tsaisieoeil = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Colorgrid1: TColorBox;
    Image1: TImage;
    GroupBox1: TGroupBox;
    GroupBox3: TGroupBox;
    editepaisseur: TSpinEdit;
    GroupBox4: TGroupBox;
    StaticText1: TLabel;
    editx1: TEdit;
    StaticText2: TLabel;
    edity1: TEdit;
    GroupBox5: TGroupBox;
    StaticText3: TLabel;
    StaticText4: TLabel;
    editx2: TEdit;
    edity2: TEdit;
    boutonsup: TBitBtn;
    log1: TLabel;
    log2: TLabel;
    log3: TLabel;
    log4: TLabel;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private  encreation:boolean;
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

var
  saisieoeil: Tsaisieoeil;
  mp_epaisseurecran:integer;
  mp_couleurecran:tcolor;
  mp_x1,mp_x2,mp_y1,mp_y2:extended;
implementation


procedure Tsaisieoeil.BitBtn1Click(Sender: TObject);
begin

mp_epaisseurecran:=editepaisseur.Value;
mp_couleurecran:=colorgrid1.selected;
try
mp_x1:=strtofloat(editx1.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce13),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;
 if ((mp_x1<xxmin) or (mp_x1>xxmax)) then begin
 application.messagebox(pchar(rsValeurInacce13),
 pchar(rsattention),mb_ok);
 exit;
 end;

 try
mp_x2:=strtofloat(editx2.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce14),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;
 if ((mp_x2<xxmin) or (mp_x2>xxmax)) then begin
 application.messagebox(pchar(rsValeurInacce14),
 pchar(rsattention),mb_ok);
 exit;
 end;

 try
mp_y1:=strtofloat(edity1.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce15),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;
 if ((mp_y1<yymin) or (mp_y1>yymax)) then begin
 application.messagebox(pchar(rsValeurInacce15),
 pchar(rsattention),mb_ok);
 exit;
 end;

 try
mp_y2:=strtofloat(edity2.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce16),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;
 if ((mp_y2<yymin) or (mp_y2>yymax)) then begin
 application.messagebox(pchar(rsValeurInacce16),
 pchar(rsattention),mb_ok);
 exit;
 end;

ReTaillePOeil(ListeOeil,nombreOeil,nombreOeil+1);

 inc(nombreOeil);
if not
(ListeOeil[-1+nombreOeil].create(mp_x1,mp_y1,mp_x2,mp_y2,
mp_epaisseurecran,
mp_couleurecran)) then begin
ReTaillePOeil(ListeOeil,nombreOeil,nombreOeil-1);
dec(nombreOeil);
application.messagebox(pchar(rsTailleDeLOei2),
pchar(rsattention),mb_ok);

end else
self.modalresult:=mrOk; 
Rafraichit;


end;

procedure Tsaisieoeil.FormCreate(Sender: TObject);
begin
   encreation:=true;
   BITBTN1.CAPTION
// msgctxt ';TSAISIEOEIL.BITBTN1.CAPTION';
:=rsOK;


BITBTN2.CAPTION
// msgctxt ';TSAISIEOEIL.BITBTN2.CAPTION';
:=rsAnnuler;


BOUTONSUP.CAPTION
:=rsSupprimerCet6;


CAPTION
:=rsAjoutDUnOeil2;


GROUPBOX1.CAPTION
:=rsCouleurDeLOe;


GROUPBOX3.CAPTION
// msgctxt ';TSAISIEOEIL.GROUPBOX3.CAPTION';
:=rsEpaisseurTra10;


GROUPBOX4.CAPTION
// msgctxt ';TSAISIEOEIL.GROUPBOX4.CAPTION';
:=rsPoint1;


GROUPBOX5.CAPTION
// msgctxt ';TSAISIEOEIL.GROUPBOX5.CAPTION';
:=rsPoint2;


LOG1.CAPTION
// msgctxt ';TSAISIEOEIL.LOG1.CAPTION';
:=rsLog1;


LOG2.CAPTION
// msgctxt ';TSAISIEOEIL.LOG2.CAPTION';
:=rsLog2;


LOG3.CAPTION
// msgctxt ';TSAISIEOEIL.LOG3.CAPTION';
:=rsLog3;


LOG4.CAPTION
// msgctxt ';TSAISIEOEIL.LOG4.CAPTION';
:=rsLog4;


STATICTEXT1.CAPTION
// msgctxt ';TSAISIEOEIL.STATICTEXT1.CAPTION';
:=rsX;


STATICTEXT2.CAPTION
// msgctxt ';TSAISIEOEIL.STATICTEXT2.CAPTION';
:=rsY;


STATICTEXT3.CAPTION
// msgctxt ';TSAISIEOEIL.STATICTEXT3.CAPTION';
:=rsX;


STATICTEXT4.CAPTION
// msgctxt ';TSAISIEOEIL.STATICTEXT4.CAPTION';
:=rsY;


end;

procedure Tsaisieoeil.FormShow(Sender: TObject);
begin
 // if encreation then begin scalefont(self); encreation:=false; end;
end;

initialization
  {$i saisieoe.lrs}

end.
