 {  This file is part of OptGeo, optical simulation software
    Copyright (C) 2004  Jean-Marie Biansan
        Contact: jeanmarie.biansan@free.fr
    Web site: http://jeanmarie.biansan.free.fr/logiciel.html
 From version 1.25, OptGeo is distribued under the terms of the
 GNU General Public License.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA}


    unit saisieop1;

{$mode objfpc}{$H+}

interface

uses
  LCLIntf, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls,unit222, Buttons, Spin, LResources,LCLType,UnitScaleFont,
  UChaines;

type

  { Tsaisieop }

  Tsaisieop = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    GroupBox9: TGroupBox;
    Image1: TImage;
    GroupBox1: TGroupBox;
    Spinmaxsegment: TSpinEdit;
    StaticText1: TLabel;
    StaticText18: TLabel;
    StaticText2: TLabel;
    editx1: TEdit;
    edity1: TEdit;
    GroupBox2: TGroupBox;
    StaticText3: TLabel;
    StaticText4: TLabel;
    editx2: TEdit;
    edity2: TEdit;
    radiocouleur: TRadioGroup;
    GroupBox3: TGroupBox;
    editepaisseur: TSpinEdit;
    GroupBox4: TGroupBox;
    GroupBox5: TGroupBox;
    StaticText5: TLabel;
    StaticText6: TLabel;
    editsx: TEdit;
    editsy: TEdit;
    GroupBox6: TGroupBox;
    spinnombrerayons: TSpinEdit;
    boutonsup: TBitBtn;
    GroupBox7: TGroupBox;
    StaticText7: TLabel;
    Spinmaxenfant: TSpinEdit;
    StaticText8: TLabel;
    StaticText9: TLabel;
    StaticText10: TLabel;
    StaticText11: TLabel;
    StaticText12: TLabel;
    editvav: TEdit;
    editvaa: TEdit;
    log1: TLabel;
    log2: TLabel;
    log3: TLabel;
    log4: TLabel;
    log5: TLabel;
    log6: TLabel;
    GroupBox8: TGroupBox;
    StaticText14: TLabel;
    StaticText13: TLabel;
    StaticText15: TLabel;
    StaticText16: TLabel;
    StaticText17: TLabel;
    editlistechemins: TEdit;
    cochesurfaces: TCheckBox;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private  encreation:boolean;
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

var
  saisieopp: Tsaisieop;
  ra_couleur:tcolor;
  ra_epaisseur,ra_nombrerayons,ra_maxenfant,ra_maxsegment:integer;
  ra_x1,ra_x2,ra_y1,ra_y2,ra_sx,ra_sy:extended;
  ra_virtuelavant,ra_virtuelarriere:boolean;
implementation


procedure Tsaisieop.BitBtn1Click(Sender: TObject);
begin
ra_epaisseur:=editepaisseur.value;
ra_maxsegment:=spinmaxsegment.value;
ra_nombrerayons:=spinnombrerayons.value;
ra_maxenfant:=spinmaxenfant.value;
case radiocouleur.ItemIndex of
0: ra_couleur:=clred;
1: ra_couleur:=clgreen;
2: ra_couleur:=clblue;
3: ra_couleur:=clblack;
end;

try
ra_sx:=strtofloat(editsx.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce34),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;
 if ((ra_sx<xxmin) or (ra_sx>xxmax)) then begin
 application.messagebox(pchar(rsValeurInacce34),
 pchar(rsattention),mb_ok);
 exit;
 end;

 try
ra_sy:=strtofloat(editsy.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce35),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;
 if ((ra_sy<yymin) or (ra_sy>yymax)) then begin
 application.messagebox(pchar(rsValeurInacce35),
 pchar(rsattention),mb_ok);
 exit;
 end;

try
ra_x1:=strtofloat(editx1.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce13),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;
 if ((ra_x1<xxmin) or (ra_x1>xxmax)) then begin
 application.messagebox(pchar(rsValeurInacce13),
 pchar(rsattention),mb_ok);
 exit;
 end;

 try
ra_x2:=strtofloat(editx2.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce14),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;
 if ((ra_x2<xxmin) or (ra_x2>xxmax)) then begin
 application.messagebox(pchar(rsValeurInacce14),
 pchar(rsattention),mb_ok);
 exit;
 end;

 try
ra_y1:=strtofloat(edity1.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce15),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;
 if ((ra_y1<yymin) or (ra_y1>yymax)) then begin
 application.messagebox(pchar(rsValeurInacce15),
 pchar(rsattention),mb_ok);
 exit;
 end;

 try
ra_y2:=strtofloat(edity2.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce16),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;
 if ((ra_y2<yymin) or (ra_y2>yymax)) then begin
 application.messagebox(pchar(rsValeurInacce16),
 pchar(rsattention),mb_ok);
 exit;
 end;

  ReTaillePondeplane(Listeondeplane,nombreondeplane,nombreondeplane+1);
 inc(nombreondeplane);
if not
(ListeOndePlane[-1+nombreOndePlane].create(ra_nombrerayons,
ra_sx,ra_sy,ra_x1,ra_y1,ra_x2,ra_y2,
ra_couleur,ra_epaisseur,editvav.text,editvaa.text,ra_maxenfant,
cochesurfaces.checked,editlistechemins.text,ra_maxsegment)) then begin
ReTaillePondeplane(Listeondeplane,nombreondeplane,nombreondeplane-1);
dec(nombreOndePlane);
application.messagebox(pchar(rsValeursEntrE),
pchar(rsattention),mb_ok);
end else
self.ModalResult:=mrok;
Rafraichit;
end;

procedure Tsaisieop.FormCreate(Sender: TObject);
begin
   encreation:=true;
   BITBTN1.CAPTION
// msgctxt ';TSAISIEOP.BITBTN1.CAPTION';
:=rsOK;


BITBTN2.CAPTION
// msgctxt ';TSAISIEOP.BITBTN2.CAPTION';
:=rsAnnuler;


BOUTONSUP.CAPTION
:=rsSupprimerCet7;


CAPTION
:=rsSaisieDUneOn;


COCHESURFACES.CAPTION
// msgctxt ';TSAISIEOP.COCHESURFACES.CAPTION';
:=rsTracer;


GROUPBOX1.CAPTION
// msgctxt ';TSAISIEOP.GROUPBOX1.CAPTION';
:=rsPoint1;


GROUPBOX2.CAPTION
// msgctxt ';TSAISIEOP.GROUPBOX2.CAPTION';
:=rsPoint2;


GROUPBOX3.CAPTION
:=rsEpaisseurLCr;


GROUPBOX4.CAPTION
:=rsTracerEnPoin;


GROUPBOX5.CAPTION
:=rsExtrMitRayon;


GROUPBOX6.CAPTION
:=rsNombreDeRayo3;


GROUPBOX7.CAPTION
:=rsPourChaqueRa;


GROUPBOX8.CAPTION
:=rsTracerDesSur;


GROUPBOX9.CAPTION
:=rsNombreMaxDeS;


LOG1.CAPTION
// msgctxt ';TSAISIEOP.LOG1.CAPTION';
:=rsLog1;


LOG2.CAPTION
// msgctxt ';TSAISIEOP.LOG2.CAPTION';
:=rsLog2;


LOG3.CAPTION
// msgctxt ';TSAISIEOP.LOG3.CAPTION';
:=rsLog3;


LOG4.CAPTION
// msgctxt ';TSAISIEOP.LOG4.CAPTION';
:=rsLog4;


LOG5.CAPTION
// msgctxt ';TSAISIEOP.LOG5.CAPTION';
:=rsLog5;


LOG6.CAPTION
// msgctxt ';TSAISIEOP.LOG6.CAPTION';
:=rsLog6;


RADIOCOULEUR.CAPTION
:=rsCouleurDesRa;


STATICTEXT1.CAPTION
// msgctxt ';TSAISIEOP.STATICTEXT1.CAPTION';
:=rsX;


STATICTEXT10.CAPTION
:=rsListeDesSegm;


STATICTEXT11.CAPTION
:=rsEntreParenth;


STATICTEXT12.CAPTION
:=rsEtSansEspace;


STATICTEXT13.CAPTION
:=rsDOndeSParezC;


STATICTEXT14.CAPTION
:=rsDonnerLesVal;


STATICTEXT15.CAPTION
:=rsExemple12014;


STATICTEXT16.CAPTION
:=rsRemarqueSiLa;


STATICTEXT17.CAPTION
:=rsNePeutTreEtN;


STATICTEXT18.CAPTION
// msgctxt ';TSAISIEOP.STATICTEXT18.CAPTION';
:=rsNombreMaxima35;


STATICTEXT2.CAPTION
// msgctxt ';TSAISIEOP.STATICTEXT2.CAPTION';
:=rsY;


STATICTEXT3.CAPTION
// msgctxt ';TSAISIEOP.STATICTEXT3.CAPTION';
:=rsX;


STATICTEXT4.CAPTION
// msgctxt ';TSAISIEOP.STATICTEXT4.CAPTION';
:=rsY;


STATICTEXT5.CAPTION
// msgctxt ';TSAISIEOP.STATICTEXT5.CAPTION';
:=rsX;


STATICTEXT6.CAPTION
// msgctxt ';TSAISIEOP.STATICTEXT6.CAPTION';
:=rsY;


STATICTEXT7.CAPTION
// msgctxt ';TSAISIEOP.STATICTEXT7.CAPTION';
:=rsNombreMaxima35;


STATICTEXT8.CAPTION
:=rsVersLAvant;


STATICTEXT9.CAPTION
:=rsVersLArriRe;

 radiocouleur.Items[0]:=rsRouge;
 radiocouleur.Items[1]:=rsVert;
 radiocouleur.Items[2]:=rsBleu;
 radiocouleur.Items[3]:=rsLumiReBlanch;
end;

procedure Tsaisieop.FormShow(Sender: TObject);
begin
  //if encreation then begin scalefont(self); encreation:=false; end;
end;






initialization
  {$i saisieop1.lrs}


end.
