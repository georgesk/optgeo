{  This file is part of OptGeo, optical simulation software
    Copyright (C) 2004  Jean-Marie Biansan
        Contact: jeanmarie.biansan@free.fr
    Web site: http://jeanmarie.biansan.free.fr/logiciel.html
 From version 1.25, OptGeo is distribued under the terms of the
 GNU General Public License.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA}


    unit saisielec;

{$mode objfpc}{$H+}

interface

uses
  LCLIntf, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls,ColorBox, LResources,LCLType,UnitScaleFont,
  UChaines;

type

  { Tsaisielentille }

  Tsaisielentille = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    GroupBox1: TGroupBox;
    StaticText1: TLabel;
    StaticText2: TLabel;
    StaticText3: TLabel;
    StaticText4: TLabel;
    StaticText5: TLabel;
    StaticText6: TLabel;
    edits1x: TEdit;
    edits1y: TEdit;
    edits2x: TEdit;
    edits2y: TEdit;
    Image1: TImage;
    GroupBox2: TGroupBox;
    StaticText7: TLabel;
    StaticText8: TLabel;
    StaticText9: TLabel;
    StaticText10: TLabel;
    editr1: TEdit;
    editr2: TEdit;
    GroupBox3: TGroupBox;
    StaticText11: TLabel;
    editdiametre: TEdit;
    GroupBox4: TGroupBox;
    StaticText12: TLabel;
    StaticText13: TLabel;
    StaticText14: TLabel;
    editnrouge: TEdit;
    editnvert: TEdit;
    editnbleu: TEdit;
    boutonsup: TBitBtn;
    GroupBox5: TGroupBox;
    ColorGrid1: TColorBox;
    log1: TLabel;
    log2: TLabel;
    log3: TLabel;
    log4: TLabel;
    log5: TLabel;
    log6: TLabel;
    log7: TLabel;
    StaticText15: TLabel;
    combomateriaux: TComboBox;
    procedure BitBtn1Click(Sender: TObject);
    procedure combomateriauxChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private  encreation:boolean;
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

var
  saisielentille: Tsaisielentille;
lec_s1x,lec_s1y,lec_s2x,lec_s2y,lec_r1,lec_r2,lec_nr,lec_nv,lec_nb,
lec_diametre:extended;
implementation

uses unit222;


procedure Tsaisielentille.BitBtn1Click(Sender: TObject);
begin
try
 lec_s1x:=strtofloat(edits1x.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce17),
 pchar(rsAttention), mb_ok);

 exit;
 end;  end;

try
 lec_s1y:=strtofloat(edits1y.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce18),
 pchar(rsattention),mb_ok);

 exit;
 end;  end;


 try
 lec_s2x:=strtofloat(edits2x.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce19),
 pchar(rsattention),mb_ok);

 exit;
 end;  end;


 try
 lec_s2y:=strtofloat(edits2y.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce20),
 pchar(rsattention),mb_ok);

 exit;
 end;  end;


try
 lec_r1:=strtofloat(editr1.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce21),
 pchar(rsattention),mb_ok);

 exit;
 end;  end;



 try
 lec_r2:=strtofloat(editr2.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce22),
 pchar(rsattention),mb_ok);

 exit;
 end;  end;


 try
 lec_nr:=strtofloat(editnrouge.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce4),
 pchar(rsattention),mb_ok);

 exit;
 end;  end;


  try
 lec_nv:=strtofloat(editnvert.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce5),
 pchar(rsattention),mb_ok);

 exit;
 end;  end;


  try
 lec_nb:=strtofloat(editnbleu.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce6),
 pchar(rsattention),mb_ok);

 exit;
 end;  end;

  try
 lec_diametre:=abs(strtofloat(editdiametre.text));
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce23),
 pchar(rsattention),mb_ok);

 exit;
 end;  end;



ReTaillePlec(Listelec,nombrelec,nombrelec+1);

 inc(nombrelec);
if not
(Listelec[-1+nombrelec].create(lec_s1x,lec_s1y,lec_s2x,lec_s2y,lec_r1,lec_r2,
lec_diametre,colorgrid1.selected,clred,true,lec_nr,lec_nb,lec_nv
,jamaisreflechi,jamaisreflechi)) then begin
ReTaillePlec(Listelec,nombrelec,nombrelec-1);
dec(nombrelec);
application.messagebox(pchar(rsTypeDeLentil),
pchar(rsattention),mb_ok);

end else
self.modalresult:=mrOk;
Rafraichit;

end;
 
 
procedure Tsaisielentille.combomateriauxChange(Sender: TObject);
begin
if combomateriaux.itemindex=0 then exit;
editnrouge.text:=listenrouge.strings[combomateriaux.itemindex];
editnvert.text:=listenvert.strings[combomateriaux.itemindex];
editnbleu.text:=listenbleu.strings[combomateriaux.itemindex];
end;

procedure Tsaisielentille.FormCreate(Sender: TObject);
begin
   encreation:=true;
   BITBTN1.CAPTION
:=rsOK;


BITBTN2.CAPTION
:=rsAnnuler;


BOUTONSUP.CAPTION
:=rsSupprimerCet5;


CAPTION
:=rsAjoutDUneLen4;


if decimalseparator=',' then
EDITNBLEU.TEXT
:=rs17 else
 EDITNBLEU.TEXT
:=rs17b;

if decimalseparator=',' then
EDITNROUGE.TEXT
:=rs15 else
 EDITNROUGE.TEXT
:=rs15b;

 if decimalseparator=',' then
EDITNVERT.TEXT
:=rs16 else
 EDITNVERT.TEXT
:=rs16b;



GROUPBOX1.CAPTION
:=rsCoordonnEsDe3;


GROUPBOX2.CAPTION
:=rsRayonsDeCour;


GROUPBOX3.CAPTION
:=rsDiamTre;


GROUPBOX4.CAPTION
:=rsIndiceDeRFra2;


GROUPBOX5.CAPTION
:=rsCouleurLCran5;


LOG1.CAPTION
:=rsLog1;


LOG2.CAPTION
:=rsLog2;


LOG3.CAPTION
:=rsLog3;


LOG4.CAPTION
:=rsLog4;


LOG5.CAPTION
:=rsLog5;


LOG6.CAPTION
:=rsLog6;


LOG7.CAPTION
:=rsLog7;


STATICTEXT1.CAPTION
:=rsSommetS1;


STATICTEXT10.CAPTION
:=rsR2;


STATICTEXT11.CAPTION
:=rsD;


STATICTEXT12.CAPTION
:=rsPourLeRouge;


STATICTEXT13.CAPTION
:=rsPourLeVert;


STATICTEXT14.CAPTION
:=rsPourLeBleu;


STATICTEXT15.CAPTION
:=rsMatRiau;


STATICTEXT2.CAPTION
:=rsSommetS2;


STATICTEXT3.CAPTION
:=rsX;


STATICTEXT4.CAPTION
:=rsY;


STATICTEXT5.CAPTION
:=rsX;


STATICTEXT6.CAPTION
:=rsY;


STATICTEXT7.CAPTION
:=rsSigneSiCentr;


STATICTEXT8.CAPTION
:=rsSurLAxeOrien;


STATICTEXT9.CAPTION
:=rsR1;

end;

procedure Tsaisielentille.FormShow(Sender: TObject);
begin
 // if encreation then begin scalefont(self); encreation:=false; end;
end;

initialization
  {$i saisielec.lrs}


end.
