{  This file is part of OptGeo, optical simulation software
    Copyright (C) 2004  Jean-Marie Biansan
        Contact: jeanmarie.biansan@free.fr
    Web site: http://jeanmarie.biansan.free.fr/logiciel.html
 From version 1.25, OptGeo is distribued under the terms of the
 GNU General Public License.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA}

    unit Unit3;

{$mode objfpc}{$H+}

interface

uses
  LCLIntf, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, StdCtrls, ExtCtrls, Buttons, Colorbox, Spin,
  unit222, LResources,UnitScaleFont,UChaines;

type

  { Tproprietespoints }

  Tproprietespoints = class(TForm)
    BitBtn1: TBitBtn;
    ColorGrid1: TColorBox;
    RadioGroup1: TRadioGroup;
    radiocroixx: TRadioButton;
    radiocroixp: TRadioButton;
    radiodisque: TRadioButton;
    GroupBox1: TGroupBox;
    editeurtaillepoints: TSpinEdit;
    GroupBox2: TGroupBox;
    radiocercle: TRadioButton;
    GroupBox3: TGroupBox;
    casereperer: TCheckBox;
    GroupBox4: TGroupBox;
    cochetracergrille: TCheckBox;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private  encreation:boolean;
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

var
  proprietespoints: Tproprietespoints;

implementation


procedure Tproprietespoints.BitBtn1Click(Sender: TObject);
begin
taillepoint:=editeurtaillepoints.value;
if radiocroixx.checked then stylepoint:=tpcroixx;
if radiocroixp.checked then stylepoint:=tpcroixp;
if radiodisque.checked then stylepoint:=tpdisque;
if radiocercle.checked then stylepoint:=tpcercle;
signaler:=casereperer.checked;
tracergrille:=cochetracergrille.checked;
couleurpoint:=colorgrid1.selected;
Rafraichit;
end;

procedure Tproprietespoints.FormCreate(Sender: TObject);
begin
   encreation:=true;
   BITBTN1.CAPTION
// msgctxt ';TPROPRIETESPOINTS.BITBTN1.CAPTION';
:=rsOK;


CAPTION
:=rsPropriTSDesP;


CASEREPERER.CAPTION
:=rsRepRer;


COCHETRACERGRILLE.CAPTION
// msgctxt ';TPROPRIETESPOINTS.COCHETRACERGRILLE.CAPTION';
:=rsTracer;


GROUPBOX1.CAPTION
:=rsTailleDesPoi;


GROUPBOX2.CAPTION
:=rsCouleurDesPo;


GROUPBOX3.CAPTION
:=rsPointCourant;


GROUPBOX4.CAPTION
:=rsGrilleDeRepR;


RADIOCERCLE.CAPTION
// msgctxt ';TPROPRIETESPOINTS.RADIOCERCLE.CAPTION';
:=rsCercle;


RADIOCROIXP.CAPTION
:=rsCroixDroite;


RADIOCROIXX.CAPTION
:=rsCroixOblique;


RADIODISQUE.CAPTION
// msgctxt ';TPROPRIETESPOINTS.RADIODISQUE.CAPTION';
:=rsDisque;


RADIOGROUP1.CAPTION
:=rsStyleDesPoin;


end;

procedure Tproprietespoints.FormShow(Sender: TObject);
begin
 // if encreation then begin scalefont(self); encreation:=false; end;
end;

initialization
  {$i Unit3.lrs}


end.
