{  This file is part of OptGeo, optical simulation software
    Copyright (C) 2004  Jean-Marie Biansan
        Contact: jeanmarie.biansan@free.fr
    Web site: http://jeanmarie.biansan.free.fr/logiciel.html
 From version 1.25, OptGeo is distribued under the terms of the
 GNU General Public License.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA}
    unit Unit17;

{$mode objfpc}{$H+}

interface

uses
  LCLIntf, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Spin,unit222, ExtCtrls,Unit18,filectrl, ComCtrls, LResources,
  LCLType,UnitScaleFont,UChaines,LazFileUtils,Unit_commune;

type

  { Tsaisiepreferences }

  Tsaisiepreferences = class(TForm)
    BitBtn1: TBitBtn;
PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    ComboBox1: TComboBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    SpinEdit1: TSpinEdit;
    SpinEdit2: TSpinEdit;
    Label4: TLabel;
    boutoncouleur: TButton;
    boutoncouleuraxe: TButton;
     boutoncouleu1: TButton;
    boutoncouleuraxe1: TButton;
    Labelcouleur: TLabel;
    Labelcouleuraxe: TLabel;
     Labelcouleur1: TLabel;
    Labelcouleuraxe1: TLabel;
    FontDialog1: TFontDialog;
    Label5: TLabel;
    boutonpolice: TButton;
    BitBtn2: TBitBtn;
    ColorDialog1: TColorDialog;
    BitBtn3: TBitBtn;
    TabSheet2: TTabSheet;
    Label6: TLabel;
    Label7: TLabel;
    Button1: TButton;
    Labecouleurrayon: TLabel;
    Memo1: TMemo;
    Memo2: TMemo;
    procedure BitBtn1Click(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
    procedure boutonpoliceClick(Sender: TObject);
    procedure boutoncouleurClick(Sender: TObject);
    procedure boutoncouleuraxeClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SpinEdit1Change(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure boutoncouleu1Click(Sender: TObject);
    procedure boutoncouleuraxe1Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure SpinEdit2Change(Sender: TObject);
    procedure boutonpolice1Click(Sender: TObject);
  private  encreation:boolean;
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

var
  saisiepreferences: Tsaisiepreferences;

implementation


procedure Tsaisiepreferences.ComboBox1Change(Sender: TObject);
begin
if pos(rsMiroirPlan, combobox1.items[combobox1.itemindex])<>0 then begin
boutoncouleur.enabled:=true;
labelcouleur.width:=89;
labelcouleur.height:=25;
 labelcouleur.Color:=couleurmiroirplan;
 spinedit1.enabled:=true;
 spinedit1.Value:=epaisseurmiroirplan;
 boutoncouleuraxe.enabled:=false;
 labelcouleuraxe.width:=113;
labelcouleuraxe.height:=25;
 labelcouleuraxe.color:=saisiepreferences.Color;
 boutonpolice.Enabled:=false;
 exit;
end;



if pos(rsRSeau, combobox1.items[combobox1.itemindex])<>0 then begin
boutoncouleur.enabled:=true;
labelcouleur.width:=89;
labelcouleur.height:=25;
 labelcouleur.Color:=couleurreseau;
 spinedit1.enabled:=true;
 spinedit1.Value:=epaisseurreseau;
 boutoncouleuraxe.enabled:=false;
 labelcouleuraxe.width:=113;
labelcouleuraxe.height:=25;
 labelcouleuraxe.color:=saisiepreferences.Color;
 boutonpolice.Enabled:=false;
 exit;
end;

if pos(rsMiroirSphRiq3, combobox1.items[combobox1.itemindex])<>0 then begin
boutoncouleur.enabled:=true;
labelcouleur.width:=89;
labelcouleur.height:=25;
 labelcouleur.Color:=couleurmscopa;
  spinedit1.enabled:=true;
 spinedit1.Value:=epaisseurmscopa;
 boutoncouleuraxe.enabled:=true;
 labelcouleuraxe.width:=113;
labelcouleuraxe.height:=25;
 labelcouleuraxe.color:=couleuraxemscopa;
  boutonpolice.Enabled:=false;
 exit;
end;

if pos(rsMiroirSphRiq4, combobox1.items[combobox1.itemindex])<>0 then begin
boutoncouleur.enabled:=true;
labelcouleur.width:=89;
labelcouleur.height:=25;
 labelcouleur.Color:=couleurmscepa;
  spinedit1.enabled:=true;
 spinedit1.Value:=epaisseurmscepa;
 boutoncouleuraxe.enabled:=true;
 labelcouleuraxe.width:=113;
labelcouleuraxe.height:=25;
 labelcouleuraxe.color:=couleuraxemscepa;
  boutonpolice.Enabled:=false;
 exit;
end;

if pos(rsMiroirSphRiq5, combobox1.items[combobox1.itemindex])<>0 then begin
boutoncouleur.enabled:=true;
labelcouleur.width:=89;
labelcouleur.height:=25;
 labelcouleur.Color:=couleurmscore;
  spinedit1.enabled:=true;
 spinedit1.Value:=epaisseurmscore;
 boutoncouleuraxe.enabled:=true;
 labelcouleuraxe.width:=113;
labelcouleuraxe.height:=25;
 labelcouleuraxe.color:=couleuraxemscore;
  boutonpolice.Enabled:=false;
 exit;
end;


if pos(rsMiroirSphRiq6, combobox1.items[combobox1.itemindex])<>0 then begin
boutoncouleur.enabled:=true;
labelcouleur.width:=89;
labelcouleur.height:=25;
 labelcouleur.Color:=couleurmscere;
  spinedit1.enabled:=true;
 spinedit1.Value:=epaisseurmscere;
 boutoncouleuraxe.enabled:=true;
 labelcouleuraxe.width:=113;
labelcouleuraxe.height:=25;
 labelcouleuraxe.color:=couleuraxemscere;
  boutonpolice.Enabled:=false;
 exit;
end;

if pos(rsMiroirCNique, combobox1.items[combobox1.itemindex])<>0 then begin
boutoncouleur.enabled:=true;
labelcouleur.width:=89;
labelcouleur.height:=25;
 labelcouleur.Color:=couleurmiroirconique;
  spinedit1.enabled:=true;
 spinedit1.Value:=epaisseurmiroirconique;
 boutoncouleuraxe.enabled:=true;
 labelcouleuraxe.width:=113;
labelcouleuraxe.height:=25;
 labelcouleuraxe.color:=couleuraxemiroirconique;
  boutonpolice.Enabled:=false;
 exit;
end;


if pos(rsLentilleMinc3, combobox1.items[combobox1.itemindex])<>0 then begin
boutoncouleur.enabled:=true;
labelcouleur.width:=89;
labelcouleur.height:=25;
 labelcouleur.Color:=couleurlmc;
  spinedit1.enabled:=true;
 spinedit1.Value:=epaisseurlmc;
 boutoncouleuraxe.enabled:=true;
 labelcouleuraxe.width:=113;
labelcouleuraxe.height:=25;
 labelcouleuraxe.color:=couleuraxelmc;
  boutonpolice.Enabled:=false;
 exit;
end;


if pos(rsLentilleMinc4, combobox1.items[combobox1.itemindex])<>0 then begin
boutoncouleur.enabled:=true;
labelcouleur.width:=89;
labelcouleur.height:=25;
 labelcouleur.Color:=couleurlmd;
  spinedit1.enabled:=true;
 spinedit1.Value:=epaisseurlmd;
 boutoncouleuraxe.enabled:=true;
 labelcouleuraxe.width:=113;
labelcouleuraxe.height:=25;
 labelcouleuraxe.color:=couleuraxelmd;
  boutonpolice.Enabled:=false;
 exit;
end;

if pos(rsLentillePais, combobox1.items[combobox1.itemindex])<>0 then begin
boutoncouleur.enabled:=true;
labelcouleur.width:=89;
labelcouleur.height:=25;
 labelcouleur.Color:=couleurlec;
  spinedit1.enabled:=false;
 spinedit1.Value:=epaisseurlec;
 boutoncouleuraxe.enabled:=true;
 labelcouleuraxe.width:=113;
labelcouleuraxe.height:=25;
 labelcouleuraxe.color:=couleuraxelec;
  boutonpolice.Enabled:=false;
 exit;
end;

if pos(rsPolyhDreRFra, combobox1.items[combobox1.itemindex])<>0 then begin
boutoncouleur.enabled:=true;
labelcouleur.width:=89;
labelcouleur.height:=25;
 labelcouleur.Color:=couleurpolyhedre;
  spinedit1.enabled:=false;
 spinedit1.Value:=epaisseurpolyhedre;
 boutoncouleuraxe.enabled:=false;
 labelcouleuraxe.width:=113;
labelcouleuraxe.height:=25;
 labelcouleuraxe.color:=saisiepreferences.Color;
  boutonpolice.Enabled:=false;
 exit;
end;

if pos(rsPrisme, combobox1.items[combobox1.itemindex])<>0 then begin
boutoncouleur.enabled:=true;
labelcouleur.width:=89;
labelcouleur.height:=25;
 labelcouleur.Color:=couleurPrisme;
  spinedit1.enabled:=false;
 spinedit1.Value:=epaisseurPrisme;
 boutoncouleuraxe.enabled:=false;
 labelcouleuraxe.width:=113;
labelcouleuraxe.height:=25;
 labelcouleuraxe.color:=saisiepreferences.Color;
  boutonpolice.Enabled:=false;
 exit;
end;

if pos(rsPolycercleRF, combobox1.items[combobox1.itemindex])<>0 then begin
boutoncouleur.enabled:=true;
labelcouleur.width:=89;
labelcouleur.height:=25;
 labelcouleur.Color:=couleurpolycercle;
  spinedit1.enabled:=false;
 spinedit1.Value:=epaisseurpolycercle;
 boutoncouleuraxe.enabled:=false;
 labelcouleuraxe.width:=113;
labelcouleuraxe.height:=25;
 labelcouleuraxe.color:=saisiepreferences.Color;
  boutonpolice.Enabled:=false;
 exit;
end;


if pos(rsSphReRFracta, combobox1.items[combobox1.itemindex])<>0 then begin
boutoncouleur.enabled:=true;
labelcouleur.width:=89;
labelcouleur.height:=25;
 labelcouleur.Color:=couleursphere;
  spinedit1.enabled:=false;
 spinedit1.Value:=epaisseursphere;
 boutoncouleuraxe.enabled:=false;
 labelcouleuraxe.width:=113;
labelcouleuraxe.height:=25;
 labelcouleuraxe.color:=saisiepreferences.Color;
  boutonpolice.Enabled:=false;
 exit;
end;

if pos(rsRayonUnique, combobox1.items[combobox1.itemindex])<>0 then begin
boutoncouleur.enabled:=true;
labelcouleur.width:=89;
labelcouleur.height:=25;
 labelcouleur.Color:=couleurrayon;
  spinedit1.enabled:=true;
 spinedit1.Value:=epaisseurrayon;
 boutoncouleuraxe.enabled:=false;
 labelcouleuraxe.width:=113;
labelcouleuraxe.height:=25;
 labelcouleuraxe.color:=saisiepreferences.Color;
  boutonpolice.Enabled:=false;
 exit;
end;


if pos(rsOndePlane, combobox1.items[combobox1.itemindex])<>0 then begin
boutoncouleur.enabled:=true;
labelcouleur.width:=89;
labelcouleur.height:=25;
 labelcouleur.Color:=couleurondeplane;
  spinedit1.enabled:=true;
 spinedit1.Value:=epaisseurondeplane;
 boutoncouleuraxe.enabled:=false;
 labelcouleuraxe.width:=113;
labelcouleuraxe.height:=25;
 labelcouleuraxe.color:=saisiepreferences.Color;
  boutonpolice.Enabled:=false;
 exit;
end;


if pos(rsSourcePonctu, combobox1.items[combobox1.itemindex])<>0 then begin
boutoncouleur.enabled:=true;
labelcouleur.width:=89;
labelcouleur.height:=25;
 labelcouleur.Color:=couleursourceponctuelle;
  spinedit1.enabled:=true;
 spinedit1.Value:=epaisseursourceponctuelle;
 boutoncouleuraxe.enabled:=false;
 labelcouleuraxe.width:=113;
labelcouleuraxe.height:=25;
 labelcouleuraxe.color:=saisiepreferences.Color;
  boutonpolice.Enabled:=false;
 exit;
end;

 if pos(rsEcran, combobox1.items[combobox1.itemindex])<>0 then begin
boutoncouleur.enabled:=true;
labelcouleur.width:=89;
labelcouleur.height:=25;
 labelcouleur.Color:=couleurecran;
  spinedit1.enabled:=true;
 spinedit1.Value:=epaisseurecran;
 boutoncouleuraxe.enabled:=false;
 labelcouleuraxe.width:=113;
labelcouleuraxe.height:=25;
 labelcouleuraxe.color:=saisiepreferences.Color;
  boutonpolice.Enabled:=false;
 exit;
end;

if pos(rsLameSemiRFlC2, combobox1.items[combobox1.itemindex])<>0 then begin
boutoncouleur.enabled:=true;
labelcouleur.width:=89;
labelcouleur.height:=25;
 labelcouleur.Color:=couleurlsr;
  spinedit1.enabled:=true;
 spinedit1.Value:=epaisseurlsr;
 boutoncouleuraxe.enabled:=false;
 labelcouleuraxe.width:=113;
labelcouleuraxe.height:=25;
 labelcouleuraxe.color:=saisiepreferences.Color;
  boutonpolice.Enabled:=false;
 exit;
end;

if pos(rsDiaphragme, combobox1.items[combobox1.itemindex])<>0 then begin
boutoncouleur.enabled:=true;
labelcouleur.width:=89;
labelcouleur.height:=25;
 labelcouleur.Color:=couleurdiaphragme;
  spinedit1.enabled:=true;
 spinedit1.Value:=epaisseurdiaphragme;
 boutoncouleuraxe.enabled:=false;
 labelcouleuraxe.width:=113;
labelcouleuraxe.height:=25;
 labelcouleuraxe.color:=saisiepreferences.Color;
  boutonpolice.Enabled:=false;
 exit;
end;

if pos(rsOeil, combobox1.items[combobox1.itemindex])<>0 then begin
boutoncouleur.enabled:=true;
labelcouleur.width:=89;
labelcouleur.height:=25;
 labelcouleur.Color:=couleuroeil;
  spinedit1.enabled:=true;
 spinedit1.Value:=epaisseuroeil;
 boutoncouleuraxe.enabled:=false;
 labelcouleuraxe.width:=113;
labelcouleuraxe.height:=25;
 labelcouleuraxe.color:=saisiepreferences.Color;
  boutonpolice.Enabled:=false;
 exit;
end;

if pos(rsFlChe, combobox1.items[combobox1.itemindex])<>0 then begin
boutoncouleur.enabled:=true;
labelcouleur.width:=89;
labelcouleur.height:=25;
 labelcouleur.Color:=couleurfleche;
  spinedit1.enabled:=true;
 spinedit1.Value:=epaisseurfleche;
 boutoncouleuraxe.enabled:=false;
 labelcouleuraxe.width:=113;
labelcouleuraxe.height:=25;
 labelcouleuraxe.color:=saisiepreferences.Color;
  boutonpolice.Enabled:=false;
 exit;
end;

if pos(rsTexte, combobox1.items[combobox1.itemindex])<>0 then begin
boutoncouleur.enabled:=false;
labelcouleur.width:=89;
labelcouleur.height:=25;
 labelcouleur.Color:=saisiepreferences.Color;
  spinedit1.enabled:=false;
 boutoncouleuraxe.enabled:=false;
 labelcouleuraxe.width:=113;
labelcouleuraxe.height:=25;
 labelcouleuraxe.color:=saisiepreferences.Color;
 boutonpolice.Enabled:=true;
 exit;
end;





end;

procedure Tsaisiepreferences.BitBtn1Click(Sender: TObject);
begin

end;

procedure Tsaisiepreferences.boutonpoliceClick(Sender: TObject);
begin
fontdialog1.font:=policedefaut;
if fontdialog1.execute then policedefaut:=fontdialog1.Font;
memo2.Lines.Clear;
memo2.lines.Add(Format(rsNom, [policedefaut.name]));
memo2.lines.Add(Format(rsTaille, [inttostr(policedefaut.size)]));
if (fsunderline in policedefaut.style) then memo2.lines.add(rsSoulign);
if (fsbold in policedefaut.style) then memo2.lines.add(rsGras);
if (fsitalic in policedefaut.style) then memo2.lines.add(rsItalique);
if (fsstrikeout in policedefaut.style) then memo2.lines.add(rsBarr);
end;

procedure Tsaisiepreferences.boutoncouleurClick(Sender: TObject);
begin

if pos(rsMiroirPlan, combobox1.items[combobox1.itemindex])<>0 then begin
colordialog1.color:=couleurmiroirplan;
if colordialog1.Execute then   couleurmiroirplan:=colordialog1.color;
labelcouleur.Color:=couleurmiroirplan;
  exit;
end;




 if pos(rsRSeau, combobox1.items[combobox1.itemindex])<>0 then begin
colordialog1.color:=couleurreseau;
if colordialog1.Execute then   couleurreseau:=colordialog1.color;
labelcouleur.Color:=couleurreseau;
  exit;
end;


if pos(rsMiroirSphRiq3, combobox1.items[combobox1.itemindex])<>0 then begin
colordialog1.color:=couleurmscopa;
if colordialog1.Execute then   couleurmscopa:=colordialog1.color;
labelcouleur.Color:=couleurmscopa;
 exit;
end;

if pos(rsMiroirSphRiq4, combobox1.items[combobox1.itemindex])<>0 then begin
colordialog1.color:=couleurmscepa;
if colordialog1.Execute then   couleurmscepa:=colordialog1.color;
labelcouleur.Color:=couleurmscepa;
 exit;
end;

if pos(rsMiroirSphRiq5, combobox1.items[combobox1.itemindex])<>0 then begin
colordialog1.color:=couleurmscore;
if colordialog1.Execute then   couleurmscore:=colordialog1.color;
labelcouleur.Color:= couleurmscore;
 exit;
end;


if pos(rsMiroirSphRiq6, combobox1.items[combobox1.itemindex])<>0 then begin
colordialog1.color:=couleurmscere;
if colordialog1.Execute then   couleurmscere:=colordialog1.color;
labelcouleur.Color:=couleurmscere;
 exit;
end;

if pos(rsMiroirCNique, combobox1.items[combobox1.itemindex])<>0 then begin
colordialog1.color:=couleurmiroirconique;
if colordialog1.Execute then   couleurmiroirconique:=colordialog1.color;
labelcouleur.Color:= couleurmiroirconique;
 exit;
end;


if pos(rsLentilleMinc3, combobox1.items[combobox1.itemindex])<>0 then begin
colordialog1.color:=couleurlmc;
if colordialog1.Execute then   couleurlmc:=colordialog1.color;
labelcouleur.Color:= couleurlmc;
 exit;
end;


if pos(rsLentilleMinc4, combobox1.items[combobox1.itemindex])<>0 then begin
colordialog1.color:=couleurlmd;
if colordialog1.Execute then   couleurlmd:=colordialog1.color;
labelcouleur.Color:= couleurlmd;
 exit;
end;

if pos(rsLentillePais, combobox1.items[combobox1.itemindex])<>0 then begin
colordialog1.color:=couleurlec;
if colordialog1.Execute then   couleurlec:=colordialog1.color;
labelcouleur.Color:= couleurlec;
 exit;
end;

if pos(rsPolyhDreRFra, combobox1.items[combobox1.itemindex])<>0 then begin
colordialog1.color:=couleurpolyhedre;
if colordialog1.Execute then   couleurpolyhedre:=colordialog1.color;
labelcouleur.Color:= couleurpolyhedre;
 exit;
end;

if pos(rsPrisme, combobox1.items[combobox1.itemindex])<>0 then begin
colordialog1.color:=couleurPrisme;
if colordialog1.Execute then   couleurPrisme:=colordialog1.color;
labelcouleur.Color:= couleurPrisme;
 exit;
end;

if pos(rsPolycercleRF, combobox1.items[combobox1.itemindex])<>0 then begin
colordialog1.color:=couleurpolycercle;
if colordialog1.Execute then   couleurpolycercle:=colordialog1.color;
labelcouleur.Color:= couleurpolycercle;
 exit;
end;


if pos(rsSphReRFracta, combobox1.items[combobox1.itemindex])<>0 then begin
colordialog1.color:=couleursphere;
if colordialog1.Execute then   couleursphere:=colordialog1.color;
labelcouleur.Color:= couleursphere;
 exit;
end;

if pos(rsRayonUnique, combobox1.items[combobox1.itemindex])<>0 then begin
 saisiecouleurrayondefaut:=Tsaisiecouleurrayondefaut.create(self);
 saisiecouleurrayondefaut.showmodal;
 case saisiecouleurrayondefaut.RadioGroup1.ItemIndex of
 0: couleurrayon:=clred;
 1: couleurrayon:=clgreen;
 2: couleurrayon:=clblue;
 3: couleurrayon:=clblack;
 end;
 saisiecouleurrayondefaut.Free;
 labelcouleur.Color:= couleurrayon;
 exit;
end;


if pos(rsOndePlane, combobox1.items[combobox1.itemindex])<>0 then begin
saisiecouleurrayondefaut:=Tsaisiecouleurrayondefaut.create(self);
 saisiecouleurrayondefaut.showmodal;
 case saisiecouleurrayondefaut.RadioGroup1.ItemIndex of
 0: couleurondeplane:=clred;
 1: couleurondeplane:=clgreen;
 2: couleurondeplane:=clblue;
  3: couleurondeplane:=clblack;
 end;
  saisiecouleurrayondefaut.Free;
labelcouleur.Color:= couleurondeplane;
 exit;
end;


if pos(rsSourcePonctu, combobox1.items[combobox1.itemindex])<>0 then begin
saisiecouleurrayondefaut:=Tsaisiecouleurrayondefaut.create(self);
 saisiecouleurrayondefaut.showmodal;
 case saisiecouleurrayondefaut.RadioGroup1.ItemIndex of
 0: couleursourceponctuelle:=clred;
 1: couleursourceponctuelle:=clgreen;
 2: couleursourceponctuelle:=clblue;
  3: couleursourceponctuelle:=clblack;
 end;
  saisiecouleurrayondefaut.Free;
labelcouleur.Color:= couleursourceponctuelle;
 exit;
end;

 if pos(rsEcran, combobox1.items[combobox1.itemindex])<>0 then begin
colordialog1.color:=couleurecran;
if colordialog1.Execute then   couleurecran:=colordialog1.color;
labelcouleur.Color:= couleurecran;
 exit;
end;

if pos(rsLameSemiRFlC2, combobox1.items[combobox1.itemindex])<>0 then begin
colordialog1.color:=couleurlsr;
if colordialog1.Execute then   couleurlsr:=colordialog1.color;
labelcouleur.Color:=couleurlsr;
 exit;
end;

if pos(rsDiaphragme, combobox1.items[combobox1.itemindex])<>0 then begin
colordialog1.color:=couleurdiaphragme;
if colordialog1.Execute then   couleurdiaphragme:=colordialog1.color;
labelcouleur.Color:=couleurdiaphragme;
 exit;
end;

if pos(rsOeil, combobox1.items[combobox1.itemindex])<>0 then begin
colordialog1.color:=couleuroeil;
if colordialog1.Execute then   couleuroeil:=colordialog1.color;
labelcouleur.Color:=couleuroeil;
 exit;
end;

if pos(rsFlChe, combobox1.items[combobox1.itemindex])<>0 then begin
colordialog1.color:=couleurfleche;
if colordialog1.Execute then   couleurfleche:=colordialog1.color;
labelcouleur.Color:=couleurfleche;
 exit;
end;


end;

procedure Tsaisiepreferences.boutoncouleuraxeClick(Sender: TObject);
begin


if pos(rsMiroirSphRiq3, combobox1.items[combobox1.itemindex])<>0 then begin
colordialog1.color:=couleuraxemscopa;
if colordialog1.Execute then   couleuraxemscopa:=colordialog1.color;
labelcouleuraxe.Color:=couleuraxemscopa;
 exit;
end;

if pos(rsMiroirSphRiq4, combobox1.items[combobox1.itemindex])<>0 then begin
colordialog1.color:=couleuraxemscepa;
if colordialog1.Execute then   couleuraxemscepa:=colordialog1.color;
labelcouleuraxe.Color:=couleuraxemscepa;
 exit;
end;

if pos(rsMiroirSphRiq5, combobox1.items[combobox1.itemindex])<>0 then begin
colordialog1.color:=couleuraxemscore;
if colordialog1.Execute then   couleuraxemscore:=colordialog1.color;
labelcouleuraxe.Color:= couleuraxemscore;
 exit;
end;


if pos(rsMiroirSphRiq6, combobox1.items[combobox1.itemindex])<>0 then begin
colordialog1.color:=couleuraxemscere;
if colordialog1.Execute then   couleuraxemscere:=colordialog1.color;
labelcouleuraxe.Color:=couleuraxemscere;
 exit;
end;

if pos(rsMiroirCNique, combobox1.items[combobox1.itemindex])<>0 then begin
colordialog1.color:=couleuraxemiroirconique;
if colordialog1.Execute then   couleuraxemiroirconique:=colordialog1.color;
labelcouleuraxe.Color:= couleuraxemiroirconique;
 exit;
end;


if pos(rsLentilleMinc3, combobox1.items[combobox1.itemindex])<>0 then begin
colordialog1.color:=couleuraxelmc;
if colordialog1.Execute then   couleuraxelmc:=colordialog1.color;
labelcouleuraxe.Color:= couleuraxelmc;
 exit;
end;


if pos(rsLentilleMinc4, combobox1.items[combobox1.itemindex])<>0 then begin
colordialog1.color:=couleuraxelmd;
if colordialog1.Execute then   couleuraxelmd:=colordialog1.color;
labelcouleuraxe.Color:= couleuraxelmd;
 exit;
end;

if pos(rsLentillePais, combobox1.items[combobox1.itemindex])<>0 then begin
colordialog1.color:=couleuraxelec;
if colordialog1.Execute then   couleuraxelec:=colordialog1.color;
labelcouleuraxe.Color:= couleuraxelec;
 exit;
end;

end;

procedure Tsaisiepreferences.FormCreate(Sender: TObject);
begin
   encreation:=true;
   BITBTN1.CAPTION
// msgctxt ';TSAISIEPREFERENCES.BITBTN1.CAPTION';
:=rsOK;


BITBTN2.CAPTION
:=rsOkEtEnregist;


BITBTN3.CAPTION
:=rsRTablirLesVa;


BOUTONCOULEU1.CAPTION
// msgctxt ';TSAISIEPREFERENCES.BOUTONCOULEU1.CAPTION';
:=rsModifier;


BOUTONCOULEUR.CAPTION
// msgctxt ';TSAISIEPREFERENCES.BOUTONCOULEUR.CAPTION';
:=rsModifier;


BOUTONCOULEURAXE.CAPTION
// msgctxt ';TSAISIEPREFERENCES.BOUTONCOULEURAXE.CAPTION';
:=rsModifier;


BOUTONCOULEURAXE1.CAPTION
// msgctxt ';TSAISIEPREFERENCES.BOUTONCOULEURAXE1.CAPTION';
:=rsModifier;


BOUTONPOLICE.CAPTION
// msgctxt ';TSAISIEPREFERENCES.BOUTONPOLICE.CAPTION';
:=rsModifier;


BUTTON1.CAPTION
// msgctxt ';TSAISIEPREFERENCES.BUTTON1.CAPTION';
:=rsModifier;


CAPTION
:=rsPrFRencesPro;


LABEL1.CAPTION
:=rsTypeDLMent;


LABEL2.CAPTION
:=rsCouleurParDF;


LABEL3.CAPTION
:=rsEpaisseurPar;


LABEL4.CAPTION
:=rsCouleurAxePa;


LABEL5.CAPTION
:=rsPoliceParDFa;


LABEL6.CAPTION
:=rsIndiquerIciL;


LABEL7.CAPTION
:=rsCouleurRayon;


TABSHEET1.CAPTION
:=rsElementParLM;


TABSHEET2.CAPTION
:=rsPourTousLesL;

combobox1.Items.Clear;

combobox1.Items.add(rsMiroirPlan);
combobox1.Items.add(rsMiroirSphRiq3);
combobox1.Items.add(rsMiroirSphRiq4);
combobox1.Items.add(rsMiroirSphRiq5);
combobox1.Items.add(rsMiroirSphRiq6);
combobox1.Items.add(rsMiroirCNique);
combobox1.Items.add(rsLentilleMinc3);
combobox1.Items.add(rsLentilleMinc4);
combobox1.Items.add(rsLentillePais);
combobox1.Items.add(rsPolyhDreRFra);
combobox1.Items.add(rsPolycercleRF);
combobox1.Items.add(rsSphReRFracta);
combobox1.Items.add(rsRayonUnique);
combobox1.Items.add(rsOndePlane);
combobox1.Items.add(rsSourcePonctu);
combobox1.Items.add(rsEcran);
combobox1.Items.add(rsLameSemiRFlC2);
combobox1.Items.add(rsDiaphragme);
combobox1.Items.add(rsOeil);
combobox1.Items.add(rsFlChe);
combobox1.Items.add(rsTexte);
combobox1.Items.add(rsPrisme);
combobox1.Items.add(rsRSeau);

end;

procedure Tsaisiepreferences.FormShow(Sender: TObject);
begin
 // if encreation then begin scalefont(self); encreation:=false; end;
end;

procedure Tsaisiepreferences.SpinEdit1Change(Sender: TObject);
begin


if pos(rsMiroirPlan, combobox1.items[combobox1.itemindex])<>0 then begin
epaisseurmiroirplan:=spinedit1.value;
  exit;
end;


if pos(rsRSeau, combobox1.items[combobox1.itemindex])<>0 then begin
epaisseurreseau:=spinedit1.value;
  exit;
end;

if pos(rsMiroirSphRiq3, combobox1.items[combobox1.itemindex])<>0 then begin
epaisseurmscopa:=spinedit1.value;
 exit;
end;

if pos(rsMiroirSphRiq4, combobox1.items[combobox1.itemindex])<>0 then begin
epaisseurmscepa:=spinedit1.value;
 exit;
end;

if pos(rsMiroirSphRiq5, combobox1.items[combobox1.itemindex])<>0 then begin
epaisseurmscore:=spinedit1.value;
 exit;
end;


if pos(rsMiroirSphRiq6, combobox1.items[combobox1.itemindex])<>0 then begin
epaisseurmscere:=spinedit1.value;
 exit;
end;

if pos(rsMiroirCNique, combobox1.items[combobox1.itemindex])<>0 then begin
epaisseurmiroirconique:=spinedit1.value;
 exit;
end;


if pos(rsLentilleMinc3, combobox1.items[combobox1.itemindex])<>0 then begin
epaisseurlmc:=spinedit1.value;
end;


if pos(rsLentilleMinc4, combobox1.items[combobox1.itemindex])<>0 then begin
epaisseurlmd:=spinedit1.value;
 exit;
end;


if pos(rsRayonUnique, combobox1.items[combobox1.itemindex])<>0 then begin
 epaisseurrayon:=spinedit1.value;
 exit;
end;


if pos(rsOndePlane, combobox1.items[combobox1.itemindex])<>0 then begin
epaisseurondeplane:=spinedit1.value;
 exit;
end;


if pos(rsSourcePonctu, combobox1.items[combobox1.itemindex])<>0 then begin
epaisseursourceponctuelle:=spinedit1.value;
 exit;
end;

 if pos(rsEcran, combobox1.items[combobox1.itemindex])<>0 then begin
epaisseurecran:=spinedit1.value;
 exit;
end;

if pos(rsLameSemiRFlC2, combobox1.items[combobox1.itemindex])<>0 then begin
epaisseurlsr:=spinedit1.value;
 exit;
end;

if pos(rsDiaphragme, combobox1.items[combobox1.itemindex])<>0 then begin
epaisseurdiaphragme:=spinedit1.value;
 exit;
end;

if pos(rsOeil, combobox1.items[combobox1.itemindex])<>0 then begin
epaisseuroeil:=spinedit1.value;
 exit;
end;

if pos(rsFlChe, combobox1.items[combobox1.itemindex])<>0 then begin
epaisseurfleche:=spinedit1.value;
 exit;
end;

end;

procedure Tsaisiepreferences.BitBtn3Click(Sender: TObject);
begin
  couleurmiroirplan:=clblack;   epaisseurmiroirplan:=1;
   couleurreseau:=clblue;   epaisseurreseau:=1;
   couleurmscopa:=clblack;  epaisseurmscopa:=1; couleuraxemscopa:=clblue;
   couleurmscepa:=clblack;  epaisseurmscepa:=1; couleuraxemscepa:=clblue;
   couleurmscore:=clblack;    epaisseurmscore:=1; couleuraxemscore:=clblue;
    couleurmscere:=clblack;   epaisseurmscere:=1;  couleuraxemscere:=clblue;
     couleurlmc:=clblue;      epaisseurlmc:=1;     couleuraxelmc:=clFuchsia;
     couleurlmd:=clblue;      epaisseurlmd:=1;     couleuraxelmd:=clFuchsia;
       couleurlec:=claqua; couleuraxelec:=clred;  epaisseurlec:=1;
    couleurrayon:=clred;    epaisseurrayon:=1;
     couleurondeplane:=clred;  epaisseurondeplane:=1;
     couleursourceponctuelle:=clred;  epaisseursourceponctuelle:=1;
     couleurecran:=clblack;  epaisseurecran:=2;
      couleurlsr:=clblack;   epaisseurlsr:=1;
       couleurpolyhedre:=claqua; epaisseurpolyhedre:=1;
       couleurprisme:=claqua; epaisseurprisme:=1;
       couleurpolycercle:=claqua; epaisseurpolycercle:=1;
   couleursphere:=claqua; epaisseursphere:=1;
    couleurmiroirconique:=clblack; couleuraxemiroirconique:=clblue;
    couleurtexte:=clblack; taillefontetexte:=10;
    couleurdiaphragme:=clblack; epaisseurdiaphragme:=1;
    couleurfleche:=clblack;  epaisseurfleche:=1;
    couleuroeil:=clgreen;   epaisseuroeil:=1;
    couleurangle:=clblack; epaisseurangle:=1;
    couleurmetre:=clblack; epaisseurmetre:=1;
    combobox1.itemindex:=0;
boutoncouleur.enabled:=true;
labelcouleur.width:=89;
labelcouleur.height:=25;
 labelcouleur.Color:=couleurmiroirplan;
 labelcouleuraxe.width:=113;
labelcouleuraxe.height:=25;
 labelcouleuraxe.color:=saisiepreferences.Color;
 spinedit1.enabled:=true;
 spinedit1.Value:=epaisseurmiroirplan;
 boutoncouleuraxe.enabled:=false;
  boutonpolice.Enabled:=false;
  modalresult:=0;
end;

procedure Tsaisiepreferences.BitBtn2Click(Sender: TObject);
var fc:textfile;
begin
if nom_fichier_config_perso='' then exit;
try
if not(directoryexistsUTF8(repertoire_config_perso)) then
   forcedirectories((repertoire_config_perso));
assignfile(fc,(nom_fichier_config_perso));
rewrite(fc);
except
application.messagebox(pchar(Format(rsImpossibleDC, [nom_fichier_config_perso])
  ),
pchar(rsHLas), mb_ok);
exit;
end;
writeln(fc,'Repertoire perso');
writeln(fc,repertoire_simul_perso);

writeln(fc,'Repertoire courant');
writeln(fc,repertoire_actuel);
writeln(fc,'Miroir plan');
writeln(fc,couleurmiroirplan);
writeln(fc,epaisseurmiroirplan);

writeln(fc,'Miroir sphérique concave paraxial');
writeln(fc,couleurmscopa);
writeln(fc,epaisseurmscopa);
writeln(fc,couleuraxemscopa);

writeln(fc,'Miroir sphérique convexe paraxial');
writeln(fc,couleurmscepa);
writeln(fc,epaisseurmscepa);
writeln(fc,couleuraxemscepa);

writeln(fc,'Miroir sphérique concave réel');
writeln(fc,couleurmscore);
writeln(fc,epaisseurmscore);
 writeln(fc,couleuraxemscore);

 writeln(fc,'Miroir sphérique convexe réel');
writeln(fc,couleurmscere);
writeln(fc,epaisseurmscere);
writeln(fc,couleuraxemscere);

writeln(fc,'Miroir cônique');
writeln(fc,couleurmiroirconique);
writeln(fc,epaisseurmiroirconique);
 writeln(fc,couleuraxemiroirconique);

 writeln(fc,'Lentille mince convergente idéale');
writeln(fc,couleurlmc);
writeln(fc,epaisseurlmc);
writeln(fc,couleuraxelmc);

 writeln(fc,'Lentille mince divergente idéale');
writeln(fc,couleurlmd);
writeln(fc,epaisseurlmd);
writeln(fc,couleuraxelmd);

 writeln(fc,'Lentille épaisse');
writeln(fc,couleurlec);
writeln(fc,couleuraxelec);


 writeln(fc,'Polyhèdre réfractant');
writeln(fc,couleurpolyhedre);

writeln(fc,'Polycercle réfractant');
writeln(fc,couleurpolycercle);

writeln(fc,'Sphère réfractante');
writeln(fc,couleursphere);

writeln(fc,'Rayon unique');
writeln(fc,couleurrayon);
writeln(fc,epaisseurrayon);

writeln(fc,'Onde plane');
writeln(fc,couleurondeplane);
writeln(fc,epaisseurondeplane);

writeln(fc,'Source ponctuelle');
writeln(fc,couleursourceponctuelle);
writeln(fc,epaisseursourceponctuelle);

writeln(fc,'Ecran');
writeln(fc,couleurecran);
writeln(fc,epaisseurecran);

writeln(fc,'Lame semi-réfléchissante');
writeln(fc,couleurlsr);
writeln(fc,epaisseurlsr);

writeln(fc,'Diaphragme');
writeln(fc,couleurDiaphragme);
writeln(fc,epaisseurDiaphragme);

writeln(fc,'Oeil');
writeln(fc,couleurOeil);
writeln(fc,epaisseurOeil);

 writeln(fc,'Fléche');
writeln(fc,couleurFleche);
writeln(fc,epaisseurFleche);

writeln(fc,'Texte');
   writeln(fc,policedefaut.charset);
   writeln(fc,policedefaut.color);
   writeln(fc,policedefaut.name);
   writeln(fc,policedefaut.size);
   if (fsbold in policedefaut.style) then
   writeln(fc,'fsbold') else writeln(fc,'no');
   if (fsitalic in policedefaut.style)then
   writeln(fc,'fsitalic') else writeln(fc,'no');
 if (fsunderline in policedefaut.style)  then
   writeln(fc,'fsunderline') else writeln(fc,'no');
if (fsstrikeout in policedefaut.style) then
   writeln(fc,'fsstrikeout') else writeln(fc,'no');
   if policedefaut.pitch=fpDefault then writeln(fc,'fpDefault');
   if  policedefaut.pitch=fpVariable then writeln(fc,'fpVariable');
   if  policedefaut.pitch=fpFixed then writeln(fc,'fpFixed');
  writeln(fc,'Prisme');
writeln(fc,couleurprisme);
writeln(fc,'Reseau');
writeln(fc,couleurreseau);
writeln(fc,epaisseurreseau);

closefile(fc);
modalresult:=mrok;
end;

procedure Tsaisiepreferences.boutoncouleu1Click(Sender: TObject);
begin


if colordialog1.Execute then   begin
labelcouleur1.width:=89;
labelcouleur1.height:=25;
couleurglobale:=colordialog1.color;
labelcouleur1.Color:=couleurglobale;
couleurmiroirplan:=couleurglobale;
couleurreseau:=couleurglobale;
couleurmscopa:=couleurglobale;
couleurmscepa:=couleurglobale;
couleurmscore:=couleurglobale;
couleurmscere:=couleurglobale;
couleurmiroirconique:=couleurglobale;
couleurlmc:=couleurglobale;
couleurlmd:=couleurglobale;
couleurlec:=couleurglobale;
couleurpolyhedre:=couleurglobale;
couleurpolycercle:=couleurglobale;
couleurprisme:=couleurglobale;
couleursphere:=couleurglobale;
couleurecran:=couleurglobale;
couleurlsr:=couleurglobale;
couleurdiaphragme:=couleurglobale;
couleurfleche:=couleurglobale;
combobox1.itemindex:=0;
combobox1change(sender);
end;

end;

procedure Tsaisiepreferences.boutoncouleuraxe1Click(Sender: TObject);
begin

if colordialog1.Execute then   begin
labelcouleuraxe1.width:=89;
labelcouleuraxe1.height:=25;
couleurglobaleaxe:=colordialog1.color;
labelcouleuraxe1.Color:=couleurglobaleaxe;
couleuraxemscopa:=colordialog1.color;
couleuraxemscepa:=colordialog1.color;
couleuraxemscore:=colordialog1.color;
couleuraxemscere:=colordialog1.color;
couleuraxemiroirconique:=colordialog1.color;
couleuraxelmc:=colordialog1.color;
couleuraxelmd:=colordialog1.color;
couleuraxelec:=colordialog1.color;
combobox1.itemindex:=0;
combobox1change(sender);
end;

end;

procedure Tsaisiepreferences.Button1Click(Sender: TObject);
begin
 saisiecouleurrayondefaut:=Tsaisiecouleurrayondefaut.create(self);
 saisiecouleurrayondefaut.showmodal;
 case saisiecouleurrayondefaut.RadioGroup1.ItemIndex of
 0: begin
 couleurrayon:=clred;
 couleurondeplane:=clred;
 couleursourceponctuelle:=clred;
 couleurglobalerayon:=clred;
 end;
 1: begin
 couleurrayon:=clgreen;
 couleurondeplane:=clgreen;
 couleursourceponctuelle:=clgreen;
 couleurglobalerayon:=clgreen;
 end;
 2: begin
 couleurrayon:=clblue;
 couleurondeplane:=clblue;
 couleursourceponctuelle:=clblue;
 couleurglobalerayon:=clblue;
 end;
  3: begin
 couleurrayon:=clblack;
 couleurondeplane:=clblack;
 couleursourceponctuelle:=clblack;
 couleurglobalerayon:=clblack;
 end;
 end;
  saisiecouleurrayondefaut.Free;

 labecouleurrayon.Color:= couleurglobalerayon ;

 combobox1.itemindex:=0;
combobox1change(sender);
end;

procedure Tsaisiepreferences.SpinEdit2Change(Sender: TObject);
begin
 epaisseurglobale:=spinedit2.value;
epaisseurmiroirplan:=spinedit2.value;
epaisseurreseau:=spinedit2.value;
 epaisseurmscopa:=spinedit2.value;
 epaisseurmscepa:=spinedit2.value;
 epaisseurmscore:=spinedit2.value;
 epaisseurmscere:=spinedit2.value;
epaisseurmiroirconique:=spinedit2.value;
epaisseurlmc:=spinedit2.value;
epaisseurlmd:=spinedit2.value;
 epaisseurrayon:=spinedit2.value;
epaisseurondeplane:=spinedit2.value;
epaisseursourceponctuelle:=spinedit2.value;
epaisseurecran:=spinedit2.value;
epaisseurlsr:=spinedit2.value;
epaisseurdiaphragme:=spinedit2.value;
epaisseuroeil:=spinedit2.value;
 epaisseurfleche:=spinedit2.value;
 combobox1.itemindex:=0;
combobox1change(sender);
end;

procedure Tsaisiepreferences.boutonpolice1Click(Sender: TObject);
begin
fontdialog1.font:=policedefaut;
if fontdialog1.execute then policedefaut:=fontdialog1.Font;
memo1.Lines.Clear;
memo1.lines.Add(Format(rsNom, [policedefaut.name]));
memo1.lines.Add(Format(rsTaille, [inttostr(policedefaut.size)]));
if (fsunderline in policedefaut.style) then memo1.lines.add(rsSoulign);
if (fsbold in policedefaut.style) then memo1.lines.add(rsGras);
if (fsitalic in policedefaut.style) then memo1.lines.add(rsItalique);
if (fsstrikeout in policedefaut.style) then memo1.lines.add(rsBarr);
memo2.Lines.Clear;
memo2.lines.Add(Format(rsNom, [policedefaut.name]));
memo2.lines.Add(Format(rsTaille, [inttostr(policedefaut.size)]));
if (fsunderline in policedefaut.style) then memo2.lines.add(rsSoulign);
if (fsbold in policedefaut.style) then memo2.lines.add(rsGras);
if (fsitalic in policedefaut.style) then memo2.lines.add(rsItalique);
if (fsstrikeout in policedefaut.style) then memo2.lines.add(rsBarr);
end;

initialization
  {$i Unit17.lrs}


end.
