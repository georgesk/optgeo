{  This file is part of OptGeo, optical simulation software
    Copyright (C) 2004  Jean-Marie Biansan
        Contact: jeanmarie.biansan@free.fr
    Web site: http://jeanmarie.biansan.free.fr/logiciel.html
 From version 1.25, OptGeo is distribued under the terms of the
 GNU General Public License.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA}

    unit Unit19;

{$mode objfpc}{$H+}

interface

uses
  LCLIntf, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Spin, LResources,UnitScaleFont,UChaines;

type

  { Tsaisieempennage }

  Tsaisieempennage = class(TForm)
    BitBtn1: TBitBtn;
    spinposition: TSpinEdit;
    Label2: TLabel;
    Label1: TLabel;
    Label3: TLabel;
    SpinEdit1: TSpinEdit;
    Label4: TLabel;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private  encreation:boolean;
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

var
  saisieempennage: Tsaisieempennage;

implementation


{ Tsaisieempennage }

procedure Tsaisieempennage.BitBtn1Click(Sender: TObject);
begin

end;

procedure Tsaisieempennage.FormCreate(Sender: TObject);
begin
   encreation:=true;
   BITBTN1.CAPTION
:=rsOK;


CAPTION
:=rsPropriTSDeLE;


LABEL1.CAPTION
:=rsEn1000MeDeLa;


LABEL2.CAPTION
:=rsPositionDeLE;


LABEL3.CAPTION
:=rsTailleDeLEmp;


LABEL4.CAPTION
:=rsEn1000MeDeLa2;
end;

procedure Tsaisieempennage.FormShow(Sender: TObject);
begin
  //if encreation then begin scalefont(self); encreation:=false; end;
end;

initialization

  {$i Unit19.lrs}

end.
