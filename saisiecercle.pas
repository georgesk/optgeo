 {  This file is part of OptGeo, optical simulation software
    Copyright (C) 2004  Jean-Marie Biansan
        Contact: jeanmarie.biansan@free.fr
    Web site: http://jeanmarie.biansan.free.fr/logiciel.html
 From version 1.25, OptGeo is distribued under the terms of the
 GNU General Public License.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA}

    unit saisiecercle;

{$mode objfpc}{$H+}

interface

uses
  LCLIntf, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Spin, ExtCtrls,unit222,Unit8,ColorBox, LResources,LCLType,UnitScaleFont,
  UChaines;

type

  { Tsaisiepolycercle }

  Tsaisiepolycercle = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    ColorGrid1: TColorBox;
    RadioGroupaigu: TRadioGroup;
    radiosortant: TRadioGroup;
    radioentrant: TRadioGroup;
    GroupBox2: TGroupBox;
    StaticText2: TLabel;
    StaticText3: TLabel;
    StaticText4: TLabel;
    editnrouge: TEdit;
    editnvert: TEdit;
    editnbleu: TEdit;
    ComboBox1: TComboBox;
    GroupBox3: TGroupBox;
    StaticText5: TLabel;
    StaticText6: TLabel;
    editx: TEdit;
    edity: TEdit;
    boutonsup: TBitBtn;
    radiorectiligne: TRadioGroup;
    radiorentrant: TRadioGroup;
    GroupBox1: TGroupBox;
    StaticText1: TLabel;
    editrayon: TEdit;
    GroupBox4: TGroupBox;
    log1: TLabel;
    log2: TLabel;
    log3: TLabel;
    StaticText7: TLabel;
    combomateriaux: TComboBox;
    procedure BitBtn1Click(Sender: TObject);
    procedure boutonsupClick(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
    procedure combomateriauxChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure GroupBox4Click(Sender: TObject);
  private  encreation:boolean;
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

var
  saisiepolycercle: Tsaisiepolycercle;
    oldindex:integer;
  nrouge,nvert,nbleu:extended;
  tentr,tsort:treflechipolyhedre;
  trigogo:boolean;
implementation


procedure Tsaisiepolycercle.BitBtn1Click(Sender: TObject);
var dAB,dIS:extended;  i:integer;
begin
try
 strtofloat(editx.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce2),
 pchar(rsAttention), mb_ok);
 ComboBox1.itemindex:=oldindex;
 exit;
 end;  end;
ttpoly[combobox1.ItemIndex+1].ax:=strtofloat(editx.text);

try
 strtofloat(edity.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce3),
pchar(rsAttention),mb_ok);
 ComboBox1.itemindex:=oldindex;
 exit;
 end;  end;
ttpoly[combobox1.ItemIndex+1].ay:=strtofloat(edity.text);


case radioentrant.ItemIndex of
0: tentr:=toujoursreflechi;
1: tentr:=jamaisreflechi;
2: tentr:=reflechisirefracte;
3: tentr:=reflechisipasrefracte;
end;

case radiosortant.ItemIndex of
0: tsort:=toujoursreflechi;
1: tsort:=jamaisreflechi;
2: tsort:=reflechisirefracte;
3: tsort:=reflechisipasrefracte;
end;

try
nrouge:=strtofloat(editnrouge.text);
except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce4),
 pchar(rsAttention),mb_ok);
 exit;
 end;  end;
 
try
nvert:=strtofloat(editnvert.text);
except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce5),
 pchar(rsAttention),mb_ok);
 exit;
 end;  end;


try
nbleu:=strtofloat(editnbleu.text);
except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce6),
 pchar(rsAttention),mb_ok);
 exit;
 end;  end;


 youri[combobox1.ItemIndex+1]:=(radiorectiligne.ItemIndex=0);
helena[combobox1.ItemIndex+1]:=(radiorentrant.ItemIndex=0);
tavarich[combobox1.ItemIndex+1]:=(radiogroupaigu.ItemIndex=0);
if  not(youri[combobox1.ItemIndex+1]) then
begin
 try
 strtofloat(editrayon.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce7),
 pchar(rsAttention),mb_ok);
 ComboBox1.itemindex:=oldindex;
 exit;
 end;  end;
selene[combobox1.ItemIndex+1]:=abs(strtofloat(editrayon.text));
 end;

  ttpoly[nbpoly+1].ax:=ttpoly[1].ax;
 ttpoly[nbpoly+1].ay:=ttpoly[1].ay;
 for i:=1 to nbpoly do begin
 ttsom[i].ax:=0;
 ttsom[i].ay:=0;
 end;
for i:=1 to nbpoly do if not youri[i] then begin
dAB:=sqrt(sqr(ttpoly[i+1].ax-ttpoly[i].ax)+sqr(ttpoly[i+1].ay-ttpoly[i].ay));
if dAB=0 then begin
application.messagebox(pchar(rsDeuxSommetsC),
 pchar(rsAttention),mb_ok);
 exit;
end;
if tavarich[i] then dIS:=selene[i]-sqrt(abs(sqr(selene[i])-sqr(dAB/2))) else
 dIS:=selene[i]+sqrt(abs(sqr(selene[i])-sqr(dAB/2)));
if not(helena[i]) then begin
ttsom[i].ax:=(ttpoly[i+1].ay-ttpoly[i].ay)*dIS/dAB+(ttpoly[i+1].ax+ttpoly[i].ax)/2;
ttsom[i].ay:=-(ttpoly[i+1].ax-ttpoly[i].ax)*dIS/dAB+(ttpoly[i+1].ay+ttpoly[i].ay)/2;
end else begin
ttsom[i].ax:=-(ttpoly[i+1].ay-ttpoly[i].ay)*dIS/dAB+(ttpoly[i+1].ax+ttpoly[i].ax)/2;
ttsom[i].ay:=(ttpoly[i+1].ax-ttpoly[i].ax)*dIS/dAB+(ttpoly[i+1].ay+ttpoly[i].ay)/2;
end;
 end;




ReTaillePpolycercle(Listepolycercle,nombrepolycercle,nombrepolycercle+1);

 inc(nombrepolycercle);





 if not
(Listepolycercle[-1+nombrepolycercle].create(nbpoly,ttpoly,ttsom,
youri,nrouge,nbleu,nvert,clblack,colorgrid1.selected,
tentr,tsort))
 then begin
ReTaillePpolycercle(Listepolycercle,nombrepolycercle,nombrepolycercle-1);
dec(nombrepolycercle);
application.messagebox(pchar(rsPointsAberra),
pchar(rsAttention),mb_ok);

end else
self.modalresult:=mrOk;
Rafraichit;
end;

procedure Tsaisiepolycercle.boutonsupClick(Sender: TObject);
begin

end;

procedure Tsaisiepolycercle.ComboBox1Change(Sender: TObject);
begin

 try
 strtofloat(editx.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce2),
 pchar(rsAttention),mb_ok);
 ComboBox1.itemindex:=oldindex;
 exit;
 end;  end;
ttpoly[oldindex+1].ax:=strtofloat(editx.text);

try
 strtofloat(edity.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce3),
 pchar(rsAttention),mb_ok);
 ComboBox1.itemindex:=oldindex;
 exit;
 end;  end;
ttpoly[oldindex+1].ay:=strtofloat(edity.text);

youri[oldindex+1]:=(radiorectiligne.ItemIndex=0);
helena[oldindex+1]:=(radiorentrant.ItemIndex=0);
tavarich[oldindex+1]:=(radiogroupaigu.ItemIndex=0);
if  not(youri[oldindex+1]) then
begin
 try
 strtofloat(editrayon.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce7),
 pchar(rsAttention),mb_ok);
 ComboBox1.itemindex:=oldindex;
 exit;
 end;  end;
selene[oldindex+1]:=abs(strtofloat(editrayon.text));
 end;


editx.text:=floattostr(ttpoly[ComboBox1.itemindex+1].ax);
edity.text:=floattostr(ttpoly[ComboBox1.itemindex+1].ay);
if youri[ComboBox1.itemindex+1] then radiorectiligne.ItemIndex:=0 else
 radiorectiligne.ItemIndex:=1;
 if helena[ComboBox1.itemindex+1] then radiorentrant.ItemIndex:=0 else
 radiorentrant.ItemIndex:=1;
 if tavarich[ComboBox1.itemindex+1] then radiogroupaigu.ItemIndex:=0 else
 radiogroupaigu.ItemIndex:=1;
 editrayon.text:=floattostr(selene[ComboBox1.itemindex+1]);
oldindex:=ComboBox1.itemindex;
end;

procedure Tsaisiepolycercle.combomateriauxChange(Sender: TObject);
begin
if combomateriaux.itemindex=0 then exit;
editnrouge.text:=listenrouge.strings[combomateriaux.itemindex];
editnvert.text:=listenvert.strings[combomateriaux.itemindex];
editnbleu.text:=listenbleu.strings[combomateriaux.itemindex];
end;

procedure Tsaisiepolycercle.FormCreate(Sender: TObject);
begin
   encreation:=true;
   BITBTN1.CAPTION:=rsOK;


BITBTN2.CAPTION:=rsAnnuler;


BOUTONSUP.CAPTION:=rsSupprimerCeP;


CAPTION:=rsPropriTSDeCe2;


GROUPBOX1.CAPTION:=rsSonRayonDeCo;


GROUPBOX2.CAPTION:=rsIndiceDeRFra;


GROUPBOX3.CAPTION:=rsCoordonnEsDe2;


GROUPBOX4.CAPTION:=rsCouleurLCran2;


LOG1.CAPTION:=rsLog1;


LOG2.CAPTION:=rsLog2;


LOG3.CAPTION:=rsLog3;


RADIOENTRANT.CAPTION:=rsRayonRFlChiS;
RADIOENTRANT.Items[0]:=rsToujours;
RADIOENTRANT.Items[1]:=rsJamais;
RADIOENTRANT.Items[2]:=rsSeulementSiL;
RADIOENTRANT.Items[3]:=rsSeulementSiL2;

RADIORECTILIGNE.CAPTION:=rsCeSommetEstR;
RADIORECTILIGNE.Items[0]:=rsUnSegmentDeD;
RADIORECTILIGNE.Items[1]:=rsUnArcDeCercl;



RADIORENTRANT.CAPTION:=rsCetArcDeCerc;
 RADIORENTRANT.Items[0]:=rsRentrant;
 RADIORENTRANT.Items[1]:=rsSortant;


RADIOSORTANT.CAPTION:=rsRayonRFlChiS2;
RADIOSORTANT.Items[0]:=rsToujours;
RADIOSORTANT.Items[1]:=rsJamais;
RADIOSORTANT.Items[2]:=rsSeulementSiL;
RADIOSORTANT.Items[3]:=rsSeulementSiL3;

STATICTEXT1.CAPTION:=rsRi;


STATICTEXT2.CAPTION:=rsPourLeRouge;


STATICTEXT3.CAPTION:=rsPourLeVert;


STATICTEXT4.CAPTION:=rsPourLeBleu;


STATICTEXT5.CAPTION:=rsX;


STATICTEXT6.CAPTION:=rsY;


STATICTEXT7.CAPTION:=rsMatRiau;
end;

procedure Tsaisiepolycercle.FormShow(Sender: TObject);
begin
 // if encreation then begin scalefont(self); encreation:=false; end;
end;

procedure Tsaisiepolycercle.GroupBox4Click(Sender: TObject);
begin

end;

initialization
  {$i saisiecercle.lrs}


end.
