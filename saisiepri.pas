unit saisiepri;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, LazFileUtils, LResources, Forms, Controls, Graphics, Dialogs,
  ExtCtrls, StdCtrls, Buttons, ColorBox,unit222, LCLType,UnitScaleFont,
  UChaines;

type

  { Tsaisieprisme }

  Tsaisieprisme = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    boutonsup: TBitBtn;
    colorgrid1: TColorBox;
    combomateriaux: TComboBox;
    editnbleu: TEdit;
    editnrouge: TEdit;
    editnvert: TEdit;
    Edit_hauteur: TEdit;
    Edit_anglepolaire: TEdit;
    Edit_angleausommet: TEdit;
    Edit_gy: TEdit;
    Edit_gx: TEdit;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    GroupBox4: TGroupBox;
    GroupBox5: TGroupBox;
    GroupBox6: TGroupBox;
    Image1: TImage;
    Label1: TLabel;
    Label2: TLabel;
    log2: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    log3: TLabel;
    log1: TLabel;
    radioentrant: TRadioGroup;
    radiosortant: TRadioGroup;
    StaticText2: TLabel;
    StaticText3: TLabel;
    StaticText4: TLabel;
    StaticText7: TLabel;

    procedure BitBtn1Click(Sender: TObject);
    procedure combomateriauxChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private  encreation:boolean;

    { private  encreation:boolean; declarations }
  public
    { public declarations }
  end; 

var
  saisieprisme: Tsaisieprisme;
  prisme_gx,prisme_gy,prisme_ax,prisme_ay,prisme_bx,prisme_by,
  prisme_angleausommet,prisme_anglepolaire,prisme_hauteur:extended;
prisme_couleur:tcolor;  tentr,tsort:treflechipolyhedre;
nrouge,nbleu,nvert:extended;
implementation

{ Tsaisieprisme }

procedure Tsaisieprisme.BitBtn1Click(Sender: TObject);
begin
 try
prisme_gx:=strtofloat(edit_gx.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce36),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;
 if ((prisme_gx<xxmin) or (prisme_gx>xxmax)) then begin
 application.messagebox(pchar(rsValeurInacce36),
 pchar(rsattention),mb_ok);
 exit;
 end;


  try
prisme_gy:=strtofloat(edit_gy.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce37),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;
 if ((prisme_gy<xxmin) or (prisme_gy>xxmax)) then begin
 application.messagebox(pchar(rsValeurInacce37),
 pchar(rsattention),mb_ok);
 exit;
 end;

  try
prisme_angleausommet:=strtofloat(edit_angleausommet.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce38),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;

 try
prisme_anglepolaire:=strtofloat(edit_anglepolaire.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce39),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;


 try
prisme_hauteur:=strtofloat(edit_hauteur.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce36),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;
 if ((prisme_hauteur<0) or (prisme_hauteur>xxmax)) then begin
 application.messagebox(pchar(rsValeurInacce40),
 pchar(rsattention),mb_ok);
 exit;
 end;

 prisme_couleur:=colorgrid1.Selected;

 case radioentrant.ItemIndex of
0: tentr:=toujoursreflechi;
1: tentr:=jamaisreflechi;
2: tentr:=reflechisirefracte;
3: tentr:=reflechisipasrefracte;
end;

case radiosortant.ItemIndex of
0: tsort:=toujoursreflechi;
1: tsort:=jamaisreflechi;
2: tsort:=reflechisirefracte;
3: tsort:=reflechisipasrefracte;
end;

try
nrouge:=strtofloat(editnrouge.text);
except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce4),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;

try
nvert:=strtofloat(editnvert.text);
except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce5),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;


try
nbleu:=strtofloat(editnbleu.text);
except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce6),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;

 prisme_ax:=prisme_gx+prisme_hauteur*2/3*cos(prisme_anglepolaire);
 prisme_ay:=prisme_gy+prisme_hauteur*2/3*sin(prisme_anglepolaire);
 prisme_bx:=prisme_ax+prisme_hauteur/cos(prisme_angleausommet/2)*
 cos(prisme_anglepolaire+pi-prisme_angleausommet/2);
 prisme_by:=prisme_ay+prisme_hauteur/cos(prisme_angleausommet/2)*
 sin(prisme_anglepolaire+pi-prisme_angleausommet/2);
 ReTaillePprisme(Listeprisme,nombreprisme,nombreprisme+1);

 inc(nombreprisme);
if not
(Listeprisme[-1+nombreprisme].create(prisme_gx,prisme_gy,
prisme_ax,prisme_ay,prisme_bx,prisme_by,nrouge,nbleu,nvert,clblack,colorgrid1.selected,
tentr,tsort)) then begin
ReTaillePprisme(Listeprisme,nombreprisme,nombreprisme-1);
dec(nombreprisme);
application.messagebox(pchar(rsPointsAberra),
pchar(rsattention),mb_ok);

end else
self.modalresult:=mrOk;
Rafraichit;



end;

procedure Tsaisieprisme.combomateriauxChange(Sender: TObject);
begin
 if combomateriaux.itemindex=0 then exit;
editnrouge.text:=listenrouge.strings[combomateriaux.itemindex];
editnvert.text:=listenvert.strings[combomateriaux.itemindex];
editnbleu.text:=listenbleu.strings[combomateriaux.itemindex];
end;

procedure Tsaisieprisme.FormCreate(Sender: TObject);
begin
  encreation:=true;
  BITBTN1.CAPTION
// msgctxt ';TSAISIEPRISME.BITBTN1.CAPTION';
:=rsOK;


BITBTN2.CAPTION
// msgctxt ';TSAISIEPRISME.BITBTN2.CAPTION';
:=rsAnnuler;


BOUTONSUP.CAPTION
:=rsSupprimerCeP2;


CAPTION
:=rsAjoutDUnPris;

if decimalseparator=',' then
EDITNBLEU.TEXT
:=rs17 else
 EDITNBLEU.TEXT
:=rs17b;

if decimalseparator=',' then
EDITNROUGE.TEXT
:=rs15 else
 EDITNROUGE.TEXT
:=rs15b;

 if decimalseparator=',' then
EDITNVERT.TEXT
:=rs16 else
 EDITNVERT.TEXT
:=rs16b;

GROUPBOX1.CAPTION
:=rsCentreDInert;


GROUPBOX2.CAPTION
:=rsAngleAuSomme;


GROUPBOX3.CAPTION
:=rsAnglePolaire3;


GROUPBOX4.CAPTION
:=rsHauteur;


GROUPBOX5.CAPTION
:=rsCouleurDuPri;


GROUPBOX6.CAPTION
// msgctxt ';TSAISIEPRISME.GROUPBOX6.CAPTION';
:=rsIndiceDeRFra;


LABEL1.CAPTION
// msgctxt ';TSAISIEPRISME.LABEL1.CAPTION';
:=rsX;


LABEL2.CAPTION
// msgctxt ';TSAISIEPRISME.LABEL2.CAPTION';
:=rsY;


LABEL4.CAPTION
:=rsA2;


LABEL5.CAPTION
// msgctxt ';TSAISIEPRISME.LABEL5.CAPTION';
:=rsT;


LABEL6.CAPTION
:=rsHauteur2;


LABEL7.CAPTION
// msgctxt ';TSAISIEPRISME.LABEL7.CAPTION';
:=rsRad;


LABEL8.CAPTION
// msgctxt ';TSAISIEPRISME.LABEL8.CAPTION';
:=rsRad;


LABEL9.CAPTION
:=rsCouleur;


LOG1.CAPTION
// msgctxt ';TSAISIEPRISME.LOG1.CAPTION';
:=rsLog1;


LOG2.CAPTION
// msgctxt ';TSAISIEPRISME.LOG2.CAPTION';
:=rsLog2;


LOG3.CAPTION
// msgctxt ';TSAISIEPRISME.LOG3.CAPTION';
:=rsLog3;


RADIOENTRANT.CAPTION
// msgctxt ';TSAISIEPRISME.RADIOENTRANT.CAPTION';
:=rsRayonRFlChiS;


RADIOSORTANT.CAPTION
// msgctxt ';TSAISIEPRISME.RADIOSORTANT.CAPTION';
:=rsRayonRFlChiS2;


STATICTEXT2.CAPTION
// msgctxt ';TSAISIEPRISME.STATICTEXT2.CAPTION';
:=rsPourLeRouge;


STATICTEXT3.CAPTION
// msgctxt ';TSAISIEPRISME.STATICTEXT3.CAPTION';
:=rsPourLeVert;


STATICTEXT4.CAPTION
// msgctxt ';TSAISIEPRISME.STATICTEXT4.CAPTION';
:=rsPourLeBleu;


STATICTEXT7.CAPTION
// msgctxt ';TSAISIEPRISME.STATICTEXT7.CAPTION';
:=rsMatRiau;

radiosortant.Items[0]:=rsToujours;
radiosortant.Items[1]:=rsJamais;
radiosortant.Items[2]:=rsSeulementSiL;
radiosortant.Items[3]:=rsSeulementSiL4;

radioentrant.Items[0]:=rsToujours;
radioentrant.Items[1]:=rsJamais;
radioentrant.Items[2]:=rsSeulementSiL;
radioentrant.Items[3]:=rsSeulementSiL5;

end;

procedure Tsaisieprisme.FormShow(Sender: TObject);
begin
{ if encreation then begin
 scalefont(self);
 encreation:=false;
 end;}
end;

initialization
  {$I saisiepri.lrs}

end.

