{  This file is part of OptGeo, optical simulation software
    Copyright (C) 2004  Jean-Marie Biansan
        Contact: jeanmarie.biansan@free.fr
    Web site: http://jeanmarie.biansan.free.fr/logiciel.html
 From version 1.25, OptGeo is distribued under the terms of the
 GNU General Public License.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA}


    unit saisiesphere;

{$mode objfpc}{$H+}

interface

uses
  LCLIntf, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Spin, ExtCtrls,unit222,Unit8,ColorBox, LResources,LCLType,UnitScaleFont,UChaines;

type

  { Tsaisiesph }

  Tsaisiesph = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Colorgrid1: TColorBox;
    radiosortant: TRadioGroup;
    radioentrant: TRadioGroup;
    GroupBox2: TGroupBox;
    StaticText2: TLabel;
    StaticText3: TLabel;
    StaticText4: TLabel;
    editnrouge: TEdit;
    editnvert: TEdit;
    editnbleu: TEdit;
    GroupBox3: TGroupBox;
    StaticText5: TLabel;
    StaticText6: TLabel;
    editx: TEdit;
    edity: TEdit;
    boutonsup: TBitBtn;
    GroupBox1: TGroupBox;
    editrayon: TEdit;
    StaticText1: TLabel;
    GroupBox4: TGroupBox;
    log1: TLabel;
    log2: TLabel;
    log3: TLabel;
    StaticText7: TLabel;
    combomateriaux: TComboBox;
    procedure BitBtn1Click(Sender: TObject);
    procedure combomateriauxChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);

  private  encreation:boolean;
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

var
  saisiesph: Tsaisiesph;
  
  nrouge,nvert,nbleu:extended;
  tentr,tsort:treflechipolyhedre;
  xsp,ysp,rsp:extended;
implementation


procedure Tsaisiesph.BitBtn1Click(Sender: TObject);
begin
try
 rsp:=strtofloat(editrayon.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce),
 pchar(rsAttention), mb_ok);
 exit;
 end;  end;
 rsp:=abs(rsp);

try
 xsp:=strtofloat(editx.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce2),
 pchar(rsAttention),mb_ok);
 exit;
 end;  end;

try
 ysp:=strtofloat(edity.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce3),
 pchar(rsAttention),mb_ok);
 exit;
 end;  end;


case radioentrant.ItemIndex of
0: tentr:=toujoursreflechi;
1: tentr:=jamaisreflechi;
2: tentr:=reflechisirefracte;
3: tentr:=reflechisipasrefracte;
end;

case radiosortant.ItemIndex of
0: tsort:=toujoursreflechi;
1: tsort:=jamaisreflechi;
2: tsort:=reflechisirefracte;
3: tsort:=reflechisipasrefracte;
end;

try
nrouge:=strtofloat(editnrouge.text);
except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce4),
 pchar(rsAttention),mb_ok);
 exit;
 end;  end;
 
try
nvert:=strtofloat(editnvert.text);
except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce5),
 pchar(rsAttention),mb_ok);
 exit;
 end;  end;


try
nbleu:=strtofloat(editnbleu.text);
except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce6),
 pchar(rsAttention),mb_ok);
 exit;
 end;  end;


ReTaillePsphere(Listesphere,nombresphere,nombresphere+1);

 inc(nombresphere);
if not
(Listesphere[-1+nombresphere].create(xsp,ysp,rsp,nrouge,nbleu,nvert,clblack,colorgrid1.selected,
tentr,tsort)) then begin
ReTaillePsphere(Listesphere,nombresphere,nombresphere-1);
dec(nombresphere);
application.messagebox(pchar(rsPointsAberra),
pchar(rsAttention),mb_ok);

end else
self.modalresult:=mrOk; 
Rafraichit;
end;


procedure Tsaisiesph.combomateriauxChange(Sender: TObject);
begin
if combomateriaux.itemindex=0 then exit;
editnrouge.text:=listenrouge.strings[combomateriaux.itemindex];
editnvert.text:=listenvert.strings[combomateriaux.itemindex];
editnbleu.text:=listenbleu.strings[combomateriaux.itemindex];
end;

procedure Tsaisiesph.FormCreate(Sender: TObject);
begin
   encreation:=true;
   BITBTN1.CAPTION
:=rsOK;


BITBTN2.CAPTION
:=rsAnnuler;


BOUTONSUP.CAPTION
:=rsSupprimerCet;


CAPTION
:=rsPropriTSDeCe;


if decimalseparator=',' then
EDITNBLEU.TEXT
:=rs17 else
 EDITNBLEU.TEXT
:=rs17b;

if decimalseparator=',' then
EDITNROUGE.TEXT
:=rs15 else
 EDITNROUGE.TEXT
:=rs15b;

 if decimalseparator=',' then
EDITNVERT.TEXT
:=rs16 else
 EDITNVERT.TEXT
:=rs16b;


GROUPBOX1.CAPTION
:=rsRayonDeCourb;


GROUPBOX2.CAPTION
:=rsIndiceDeRFra;


GROUPBOX3.CAPTION
:=rsCoordonnEsDu;


GROUPBOX4.CAPTION
:=rsCouleurLCran;


LOG1.CAPTION
:=rsLog1;


LOG2.CAPTION
:=rsLog2;


LOG3.CAPTION
:=rsLog3;


RADIOENTRANT.CAPTION
:=rsRayonRFlChiS;


RADIOSORTANT.CAPTION
:=rsRayonRFlChiS2;


STATICTEXT1.CAPTION
:=rsR;


STATICTEXT2.CAPTION
:=rsPourLeRouge;


STATICTEXT3.CAPTION
:=rsPourLeVert;


STATICTEXT4.CAPTION
:=rsPourLeBleu;


STATICTEXT5.CAPTION
:=rsX;


STATICTEXT6.CAPTION
:=rsY;


STATICTEXT7.CAPTION
:=rsMatRiau;

radioentrant.Items[0]:=rsToujours;
radioentrant.Items[1]:=rsJamais;
radioentrant.Items[2]:=rsSeulementSiL;
radioentrant.Items[3]:=rsSeulementSiL6;

radiosortant.Items[0]:=rsToujours;
radiosortant.Items[1]:=rsJamais;
radiosortant.Items[2]:=rsSeulementSiL;
radiosortant.Items[3]:=rsSeulementSiL7;
end;

procedure Tsaisiesph.FormShow(Sender: TObject);
begin
 // if encreation then begin scalefont(self); encreation:=false; end;
end;

initialization
  {$i saisiesphere.lrs}


end.
