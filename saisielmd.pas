{  This file is part of OptGeo, optical simulation software
    Copyright (C) 2004  Jean-Marie Biansan
        Contact: jeanmarie.biansan@free.fr
    Web site: http://jeanmarie.biansan.free.fr/logiciel.html
 From version 1.25, OptGeo is distribued under the terms of the
 GNU General Public License.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA}


    unit saisielmd;

{$mode objfpc}{$H+}

interface

uses
  LCLIntf, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, Buttons, Spin,ColorBox, LResources,LCLType,UnitScaleFont,
  UChaines;

type

  { Tsaisielentillemd }

  Tsaisielentillemd = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    gridaxe: TColorBox;
    Colorgrid1: TColorBox;
    Image1: TImage;
    GroupBox1: TGroupBox;
    StaticText1: TLabel;
    StaticText2: TLabel;
    editx1: TEdit;
    edity1: TEdit;
    GroupBox2: TGroupBox;
    StaticText3: TLabel;
    StaticText4: TLabel;
    editx2: TEdit;
    edity2: TEdit;
    GroupBox3: TGroupBox;
    GroupBox4: TGroupBox;
    editepaisseur: TSpinEdit;
    GroupBox5: TGroupBox;
    cocheaxe: TCheckBox;
    GroupBox6: TGroupBox;
    StaticText5: TLabel;
    editfocale: TEdit;
    GroupBox7: TGroupBox;
    boutonsup: TBitBtn;
    log1: TLabel;
    log2: TLabel;
    log3: TLabel;
    log4: TLabel;
    log5: TLabel;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private  encreation:boolean;
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

var
  saisielentillemd: Tsaisielentillemd;
   lmd_axe:boolean;
  lmd_epaisseur:integer;
  lmd_couleur,lmd_couleuraxe:tcolor;
  lmd_x1,lmd_x2,lmd_y1,lmd_y2,lmd_focale:extended;
implementation

uses unit222;


procedure Tsaisielentillemd.BitBtn1Click(Sender: TObject);
begin

lmd_axe:=cocheaxe.Checked;
lmd_epaisseur:=editepaisseur.Value;
lmd_couleur:=colorgrid1.selected;
lmd_couleuraxe:=gridaxe.selected;
try
lmd_x1:=strtofloat(editx1.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce13),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;

 try
lmd_x2:=strtofloat(editx2.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce14),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;


 try
lmd_y1:=strtofloat(edity1.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce15),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;


 try
lmd_y2:=strtofloat(edity2.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce16),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;



 try
lmd_focale:=strtofloat(editfocale.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce24),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;
 if (lmd_focale=0) then begin
 application.messagebox(pchar(rsLaFocaleDoit),
 pchar(rsattention),mb_ok);
 exit;
 end;
 lmd_focale:=abs(lmd_focale);
  ReTaillePLmd(ListeLmd,nombrelmd,nombrelmd+1);
 inc(nombrelmd);
if not
(Listelmd[-1+nombrelmd].create2(lmd_x1,lmd_y1,lmd_x2,lmd_y2,
lmd_focale,lmd_epaisseur,
lmd_couleur,lmd_couleuraxe,lmd_axe)) then begin
ReTaillePLmd(ListeLmd,nombrelmd,nombrelmd-1);
dec(nombrelmd);
application.messagebox(pchar(rsDonnEsAberra2),
pchar(rsattention),mb_ok);
end else
self.ModalResult:=mrok;
Rafraichit;


end;

procedure Tsaisielentillemd.FormCreate(Sender: TObject);
begin
   encreation:=true;
   BITBTN1.CAPTION
:=rsOK;


BITBTN2.CAPTION
:=rsAnnuler;


BOUTONSUP.CAPTION
:=rsSupprimerCet5;


CAPTION
:=rsAjoutDUneLen6;


COCHEAXE.CAPTION
:=rsAxeOptique;


GROUPBOX1.CAPTION
:=rsPoint1;


GROUPBOX2.CAPTION
:=rsPoint2;


GROUPBOX3.CAPTION
:=rsCouleurDeLaL;


GROUPBOX4.CAPTION
:=rsEpaisseurTra4;


GROUPBOX5.CAPTION
:=rsAspect;


GROUPBOX6.CAPTION
:=rsFocaleValeur;


GROUPBOX7.CAPTION
:=rsCouleurDeLAx2;


LOG1.CAPTION
:=rsLog1;


LOG2.CAPTION
:=rsLog2;


LOG3.CAPTION
:=rsLog3;


LOG4.CAPTION
:=rsLog4;


LOG5.CAPTION
:=rsLog5;


STATICTEXT1.CAPTION
:=rsX;


STATICTEXT2.CAPTION
:=rsY;


STATICTEXT3.CAPTION
:=rsX;


STATICTEXT4.CAPTION
:=rsY;


STATICTEXT5.CAPTION
:=rsF;

end;

procedure Tsaisielentillemd.FormShow(Sender: TObject);
begin
 // if encreation then begin scalefont(self); encreation:=false; end;
end;

initialization
  {$i saisielmd.lrs}


end.
