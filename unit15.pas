{  This file is part of OptGeo, optical simulation software
    Copyright (C) 2004  Jean-Marie Biansan
        Contact: jeanmarie.biansan@free.fr
    Web site: http://jeanmarie.biansan.free.fr/logiciel.html
 From version 1.25, OptGeo is distribued under the terms of the
 GNU General Public License.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA}

    unit Unit15;

{$mode objfpc}{$H+}

interface

uses
  LCLIntf, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, LResources,UnitScaleFont,UChaines;

type

  { Tsaisiejoindre }

  Tsaisiejoindre = class(TForm)
    BitBtn1: TBitBtn;
    RadioGroup1: TRadioGroup;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private  encreation:boolean;
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

var
  saisiejoindre: Tsaisiejoindre;
  rere:boolean;
implementation


procedure Tsaisiejoindre.BitBtn1Click(Sender: TObject);
begin
 rere:=radiogroup1.ItemIndex=1;
end;

procedure Tsaisiejoindre.FormCreate(Sender: TObject);
begin
   encreation:=true;
   BITBTN1.CAPTION:=rsOK;
CAPTION:=rsRelierLesPoi2;
RADIOGROUP1.CAPTION:=rsJoindreLesPo;
radiogroup1.Items[0]:=rsUnArcDeCercl;
radiogroup1.Items[1]:=rsUnSegmentDeD;
end;

procedure Tsaisiejoindre.FormShow(Sender: TObject);
begin
 // if encreation then begin scalefont(self); encreation:=false; end;
end;

initialization
  {$i Unit15.lrs}

end.
