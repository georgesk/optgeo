{  This file is part of OptGeo, optical simulation software
    Copyright (C) 2004  Jean-Marie Biansan
        Contact: jeanmarie.biansan@free.fr
    Web site: http://jeanmarie.biansan.free.fr/logiciel.html
 From version 1.25, OptGeo is distribued under the terms of the
 GNU General Public License.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA}


    unit RTFtoPlainText;

{$mode objfpc}{$H+}

interface
 function Rtf2PlainText(const rtf:string):string;
implementation
 function Rtf2PlainText(const rtf:string):string;
 var s:string;
 niveau,i:integer; slash:boolean;
 label 999;
 begin
 niveau:=0; i:=0; slash:=false; s:='';
   if rtf='' then begin
   result:='';
   exit;
   end;
   
 repeat
 inc(i);

 if rtf[i]='{' then begin
 inc(niveau);
 goto 999;
 end;

 if rtf[i]='}' then begin
 dec(niveau);
 goto 999;
 end;

 if ((rtf[i]='\') and (i+3<=length(rtf))) then
 if copy(rtf,i,4)='\''e9' then
 begin
 s:=s+'é';
 inc(i,3);
 goto 999;
 end;

 if ((rtf[i]='\') and (i+3<=length(rtf))) then
 if copy(rtf,i,4)='\''ea' then
 begin
 s:=s+'ê';
 inc(i,3);
 goto 999;
 end;

  if ((rtf[i]='\') and (i+3<=length(rtf))) then
 if copy(rtf,i,4)='\''e8' then
 begin
 s:=s+'è';
 inc(i,3);
 goto 999;
 end;

  if ((rtf[i]='\') and (i+3<=length(rtf))) then
 if copy(rtf,i,4)='\''e0' then
 begin
 s:=s+'à';
 inc(i,3);
 goto 999;
 end;

  if ((rtf[i]='\') and (i+3<=length(rtf))) then
 if copy(rtf,i,4)='\''b0' then
 begin
 s:=s+'°';
 inc(i,3);
 goto 999;
 end;

 if ((rtf[i]='\') and (i+3<=length(rtf))) then
 if copy(rtf,i,4)='\''ee' then
 begin
 s:=s+'î';
 inc(i,3);
 goto 999;
 end;

 if ((rtf[i]='\') and (i+3<=length(rtf))) then
 if copy(rtf,i,4)='\''f9' then
 begin
 s:=s+'ù';
 inc(i,3);
 goto 999;
 end;

 if ((rtf[i]='\') and (i+3<=length(rtf))) then
 if copy(rtf,i,4)='\''eb' then
 begin
 s:=s+'ë';
 inc(i,3);
 goto 999;
 end;

 if ((rtf[i]='\') and (i+3<=length(rtf))) then
 if copy(rtf,i,4)='\''e4' then
 begin
 s:=s+'ä';
 inc(i,3);
 goto 999;
 end;

 if ((rtf[i]='\') and (i+3<=length(rtf))) then
 if copy(rtf,i,4)='\''e7' then
 begin
 s:=s+'ç';
 inc(i,3);
 goto 999;
 end;

 if ((rtf[i]='\') and (i+3<=length(rtf))) then
 if copy(rtf,i,4)='\''b5' then
 begin
 s:=s+'µ';
 inc(i,3);
 goto 999;
 end;




 if rtf[i]='\' then begin
 slash:=true;
 goto 999;
 end;

 if ((rtf[i]=' ') and slash) then begin
 slash:=false;
 goto 999;
 end;

  if ((niveau=0) and not(slash) and
  not(rtf[i]=#00)) then
 begin
 s:=s+rtf[i];
 goto 999;
 end;
999:
 until i=length(rtf);

 result:=s;


end;

end.
