{  This file is part of OptGeo, optical simulation software
    Copyright (C) 2004  Jean-Marie Biansan
        Contact: jeanmarie.biansan@free.fr
    Web site: http://jeanmarie.biansan.free.fr/logiciel.html
 From version 1.25, OptGeo is distribued under the terms of the
 GNU General Public License.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA}

    unit Unit9;

{$mode objfpc}{$H+}

interface

uses
 LCLIntf, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, Buttons, Spin,ColorBox, LResources,LCLType,
 UnitScaleFont,UChaines;

type

  { Tsaisietexte }

  Tsaisietexte = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    FontDialog1: TFontDialog;
    GroupBox1: TGroupBox;
    Edit1: TEdit;
    SpeedButton1: TSpeedButton;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private  encreation:boolean;
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

var
  saisietexte: Tsaisietexte;
   fontetexte:tfont;
   texteaafficher:string;
implementation


procedure Tsaisietexte.SpeedButton1Click(Sender: TObject);
begin
if fontdialog1.Execute then 
fontetexte:=fontdialog1.Font;
end;

procedure Tsaisietexte.FormShow(Sender: TObject);
begin
 // if encreation then begin scalefont(self); encreation:=false; end;
end;

procedure Tsaisietexte.FormCreate(Sender: TObject);
begin
   encreation:=true;
   BITBTN1.CAPTION
// msgctxt ';TSAISIETEXTE.BITBTN1.CAPTION';
:=rsOK;


BITBTN2.CAPTION
// msgctxt ';TSAISIETEXTE.BITBTN2.CAPTION';
:=rsAnnuler;


CAPTION
// msgctxt ';TSAISIETEXTE.CAPTION';
:=rsSaisirLeText;


GROUPBOX1.CAPTION
// msgctxt ';TSAISIETEXTE.GROUPBOX1.CAPTION';
:=rsTexte;


SPEEDBUTTON1.CAPTION
// msgctxt ';TSAISIETEXTE.SPEEDBUTTON1.CAPTION';
:=rsPolice;

end;

procedure Tsaisietexte.BitBtn1Click(Sender: TObject);
begin
texteaafficher:=edit1.Text;
end;


initialization
 {$i unit9.lrs}
end.
