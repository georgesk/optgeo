{  This file is part of OptGeo, optical simulation software
    Copyright (C) 2004  Jean-Marie Biansan
        Contact: jeanmarie.biansan@free.fr
    Web site: http://jeanmarie.biansan.free.fr/logiciel.html
 From version 1.25, OptGeo is distribued under the terms of the
 GNU General Public License.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA}

    unit Unit11;

{$mode objfpc}{$H+}

interface

uses
  LCLIntf, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons,ray1, ExtCtrls,unit222, LResources,LCLType,UnitScaleFont,
  UChaines;

type

  { Tsaisieespacetravail }

  Tsaisieespacetravail = class(TForm)
    BitBtn1: TBitBtn;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    cocherespect: TCheckBox;
    StaticText1: TLabel;
    StaticText2: TLabel;
    editxxmax: TEdit;
    edityymax: TEdit;
    rapporthl: TGroupBox;
    StaticText5: TLabel;
    statrapportactuel: TLabel;
    StaticText6: TLabel;
    StaticText7: TLabel;
    statrapportideal: TLabel;
    radiounite: TRadioGroup;
    saisiexxmin: TGroupBox;
    editxxmin: TEdit;
    saisieyymin: TGroupBox;
    edityymin: TEdit;
    procedure BitBtn1Click(Sender: TObject);
    procedure editxxmaxChange(Sender: TObject);
    procedure edityymaxChange(Sender: TObject);
    procedure editxxminChange(Sender: TObject);
    procedure edityyminChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);

  private  encreation:boolean;
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

var
  saisieespacetravail: Tsaisieespacetravail;
  sa_largeur,sa_hauteur:extended;
  sa_respect:boolean;
  sa_yymin,sa_xxmin,sa_xxmax,sa_yymax:extended;
implementation




procedure Tsaisieespacetravail.BitBtn1Click(Sender: TObject);
begin
 sa_respect:=cocherespect.checked;
  try
 sa_xxmin:=strtofloat(editxxmin.text);
 except
 application.messagebox(pchar(rsExpressionIl5), pchar(rsattention), mb_ok);
 exit;
 end;
  try
 sa_yymin:=strtofloat(edityymin.text);
 except
 application.messagebox(pchar(rsExpressionIl6), pchar(rsattention), mb_ok);
 exit;
 end;

 try
 sa_xxmax:=strtofloat(editxxmax.text);
 except
 application.messagebox(pchar(rsExpressionIl7), pchar(rsattention), mb_ok);
 exit;
 end;
  try
 sa_yymax:=strtofloat(edityymax.text);
 except
 application.messagebox(pchar(rsExpressionIl8), pchar(rsattention), mb_ok);
 exit;
 end;

 sa_largeur:=sa_xxmax-sa_xxmin;
    sa_hauteur:=sa_yymax-sa_yymin;
 if ((sa_hauteur<=0) or (sa_largeur<=0)) then begin
 application.messagebox(pchar(rsValeurNulleO), pchar(rsattention), mb_ok);
 exit;
 end;
 unitelongueur:=radiounite.items[radiounite.itemindex];
end;

procedure Tsaisieespacetravail.editxxmaxChange(Sender: TObject);
begin
try
sa_largeur:=strtofloat(editxxmax.text)-strtofloat(editxxmin.text);
except
exit;
end;
try
 sa_hauteur:=strtofloat(edityymax.text)-strtofloat(edityymin.text);
except
exit;
end;
 if sa_largeur=0 then exit;
 statrapportactuel.Caption:=floattostr(sa_hauteur/sa_largeur);
end;

procedure Tsaisieespacetravail.edityymaxChange(Sender: TObject);
begin
try
sa_largeur:=strtofloat(editxxmax.text)-strtofloat(editxxmin.text);
except
exit;
end;
try
 sa_hauteur:=strtofloat(edityymax.text)-strtofloat(edityymin.text);
except
exit;
end;
 if sa_largeur=0 then exit;
 statrapportactuel.Caption:=floattostr(sa_hauteur/sa_largeur);
end;




procedure Tsaisieespacetravail.editxxminChange(Sender: TObject);
begin
try
sa_largeur:=strtofloat(editxxmax.text)-strtofloat(editxxmin.text);
except
exit;
end;
try
 sa_hauteur:=strtofloat(edityymax.text)-strtofloat(edityymin.text);
except
exit;
end;
 if sa_largeur=0 then exit;
 statrapportactuel.Caption:=floattostr(sa_hauteur/sa_largeur);
end;

procedure Tsaisieespacetravail.edityyminChange(Sender: TObject);
begin
try
sa_largeur:=strtofloat(editxxmax.text)-strtofloat(editxxmin.text);
except
exit;
end;
try
 sa_hauteur:=strtofloat(edityymax.text)-strtofloat(edityymin.text);
except
exit;
end;
 if sa_largeur=0 then exit;
 statrapportactuel.Caption:=floattostr(sa_hauteur/sa_largeur);
end;

procedure Tsaisieespacetravail.FormCreate(Sender: TObject);
begin
   encreation:=true;

BITBTN1.CAPTION
// msgctxt ';TSAISIEESPACETRAVAIL.BITBTN1.CAPTION';
:=rsOK;


CAPTION
:=rsDimensionsDe;


COCHERESPECT.CAPTION
:=rsRespecterLeR2;


EDITXXMIN.TEXT
// msgctxt ';TSAISIEESPACETRAVAIL.EDITXXMIN.TEXT';
:=rs0;


EDITYYMIN.TEXT
// msgctxt ';TSAISIEESPACETRAVAIL.EDITYYMIN.TEXT';
:=rs0;


GROUPBOX1.CAPTION
:=rsAbscisseMaxi;


GROUPBOX2.CAPTION
:=rsOrdonnEsMaxi;


GROUPBOX3.CAPTION
:=rsRepReOrthono;


RADIOUNITE.CAPTION
:=rsUnitDeLongue;


RAPPORTHL.CAPTION
// msgctxt ';TSAISIEESPACETRAVAIL.RAPPORTHL.CAPTION';
:=rsRapportHaute;


SAISIEXXMIN.CAPTION
:=rsAbscisseMini;


SAISIEYYMIN.CAPTION
:=rsOrdonnEMinim;


STATICTEXT1.CAPTION
:=rsRemarqueLeNo;


STATICTEXT2.CAPTION
:=rsLesAnglesLCr;


STATICTEXT5.CAPTION
:=rsRapport;


STATICTEXT6.CAPTION
:=rsRapportIdAlP;


STATICTEXT7.CAPTION
:=rsDeSurfaceDCr;


STATRAPPORTACTUEL.CAPTION
:=rs2;


STATRAPPORTIDEAL.CAPTION
:=rs3;

radiounite.Items[0]:=rsKm;
radiounite.Items[1]:=rsHm;
radiounite.Items[2]:=rsDam;
radiounite.Items[3]:=rsM;
radiounite.Items[4]:=rsDm;
radiounite.Items[5]:=rsCm;
radiounite.Items[6]:=rsMm;
radiounite.Items[7]:=rsM2;



end;

procedure Tsaisieespacetravail.FormShow(Sender: TObject);
begin
  //if encreation then begin scalefont(self); encreation:=false; end;
end;

initialization
  {$i Unit11.lrs}


end.
