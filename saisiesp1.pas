{  This file is part of OptGeo, optical simulation software
    Copyright (C) 2004  Jean-Marie Biansan
        Contact: jeanmarie.biansan@free.fr
    Web site: http://jeanmarie.biansan.free.fr/logiciel.html
 From version 1.25, OptGeo is distribued under the terms of the
 GNU General Public License.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA}

    unit saisiesp1;

{$mode objfpc}{$H+}

interface

uses
  LCLIntf, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls,unit222, Buttons, Spin, LResources,LCLType,UnitScaleFont,
  UChaines;

type

  { Tsaisiesp }

  Tsaisiesp = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    GroupBox9: TGroupBox;
    Image1: TImage;
    GroupBox1: TGroupBox;
    Spinmaxsegment: TSpinEdit;
    StaticText1: TLabel;
    StaticText18: TLabel;
    StaticText2: TLabel;
    editx1: TEdit;
    edity1: TEdit;
    GroupBox2: TGroupBox;
    StaticText3: TLabel;
    StaticText4: TLabel;
    editx2: TEdit;
    edity2: TEdit;
    radiocouleur: TRadioGroup;
    GroupBox3: TGroupBox;
    editepaisseur: TSpinEdit;
    GroupBox4: TGroupBox;
    GroupBox5: TGroupBox;
    StaticText5: TLabel;
    StaticText6: TLabel;
    editsx: TEdit;
    editsy: TEdit;
    GroupBox6: TGroupBox;
    spinnombrerayons: TSpinEdit;
    boutonsup: TBitBtn;
    GroupBox7: TGroupBox;
    StaticText7: TLabel;
    Spinmaxenfant: TSpinEdit;
    radiovirtuelle: TRadioGroup;
    StaticText8: TLabel;
    StaticText9: TLabel;
    StaticText10: TLabel;
    StaticText11: TLabel;
    StaticText12: TLabel;
    editvav: TEdit;
    editvaa: TEdit;
    log1: TLabel;
    log2: TLabel;
    log3: TLabel;
    log4: TLabel;
    log5: TLabel;
    log6: TLabel;
    GroupBox8: TGroupBox;
    cochesurfacesonde: TCheckBox;
    StaticText13: TLabel;
    StaticText14: TLabel;
    StaticText15: TLabel;
    editlistechemins: TEdit;
    StaticText16: TLabel;
    StaticText17: TLabel;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure radiovirtuelleClick(Sender: TObject);
  private  encreation:boolean;
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

var
  saisiespp: Tsaisiesp;
  ra_couleur:tcolor;
  ra_epaisseur,ra_nombrerayons,ra_maxenfant,ra_maxsegment:integer;
  ra_x1,ra_x2,ra_y1,ra_y2,ra_sx,ra_sy:extended;
  ra_virtuelavant,ra_virtuelarriere,ravirtuelle:boolean;
implementation


procedure Tsaisiesp.BitBtn1Click(Sender: TObject);
begin
ra_epaisseur:=editepaisseur.value;
ra_nombrerayons:=spinnombrerayons.value;
ra_maxenfant:=spinmaxenfant.value;
ra_maxsegment:=spinmaxsegment.value;
case radiocouleur.ItemIndex of
0: ra_couleur:=clred;
1: ra_couleur:=clgreen;
2: ra_couleur:=clblue;
3: ra_couleur:=clblack;
end;

try
ra_sx:=strtofloat(editsx.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce41),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;
 if ((ra_sx<xxmin) or (ra_sx>xxmax)) then begin
 application.messagebox(pchar(rsValeurInacce41),
 pchar(rsattention),mb_ok);
 exit;
 end;

 try
ra_sy:=strtofloat(editsy.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce42),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;
 if ((ra_sy<yymin) or (ra_sy>yymax)) then begin
 application.messagebox(pchar(rsValeurInacce42),
 pchar(rsattention),mb_ok);
 exit;
 end;

try
ra_x1:=strtofloat(editx1.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce13),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;
 if ((ra_x1<xxmin) or (ra_x1>xxmax)) then begin
 application.messagebox(pchar(rsValeurInacce13),
 pchar(rsattention),mb_ok);
 exit;
 end;

 try
ra_x2:=strtofloat(editx2.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce14),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;
 if ((ra_x2<xxmin) or (ra_x2>xxmax)) then begin
 application.messagebox(pchar(rsValeurInacce14),
 pchar(rsattention),mb_ok);
 exit;
 end;

 try
ra_y1:=strtofloat(edity1.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce15),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;
 if ((ra_y1<yymin) or (ra_y1>yymax)) then begin
 application.messagebox(pchar(rsValeurInacce15),
 pchar(rsattention),mb_ok);
 exit;
 end;

 try
ra_y2:=strtofloat(edity2.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce16),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;
 if ((ra_y2<yymin) or (ra_y2>yymax)) then begin
 application.messagebox(pchar(rsValeurInacce16),
 pchar(rsattention),mb_ok);
 exit;
 end;

 ravirtuelle:=(radiovirtuelle.itemindex=1);
 ReTaillePsourceponctuelle(Listesourceponctuelle,nombresourceponctuelle,nombresourceponctuelle+1);
 inc(nombresourceponctuelle);
if not
(ListeSourcePonctuelle[-1+nombresourceponctuelle].create(ra_nombrerayons,
ra_sx,ra_sy,ra_x1,ra_y1,ra_x2,ra_y2,
ra_couleur,ra_epaisseur,editvav.text,editvaa.text,ra_maxenfant,ravirtuelle,
cochesurfacesonde.checked,editlistechemins.text,ra_maxsegment)) then begin
ReTaillePsourceponctuelle(Listesourceponctuelle,nombresourceponctuelle,nombresourceponctuelle-1);
dec(nombresourceponctuelle);
application.messagebox(pchar(rsValeursEntrE),
pchar(rsattention),mb_ok);
end else
modalresult:=mrok;
Rafraichit;
end;

procedure Tsaisiesp.FormCreate(Sender: TObject);
begin
   encreation:=true;
   BITBTN1.CAPTION
// msgctxt ';TSAISIESP.BITBTN1.CAPTION';
:=rsOK;


BITBTN2.CAPTION
// msgctxt ';TSAISIESP.BITBTN2.CAPTION';
:=rsAnnuler;


BOUTONSUP.CAPTION
:=rsSupprimerCet8;


CAPTION
:=rsSaisieDUneSo;


COCHESURFACESONDE.CAPTION
// msgctxt ';TSAISIESP.COCHESURFACESONDE.CAPTION';
:=rsTracer;


GROUPBOX1.CAPTION
// msgctxt ';TSAISIESP.GROUPBOX1.CAPTION';
:=rsPoint1;


GROUPBOX2.CAPTION
// msgctxt ';TSAISIESP.GROUPBOX2.CAPTION';
:=rsPoint2;


GROUPBOX3.CAPTION
// msgctxt ';TSAISIESP.GROUPBOX3.CAPTION';
:=rsEpaisseurLCr3;


GROUPBOX4.CAPTION
// msgctxt ';TSAISIESP.GROUPBOX4.CAPTION';
:=rsTracerEnPoin3;


GROUPBOX5.CAPTION
// msgctxt ';TSAISIESP.GROUPBOX5.CAPTION';
:=rsSource;


GROUPBOX6.CAPTION
// msgctxt ';TSAISIESP.GROUPBOX6.CAPTION';
:=rsNombreDeRayo3;


GROUPBOX7.CAPTION
// msgctxt ';TSAISIESP.GROUPBOX7.CAPTION';
:=rsPourChaqueRa;


GROUPBOX8.CAPTION
:=rsTracerDesSur2;


GROUPBOX9.CAPTION
:=rsNombreMaxDeS3;


LOG1.CAPTION
// msgctxt ';TSAISIESP.LOG1.CAPTION';
:=rsLog1;


LOG2.CAPTION
// msgctxt ';TSAISIESP.LOG2.CAPTION';
:=rsLog2;


LOG3.CAPTION
// msgctxt ';TSAISIESP.LOG3.CAPTION';
:=rsLog3;


LOG4.CAPTION
// msgctxt ';TSAISIESP.LOG4.CAPTION';
:=rsLog4;


LOG5.CAPTION
// msgctxt ';TSAISIESP.LOG5.CAPTION';
:=rsLog5;


LOG6.CAPTION
// msgctxt ';TSAISIESP.LOG6.CAPTION';
:=rsLog6;


RADIOCOULEUR.CAPTION
:=rsCouleurDeLaS;


RADIOVIRTUELLE.CAPTION
:=rsTypeDeSource;


STATICTEXT1.CAPTION
// msgctxt ';TSAISIESP.STATICTEXT1.CAPTION';
:=rsX;


STATICTEXT10.CAPTION
// msgctxt ';TSAISIESP.STATICTEXT10.CAPTION';
:=rsListeDesSegm;


STATICTEXT11.CAPTION
// msgctxt ';TSAISIESP.STATICTEXT11.CAPTION';
:=rsEntreParenth;


STATICTEXT12.CAPTION
// msgctxt ';TSAISIESP.STATICTEXT12.CAPTION';
:=rsEtSansEspace;


STATICTEXT13.CAPTION
// msgctxt ';TSAISIESP.STATICTEXT13.CAPTION';
:=rsDonnerLesVal2;


STATICTEXT14.CAPTION
// msgctxt ';TSAISIESP.STATICTEXT14.CAPTION';
:=rsDOndeSParezC2;


STATICTEXT15.CAPTION
// msgctxt ';TSAISIESP.STATICTEXT15.CAPTION';
:=rsExemple12014;


STATICTEXT16.CAPTION
// msgctxt ';TSAISIESP.STATICTEXT16.CAPTION';
:=rsRemarqueSiLa2;


STATICTEXT17.CAPTION
// msgctxt ';TSAISIESP.STATICTEXT17.CAPTION';
:=rsNePeutTreEtN;


STATICTEXT18.CAPTION
// msgctxt ';TSAISIESP.STATICTEXT18.CAPTION';
:=rsNombreMaxima35;


STATICTEXT2.CAPTION
// msgctxt ';TSAISIESP.STATICTEXT2.CAPTION';
:=rsY;


STATICTEXT3.CAPTION
// msgctxt ';TSAISIESP.STATICTEXT3.CAPTION';
:=rsX;


STATICTEXT4.CAPTION
// msgctxt ';TSAISIESP.STATICTEXT4.CAPTION';
:=rsY;


STATICTEXT5.CAPTION
// msgctxt ';TSAISIESP.STATICTEXT5.CAPTION';
:=rsX;


STATICTEXT6.CAPTION
// msgctxt ';TSAISIESP.STATICTEXT6.CAPTION';
:=rsY;


STATICTEXT7.CAPTION
// msgctxt ';TSAISIESP.STATICTEXT7.CAPTION';
:=rsNombreMaxima35;


STATICTEXT8.CAPTION
// msgctxt ';TSAISIESP.STATICTEXT8.CAPTION';
:=rsVersLAvant3;


STATICTEXT9.CAPTION
// msgctxt ';TSAISIESP.STATICTEXT9.CAPTION';
:=rsVersLArriRe3;

radiocouleur.Items[0]:=rsRouge;
radiocouleur.Items[1]:=rsVert;
radiocouleur.Items[2]:=rsBleu;
radiocouleur.Items[3]:=rsLumiReBlanch;

end;

procedure Tsaisiesp.FormShow(Sender: TObject);
begin
 // if encreation then begin scalefont(self); encreation:=false; end;
end;

procedure Tsaisiesp.radiovirtuelleClick(Sender: TObject);
begin
if radiovirtuelle.itemindex=1 then begin

 cochesurfacesonde.enabled:=false;
 editlistechemins.enabled:=false;
 end;
 if radiovirtuelle.itemindex=0 then begin

    cochesurfacesonde.enabled:=true;
 editlistechemins.enabled:=true;
 end;
end;

initialization
  {$i saisiesp1.lrs}


end.
