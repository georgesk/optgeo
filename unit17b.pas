unit Unit17b; 

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, LazFileUtils, LResources, Forms, Controls, Graphics, Dialogs,
  StdCtrls,UChaines,translations,UnitScaleFont;

type

  { Tsplashscreen }

  Tsplashscreen = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { private declarations }
    encreation:boolean;
  public
    { public declarations }
  end; 

var
  splashscreen: Tsplashscreen;
    Lang, FallbackLang: String;
implementation

{ Tsplashscreen }

procedure Tsplashscreen.FormCreate(Sender: TObject);
 var
      PODirectory: String;
      f:textfile;  nom_ini_file:string;
      label 1888;
    begin

      encreation:=true;



 Caption := rsSplashscreen;
 Label1.Caption := rsOptgeo2;
 Label2.Caption := rsLogicielLibr;
 Label3.Caption := rsVersion205Du;
 Label4.Caption := rsAuteurJeanMa;


end;

procedure Tsplashscreen.FormShow(Sender: TObject);
begin
  // if encreation then begin scalefont(self); encreation:=false; end;
   label1.Left:=(splashscreen.ClientWidth div 2)- (label1.Canvas.TextWidth(label1.Caption) div 2);
   label2.Left:=(splashscreen.ClientWidth div 2)- (label2.Canvas.TextWidth(label2.Caption) div 2);
   label3.Left:=(splashscreen.ClientWidth div 2)- (label3.Canvas.TextWidth(label3.Caption) div 2);
   label4.Left:=(splashscreen.ClientWidth div 2)- (label4.Canvas.TextWidth(label4.Caption) div 2);
end;

initialization
  {$I unit17b.lrs}

end.

