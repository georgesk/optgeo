{  This file is part of OptGeo, optical simulation software
    Copyright (C) 2004  Jean-Marie Biansan
        Contact: jeanmarie.biansan@free.fr
    Web site: http://jeanmarie.biansan.free.fr/logiciel.html
 From version 1.25, OptGeo is distribued under the terms of the
 GNU General Public License.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA}


    unit saisiefant;

{$mode objfpc}{$H+}

interface

uses
  LCLIntf, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons,unit222, LResources,UnitScaleFont;

type

  { Tsaisiefantomes }

  Tsaisiefantomes = class(TForm)
    BitBtn1: TBitBtn;
    GroupBox1: TGroupBox;
    StaticText1: TLabel;
    StaticText2: TLabel;
    StaticText3: TLabel;
    StaticText4: TLabel;
    cochefantomes: TCheckBox;
    StaticText5: TLabel;
    StaticText6: TLabel;
    editfantomes: TEdit;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private  encreation:boolean;
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

var
  saisiefantomes: Tsaisiefantomes;

implementation


procedure Tsaisiefantomes.BitBtn1Click(Sender: TObject);
var i:integer;
begin
fantomespresents:=cochefantomes.checked;
listefantomes:=editfantomes.text;
if fantomespresents then
for i:=1 to maxtotalsegment do
  fantomes[i]:=(pos(','+inttostr(i)+',',listefantomes)<>0) or (pos('('+inttostr(i)+',',listefantomes)<>0) or
  (pos(','+inttostr(i)+')',listefantomes)<>0) or
  (pos('('+inttostr(i)+')',listefantomes)<>0);
end;

procedure Tsaisiefantomes.FormCreate(Sender: TObject);
begin
   encreation:=true;
end;

procedure Tsaisiefantomes.FormShow(Sender: TObject);
begin
 // if encreation then begin scalefont(self); encreation:=false; end;
end;

initialization
  {$i saisiefant.lrs}


end.
