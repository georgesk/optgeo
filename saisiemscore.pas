{  This file is part of OptGeo, optical simulation software
    Copyright (C) 2004  Jean-Marie Biansan
        Contact: jeanmarie.biansan@free.fr
    Web site: http://jeanmarie.biansan.free.fr/logiciel.html
 From version 1.25, OptGeo is distribued under the terms of the
 GNU General Public License.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA}


    unit saisiemscore;

{$mode objfpc}{$H+}

interface

uses
  LCLIntf, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, Buttons, Spin,ColorBox, LResources,LCLType,UnitScaleFont,
  UChaines;

type

  { Tsaisiemiroirscore }

  Tsaisiemiroirscore = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    gridaxe: TColorBox;
    Colorgrid1: TColorBox;
    Image1: TImage;
    GroupBox1: TGroupBox;
    StaticText1: TLabel;
    StaticText2: TLabel;
    editx1: TEdit;
    edity1: TEdit;
    GroupBox2: TGroupBox;
    StaticText3: TLabel;
    StaticText4: TLabel;
    editx2: TEdit;
    edity2: TEdit;
    GroupBox3: TGroupBox;
    GroupBox4: TGroupBox;
    editepaisseur: TSpinEdit;
    GroupBox5: TGroupBox;
    cochehachures: TCheckBox;
    cocheaxe: TCheckBox;
    GroupBox6: TGroupBox;
    StaticText5: TLabel;
    GroupBox7: TGroupBox;
    editrayoncourbure: TEdit;
    GroupBox8: TGroupBox;
    Checkobtu: TCheckBox;
    boutonsup: TBitBtn;
    GroupBox9: TGroupBox;
    cochetrou: TCheckBox;
    editdiametretrou: TEdit;
    StaticText6: TLabel;
    StaticText7: TLabel;
    log1: TLabel;
    log2: TLabel;
    log3: TLabel;
    log4: TLabel;
    log5: TLabel;
    StaticText13: TLabel;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private  encreation:boolean;
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

var
  saisiemiroirscore: Tsaisiemiroirscore;
   mscore_hachures,mscore_axe,mscore_aigu,mscore_trou:boolean;
  mscore_epaisseur:integer;
  mscore_couleur,mscore_couleuraxe:tcolor;
  mscore_x1,mscore_x2,mscore_y1,mscore_y2,mscore_rayoncourbure,mscore_diametretrou:extended;
implementation

uses unit222;


procedure Tsaisiemiroirscore.BitBtn1Click(Sender: TObject);
begin
mscore_aigu:=not(checkobtu.checked);
mscore_hachures:=cochehachures.Checked;
mscore_axe:=cocheaxe.Checked;
mscore_epaisseur:=editepaisseur.Value;
mscore_couleur:=colorgrid1.selected;
mscore_couleuraxe:=gridaxe.selected;
mscore_trou:=cochetrou.checked;
if not(mscore_trou) then mscore_diametretrou:=0 else
try
mscore_diametretrou:=strtofloat(editdiametretrou.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce32),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;





try
mscore_x1:=strtofloat(editx1.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce13),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;
 if ((mscore_x1<xxmin) or (mscore_x1>xxmax)) then begin
 application.messagebox(pchar(rsValeurInacce13),
 pchar(rsattention),mb_ok);
 exit;
 end;

 try
mscore_x2:=strtofloat(editx2.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce14),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;
 if ((mscore_x2<xxmin) or (mscore_x2>xxmax)) then begin
 application.messagebox(pchar(rsValeurInacce14),
 pchar(rsattention),mb_ok);
 exit;
 end;

 try
mscore_y1:=strtofloat(edity1.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce15),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;
 if ((mscore_y1<yymin) or (mscore_y1>yymax)) then begin
 application.messagebox(pchar(rsValeurInacce15),
 pchar(rsattention),mb_ok);
 exit;
 end;

 try
mscore_y2:=strtofloat(edity2.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce16),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;
 if ((mscore_y2<yymin) or (mscore_y2>yymax)) then begin
 application.messagebox(pchar(rsValeurInacce16),
 pchar(rsattention),mb_ok);
 exit;
 end;


 try
mscore_rayoncourbure:=strtofloat(editrayoncourbure.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce33),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;
 if (mscore_rayoncourbure=0) then begin
 application.messagebox(pchar(rsLeRayonDeCou),
 pchar(rsattention),mb_ok);
 exit;
 end;
 mscore_rayoncourbure:=abs(mscore_rayoncourbure);
 if sqrt(sqr(mscore_x2-mscore_x1)+sqr(mscore_y2-mscore_y1))>
 2*mscore_rayoncourbure then begin
 application.messagebox(pchar(rsLaDistanceEn),
 pchar(rsattention),mb_ok);
 exit;
 end;
  ReTaillePMscore(ListeMscore,nombreMscore,nombreMscore+1);
 inc(nombremscore);
if not
(Listemscore[-1+nombremscore].create2(mscore_x1,mscore_y1,mscore_x2,mscore_y2,
mscore_rayoncourbure,mscore_epaisseur,
mscore_couleur,mscore_couleuraxe,mscore_hachures,mscore_axe,mscore_aigu,mscore_trou,mscore_diametretrou)) then begin
ReTaillePMscore(ListeMscore,nombreMscore,nombreMscore-1);
dec(nombremscore);
application.messagebox(pchar(rsTailleDuMiro8),
pchar(rsattention),mb_ok);
end else
self.modalresult:=mrok;
Rafraichit;


end;

procedure Tsaisiemiroirscore.FormCreate(Sender: TObject);
begin
   encreation:=true;
   BITBTN1.CAPTION
// msgctxt ';TSAISIEMIROIRSCORE.BITBTN1.CAPTION';
:=rsOK;


BITBTN2.CAPTION
// msgctxt ';TSAISIEMIROIRSCORE.BITBTN2.CAPTION';
:=rsAnnuler;


BOUTONSUP.CAPTION
// msgctxt ';TSAISIEMIROIRSCORE.BOUTONSUP.CAPTION';
:=rsSupprimerCeM;


CAPTION
:=rsAjoutDUnMiro12;


CHECKOBTU.CAPTION
// msgctxt ';TSAISIEMIROIRSCORE.CHECKOBTU.CAPTION';
:=rsObtu;


COCHEAXE.CAPTION
// msgctxt ';TSAISIEMIROIRSCORE.COCHEAXE.CAPTION';
:=rsAxeDeSymTrie;


COCHEHACHURES.CAPTION
// msgctxt ';TSAISIEMIROIRSCORE.COCHEHACHURES.CAPTION';
:=rsHachures;


COCHETROU.CAPTION
// msgctxt ';TSAISIEMIROIRSCORE.COCHETROU.CAPTION';
:=rsTrouer;


GROUPBOX1.CAPTION
// msgctxt ';TSAISIEMIROIRSCORE.GROUPBOX1.CAPTION';
:=rsPoint1;


GROUPBOX2.CAPTION
// msgctxt ';TSAISIEMIROIRSCORE.GROUPBOX2.CAPTION';
:=rsPoint2;


GROUPBOX3.CAPTION
// msgctxt ';TSAISIEMIROIRSCORE.GROUPBOX3.CAPTION';
:=rsCouleurDuMir;


GROUPBOX4.CAPTION
// msgctxt ';TSAISIEMIROIRSCORE.GROUPBOX4.CAPTION';
:=rsEpaisseurTra9;


GROUPBOX5.CAPTION
// msgctxt ';TSAISIEMIROIRSCORE.GROUPBOX5.CAPTION';
:=rsAspect;


GROUPBOX6.CAPTION
// msgctxt ';TSAISIEMIROIRSCORE.GROUPBOX6.CAPTION';
:=rsRayonDeCourb;


GROUPBOX7.CAPTION
// msgctxt ';TSAISIEMIROIRSCORE.GROUPBOX7.CAPTION';
:=rsCouleurDeLAx7;


GROUPBOX8.CAPTION
// msgctxt ';TSAISIEMIROIRSCORE.GROUPBOX8.CAPTION';
:=rsAngleDOuvert2;


GROUPBOX9.CAPTION
// msgctxt ';TSAISIEMIROIRSCORE.GROUPBOX9.CAPTION';
:=rsTrouerLeMiro;


LOG1.CAPTION
// msgctxt ';TSAISIEMIROIRSCORE.LOG1.CAPTION';
:=rsLog1;


LOG2.CAPTION
// msgctxt ';TSAISIEMIROIRSCORE.LOG2.CAPTION';
:=rsLog2;


LOG3.CAPTION
// msgctxt ';TSAISIEMIROIRSCORE.LOG3.CAPTION';
:=rsLog3;


LOG4.CAPTION
// msgctxt ';TSAISIEMIROIRSCORE.LOG4.CAPTION';
:=rsLog4;


LOG5.CAPTION
// msgctxt ';TSAISIEMIROIRSCORE.LOG5.CAPTION';
:=rsLog5;


STATICTEXT1.CAPTION
// msgctxt ';TSAISIEMIROIRSCORE.STATICTEXT1.CAPTION';
:=rsX;


STATICTEXT13.CAPTION
// msgctxt ';TSAISIEMIROIRSCORE.STATICTEXT13.CAPTION';
:=rsRad;


STATICTEXT2.CAPTION
// msgctxt ';TSAISIEMIROIRSCORE.STATICTEXT2.CAPTION';
:=rsY;


STATICTEXT3.CAPTION
// msgctxt ';TSAISIEMIROIRSCORE.STATICTEXT3.CAPTION';
:=rsX;


STATICTEXT4.CAPTION
// msgctxt ';TSAISIEMIROIRSCORE.STATICTEXT4.CAPTION';
:=rsY;


STATICTEXT5.CAPTION
// msgctxt ';TSAISIEMIROIRSCORE.STATICTEXT5.CAPTION';
:=rsR;


STATICTEXT6.CAPTION
// msgctxt ';TSAISIEMIROIRSCORE.STATICTEXT6.CAPTION';
:=rsDiamTreAngul;


STATICTEXT7.CAPTION
// msgctxt ';TSAISIEMIROIRSCORE.STATICTEXT7.CAPTION';
:=rsVuDepuisLeCe;


end;

procedure Tsaisiemiroirscore.FormShow(Sender: TObject);
begin
  //if encreation then begin scalefont(self); encreation:=false; end;
end;

initialization
  {$i saisiemscore.lrs}


end.
