unit UnitConfigPostScript;

{$mode objfpc}

interface

uses
  Classes, SysUtils, LazFileUtils, LResources, Forms, Controls, Graphics, Dialogs,
  StdCtrls, ExtCtrls, Spin, Buttons,UChaines,UnitScaleFont,MonPostscript;

type
  tgraphehorizontal=(tgauche,tdroite,tcentreh);
tgraphevertical=(thaut,tbas,tcentrev);
  { TFormConfigPostScript }

  TFormConfigPostScript = class(TForm)
  BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    CheckBoxrespect: TCheckBox;
    ComboBox1: TComboBox;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    GroupBox4: TGroupBox;
    GroupBox5: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    RadioGrouporientation: TRadioGroup;
    RadioGroupevertical: TRadioGroup;
    radiogroupehorizontal: TRadioGroup;
    SpinEdit_resx: TSpinEdit;
    SpinEdit_resy: TSpinEdit;
    spinpourcentagevertical: TSpinEdit;
    spinpourcentagehorizontal: TSpinEdit;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private   encreation:boolean;
    { private declarations }
  public
    { public declarations }
  end; 

var
  FormConfigPostScript: TFormConfigPostScript;

 graphehorizontal:tgraphehorizontal;
  graphevertical:tgraphevertical;
  taillegraphehorizontal,taillegraphevertical:integer;
implementation
 uses Ray1;
{ TFormConfigPostScript }

procedure TFormConfigPostScript.FormCreate(Sender: TObject);
begin
  encreation:=true;

 CHECKBOXRESPECT.CAPTION
 :=rsRespecterLes;


 GROUPBOX1.CAPTION
 :=rsPosition;




 GROUPBOX3.CAPTION
 :=rsTailleEnDeLa;


 GROUPBOX4.CAPTION
 :=rsRapportHaute;


 LABEL1.CAPTION
 :=rsNombre;


 LABEL2.CAPTION
 :=rsHorizontalem;


 LABEL3.CAPTION
 :=rsVerticalemen;

 groupbox5.Caption:=rsRSolutions;
   label1.Caption:=rsHorizontale2;
   label4.Caption:=rsVerticale2;

   combobox1.Items.Clear;
   combobox1.Items.Add(rsA4);
   combobox1.Items.Add(rsA3);
   combobox1.Items.Add(rsA22);
   combobox1.Items.Add(rsA1);
   combobox1.Items.Add(rsA0);
   combobox1.Items.Add(rsA5);
   combobox1.Items.Add(rsLetter);
   combobox1.Items.Add(rsLegal);
   combobox1.Items.Add(rsLedger);

 RADIOGROUPORIENTATION.CAPTION
 :=rsOrientation;


   FormConfigPostScript.caption :=rsPropriTSDuFi;

    radiogroupehorizontal.Caption := rsHorizontale;

   // radiogroupehorizontal.Items.clear;
    radiogroupehorizontal.Items[0]:=rsDroite;
      radiogroupehorizontal.Items[1]:=rsGauche;
      radiogroupehorizontal.Items[2]:=rsCentr;

     RadioGroupevertical.Caption := rsVerticale;
     //RadioGroupevertical.Items.clear;
     RadioGroupevertical.Items[0]:=rsHaut;
      RadioGroupevertical.Items[1]:=rsBas;
      RadioGroupevertical.Items[2]:=rsCentr;


      Label1.Caption := rsNombre;
      GroupBox3.Caption := rsTailleEnDeLa;
      Label2.Caption := rsHorizontalem;
      Label3.Caption := rsVerticalemen;
      RadioGrouporientation.Caption := rsOrientation;
      //RadioGrouporientation.Items.clear;
      RadioGrouporientation.Items[0]:=rsPortrait;
       RadioGrouporientation.Items[1]:= rsPaysage;
        BitBtn2.Caption := rsAnnuler;
        BitBtn1.Caption := rsOK;
end;

procedure TFormConfigPostScript.BitBtn1Click(Sender: TObject);
begin
  if radiogroupehorizontal.itemindex=1 then graphehorizontal:=tgauche;
if  radiogroupehorizontal.itemindex=0 then graphehorizontal:=tdroite;
if  radiogroupehorizontal.itemindex=2 then graphehorizontal:=tcentreh;
if radiogroupevertical.itemindex=0 then graphevertical:=thaut;
if radiogroupevertical.itemindex=1 then graphevertical:=tbas;
if radiogroupevertical.itemindex=2 then graphevertical:=tcentrev;
taillegraphehorizontal:=spinpourcentagehorizontal.value;
taillegraphevertical:=spinpourcentagevertical.value;

if radiogrouporientation.ItemIndex=0 then
or_ps:=portrait else
or_ps:=paysage;
respect_ps:=checkboxrespect.Checked;

 if combobox1.Items[combobox1.ItemIndex]=rsA4 then p_ps:=A4;
  if combobox1.Items[combobox1.ItemIndex]=rsA3 then p_ps:=A3;
   if combobox1.Items[combobox1.ItemIndex]=rsA22 then p_ps:=A2;
    if combobox1.Items[combobox1.ItemIndex]=rsA1 then p_ps:=A1;
     if combobox1.Items[combobox1.ItemIndex]=rsA0 then p_ps:=A0;
      if combobox1.Items[combobox1.ItemIndex]=rsA5 then p_ps:=A5;
       if combobox1.Items[combobox1.ItemIndex]=rsLegal then p_ps:=Legal;
        if combobox1.Items[combobox1.ItemIndex]=rsLetter then p_ps:=Letter;
         if combobox1.Items[combobox1.ItemIndex]=rsLedger then p_ps:=Ledger;
   res_ps_x:=spinedit_resx.Value;
   res_ps_y:=spinedit_resy.Value;
end;

initialization
  {$I unitconfigpostscript.lrs}
 grapheHorizontal:=tcentreh;
 graphevertical:=tcentrev;
 taillegraphehorizontal:=90;
 taillegraphevertical:=90;
  or_ps:=paysage;
  p_ps:=A4;
 res_ps_x:=72;
  res_ps_y:=72;
end.

