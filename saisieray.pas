{  This file is part of OptGeo, optical simulation software
    Copyright (C) 2004  Jean-Marie Biansan
        Contact: jeanmarie.biansan@free.fr
    Web site: http://jeanmarie.biansan.free.fr/logiciel.html
 From version 1.25, OptGeo is distribued under the terms of the
 GNU General Public License.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA}


    unit saisieray;

{$mode objfpc}{$H+}

interface

uses
  LCLIntf, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls,unit222, Buttons, Spin, LResources,LCLType,UnitScaleFont,
  UChaines;

type

  { Tsaisierayon }

  Tsaisierayon = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    GroupBox6: TGroupBox;
    Image1: TImage;
    GroupBox1: TGroupBox;
    Spinmaxsegment: TSpinEdit;
    StaticText1: TLabel;
    StaticText11: TLabel;
    StaticText2: TLabel;
    editx1: TEdit;
    edity1: TEdit;
    GroupBox2: TGroupBox;
    StaticText3: TLabel;
    StaticText4: TLabel;
    editx2: TEdit;
    edity2: TEdit;
    radiocouleur: TRadioGroup;
    GroupBox3: TGroupBox;
    editepaisseur: TSpinEdit;
    GroupBox4: TGroupBox;
    boutonsup: TBitBtn;
    GroupBox5: TGroupBox;
    Spinenfantmax: TSpinEdit;
    StaticText5: TLabel;
    StaticText6: TLabel;
    StaticText7: TLabel;
    StaticText8: TLabel;
    editvav: TEdit;
    editvaa: TEdit;
    StaticText9: TLabel;
    StaticText10: TLabel;
    log1: TLabel;
    log2: TLabel;
    log3: TLabel;
    log4: TLabel;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private  encreation:boolean;
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

var
  saisierayon: Tsaisierayon;
  ra_couleur:tcolor;
  ra_epaisseur,ra_enfantmax,ra_maxsegment:integer;
  ra_x1,ra_x2,ra_y1,ra_y2:extended;
  ra_virtuelavant,ra_virtuelarriere:boolean;
implementation


procedure Tsaisierayon.BitBtn1Click(Sender: TObject);
begin
ra_epaisseur:=editepaisseur.value;
case radiocouleur.ItemIndex of
0: ra_couleur:=clred;
1: ra_couleur:=clgreen;
2: ra_couleur:=clblue;
3: ra_couleur:=clblack;
end;
 ra_enfantmax:=spinenfantmax.value;
 ra_maxsegment:=spinmaxsegment.value;
try
ra_x1:=strtofloat(editx1.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce13),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;
 if ((ra_x1<xxmin) or (ra_x1>xxmax)) then begin
 application.messagebox(pchar(rsValeurInacce13),
 pchar(rsattention),mb_ok);
 exit;
 end;

 try
ra_x2:=strtofloat(editx2.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce14),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;
 if ((ra_x2<xxmin) or (ra_x2>xxmax)) then begin
 application.messagebox(pchar(rsValeurInacce14),
 pchar(rsattention),mb_ok);
 exit;
 end;

 try
ra_y1:=strtofloat(edity1.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce15),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;
 if ((ra_y1<yymin) or (ra_y1>yymax)) then begin
 application.messagebox(pchar(rsValeurInacce15),
 pchar(rsattention),mb_ok);
 exit;
 end;

 try
ra_y2:=strtofloat(edity2.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce16),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;
 if ((ra_y2<yymin) or (ra_y2>yymax)) then begin
 application.messagebox(pchar(rsValeurInacce16),
 pchar(rsattention),mb_ok);
 exit;
 end;

   ReTaillePrayon(Listerayon,nombrerayon,nombrerayon+1);
 inc(nombrerayon);
if not
(ListeRayon[-1+nombrerayon].create(ra_x1,ra_y1,ra_x2-ra_x1,ra_y2-ra_y1,
ra_couleur,ra_epaisseur,editvav.Text,editvaa.Text,ra_enfantmax,ra_maxsegment)) then begin
ReTaillePrayon(Listerayon,nombrerayon,nombrerayon-1);
dec(nombrerayon);
application.messagebox(pchar(rsLesDeuxPoint),
pchar(rsattention),mb_ok);
end else
self.modalresult:=mrok;

Rafraichit;
end;

procedure Tsaisierayon.FormCreate(Sender: TObject);
begin
   encreation:=true;
   BITBTN1.CAPTION
// msgctxt ';TSAISIERAYON.BITBTN1.CAPTION';
:=rsOK;


BITBTN2.CAPTION
// msgctxt ';TSAISIERAYON.BITBTN2.CAPTION';
:=rsAnnuler;


BOUTONSUP.CAPTION
:=rsSupprimerCeR;


CAPTION
:=rsSaisieDUnRay;


GROUPBOX1.CAPTION
:=rsOriginePoint;


GROUPBOX2.CAPTION
:=rsExtrMitPoint;


GROUPBOX3.CAPTION
// msgctxt ';TSAISIERAYON.GROUPBOX3.CAPTION';
:=rsEpaisseurLCr2;


GROUPBOX4.CAPTION
:=rsTracerEnPoin2;


GROUPBOX5.CAPTION
:=rsRayonsEnfant;


GROUPBOX6.CAPTION
:=rsNombreMaxDeS2;


LOG1.CAPTION
// msgctxt ';TSAISIERAYON.LOG1.CAPTION';
:=rsLog1;


LOG2.CAPTION
// msgctxt ';TSAISIERAYON.LOG2.CAPTION';
:=rsLog2;


LOG3.CAPTION
// msgctxt ';TSAISIERAYON.LOG3.CAPTION';
:=rsLog3;


LOG4.CAPTION
// msgctxt ';TSAISIERAYON.LOG4.CAPTION';
:=rsLog4;


RADIOCOULEUR.CAPTION
:=rsCouleurDuRay;


STATICTEXT1.CAPTION
// msgctxt ';TSAISIERAYON.STATICTEXT1.CAPTION';
:=rsX;


STATICTEXT10.CAPTION
// msgctxt ';TSAISIERAYON.STATICTEXT10.CAPTION';
:=rsVersLArriRe2;


STATICTEXT11.CAPTION
// msgctxt ';TSAISIERAYON.STATICTEXT11.CAPTION';
:=rsNombreMaxima35;


STATICTEXT2.CAPTION
// msgctxt ';TSAISIERAYON.STATICTEXT2.CAPTION';
:=rsY;


STATICTEXT3.CAPTION
// msgctxt ';TSAISIERAYON.STATICTEXT3.CAPTION';
:=rsX;


STATICTEXT4.CAPTION
// msgctxt ';TSAISIERAYON.STATICTEXT4.CAPTION';
:=rsY;


STATICTEXT5.CAPTION
// msgctxt ';TSAISIERAYON.STATICTEXT5.CAPTION';
:=rsNombreMaxima35;


STATICTEXT6.CAPTION
// msgctxt ';TSAISIERAYON.STATICTEXT6.CAPTION';
:=rsListeDesSegm;


STATICTEXT7.CAPTION
// msgctxt ';TSAISIERAYON.STATICTEXT7.CAPTION';
:=rsEntreParenth;


STATICTEXT8.CAPTION
// msgctxt ';TSAISIERAYON.STATICTEXT8.CAPTION';
:=rsEtSansEspace;


STATICTEXT9.CAPTION
// msgctxt ';TSAISIERAYON.STATICTEXT9.CAPTION';
:=rsVersLAvant2;

radiocouleur.Items[0]:=rsRouge;
radiocouleur.Items[1]:=rsVert;
radiocouleur.Items[2]:=rsBleu;
radiocouleur.Items[3]:=rsLumiReBlanch;

end;

procedure Tsaisierayon.FormShow(Sender: TObject);
begin
 // if encreation then begin scalefont(self); encreation:=false; end;
end;

initialization
  {$i saisieray.lrs}


end.
