unit MonPostScript;




{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, LResources, Forms, Controls, Graphics, Dialogs, ExtCtrls,LCLProc,Math,LCLType
  {$ifdef windows},Windows{$endif},MonPostScriptCanvas,Printers,Definitions;



type
page_ps=(A4,A3,A2,A1,A0,A5,Letter,Legal,Ledger);
orientation_ps=(portrait,paysage);
ppscanvas=^TMonPostscriptCanvas;

  TMonPostScript =Object
   canvas_pointe:ppscanvas;
res_x,res_y:integer;
type_page_ps:page_ps;   type_orientation_ps:orientation_ps;
   Xmin,Xmax,Ymin1,Ymax1,Ymin2,Ymax2,GraduationX, GraduationY1,GraduationY2:extended;

  Largeur,Hauteur,BordureBasse,BordureHaute,
  BordureGauche,BordureDroite,EpaisseurGrille,PuissanceDeDixX,
  PuissancedeDixY1,PuissancedeDixY2,epaisseurcadre,epaisseurgraduation:integer;
  longueurgraduationX,longueurgraduationY1,longueurgraduationY2:extended;
  PasGrillex,PasGrilley1,PasGrilley2:extended;
  couleurfondsim,couleurcadre,couleurgraduation,
  couleurgrille1,couleurgrille2:tcolor;
  cadre,gradue,grille1,grille2,fond,borduresverticalessymetriques,echelle_g,echelle_d:boolean;
   titre,labelx,labely1,labely2,unitex,unitey1,unitey2:string;
  fontegraduation:tfont;
  fontetitre:tfont;
    procedure background(couleur:tcolor);


  function LimitesEtAxes(pointeur_sur_canvas:ppscanvas;
  _res_x,_res_y:integer;_type_page_ps:page_ps;
  _type_orientation_ps:orientation_ps;
  _xmin,_xmax,_ymin1,_ymax1,_ymin2,_ymax2:extended;
  _police:tfont;
 _titre:string;
  _fontetitre:tfont;
  _Fond:boolean;
 _CouleurFondsim:tcolor;
 _Cadre:boolean; _EpaisseurCadre:integer;
 _CouleurCadre:tcolor;
 _Gradue:boolean;
 _EpaisseurGraduation,_LongueurGraduation:integer;
 _couleurgraduation:tcolor;
 _fontegraduation:tfont;
 _Grille1,_Grille2:boolean; _EpaisseurGrille:integer;
 _CouleurGrille1,_CouleurGrille2:tcolor;
 _labelx,_labely1,_labely2,_unitex,_unitey1,_unitey2:string;
  _borduresverticalessymetriques,
 _echelle_g,_echelle_d:boolean;
   b_g,b_d,b_b,b_h:integer):boolean;



procedure  dessinergrille(pas_grillex,pas_grilley:extended);

procedure arronditalagrille(var x1,y1:extended;pas_grillex,pas_grilley:extended);

 function invconvert(var xi,yi:integer; x,y:extended;gauche:boolean):boolean;

 function Convert(xi,yi:integer; var x,y:extended;gauche:boolean):boolean;

 function CroixX(x,y:extended;
 demi_diagonale,epaisseur:integer; couleur:tcolor; penmod:tpenmode;gauche:boolean):boolean;

 function CroixP(x,y:extended;
 demi_longueur,epaisseur:integer; couleur:tcolor; penmod:tpenmode;gauche:boolean):boolean;

 function Carreau(x,y:extended;
 demi_diagonale,epaisseur:integer; couleur:tcolor; penmod:tpenmode;gauche:boolean):boolean;

            function Trait(x1,y1,x2,y2:extended; epaisseur:integer;
 couleur:tcolor; sty:tpenstyle; _penmod:tpenmode;gauche:boolean):boolean;

 function Point(x,y:extended; rayon:integer; couleur:tcolor; _penmod:tpenmode;gauche:boolean):boolean;

 function cercle(x,y:extended; rayon,epaisseur:integer; couleur:tcolor; _penmod:tpenmode;gauche:boolean):boolean;

  function disque(x,y:extended; rayon:extended; couleurbord,couleurf:tcolor;_transparent:boolean;gauche:boolean):
 boolean;

  function disque2(x,y:extended; rayon:integer; couleurbord,couleurf:tcolor;gauche:boolean):
 boolean;

  function arcdecercle(x,y:extended; rayonreel:extended;
     epaisseur:integer; theta1,theta2:extended;
     couleur:tcolor; _penmod:tpenmode;_style:tpenstyle;gauche:boolean):boolean;
 function arcdecercleoriente(x,y:extended; rayonreel:extended;
     epaisseur:integer; theta1,theta2:extended;
     couleur:tcolor; _penmod:tpenmode;_style:tpenstyle;gauche,trigod:boolean): boolean;

     function tracepolygone(nombrepoint:integer;listex,listey:array of extended;
	 couleurbord,couleurf:tcolor;
        _transparent:boolean;_mode:tpenmode; gauche:boolean):boolean;

   procedure traceconique(theta0,theta1,theta2,fx,fy,excentricite,parametre:extended;
    couleur:tcolor;gauche:boolean);

     procedure arronditalagrille(var x1,y1:extended;gauche:boolean);

      procedure ecrire(x,y:extended; s:string;gauche:boolean; police:tfont);
      procedure ecrireI(x,y:integer; s:string; police:tfont);

      procedure  tracelec(c1x,c1y,r1,c2x,c2y,r2:extended;couleurfond:tcolor);

       procedure tracepolycercle(MesSommets: TableauPointPolyhedre; NumPts: Integer;
 MesCentres: TableauPointPolyhedre;
 rectiligne,rentrant:tableaurectiligne; theta1,theta2:sommetsreels; couleur_f,couleur_b:tcolor; listetrigodirect:tableaurectiligne);



      procedure graduation(xi,xa:extended;
var xci,xca:extended; var graduation:extended;
var PuissanceDeDix:integer; var nombregraduation:integer);



  end;

   procedure echange(var a,b:extended);

       procedure echange_entiers(var a,b:integer);

        function partieentiere(x:extended):extended;

         function dix_pp(p:longint):extended;

 function DonneAnglePolaireDansPiPi(x,y:extended):extended;
   function DonneAnglePolaireDans02Pi(x,y:extended):extended; 

  procedure Donne_Hauteur_Largeur(type_page_ps:page_ps;type_orientation_ps:orientation_ps;
 res_x,res_y:integer; var hauteur,largeur:integer);


implementation

function DonneAnglePolaireDansPiPi(x,y:extended):extended;
 var toto:extended;
 begin
 if ((x=0) and (y=0)) then begin
  DonneAnglePolaireDansPiPi:=0;
  exit;
  end;

  if (x=0) then
  if (y>0) then begin
  DonneAnglePolaireDansPiPi:=Pi/2;
  exit;
  end else begin
  DonneAnglePolaireDansPiPi:=-Pi/2;
  exit;
  end;

  if (y=0) then
  if (x>0) then begin
  DonneAnglePolaireDansPiPi:=0;
  exit;
  end else begin
  DonneAnglePolaireDansPiPi:=Pi;
  exit;
  end;

  try
  toto:=arctan(y/x);
  except
  if (((x>0) and (y>0)) or ((x<0) and (y<0))) then
  toto:=pi/2 else toto:=-pi/2;
  end;

  if (x<0) then toto:=toto+Pi;
  if toto>Pi then    toto:=toto-2*Pi;
  if toto<-Pi then toto:=toto+2*pi;
   DonneAnglePolaireDansPiPi:=toto;
  end;


   function DonneAnglePolaireDans02Pi(x,y:extended):extended;
     var toto:extended;
     begin
    toto:=DonneAnglePolaireDansPiPi(x,y);
     if toto<0 then toto:=toto+2*Pi;
     DonneAnglePolaireDans02Pi:=toto;
     end;      


 procedure  TMonPostScript.tracelec(c1x,c1y,r1,c2x,c2y,r2:extended;couleurfond:tcolor);
 var x1,y1,rx,ry,res:integer;
 rgn1,rgn2,rgn:hrgn;
 begin
 invconvert(x1,y1,c1x,c1y,true);
  rx:= trunc(abs(r1)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite));
 ry:=trunc(abs(r1)/(ymax1-ymin1)*(hauteur-BordureHaute-BordureBasse));
 {$ifdef windows}
 rgn1:=windows.CreateEllipticRgn(x1-rx,y1-ry,x1+rx,y1+ry);
 if rgn1=NULLREGION then begin
   windows.deleteobject(rgn1);
   exit;
   end;
 invconvert(x1,y1,c2x,c2y,true);
  rx:= trunc((abs(r2))/(xmax-xmin)*(largeur-BordureGauche-BordureDroite));
 ry:=trunc((abs(r2))/(ymax1-ymin1)*(hauteur-BordureHaute-BordureBasse));

 rgn2:=windows.CreateEllipticRgn(x1-rx,y1-ry,x1+rx,y1+ry);
 if rgn2=NULLREGION then begin
   windows.deleteobject(rgn2);
   exit;
   end;
 rgn:=windows.CreateEllipticRgn(x1-rx,y1-ry,x1+rx,y1+ry);
    if r1*r2<0 then  res:=windows.combinergn(rgn,rgn1,rgn2,RGN_AND) else
    if  abs(r1)>abs(r2) then res:=windows.combinergn(rgn,rgn2,rgn1,RGN_DIFF) else
    res:=windows.combinergn(rgn,rgn1,rgn2,RGN_DIFF);
     if res=ERROR then begin
   windows.deleteobject(res);
   exit;
   end;
  canvas_pointe^.brush.color:=couleurfond;
 canvas_pointe^.brush.style:=bsSolid;
 windows.FillRgn(canvas_pointe^.handle,rgn,canvas_pointe^.brush.handle);

   windows.DeleteObject(rgn); windows.deleteobject(rgn1); windows.deleteobject(rgn2);
    canvas_pointe^.brush.color:=clwhite;
    {$endif}
    end;


 procedure TMonPostScript.arronditalagrille(var x1,y1:extended;pas_grillex,pas_grilley:extended);
  var newx,newy,divx,divy,fracx,fracy:extended;
  begin
  divx:=partieentiere((x1-xmin)/pas_grillex);
  divy:=partieentiere((y1-ymin1)/pas_grilley);
  fracx:=x1-xmin-divx*pas_grillex;
  fracy:=y1-ymin1-divy*pas_grilley;
  if fracx>0.5*pas_grillex then newx:=xmin+(divx+1)*pas_grillex else newx:=xmin+divx*pas_grillex;
  if fracy>0.5*pas_grilley then newy:=ymin1+(divy+1)*pas_grilley else newy:=ymin1+divy*pas_grilley;
  x1:=newx; y1:=newy;
                        end;


procedure  TMonPostScript.dessinergrille(pas_grillex,pas_grilley:extended);
 var i,j,x1,y1:integer; x,y:extended;
   old_pencolor:tcolor;
old_penmode:tpenmode;
 old_penstyle:tpenstyle;
 old_penwidth:integer;
 old_brushcolor:tcolor;
 old_brushstyle:tbrushstyle;
  begin

  old_pencolor:=canvas_pointe^.Pen.Color;
old_penmode:=canvas_pointe^.Pen.Mode;
 old_penstyle:=canvas_pointe^.Pen.Style;
 old_penwidth:=canvas_pointe^.Pen.Width;
 old_brushcolor:=canvas_pointe^.Brush.Color;
 old_brushstyle:=canvas_pointe^.Brush.Style;





      canvas_pointe^.pen.color:=couleurgrille1;
    canvas_pointe^.pen.style:=pssolid;
    canvas_pointe^.pen.width:=1;
                    canvas_pointe^.pen.mode:=pmcopy;
         canvas_pointe^.brush.style:=bssolid;
    canvas_pointe^.brush.color:=couleurgrille1;

  for i:=1 to trunc((xmax-xmin)/pas_grillex) do
  for j:=1 to trunc((ymax1-ymin1)/pas_grilley) do begin
  x:=xmin+i*pas_grillex; y:=ymin1+j*pas_grilley;
  invconvert(x1,y1,x,y,true);
    // canvas_pointe^.pixels[x1,y1]:=couleurgrille1;
     // canvas_pointe^.Ellipse(x1,y1,x1,y1);
            canvas_pointe^.DoOnePoint(x1,y1);
        end;
     canvas_pointe^.Pen.Color:=old_pencolor;
canvas_pointe^.Pen.Mode:=old_penmode;
 canvas_pointe^.Pen.Style:=old_penstyle;
 canvas_pointe^.Pen.Width:=old_penwidth;
 canvas_pointe^.Brush.Color:=old_brushcolor;
 canvas_pointe^.Brush.Style:=old_brushstyle;
      end;






procedure TMonPostScript.graduation(xi,xa:extended;
var xci,xca:extended; var graduation:extended;
var PuissanceDeDix:integer; var nombregraduation:integer);

var n,gm,g,d,logd,tht:extended;
p,i,xpa,xpi:longint;
res:array[1..12] of extended;
nb:longint;

begin
if xa<xi then echange(xi,xa);
d:=xa-xi;
 logd:=ln(d)/ln(10);
if (logd>=0) then p :=longint(trunc(logd)) else
p:=longint(trunc(logd)-1);

  PuissanceDeDix:=p;
if xa>=0 then begin
gm:=trunc(xa/dix_pp(p-1))*dix_pp(p-1);
if (gm>=xa) then xpa:=trunc(xa/dix_pp(p-1)) else
xpa:=trunc(xa/dix_pp(p-1))+1;
   end;
if xa<0 then begin
gm:=trunc(xa/dix_pp(p-1))*dix_pp(p-1);
if (gm>=xa) then xpa:=trunc(xa/dix_pp(p-1)) else
xpa:=trunc(xa/dix_pp(p-1));
       end;

if xi>=0 then begin
gm:=trunc(xi/dix_pp(p-1))*dix_pp(p-1);
if (gm<=xi) then xpi:=trunc(xi/dix_pp(p-1)) else
xpi:=trunc(xi/dix_pp(p-1))-1;
           end;
if xi<0 then begin
gm:=trunc(xi/dix_pp(p-1))*dix_pp(p-1);
if (gm<=xi) then xpi:=trunc(xi/dix_pp(p-1)) else
xpi:=trunc(xi/dix_pp(p-1))-1;
               end;


case (xpa-xpi ) of
1: g:=0.1;
2: g:=0.2;
3,4,5: g:=0.5;
6,7,8,9,10: g:=1;
 else
 g:=trunc((xpa-xpi-1)/10+0.001)+1;
end;

        graduation:=g*dix_pp(p-1);


{xca:=xpa*dix_pp(p-1);
nombregraduation:=trunc((xpa-xpi)/g);
xci:=xca-graduation*nombregraduation;
if xci>xi then begin
xci:=xci-graduation;
inc(nombregraduation);
 end;  }
 xci:=xpi*dix_pp(p-1);
nombregraduation:=trunc((xpa-xpi)/g);
xca:=xci+graduation*nombregraduation;
if xca<xa then begin
xca:=xca+graduation;
inc(nombregraduation);
 end;
{for i:=1 to nb do writeln(res[i]);}
end;




 function dix_pp(p:longint):extended;
var i:longint;
inter:extended;
begin

if (p<-1) then begin
inter:=1/10;
for i:=1 to abs(p)-1 do
inter:=inter/10;
result:=inter;
end else


if p=0 then begin
result:=1;
end else

if p=1 then begin
 result:=10;
end else

if (p>1) then begin
inter:=10;
for i:=1 to p-1 do
inter:=inter*10;
result:=inter;
end else

if (p=-1) then begin
result:=1/10;
end;



       end;


function partieentiere(x:extended):extended;
begin
if x>=0 then  begin
partieentiere:=int(x);
exit;
end;
if frac(x)=0 then begin
partieentiere:=x;
exit;
end;
{cas ou x est <0 et non entier}
partieentiere:=int(x)-1;
end;


  procedure TMonPostScript.tracepolycercle(MesSommets: TableauPointPolyhedre; NumPts: Integer;
 MesCentres: TableauPointPolyhedre;
 rectiligne,rentrant:tableaurectiligne; theta1,theta2:sommetsreels; couleur_f,couleur_b:tcolor; listetrigodirect:tableaurectiligne);
   var i:integer;
            ymax,ymin,pasgrilley:extended;
              old_pencolor:tcolor;
old_penmode:tpenmode;
 old_penstyle:tpenstyle;
 old_penwidth:integer;
 old_brushcolor:tcolor;
 old_brushstyle:tbrushstyle;
  MesSOmmetsPoints,MesCentresPoints: arraypoints;
   recti,rentr,trig:arrayboolean;theta1b,theta2b:arrayextended ;
 begin

  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1;

 old_pencolor:=canvas_pointe^.Pen.Color;
old_penmode:=canvas_pointe^.Pen.Mode;
 old_penstyle:=canvas_pointe^.Pen.Style;
 old_penwidth:=canvas_pointe^.Pen.Width;
 old_brushcolor:=canvas_pointe^.Brush.Color;
 old_brushstyle:=canvas_pointe^.Brush.Style;

  setlength(MesSOmmetsPoints,numpts+1);
  setlength(MesCentresPoints,numpts+1);
  setlength(recti,numpts+1);
   setlength(rentr,numpts+1);
   setlength(theta1b,numpts+1);
   setlength(theta2b,numpts+1);
    setlength(trig,numpts+1);
  for i:=1 to  NumPts+1 do begin
   invconvert(MesSOmmetsPoints[i-1].x,MesSOmmetsPoints[i-1].y,
   MesSommets[i].ax,MesSommets[i].ay,true);
   invconvert(MesCentresPoints[i-1].x,MesCentresPoints[i-1].y,
   MesCentres[i].ax,MesCentres[i].ay,true);
   recti[i-1]:=rectiligne[i];
   rentr[i-1]:=rentrant[i];
   theta1b[i-1]:=theta1[i];
   theta2b[i-1]:=theta2[i];
    trig[i-1]:=listetrigodirect[i];
  end;

  canvas_pointe^.Pen.Color:=couleur_f;
canvas_pointe^.Pen.Mode:=pmcopy;
 canvas_pointe^.Pen.Style:=pssolid;
 canvas_pointe^.Pen.Width:=1;
 canvas_pointe^.Brush.Color:=couleur_f;
 canvas_pointe^.Brush.Style:=bssolid;


  canvas_pointe^.PolyCercle(MesSOmmetsPoints,NumPts,MesCentresPoints,
 recti,rentr,theta1b,theta2b,trig);

 canvas_pointe^.Pen.Color:=old_pencolor;
canvas_pointe^.Pen.Mode:=old_penmode;
 canvas_pointe^.Pen.Style:=old_penstyle;
 canvas_pointe^.Pen.Width:=old_penwidth;
 canvas_pointe^.Brush.Color:=old_brushcolor;
 canvas_pointe^.Brush.Style:=old_brushstyle;

     end;

function TMonPostScript.disque2(x,y:extended; rayon:integer; couleurbord,couleurf:tcolor;gauche:boolean):
 boolean;

            var x1,y1,r1x,r1y,i:integer;
            ymax,ymin,pasgrilley:extended;
              old_pencolor:tcolor;
old_penmode:tpenmode;
 old_penstyle:tpenstyle;
 old_penwidth:integer;
 old_brushcolor:tcolor;
 old_brushstyle:tbrushstyle;




  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;

 if ((x<xmin) or (x>xmax)
 or (y>ymax) or (y<ymin))
  then begin
 disque2:=false;
 exit;
 end;

 invconvert(x1,y1,x,y,gauche);

     if rayon=0 then exit;

 old_pencolor:=canvas_pointe^.Pen.Color;
old_penmode:=canvas_pointe^.Pen.Mode;
 old_penstyle:=canvas_pointe^.Pen.Style;
 old_penwidth:=canvas_pointe^.Pen.Width;
 old_brushcolor:=canvas_pointe^.Brush.Color;
 old_brushstyle:=canvas_pointe^.Brush.Style;





      canvas_pointe^.pen.color:=couleurbord;
    canvas_pointe^.pen.style:=pssolid;
    canvas_pointe^.pen.width:=1;

    canvas_pointe^.pen.mode:=pmcopy;
     disque2:=true;

    canvas_pointe^.brush.style:=bssolid;
    canvas_pointe^.brush.color:=couleurf;


canvas_pointe^.ellipse(x1-rayon,y1-rayon,x1+rayon,
y1+rayon);


   canvas_pointe^.Pen.Color:=old_pencolor;
canvas_pointe^.Pen.Mode:=old_penmode;
 canvas_pointe^.Pen.Style:=old_penstyle;
 canvas_pointe^.Pen.Width:=old_penwidth;
 canvas_pointe^.Brush.Color:=old_brushcolor;
 canvas_pointe^.Brush.Style:=old_brushstyle;





     end;




 function TMonPostScript.Carreau(x,y:extended;
 demi_diagonale,epaisseur:integer; couleur:tcolor; penmod:tpenmode;gauche:boolean):boolean;
 var x1,y1:integer;
ymax,ymin,pasgrilley:extended;
   old_pencolor:tcolor;
old_penmode:tpenmode;
 old_penstyle:tpenstyle;
 old_penwidth:integer;
 old_brushcolor:tcolor;
 old_brushstyle:tbrushstyle;



  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;
  invconvert(x1,y1,x,y,gauche);

 if ((x<xmin) or (x>xmax)
 or (y>ymax) or (y<ymin))
  then begin
 carreau:=false;
 exit;
 end;




     old_pencolor:=canvas_pointe^.Pen.Color;
old_penmode:=canvas_pointe^.Pen.Mode;
 old_penstyle:=canvas_pointe^.Pen.Style;
 old_penwidth:=canvas_pointe^.Pen.Width;
 old_brushcolor:=canvas_pointe^.Brush.Color;
 old_brushstyle:=canvas_pointe^.Brush.Style;



canvas_pointe^.pen.width:=epaisseur;
 canvas_pointe^.pen.mode:=penmod;
 canvas_pointe^.pen.style:=pssolid;
 canvas_pointe^.pen.color:=couleur;
  canvas_pointe^.DoLineFromTo(x1,y1+demi_diagonale,x1+demi_diagonale,y1);
  canvas_pointe^.DoLineFromTo(x1+demi_diagonale,y1,x1,y1-demi_diagonale);
   canvas_pointe^.DoLineFromTo(x1,y1-demi_diagonale,x1-demi_diagonale,y1);
    canvas_pointe^.DoLineFromTo(x1-demi_diagonale,y1,x1,y1+demi_diagonale);


   carreau:=true;

       canvas_pointe^.Pen.Color:=old_pencolor;
canvas_pointe^.Pen.Mode:=old_penmode;
 canvas_pointe^.Pen.Style:=old_penstyle;
 canvas_pointe^.Pen.Width:=old_penwidth;
 canvas_pointe^.Brush.Color:=old_brushcolor;
 canvas_pointe^.Brush.Style:=old_brushstyle;




   end;



    procedure echange_entiers(var a,b:integer);
     var c:integer;
    begin
    c:=a; a:=b; b:=c; end;


    procedure echange(var a,b:extended);
    var c:extended;
    begin
    c:=a; a:=b; b:=c; end;






 procedure TMonPostScript.traceconique(theta0,theta1,theta2,fx,fy,excentricite,parametre:extended;
   couleur:tcolor;gauche:boolean);
   var pas,theta,r,ract,thetaact,nx,ny,ttai:extended;
   nombrepas,i:integer;
   ymax,ymin,pasgrilley:extended;


  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;
   nombrepas:=1000;
   pas:=(theta2-theta1)/nombrepas;
   thetaact:=theta1;
   ract:=parametre/(1+excentricite*cos(thetaact-theta0));
   for i:=1 to nombrepas do begin
   theta:=theta1+i*pas;
   r:=parametre/(1+excentricite*cos(theta-theta0));
   if ((r>0) and (ract>0)) then trait(fx+ract*cos(thetaact),fy+ract*sin(thetaact),fx+r*cos(theta),fy+r*sin(theta),
   1,couleur,pssolid,pmcopy,gauche);
   thetaact:=theta;
   ract:=r;
   end;
    end;


function TMonPostScript.tracepolygone(nombrepoint:integer;listex,listey:array of extended;
  couleurbord,couleurf:tcolor;_transparent:boolean;_mode:tpenmode;gauche:boolean):boolean;
  var i,x1,y1:longint;  titi:arraypoints;
  ymax,ymin,pasgrilley:extended;
    old_pencolor:tcolor;
old_penmode:tpenmode;
 old_penstyle:tpenstyle;
 old_penwidth:integer;
 old_brushcolor:tcolor;
 old_brushstyle:tbrushstyle;




  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;
   setlength(titi,nombrepoint);
 for i:=1 to nombrepoint do begin
     invconvert(x1,y1,listex[i-1],listey[i-1],gauche);
   titi[i-1].x:=x1;
 titi[i-1].y:=y1;
 end;

 old_pencolor:=canvas_pointe^.Pen.Color;
old_penmode:=canvas_pointe^.Pen.Mode;
 old_penstyle:=canvas_pointe^.Pen.Style;
 old_penwidth:=canvas_pointe^.Pen.Width;
 old_brushcolor:=canvas_pointe^.Brush.Color;
 old_brushstyle:=canvas_pointe^.Brush.Style;

canvas_pointe^.Pen.Color:=couleurbord;
canvas_pointe^.brush.color:=couleurf;






 canvas_pointe^.brush.style:=bsSolid;
 canvas_pointe^.pen.Color:=couleurbord;
 canvas_pointe^.Pen.Style:=pssolid;
 canvas_pointe^.pen.Width:=1;
 canvas_pointe^.pen.Mode:=pmcopy;








if not(_transparent) then canvas_pointe^.brush.style:=bsSolid else
canvas_pointe^.brush.style:=bsclear;

 canvas_pointe^.Polygon(titi,nombrepoint,false);

 canvas_pointe^.Pen.Color:=old_pencolor;
canvas_pointe^.Pen.Mode:=old_penmode;
 canvas_pointe^.Pen.Style:=old_penstyle;
 canvas_pointe^.Pen.Width:=old_penwidth;
 canvas_pointe^.Brush.Color:=old_brushcolor;
 canvas_pointe^.Brush.Style:=old_brushstyle;



  end;


     function TMonPostScript.arcdecercle(x,y:extended; rayonreel:extended;
     epaisseur:integer; theta1,theta2:extended;
     couleur:tcolor; _penmod:tpenmode;_style:tpenstyle;gauche:boolean): boolean;

            var x1,y1,rax,ray:integer;
           ymax,ymin,pasgrilley:extended;
       old_pencolor:tcolor;
old_penmode:tpenmode;
 old_penstyle:tpenstyle;
 old_penwidth:integer;
 old_brushcolor:tcolor;
 old_brushstyle:tbrushstyle;

         t1,t2:extended;


  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;

{  if ((x<xmin) or (x>xmax)
 or (y>ymax) or (y<ymin))
  then begin
 arcdecercle:=false;
 exit;
 end;}

      invconvert(x1,y1,x,y,gauche);
   rax:= trunc(rayonreel/(xmax-xmin)*(largeur-BordureGauche-BordureDroite));
   ray:= trunc(rayonreel/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse));
          if rayonreel=0 then exit;


    t1:=donneanglepolairedans02pi(cos(theta1),sin(theta1));
     t2:=donneanglepolairedans02pi(cos(theta2),sin(theta2));
         if t1>t2 then t1:=t1-2*Pi;

        old_pencolor:=canvas_pointe^.Pen.Color;
old_penmode:=canvas_pointe^.Pen.Mode;
 old_penstyle:=canvas_pointe^.Pen.Style;
 old_penwidth:=canvas_pointe^.Pen.Width;
 old_brushcolor:=canvas_pointe^.Brush.Color;
 old_brushstyle:=canvas_pointe^.Brush.Style;


    canvas_pointe^.pen.style:=pssolid;
    canvas_pointe^.pen.width:=epaisseur;
    canvas_pointe^.pen.Style:=_style;

    if ((_penmod=pmnot) or (_penmod=pmxor)or (_penmod=pmnotxor)) then begin
 canvas_pointe^.pen.mode:=pmxor;
 canvas_pointe^.pen.color:=colortorgb(couleur) xor colortorgb(couleurfondsim);
end else begin
  canvas_pointe^.pen.color:=couleur;
  canvas_pointe^.pen.mode:=_penmod;
 end;




     arcdecercle:=true;
    canvas_pointe^.brush.style:=bsclear;


    canvas_pointe^.arc(x1-rax,y1+ray,x1+rax,y1-ray,trunc(t1/Pi*180*16),trunc((t2)/Pi*180*16));
     arcdecercle:=true;

     canvas_pointe^.Pen.Color:=old_pencolor;
canvas_pointe^.Pen.Mode:=old_penmode;
 canvas_pointe^.Pen.Style:=old_penstyle;
 canvas_pointe^.Pen.Width:=old_penwidth;
 canvas_pointe^.Brush.Color:=old_brushcolor;
 canvas_pointe^.Brush.Style:=old_brushstyle;



     end;



      function TMonPostScript.arcdecercleoriente(x,y:extended; rayonreel:extended;
     epaisseur:integer; theta1,theta2:extended;
     couleur:tcolor; _penmod:tpenmode;_style:tpenstyle;gauche,trigod:boolean): boolean;

            var x1,y1,rax,ray:integer;
           ymax,ymin,pasgrilley:extended;
       old_pencolor:tcolor;
old_penmode:tpenmode;
 old_penstyle:tpenstyle;
 old_penwidth:integer;
 old_brushcolor:tcolor;
 old_brushstyle:tbrushstyle;

         t1,t2:extended;


  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;

{  if ((x<xmin) or (x>xmax)
 or (y>ymax) or (y<ymin))
  then begin
 arcdecercle:=false;
 exit;
 end;}

      invconvert(x1,y1,x,y,gauche);
   rax:= trunc(rayonreel/(xmax-xmin)*(largeur-BordureGauche-BordureDroite));
   ray:= trunc(rayonreel/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse));
          if rayonreel=0 then exit;


    t1:=donneanglepolairedans02pi(cos(theta1),sin(theta1));
     t2:=donneanglepolairedans02pi(cos(theta2),sin(theta2));
         //if t1>t2 then t1:=t1-2*Pi;

    if trigod and (t2<t1) then t1:=t1-2*Pi;
    if not(trigod)  and (t2>t1) then t2:=t2-2*Pi;


        old_pencolor:=canvas_pointe^.Pen.Color;
old_penmode:=canvas_pointe^.Pen.Mode;
 old_penstyle:=canvas_pointe^.Pen.Style;
 old_penwidth:=canvas_pointe^.Pen.Width;
 old_brushcolor:=canvas_pointe^.Brush.Color;
 old_brushstyle:=canvas_pointe^.Brush.Style;


    canvas_pointe^.pen.style:=pssolid;
    canvas_pointe^.pen.width:=epaisseur;
    canvas_pointe^.pen.Style:=_style;

    if ((_penmod=pmnot) or (_penmod=pmxor)or (_penmod=pmnotxor)) then begin
 canvas_pointe^.pen.mode:=pmxor;
 canvas_pointe^.pen.color:=colortorgb(couleur) xor colortorgb(couleurfondsim);
end else begin
  canvas_pointe^.pen.color:=couleur;
  canvas_pointe^.pen.mode:=_penmod;
 end;




     arcdecercleoriente:=true;
    canvas_pointe^.brush.style:=bsclear;


    canvas_pointe^.arc(x1-rax,y1+ray,x1+rax,y1-ray,trunc(t1/Pi*180*16),trunc((t2)/Pi*180*16));
     arcdecercleoriente:=true;

     canvas_pointe^.Pen.Color:=old_pencolor;
canvas_pointe^.Pen.Mode:=old_penmode;
 canvas_pointe^.Pen.Style:=old_penstyle;
 canvas_pointe^.Pen.Width:=old_penwidth;
 canvas_pointe^.Brush.Color:=old_brushcolor;
 canvas_pointe^.Brush.Style:=old_brushstyle;



     end;



function TMonPostScript.Point(x,y:extended; rayon:integer; couleur:tcolor
 ; _penmod:tpenmode;gauche:boolean):
 boolean;
            var x1,y1:integer;
            ymax,ymin,pasgrilley:extended;
          old_pencolor:tcolor;
old_penmode:tpenmode;
 old_penstyle:tpenstyle;
 old_penwidth:integer;
 old_brushcolor:tcolor;
 old_brushstyle:tbrushstyle;


  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;
  invconvert(x1,y1,x,y,gauche);



 if ((x<xmin) or (x>xmax)
 or (y>ymax) or (y<ymin))
  then begin
 point:=false;
 exit;
 end;





if rayon=0 then canvas_pointe^.pixels[x1,y1]:=couleur;

     if rayon=0 then exit;

       old_pencolor:=canvas_pointe^.Pen.Color;
old_penmode:=canvas_pointe^.Pen.Mode;
 old_penstyle:=canvas_pointe^.Pen.Style;
 old_penwidth:=canvas_pointe^.Pen.Width;
 old_brushcolor:=canvas_pointe^.Brush.Color;
 old_brushstyle:=canvas_pointe^.Brush.Style;



canvas_pointe^.pen.color:=couleur;
    canvas_pointe^.pen.style:=pssolid;
    canvas_pointe^.pen.width:=1;

    canvas_pointe^.pen.mode:=pmcopy;
    canvas_pointe^.brush.style:=bssolid;
    canvas_pointe^.brush.Color:=couleur;
    canvas_pointe^.ellipse(x1-rayon,y1-rayon,x1+rayon,y1+rayon);
      canvas_pointe^.Pen.Color:=old_pencolor;
canvas_pointe^.Pen.Mode:=old_penmode;
 canvas_pointe^.Pen.Style:=old_penstyle;
 canvas_pointe^.Pen.Width:=old_penwidth;
 canvas_pointe^.Brush.Color:=old_brushcolor;
 canvas_pointe^.Brush.Style:=old_brushstyle;


     end;


function TMonPostScript.cercle(x,y:extended; rayon,epaisseur:integer; couleur:tcolor
; _penmod:tpenmode;gauche:boolean):
 boolean;
            var xx1,yy1:integer;
           ymax,ymin,pasgrilley:extended;
          old_pencolor:tcolor;
old_penmode:tpenmode;
 old_penstyle:tpenstyle;
 old_penwidth:integer;
 old_brushcolor:tcolor;
 old_brushstyle:tbrushstyle;





  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;
            invconvert(xx1,yy1,x,y,gauche);



 if ((x<=xmin) or (x>=xmax)
 or (y<ymin) or (y>ymax) or (rayon=0))

  then begin
 cercle:=false;
 exit;
 end;

    old_pencolor:=canvas_pointe^.Pen.Color;
old_penmode:=canvas_pointe^.Pen.Mode;
 old_penstyle:=canvas_pointe^.Pen.Style;
 old_penwidth:=canvas_pointe^.Pen.Width;
 old_brushcolor:=canvas_pointe^.Brush.Color;
 old_brushstyle:=canvas_pointe^.Brush.Style;


    canvas_pointe^.pen.style:=pssolid;
    canvas_pointe^.pen.width:=epaisseur;


    if ((_penmod=pmnot) or (_penmod=pmxor)or (_penmod=pmnotxor)) then begin
 canvas_pointe^.pen.mode:=pmxor;
 canvas_pointe^.pen.color:=colortorgb(couleur) xor colortorgb(couleurfondsim);
end else begin
  canvas_pointe^.pen.color:=couleur;
  canvas_pointe^.pen.mode:=_penmod;
 end;



     cercle:=true;
    canvas_pointe^.brush.style:=bsclear;


canvas_pointe^.arc(xx1-rayon,yy1+rayon,xx1+rayon,yy1-rayon,0,
360*16);

        canvas_pointe^.Pen.Color:=old_pencolor;
canvas_pointe^.Pen.Mode:=old_penmode;
 canvas_pointe^.Pen.Style:=old_penstyle;
 canvas_pointe^.Pen.Width:=old_penwidth;
 canvas_pointe^.Brush.Color:=old_brushcolor;
 canvas_pointe^.Brush.Style:=old_brushstyle;

     end;

     function TMonPostScript.disque(x,y:extended; rayon:extended; couleurbord,couleurf:tcolor;_transparent:boolean;gauche:boolean):
 boolean;
            var x1,y1,r1x,r1y:integer;
            ymax,ymin,pasgrilley:extended;
          old_pencolor:tcolor;
old_penmode:tpenmode;
 old_penstyle:tpenstyle;
 old_penwidth:integer;
 old_brushcolor:tcolor;
 old_brushstyle:tbrushstyle;



  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;
  invconvert(x1,y1,x,y,gauche);

 if ((x<xmin) or (x>xmax)
 or (y>ymax) or (y<ymin))
  then begin
 disque:=false;
 exit;
 end;

   r1x:= trunc(rayon/(xmax-xmin)*(largeur-BordureGauche-BordureDroite));
 r1y:=trunc((rayon)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse));
      if rayon=0 then exit;



      old_pencolor:=canvas_pointe^.Pen.Color;
old_penmode:=canvas_pointe^.Pen.Mode;
 old_penstyle:=canvas_pointe^.Pen.Style;
 old_penwidth:=canvas_pointe^.Pen.Width;
 old_brushcolor:=canvas_pointe^.Brush.Color;
 old_brushstyle:=canvas_pointe^.Brush.Style;

   canvas_pointe^.pen.color:=couleurbord;
    canvas_pointe^.pen.style:=pssolid;
    canvas_pointe^.pen.width:=1;

    canvas_pointe^.pen.mode:=pmcopy;
     disque:=true;

    canvas_pointe^.brush.color:=couleurf;
   if not(_transparent) then canvas_pointe^.brush.style:=bssolid else
  canvas_pointe^.brush.style:=bsclear;


canvas_pointe^.ellipse(x1-r1x,y1+r1y,x1+r1x,
y1-r1y);

  canvas_pointe^.Pen.Color:=old_pencolor;
canvas_pointe^.Pen.Mode:=old_penmode;
 canvas_pointe^.Pen.Style:=old_penstyle;
 canvas_pointe^.Pen.Width:=old_penwidth;
 canvas_pointe^.Brush.Color:=old_brushcolor;
 canvas_pointe^.Brush.Style:=old_brushstyle;




     end;


function TMonPostScript.Trait(x1,y1,x2,y2:extended; epaisseur:integer;
 couleur:tcolor;sty:tpenstyle; _penmod:tpenmode;gauche:boolean):boolean;
 var ymax,ymin,pasgrilley:extended;
       xi1,yi1,xi2,yi2:integer;
     old_pencolor:tcolor;
old_penmode:tpenmode;
 old_penstyle:tpenstyle;
 old_penwidth:integer;
 old_brushcolor:tcolor;
 old_brushstyle:tbrushstyle;


  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;
       if xmax=xmin then exit;
       if ymax=ymin then exit;

       if ((x1>xmax) or (x1<xmin) or (y1>ymax) or (y1<ymin) or
        (x2>xmax) or (x2<xmin) or (y2>ymax) or (y2<ymin))
       then begin
       trait:=false;
       exit;
       end;



       invconvert(xi1,yi1,x1,y1,true);
       invconvert(xi2,yi2,x2,y2,true);
   old_pencolor:=canvas_pointe^.Pen.Color;
old_penmode:=canvas_pointe^.Pen.Mode;
 old_penstyle:=canvas_pointe^.Pen.Style;
 old_penwidth:=canvas_pointe^.Pen.Width;
 old_brushcolor:=canvas_pointe^.Brush.Color;
 old_brushstyle:=canvas_pointe^.Brush.Style;
canvas_pointe^.pen.width:=epaisseur;
  canvas_pointe^.pen.Style:=sty;
if ((_penmod=pmnot) or (_penmod=pmxor) or (_penmod=pmnotxor)) then begin
 canvas_pointe^.pen.mode:=pmxor;
 canvas_pointe^.pen.color:=colortorgb(couleur) xor colortorgb(couleurfondsim);
end else begin
  canvas_pointe^.pen.color:=couleur;
  canvas_pointe^.pen.mode:=_penmod;
 end;

canvas_pointe^.doLinefromto(xi1,yi1,xi2,yi2);







  canvas_pointe^.Pen.Color:=old_pencolor;
canvas_pointe^.Pen.Mode:=old_penmode;
 canvas_pointe^.Pen.Style:=old_penstyle;
 canvas_pointe^.Pen.Width:=old_penwidth;
 canvas_pointe^.Brush.Color:=old_brushcolor;
 canvas_pointe^.Brush.Style:=old_brushstyle;


 end;



function TMonPostScript.CroixP(x,y:extended;
 demi_longueur,epaisseur:integer; couleur:tcolor; penmod:tpenmode;gauche:boolean):boolean;
 var x1,y1:integer;
 ymax,ymin,pasgrilley:extended;
  old_pencolor:tcolor;
old_penmode:tpenmode;
 old_penstyle:tpenstyle;
 old_penwidth:integer;
 old_brushcolor:tcolor;
 old_brushstyle:tbrushstyle;





  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;
  invconvert(x1,y1,x,y,gauche);

 if ((x<xmin) or (x>xmax)
 or (y>ymax) or (y<ymin))
  then begin
 croixp:=false;
 exit;
 end;



  old_pencolor:=canvas_pointe^.Pen.Color;
old_penmode:=canvas_pointe^.Pen.Mode;
 old_penstyle:=canvas_pointe^.Pen.Style;
 old_penwidth:=canvas_pointe^.Pen.Width;
 old_brushcolor:=canvas_pointe^.Brush.Color;
 old_brushstyle:=canvas_pointe^.Brush.Style;






canvas_pointe^.pen.width:=epaisseur;
 canvas_pointe^.pen.mode:=penmod;
 canvas_pointe^.pen.style:=pssolid;
 canvas_pointe^.pen.color:=couleur;
  canvas_pointe^.DoLineFromTo(x1,y1,x1,y1+demi_longueur);


      canvas_pointe^.DoLineFromTo(x1,y1,x1,y1-demi_longueur);


       canvas_pointe^.DoLineFromTo(x1,y1,x1+demi_longueur,y1);

    canvas_pointe^.DoLineFromTo(x1,y1,x1-demi_longueur,y1);

     croixp:=true;

   canvas_pointe^.Pen.Color:=old_pencolor;
canvas_pointe^.Pen.Mode:=old_penmode;
 canvas_pointe^.Pen.Style:=old_penstyle;
 canvas_pointe^.Pen.Width:=old_penwidth;
 canvas_pointe^.Brush.Color:=old_brushcolor;
 canvas_pointe^.Brush.Style:=old_brushstyle;



   end;


   procedure TMonPostScript.arronditalagrille(var x1,y1:extended;gauche:boolean);
  var newx,newy,divx,divy,fracx,fracy:extended;
  ymax,ymin,pasgrilley:extended;
  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;

  divx:=partieentiere((x1-xmin)/pasgrillex);
  divy:=partieentiere((y1-ymin)/pasgrilley);
  fracx:=x1-xmin-divx*pasgrillex;
  fracy:=y1-ymin-divy*pasgrilley;
  if fracx>0.5*pasgrillex then newx:=(divx+1)*pasgrillex else newx:=divx*pasgrillex;
  if fracy>0.5*pasgrilley then newy:=(divy+1)*pasgrilley else newy:=divy*pasgrilley;
  x1:=newx; y1:=newy;
                        end;



   procedure  TMonPostScript.ecrireI(x,y:integer; s:string; police:tfont);
   var xi,yi:integer;
   old_font:tfont;
   old_pencolor:tcolor;
old_penmode:tpenmode;
 old_penstyle:tpenstyle;
 old_penwidth:integer;
 old_brushcolor:tcolor;
 old_brushstyle:tbrushstyle;
   begin
   old_pencolor:=canvas_pointe^.Pen.Color;
old_penmode:=canvas_pointe^.Pen.Mode;
 old_penstyle:=canvas_pointe^.Pen.Style;
 old_penwidth:=canvas_pointe^.Pen.Width;
 old_brushcolor:=canvas_pointe^.Brush.Color;
 old_brushstyle:=canvas_pointe^.Brush.Style;
    canvas_pointe^.Brush.Color:=couleurfondsim;
    canvas_pointe^.Brush.Style:=bsclear;
       canvas_pointe^.Pen.Color:=couleurfondsim;
    old_font:=canvas_pointe^.Font;
   canvas_pointe^.Font:=police;

   canvas_pointe^.textout(x,y,s);
   canvas_pointe^.Font:=old_font;
    canvas_pointe^.Pen.Color:=old_pencolor;
canvas_pointe^.Pen.Mode:=old_penmode;
 canvas_pointe^.Pen.Style:=old_penstyle;
 canvas_pointe^.Pen.Width:=old_penwidth;
 canvas_pointe^.Brush.Color:=old_brushcolor;
 canvas_pointe^.Brush.Style:=old_brushstyle;
      end;


  procedure  TMonPostScript.ecrire(x,y:extended; s:string;gauche:boolean; police:tfont);
   var xi,yi:integer;
   old_font:tfont;
   old_pencolor:tcolor;
old_penmode:tpenmode;
 old_penstyle:tpenstyle;
 old_penwidth:integer;
 old_brushcolor:tcolor;
 old_brushstyle:tbrushstyle;
   begin
   old_pencolor:=canvas_pointe^.Pen.Color;
old_penmode:=canvas_pointe^.Pen.Mode;
 old_penstyle:=canvas_pointe^.Pen.Style;
 old_penwidth:=canvas_pointe^.Pen.Width;
 old_brushcolor:=canvas_pointe^.Brush.Color;
 old_brushstyle:=canvas_pointe^.Brush.Style;
    canvas_pointe^.Brush.Color:=couleurfondsim;
     canvas_pointe^.Brush.Style:=bsclear;
       canvas_pointe^.Pen.Color:=couleurfondsim;
    invconvert(xi,yi,x,y,gauche);
    old_font:=canvas_pointe^.Font;
   canvas_pointe^.Font:=police;

   canvas_pointe^.textout(xi,yi,s);
   canvas_pointe^.Font:=old_font;
    canvas_pointe^.Pen.Color:=old_pencolor;
canvas_pointe^.Pen.Mode:=old_penmode;
 canvas_pointe^.Pen.Style:=old_penstyle;
 canvas_pointe^.Pen.Width:=old_penwidth;
 canvas_pointe^.Brush.Color:=old_brushcolor;
 canvas_pointe^.Brush.Style:=old_brushstyle;
      end;


procedure TMonPostScript.background(couleur:tcolor);
var
old_pencolor:tcolor;
old_penmode:tpenmode;
 old_penstyle:tpenstyle;
 old_penwidth:integer;
 old_brushcolor:tcolor;
 old_brushstyle:tbrushstyle;




begin
 old_pencolor:=canvas_pointe^.Pen.Color;
old_penmode:=canvas_pointe^.Pen.Mode;
 old_penstyle:=canvas_pointe^.Pen.Style;
 old_penwidth:=canvas_pointe^.Pen.Width;
 old_brushcolor:=canvas_pointe^.Brush.Color;
 old_brushstyle:=canvas_pointe^.Brush.Style;


canvas_pointe^.Brush.Color:=couleur;
canvas_pointe^.Brush.Style:=bssolid;
canvas_pointe^.Pen.Color:=couleur;
   canvas_pointe^.Pen.Mode:=pmcopy;
canvas_pointe^.Rectangle(borduregauche,bordurehaute,largeur-borduredroite,hauteur-bordurebasse);
canvas_pointe^.Pen.Color:=old_pencolor;
canvas_pointe^.Pen.Mode:=old_penmode;
 canvas_pointe^.Pen.Style:=old_penstyle;
 canvas_pointe^.Pen.Width:=old_penwidth;
 canvas_pointe^.Brush.Color:=old_brushcolor;
 canvas_pointe^.Brush.Style:=old_brushstyle;


end;

procedure Donne_Hauteur_Largeur(type_page_ps:page_ps;type_orientation_ps:orientation_ps;
 res_x,res_y:integer; var hauteur,largeur:integer);

 function Sx(AX: Integer): Integer;
    begin
          result := round(res_x/72*Ax);
    end;

    function Sy(AY: Integer): Integer;
    begin
           result := round(res_y/72*Ay);
    end;

    begin

 if ((type_page_ps=Ledger) and  (type_orientation_ps=portrait)) then begin
      hauteur:=Sx(1224);
      largeur:=Sy(792);
    end;

if ((type_page_ps=Ledger) and  (type_orientation_ps=paysage)) then begin
      hauteur:=Sx(792);
      largeur:=Sy(1224);
    end;


if ((type_page_ps=Legal) and  (type_orientation_ps=portrait)) then begin
      hauteur:=Sx(1008);
      largeur:=Sy(612);
    end;

if ((type_page_ps=Legal) and  (type_orientation_ps=paysage)) then begin
      hauteur:=Sx(612);
      largeur:=Sy(1008);
    end;


if ((type_page_ps=Letter) and  (type_orientation_ps=portrait)) then begin
      hauteur:=Sx(792);
      largeur:=Sy(612);
    end;

if ((type_page_ps=Letter) and  (type_orientation_ps=paysage)) then begin
      hauteur:=Sx(612);
      largeur:=Sy(792);
    end;


if ((type_page_ps=A4) and  (type_orientation_ps=portrait)) then begin
      hauteur:=Sy(842);
      largeur:=Sx(595);
    end;


if ((type_page_ps=A4) and  (type_orientation_ps=paysage)) then begin
      hauteur:=Sy(595);
      largeur:=Sx(842);
    end;

if ((type_page_ps=A3) and  (type_orientation_ps=portrait)) then begin
      hauteur:=Sy(1190);
      largeur:=Sx(842);
    end;


if ((type_page_ps=A3) and  (type_orientation_ps=paysage)) then begin
      hauteur:=Sy(842);
      largeur:=Sx(1190);
    end;

if ((type_page_ps=A2) and  (type_orientation_ps=portrait)) then begin
      hauteur:=Sy(1684);
      largeur:=Sx(1190);
    end;


if ((type_page_ps=A2) and  (type_orientation_ps=paysage)) then begin
      hauteur:=Sy(1190);
      largeur:=Sx(1684);
    end;

if ((type_page_ps=A1) and  (type_orientation_ps=portrait)) then begin
      hauteur:=Sy(2380);
      largeur:=Sx(1684);
    end;


if ((type_page_ps=A1) and  (type_orientation_ps=paysage)) then begin
      hauteur:=Sy(1684);
      largeur:=Sx(2380);
    end;

if ((type_page_ps=A0) and  (type_orientation_ps=portrait)) then begin
      hauteur:=Sy(3368);
      largeur:=Sx(2380);
    end;


if ((type_page_ps=A0) and  (type_orientation_ps=paysage)) then begin
      hauteur:=Sy(2380);
      largeur:=Sx(3368);
    end;

if ((type_page_ps=A5) and  (type_orientation_ps=portrait)) then begin
      hauteur:=Sy(595);
      largeur:=Sx(421);
    end;


if ((type_page_ps=A5) and  (type_orientation_ps=paysage)) then begin
      hauteur:=Sy(421);
      largeur:=Sx(595);
    end;


end;



 function TMonPostScript.LimitesEtAxes(pointeur_sur_canvas:ppscanvas;
 _res_x,_res_y:integer;_type_page_ps:page_ps;
 _type_orientation_ps:orientation_ps;
 _xmin,_xmax,_ymin1,_ymax1,_ymin2,_ymax2:extended;
 _police:tfont;
  _titre:string;
  _fontetitre:tfont;
  _Fond:boolean;
 _CouleurFondsim:tcolor;
 _Cadre:boolean; _EpaisseurCadre:integer;
 _CouleurCadre:tcolor;
 _Gradue:boolean;
 _EpaisseurGraduation,_LongueurGraduation:integer;
 _couleurgraduation:tcolor;
 _fontegraduation:tfont;
 _Grille1,_Grille2:boolean; _EpaisseurGrille:integer;
 _CouleurGrille1,_CouleurGrille2:tcolor;
 _labelx,_labely1,_labely2,_unitex,_unitey1,_unitey2:string;
  _borduresverticalessymetriques,
 _echelle_g,_echelle_d:boolean;
  b_g,b_d,b_b,b_h:integer):boolean;




  var xi,xa,yi1,yi2,ya1,ya2,gi,ga:extended;
     t1,t2,i:integer;
     dixx,dixy1,dixy2,tt,aff:string;
     nbgx,nbgy1,nbgy2:integer;
   old_pencolor:tcolor;
old_penmode:tpenmode;
 old_penstyle:tpenstyle;
 old_penwidth:integer;
 old_brushcolor:tcolor;
 old_brushstyle:tbrushstyle;

        old_font:tfont;

  hhh,lll:integer;


 begin
canvas_pointe:=pointeur_sur_canvas;
res_x:=_res_x;
res_y:=_res_y;
type_page_ps:=_type_page_ps;
type_orientation_ps:=_type_orientation_ps;
canvas_pointe^.XDPI := res_x;
canvas_pointe^.YDPI := res_y;

Donne_Hauteur_Largeur(type_page_ps,type_orientation_ps,res_x,res_y,hhh,lll);
           canvas_pointe^.PaperHeight:=hhh;
      canvas_pointe^.PaperWidth:=lll;
   if  type_orientation_ps=portrait then  canvas_pointe^.Orientation:=poportrait else
     canvas_pointe^.Orientation:=polandscape;



    canvas_pointe^.BeginDoc;  
     canvas_pointe^.Font.Assign(_police);


  old_pencolor:=canvas_pointe^.Pen.Color;
old_penmode:=canvas_pointe^.Pen.Mode;
 old_penstyle:=canvas_pointe^.Pen.Style;
 old_penwidth:=canvas_pointe^.Pen.Width;
 old_brushcolor:=canvas_pointe^.Brush.Color;
 old_brushstyle:=canvas_pointe^.Brush.Style;
             old_font:=canvas_pointe^.Font;


 fond:=_fond;        echelle_g:=_echelle_g; echelle_d:=_echelle_d;
  borduresverticalessymetriques:=_borduresverticalessymetriques;
 labelx:=_labelx;
 labely1:=_labely1;   labely2:=_labely2;
 unitex:=_unitex; unitey1:=_unitey1; unitey2:=_unitey2;
   fontetitre:=_fontetitre;
  largeur:=canvas_pointe^.Paperwidth;
  hauteur:=canvas_pointe^.PaperHeight;

    titre:=_titre;
  couleurfondsim:=_couleurfondsim;

  cadre:=_cadre; epaisseurcadre:=_epaisseurcadre;
  couleurcadre:=_couleurcadre;
  gradue:=_gradue; epaisseurgraduation:=_epaisseurgraduation;

  fontegraduation:=_fontegraduation;
  couleurgraduation:=_couleurgraduation;
  grille1:=_grille1;   grille2:=_grille2;
  epaisseurgrille:=_epaisseurgrille;
  couleurgrille1:=_couleurgrille1;
  couleurgrille2:=_couleurgrille2;

 if (echelle_g and ( (_ymin1=_ymax1) or (_xmax=_xmin))) then begin
 LimitesEtAxes:=false;
 exit;
 end;
   if (echelle_d and ( (_ymin2=_ymax2) or (_xmax=_xmin))) then begin
 LimitesEtAxes:=false;
 exit;
 end;

 LimitesEtAxes:=true;
 if (_xmin>_xmax ) then begin
 xmin:=_xmax;
 xmax:=_xmin;
 end else begin
 xmin:=_xmin;
 xmax:=_xmax;
 end;

 if (_ymin1>_ymax1 ) then begin
 ymin1:=_ymax1;
 ymax1:=_ymin1;
 end else begin
 ymin1:=_ymin1;
 ymax1:=_ymax1;
 end;

 if (_ymin2>_ymax2 ) then begin
 ymin2:=_ymax2;
 ymax2:=_ymin2;
 end else begin
 ymin2:=_ymin2;
 ymax2:=_ymax2;
 end;

  borduregauche:=b_g;
  borduredroite:=b_d;
  bordurebasse:=b_b;
  bordurehaute:=b_h;


if fond then background(couleurfondsim);

   xi:=xmin; yi1:=ymin1; yi2:=ymin2; xa:=xmax; ya1:=ymax1;  ya2:=ymax2;


canvas_pointe^.font:=fontetitre;
 if (titre<>'') then begin
 t1:=trunc(canvas_pointe^.textheight(titre))+10;
 end else t1:=10;

 if gradue then begin
canvas_pointe^.font:=fontegraduation;
 if ((dixy1='0') and (dixy2='0')) then t2:=trunc(canvas_pointe^.textheight('10'+unitex+labelx)*1.5)
 else t2:=trunc(canvas_pointe^.textheight('10'+unitex+labelx)*2.2);
 end else t2:=10;

 if (t1>t2)  then bordurehaute:=t1 else bordurehaute:=t2;

{version pour optgeo: pas de bordure}
   bordurehaute:=0;



 {taille bordure basse}
 if gradue then begin
canvas_pointe^.font:=fontegraduation;
 t1:=trunc(canvas_pointe^.textheight('x10'))+10;
 end else t1:=10;
 bordurebasse:=t1;

 {version pour optgeo: pas de bordure}
   bordurebasse:=0;

 {taille bordure droite}

 if (gradue and echelle_d) then begin
canvas_pointe^.font:=fontegraduation;
 str( trunc((ymin2)/dix_pp(puissancededixy2-1)),tt);
 t1:=trunc(canvas_pointe^.textwidth(tt))+10;
  str( trunc((ymin2+nbgy2*graduationy2)/dix_pp(puissancededixy2-1)),tt);
 t2:=trunc(canvas_pointe^.textwidth(tt))+10;
 end else begin
 t1:=10;
 t2:=10;
 end;

 if t1<t2 then t1:=t2;
 if gradue then begin
canvas_pointe^.font:=fontegraduation;
 str( trunc((xmin+nbgx*graduationx)/dix_pp(puissancededixx-1)),tt);
 t2:=trunc(canvas_pointe^.textwidth(tt)*1.2)div 2;
 end else t2:=10;
 if t1>t2 then borduredroite:=t1 else borduredroite:=t2;

 {version pour optgeo: pas de bordure}
   borduredroite:=0;


 {taille bordure gauche}

 if (gradue and echelle_g) then begin
canvas_pointe^.font:=fontegraduation;
 str( trunc((ymin1)/dix_pp(puissancededixy1-1)),tt);
 t1:=trunc(canvas_pointe^.textwidth(tt))+10;
  str( trunc((ymin1+nbgy1*graduationy1)/dix_pp(puissancededixy1-1)),tt);
 t2:=trunc(canvas_pointe^.textwidth(tt))+10;
 end else begin
 t1:=10;
 t2:=10;
 end;

 if t1<t2 then t1:=t2;
 if gradue then begin
canvas_pointe^.font:=fontegraduation;
 str( trunc((xmin)/dix_pp(puissancededixx-1)),tt);
 t2:=trunc(canvas_pointe^.textwidth(tt)*1.2)div 2;
 end else t2:=10;

 if t1>t2 then borduregauche:=t1 else borduregauche:=t2;

 {version pour optgeo: pas de bordure}
   borduregauche:=0;


 if borduresverticalessymetriques then begin
  borduregauche:=max(borduregauche,borduredroite);
  borduredroite:=borduregauche;
  end;


  borduregauche:=b_g;
  borduredroite:=b_d;
  bordurebasse:=b_b;
  bordurehaute:=b_h;











 {longueurgraduation}
  longueurgraduationX:=_longueurgraduation/largeur*(xmax-xmin);
  if echelle_g then
   longueurgraduationy1:=_longueurgraduation/hauteur*(ymax1-ymin1);
  if echelle_d then
   longueurgraduationy2:=_longueurgraduation/hauteur*(ymax2-ymin2);

 if grille1 then begin
   canvas_pointe^.pen.style:=psdot;
   canvas_pointe^.pen.color:=couleurgrille1;
   canvas_pointe^.pen.width:=epaisseurgrille;
       for i:=0 to nbgx do
          trait(xmin+i*graduationx,ymin1,xmin+i*graduationx,ymax1,
          epaisseurgrille,couleurgrille1,psdot,pmcopy,true);
         for i:=0 to nbgy1 do
          trait(xmin,ymin1+i*graduationy1,xmax,ymin1+i*graduationy1,
          epaisseurgrille,couleurgrille1,psdot,pmcopy,true);
            end;
   if grille2 then begin
   canvas_pointe^.pen.style:=psdot;
   canvas_pointe^.pen.color:=couleurgrille2;
   canvas_pointe^.pen.width:=epaisseurgrille;
       for i:=0 to nbgx do
          trait(xmin+i*graduationx,ymin2,xmin+i*graduationx,ymax2,
          epaisseurgrille,couleurgrille2,psdot,pmcopy,false);
         for i:=0 to nbgy2 do
          trait(xmin,ymin2+i*graduationy2,xmax,ymin2+i*graduationy2,
          epaisseurgrille,couleurgrille2,psdot,pmcopy,false);
            end;
 {cadre}
 if cadre then begin
   canvas_pointe^.pen.width:=epaisseurcadre;
 canvas_pointe^.pen.mode:=pmcopy;
 canvas_pointe^.pen.style:=pssolid;
 canvas_pointe^.pen.color:=couleurcadre;

   canvas_pointe^.DoLineFromTo(BordureGauche,BordureHaute,largeur-BordureDroite,BordureHaute);
   canvas_pointe^.DoLineFromTo(largeur-BordureDroite,BordureHaute,largeur-BordureDroite,hauteur-BordureBasse);
   canvas_pointe^.DoLineFromTo(largeur-BordureDroite,hauteur-BordureBasse,BordureGauche,hauteur-BordureBasse);
   canvas_pointe^.DoLineFromTo(BordureGauche,hauteur-BordureBasse,BordureGauche,BordureHaute);
      end;

    {affichage du titre}
canvas_pointe^.font:=fontetitre;
 while   ((canvas_pointe^.textwidth(titre)>largeur) and
 (canvas_pointe^.Font.Size>1)) do
canvas_pointe^.Font.Size:=canvas_pointe^.Font.Size-1;
 if canvas_pointe^.Font.Size<6 then canvas_pointe^.Font.Size:=6;


canvas_pointe^.textout(largeur div 2 -(canvas_pointe^.textwidth(titre) div 2),
 2,titre);
    {graduation}

    if gradue then begin

                 if dixx='0' then aff:='' else begin
                 aff:='10';
                 if dixx<>'1' then for i:=1 to length(dixx) do aff:=aff+' ';
                 aff:=aff+' ';
                 end;
                 aff:=aff+labelx;
                 if ((unitex<>'1') and (unitex<>'')) then aff:=aff+' en '+unitex;
 canvas_pointe^.font:=fontegraduation;
 canvas_pointe^.textout(largeur-borduredroite-trunc(canvas_pointe^.textwidth
  (aff)*1.3),hauteur-bordurebasse-
  trunc(canvas_pointe^.textheight('x10')*1.3),aff);
  if ((dixx<>'0') and (dixx<>'1')) then
 canvas_pointe^.textout(largeur-borduredroite-trunc(canvas_pointe^.textwidth(aff)*1.3)+
  trunc(canvas_pointe^.textwidth('10')),hauteur-bordurebasse
  -trunc(1.9*canvas_pointe^.textheight('x10')),
  dixx);

  for i:=0 to nbgx do begin
  str(round((xmin+i*graduationx)/dix_pp(puissancededixx-1)),tt);
 canvas_pointe^.textout(

  trunc(i*graduationx/(xmax-xmin)*(largeur-borduredroite-borduregauche)+
  borduregauche)-canvas_pointe^.textwidth(tt) div 2,
  hauteur-trunc(canvas_pointe^.textheight(dixx)+10),tt);


              end;


      if echelle_g then begin
       if dixy1='0' then aff:='' else begin
                 aff:='10';
       if dixy1<>'1' then   for i:=1 to length(dixy1) do aff:=aff+' ';
                 aff:=aff+' ';
                 end;
                 aff:=aff+labely1;
                 if ((unitey1<>'1') and (unitey1<>'')) then aff:=aff+' en '+unitey1;
 canvas_pointe^.font:=fontegraduation;
 canvas_pointe^.textout(borduregauche,bordurehaute-trunc(canvas_pointe^.textheight(aff)*1.2),aff);
  if ((dixy1<>'0') and (dixy1<>'1')) then
 canvas_pointe^.textout(borduregauche+canvas_pointe^.textwidth('10'),bordurehaute
  -trunc(canvas_pointe^.textheight(aff)*1.8),
  dixy1); end;

  if echelle_d then begin
       if dixy2='0' then aff:='' else begin
                 aff:='10';
       if dixy2<>'1' then   for i:=1 to length(dixy2) do aff:=aff+' ';
                 aff:=aff+' ';
                 end;
                 aff:=aff+labely2;
                 if ((unitey2<>'1') and (unitey2<>'')) then aff:=aff+' en '+unitey2;
 canvas_pointe^.font:=fontegraduation;
  canvas_pointe^.textout(largeur-borduredroite-trunc(canvas_pointe^.textwidth(aff)*1.2),
  bordurehaute-trunc(canvas_pointe^.textheight(aff)*1.2),aff);
  if ((dixy2<>'0') and (dixy2<>'1')) then
 canvas_pointe^.textout(largeur-borduredroite-trunc(canvas_pointe^.textwidth(aff)*1.2)+canvas_pointe^.textwidth('10'),
  bordurehaute
  -trunc(canvas_pointe^.textheight(aff)*1.8),
  dixy2);  end;



  if echelle_g then
  for i:=0 to nbgy1 do begin
  str( round((ymin1+i*graduationy1)/dix_pp(puissancededixy1-1)),tt);
  if i=0 then
     canvas_pointe^.textout( borduregauche-trunc(canvas_pointe^.textwidth(tt)*1.1),
  trunc(-  bordurebasse+hauteur)-canvas_pointe^.textheight(tt) ,
  tt) else
     canvas_pointe^.textout( borduregauche-trunc(canvas_pointe^.textwidth(tt)*1.1),
  trunc(-i*graduationy1/(ymax1-ymin1)*(hauteur-bordurehaute-bordurebasse)-
  bordurebasse+hauteur)-canvas_pointe^.textheight(tt) div 2,
  tt);
              end;

              if echelle_d then
     for i:=0 to nbgy2 do begin
  str( round((ymin2+i*graduationy2)/dix_pp(puissancededixy2-1)),tt);
  if i=0 then

 canvas_pointe^.textout( largeur-borduredroite+5,
  trunc(-  bordurebasse+hauteur)-canvas_pointe^.textheight(tt) ,
  tt) else

 canvas_pointe^.textout( largeur-borduredroite+5,
  trunc(-i*graduationy2/(ymax2-ymin2)*(hauteur-bordurehaute-bordurebasse)-
  bordurebasse+hauteur)-canvas_pointe^.textheight(tt) div 2,
  tt);
              end;






 if echelle_g then begin
   for i:=0 to nbgx do
          trait(xmin+i*graduationx,ymin1,xmin+i*graduationx,
          ymin1+longueurgraduationy1,epaisseurgraduation,couleurgraduation,pssolid,pmcopy,true);

           for i:=0 to nbgx do
          trait(xmin+i*graduationx,ymax1,xmin+i*graduationx,
          ymax1-longueurgraduationy1,epaisseurgraduation,couleurgraduation,pssolid,pmcopy,true);


   for i:=0 to nbgy1 do
          trait(xmin,ymin1+i*graduationy1,xmin+longueurgraduationx,ymin1+i*graduationy1,
          epaisseurgraduation,couleurgraduation,pssolid,pmcopy,true);

     for i:=0 to nbgy1 do
          trait(xmax,ymin1+i*graduationy1,xmax-longueurgraduationx,ymin1+i*graduationy1,
          epaisseurgraduation,couleurgraduation,pssolid,pmcopy,true);
           end;

    if echelle_d then begin
   for i:=0 to nbgx do
          trait(xmin+i*graduationx,ymin2,xmin+i*graduationx,
          ymin2+longueurgraduationy2,epaisseurgraduation,couleurgraduation,pssolid,pmcopy,false);

           for i:=0 to nbgx do
          trait(xmin+i*graduationx,ymax2,xmin+i*graduationx,
          ymax2-longueurgraduationy2,epaisseurgraduation,couleurgraduation,pssolid,pmcopy,false);


   for i:=0 to nbgy2 do
          trait(xmin,ymin2+i*graduationy2,xmin+longueurgraduationx,ymin2+i*graduationy2,
          epaisseurgraduation,couleurgraduation,pssolid,pmcopy,false);

     for i:=0 to nbgy2 do
          trait(xmax,ymin2+i*graduationy2,xmax-longueurgraduationx,ymin2+i*graduationy2,
          epaisseurgraduation,couleurgraduation,pssolid,pmcopy,false);
           end;
   end;
   canvas_pointe^.Pen.Color:=old_pencolor;
canvas_pointe^.Pen.Mode:=old_penmode;
 canvas_pointe^.Pen.Style:=old_penstyle;
 canvas_pointe^.Pen.Width:=old_penwidth;
 canvas_pointe^.Brush.Color:=old_brushcolor;
 canvas_pointe^.Brush.Style:=old_brushstyle;
        canvas_pointe^.font:=old_font;
        end;







    function TMonPostScript.Convert(xi,yi:integer; var x,y:extended;gauche:boolean):boolean;
   var  ymax,ymin,pasgrilley:extended;
  begin

  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;
     x:=(xi-BordureGauche)*(xmax-xmin)/ (largeur-BordureGauche-BordureDroite)
     +xmin;
     y:=-(yi-BordureHaute)*(ymax-ymin)/(hauteur-BordureHaute-BordureBasse)
     +ymax;
     convert:=true;
     end;


function TMonPostScript.invconvert(var xi,yi:integer; x,y:extended;gauche:boolean):boolean;
var ymax,ymin,pasgrilley:extended;
  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;

 xi:= trunc((x-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite))+
 BordureGauche;
 yi:=trunc((ymax-y)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse))+
 BordureHaute;
invconvert:=true;
end;

function TMonPostScript.CroixX(x,y:extended;
 demi_diagonale,epaisseur:integer; couleur:tcolor; penmod:tpenmode;gauche:boolean):boolean;
 var x1,y1:integer;
 ymax,ymin,pasgrilley:extended;
  old_pencolor:tcolor;
old_penmode:tpenmode;
 old_penstyle:tpenstyle;
 old_penwidth:integer;
 old_brushcolor:tcolor;
 old_brushstyle:tbrushstyle;




  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;

  invconvert(x1,y1,x,y,gauche);

 if ((x<xmin) or (x>xmax)
 or (y>ymax) or (y<ymin))
  then begin
 croixx:=false;
 exit;
 end;


   old_pencolor:=canvas_pointe^.Pen.Color;
old_penmode:=canvas_pointe^.Pen.Mode;
 old_penstyle:=canvas_pointe^.Pen.Style;
 old_penwidth:=canvas_pointe^.Pen.Width;
 old_brushcolor:=canvas_pointe^.Brush.Color;
 old_brushstyle:=canvas_pointe^.Brush.Style;




canvas_pointe^.pen.width:=epaisseur;
 canvas_pointe^.pen.mode:=penmod;
 canvas_pointe^.pen.style:=pssolid;
 canvas_pointe^.pen.color:=couleur;
  canvas_pointe^.DoLineFromTo(x1,y1,x1+demi_diagonale,y1+demi_diagonale);


      canvas_pointe^.DoLineFromTo(x1,y1,x1-demi_diagonale,y1+demi_diagonale);


       canvas_pointe^.DoLineFromTo(x1,y1,x1+demi_diagonale,y1-demi_diagonale);

    canvas_pointe^.DoLineFromTo(x1,y1,x1-demi_diagonale,y1-demi_diagonale);

     croixx:=true;

   canvas_pointe^.Pen.Color:=old_pencolor;
canvas_pointe^.Pen.Mode:=old_penmode;
 canvas_pointe^.Pen.Style:=old_penstyle;
 canvas_pointe^.Pen.Width:=old_penwidth;
 canvas_pointe^.Brush.Color:=old_brushcolor;
 canvas_pointe^.Brush.Style:=old_brushstyle;



   end;


end.

