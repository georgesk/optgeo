{  This file is part of OptGeo, optical simulation software
    Copyright (C) 2004  Jean-Marie Biansan
        Contact: jeanmarie.biansan@free.fr
    Web site: http://jeanmarie.biansan.free.fr/logiciel.html
 From version 1.25, OptGeo is distribued under the terms of the
 GNU General Public License.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA}

    unit saisiech;

{$mode objfpc}{$H+}

interface

uses
  LCLIntf, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, LResources,UnitScaleFont,UChaines;

type

  { Tsaisiechoix }

  Tsaisiechoix = class(TForm)
    Edit_indice_bleu_par_defaut: TEdit;
    Edit_indice_vert_par_defaut: TEdit;
    Edit_indice_rouge_par_defaut: TEdit;
    radiochoix: TRadioGroup;
    BitBtn1: TBitBtn;
    StaticText2: TLabel;
    StaticText3: TLabel;
    StaticText4: TLabel;
    StaticText5: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private  encreation:boolean;
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

var
  saisiechoix: Tsaisiechoix;

implementation


{ Tsaisiechoix }

procedure Tsaisiechoix.FormShow(Sender: TObject);
begin
 // if encreation then begin scalefont(self); encreation:=false; end;
end;

procedure Tsaisiechoix.FormCreate(Sender: TObject);
begin
   encreation:=true;
   BITBTN1.CAPTION
:=rsSuivant;

CAPTION:=
rsQueVoulezVou;

RADIOCHOIX.CAPTION:=rsVoulezVous;



RADIOCHOIX.Items[0]:=rsCrRUneNouvel;
RADIOCHOIX.Items[1]:=rsCrRUneNouvel2;
RADIOCHOIX.Items[2]:=rsOuvrirUneSim;

STATICTEXT2.CAPTION :=rsVousPouvezCh;
STATICTEXT3.CAPTION:=rsPourLeVert;
STATICTEXT4.CAPTION:=rsPourLeRouge;
STATICTEXT5.CAPTION:=rsPourLeBleu;

end;

initialization

  {$i saisiech.lrs}

end.
