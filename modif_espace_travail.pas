{  This file is part of OptGeo, optical simulation software
    Copyright (C) 2004  Jean-Marie Biansan
        Contact: jeanmarie.biansan@free.fr
    Web site: http://jeanmarie.biansan.free.fr/logiciel.html
 From version 1.25, OptGeo is distribued under the terms of the
 GNU General Public License.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA}

    unit modif_espace_travail;

{$mode objfpc}{$H+}

interface

uses
  LCLIntf, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, LResources,LCLType,UnitScaleFont,UChaines;

type

  { Tsaisieretailleespace }

  Tsaisieretailleespace = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Label1: TLabel;
    Edit_xxmin_m: TEdit;
    Label2: TLabel;
    edit_xxmax_m: TEdit;
    Label3: TLabel;
    Label4: TLabel;
    Edit_yymin_m: TEdit;
    Edit_yymax_m: TEdit;
    log1: TLabel;
    log2: TLabel;
    log3: TLabel;
    log4: TLabel;
    cocherespect_m: TCheckBox;
    Label5: TLabel;

    procedure BitBtn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private  encreation:boolean;
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

var
  saisieretailleespace: Tsaisieretailleespace;

  xxmin_m,xxmax_m,yymin_m,yymax_m:extended;
  respect_m:boolean;
implementation




procedure Tsaisieretailleespace.BitBtn1Click(Sender: TObject);
begin
try
xxmin_m:=strtofloat(edit_xxmin_m.text);
except
application.messagebox(pchar(rsExpressionIl), pchar(rsAttention3), mb_ok);
 self.ModalResult:=mrcancel;
 exit;
 end;

try
xxmax_m:=strtofloat(edit_xxmax_m.text);
except
application.messagebox(pchar(rsExpressionIl2), pchar(rsAttention3), mb_ok);
 self.ModalResult:=mrcancel;
 exit;
 end;

 try
yymin_m:=strtofloat(edit_yymin_m.text);
except
application.messagebox(pchar(rsExpressionIl3), pchar(rsAttention3), mb_ok);
 self.ModalResult:=mrcancel;
 exit;
 end;

 try
yymax_m:=strtofloat(edit_yymax_m.text);
except
application.messagebox(pchar(rsExpressionIl4), pchar(rsAttention3), mb_ok);
 self.ModalResult:=mrcancel;
 exit;
 end;

if xxmax_m<=xxmin_m then begin
application.messagebox(pchar(rsXmaxDoitTreS), pchar(rsAttention3), mb_ok);
 self.ModalResult:=mrcancel;
 exit;
 end;

 if yymax_m<=yymin_m then begin
application.messagebox(pchar(rsYmaxDoitTreS), pchar(rsAttention3), mb_ok);
 self.ModalResult:=mrcancel;
 exit;
 end;

 respect_m:=cocherespect_m.Checked;
end;

procedure Tsaisieretailleespace.FormCreate(Sender: TObject);
begin
  encreation:=true;
  BITBTN1.CAPTION:=rsOK;


BITBTN2.CAPTION:=rsAnnuler;


CAPTION:=rsModifierLEsp;


COCHERESPECT_M.CAPTION:=rsRespecterRap;


LABEL1.CAPTION:=rsAbscisseMin;


LABEL2.CAPTION:=rsAbscisseMax;


LABEL3.CAPTION:=rsOrdonnEMin;


LABEL4.CAPTION:=rsOrdonnEMax;


LABEL5.CAPTION:=rsPourAvoirUnR;
end;

procedure Tsaisieretailleespace.FormShow(Sender: TObject);
begin
  //if encreation then begin scalefont(self); encreation:=false; end;
end;

initialization
  {$i modif_espace_travail.lrs}


end.
