{  This file is part of OptGeo, optical simulation software
    Copyright (C) 2004  Jean-Marie Biansan
        Contact: jeanmarie.biansan@free.fr
    Web site: http://jeanmarie.biansan.free.fr/logiciel.html
 From version 1.25, OptGeo is distribued under the terms of the
 GNU General Public License.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA}


    unit saisiemscopa;

{$mode objfpc}{$H+}

interface

uses
  LCLIntf, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, Buttons, Spin,ColorBox, LResources,LCLType,UnitScaleFont,UChaines;

type

  { Tsaisiemiroirscopa }

  Tsaisiemiroirscopa = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    gridaxe: TColorBox;
    Image1: TImage;
    GroupBox1: TGroupBox;
    StaticText1: TLabel;
    StaticText2: TLabel;
    editx1: TEdit;
    edity1: TEdit;
    GroupBox2: TGroupBox;
    StaticText3: TLabel;
    StaticText4: TLabel;
    editx2: TEdit;
    edity2: TEdit;
    GroupBox3: TGroupBox;
    ColorGrid1: TColorBox;
    GroupBox4: TGroupBox;
    editepaisseur: TSpinEdit;
    GroupBox5: TGroupBox;
    cochehachures: TCheckBox;
    cocheaxe: TCheckBox;
    GroupBox6: TGroupBox;
    StaticText5: TLabel;
    editfocale: TEdit;
    GroupBox7: TGroupBox;
    boutonsup: TBitBtn;
    GroupBox8: TGroupBox;
    checktrou: TCheckBox;
    StaticText6: TLabel;
    editdiametretrou: TEdit;
    log1: TLabel;
    log2: TLabel;
    log3: TLabel;
    log4: TLabel;
    log5: TLabel;
    log6: TLabel;
    procedure BitBtn1Click(Sender: TObject);
    procedure checktrouClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private  encreation:boolean;
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

var
  saisiemiroirscopa: Tsaisiemiroirscopa;
   mscopa_hachures,mscopa_axe,mscopa_trou:boolean;
  mscopa_epaisseur:integer;
  mscopa_couleur,mscopa_couleuraxe:tcolor;
  mscopa_x1,mscopa_x2,mscopa_y1,mscopa_y2,mscopa_focale,mscopa_diametretrou:extended;
implementation

uses unit222;


procedure Tsaisiemiroirscopa.BitBtn1Click(Sender: TObject);
begin
mscopa_hachures:=cochehachures.Checked;
mscopa_trou:=checktrou.checked;
mscopa_axe:=cocheaxe.Checked;
mscopa_epaisseur:=editepaisseur.Value;
mscopa_couleur:=colorgrid1.selected;
mscopa_couleuraxe:=gridaxe.selected;

if mscopa_trou then
try
mscopa_diametretrou:=strtofloat(editdiametretrou.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce32),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;

 if not(mscopa_trou) then mscopa_diametretrou:=0;


try
mscopa_x1:=strtofloat(editx1.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce13),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;


 try
mscopa_x2:=strtofloat(editx2.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce14),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;


 try
mscopa_y1:=strtofloat(edity1.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce15),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;


 try
mscopa_y2:=strtofloat(edity2.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce16),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;



 try
mscopa_focale:=strtofloat(editfocale.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce24),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;
 if (mscopa_focale=0) then begin
 application.messagebox(pchar(rsLaFocaleDoit),
 pchar(rsattention),mb_ok);
 exit;
 end;
 mscopa_focale:=abs(mscopa_focale);
    ReTaillePMscopa(ListeMscopa,nombreMscopa,nombreMscopa+1);
 inc(nombremscopa);
if not
(Listemscopa[-1+nombremscopa].create2(mscopa_x1,mscopa_y1,mscopa_x2,mscopa_y2,
mscopa_focale,mscopa_epaisseur,
mscopa_couleur,mscopa_couleuraxe,mscopa_hachures,mscopa_axe,
mscopa_trou,mscopa_diametretrou)) then begin
ReTaillePMscopa(ListeMscopa,nombreMscopa,nombreMscopa-1);
dec(nombremscopa);
application.messagebox(pchar(rsTailleDuMiro2),
pchar(rsattention),mb_ok);
end else
modalresult:=mrok;
Rafraichit;


end;

procedure Tsaisiemiroirscopa.checktrouClick(Sender: TObject);
begin
editdiametretrou.enabled:=checktrou.checked;
end;

procedure Tsaisiemiroirscopa.FormCreate(Sender: TObject);
begin
   encreation:=true;
   BITBTN1.CAPTION
// msgctxt ';TSAISIEMIROIRSCOPA.BITBTN1.CAPTION';
:=rsOK;


BITBTN2.CAPTION
// msgctxt ';TSAISIEMIROIRSCOPA.BITBTN2.CAPTION';
:=rsAnnuler;


BOUTONSUP.CAPTION
// msgctxt ';TSAISIEMIROIRSCOPA.BOUTONSUP.CAPTION';
:=rsSupprimerCeM;


CAPTION
:=rsAjoutDUnMiro11;


CHECKTROU.CAPTION
// msgctxt ';TSAISIEMIROIRSCOPA.CHECKTROU.CAPTION';
:=rsTrouer;


COCHEAXE.CAPTION
// msgctxt ';TSAISIEMIROIRSCOPA.COCHEAXE.CAPTION';
:=rsAxeOptique;


COCHEHACHURES.CAPTION
// msgctxt ';TSAISIEMIROIRSCOPA.COCHEHACHURES.CAPTION';
:=rsHachures;


GROUPBOX1.CAPTION
// msgctxt ';TSAISIEMIROIRSCOPA.GROUPBOX1.CAPTION';
:=rsPoint1;


GROUPBOX2.CAPTION
// msgctxt ';TSAISIEMIROIRSCOPA.GROUPBOX2.CAPTION';
:=rsPoint2;


GROUPBOX3.CAPTION
// msgctxt ';TSAISIEMIROIRSCOPA.GROUPBOX3.CAPTION';
:=rsCouleurDuMir;


GROUPBOX4.CAPTION
// msgctxt ';TSAISIEMIROIRSCOPA.GROUPBOX4.CAPTION';
:=rsEpaisseurTra8;


GROUPBOX5.CAPTION
// msgctxt ';TSAISIEMIROIRSCOPA.GROUPBOX5.CAPTION';
:=rsAspect;


GROUPBOX6.CAPTION
// msgctxt ';TSAISIEMIROIRSCOPA.GROUPBOX6.CAPTION';
:=rsFocale;


GROUPBOX7.CAPTION
// msgctxt ';TSAISIEMIROIRSCOPA.GROUPBOX7.CAPTION';
:=rsCouleurDeLAx6;


GROUPBOX8.CAPTION
// msgctxt ';TSAISIEMIROIRSCOPA.GROUPBOX8.CAPTION';
:=rsTrouerLeMiro;


LOG1.CAPTION
// msgctxt ';TSAISIEMIROIRSCOPA.LOG1.CAPTION';
:=rsLog1;


LOG2.CAPTION
// msgctxt ';TSAISIEMIROIRSCOPA.LOG2.CAPTION';
:=rsLog2;


LOG3.CAPTION
// msgctxt ';TSAISIEMIROIRSCOPA.LOG3.CAPTION';
:=rsLog3;


LOG4.CAPTION
// msgctxt ';TSAISIEMIROIRSCOPA.LOG4.CAPTION';
:=rsLog4;


LOG5.CAPTION
// msgctxt ';TSAISIEMIROIRSCOPA.LOG5.CAPTION';
:=rsLog5;


LOG6.CAPTION
// msgctxt ';TSAISIEMIROIRSCOPA.LOG6.CAPTION';
:=rsLog6;


STATICTEXT1.CAPTION
// msgctxt ';TSAISIEMIROIRSCOPA.STATICTEXT1.CAPTION';
:=rsX;


STATICTEXT2.CAPTION
// msgctxt ';TSAISIEMIROIRSCOPA.STATICTEXT2.CAPTION';
:=rsY;


STATICTEXT3.CAPTION
// msgctxt ';TSAISIEMIROIRSCOPA.STATICTEXT3.CAPTION';
:=rsX;


STATICTEXT4.CAPTION
// msgctxt ';TSAISIEMIROIRSCOPA.STATICTEXT4.CAPTION';
:=rsY;


STATICTEXT5.CAPTION
// msgctxt ';TSAISIEMIROIRSCOPA.STATICTEXT5.CAPTION';
:=rsF;


STATICTEXT6.CAPTION
// msgctxt ';TSAISIEMIROIRSCOPA.STATICTEXT6.CAPTION';
:=rsDiamTreDuTro;

end;

procedure Tsaisiemiroirscopa.FormShow(Sender: TObject);
begin
 // if encreation then begin scalefont(self); encreation:=false; end;
end;

initialization
  {$i saisiemscopa.lrs}


end.
