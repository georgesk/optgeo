unit UChaines;

{$mode objfpc}

interface

uses
  Classes, SysUtils; 

resourcestring
  rsOptGeoEstDif = 'OptGeo est diffusé sous license GPL; veuillez la lire:';
  rsBienvenue = 'Bienvenue...';
  rsImpossibleDe = 'Impossible de créer le fichier de configuration primaire '
    +'d''OptGeo. Vérifiez que vous avez le droit d''écriture dans le ré'
    +'pertoire d''installation d''OptGeo. Pour utiliser OptGeo sans fichier '
    +'de configuration, cliquez sur OK. Sinon, cliquez sur Abandon, rendez le '
    +'répertoire d''installation d''OptGeo accessible en écriture,puis '
    +'relancez OptGeo';
  rsErreurCritiq = 'Erreur critique !';
  rsOptGeoEstExC = 'OptGeo est exécuté pour la première fois, ou bien son '
    +'fichier de configuration est corrompu: vous allez maintenant le créer !';
  rsJeNeParviens = 'Je ne parviens pas à créer le répertoire exemples que '
    +'vous m''indiquez! Vérifiez que vous avez des droits suffisants !';
  rsMaisEuh = 'Mais euh !!';
  rsJeNeParviens2 = 'Je ne parviens pas à créer le répertoire de '
    +'simulations personnelles que vous m''indiquez! Vérifiez que vous avez '
    +'des droits suffisants !';
  rsJeNeParviens3 = 'Je ne parviens pas à créer le répertoire de '
    +'configuration personnelle que vous m''indiquez! Vérifiez que vous avez '
    +'des droits suffisants !';
  rsRepertoireDe = 'Repertoire des exemples introuvable et incréable!';
  rsRepertoireDe2 = 'Repertoire des simulations personnelles introuvable et '
    +'incréable !';
  rsRepertoireDe3 = 'Repertoire de configuration personnelle introuvable et '
    +'incréable. Vous ne pourrez pas sauver vos modifications '
    +'d''environnement !';
  rsPositionnezL = 'Positionnez l''extrémité du segment et cliquez...';
  rsPositionnezL2 = 'Positionnez l''extrémité du 1er segment et cliquez...';
  rsPositionnezL3 = 'Positionnez l''extrêmité du 2nd segment...';
  rsPositionnezL4 = 'Positionnez le point suivant et cliquez...';
  rsPointsSaisis = 'Points saisis aberrants !';
  rsAttention = 'Attention !';
  rsRelierLesPoi = 'Relier les points %s et %s par:';
  rsPositionnezL5 = 'Positionnez le sommet d''arc de cercle suivant...';
  rsPositionnezL6 = 'Positionnez le point 2 (direction du rayon) et cliquez...';
  rsPositionnezL7 = 'Positionnez le point D (direction du rayon) et cliquez...';
  rsPositionnezL8 = 'Positionnez le point 1 (rayon1 limitant le faisceau) et '
    +'cliquez...';
  rsPositionnezU = 'Positionnez un point de la surface et cliquez...';
  rsPositionnezL9 = 'Positionnez le sommet de la 2ème face...';
  rsPositionnezL10 = 'Positionnez le bord intérieur (point 1) et cliquez...';
  rsPositionnezL11 = 'Positionnez le point 2 (autre bord du miroir) et '
    +'cliquez...';
  rsPositionnezL12 = 'Positionnez l''autre extrémité du trait et cliquez...';
  rsPositionnezL13 = 'Positionnez le foyer de la conique et cliquez...';
  rsPositionnezL14 = 'Positionnez le point 2 (autre bord de l''écran) et '
    +'cliquez...';
  rsPositionnezL15 = 'Positionnez le point 2 (centre pupille) et cliquez...';
  rsPositionnezL16 = 'Positionnez le point 2 (autre bord de la lame) et '
    +'cliquez...';
  rsPositionnezL17 = 'Positionnez le point A (sommet du prisme) et cliquez...';
  rsPositionnezL18 = 'Positionnez le point 2 (autre bord de la lentille) et '
    +'cliquez...';
  rsSommetsTropP = 'Sommets trop proches !';
  rsAttention2 = 'Attention !!';
  rsPositionnezL19 = 'Positionnez le centre de courbure de la 1ère face puis '
    +'cliquez...';
  rsPositionnezL20 = 'Positionnez le centre de courbure de la 2ème face puis '
    +'cliquez...';
  rsSaisissezLeD = 'Saisissez le diamètre de la lentille...';
  rsDonnEsAberra = 'Données aberrantes!';
  rsRayonDeLaSph = 'Rayon de la sphère trop petit ou trop grand!';
  rsTailleDuMiro = 'Taille du miroir trop petite, ou débordant de l''écran !';
  rsTailleDuTrai = 'Taille du trait trop petite !';
  rsTailleDeLCra = 'Taille de l''écran trop petite !';
  rsTailleDeLOei = 'Taille de l''oeil trop petite !';
  rsTailleDeLaLa = 'Taille de la lame trop petite !';
  rsPointsTropPr = 'Points trop proches !';
  rsPositionnezL21 = 'Positionnez le point 2 (rayon 2 limitant le faisceau) '
    +'puis cliquez...';
  rsPositionnezL22 = 'Positionnez le point 2 (extrémité du plan d''onde) '
    +'puis cliquez...';
  rsNombreDeRayo = 'Nombre de rayons convergents vers cet objet virtuel';
  rsNombreDeRayo2 = 'Nombre de rayons émis par cette source ponctuelle';
  rsN = 'n';
  rsRayonIntRieu = 'Rayon intérieur du diaphragme trop petit !';
  rsPositionnezL23 = 'Positionnez le bord extérieur (point 2) et cliquez..';
  rsTailleDuMiro2 = 'Taille du miroir trop petite !';
  rsPositionnezL24 = 'Positionnez le foyer et cliquez.';
  rsPositionnezL25 = 'Positionnez le centre de courbure et cliquez.';
  rsPositionnezL26 = 'Positionnez le sommet de la conique et cliquez...';
  rsDonnEsDeCons = 'Données de construction de la conique aberrantes !';
  rsPourFinirPos = 'Pour finir, positionnez l''autre extrémité du miroir et '
    +'cliquez.';
  rsDonnEsDeCons2 = 'Données de construction du miroir aberrantes !';
  rsTailleDuPris = 'Taille du prisme trop petite !';
  rsDernierPoint = 'Dernier point pour l''angle au sommet...';
  rsTailleDeLaLe = 'Taille de la lentille trop petite !';
  rsPositionnezU2 = 'Positionnez un foyer et cliquez.';
  rsEpaisseurDuD = 'Epaisseur du diaphragme trop petite, ou diaphragme dé'
    +'bordant de l''écran !';
  rsTailleDuMiro3 = 'Taille du miroir trop petite ou focale trop courte, ou '
    +'débordant de l''écran !';
  rsTailleDuMiro4 = 'Taille du miroir trop petite ou focale trop courte, ou '
    +'débordant de l''écran !';
  rsTailleDuMiro5 = 'Taille du miroir trop petite ou focale trop courte, ou '
    +'miroir débordant de l''écran !';
  rsTailleDuMiro6 = 'Taille du miroir trop petite ou focale trop courte, ou '
    +'miroir débordant de l''écran !';
  rsCommentaires = 'Commentaires sur la simulation de titre non précisé...';
  rsValeurInacce = 'Valeur inacceptable pour le rayon !';
  rsValeurInacce2 = 'Valeur inacceptable pour x !';
  rsValeurInacce3 = 'Valeur inacceptable pour y !';
  rsValeurInacce4 = 'Valeur inacceptable pour indice rouge !';
  rsValeurInacce5 = 'Valeur inacceptable pour indice vert !';
  rsValeurInacce6 = 'Valeur inacceptable pour indice bleu !';
  rsPointsAberra = 'Points aberrants !';
  rsDPlacer = 'Déplacer';
  rsFinDPl = 'Fin dépl.';
  rsDupliquer = 'Dupliquer';
  rsFinDupl = 'Fin dupl.';
  rsPropriTS = 'Propriétés';
  rsFinProp = 'Fin prop.';
  rsSupprimer = 'Supprimer';
  rsFinSup = 'Fin sup.';
  rsGrouper = 'Grouper';
  rsImpossibleDe2 = 'Impossible de grouper moins de 2 éléments !';
  rsAttention3 = 'Attention';
  rsNombreMaxima = 'Nombre maximal de groupes deja atteint !';
  rsFinGroupemen = 'Fin groupement';
  rsFinDeLaCrAti = 'Fin de la création d''un groupe';
  rsCliquezSurLe = 'Cliquez sur les croix violettes des éléments à '
    +'grouper, puis sur Fin...';
  rsLeGroupeNVie = 'Le groupe n°%s vient d''être créé. Ses éléments '
    +'pourront être translatés ensemble.';
  rsFait = 'Fait !';
  rsGrouperDesLM = 'Grouper des éléments';
  rsModeAjoutDUn = 'Mode: ajout d''un rayon lumineux. Esc pour annuler.';
  rsModeAjoutDUn2 = 'Mode: ajout d''un miroir plan. Esc pour annuler.';
  rsModeAjoutDUn3 = 'Mode: ajout d''un trait ou d''une fléche. Esc pour '
    +'annuler.';
  rsModeAjoutDUn4 = 'Mode: ajout d''un miroir conique concave. Esc pour '
    +'annuler.';
  rsModeAjoutDUn5 = 'Mode: ajout d''un miroir conique convexe. Esc pour '
    +'annuler.';
  rsModeAjoutDUn6 = 'Mode: ajout d''un polyhèdre réfractant. Esc pour '
    +'annuler.';
  rsModeAjoutDUn7 = 'Mode: ajout d''un prisme isocèle. Esc pour annuler.';
  rsModeAjoutDUn8 = 'Mode: ajout d''une polysphère réfractante. Esc pour '
    +'annuler.';
  rsModeAjoutDUn9 = 'Mode: ajout d''un écran. Esc pour annuler.';
  rsModeAjoutDUn10 = 'Mode: ajout d''un oeil. Esc pour annuler.';
  rsModeAjoutDUn11 = 'Mode: ajout d''une lame semi-réfléchissante. Esc pour '
    +'annuler.';
  rsModeAjoutDUn12 = 'Mode: ajout d''un miroir sphérique concave. Esc pour '
    +'annuler.';
  rsModeAjoutDUn13 = 'Mode: ajout d''un miroir sphérique convexe. Esc pour '
    +'annuler.';
  rsModeAjoutDUn14 = 'Mode: ajout d''une lentille mince convergente. Esc pour '
    +'annuler.';
  rsModeAjoutDUn15 = 'Mode: ajout d''une lentille épaisse. Esc pour annuler.';
  rsModeAjoutDUn16 = 'Mode: ajout d''une sphère réfractante. Esc pour '
    +'annuler.';
  rsModeAjoutDUn17 = 'Mode: ajout d''une lentille mince divergente. Esc pour '
    +'annuler.';
  rsModeAjoutDUn18 = 'Mode: ajout d''un miroir sphérique concave. Esc pour '
    +'annuler.';
  rsModeAjoutDUn19 = 'Mode: ajout d''un miroir sphérique convexe. Esc pour '
    +'annuler.';
  rsModeAjoutDUn20 = 'Mode: ajout d''une source ponctuelle. Esc pour annuler.';
  rsModeAjoutDUn21 = 'Mode: ajout d''une onde plane';
  rsModeDPlaceme = 'Mode: déplacement d''un élément existant. Esc pour '
    +'annuler.';
  rsModePropriTS = 'Mode: propriétés d''un élément existant. Esc pour '
    +'annuler.';
  rsModeDuplicat = 'Mode: duplication d''un élément existant. Esc pour '
    +'annuler.';
  rsModeSuppress = 'Mode: suppression d''un élément existant. Esc pour '
    +'annuler.';
  rsModeMesureDe = 'Mode: mesure de distance. Esc pour annuler.';
  rsModeMesureDA = 'Mode: mesure d''angle. Esc pour annuler.';
  rsModeAjoutDUn22 = 'Mode: ajout d''un texte. Esc pour annuler.';
  rsModeAjoutDUn23 = 'Mode: ajout d''un diaphragme. Esc pour annuler.';
  rsModeAjoutDUn24 = 'Mode: ajout d''un groupe. Esc pour annuler. %sFin%s '
    +'pour achever le groupement.';
  rsDPlacerUnLMe = 'Déplacer un élément existant: translation, rotation, '
    +'changement focale ou rayon de courbure...';
  rsDupliquerUnL = 'Dupliquer un élément existant';
  rsGrouperDesLM2 = 'Grouper des éléments existants pour les translater '
    +'ensemble';
  rsPropriTSDUnL = 'Propriétés d''un élément existant';
  rsSupprimerUnL = 'Supprimer un élément existant';
  rsActions = 'Actions';
  rsPourDPlacerC = 'Pour déplacer cette palette, glissez-déposez à partir '
    +'de cette zone...';
  rsImprimer = 'Imprimer';
  rsImprimerLaSi = 'Imprimer la simulation';
  rsExportPNG = 'Export PNG';
  rsExporterLaSi = 'Exporter la simulation au format image PNG';
  rsExportJPG = 'Export JPG';
  rsExportDeLaSi = 'Export de la simulation au format image JPG';
  rsCopier = 'Copier';
  rsCopierSimula = 'Copier simulation dans le presse-papier';
  rsAfficherLaGr = 'Afficher la grille';
  rsAfficherLesA = 'Afficher les angles';
  rsAide = 'Aide';
  rsAideSurOptGe = 'Aide sur OptGeo';
  rsAjouter = 'Ajouter';
  rsAjusterAutom = 'Ajuster automatiquement à la plus petite taille';
  rsAPropos = 'A propos';
  rsAssocierLesF = 'Associer les fichiers .opt avec OptGeo';
  rsAttractionDe = 'Attraction de la grille';
  rsBaseDeRegist = 'Base de registre';
  rsMesurerUnAng = 'Mesurer un angle';
  rsAjouterUnMir = 'Ajouter un miroir  parabolique (ou elliptique, '
    +'hyperbolique) concave';
  rsAjouterUnMir2 = 'Ajouter un miroir parabolique (ou elliptique, '
    +'hyperbolique) convexe';
  rsAjouterUnDia = 'Ajouter un diaphragme à iris';
  rsMesurerLaDis = 'Mesurer la distance entre deux points';
  rsAjouterUnCra = 'Ajouter un écran';
  rsCacherDesSeg = 'Cacher des segments de rayon';
  rsAjouterUnTra = 'Ajouter un trait, une fléche...';
  rsAjouterUneLe = 'Ajouter une lentille épaisse';
  rsAjouterUneLe2 = 'Ajouter une lentille mince convergente paraxiale';
  rsAjouterUneLe3 = 'Ajouter une lentille mince divergente paraxiale';
  rsAjouterUneLa = 'Ajouter une lame semi-réfléchissante';
  rsAjouterUnMir3 = 'Ajouter un miroir plan';
  rsAjouterUnMir4 = 'Ajouter un miroir sphérique convexe paraxial';
  rsAjouterUnMir5 = 'Ajouter un miroir sphérique convexe réel';
  rsAjouterUnMir6 = 'Ajouter un miroir sphérique concave paraxial';
  rsAjouterUnMir7 = 'Ajouter un miroir sphérique concave réel';
  rsAjouterUnOei = 'Ajouter un oeil stylisé';
  rsAjouterUneOn = 'Ajouter une onde plane';
  rsAjouterUnPol = 'Ajouter un polygône réfractant';
  rsAjouterUnePo = 'Ajouter une polycercle réfractant';
  rsAjouterUnPri = 'Ajouter un prisme isocéle';
  rsAjouterUnRay = 'Ajouter un rayon';
  rsAjouterUnObj = 'Ajouter un objet ponctuel réel';
  rsAjouterUneSp = 'Ajouter une sphère réfractante';
  rsAjouterUnObj2 = 'Ajouter un objet ponctuel virtuel';
  rsA = 'A';
  rsAjouterUnTex = 'Ajouter un texte sur le dessin';
  rsOptGeo = 'OptGeo';
  rsAnglesDIncid = 'Angles d''incid. et de réfr.';
  rsAttraction = 'Attraction';
  rsGrille = 'Grille';
  rsNormales = 'Normales';
  rsFacteurDeZoo = 'Facteur de zoom';
  rsCommentaireS2 = 'Commentaire sur la simulation';
  rsContacterLAu = 'Contacter l''auteur';
  rsCopierSimula2 = 'Copier simulation dans le presse papier';
  rsCouleurDeFon = 'Couleur de fond ou image de fond';
  rsDFinirLeRPer = 'Définir le répertoire personnel';
  rsDiaphragme = 'Diaphragme';
  rsDivers = 'Divers';
  rsDPlacerUnLMe2 = 'Déplacer un élément';
  rsDSassocier = 'Désassocier';
  rsDuDernierRPe = 'du dernier répertoire de chargement autre que ceux ci-'
    +'dessus';
  rsDupliquerUnL2 = 'Dupliquer un élément';
  rsDeMonRPertoi = 'de mon répertoire personnel';
  rsDuRPertoireE = 'du répertoire %sexemples%s';
  rsEcran = 'Ecran';
  rsEditerLeComm = 'Editer le commentaire de la simulation';
  rsEdition = 'Edition';
  rsElMentsChois = 'Eléments à choisir';
  rsEmpennageDes = 'Empennage des rayons';
  rsEnregistrerS = 'Enregistrer simulation sous';
  rsEnregistrerS2 = 'Enregistrer simulation';
  rsFichier = 'Fichier';
  rsOptions = 'Options';
  rsJoindreParUn = 'Joindre par un arc de cercle';
  rsJoindreParUn2 = 'Joindre par un segment';
  rsLameSemiRFlC = 'Lame semi réfléchissante';
  rsLentille = 'Lentille';
  rsLentilleMinc = 'Lentille mince convergente';
  rsLentilleMinc2 = 'Lentille mince divergente';
  rsLicenseGPL = 'License GPL';
  rsManuellement = 'Manuellement';
  rsExporterGrap = 'Exporter graphe';
  rsPNG = 'PNG';
  rsJPG = 'JPG';
  rsLangue = 'Langue';
  rsPrisme = 'Prisme';
  rsMesureDAngle = 'Mesure d''angle';
  rsMesureDeDist = 'Mesure de distance';
  rsMiroirCNique = 'Miroir cônique';
  rsMiroirPlan = 'Miroir plan';
   rsreseau = 'Réseau';
  rsMiroirSphRiq = 'Miroir sphérique concave';
  rsMiroirSphRiq2 = 'Miroir sphérique convexe';
  rsModifierLesD = 'Modifier les dimensions de l''espace de travail';
  rsAfficherLesN = 'Afficher les normales';
  rsNouvelleSimu = 'Nouvelle simulation';
  rsOeilStylis = 'Oeil stylisé';
  rsOndePlane = 'Onde plane';
  rsOptionsDeLaG = 'Options de la grille';
  rsOptionsDesNo = 'Options des normales';
  rsOuvrirSimula = 'Ouvrir simulation';
  rsPolyhDreRFra = 'Polyhèdre réfractant';
  rsPolysphReRFr = 'Polysphère réfractante';
  rsPrFRences = 'Préférences';
  rsPropriTSDUnL2 = 'Propriétés d''un élément';
  rsQuitter = 'Quitter';
  rsRayonsFantom = 'Rayons fantomes';
  rsRayonUnique = 'Rayon unique';
  rsRefaire = 'Refaire';
  rsAnnuler = 'Annuler';
  rsSiteWeb = 'Site Web';
  rsSource = 'Source';
  rsObjetPonctue = 'Objet ponctuel réel ou virtuel';
  rsAjouterOuDit = 'Ajouter (ou éditer) le commentaire de la simulation';
  rsCoord = 'Coord.';
  rsAfficherLesC = 'Afficher les coordonnées des segments';
  rsSphReRFracta = 'Sphère réfractante';
  rsZoom = 'Zoom:';
  rsMiroirs = 'Miroirs:';
  rsSources = 'Sources:';
  rsDioptres = 'Dioptres:';
  rsEcrans = 'Ecrans:';
  rsDivers2 = 'Divers:';
  rsAfficher = 'Afficher:';
  rsSupprimerUnL2 = 'Supprimer un élément';
  rsSurfacesDOnd = 'Surfaces d''onde';
  rsSystMeOptiqu = 'Système optique idéal';
  rsSystMeOptiqu2 = 'Système optique réel';
  rsTexteSurLeDe = 'Texte sur le dessin';
  rsTousLesLMent = 'Tous les éléments';
  rsTraitFlChe = 'Trait, fléche...';
  rsOK = '&OK';
  rsSupprimerCet = 'Supprimer cette sphère';
  rsPropriTSDeCe = 'Propriétés de cette sphère';
  rs17 = '1,7';
  rs15 = '1,5';
  rs16 = '1,6';
  rs17b = '1.7';
  rs15b = '1.5';
  rs16b = '1.6';
  rsRayonDeCourb = 'Rayon de courbure';
  rsIndiceDeRFra = 'Indice de réfraction du milieu';
  rsCoordonnEsDu = 'Coordonnées du centre';
  rsCouleurLCran = 'Couleur à l''écran';
  rsLog1 = 'log1';
  rsLog2 = 'log2';
  rsLog3 = 'log3';
  rsRayonRFlChiS = 'Rayon réfléchi (sens entrant)';
  rsRayonRFlChiS2 = 'Rayon réfléchi (sens sortant)';
  rsR = 'R=';
  rsPourLeRouge = 'Pour le rouge:';
  rsPourLeVert = 'Pour le vert:';
  rsPourLeBleu = 'Pour le bleu:';
  rsX = 'x=';
  rsY = 'y=';
  rsMatRiau = 'Matériau:';
  rsDonnEsAberra2 = 'Données aberrantes !';
  rsCliquezPourA = 'Cliquez pour afficher les propriétés de cet élément...';
  rsCliquezPourD = 'Cliquez pour dupliquer cet élément...';
  rsPositionnezL27 = 'Positionnez le sommet de l''arc de cercle entre les '
    +'points %s et %s...';
  rsPositionnezL28 = 'Positionnez le sommet de l''arc de cercle entre les '
    +'points %s et 1...';
  rsVoulezVousLC = 'Voulez-vous l''écraser ?';
  rsCeFichierExi = 'Ce fichier existe déjà.';
  rsVoulezVousLC2 = 'Voulez-vous l''écraser ?';
  rsPourPrendreE = 'Pour prendre en compte le changement de langue de '
    +'l''interface, Optgeo doit maintenant être arrété. Puis relancez le.';
  rsChangementDe = 'Changement de langue';
  rsNombreMaxima2 = 'Nombre maximal de prismes deja atteint !';
  rsFichierDesIn = 'Fichier des indices de réfraction non trouvé !';
  rsEspaceDeTrav = 'Espace de travail: horiz: [%s,%s]; vert: [%s,%s]   ';
  rsLaSimulation = 'La simulation a été modifiée est les modifications '
    +'n''ont pas été sauvegardées. Voulez vous vraiment quitter OptGéo ?';
  rsAttention4 = 'Attention !!!';
  rsNombreMaxima3 = 'Nombre maximal de miroirs plans deja atteint !';
  rsPositionnezL29 = 'Positionnez le point 1 (premier bord du miroir) et '
    +'cliquez...';
  rsNombreMaxima4 = 'Nombre maximal de traits déjà atteint !';
  rsPositionnezL30 = 'Positionnez le point 1 (extrémité 1 du trait) et '
    +'cliquez...';
  rsNombreMaxima5 = 'Nombre maximal d''écran deja atteint !';
  rsPositionnezL31 = 'Positionnez le point 1 (premier bord de l''écran) et '
    +'cliquez...';
  rsNombreMaxima6 = 'Nombre maximal d''écran deja atteint !';
  rsPositionnezL32 = 'Positionnez le centre du diaphragme et cliquez...';
  rsNombreMaxima7 = 'Nombre maximal d''écran deja atteint !';
  rsPositionnezL33 = 'Positionnez le point 1 (premier bord de l''écran) et '
    +'cliquez...';
  rsX1 = 'x1';
  rsX2 = 'x2';
  rsX5 = 'x5';
  rsX10 = 'x10';
  rsX20 = 'x20';
  rsX50 = 'x50';
  rsX100 = 'x100';
  rsAjoutDUnMiro = 'Ajout d''un miroir plan:';
  rsCliquerSurLO = 'Cliquer sur l''origine du segment...';
  rsCliquerSurLe = 'Cliquer sur le centre (intersection des droites)...';
  rsNombreMaxima8 = 'Nombre maximal de rayons deja atteint !';
  rsPositionnezL34 = 'Positionnez le point 1 (origine du rayon) et cliquez...';
  rsNombreMaxima9 = 'Nombre maximal de spheres deja atteint !';
  rsPositionnezL35 = 'Positionnez le centre de la sphère et cliquez...';
  rsPositionnezL36 = 'Positionnez le centre d''inertie G du prisme et '
    +'cliquez...';
  rsNombreMaxima10 = 'Nombre maximal de polyhèdres deja atteint !';
  rsPositionnezL37 = 'Positionnez le point 1 du polyhèdre et cliquez...';
  rsSommetsDeCeP = 'Sommets de ce polyhèdre';
  rsNombreMaxima11 = 'Nombre maximal de miroirs sphériques convergents '
    +'paraxiaux deja atteint !';
  rsPositionnezL38 = 'Positionnez le point 1 (premier bord du miroir) et '
    +'cliquez...';
  rsNombreMaxima12 = 'Nombre maximal de miroirs sphériques convergents '
    +'paraxiaux deja atteint !';
  rsPositionnezL39 = 'Positionnez le point 1 (premier bord du miroir) et '
    +'cliquez...';
  rsNombreMaxima13 = 'Nombre maximal de miroirs sphériques convergents '
    +'paraxiaux deja atteint !';
  rsPositionnezL40 = 'Positionnez le point 1 (premier bord du miroir) et '
    +'cliquez...';
  rsNombreMaxima14 = 'Nombre maximal de miroirs sphériques convergents '
    +'paraxiaux deja atteint !';
  rsPositionnezL41 = 'Positionnez le point 1 (premier bord du miroir) et '
    +'cliquez...';
  rsNombreMaxima15 = 'Nombre maximal de lentilles mines convergentes '
    +'paraxiales deja atteint !';
  rsPositionnezL42 = 'Positionnez le point 1 (premier bord de la lentille) et '
    +'cliquez...';
  rsNombreMaxima16 = 'Nombre maximal de lentilles mines convergentes '
    +'paraxiales deja atteint !';
  rsPositionnezL43 = 'Positionnez le point 1 (premier bord de la lentille) et '
    +'cliquez...';
  rsNombreMaxima17 = 'Nombre maximal de sources ponctuelles deja atteint !';
  rsPositionnezL44 = 'Positionnez le point objet virtuel et cliquez...';
  rsPositionnezL45 = 'Positionnez le point objet réel et cliquez...';
  rsNombreMaxima18 = 'Nombre maximal d''ondes planes deja atteint !';
  rsPositionnezL46 = 'Positionnez le point 1 (origine d''un rayon et du plan '
    +'d''onde) et cliquez...';
  rsAjoutDUnRayo = 'Ajout d''un rayon:';
  rsNombreMaxima19 = 'Nombre maximal de miroirs  deja atteint !';
  rs = '''';
  rsAjoutDUnMiro2 = 'Ajout d''un miroir concave:';
  rsAjoutDUnMiro3 = 'Ajout d''un miroir convexe:';
  rsNombreMaxima20 = 'Nombre maximal de lentilles deja atteint !';
  rsAjoutDUneLen = 'Ajout d''une lentille convergente:';
  rsAjoutDUneLen2 = 'Ajout d''une lentille divergente:';
  rsNombreMaxima21 = 'Nombre maximal de lames deja atteint !';
  rsAjoutDUneLam = 'Ajout d''une lame semi-réfléchissante:';
  rsNombreMaxima22 = 'Nombre maximal de miroirs deja atteint !';
  rsAjoutDUnMiro4 = 'Ajout d''un miroir concave:';
  rsAjoutDUneLen3 = 'Ajout d''une lentille épaisse:';
  rsAjoutDUnMiro5 = 'Ajout d''un miroir convexe:';
  rsNombreMaxima23 = 'Nombre maximal de miroirs coniques deja atteint !';
  rsAjoutDUnMiro6 = 'Ajout d''un miroir cônique:';
  rsNombreMaxima24 = 'Nombre maximal d''écrans deja atteint !';
  rsAjoutDUnCran = 'Ajout d''un écran:';
  rsNombreMaxima25 = 'Nombre maximal de diaphragmes deja atteint !';
  rsAjoutDUnDiap = 'Ajout d''un diaphragme:';
  rsNombreMaxima26 = 'Nombre maximal de polysphères deja atteint !';
  rsSommetsDeCet = 'Sommets de cette polysphére';
  rsSommet = 'Sommet %s';
  rsNombreMaxima27 = 'Nombre maximal de polyhedres deja atteint !';
  rsNombreMaxima28 = 'Nombre maximal de source ponctuelle deja atteint !';
  rsAjoutDUneSou = 'Ajout d''une source ponctuelle:';
  rsNombreMaxima29 = 'Nombre maximal d''onde plane deja atteint !';
  rsAjoutDUneCet = 'Ajout d''une cette onde plane:';
  rsMm = 'mm';
  rsCommentaires3 = 'Commentaires sur la simulation de nom non précisé...';
  rsEspaceDeTrav2 = 'Espace de travail: horiz: [%s , %s]; vert: [%s , %s]   ';
  rsLaSimulation2 = 'La simulation a été modifiée et les modifications '
    +'n''ont pas été sauvegardées. Voulez vous vraiment commencer une '
    +'nouvelle simulation ?';
  rsImpossibleDe3 = 'Impossible de sauver le commentaire';
  rsErreurDCritu = 'Erreur d''écriture sur le disque';
  rsCommentaires4 = 'Commentaires sur la simulation %s';
  rsUnFichierPor = 'Un fichier portant ce nom existe déjà. Voulez-vous l''é'
    +'craser ?';
  rsEspaceDeTrav3 = 'Espace de travail: horiz: [%s , %s]; vert: [%s , %s] - '
    +'Simulation: %s';
  rsFichierInacc = 'Fichier inaccessible !';
  rsFichierInacc2 = 'Fichier inaccessible en lecture!';
  rsFichierEndom = 'Fichier endommagé!!!';
  rsErreur = 'Erreur !!!';
  rsCommentaires5 = 'Commentaires sur la simulation %sopt';
  rsEspaceDeTrav4 = 'Espace de travail: horiz: [%s , %s]; vert: [%s , %s] - '
    +'Simulation: %s';
  rsPositionnezL47 = 'Positionnez le premier bord du miroir (point 1) et '
    +'cliquez...';
  rsPositionnezL48 = 'Positionnez le premier bord du miroir (point 1) et '
    +'cliquez...';
  rsNombreMaxima30 = 'Nombre maximal de textes deja atteint !';
  rsPositionnezL49 = 'Positionnez le bord supérieur gauche du texte et '
    +'cliquez...';
  rsNombreMaxima31 = 'Nombre maximal de texte deja atteint !';
  rsAjoutDUnText = 'Ajout d''un texte:';
  rsNombreMaxima32 = 'Nombre maximal de lentilles épaisses deja atteint !';
  rsPositionnezL50 = 'Positionnez le sommet de la 1ère face...';
  rsLAffichageDe = 'L''affichage des angles impose le tracé des normales';
  rsPositionnezL51 = 'Positionnez le sommet 1 de la polysphère et cliquez...';
  rsSommetsDeCet2 = 'Sommets de cette polysphère';
  rsAjoutDUneMes = 'Ajout d''une mesure de distance:';
  rsAjoutDUneMes2 = 'Ajout d''une mesure d''angle:';
  rsAjoutDUnTrai = 'Ajout d''un trait...';
  rsPositionnezL52 = 'Positionnez le point 1 (rétine) et cliquez...';
  rsNombreMaxima33 = 'Nombre maximal d''yeux deja atteint !';
  rsNombreMaxima34 = 'Nombre maximal d''yeux deja atteint !';
  rsAjoutDUnOeil = 'Ajout d''un oeil stylisé:';
  rsDoisJeCrErLe = 'Dois je créer le groupe n°%s en y mettant tous les élé'
    +'ments présents ?';
  rsCrAtion = 'Création ?';
  rsUnAuMoinsDes = 'Un au moins des objets actuels ne rentre pas sur ce '
    +'nouvel espace de travail !';
  rsRedimensionn = 'Redimensionnement impossible';
  rsJeNeParviens4 = 'Je ne parviens pas à créer le répertoire de '
    +'simulations personnelles que vous m''indiquez! Vérifiez que vous avez '
    +'des droits suffisants !';
  rsSuivant = 'Suivant >';
  rsQueVoulezVou = 'Que voulez vous faire ?';
  rsVoulezVous = 'Voulez vous:';
  rsChoixDuSPara = 'Choix du séparateur décimal';
  rsChoisissezAu = 'Choisissez aussi éventuellement:';
  rsCrRUneNouvel = 'Créér une nouvelle simulation';
  rsCrRUneNouvel2 = 'Créér une nouvelle simulation en choisissant les '
    +'dimensions de l''espace de travail';
  rsOuvrirUneSim = 'Ouvrir une simulation existante';
  rsLaVirgule = 'La virgule: ,';
  rsLePoint = 'Le point: .';
  rsCoordonnEsDe = 'Coordonnées des segments';
  rsAProposDeCeL = 'A propos de ce logiciel';
  rsEnCliquantSu = 'En cliquant sur le bouton %sOK%s, vous déclarez accepter '
    +'les termes';
  rsDeLaLicenseG = 'de la license GNU GPL';
  rsOptGeoLogici = 'OptGeo, logiciel de simulation d''optique géométrique';
  rsDeuxDimensio = 'à deux dimensions,  libre et gratuit, sous license GPL.';
  rsVersion200Be = 'Version 2.25 du 05/01/2018, écrite en Lazarus';
  rsPourTouteRem = 'Pour toute remarque, cliquer sur:';
  rsVRifierQueVo =
    'Vérifier que vous avez la dernière version ? Cliquer sur:';
  rsSiteWebDeLAu = 'Site Web de l''auteur';
  rsModifierLEsp = 'Modifier l''espace de travail';
  rsRespecterRap = 'Respecter rapport hauteur/largeur de l''écran';
  rsAbscisseMin = 'Abscisse min.:';
  rsAbscisseMax = 'Abscisse max.:';
  rsOrdonnEMin = 'Ordonnée min.:';
  rsOrdonnEMax = 'Ordonnée max.:';
  rsPourAvoirUnR = 'pour avoir un repère approximativement orthonormé';
  rsExpressionIl = 'Expression illégale pour la l''abscisse minimale !';
  rsExpressionIl2 = 'Expression illégale pour la l''abscisse maximale !';
  rsExpressionIl3 = 'Expression illégale pour la l''ordonnée minimale !';
  rsExpressionIl4 = 'Expression illégale pour la l''ordonnée maximale !';
  rsXmaxDoitTreS = 'xmax doit être supérieur à xmin !';
  rsYmaxDoitTreS = 'ymax doit être supérieur à ymin !';
  rsSupprimerCeP = 'Supprimer ce polycercle';
  rsPropriTSDeCe2 = 'Propriétés de ce polycercle';
  rsSonRayonDeCo = 'Son rayon de courbure vaut:';
  rsCoordonnEsDe2 = 'Coordonnées de ce sommet';
  rsCouleurLCran2 = 'Couleur à l''écran';
  rsCeSommetEstR = 'Ce sommet est relié au suivant par:';
  rsCetArcDeCerc = 'En allant de Ai vers Ai+1, le sommet de l''arc de cercle est:';
  rsRi = 'Ri=';
  rsToujours = 'Toujours';
  rsJamais = 'Jamais';
  rsSeulementSiL = 'Seulement si le réfracté existe';
  rsSeulementSiL2 = 'Seulement si le réfracté n''existe pas';
  rsUnSegmentDeD = 'Un segment de droite';
  rsUnArcDeCercl = 'Un arc de cercle';
  rsRentrant = 'A gauche';
  rsSortant = 'A droite';
  rsSeulementSiL3 = 'Seulement si le réfracté n''existe pas';
  rsValeurInacce7 = 'Valeur inacceptable pour Ri !';
  rsOK2 = 'OK';
  rsSupprimerCeD = 'Supprimer ce diaphragme';
  rsAjoutDUnDiap2 = 'Ajout d''un diaphragme';
  rsCentreC = 'Centre C';
  rsRayonExtRieu = 'Rayon extérieur';
  rsCouleurDuDia = 'Couleur du diaphragme';
  rsAnglePolaire = 'Angle polaire du diaphragme';
  rsEpaisseurDuT = 'Epaisseur du trait';
  rsRayonIntRieu2 = 'Rayon intérieur';
  rsLog4 = 'log4';
  rsRad = 'rad';
  rsRext = 'Rext=';
  rsT = 't=';
  rsRint = 'Rint=';
  rsValeurInacce8 = 'Valeur inacceptable pour cx !';
  rsValeurInacce9 = 'Valeur inacceptable pour cy !';
  rsValeurInacce10 = 'Valeur inacceptable pour Rint !';
  rsValeurInacce11 = 'Valeur inacceptable pour Rext !';
  rsValeurInacce12 = 'Valeur inacceptable pour t !';
  rsSupprimerCet2 = 'Supprimer cet écran';
  rsAjoutDUnCran2 = 'Ajout d''un écran';
  rsCouleurLCran3 = 'Couleur à l''écran';
  rsEpaisseurTra = 'Epaisseur trait à l''écran';
  rsPoint1 = 'Point 1';
  rsPoint2 = 'Point 2';
  rsValeurInacce13 = 'Valeur inacceptable pour x1 !';
  rsValeurInacce14 = 'Valeur inacceptable pour x2 !';
  rsValeurInacce15 = 'Valeur inacceptable pour y1 !';
  rsValeurInacce16 = 'Valeur inacceptable pour y2 !';
  rsSupprimerCeT3 = 'Supprimer ce trait';
  rsAjoutDUnTrai2 = 'Ajout d''un trait, fléche...';
  rsTailleDeLaPr = 'Taille de la première extrémité';
  rsTailleDeLaSe = 'Taille de la seconde extrémité';
  rsCouleurDuTra = 'Couleur du trait';
  rsCoordonnEsPo = 'Coordonnées point 1';
  rsCoordonnEsPo2 = 'Coordonnées point 2';
  rsFormeDeLaPre = 'Forme de la première extrémité';
  rsFormeDeLaSec = 'Forme de la seconde extrémité';
  rsStyleDuTrait = 'Style du trait (valable si épaisseur=1)';
  rsEn11000DeLaD = 'en 1/1000 de la diagonale';
  rsDeLEspaceDeT = 'de l''espace de travail';
  rsDeLEspaceDeT2 = 'de l''espace de travail';
  rsNormale = 'Normale';
  rsFlChe = 'Fléche';
  rsTraitPerpend = 'Trait perpendiculaire';
  rsCercle = 'Cercle';
  rsCroix = 'Croix +';
  rsCroixX = 'Croix x';
  rsDisque = 'Disque';
  rsPlein = 'Plein';
  rsPointillS = 'Pointillés';
  rsTirS = 'Tirés';
  rsTirSPoints = 'Tirés - Points';
  rsTirSPointsPo = 'Tirés - Points - Points';
  rsTailleDuTrai2 = 'Taille du trait trop petite ou points hors de l''espace '
    +'de travail !';
  rsPropriTSDeLa = 'Propriétés de la grille';
  rsAfficher2 = 'Afficher';
  rsPasDeLaGrill = 'Pas de la grille';
  rsPasHorizonta = 'Pas horizontal=';
  rsPasVertical = 'Pas vertical=';
  rsValeurDuPasH = 'Valeur du pas horizontal illégale !';
  rsValeurDuPasH2 = 'Valeur du pas horizontal très faible ou très forte. '
    +'Etes-vous sûr ?';
  rsValeurDuPasV = 'Valeur du pas vertical illégale !';
  rsValeurDuPasV2 = 'Valeur du pas vertical très faible ou très forte. Etes-'
    +'vous sûr ?';
  rsSupprimerCet4 = 'Supprimer cette lame';
  rsAjoutDUneLam2 = 'Ajout d''une lame semi-réfléchissante';
  rsCouleurLCran4 = 'Couleur à l''écran';
  rsEpaisseurTra2 = 'Epaisseur trait à l''écran';
  rsSupprimerCet5 = 'Supprimer cette lentille';
  rsAjoutDUneLen4 = 'Ajout d''une lentille épaisse';
  rsCoordonnEsDe3 = 'Coordonnées des sommets des faces';
  rsRayonsDeCour = 'Rayons de courbure des faces';
  rsDiamTre = 'Diamètre';
  rsIndiceDeRFra2 = 'Indice de réfraction du verre';
  rsCouleurLCran5 = 'Couleur à l''écran';
  rsLog5 = 'log5';
  rsLog6 = 'log6';
  rsLog7 = 'log7';
  rsSommetS1 = 'Sommet S1';
  rsR2 = 'R2=';
  rsD = 'D=';
  rsSommetS2 = 'Sommet S2';
  rsSigneSiCentr = 'Signe + si centre de courbure Ci à gauche de Si';
  rsSurLAxeOrien = 'sur l''axe orienté de S1 vers S2, signe - sinon.';
  rsR1 = 'R1=';
  rsValeurInacce17 = 'Valeur inacceptable pour S1x !';
  rsValeurInacce18 = 'Valeur inacceptable pour S1y !';
  rsValeurInacce19 = 'Valeur inacceptable pour S2x !';
  rsValeurInacce20 = 'Valeur inacceptable pour S2y !';
  rsValeurInacce21 = 'Valeur inacceptable pour R1 !';
  rsValeurInacce22 = 'Valeur inacceptable pour R2 !';
  rsValeurInacce23 = 'Valeur inacceptable pour le diamètre !';
  rsTypeDeLentil = 'Type de lentille incompatible avec les valeurs proposé'
    +'es, ou lentille débordant de l''espace de travail !';
  rsAjoutDUneLen5 = 'Ajout d''une lentille mince convergente paraxiale';
  rsAxeOptique = 'Axe optique';
  rsCouleurDeLaL = 'Couleur de la lentille';
  rsEpaisseurTra3 = 'Epaisseur trait à l''écran';
  rsAspect = 'Aspect';
  rsFocaleValeur = 'Focale (valeur absolue)';
  rsCouleurDeLAx = 'Couleur de l''axe optique';
  rsF = '|f|=';
  rsValeurInacce24 = 'Valeur inacceptable pour la focale !';
  rsLaFocaleDoit = 'La focale doit être non nulle !';
  rsAjoutDUneLen6 = 'Ajout d''une lentille mince divergente paraxiale';
  rsEpaisseurTra4 = 'Epaisseur trait à l''écran';
  rsCouleurDeLAx2 = 'Couleur de l''axe optique';
  rsSupprimerCeM = 'Supprimer ce miroir';
  rsAjoutDUnMiro7 =
    'Ajout d''un miroir parabolique, elliptique ou hyperbolique';
  rsAxeFocal = 'Axe focal';
  rsConcave = 'Concave';
  rsHachures = 'Hachures';
  rsFoyerF = 'Foyer F';
  rsExcentricit = 'Excentricité';
  rsCouleurDuMir = 'Couleur du miroir';
  rsAnglePolaire2 = 'Angle polaire de l''axe focal';
  rsAnglesPolair = 'Angles polaires FMi (extrémités)';
  rsParamTre = 'Paramétre';
  rsCouleurDeLAx3 = 'Couleur de l''axe';
  rsAspectLCran = 'Aspect à l''écran';
  rsConcaveOuCon = 'Concave ou convexe ?';
  rsE = 'e=';
  rsT0 = 't0=';
  rsP = 'P=';
  rsT1 = 't1=';
  rsT2 = 't2=';
  rsValeurInacce25 = 'Valeur inacceptable pour fx !';
  rsValeurInacce26 = 'Valeur inacceptable pour fy !';
  rsValeurInacce27 = 'Valeur inacceptable pour e !';
  rsValeurInacce28 = 'Valeur inacceptable pour p !';
  rsValeurInacce29 = 'Valeur inacceptable pour t0 !';
  rsValeurInacce30 = 'Valeur inacceptable pour t1 !';
  rsValeurInacce31 = 'Valeur inacceptable pour t2 !';
  rsAjoutDUnMiro8 = 'Ajout d''un miroir plan';
  rsCouleurLCran6 = 'Couleur à l''écran';
  rsEpaisseurTra5 = 'Epaisseur trait à l''écran';
  rsAjoutDUnMiro9 = 'Ajout d''un miroir sphérique convexe paraxial';
  rsTrouer = 'Trouer';
  rsEpaisseurTra6 = 'Epaisseur trait à l''écran';
  rsFocale = 'Focale';
  rsCouleurDeLAx4 = 'Couleur de l''axe';
  rsTrouerLeMiro = 'Trouer le miroir en son centre';
  rsDiamTreDuTro = 'Diamètre du trou:';
  rsValeurInacce32 = 'Valeur inacceptable pour le diametre du trou !';
  rsValeurInacce33 = 'Valeur inacceptable pour le rayon de courbure !';
  rsLeRayonDeCou = 'Le rayon de courbure doit être non nul !';
  rsTailleDuMiro7 =
    'Taille du miroir trop petite, ou débordant de l''écran !';
  rsAjoutDUnMiro10 = 'Ajout d''un miroir sphérique convexe réel';
  rsObtu = 'Obtu';
  rsAxeDeSymTrie = 'Axe de symétrie';
  rsEpaisseurTra7 = 'Epaisseur trait à l''écran';
  rsCouleurDeLAx5 = 'Couleur de l''axe';
  rsAngleDOuvert = 'Angle d''ouverture';
  rsDiamTreAngul = 'Diamètre angulaire du trou:';
  rsVuDepuisLeCe = '(vu depuis le centre C)';
  rsAjoutDUnMiro11 = 'Ajout d''un miroir sphérique concave paraxial';
  rsEpaisseurTra8 = 'Epaisseur trait à l''écran';
  rsCouleurDeLAx6 = 'Couleur de l''axe';
  rsLaDistanceEn = 'La distance entre les points 1 et 2 doit être infé'
    +'rieure au double du rayon !';
  rsTailleDuMiro8 =
    'Taille du miroir trop petite, ou débordant de l''écran !';
  rsAjoutDUnMiro12 = 'Ajout d''un miroir sphérique concave réel';
  rsEpaisseurTra9 = 'Epaisseur trait à l''écran';
  rsCouleurDeLAx7 = 'Couleur de l''axe';
  rsAngleDOuvert2 = 'Angle d''ouverture';
  rsOptionsPourL =
    'Options pour le tracé des normales aux dioptres et miroirs';
  rsTracer = 'Tracer';
  rsTracerLesNor = 'Tracer les normales';
  rsEpaisseurDeL = 'Epaisseur de la trace';
  rsCouleurDeLaT = 'Couleur de la trace';
  rsLongueurTrac = 'Longueur trace (en % de la diagonale de l''espace de '
    +'travail)';
  rsAfficherVale = 'Afficher valeurs angles (en degrés)';
  rsNombreDeDCim = 'Nombre de décimales:';
  rsTailleDeLOei2 = 'Taille de l''oeil trop petite !';
  rsSupprimerCet6 = 'Supprimer cet oeil';
  rsAjoutDUnOeil2 = 'Ajout d''un oeil';
  rsCouleurDeLOe = 'Couleur de l''oeil';
  rsEpaisseurTra10 = 'Epaisseur trait à l''écran';
  rsValeurInacce34 = 'Valeur inacceptable pour Dx !';
  rsValeurInacce35 = 'Valeur inacceptable pour Dy !';
  rsValeursEntrE = 'Valeurs entrées aberrantes !';
  rsSupprimerCet7 = 'Supprimer cette onde plane';
  rsSaisieDUneOn = 'Saisie d''une onde plane';
  rsEpaisseurLCr = 'Epaisseur à l''écran';
  rsTracerEnPoin = 'Tracer en pointillé le prolongement des différents '
    +'segments des rayons';
  rsExtrMitRayon = 'Extrémité rayon 1 (point D)';
  rsNombreDeRayo3 = 'Nombre de rayons';
  rsPourChaqueRa = 'Pour chaque rayon, rayons enfants:';
  rsTracerDesSur = 'Tracer des surfaces d''onde issues de cette onde plane';
  rsNombreMaxDeS = 'Nombre max. de segments par rayon';
  rsCouleurDesRa = 'Couleur des rayons';
  rsListeDesSegm = 'Liste des segments concernés';
  rsEntreParenth = '(entre parenthéses et séparés par des virgules';
  rsEtSansEspace = 'et sans espaces); exemple: (1,4,10)';
  rsDOndeSParezC = 'd''onde (séparez ces valeurs par un unique espace; ne '
    +'mettez ni parenthèses ni virgule de séparation)';
  rsDonnerLesVal = 'Donner les valeurs de chemins optiques pour lesquels vous '
    +'voulez que soient tracées des surfaces';
  rsExemple12014 = '(exemple: 120 145  276)';
  rsRemarqueSiLa = 'Remarque: si la simulation comprend au moins une lentille '
    +'mince et/ou un miroir paraxial, le tracé  des ces surfaces';
  rsNePeutTreEtN = 'ne peut être et ne sera pas fait.';
  rsNombreMaxima35 = 'Nombre maximal:';
  rsVersLAvant = 'Vers l''avant:';
  rsVersLArriRe = 'Vers l''arrière:';
  rsRouge = 'Rouge';
  rsVert = 'Vert';
  rsBleu = 'Bleu';
  rsLumiReBlanch = 'Lumière blanche';
  rsSupprimerCeP2 = 'Supprimer ce prisme';
  rsAjoutDUnPris = 'Ajout d''un prisme';
  rsCentreDInert = 'Centre d''inertie';
  rsAngleAuSomme = 'Angle au sommet';
  rsAnglePolaire3 = 'Angle polaire de la hauteur principale';
  rsHauteur = 'Hauteur';
  rsCouleurDuPri = 'Couleur du prisme';
  rsA2 = 'A=';
  rsHauteur2 = 'Hauteur=';
  rsCouleur = 'Couleur:';
  rsSeulemenxtSi = 'Seulemenxt si le réfracté existe';
  rsSeulementSiL4 = 'Seulement si le réfracté n''existe pas';
  rsSeulementSiL5 = 'Seulement si le réfracté n''existe pas';
  rsValeurInacce36 = 'Valeur inacceptable pour gx !';
  rsValeurInacce37 = 'Valeur inacceptable pour gy !';
  rsValeurInacce38 = 'Valeur inacceptable pour l''angle au sommet !';
  rsValeurInacce39 = 'Valeur inacceptable pour l''angle polaire !';
  rsValeurInacce40 = 'Valeur inacceptable pour la hauteur!';
  rsPourLeTracDe = 'Pour le tracé des surfaces d''onde:';
  rsNombreDePoin =
    'Nombre de points calculés pour tracer une surface d''onde:';
  rsLesDeuxPoint = 'Les deux points doivent être distincts !';
  rsSupprimerCeR = 'Supprimer ce rayon';
  rsSaisieDUnRay = 'Saisie d''un rayon';
  rsOriginePoint = 'Origine (point 1)';
  rsExtrMitPoint = 'Extrémité (point 2)';
  rsEpaisseurLCr2 = 'Epaisseur à l''écran';
  rsTracerEnPoin2 = 'Tracer en pointillé le prolongement des différents '
    +'segments du rayon';
  rsRayonsEnfant = 'Rayons enfants';
  rsNombreMaxDeS2 = 'Nombre max. de segments';
  rsCouleurDuRay = 'Couleur du rayon';
  rsVersLArriRe2 = 'Vers l''arrière:';
  rsVersLAvant2 = 'Vers l''avant:';
  rsValeurInacce41 = 'Valeur inacceptable pour sx !';
  rsValeurInacce42 = 'Valeur inacceptable pour sy !';
  rsSupprimerCet8 = 'Supprimer cette source ponctuelle';
  rsSaisieDUneSo = 'Saisie d''une source ponctuelle';
  rsEpaisseurLCr3 = 'Epaisseur à l''écran';
  rsTracerEnPoin3 = 'Tracer en pointillé le prolongement des différents '
    +'segments des rayons';
  rsTracerDesSur2 = 'Tracer des surfaces d''onde issues de cette source '
    +'ponctuelle';
  rsNombreMaxDeS3 = 'Nombre max. de segment par rayon';
  rsCouleurDeLaS = 'Couleur de la source';
  rsTypeDeSource = 'Type de source';
  rsDonnerLesVal2 = 'Donner les valeurs de chemins optiques pour lesquels '
    +'vous voulez que soient tracées des surfaces';
  rsDOndeSParezC2 = 'd''onde (séparez ces valeurs par un unique espace; ne '
    +'mettez ni parenthèses ni virgule de séparation)';
  rsRemarqueSiLa2 = 'Remarque: si la simulation comprend au moins une '
    +'lentille mince et/ou un miroir paraxial, le tracé  des ces surfaces';
  rsVersLAvant3 = 'Vers l''avant:';
  rsVersLArriRe3 = 'Vers l''arrière:';
  rsSeulementSiL6 = 'Seulement si le réfracté n''existe pas';
  rsSeulementSiL7 = 'Seulement si le réfracté n''existe pas';
  rsRespecterLes = 'Respecter les angles à l''impression';
  rsPosition = 'Position';
  rsNombreDeCopi = 'Nombre de copies';
  rsTailleEnDeLa = 'Taille (en % de la page)';
  rsRapportHaute = 'Rapport hauteur/largeur';
  rsNombre = 'Nombre:';
  rsHorizontalem = 'Horizontalement';
  rsVerticalemen = 'Verticalement';
  rsOrientation = 'Orientation';
  rsConfiguratio = 'Configuration de la page pour l''impression';
  rsHorizontale = 'Horizontale';
  rsDroite = 'Droite';
  rsGauche = 'Gauche';
  rsCentr = 'Centré';
  rsVerticale = 'Verticale';
  rsHaut = 'Haut';
  rsBas = 'Bas';
  rsPortrait = 'Portrait';
  rsPaysage = 'Paysage';
  rsPropriTSDeLO = 'Propriétés de l''objet:';
  rsSupprimerCeT9 = 'Supprimer ce texte';
  rsSaisirLeText = 'Saisir le texte à afficher';
  rsTexte = 'Texte';
  rsCoordonnEsDu2 = 'Coordonnées du point supérieur gauche du texte';
  rsPolice = 'Police';
  rsValeurIncorr = 'Valeur incorrecte pour x';
  rsValeurIncorr2 = 'Valeur incorrecte pour y';
  rsValeursNonAu = 'Valeurs non autorisées pour x ou y';
  rsDimensionsDe = 'Dimensions de l''espace de travail';
  rsRespecterLeR = 'Respecter le rapport hauteur/largeur de l';
  rs0 = '0';
  rsAbscisseMaxi = 'Abscisse maximale';
  rsOrdonnEsMaxi = 'Ordonnées maximale';
  rsRepReOrthono = 'Repére orthonormé (approximativement)';
  rsUnitDeLongue = 'Unité de longueur';
  rsAbscisseMini = 'Abscisse minimale';
  rsOrdonnEMinim = 'Ordonnée minimale';
  rsRemarqueLeNo = 'Remarque: le non respect du rapport implique que';
  rsLesAnglesLCr = 'les angles à l''écran ne sont pas respectés.';
  rsRapport = 'Rapport=';
  rsRapportIdAlP = 'Rapport idéal (permettant d''occuper le maximum';
  rsDeSurfaceDCr = 'de surface d''écran avec repére orthonormé)=';
  rs2 = '        ';
  rs3 = '         ';
  rsKm = 'km';
  rsHm = 'hm';
  rsDam = 'dam';
  rsM = 'm';
  rsDm = 'dm';
  rsCm = 'cm';
  rsM2 = 'µm';
  rsNm = 'nm';
  rsPm = 'pm';
  rsExpressionIl5 = 'Expression illégale pour la l''abcisse minimale !';
  rsExpressionIl6 = 'Expression illégale pour la l''ordonnée minimale !';
  rsExpressionIl7 = 'Expression illégale pour la l''abcisse maximale !';
  rsExpressionIl8 = 'Expression illégale pour la l''ordonnée maximale !';
  rsValeurNulleO = 'Valeur nulle ou négative interdite pour la hauteur et/ou '
    +'la largeur !';
  rsRespecterLeR2 = 'Respecter le rapport hauteur/largeur de l''écran';
  rsCeciNEstPasU = 'Ceci n''est pas une valeur numérique acceptable !';
  rsSaissezLeDia = 'Saissez le diamètre de la lentille';
  rsSaisissez = 'Saisissez';
  rsDiamTre2 = 'Diamètre=';
  rsPointsTropPr2 = 'Points trop proches ou hors de l''espace de travail !';
  rsSupprimerCet10 = 'Supprimer cette distance';
  rsModification = 'Modification d''une mesure de distance';
  rsCoordonnEsDu3 = 'Coordonnées du 1er point';
  rsCoordonnEsDu4 = 'Coordonnées du 2nd point';
  rsPointsTropPr3 = 'Points trop proches ou hors de l''espace de travail !';
  rsSupprimerCet11 = 'Supprimer cet angle';
  rsModification2 = 'Modification d''une mesure d''angle';
  rsCoordonnEsDe4 = 'Coordonnées de C';
  rsRelierLesPoi2 = 'Relier les points';
  rsJoindreLesPo = 'Joindre les points par:';
  rsRPertoireDIn = 'Répertoire d''installation d''OptGeo';
  rsRPertoireDes = 'Répertoire des exemples';
  rsRPertoireDev = 'Répertoire devant recevoir vos simulations personnelles';
  rsRPertoireDev2 = 'Répertoire devant contenir votre configuration '
    +'personnelle d''OptGeo';
  rsConfiguratio2 = 'Configuration primaire d''OptGeo';
  rsMiroirSphRiq3 = 'Miroir sphérique concave paraxial';
  rsMiroirSphRiq4 = 'Miroir sphérique convexe paraxial';
  rsMiroirSphRiq5 = 'Miroir sphérique concave réel';
  rsMiroirSphRiq6 = 'Miroir sphérique convexe réel';
  rsLentilleMinc3 = 'Lentille mince convergente idéale';
  rsLentilleMinc4 = 'Lentille mince divergente idéale';
  rsLentillePais = 'Lentille épaisse';
  rsPolycercleRF = 'Polycercle réfractant';
  rsSourcePonctu = 'Source ponctuelle';
  rsLameSemiRFlC2 = 'Lame semi-réfléchissante';
  rsOeil = 'Oeil';
  rsNom = 'Nom: %s';
  rsTaille = 'Taille: %s';
  rsSoulign = 'Souligné';
  rsGras = 'Gras';
  rsItalique = 'Italique';
  rsBarr = 'Barré';
  rsOkEtEnregist = '&Ok et enregistrer sur disque';
  rsRTablirLesVa = '&Rétablir les valeurs d''origine de tous les éléments';
  rsModifier = 'Modifier';
  rsPrFRencesPro = 'Préférences: propriétés par défaut à la création';
  rsTypeDLMent = 'Type d''élément';
  rsCouleurParDF = 'Couleur par défaut';
  rsEpaisseurPar = 'Epaisseur par défaut';
  rsCouleurAxePa = 'Couleur axe par défaut';
  rsPoliceParDFa = 'Police par défaut';
  rsIndiquerIciL = 'Indiquer ici les propriétés communes à appliquer à '
    +'tous les éléments:';
  rsCouleurRayon = 'Couleur rayon par défaut';
  rsElementParLM = 'Element par élément';
  rsPourTousLesL = 'Pour tous les éléments';
  rsImpossibleDC = 'Impossible d''écrire le fichier de configuration %s. La '
    +'configuration ne sera pas sauvegardée.';
  rsHLas = 'Hélas...';
  rsCouleur2 = 'Couleur';
  rsBlanc = 'Blanc';
  rsPropriTSDeLE = 'Propriétés de l''empennage des rayons';
  rsEn1000MeDeLa = '(en 1000éme de la taille du segment)';
  rsPositionDeLE = 'Position de l''empennage sur le segment:';
  rsTailleDeLEmp = 'Taille de l''empennage:';
  rsEn1000MeDeLa2 = '(en 1000éme de la diagonale de l''espace de travail)';
  rsPropriTSDesP = 'Propriétés des points';
  rsRepRer = 'Repérer';
  rsTailleDesPoi = 'Taille des points';
  rsCouleurDesPo = 'Couleur des points';
  rsPointCourant = 'Point courant';
  rsGrilleDeRepR = 'Grille de repérage';
  rsCroixDroite = 'Croix droite';
  rsCroixOblique = 'Croix  oblique';
  rsStyleDesPoin = 'Style des points';
  rsEditeurDeTex = 'Editeur de texte';
  rsNombreDeRayo4 = 'Nombre de rayons émis par cette source ponctuelle:';
  rsSupprimerCeP3 = 'Supprimer ce polygône';
  rsPropriTSDeCe3 = 'Propriétés de ce polygône';
  rsCouleurLCran7 = 'Couleur à l''écran';
  rsSeulementSiL8 = 'Seulement si le réfracté n''existe pas';
  rsSeulementSiL9 = 'Seulement si le réfracté n''existe pas';
  rsNombreDeSomm = 'Nombre de sommets';
  rsCeTexte = 'ce texte';
  rsCetteSphRe = 'cette sphère';
  rsCePrisme = 'ce prisme';
  rsCeTrait = 'ce trait';
  rsCetOeil = 'cet oeil';
  rsCeMiroirPlan = 'ce miroir plan';
  rsCeMiroirConi = 'ce miroir conique';
  rsCePolygone = 'ce polygone';
  rsCePolycercle = 'ce polycercle';
  rsCetCran = 'cet écran';
  rsCeDiaphragme = 'ce diaphragme';
  rsCetteLameSem = 'cette lame semi-réfléchissante';
  rsCeMiroirConc = 'ce miroir concave paraxial';
  rsCeMiroirConv = 'ce miroir convexe paraxial';
  rsCeMiroirSphR = 'ce miroir sphérique concave';
  rsCeMiroirSphR2 = 'ce miroir sphérique convexe';
  rsCetteLentill = 'cette lentille mince convergente';
  rsCetteLentill2 = 'cette lentille épaisse';
  rsCetteLentill3 = 'cette lentille mince divergente';
  rsCeRayonLumin = 'ce rayon lumineux';
  rsCetteSourceP = 'cette source ponctuelle';
  rsCetteOndePla = 'cette onde plane';
  rsCetAngle = 'cet angle';
  rsCetteMesureD = 'cette mesure de distance';
  rsFaitesPasser = 'Faites passer la souris sur une des croix violettes, ou '
    +'cliquez sur Fin...';
  rsClicGauchePo = 'Clic gauche pour ajouter %s au groupe...';
  rsFaitesPasser2 = 'Faites passer la souris sur un des éléments colorés...';
  rsClicGauchePo2 = 'Clic gauche pour sélectionner l''élément...';
  rsClicGauchePo3 = 'Clic gauche pour sélectionner l''élément...';
  rsClicGauchePo4 = 'Clic gauche pour sélectionner ce centre...';
  rsClicGauchePo5 = 'Clic gauche pour sélectionner ce foyer...';
  rsClicGauchePo6 = 'Clic gauche pour sélectionner ce bord de trou...';
  rsClicGauchePo7 = 'Clic gauche pour sélectionner ce sommet de prisme...';
  rsClicGauchePo8 = 'Clic gauche pour sélectionner ce sommet d''arc de '
    +'cercle...';
  rsCeMiroirCNiq = 'ce miroir cônique';
  rsCePolygNe = 'ce polygône';
  rsCeGroupe = 'ce groupe';
  rsCeMiroirSphR3 = 'ce miroir sphèrique';
  rsCetteLentill4 = 'cette lentille';
  rsCeRayon = 'ce rayon';
  rsCetteMesureD2 = 'cette mesure d''angle';
  rsCetteMesureD3 = 'cette mesure d''angle';
  rsCetteMesureD4 = 'cette mesure d''angle';
  rsFaitesPasser3 = 'Faites passer la souris sur un des cercles rouges...';
  rsClicGauchePo9 = 'Clic gauche pour supprimer %s...';
  rsChoixDeLObje = 'Choix de l''objet:';
  rsMesureDeDist2 = 'Mesure de distance n° %s';
  rsPrismeN = 'Prisme n° %s';
  rsMiroirPlanN = 'Miroir plan n° %s';
   rsReseauN = 'Réseau n° %s';
  rsMiroirConcav = 'Miroir concave idéal n° %s';
  rsMiroirConvex = 'Miroir convexe idéal n° %s';
  rsMiroirConcav2 = 'Miroir concave  n° %s';
  rsMiroirConvex2 = 'Miroir convexe n° %s';
  rsLentilleMinc5 = 'Lentille mince convergente n° %s';
  rsLentilleMinc6 = 'Lentille mince divergente n° %s';
  rsRayonN = 'Rayon n° %s';
  rsSourcePonctu2 = 'Source ponctuelle n° %s';
  rsOndePlaneN = 'Onde plane n° %s';
  rsEcranN = 'Ecran n° %s';
  rsLameSemiRFlC3 = 'Lame semi réfléchissante n° %s';
  rsPolyhDreRFra2 = 'Polyhèdre réfractant n° %s';
  rsSphReRFracta2 = 'Sphère réfractante n° %s';
  rsMiroirCNique2 = 'Miroir cônique n° %s';
  rsTexteN = 'Texte n° %s';
  rsDiaphragmeN = 'Diaphragme n° %s';
  rsLentillePais2 = 'Lentille épaisse n° %s';
  rsMesureDAngle2 = 'Mesure d''angle n° %s';
  rsPolycercleRF2 = 'Polycercle réfractant n° %s';
  rsFlCheN = 'Fléche n° %s';
  rsOeilN = 'Oeil n° %s';
  rsNAntN = 'Néant n° %s';
  rsGroupeN = 'Groupe n° %s';
  rsDPlacerLLMen = 'Déplacer l''élément, puis clic gauche pour le poser...';
  rsModeTranslat = 'Mode: translation d''un élément existant. Esc pour '
    +'annuler.';
  rsDPlacerLeBor = 'Déplacer le bord, puis clic gauche pour le poser...';
  rsModeChangeme = 'Mode: changement de taille du trou. Esc pour annuler.';
  rsDPlacerLeSom = 'Déplacer le sommet, puis clic gauche pour le poser...';
  rsModeChangeme2 = 'Mode: changement d''orientation du prisme. Esc pour '
    +'annuler.';
  rsModeChangeme3 = 'Mode: changement de la hauteur du prisme. Esc pour '
    +'annuler.';
  rsModeChangeme4 = 'Mode: changement de l''angle au sommet du prisme. Esc '
    +'pour annuler.';
  rsDPlacerLeFoy = 'Déplacer le foyer, puis clic gauche pour le poser...';
  rsModeChangeme5 = 'Mode: changement focale d''un élément existant. Esc '
    +'pour annuler.';
  rsDPlacerLeCen = 'Déplacer le centre, puis clic gauche pour le poser...';
  rsModeChgtRayo = 'Mode: chgt rayon courbure d''un élément existant. Esc '
    +'pour annuler.';
  rsModeChgtRayo2 = 'Mode: chgt rayon courbure d''un élément existant. Esc '
    +'pour annuler.';
  rsDPlacerLLMen2 = 'Déplacer l''élément, puis clic gauche pour le poser...';
  rsModeRotation = 'Mode: rotation d''un élément existant. Esc pour annuler.';
  rsFaitesPasser4 = 'Faites passer la souris sur un point cerclé...';
  rsModeDPlaceme2 = 'Mode: déplacement d''un élément existant. Esc pour '
    +'annuler.';
  rsCetteMesureD5 = 'cette mesure d''angle';
  rsCetteMesureD6 = 'cette mesure d''angle';
  rsCetteMesureD7 = 'cette mesure d''angle';
  rsCeMiroirSphR4 = 'ce miroir sphérique';
  rsChoisissezLO = 'Choisissez l''objet à supprimer:';
  rsMesureDAngle3 = 'Mesure d''angle n° %s';
  rsEtesVousSurD = 'Etes-vous sur de vouloir supprimer %s ?';
  rsMiroirConiqu = 'Miroir conique';
  rsSphRe = 'Sphère';
  rsMesureDAngle4 = 'Mesure d''angle';
  rsTrait = 'Trait';
  rsPolygone = 'Polygone';
  rsPolycercle = 'Polycercle';
  rsLameSemiRFle = 'Lame semi-réflechissante';
  rsMiroirConcav3 = 'Miroir concave paraxial';
  rsMiroirConvex3 = 'Miroir convexe paraxial';
  rsRayon = 'Rayon';
  rsAjoutAuGroup = '%s ajouté au groupe n°%s en cours de constitution.';
  rsPropriTSDeLO2 = 'Propriétés de l''objet:';
  rsObjetDupliqu = 'Objet à dupliquer:';
  rsMesureDAngle5 = 'Mesure d''angle n° %s';
  rsPropriTSDeCe4 = 'Propriétés de ce trait...';
  rsPropriTSDeCe5 = 'Propriétés de cette mesure d''angle:';
  rsPropriTSDeCe6 = 'Propriétés de ce texte:';
  rsPropriTSDeCe7 = 'Propriétés de ce miroir plan:';
  rsPropriTSDeCe8 = 'Propriétés de cet écran:';
  rsPropriTSDeCe9 = 'Propriétés de cet oeil stylisé:';
  rsPropriTSDeCe10 = 'Propriétés de ce diaphragme:';
  rsPropriTSDeCe11 = 'Propriétés de cette lame semi-réfléchissante:';
  rsPropriTSDeCe12 = 'Propriétés de ce rayon:';
  rsPropriTSDeCe13 = 'Propriétés de cette source ponctuelle:';
  rsPropriTSDeCe14 = 'Propriétés de cette onde plane:';
  rsPropriTSDeCe15 = 'Propriétés de ce miroir elliptique:';
  rsPropriTSDeCe16 = 'Propriétés de ce miroir hyperbolique:';
  rsPropriTSDeCe17 = 'Propriétés de ce miroir parabolique:';
  rsPropriTSDeCe18 = 'Propriétés de ce miroir concave:';
  rsPropriTSDeCe19 = 'Propriétés de ce miroir convexe:';
  rsPropriTSDeCe20 = 'Propriétés de cette lentille convergente:';
  rsPropriTSDeCe21 = 'Propriétés de cette lentille épaisse:';
  rsPropriTSDeCe22 = 'Propriétés de cette lentille divergente:';
  rsModeDPlaceme3 = 'Mode: déplacement d''un élément existant. Esc pour '
    +'annuler.';
  rsGr = 'Gr';
  rsPropriTSDeCe23 = 'Propriétés de cette mesure de distance:';
  rsModeDPlaceme4 = 'Mode: déplacement d''un élément existant. Esc pour '
    +'annuler.';
  rsLeGroupeNQui = 'Le groupe n°%s qui contenait cet élément va être lui '
    +'aussi détruit.';
  rsVotReAttenti = 'Votre attention siouplait !';
  rsLeGroupeNQui2 = 'Le groupe n°%s qui contenait cet élément va être lui '
    +'aussi détruit.';
  rsLeGroupeNQui3 = 'Le groupe n°%s qui contenait cet élément va être lui '
    +'aussi détruit.';
  rsLeGroupeNQui4 = 'Le groupe n°%s qui contenait cet élément va être lui '
    +'aussi détruit.';
  rsLeGroupeNQui5 = 'Le groupe n°%s qui contenait cet élément va être lui '
    +'aussi détruit.';
  rsLeGroupeNQui6 = 'Le groupe n°%s qui contenait cet élément va être lui '
    +'aussi détruit.';
  rsLeGroupeNQui7 = 'Le groupe n°%s qui contenait cet élément va être lui '
    +'aussi détruit.';
  rsLeGroupeNQui8 = 'Le groupe n°%s qui contenait cet élément va être lui '
    +'aussi détruit.';
  rsLeGroupeNQui9 = 'Le groupe n°%s qui contenait cet élément va être lui '
    +'aussi détruit.';
  rsLeGroupeNQui10 = 'Le groupe n°%s qui contenait cet élément va être '
    +'lui aussi détruit.';
  rsLeGroupeNQui11 = 'Le groupe n°%s qui contenait cet élément va être '
    +'lui aussi détruit.';
  rsLeGroupeNQui12 = 'Le groupe n°%s qui contenait cet élément va être '
    +'lui aussi détruit.';
  rsLeGroupeNQui13 = 'Le groupe n°%s qui contenait cet élément va être '
    +'lui aussi détruit.';
  rsLeGroupeNQui14 = 'Le groupe n°%s qui contenait cet élément va être '
    +'lui aussi détruit.';
  rsLeGroupeNQui15 = 'Le groupe n°%s qui contenait cet élément va être '
    +'lui aussi détruit.';
  rsLeGroupeNQui16 = 'Le groupe n°%s qui contenait cet élément va être '
    +'lui aussi détruit.';
  rsLeGroupeNQui17 = 'Le groupe n°%s qui contenait cet élément va être '
    +'lui aussi détruit.';
  rsLeGroupeNQui18 = 'Le groupe n°%s qui contenait cet élément va être '
    +'lui aussi détruit.';
  rsLeGroupeNQui19 = 'Le groupe n°%s qui contenait cet élément va être '
    +'lui aussi détruit.';
  rsLeGroupeNQui20 = 'Le groupe n°%s qui contenait cet élément va être '
    +'lui aussi détruit.';
  rsLeGroupeNQui21 = 'Le groupe n°%s qui contenait cet élément va être '
    +'lui aussi détruit.';
  rsLeGroupeNQui22 = 'Le groupe n°%s qui contenait cet élément va être '
    +'lui aussi détruit.';
  rsLeGroupeNQui23 = 'Le groupe n°%s qui contenait cet élément va être '
    +'lui aussi détruit.';
  rsLeGroupeNQui24 = 'Le groupe n°%s qui contenait cet élément va être '
    +'lui aussi détruit.';
  rsdefaire='Défaire';
  rsValeurIllGal = 'Valeur illégale de l''indice vert par défaut; remplacé '
    +'par 1';
  rsValeurIllGal2 = 'Valeur illégale de l''indice bleu par défaut; '
    +'remplacé par 1';
  rsValeurIllGal3 = 'Valeur illégale de l''indice rouge par défaut; '
    +'remplacé par 1';
  rsVousPouvezCh = 'Vous pouvez changer les indices du milieu dans lequel '
    +'s''effectue la simulation:';
  rsIndicesDuMil = 'Indices du milieu ambiant';
  rsExportPS = 'Export PS';
  rsExporterLaSi2 = 'Exporter la simulation au format Poscscript';
  rsPropriTSDuFi = 'Propriétés du fichier Postscript';
  rsTaillePage = 'Taille page';
  rsA4 = 'A4';
  rsA3 = 'A3';
  rsA22 = 'A2';
  rsA1 = 'A1';
  rsA0 = 'A0';
  rsA5 = 'A5';
  rsLetter = 'Letter';
  rsLegal = 'Legal';
  rsLedger = 'Ledger';
  rsRSolutions = 'Résolutions';
  rsHorizontale2 = 'Horizontale:';
  rsVerticale2 = 'Verticale:';
  rsPasDeSimulat = 'Pas de simulation en cours...';
  rsHLas2 = 'Hélas';
  rsUnNouveauGro = 'Un nouveau groupe contenant les éléments ajoutés vien '
    +'d''être créé.';
  rsImportationR = 'Importation réussie';
  rsLesParamTres = 'Les paramètres de la simulation en cours de chargement '
    +'doivent-ils écraser les actuels ?';
  rsUnNouveauGro2 = 'Un nouveau groupe contenant tous les éléments vient '
    +'d''être créé.';
  rsCrAtionRUssi = 'Création réussie.';
  rsGrouperTout = 'Grouper tout';
  rsCrErUnGroupe = 'Créer un groupe contenant tous les éléments';
  rsDuDernierRPe2 = 'du dernier répertoire de chargement autre que ceux ci-'
    +'dessus';
  rsSuperposerSi = 'Superposer simulation';
  rsCopyrightJea = 'Copyright Jean-Marie Biansan, 2004, 2018';
  rsJeanmarieBia = 'jeanmarie.biansan@free.fr';
  rsSplashscreen = 'Splashscreen';
  rsOptgeo2 = 'Optgeo';
  rsLogicielLibr = 'Logiciel libre et gratuit de simulation d''optique géomé'
    +'trique plane';
  rsVersion205Du = 'Version 2.25 du 05/01/2018';
  rsAuteurJeanMa = 'Auteur: Jean-Marie Biansan';
  rsCetteStructu = 'Cette structure serait un segment...';
  rsImpossible = 'Impossible !';
  rsPourCrErUneL = 'Pour créer une lentille autre que plan-convexe, utilisez '
    +'l''objet lentille épaisse...';
  rsPourCrErUneL2 = 'Pour créer une lentille autre que plan-convexe, '
    +'utilisez l''objet lentille épaisse...';
  rsDeuxSommetsC = 'Deux sommets consécutifs confondus...';
  rsCeReSeau = 'ce réseau';
  rsSupprimerCeR2 = 'Supprimer ce réseau';
  rsAjoutDUnRSea = 'Ajout d''un réseau';
  rsTypeDeRSeau = 'Type de réseau';
  rsEnTransmissi = 'En transmission';
  rsEnRFlexion = 'En réflexion';
  rsNombreDeTrai = 'Nombre de traits par mm';
  rsHachuresSiRS = 'Hachures si réseau en réflexion';
  rsOrdresPrendr = 'Ordres à prendre en compte';
  rsOrdreMinimum = 'Ordre minimum';
  rsOrdreMaximum = 'Ordre maximum';
  rsTailleDuRSea = 'Taille du réseau trop petite !';
  rsPropriTSDeCe24 = 'Propriétés de ce réseau';
  rsRSeau = 'Réseau';
  rsNombreMaxima36 = 'Nombre maximal de réseau déja atteint !';
  rsPositionnezL53 = 'Positionnez le point 1 (premier bord du réseau) et '
    +'cliquez...';
  rsNombreMaxima37 = 'Nombre maximal de réseaux déja atteint !';
  rsRSeauDiffrac = 'Réseau diffractant';
  rsAjouterUnRSe = 'Ajouter un réseau diffractant';
  rsPositionnezL54 = 'Positionnez le point 2 (autre bord du réseau) et '
    +'cliquez...';
  rsTailleDuRSea2 = 'Taille du réseau trop petite, ou débordant de l''écran'
    +' !';
  rsModeAjoutDUn25 = 'Mode: ajout d''un réseau diffractant. Esc pour annuler.';
  rsLongueurDOnd = 'Longueur d''onde pour le vert';
  rsLongueurDOnd2 = 'Longueur d''onde pour le rouge';
  rsLongueurDOnd3 = 'Longueur d''onde pour le bleu';
  rsLesValeursDe = 'Les valeurs de longueur d''onde sont utilisées pour les '
    +'réseaux de diffraction';
  rsIndicesDuMil2 = 'Indices du milieu ambiant et longueurs d''onde';
  rsIndicesDuMil3 = 'Indices du milieu ambiant et longueurs d''onde';
  rsRemarqueImpo = 'Remarque importante: ce réseau diffractant doit être '
    +'éclairé par une onde plane.Un rayon unique sera diffracté selon la '
    +'loi des réseaux sin i=+-sin i0+k.lambda/amais cela n''a pas de sens '
    +'pour un rayon unique.';
  rsAjoutDUnRSea2 = 'Ajout d''un réseau diffractant';
  rsLesLongueurs = 'Les longueurs d''onde peuvent être modifiées dans le '
    +'menu options, indices du milieuambiant et longueurs d''onde.';
  rsChoixDuFond = 'Choix du fond';
  rsChoisissez = 'Choisissez:';
  rsCouleurUnie = 'Couleur unie';
  rsImageDeFond = 'Image de fond';

implementation

end.

