{  This file is part of OptGeo, optical simulation software
    Copyright (C) 2004  Jean-Marie Biansan
        Contact: jeanmarie.biansan@free.fr
    Web site: http://jeanmarie.biansan.free.fr/logiciel.html
 From version 1.25, OptGeo is distribued under the terms of the
 GNU General Public License.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA}

    unit Unit10;

{$mode objfpc}{$H+}

interface

uses
   LCLIntf, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, Buttons, Spin,ColorBox, LResources,LCLType,UnitScaleFont,
   UChaines;

type

  { Tsaisietexte2 }

  Tsaisietexte2 = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    FontDialog1: TFontDialog;
    GroupBox1: TGroupBox;
    Edit1: TEdit;
    SpeedButton1: TSpeedButton;
    GroupBox2: TGroupBox;
    StaticText1: TLabel;
    editx: TEdit;
    StaticText2: TLabel;
    edity: TEdit;
    boutonsup: TBitBtn;
    log1: TLabel;
    log2: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private  encreation:boolean;
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

var
  saisietexte2: Tsaisietexte2;
   fontetexte2:tfont;
   texteaafficher2:string;
   textex,textey:extended;
implementation

uses unit222;


procedure Tsaisietexte2.SpeedButton1Click(Sender: TObject);
begin
 
if fontdialog1.Execute then
fontetexte2:=fontdialog1.Font;
end;

procedure Tsaisietexte2.FormShow(Sender: TObject);
begin
 //if encreation then begin scalefont(self); encreation:=false; end;
end;

procedure Tsaisietexte2.FormCreate(Sender: TObject);
begin
   encreation:=true;

BITBTN1.CAPTION
// msgctxt ';TSAISIETEXTE2.BITBTN1.CAPTION';
:=rsOK;


BITBTN2.CAPTION
// msgctxt ';TSAISIETEXTE2.BITBTN2.CAPTION';
:=rsAnnuler;


BOUTONSUP.CAPTION
:=rsSupprimerCeT9;


CAPTION
:=rsSaisirLeText;


GROUPBOX1.CAPTION
:=rsTexte;


GROUPBOX2.CAPTION
:=rsCoordonnEsDu2;


LOG1.CAPTION
// msgctxt ';TSAISIETEXTE2.LOG1.CAPTION';
:=rsLog1;


LOG2.CAPTION
// msgctxt ';TSAISIETEXTE2.LOG2.CAPTION';
:=rsLog2;


SPEEDBUTTON1.CAPTION
:=rsPolice;


STATICTEXT1.CAPTION
// msgctxt ';TSAISIETEXTE2.STATICTEXT1.CAPTION';
:=rsX;


STATICTEXT2.CAPTION
// msgctxt ';TSAISIETEXTE2.STATICTEXT2.CAPTION';
:=rsY;


end;

procedure Tsaisietexte2.BitBtn1Click(Sender: TObject);
begin
texteaafficher2:=edit1.Text;
try
textex:=strtofloat(editx.text);
except
application.messagebox(pchar(rsValeurIncorr), pchar(rsAttention), mb_ok);
exit;
end;
try
textey:=strtofloat(edity.text);
except
application.messagebox(pchar(rsValeurIncorr2), pchar(rsAttention), mb_ok);
exit;
end;

retailleptexteaffiche(listetexteaffiche,nombretexte,nombretexte+1);
 inc(nombretexte);
 if not(listetexteaffiche[-1+nombretexte].create(texteaafficher2,fontetexte2.name,
 fontetexte2.charset,fontetexte2.color,fontetexte2.size,fontetexte2.pitch,
 (fsbold in fontetexte2.style),(fsitalic in fontetexte2.style),
 (fsunderline in fontetexte2.style),(fsstrikeout in fontetexte2.style),
 textex,textey))  then begin
 retailleptexteaffiche(listetexteaffiche,nombretexte,nombretexte-1);
 dec(nombretexte);
 application.messagebox(pchar(rsValeursNonAu), pchar(rsAttention3), mb_ok);
 end else
self.modalresult:=mrok;
Rafraichit;
 end;





initialization
  {$i unit10.lrs}

end.
