unit fiche_affiche_coordonees;

{$mode objfpc}{$H+}

interface

uses
  LCLIntf, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, LResources,UnitScaleFont,UChaines;

type

  { Tfiche_coordonnees_segments }

  Tfiche_coordonnees_segments = class(TForm)
    Memo1: TMemo;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private  encreation:boolean;
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

var
  fiche_coordonnees_segments: Tfiche_coordonnees_segments;

implementation


{ Tfiche_coordonnees_segments }

procedure Tfiche_coordonnees_segments.FormCreate(Sender: TObject);
begin
 encreation:=true;
 CAPTION:=rsCoordonnEsDe;

end;

procedure Tfiche_coordonnees_segments.FormShow(Sender: TObject);
begin
   //if encreation then begin scalefont(self); encreation:=false; end;
end;

initialization
  {$i fiche_affiche_coordonees.lrs}


end.
