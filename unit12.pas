{  This file is part of OptGeo, optical simulation software
    Copyright (C) 2004  Jean-Marie Biansan
        Contact: jeanmarie.biansan@free.fr
    Web site: http://jeanmarie.biansan.free.fr/logiciel.html
 From version 1.25, OptGeo is distribued under the terms of the
 GNU General Public License.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA}

    unit unit12;

{$mode objfpc}{$H+}

interface

uses
  LCLIntf, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, LResources,LCLType,UnitScaleFont,UChaines;

type

  { Tsaisiediametre }

  Tsaisiediametre = class(TForm)
    BitBtn1: TBitBtn;
    GroupBox1: TGroupBox;
    StaticText1: TLabel;
    editdia: TEdit;
    log1: TLabel;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private  encreation:boolean;
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

var
  saisiediametre: Tsaisiediametre;
   diadia:extended;
implementation


procedure Tsaisiediametre.BitBtn1Click(Sender: TObject);
begin
try
diadia:=strtofloat(editdia.text);
except
application.messagebox(pchar(rsCeciNEstPasU),
pchar(rsAttention2), mb_ok);
modalresult:=mrcancel;
exit;
end;

end;

procedure Tsaisiediametre.FormCreate(Sender: TObject);
begin
   encreation:=true;
   BITBTN1.CAPTION
// msgctxt ';TSAISIEDIAMETRE.BITBTN1.CAPTION';
:=rsOK;


CAPTION
:=rsSaissezLeDia;


GROUPBOX1.CAPTION
:=rsSaisissez;


LOG1.CAPTION
// msgctxt ';TSAISIEDIAMETRE.LOG1.CAPTION';
:=rsLog1;


STATICTEXT1.CAPTION
:=rsDiamTre2;

end;

procedure Tsaisiediametre.FormShow(Sender: TObject);
begin
  //if encreation then begin scalefont(self); encreation:=false; end;
end;

initialization
  {$i unit12.lrs}


end.
