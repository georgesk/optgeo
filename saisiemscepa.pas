 {  This file is part of OptGeo, optical simulation software
    Copyright (C) 2004  Jean-Marie Biansan
        Contact: jeanmarie.biansan@free.fr
    Web site: http://jeanmarie.biansan.free.fr/logiciel.html
 From version 1.25, OptGeo is distribued under the terms of the
 GNU General Public License.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA}


    unit saisiemscepa;

{$mode objfpc}{$H+}

interface

uses
  LCLIntf, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, Buttons, Spin,ColorBox, LResources,LCLType,UnitScaleFont,
  UChaines;

type

  { Tsaisiemiroirscepa }

  Tsaisiemiroirscepa = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    gridaxe: TColorBox;
    Colorgrid1: TColorBox;
    Image1: TImage;
    GroupBox1: TGroupBox;
    StaticText1: TLabel;
    StaticText2: TLabel;
    editx1: TEdit;
    edity1: TEdit;
    GroupBox2: TGroupBox;
    StaticText3: TLabel;
    StaticText4: TLabel;
    editx2: TEdit;
    edity2: TEdit;
    GroupBox3: TGroupBox;
    GroupBox4: TGroupBox;
    editepaisseur: TSpinEdit;
    GroupBox5: TGroupBox;
    cochehachures: TCheckBox;
    cocheaxe: TCheckBox;
    GroupBox6: TGroupBox;
    StaticText5: TLabel;
    editfocale: TEdit;
    GroupBox7: TGroupBox;
    boutonsup: TBitBtn;
    GroupBox8: TGroupBox;
    checktrou: TCheckBox;
    StaticText6: TLabel;
    editdiametretrou: TEdit;
    log1: TLabel;
    log2: TLabel;
    log3: TLabel;
    log4: TLabel;
    log5: TLabel;
    log6: TLabel;
    procedure BitBtn1Click(Sender: TObject);
    procedure checktrouClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private  encreation:boolean;
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

var
  saisiemiroirscepa: Tsaisiemiroirscepa;
   mscepa_hachures,mscepa_axe,mscepa_trou:boolean;
  mscepa_epaisseur:integer;
  mscepa_couleur,mscepa_couleuraxe:tcolor;
  mscepa_x1,mscepa_x2,mscepa_y1,mscepa_y2,mscepa_focale,mscepa_diametretrou:extended;
implementation

uses unit222;


procedure Tsaisiemiroirscepa.BitBtn1Click(Sender: TObject);
begin
mscepa_hachures:=cochehachures.Checked;
mscepa_trou:=checktrou.checked;
mscepa_axe:=cocheaxe.Checked;
mscepa_epaisseur:=editepaisseur.Value;
mscepa_couleur:=colorgrid1.selected;
mscepa_couleuraxe:=gridaxe.selected;


if mscepa_trou then
try
mscepa_diametretrou:=strtofloat(editdiametretrou.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce32),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;

 if not(mscepa_trou) then mscepa_diametretrou:=0;


try
mscepa_x1:=strtofloat(editx1.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce13),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;


 try
mscepa_x2:=strtofloat(editx2.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce14),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;


 try
mscepa_y1:=strtofloat(edity1.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce15),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;


 try
mscepa_y2:=strtofloat(edity2.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce16),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;



 try
mscepa_focale:=strtofloat(editfocale.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce24),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;
 if (mscepa_focale=0) then begin
 application.messagebox(pchar(rsLaFocaleDoit),
 pchar(rsattention),mb_ok);
 exit;
 end;
 mscepa_focale:=abs(mscepa_focale);
  ReTaillePMscepa(ListeMscepa,nombreMscepa,nombreMscepa+1);
 inc(nombremscepa);
if not
(listemscepa[-1+nombremscepa].create2(mscepa_x1,mscepa_y1,mscepa_x2,mscepa_y2,
mscepa_focale,mscepa_epaisseur,
mscepa_couleur,mscepa_couleuraxe,mscepa_hachures,mscepa_axe,
mscepa_trou,mscepa_diametretrou)) then begin
ReTaillePMscepa(ListeMscepa,nombreMscepa,nombreMscepa-1);
dec(nombremscepa);
application.messagebox(pchar(rsTailleDuMiro2),
pchar(rsattention),mb_ok);
end else
self.modalresult:=mrok;
Rafraichit;


end;

procedure Tsaisiemiroirscepa.checktrouClick(Sender: TObject);
begin
editdiametretrou.enabled:=checktrou.checked;
end;

procedure Tsaisiemiroirscepa.FormCreate(Sender: TObject);
begin
   encreation:=true;
   BITBTN1.CAPTION
// msgctxt ';TSAISIEMIROIRSCEPA.BITBTN1.CAPTION';
:=rsOK;


BITBTN2.CAPTION
// msgctxt ';TSAISIEMIROIRSCEPA.BITBTN2.CAPTION';
:=rsAnnuler;


BOUTONSUP.CAPTION
// msgctxt ';TSAISIEMIROIRSCEPA.BOUTONSUP.CAPTION';
:=rsSupprimerCeM;


CAPTION
:=rsAjoutDUnMiro9;


CHECKTROU.CAPTION
:=rsTrouer;


COCHEAXE.CAPTION
// msgctxt ';TSAISIEMIROIRSCEPA.COCHEAXE.CAPTION';
:=rsAxeOptique;


COCHEHACHURES.CAPTION
// msgctxt ';TSAISIEMIROIRSCEPA.COCHEHACHURES.CAPTION';
:=rsHachures;


GROUPBOX1.CAPTION
// msgctxt ';TSAISIEMIROIRSCEPA.GROUPBOX1.CAPTION';
:=rsPoint1;


GROUPBOX2.CAPTION
// msgctxt ';TSAISIEMIROIRSCEPA.GROUPBOX2.CAPTION';
:=rsPoint2;


GROUPBOX3.CAPTION
// msgctxt ';TSAISIEMIROIRSCEPA.GROUPBOX3.CAPTION';
:=rsCouleurDuMir;


GROUPBOX4.CAPTION
// msgctxt ';TSAISIEMIROIRSCEPA.GROUPBOX4.CAPTION';
:=rsEpaisseurTra6;


GROUPBOX5.CAPTION
// msgctxt ';TSAISIEMIROIRSCEPA.GROUPBOX5.CAPTION';
:=rsAspect;


GROUPBOX6.CAPTION
:=rsFocale;


GROUPBOX7.CAPTION
// msgctxt ';TSAISIEMIROIRSCEPA.GROUPBOX7.CAPTION';
:=rsCouleurDeLAx4;


GROUPBOX8.CAPTION
:=rsTrouerLeMiro;


LOG1.CAPTION
// msgctxt ';TSAISIEMIROIRSCEPA.LOG1.CAPTION';
:=rsLog1;


LOG2.CAPTION
// msgctxt ';TSAISIEMIROIRSCEPA.LOG2.CAPTION';
:=rsLog2;


LOG3.CAPTION
// msgctxt ';TSAISIEMIROIRSCEPA.LOG3.CAPTION';
:=rsLog3;


LOG4.CAPTION
// msgctxt ';TSAISIEMIROIRSCEPA.LOG4.CAPTION';
:=rsLog4;


LOG5.CAPTION
// msgctxt ';TSAISIEMIROIRSCEPA.LOG5.CAPTION';
:=rsLog5;


LOG6.CAPTION
// msgctxt ';TSAISIEMIROIRSCEPA.LOG6.CAPTION';
:=rsLog6;


STATICTEXT1.CAPTION
// msgctxt ';TSAISIEMIROIRSCEPA.STATICTEXT1.CAPTION';
:=rsX;


STATICTEXT2.CAPTION
// msgctxt ';TSAISIEMIROIRSCEPA.STATICTEXT2.CAPTION';
:=rsY;


STATICTEXT3.CAPTION
// msgctxt ';TSAISIEMIROIRSCEPA.STATICTEXT3.CAPTION';
:=rsX;


STATICTEXT4.CAPTION
// msgctxt ';TSAISIEMIROIRSCEPA.STATICTEXT4.CAPTION';
:=rsY;


STATICTEXT5.CAPTION
// msgctxt ';TSAISIEMIROIRSCEPA.STATICTEXT5.CAPTION';
:=rsF;


STATICTEXT6.CAPTION
:=rsDiamTreDuTro;

end;

procedure Tsaisiemiroirscepa.FormShow(Sender: TObject);
begin
 // if encreation then begin scalefont(self); encreation:=false; end;
end;

initialization
  {$i saisiemscepa.lrs}


end.
