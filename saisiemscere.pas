 {  This file is part of OptGeo, optical simulation software
    Copyright (C) 2004  Jean-Marie Biansan
        Contact: jeanmarie.biansan@free.fr
    Web site: http://jeanmarie.biansan.free.fr/logiciel.html
 From version 1.25, OptGeo is distribued under the terms of the
 GNU General Public License.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA}


    unit saisiemscere;

{$mode objfpc}{$H+}

interface

uses
  LCLIntf, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, Buttons, Spin,ColorBox, LResources,LCLType,UnitScaleFont,
  UChaines;

type

  { Tsaisiemiroirscere }

  Tsaisiemiroirscere = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    gridaxe: TColorBox;
    Colorgrid1: TColorBox;
    Image1: TImage;
    GroupBox1: TGroupBox;
    StaticText1: TLabel;
    StaticText2: TLabel;
    editx1: TEdit;
    edity1: TEdit;
    GroupBox2: TGroupBox;
    StaticText3: TLabel;
    StaticText4: TLabel;
    editx2: TEdit;
    edity2: TEdit;
    GroupBox3: TGroupBox;
    GroupBox4: TGroupBox;
    editepaisseur: TSpinEdit;
    GroupBox5: TGroupBox;
    cochehachures: TCheckBox;
    cocheaxe: TCheckBox;
    GroupBox6: TGroupBox;
    StaticText5: TLabel;
    GroupBox7: TGroupBox;
    editrayoncourbure: TEdit;
    GroupBox8: TGroupBox;
    Checkobtu: TCheckBox;
    boutonsup: TBitBtn;
    GroupBox9: TGroupBox;
    cochetrou: TCheckBox;
    editdiametretrou: TEdit;
    StaticText6: TLabel;
    StaticText7: TLabel;
    log1: TLabel;
    log2: TLabel;
    log3: TLabel;
    log4: TLabel;
    log5: TLabel;
    StaticText8: TLabel;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    
  private  encreation:boolean;
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

var
  saisiemiroirscere: Tsaisiemiroirscere;
   mscere_hachures,mscere_axe,mscere_aigu,mscere_trou:boolean;
  mscere_epaisseur:integer;
  mscere_couleur,mscere_couleuraxe:tcolor;
  mscere_x1,mscere_x2,mscere_y1,mscere_y2,mscere_rayoncourbure,mscere_diametretrou:extended;
implementation

uses unit222;


procedure Tsaisiemiroirscere.BitBtn1Click(Sender: TObject);
begin
mscere_aigu:=not(checkobtu.checked);
mscere_hachures:=cochehachures.Checked;
mscere_axe:=cocheaxe.Checked;
mscere_epaisseur:=editepaisseur.Value;
mscere_couleur:=colorgrid1.selected;
mscere_couleuraxe:=gridaxe.selected;
mscere_trou:=cochetrou.checked;
if not(mscere_trou) then mscere_diametretrou:=0 else
try
mscere_diametretrou:=strtofloat(editdiametretrou.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce32),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;


try
mscere_x1:=strtofloat(editx1.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce13),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;


 try
mscere_x2:=strtofloat(editx2.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce14),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;


 try
mscere_y1:=strtofloat(edity1.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce15),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;


 try
mscere_y2:=strtofloat(edity2.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce16),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;



 try
mscere_rayoncourbure:=strtofloat(editrayoncourbure.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce33),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;
 if (mscere_rayoncourbure=0) then begin
 application.messagebox(pchar(rsLeRayonDeCou),
 pchar(rsattention),mb_ok);
 exit;
 end;
 mscere_rayoncourbure:=abs(mscere_rayoncourbure);

 ReTaillePMscere(ListeMscere,nombreMscere,nombreMscere+1);
 inc(nombremscere);
if not
(Listemscere[-1+nombremscere].create2(mscere_x1,mscere_y1,mscere_x2,mscere_y2,
mscere_rayoncourbure,mscere_epaisseur,
mscere_couleur,mscere_couleuraxe,mscere_hachures,mscere_axe,mscere_aigu,mscere_trou,mscere_diametretrou)) then begin
ReTaillePMscere(ListeMscere,nombreMscere,nombreMscere-1);
dec(nombremscere);
application.messagebox(pchar(rsTailleDuMiro7),
pchar(rsattention),mb_ok);
end else
self.modalresult:=mrok;
Rafraichit;


end;

procedure Tsaisiemiroirscere.FormCreate(Sender: TObject);
begin
   encreation:=true;
   BITBTN1.CAPTION
// msgctxt ';TSAISIEMIROIRSCERE.BITBTN1.CAPTION';
:=rsOK;


BITBTN2.CAPTION
// msgctxt ';TSAISIEMIROIRSCERE.BITBTN2.CAPTION';
:=rsAnnuler;


BOUTONSUP.CAPTION
// msgctxt ';TSAISIEMIROIRSCERE.BOUTONSUP.CAPTION';
:=rsSupprimerCeM;


CAPTION
:=rsAjoutDUnMiro10;


CHECKOBTU.CAPTION
:=rsObtu;


COCHEAXE.CAPTION
:=rsAxeDeSymTrie;


COCHEHACHURES.CAPTION
// msgctxt ';TSAISIEMIROIRSCERE.COCHEHACHURES.CAPTION';
:=rsHachures;


COCHETROU.CAPTION
// msgctxt ';TSAISIEMIROIRSCERE.COCHETROU.CAPTION';
:=rsTrouer;


GROUPBOX1.CAPTION
// msgctxt ';TSAISIEMIROIRSCERE.GROUPBOX1.CAPTION';
:=rsPoint1;


GROUPBOX2.CAPTION
// msgctxt ';TSAISIEMIROIRSCERE.GROUPBOX2.CAPTION';
:=rsPoint2;


GROUPBOX3.CAPTION
// msgctxt ';TSAISIEMIROIRSCERE.GROUPBOX3.CAPTION';
:=rsCouleurDuMir;


GROUPBOX4.CAPTION
// msgctxt ';TSAISIEMIROIRSCERE.GROUPBOX4.CAPTION';
:=rsEpaisseurTra7;


GROUPBOX5.CAPTION
// msgctxt ';TSAISIEMIROIRSCERE.GROUPBOX5.CAPTION';
:=rsAspect;


GROUPBOX6.CAPTION
// msgctxt ';TSAISIEMIROIRSCERE.GROUPBOX6.CAPTION';
:=rsRayonDeCourb;


GROUPBOX7.CAPTION
// msgctxt ';TSAISIEMIROIRSCERE.GROUPBOX7.CAPTION';
:=rsCouleurDeLAx5;


GROUPBOX8.CAPTION
:=rsAngleDOuvert;


GROUPBOX9.CAPTION
// msgctxt ';TSAISIEMIROIRSCERE.GROUPBOX9.CAPTION';
:=rsTrouerLeMiro;


LOG1.CAPTION
// msgctxt ';TSAISIEMIROIRSCERE.LOG1.CAPTION';
:=rsLog1;


LOG2.CAPTION
// msgctxt ';TSAISIEMIROIRSCERE.LOG2.CAPTION';
:=rsLog2;


LOG3.CAPTION
// msgctxt ';TSAISIEMIROIRSCERE.LOG3.CAPTION';
:=rsLog3;


LOG4.CAPTION
// msgctxt ';TSAISIEMIROIRSCERE.LOG4.CAPTION';
:=rsLog4;


LOG5.CAPTION
// msgctxt ';TSAISIEMIROIRSCERE.LOG5.CAPTION';
:=rsLog5;


STATICTEXT1.CAPTION
// msgctxt ';TSAISIEMIROIRSCERE.STATICTEXT1.CAPTION';
:=rsX;


STATICTEXT2.CAPTION
// msgctxt ';TSAISIEMIROIRSCERE.STATICTEXT2.CAPTION';
:=rsY;


STATICTEXT3.CAPTION
// msgctxt ';TSAISIEMIROIRSCERE.STATICTEXT3.CAPTION';
:=rsX;


STATICTEXT4.CAPTION
// msgctxt ';TSAISIEMIROIRSCERE.STATICTEXT4.CAPTION';
:=rsY;


STATICTEXT5.CAPTION
// msgctxt ';TSAISIEMIROIRSCERE.STATICTEXT5.CAPTION';
:=rsR;


STATICTEXT6.CAPTION
:=rsDiamTreAngul;


STATICTEXT7.CAPTION
:=rsVuDepuisLeCe;


STATICTEXT8.CAPTION
// msgctxt ';TSAISIEMIROIRSCERE.STATICTEXT8.CAPTION';
:=rsRad;

end;

procedure Tsaisiemiroirscere.FormShow(Sender: TObject);
begin
 // if encreation then begin scalefont(self); encreation:=false; end;
end;



initialization
  {$i saisiemscere.lrs}


end.
