{  This file is part of OptGeo, optical simulation software
    Copyright (C) 2004  Jean-Marie Biansan
        Contact: jeanmarie.biansan@free.fr
    Web site: http://jeanmarie.biansan.free.fr/logiciel.html
 From version 1.25, OptGeo is distribued under the terms of the
 GNU General Public License.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA}


    unit saisief1;

{$mode objfpc}{$H+}

interface

uses
  LCLIntf, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Colorbox, Spin, ExtCtrls,unit222, LResources,LCLType,UnitScaleFont,
  UChaines;

type

  { Tsaisiefle1 }

  Tsaisiefle1 = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    radioext1: TRadioGroup;
    radioext2: TRadioGroup;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    spintaille1: TSpinEdit;
    spintaille2: TSpinEdit;
    StaticText1: TLabel;
    StaticText3: TLabel;
    StaticText2: TLabel;
    StaticText4: TLabel;
    GroupBox3: TGroupBox;
    spinepaisseur: TSpinEdit;
    radiostyletrait: TRadioGroup;
    GroupBox4: TGroupBox;
    ColorGrid1: TColorBox;
    Image1: TImage;
    GroupBox5: TGroupBox;
    StaticText5: TLabel;
    StaticText6: TLabel;
    editx1: TEdit;
    edity1: TEdit;
    GroupBox6: TGroupBox;
    StaticText7: TLabel;
    StaticText8: TLabel;
    editx2: TEdit;
    edity2: TEdit;
    boutonsup: TBitBtn;
    log1: TLabel;
    log2: TLabel;
    log3: TLabel;
    log4: TLabel;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private  encreation:boolean;
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

var
  saisiefle1: Tsaisiefle1;
   ra_ext1,ra_ext2:textremite;
   ra_epaisseur:integer;
   ra_styletrait:tpenstyle;
   ra_couleur:tcolor;
    ra_taille1,ra_taille2:integer;
    ra_x1,ra_x2,ra_y1,ra_y2:extended;
implementation





procedure Tsaisiefle1.BitBtn1Click(Sender: TObject);
begin
 ra_taille1:=spintaille1.value;
 ra_taille2:=spintaille2.value;
 ra_epaisseur:=spinepaisseur.value;
 case radioext1.itemindex of
 0: ra_ext1:=trien;
 1: ra_ext1:=tffleche;
 2: ra_ext1:=ttrait;
 3: ra_ext1:=trond;
 4: ra_ext1:=tcroix;
 5: ra_ext1:=tcroixx;
 6: ra_ext1:=tdisque;
 end;

 case radioext2.itemindex of
 0: ra_ext2:=trien;
 1: ra_ext2:=tffleche;
 2: ra_ext2:=ttrait;
 3: ra_ext2:=trond;
 4: ra_ext2:=tcroix;
 5: ra_ext2:=tcroixx;
 6: ra_ext2:=tdisque;
 end;

 case radiostyletrait.itemindex of
 0: ra_styletrait:=pssolid;
 1: ra_styletrait:=psdot;
 2: ra_styletrait:=psdash;
 3: ra_styletrait:=psdashdot;
 4: ra_styletrait:=psdashdotdot;
 end;

 ra_couleur:=colorgrid1.selected;

 try
 ra_x1:=strtofloat(editx1.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce13),
 pchar(rsAttention), mb_ok);
 exit;
 end;  end;
 if ((ra_x1<xxmin) or (ra_x1>xxmax)) then begin
 application.messagebox(pchar(rsValeurInacce13),
 pchar(rsattention),mb_ok);
 exit;
 end;

try
 ra_x2:=strtofloat(editx2.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce14),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;
 if ((ra_x2<xxmin) or (ra_x2>xxmax)) then begin
 application.messagebox(pchar(rsValeurInacce14),
 pchar(rsattention),mb_ok);
 exit;
 end;



 try
 ra_y1:=strtofloat(edity1.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce15),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;
 if ((ra_y1<yymin) or (ra_y1>yymax)) then begin
 application.messagebox(pchar(rsValeurInacce15),
 pchar(rsattention),mb_ok);
 exit;
 end;


 try
 ra_y2:=strtofloat(edity2.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce16),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;
 if ((ra_y2<yymin) or (ra_y2>yymax)) then begin
 application.messagebox(pchar(rsValeurInacce16),
 pchar(rsattention),mb_ok);
 exit;
 end;


 ReTaillePfleche(Listefleche,nombrefleche,nombrefleche+1);

 inc(nombrefleche);
if not
(Listefleche[-1+nombrefleche].create(ra_x1,ra_y1,ra_x2,ra_y2,
ra_epaisseur,
ra_couleur,ra_ext1,ra_ext2,ra_styletrait,ra_taille1,ra_taille2)) then begin
ReTaillePfleche(Listefleche,nombrefleche,nombrefleche-1);
dec(nombrefleche);
application.messagebox(pchar(rsTailleDuTrai2),
pchar(rsattention),mb_ok);

end else
self.modalresult:=mrOk;
Rafraichit;

end;

procedure Tsaisiefle1.FormCreate(Sender: TObject);
begin
   encreation:=true;
   BITBTN1.CAPTION
:=rsOK;


BITBTN2.CAPTION
:=rsAnnuler;


BOUTONSUP.CAPTION
:=rsSupprimerCeT3;


CAPTION
:=rsAjoutDUnTrai2;


GROUPBOX1.CAPTION
:=rsTailleDeLaPr;


GROUPBOX2.CAPTION
:=rsTailleDeLaSe;


GROUPBOX3.CAPTION
:=rsEpaisseurDuT;


GROUPBOX4.CAPTION
:=rsCouleurDuTra;


GROUPBOX5.CAPTION
:=rsCoordonnEsPo;


GROUPBOX6.CAPTION
:=rsCoordonnEsPo2;


LOG1.CAPTION
:=rsLog1;


LOG2.CAPTION
:=rsLog2;


LOG3.CAPTION
:=rsLog3;


LOG4.CAPTION
:=rsLog4;


RADIOEXT1.CAPTION
:=rsFormeDeLaPre;


RADIOEXT2.CAPTION
:=rsFormeDeLaSec;


RADIOSTYLETRAIT.CAPTION
:=rsStyleDuTrait;


STATICTEXT1.CAPTION
:=rsEn11000DeLaD;


STATICTEXT2.CAPTION
:=rsEn11000DeLaD;


STATICTEXT3.CAPTION
:=rsDeLEspaceDeT;


STATICTEXT4.CAPTION
:=rsDeLEspaceDeT2;


STATICTEXT5.CAPTION
:=rsX;


STATICTEXT6.CAPTION
:=rsY;


STATICTEXT7.CAPTION
:=rsX;


STATICTEXT8.CAPTION
:=rsY;

radioext1.Items[0]:=rsNormale;
radioext1.Items[1]:=rsFlChe;
radioext1.Items[2]:=rsTraitPerpend;
radioext1.Items[3]:=rsCercle;
radioext1.Items[4]:=rsCroix;
radioext1.Items[5]:=rsCroixX;
radioext1.Items[6]:=rsDisque;

radioext2.Items[0]:=rsNormale;
radioext2.Items[1]:=rsFlChe;
radioext2.Items[2]:=rsTraitPerpend;
radioext2.Items[3]:=rsCercle;
radioext2.Items[4]:=rsCroix;
radioext2.Items[5]:=rsCroixX;
radioext2.Items[6]:=rsDisque;

radiostyletrait.Items[0]:=rsPlein;
radiostyletrait.Items[1]:=rsPointillS;
radiostyletrait.Items[2]:=rsTirS;
radiostyletrait.Items[3]:=rsTirSPoints;
radiostyletrait.Items[4]:=rsTirSPointsPo;

end;

procedure Tsaisiefle1.FormShow(Sender: TObject);
begin
  //if encreation then begin scalefont(self); encreation:=false; end;
end;

initialization
  {$i saisief1.lrs}


end.
