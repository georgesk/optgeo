{  This file is part of OptGeo, optical simulation software
    Copyright (C) 2004  Jean-Marie Biansan
        Contact: jeanmarie.biansan@free.fr
    Web site: http://jeanmarie.biansan.free.fr/logiciel.html
 From version 1.25, OptGeo is distribued under the terms of the
 GNU General Public License.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA}

    unit ray1;

{$mode objfpc}{$H+}

interface

uses
  Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,Unit_commune,
  StdCtrls, ExtCtrls, Buttons, Spin,ColorBox, LResources,LCLType,
   {$ifdef windows}windows,shellapi,registry,{$endif}  Menus,
  ComCtrls,clipbrd,printers, saisiemp,saisieray,
  Unit_imp,Unit3,Unit4,saisiemscopa,saisiemscepa,math,
   MonBitmap, PrintersDlgs, LazFileUtils,MonImprimante,UnitScaleFont,
  LazHelpHTML,UTF8Process,unit1_palette,LCLIntf,UChaines,Translations,Unit_indices_milieu_ambiant,
  MonPostScript,MonPostScriptCanvas,UnitConfigPostScript,Definitions,unit222,gettext;

type

  { TForm1 }

  TForm1 = class(TForm)
    Image1: TImage;
    MenuItem1: TMenuItem;
    MenuItem10: TMenuItem;
    MenuItem11: TMenuItem;
    MenuItem12: TMenuItem;
    MenuItemreseau: TMenuItem;
    MenuItem14: TMenuItem;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    MenuItem4: TMenuItem;
    MenuItem5: TMenuItem;
    MenuItem6: TMenuItem;
    MenuItem7: TMenuItem;
    MenuItem8: TMenuItem;
    MenuItem9: TMenuItem;
    MenuPrisme: TMenuItem;
    OpenDialogImageFonds: TOpenDialog;
    Pop: TPopupMenu;
    PrintDialog1: TPrintDialog;
    SaveDialog2: TSaveDialog;
    SaveDialog3: TSaveDialog;
    SaveDialog4: TSaveDialog;
    ScrollBox1: TScrollBox;
    boutonprisme: TSpeedButton;
    boutonreseau: TSpeedButton;
    StatusBar1: TStatusBar;
    ToolBar1: TToolBar;
    PrinterSetupDialog1: TPrinterSetupDialog;

    MainMenu1: TMainMenu;
    boutonmiroirplan: TSpeedButton;
    boutonmscopa: TSpeedButton;
    boutonmscepa: TSpeedButton;
    ToolBar2: TToolBar;
    boutonlmc: TSpeedButton;
    ToolButton4: TToolButton;
    boutonlmd: TSpeedButton;
    boutonmscore: TSpeedButton;
    boutonmscere: TSpeedButton;
    Ajouter1: TMenuItem;
    Systmeoptique1: TMenuItem;
    Source2: TMenuItem;
    Miroirplan2: TMenuItem;
    Lentilleminceconvergente1: TMenuItem;
    Lentillemincedivergente1: TMenuItem;
    Miroirsphriqueconvergent1: TMenuItem;
    Miroirsphriqueconvexe1: TMenuItem;
    Systmeoptiquerel1: TMenuItem;
    Miroirsphriqueconcave1: TMenuItem;
    Miroirsphriqueconvexe2: TMenuItem;
    Rayonunique1: TMenuItem;
    boutonrayon: TSpeedButton;
    boutonsourceponctuelle: TSpeedButton;
    boutonondeplane: TSpeedButton;
    Sourceponctuelle1: TMenuItem;
    Ondeplane1: TMenuItem;
    ToolButton5: TToolButton;
    Fichier1: TMenuItem;
    Nouveau1: TMenuItem;
    Enregistrersimulation1: TMenuItem;
    Ouvrirsimulation1: TMenuItem;
    SaveDialog1: TSaveDialog;
    OpenDialog1: TOpenDialog;
    N3: TMenuItem;
    Imprimer1: TMenuItem;
    N4: TMenuItem;
    Quitter1: TMenuItem;
    Edition1: TMenuItem;
    Copier1: TMenuItem;
    ToolButton2: TToolButton;
    boutonecran: TSpeedButton;
    ToolButton6: TToolButton;
    boutonlsr: TSpeedButton;
    Ecran1: TMenuItem;
    Lamesemirflchissante1: TMenuItem;
    boutonpoly: TSpeedButton;
    Polyhdrerfractant1: TMenuItem;
    boutonsphere: TSpeedButton;
    boutondistance: TSpeedButton;
    Sphrerfractante1: TMenuItem;
    boutonconiqueconcave: TSpeedButton;
    boutonconiqueconvexe: TSpeedButton;
    Miroircnique1: TMenuItem;
    boutontexte: TSpeedButton;
    Divers1: TMenuItem;
    Textesurledessin1: TMenuItem;
    boutondia: TSpeedButton;
    Diaphragme1: TMenuItem;
    N1: TMenuItem;
    N2: TMenuItem;
    N5: TMenuItem;
    Grille1: TMenuItem;
    Afficherlagrille1: TMenuItem;
    Attractiondelagrille1: TMenuItem;
    N6: TMenuItem;
    Dupliquerunlment1: TMenuItem;
    Optionsdelagrille1: TMenuItem;
    boutonlec: TSpeedButton;
    Lentille1: TMenuItem;
    boutonspv: TSpeedButton;
    N7: TMenuItem;
    Optionsdesnormales1: TMenuItem;
    N8: TMenuItem;
    StaticText1: TLabel;
    ComboZoom: TComboBox;
    ToolButton1: TToolButton;
    StaticText2: TLabel;
    StaticText3: TLabel;
    StaticText4: TLabel;
    StaticText5: TLabel;
    cochenormale: TCheckBox;
    ToolButton9: TToolButton;
    cochegrille: TCheckBox;
    boutonangle: TSpeedButton;
    StaticText6: TLabel;
    Afficherlesangles1: TMenuItem;
    cocheangles: TCheckBox;
    StaticText7: TLabel;
    boutonpolycercle: TSpeedButton;
    PopupMenu1: TPopupMenu;
    Joindreparunsegment1: TMenuItem;
    Joindreparunarcdecercle1: TMenuItem;
    Enregistrersimulation2: TMenuItem;
    boutonfantome: TSpeedButton;
    Polysphrerfractante1: TMenuItem;
    SpeedButton1: TSpeedButton;
    Aide1: TMenuItem;
    Contacterlauteur1: TMenuItem;
    N9: TMenuItem;
    AidesurOptGeo1: TMenuItem;
    N10: TMenuItem;
    Apropos2: TMenuItem;
    N11: TMenuItem;
    Surfacesdonde1: TMenuItem;
    N12: TMenuItem;
    Editerlecommentairedelasimulation1: TMenuItem;
    Supprimerunlment1: TMenuItem;
    Propritsdunlment1: TMenuItem;
    Dplacerunlment1: TMenuItem;
    Commenatiresurlasimulation1: TMenuItem;
    Mesurededistance1: TMenuItem;
    Mesuredangle1: TMenuItem;
    Rayonsfantomes1: TMenuItem;
    N13: TMenuItem;
    SiteWeb1: TMenuItem;
    boutonfleche: TSpeedButton;
    Traitflche1: TMenuItem;
    boutonoeil: TSpeedButton;
    Oeilstylis1: TMenuItem;
    Grouperdeslments1: TMenuItem;
    Tousleslments1: TMenuItem;
    Elementschoisir2: TMenuItem;
    N14: TMenuItem;
    Basederegistre1: TMenuItem;
    AssocierlesfichiersoptavecOptGeo1: TMenuItem;
    Dsassocier1: TMenuItem;
    cocheattraction: TCheckBox;
    N15: TMenuItem;
    Modifierdimensionsespacedetravail1: TMenuItem;
    durpertoireexemples1: TMenuItem;
    durpertoireexemples2: TMenuItem;
    Couleurdefond1: TMenuItem;
    N16: TMenuItem;
    ColorDialog1: TColorDialog;
    N17: TMenuItem;
    Prfrences1: TMenuItem;
    FontDialog1: TFontDialog;
    N18: TMenuItem;
    Empennagedesrayons1: TMenuItem;
    Ajusterautomatiquement1: TMenuItem;
    Manuellement1: TMenuItem;
    N19: TMenuItem;
    Rtablir1: TMenuItem;
    Refaire1: TMenuItem;
    N20: TMenuItem;
    Dfinirlerpertoirepersonnel1: TMenuItem;
    dudernierrpertoireutilis1: TMenuItem;
    N21: TMenuItem;
    LicenseGPL1: TMenuItem;
    SpeedButton2: TSpeedButton;
    procedure boutonprismeClick(Sender: TObject);
    procedure cocheanglesChange(Sender: TObject);
    procedure FormDeactivate(Sender: TObject);
    procedure MenuItem10Click(Sender: TObject);
    procedure MenuItem11Click(Sender: TObject);
    procedure MenuItem12Click(Sender: TObject);
    procedure MenuItem14Click(Sender: TObject);
    procedure MenuItem3Click(Sender: TObject);
    procedure MenuItem4Click(Sender: TObject);
    procedure MenuItem6Click(Sender: TObject);
    procedure MenuItem7Click(Sender: TObject);
    procedure MenuItem8Click(Sender: TObject);
    procedure MenuItemreseauClick(Sender: TObject);
    procedure MenuPrismeClick(Sender: TObject);
    procedure retaille_image1;
    procedure FormCreate(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormKeyPress(Sender: TObject; var Key: char);
    procedure FormResize(Sender: TObject);
    procedure boutonmiroirplanClick(Sender: TObject);
    procedure ComboZoomChange(Sender: TObject);
    procedure ConfigurationInitiale;


    procedure boutonrayonClick(Sender: TObject);
    procedure boutonmscopaClick(Sender: TObject);
    procedure desactiveboutons;
    procedure activeboutons;
    procedure FormShow(Sender: TObject);
    function modequelquechose:boolean;
    procedure Image1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Image1MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    function nombrequelquechose:integer;
    function ajoutencours:boolean;
    procedure boutonmscepaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure boutonlmcClick(Sender: TObject);
    procedure boutonlmdClick(Sender: TObject);
    procedure boutonmscoreClick(Sender: TObject);
    procedure boutonmscereClick(Sender: TObject);
    procedure Miroirplan2Click(Sender: TObject);
    procedure Rayonunique1Click(Sender: TObject);
    procedure Miroirsphriqueconvergent1Click(Sender: TObject);
    procedure Miroirsphriqueconvexe1Click(Sender: TObject);
    procedure Lentilleminceconvergente1Click(Sender: TObject);
    procedure Lentillemincedivergente1Click(Sender: TObject);
    procedure Miroirsphriqueconcave1Click(Sender: TObject);
    procedure Miroirsphriqueconvexe2Click(Sender: TObject);
    procedure boutonreseauClick(Sender: TObject);
    function taillemini:extended;
    procedure boutonsourceponctuelleClick(Sender: TObject);
    procedure Sourceponctuelle1Click(Sender: TObject);
    procedure boutonondeplaneClick(Sender: TObject);
    procedure Ondeplane1Click(Sender: TObject);
    procedure Nouveau1Click(Sender: TObject);
    procedure Enregistrersimulation1Click(Sender: TObject);
    procedure Ouvrirsimulation(Sender: TObject;repertoiredepart:string);
    procedure SuperposeSimulation(Sender: TObject;repertoiredepart:string);
    procedure Imprimer1Click(Sender: TObject);
    procedure Copier1Click(Sender: TObject);
    procedure boutonecranClick(Sender: TObject);
    procedure boutonlsrClick(Sender: TObject);
    procedure Quitter1Click(Sender: TObject);
    procedure Ecran1Click(Sender: TObject);
    procedure Lamesemirflchissante1Click(Sender: TObject);
    procedure boutonpolyClick(Sender: TObject);
    procedure Polyhdrerfractant1Click(Sender: TObject);
    procedure boutonsphereClick(Sender: TObject);
    procedure boutondistanceClick(Sender: TObject);
    procedure Sphrerfractante1Click(Sender: TObject);
    procedure boutonconiqueconcaveClick(Sender: TObject);
    procedure boutonconiqueconvexeClick(Sender: TObject);
    procedure Miroircnique1Click(Sender: TObject);
    procedure boutontexteClick(Sender: TObject);
    procedure Textesurledessin1Click(Sender: TObject);
    procedure boutondiaClick(Sender: TObject);
    procedure Diaphragme1Click(Sender: TObject);
    procedure Dupliquerunlment1Click(Sender: TObject);
    procedure Afficherlagrille1Click(Sender: TObject);
    procedure Optionsdelagrille1Click(Sender: TObject);
    procedure Attractiondelagrille1Click(Sender: TObject);
    procedure boutonlecClick(Sender: TObject);
    procedure Lentille1Click(Sender: TObject);
    procedure boutonspvClick(Sender: TObject);
    procedure N7Click(Sender: TObject);
    procedure Optionsdesnormales1Click(Sender: TObject);
    procedure cochenormaleClick(Sender: TObject);
    procedure cochegrilleClick(Sender: TObject);
    procedure boutonangleClick(Sender: TObject);
    procedure Afficherlesangles1Click(Sender: TObject);
    procedure cocheanglesClick(Sender: TObject);
    procedure boutonpolycercleClick(Sender: TObject);
    procedure Enregistrersimulation2Click(Sender: TObject);
    procedure boutonfantomeClick(Sender: TObject);
    procedure Polysphrerfractante1Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
 
    procedure Contacterlauteur1Click(Sender: TObject);
    procedure Apropos2Click(Sender: TObject);
    procedure AidesurOptGeo1Click(Sender: TObject);
    procedure Surfacesdonde1Click(Sender: TObject);
    procedure Editerlecommentairedelasimulation1Click(Sender: TObject);
    procedure Supprimerunlment1Click(Sender: TObject);
    procedure Propritsdunlment1Click(Sender: TObject);
    procedure Dplacerunlment1Click(Sender: TObject);
    procedure Commenatiresurlasimulation1Click(Sender: TObject);
    procedure Mesurededistance1Click(Sender: TObject);
    procedure Mesuredangle1Click(Sender: TObject);
    procedure Rayonsfantomes1Click(Sender: TObject);
    procedure SiteWeb1Click(Sender: TObject);
    procedure boutonflecheClick(Sender: TObject);
    procedure Traitflche1Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure boutonoeilClick(Sender: TObject);
    procedure Oeilstylis1Click(Sender: TObject);

          procedure Tousleslments1Click(Sender: TObject);
    procedure Elementschoisir2Click(Sender: TObject);
    procedure AssocierlesfichiersoptavecOptGeo1Click(Sender: TObject);
    procedure Dsassocier1Click(Sender: TObject);
    procedure cocheattractionClick(Sender: TObject);
      procedure metazero;
     function retaille_espace_possible(newxmin,newxmax,newymin,newymax:extended):boolean;
    procedure Modifierdimensionsespacedetravail11Click(Sender: TObject);
    procedure durpertoireexemples1Click(Sender: TObject);
    procedure durpertoireexemples2Click(Sender: TObject);
    procedure Couleurdefond1Click(Sender: TObject);
    procedure Prfrences1Click(Sender: TObject);
    procedure Empennagedesrayons1Click(Sender: TObject);
    procedure Manuellement1Click(Sender: TObject);
    procedure Ajusterautomatiquement1Click(Sender: TObject);
     procedure charge_virtuel;
    procedure Sauver1Click(Sender: TObject);
    procedure Rtablir1Click(Sender: TObject);
    procedure Refaire1Click(Sender: TObject);
    procedure Dfinirlerpertoirepersonnel1Click(Sender: TObject);
    procedure dudernierrpertoireutilis1Click(Sender: TObject);
    procedure LicenseGPL1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);

  private  encreation:boolean;
    { Déclarations privées }

  public
    { Déclarations publiques }

  end;




  procedure enregistrer(nomfichier:string);
   procedure sauvevirtuel;
const maxannulation=50;



var   ps : TMonPostScriptCanvas;


  Form1: TForm1;
   fermeture,creation,modepage:boolean;   versionprogramme:longint;
   rapportideal,rapportactuel:extended;   respectrapport,fichierouvert:boolean;
   etatactuel:array[1..maxannulation] of tstringlist;
   nombre_etats_sauvegardes,niveau_max_sauvegarde:integer;
   BIBI:tmonbitmap;
   imprimante:tprinter;     orientation_impression:tprinterorientation;
      or_ps:orientation_ps; p_ps:page_ps;    respect_ps:boolean;
      res_ps_x,res_ps_y:integer;
   var  il:integer;         newmenuitem:tmenuitem;



implementation

uses saisielmc, saisielmd, saisiemscore, saisiemscere, saisiesp1,saisieop1,
  saisielamesr, saisieec, Unit6, Unit8, saisiesphere, saisiemconique, Unit9,
  Unit10, saisiedia,Unit11, saisiech, saisiegr, saisielec, saisienor,
  saisiefant, saisiecercle, UNIT32, saisieptssurfaceonde, saisief1,
  saisieoe, modif_espace_travail, Unit16, Unit17, Unit19, Unit14, Unit13,
  Unitgpl, fiche_affiche_coordonees,Unit15,Unit12,saisiefle,Unit5,
  saisiepri,saisiereseau;

label 1888;


procedure tform1.ConfigurationInitiale;
   var fc,fconf:textfile; s,s1:string; titi:integer; t1:tcolor;
   label 111,112,113;
   begin
   policedefaut:=tfont.Create;
      policedefaut.Assign(form1.font);
      unit9.fontetexte:=tfont.Create;
      unit9.fontetexte.Assign(form1.font);

   if fileexistsutf8(nom_fichier_config_optgeo) then begin
    AssignFile(Fc,(nom_fichier_config_optgeo));
  reset(fc);
    while not(eof(fc)) do begin
   readln(fc,s);
   if s='repertoire exemples' then begin
      readln(fc,s1);
      repertoire_exemples:=s1;
      repertoire_exemples:=AppendPathDelim(repertoire_exemples);
      end;

   if s='repertoire simulation perso' then
      begin
      readln(fc,s1);
      repertoire_simul_perso:=s1;
            repertoire_simul_perso:=AppendPathDelim(repertoire_simul_perso);
      end;

      if s='repertoire config perso' then begin
      readln(fc,s1);

      end;
      end; { de la boucle while}
      closefile(fc);

      goto 112;
      end;


  111:


   try
   titi:=idno;
   forcedirectories((repertoire_config_perso));
    AssignFile(Fc,(nom_fichier_config_optgeo));
  rewrite(fc);
       {si creation impossible}
       except
      titi:= application.messagebox(pchar(rsImpossibleDe),
       pchar(rsErreurCritiq), mb_okcancel);
            end;
            if titi=idcancel then close;
             if titi=idok then begin
             repertoire_exemples:=repertoire_exemples_par_defaut;
                 repertoire_simul_perso:=repertoire_simul_perso_par_defaut;
                exit;
                end;

   application.messagebox(pchar(rsOptGeoEstExC), pchar(rsBienvenue), mb_ok);
  113:
  with form16
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
   form16.rep1.text:=repertoire_executable;
   form16.rep1.Enabled:=false;
    form16.directoryedit1.enabled:=true;
    form16.directoryedit2.enabled:=false;
    form16.directoryedit3.enabled:=true;
   form16.directoryedit1.Directory:=repertoire_exemples;
   form16.directoryedit2.Directory:=repertoire_config_perso;
   form16.directoryedit3.Directory:=repertoire_simul_perso;
   form16.showmodal;
   repertoire_exemples:=form16.directoryedit1.Directory;
    //repertoire_config_perso:=form16.directoryedit2.Directory;
    repertoire_simul_perso:=form16.directoryedit3.Directory;

    if not(directoryexistsUTF8(repertoire_exemples)) then   try
    forcedirectories((repertoire_exemples));
   except; end;
   if not(directoryexistsUTF8(repertoire_exemples)) then begin
   application.messagebox(pchar(rsJeNeParviens), pchar(rsMaisEuh), mb_ok);
        repertoire_exemples:=repertoire_exemples_par_defaut;
        goto 113;
    end;

   if not(directoryexistsUTF8(repertoire_simul_perso)) then
   try
   forcedirectories((repertoire_simul_perso));
   except
   end;
   if not(directoryexistsUTF8(repertoire_simul_perso)) then begin
   application.messagebox(pchar(rsJeNeParviens2), pchar(rsMaisEuh), mb_ok);
    repertoire_simul_perso:=repertoire_simul_perso_par_defaut;
    goto 113;
    end;

    if not(directoryexistsUTF8(repertoire_config_perso)) then
    try
    forcedirectories((repertoire_config_perso));
   except; end;
   if not(directoryexistsUTF8(repertoire_config_perso)) then begin
   application.messagebox(pchar(rsJeNeParviens3), pchar(rsMaisEuh), mb_ok);
    repertoire_config_perso:='';
    nom_fichier_config_perso:='';
    goto 113;
    end;


    writeln(fc,'repertoire exemples');
    writeln(fc,repertoire_exemples);
    writeln(fc,'repertoire simulation perso');
    writeln(fc,repertoire_simul_perso);
    writeln(fc,'repertoire config perso');
    writeln(fc,repertoire_config_perso);
    closefile(fc);

  112:   if not(directoryexistsUTF8(repertoire_exemples)) then   try
  forcedirectories((repertoire_exemples));

  except; end;
   if not(directoryexistsUTF8(repertoire_exemples)) then begin
   application.messagebox(pchar(rsRepertoireDe), pchar(rsMaisEuh), mb_ok);
        repertoire_exemples:=repertoire_exemples_par_defaut;
    end;

   if not(directoryexistsUTF8(repertoire_simul_perso)) then try
   forcedirectories((repertoire_simul_perso));
   except; end;
   if not(directoryexistsUTF8(repertoire_simul_perso)) then begin
   application.messagebox(pchar(rsRepertoireDe2), pchar(rsMaisEuh), mb_ok);
    repertoire_simul_perso:=repertoire_simul_perso_par_defaut;
    end;

    if not(directoryexistsUTF8(repertoire_config_perso)) then try
    forcedirectories((repertoire_config_perso));
   except; end;
   if not(directoryexistsUTF8(repertoire_config_perso)) then begin
   application.messagebox(pchar(rsRepertoireDe3), pchar(rsMaisEuh), mb_ok);
    repertoire_config_perso:='';
    nom_fichier_config_perso:='';
    exit;
    end;

    if not(fileexistsutf8(nom_fichier_config_perso)) then exit;
    {le fichier de config existe: on tente de le lire}
     assignfile(fconf,(nom_fichier_config_perso));
     reset(fconf);
     while not(eof(fconf)) do begin
      readln(fconf,s);
  {ecrase le repertoire perso de simul}
  if s='Repertoire perso' then
  readln(fconf, repertoire_simul_perso);

  if s='Repertoire courant' then
readln(fconf,repertoire_actuel);

      if s='Miroir plan' then begin
readln(fconf,couleurmiroirplan);
readln(fconf,epaisseurmiroirplan);
               end;

if s='Miroir sphérique concave paraxial' then begin
readln(fconf,couleurmscopa);
readln(fconf,epaisseurmscopa);
readln(fconf,couleuraxemscopa);
                              end;

if s='Miroir sphérique convexe paraxial' then begin
readln(fconf,couleurmscepa);
readln(fconf,epaisseurmscepa);
readln(fconf,couleuraxemscepa);                    end;

if s='Miroir sphérique concave réel' then begin
readln(fconf,couleurmscore);
readln(fconf,epaisseurmscore);
 readln(fconf,couleuraxemscore);               end;


 if s='Miroir sphérique convexe réel' then begin
readln(fconf,couleurmscere);
readln(fconf,epaisseurmscere);
readln(fconf,couleuraxemscere);                 end;


if s='Miroir cônique' then begin
readln(fconf,couleurmiroirconique);
readln(fconf,epaisseurmiroirconique);
 readln(fconf,couleuraxemiroirconique);
                                end;

if s='Lentille mince convergente idéale' then begin
readln(fconf,couleurlmc);
readln(fconf,epaisseurlmc);
readln(fconf,couleuraxelmc);                       end;


if s='Lentille mince divergente idéale' then begin
readln(fconf,couleurlmd);
readln(fconf,epaisseurlmd);
readln(fconf,couleuraxelmd);                      end;


if s='Lentille épaisse' then begin
readln(fconf,couleurlec);
readln(fconf,couleuraxelec);      end;



if s='Polyhèdre réfractant' then
readln(fconf,couleurpolyhedre);

if s='Polycercle réfractant' then
readln(fconf,couleurpolycercle);

if s='Sphère réfractante' then 
readln(fconf,couleursphere);

if s='Rayon unique' then begin
readln(fconf,couleurrayon);
readln(fconf,epaisseurrayon); end;

if s='Onde plane' then begin
readln(fconf,couleurondeplane);
readln(fconf,epaisseurondeplane); end;

if s='Source ponctuelle' then begin
readln(fconf,couleursourceponctuelle);
readln(fconf,epaisseursourceponctuelle); end;

if s='Ecran' then begin
readln(fconf,couleurecran);
readln(fconf,epaisseurecran); end;

if s='Lame semi-réfléchissante' then begin
readln(fconf,couleurlsr);
readln(fconf,epaisseurlsr);               end;

if s='Diaphragme' then begin
readln(fconf,couleurDiaphragme);
readln(fconf,epaisseurDiaphragme); end;

if s='Oeil' then begin
readln(fconf,couleurOeil);
readln(fconf,epaisseurOeil); end;

if s='Fléche' then begin
readln(fconf,couleurFleche);
readln(fconf,epaisseurFleche); end;

if s='Texte' then begin
policedefaut.style:=[];
   readln(fconf,s1);
   policedefaut.charset:=strtoint(s1);
   readln(fconf,t1);
   policedefaut.color:=t1;
   readln(fconf,s1);
   policedefaut.name:=s1;
   readln(fconf,s1);
   policedefaut.size:=strtoint(s1);
   readln(fconf,s1);
   if s1='fsbold' then policedefaut.style:=policedefaut.style+[fsbold];
   readln(fconf,s1);
   if s1='fsitalic' then   policedefaut.style:=policedefaut.style+[fsitalic];
  readln(fconf,s1);
  if s1='fsunderline'  then   policedefaut.style:=policedefaut.style+[fsunderline];
   readln(fconf,s1);
if s1='fsstrikeout'  then   policedefaut.style:=policedefaut.style+[fsstrikeout];
    readln(fconf,s1);
   if s1='fpDefault' then policedefaut.pitch:=fpDefault;
   if s1='fpVariable' then policedefaut.pitch:=fpVariable;
    if s1='fpFixed' then policedefaut.pitch:=fpFixed;
                       end;

   if s='Prisme' then
readln(fconf,couleurprisme);

    if s='Reseau' then begin
readln(fconf,couleurreseau);
    readln(fconf,epaisseurreseau);
    end;
     end;
     closefile(fconf);
  end;




 function tform1.retaille_espace_possible(newxmin,newxmax,newymin,newymax:extended):boolean;
 var oldxmin,oldxmax,oldymax,oldymin:extended;
 i:integer;
 essaimiroirplan:miroirplan;
  essaireseau:reseau;
 essaimscopa:mscopa;
 essaimscepa:mscepa;
 essaimscore:mscore;
 essaimscere:mscere;
 essailmc:lmc;
 essailmd:lmd;
 essailec:lec;
 essairayon:rayon;
 essaiondeplane:ondeplane;
 essaisourceponctuelle:sourceponctuelle;
 essaiecran:ecran;
 essailsr:lsr;
 essaipolyhedre:polyhedre;
 essaipolycercle:polycercle;
 essaisphere:sphere;
 essaimiroirconique:miroirconique;
 essaitexteaffiche:texteaffiche;
 essaidiaphragme:diaphragme;
 essaifleche:fleche;
 essaioeil:oeil;
 essaimetre:metre;
 essaiangle:angle;
 label 789456;

 begin
 retaille_espace_possible:=true;
 if nombrequelquechose=0 then exit;
 oldymax:=yymax; oldymin:=yymin; oldxmax:=xxmax; oldxmin:=xxmin;
 xxmax:=newxmax; xxmin:=newxmin; yymax:=newymax; yymin:=newymin;


 if nombremiroirplan>0 then for i:=1 to nombremiroirplan do
 if not  essaimiroirplan.create(listemiroirplan[-1+i].a1x,listemiroirplan[-1+i].a1y,
 listemiroirplan[-1+i].a2x,listemiroirplan[-1+i].a2y,listemiroirplan[-1+i].epaisseur,
 listemiroirplan[-1+i].couleur,listemiroirplan[-1+i].hachures) then goto 789456;


 if nombrereseau>0 then for i:=1 to nombrereseau do
 if not  essaireseau.create(listereseau[-1+i].a1x,listereseau[-1+i].a1y,
 listereseau[-1+i].a2x,listereseau[-1+i].a2y,listereseau[-1+i].epaisseur,
 listereseau[-1+i].couleur,listereseau[-1+i].pas,listereseau[-1+i].entransmission,
 listereseau[-1+i].hachures,listereseau[-1+i].ordre_min,listereseau[-1+i].ordre_max) then goto 789456;


 if nombremscopa>0 then for i:=1 to nombremscopa do
 if not  essaimscopa.create2(
 listemscopa[-1+i].a1x,
 listemscopa[-1+i].a1y,
 listemscopa[-1+i].a2x,
 listemscopa[-1+i].a2y,
 listemscopa[-1+i].focale,
 listemscopa[-1+i].epaisseur,
 listemscopa[-1+i].couleur,
 listemscopa[-1+i].couleuraxe,
 listemscopa[-1+i].hachures,
 listemscopa[-1+i].axefocal,
 listemscopa[-1+i].trou,
 listemscopa[-1+i].diametretrou) then goto 789456;


 if nombremscepa>0 then for i:=1 to nombremscepa do
 if not  essaimscepa.create2(
 listemscepa[-1+i].a1x,
 listemscepa[-1+i].a1y,
 listemscepa[-1+i].a2x,
 listemscepa[-1+i].a2y,
 listemscepa[-1+i].focale,
 listemscepa[-1+i].epaisseur,
 listemscepa[-1+i].couleur,
 listemscepa[-1+i].couleuraxe,
 listemscepa[-1+i].hachures,
 listemscepa[-1+i].axefocal,
listemscepa[-1+i].trou,
listemscepa[-1+i].diametretrou) then goto 789456;

 if nombremscore>0 then for i:=1 to nombremscore do
 if not  essaimscore.create2(
 listemscore[-1+i].a1x,
 listemscore[-1+i].a1y,
 listemscore[-1+i].a2x,
 listemscore[-1+i].a2y,
 listemscore[-1+i].rayoncourbure,
 listemscore[-1+i].epaisseur,
 listemscore[-1+i].couleur,
 listemscore[-1+i].couleuraxe,
 listemscore[-1+i].hachures,
 listemscore[-1+i].axefocal,
 listemscore[-1+i].aigu,
 listemscore[-1+i].trou,
 listemscore[-1+i].diametretrou) then goto 789456;

 if nombremscere>0 then for i:=1 to nombremscere do
 if not  essaimscere.create2(
 listemscere[-1+i].a1x,
 listemscere[-1+i].a1y,
 listemscere[-1+i].a2x,
 listemscere[-1+i].a2y,
 listemscere[-1+i].rayoncourbure,
 listemscere[-1+i].epaisseur,
 listemscere[-1+i].couleur,
 listemscere[-1+i].couleuraxe,
 listemscere[-1+i].hachures,
 listemscere[-1+i].axefocal,
 listemscere[-1+i].aigu,
 listemscere[-1+i].trou,
 listemscere[-1+i].diametretrou) then goto 789456;

  if nombrelec>0 then for i:=1 to nombrelec do
 if not  essailec.create(
 listelec[-1+i].s1x,
 listelec[-1+i].s1y,
 listelec[-1+i].s2x,
 listelec[-1+i].s2y,
 listelec[-1+i].r1,
 listelec[-1+i].r2,
 listelec[-1+i].diametre,
 listelec[-1+i].couleurfond,
 listelec[-1+i].couleuraxe,
 listelec[-1+i].axefocal,
 listelec[-1+i].indicerouge,
 listelec[-1+i].indicebleu,
 listelec[-1+i].indicevert,
 listelec[-1+i].reflechientrant,
 listelec[-1+i].reflechisortant) then goto 789456;

 if nombrelmc>0 then for i:=1 to nombrelmc do
 if not  essailmc.create2(
 listelmc[-1+i].a1x,
 listelmc[-1+i].a1y,
 listelmc[-1+i].a2x,
 listelmc[-1+i].a2y,
 listelmc[-1+i].focale,
 listelmc[-1+i].epaisseur,
 listelmc[-1+i].couleur,
 listelmc[-1+i].couleuraxe,
 listelmc[-1+i].axefocal) then goto 789456;


 if nombrelmd>0 then for i:=1 to nombrelmd do
 if not  essailmd.create2(
 listelmd[-1+i].a1x,
 listelmd[-1+i].a1y,
 listelmd[-1+i].a2x,
 listelmd[-1+i].a2y,
 listelmd[-1+i].focale,
 listelmd[-1+i].epaisseur,
 listelmd[-1+i].couleur,
 listelmd[-1+i].couleuraxe,
 listelmd[-1+i].axefocal) then goto 789456;


  if nombrerayon>0 then for i:=1 to nombrerayon do
 if not  essairayon.create(
 listerayon[-1+i].ax,
 listerayon[-1+i].ay,
 listerayon[-1+i].kx,
 listerayon[-1+i].ky,
 listerayon[-1+i].couleur,
 listerayon[-1+i].epaisseur,
 listerayon[-1+i].vav,
 listerayon[-1+i].vaa,
listerayon[-1+i].nombremaxenfant,
listerayon[-1+i].maxsegment) then goto 789456;


if nombreondeplane>0 then for i:=1 to nombreondeplane do
 if not  essaiondeplane.create(
 listeondeplane[-1+i].nombrederayon,
 listeondeplane[-1+i].sx,
 listeondeplane[-1+i].sy,
 listeondeplane[-1+i].a1x,
 listeondeplane[-1+i].a1y,
 listeondeplane[-1+i].a2x,
 listeondeplane[-1+i].a2y,
 listeondeplane[-1+i].couleur,
 listeondeplane[-1+i].epaisseur,
 listeondeplane[-1+i].vav,
 listeondeplane[-1+i].vaa,
listeondeplane[-1+i].maxenfantparrayon,
listeondeplane[-1+i].tracersurfacesonde,
listeondeplane[-1+i].listechemins,
listeondeplane[-1+i].maxsegment) then goto 789456;


if nombresourceponctuelle>0 then for i:=1 to nombresourceponctuelle do
 if not  essaisourceponctuelle.create(
 listesourceponctuelle[-1+i].nombrederayon,
 listesourceponctuelle[-1+i].sx,
 listesourceponctuelle[-1+i].sy,
 listesourceponctuelle[-1+i].a1x,
 listesourceponctuelle[-1+i].a1y,
 listesourceponctuelle[-1+i].a2x,
 listesourceponctuelle[-1+i].a2y,
 listesourceponctuelle[-1+i].couleur,
 listesourceponctuelle[-1+i].epaisseur,
 listesourceponctuelle[-1+i].vav,
 listesourceponctuelle[-1+i].vaa,
listesourceponctuelle[-1+i].maxenfantparrayon,
listesourceponctuelle[-1+i].sourcevirtuelle,
listesourceponctuelle[-1+i].tracersurfacesonde,
listesourceponctuelle[-1+i].listechemins,
listesourceponctuelle[-1+i].maxsegment) then goto 789456;

if nombreecran>0 then for i:=1 to nombreecran do
 if not  essaiecran.create(
 listeecran[-1+i].a1x,
 listeecran[-1+i].a1y,
 listeecran[-1+i].a2x,
 listeecran[-1+i].a2y,
 listeecran[-1+i].epaisseur,
 listeecran[-1+i].couleur) then goto 789456;

 if nombrelsr>0 then for i:=1 to nombrelsr do
 if not  essailsr.create(
 listelsr[-1+i].a1x,
 listelsr[-1+i].a1y,
 listelsr[-1+i].a2x,
 listelsr[-1+i].a2y,
 listelsr[-1+i].epaisseur,
 listelsr[-1+i].couleur) then goto 789456;


  if nombrepolyhedre>0 then for i:=1 to nombrepolyhedre do
 if not  essaipolyhedre.create(
 listepolyhedre[-1+i].nombresommet,
 listepolyhedre[-1+i].messommets,
 listepolyhedre[-1+i].indicerouge,
 listepolyhedre[-1+i].indicebleu,
 listepolyhedre[-1+i].indicevert,
 listepolyhedre[-1+i].couleurbord,
 listepolyhedre[-1+i].couleurfond,
 listepolyhedre[-1+i].reflechientrant,
 listepolyhedre[-1+i].reflechisortant) then goto 789456;


  if nombrepolycercle>0 then for i:=1 to nombrepolycercle do
 if not  essaipolycercle.create(
 listepolycercle[-1+i].nombresommet,
 listepolycercle[-1+i].messommets,
  listepolycercle[-1+i].mess,
   listepolycercle[-1+i].rectiligne,
 listepolycercle[-1+i].indicerouge,
 listepolycercle[-1+i].indicebleu,
 listepolycercle[-1+i].indicevert,
 listepolycercle[-1+i].couleurbord,
 listepolycercle[-1+i].couleurfond,
 listepolycercle[-1+i].reflechientrant,
 listepolycercle[-1+i].reflechisortant) then goto 789456;

 if nombresphere>0 then for i:=1 to nombresphere do
 if not  essaisphere.create(
 listesphere[-1+i].cx,
 listesphere[-1+i].cy,
  listesphere[-1+i].rayon,
 listesphere[-1+i].indicerouge,
 listesphere[-1+i].indicebleu,
 listesphere[-1+i].indicevert,
 listesphere[-1+i].couleurbord,
 listesphere[-1+i].couleurfond,
 listesphere[-1+i].reflechientrant,
 listesphere[-1+i].reflechisortant) then goto 789456;


  if nombremiroirconique>0 then for i:=1 to nombremiroirconique do
 if not  essaimiroirconique.create2(
 listemiroirconique[-1+i].fx,
 listemiroirconique[-1+i].fy,
 listemiroirconique[-1+i].theta0,
 listemiroirconique[-1+i].theta1,
 listemiroirconique[-1+i].theta2,
 listemiroirconique[-1+i].excentricite,
 listemiroirconique[-1+i].parametre,
 listemiroirconique[-1+i].concave,
 listemiroirconique[-1+i].couleur,
 listemiroirconique[-1+i].couleuraxe,
 listemiroirconique[-1+i].hachures,
 listemiroirconique[-1+i].axefocal) then goto 789456;


  if nombretexte>0 then for i:=1 to nombretexte do
 if not  essaitexteaffiche.create(listetexteaffiche[-1+i].texteluimeme,
 listetexteaffiche[-1+i].policename,listetexteaffiche[-1+i].
 policecharset,listetexteaffiche[-1+i].policecolor,
 listetexteaffiche[-1+i].policesize,
 listetexteaffiche[-1+i].policepitch,
 listetexteaffiche[-1+i].policefsbold,
 listetexteaffiche[-1+i].policefsitalic,
 listetexteaffiche[-1+i].policefsunderline,
 listetexteaffiche[-1+i].policefsstrikeout,
 listetexteaffiche[-1+i].x,
 listetexteaffiche[-1+i].y) then goto 789456;



  if nombrediaphragme>0 then for i:=1 to nombrediaphragme do
 if not  essaidiaphragme.create2(
 listediaphragme[-1+i].cx,
 listediaphragme[-1+i].cy,
 listediaphragme[-1+i].anglepolaire,
 listediaphragme[-1+i].rint,
 listediaphragme[-1+i].rext,
 listediaphragme[-1+i].couleur,
 listediaphragme[-1+i].epaisseur) then goto 789456;

 if nombrefleche>0 then for i:=1 to nombrefleche do
 if not  essaifleche.create(
 listefleche[-1+i].a1x,
 listefleche[-1+i].a1y,
 listefleche[-1+i].a2x,
 listefleche[-1+i].a2y,
 listefleche[-1+i].epaisseur,
 listefleche[-1+i].couleur,
 listefleche[-1+i].ext1,
 listefleche[-1+i].ext2,
  listefleche[-1+i].styletrait,
   listefleche[-1+i].taille1,
    listefleche[-1+i].taille2 ) then goto 789456;


     if nombreoeil>0 then for i:=1 to nombreoeil do
 if not  essaioeil.create(
 listeoeil[-1+i].cx,
 listeoeil[-1+i].cy,
 listeoeil[-1+i].ax,
 listeoeil[-1+i].ay,
 listeoeil[-1+i].epaisseur,
 listeoeil[-1+i].couleur) then goto 789456;

 if nombremetre>0 then for i:=1 to nombremetre do
 if not  essaimetre.create(
 listemetre[-1+i].a1x,
 listemetre[-1+i].a1y,
 listemetre[-1+i].a2x,
 listemetre[-1+i].a2y) then goto 789456;

 if nombreangle>0 then for i:=1 to nombreangle do
 if not  essaiangle.create(
 listeangle[-1+i].cx,
 listeangle[-1+i].cy,
 listeangle[-1+i].a1x,
 listeangle[-1+i].a1y,
 listeangle[-1+i].a2x,
 listeangle[-1+i].a2y) then goto 789456;



   xxmax:=oldxmax; xxmin:=oldxmin; yymax:=oldymax; yymin:=oldymin;
  exit;
 789456:   retaille_espace_possible:=false;
  xxmax:=oldxmax; xxmin:=oldxmin; yymax:=oldymax; yymin:=oldymin;
  exit;
   end;

  function tform1.nombrequelquechose:integer;
  begin
  nombrequelquechose:=nombremiroirplan+nombrereseau+nombremscopa+nombremscepa+nombremscore+nombremscere+
 nombrelmc+nombrelec+nombrelmd+nombrerayon+nombreondeplane+nombresourceponctuelle+nombreecran+
 nombrelsr+nombrepolyhedre+nombresphere+nombremiroirconique+nombretexte
 +nombrediaphragme+nombrepolycercle+nombrefleche+nombreoeil+nombreangle+nombremetre+nombreprisme;
 end;




  function TForm1.modequelquechose:boolean;
  begin
modequelquechose:=(ajoutencours or modemodif or modeinfo or modeduplication
or modedistance or modeangle or modesuppression)
  end;




  procedure TForm1.Image1MouseDown(Sender: TObject; Button: TMouseButton;
    Shift: TShiftState; X, Y: Integer);

var

     x1,y1,lambda,riri1,riri2,tx,ty,ux,uy,hx,hy,AiAj,ix,iy,tt:extended;
    iiii:integer;
    ppoint,posi:tpoint;
begin
{sous gtk2, ca ne suit pas le mouvement de la souris: on rerecupere la
position du curseur, pas actualisee assez rapidement}
 getcursorpos(posi);
  image1.OnMousedown:=nil;
  application.ProcessMessages;
   image1.OnMousedown:=@image1mousedown;
    posi:=image1.ScreenToClient(posi);
    x:=posi.x; y:=posi.y;


  if creation then exit;
if modesuppression then begin
effectuesuppression(sender,button,shift,x,y);
sauvevirtuel;
      exit;
      end;
 if modegroupement then begin
   effectuegroupement(sender,button,shift,x,y);
   sauvevirtuel;
          exit;
          end;

if (modemodif and modifencours) then begin
      finmodif(sender,button,shift,x,y);
      sauvevirtuel;
      exit;
      end;

      if (modemodif and not(modifencours)) then begin
      debutmodif(sender,button,shift,x,y);
      exit;
      end;

if (modeinfo) then begin
Info(Sender,Button,Shift,x,y);
exit;
end;

if not(Form1.modequelquechose) then exit;


bibi.LimitesEtAxes(xxmin,xxmax,yymin,yymax,yymin,yymax,form1.image1.width,
form1.image1.height,'',policedefaut,true,clwhite,false,0,clblack,false,0,0,clblack,
policedefaut,false,false,{grillex,grilley,}1,clblack,clblack,'','','','','','',false,false,false);

graphepresent:=true;
if point1fait then begin
 if (ssalt in shift)  then x:=lastxf;
 if (ssctrl in shift)  then y:=lastyf;
   end;

     BIBI.convert(x,y,x1,y1,true);
       lastxf:=x; lastyf:=y;
     if ((modedistance or modeangle) and not(point1fait)) then begin

     point1fait:=true;
     point1x:=x1; point1y:=y1;
     BIBI.cercle(point1x,point1y,3,1,clblack,pmcopy,true);

     rafraichit;
      affichecoordonnees(x1,y1);

 if modedistance then  form1.image1.Cursor:=crpoint2
 else  form1.image1.Cursor:=crpoint1 ;
                if modedistance then      form1.StatusBar1.Panels[2].text:=
                     rsPositionnezL else
                     form1.StatusBar1.Panels[2].text:=
                     rsPositionnezL2;
     exit;
     end;

   if (modedistance and point1fait) then begin
    retaillepmetre(listemetre,nombremetre,nombremetre+1);
    inc(nombremetre);
    listemetre[-1+nombremetre].create(point1x,point1y,x1,y1);
     listemetre[-1+nombremetre].dessine(BIBI);
    sauvevirtuel;

   form1.image1.Cursor:=crdefault;
                     form1.StatusBar1.Panels[2].text:=
                     '';
   modedistance:=false;
   rafraichit;
  affichecoordonnees(x1,y1);
  form1.activeboutons;
  form1.image1.Cursor:=crdefault;
                     form1.StatusBar1.Panels[2].text:='';
 exit;
   end;


   if (modeangle and point1fait and not(point2fait)) then begin
    point2x:=x1; point2y:=y1;  point2fait:=true;
    BIBI.cercle(point2x,point2y,3,1,clblack,pmcopy,true);

    affichecoordonnees(x1,y1);
   form1.image1.Cursor:=crpoint2;
                     form1.StatusBar1.Panels[2].text:=
                     rsPositionnezL3;
   rafraichit;
   exit;
   end;


    if (modeangle and point2fait) then begin
    retaillepangle(listeangle,nombreangle,nombreangle+1);
    inc(nombreangle);
    listeangle[-1+nombreangle].create(point1x,point1y,point2x,point2y,x1,y1);
      listeangle[-1+nombreangle].dessine(bibi);

    sauvevirtuel;

   form1.image1.Cursor:=crdefault;
                     form1.StatusBar1.Panels[2].text:=
                     '';
   modeangle:=false;
   rafraichit;
  affichecoordonnees(x1,y1);
  form1.activeboutons;
  form1.image1.Cursor:=crdefault;
                     form1.StatusBar1.Panels[2].text:='';
 exit;
   end;



 if( not (form1.ajoutencours)) then begin
  affichecoordonnees(x1,y1);
  //form1.monimage1.free;
  exit;
  end;

     if attractiongrille then bibi.arronditalagrille(x1,y1,grillex,grilley);
     {1er clic}

     if ajouttexte then begin
     retailleptexteaffiche(listetexteaffiche,nombretexte,nombretexte+1);
        inc(nombretexte);
        listetexteaffiche[-1+nombretexte].create(texteaafficher,fontetexte.name,
 fontetexte.charset,fontetexte.color,fontetexte.size,fontetexte.pitch,
 (fsbold in fontetexte.style),(fsitalic in fontetexte.style),
 (fsunderline in fontetexte.style),(fsstrikeout in fontetexte.style),x1,y1);
     affichecoordonnees(x1,y1);
   form1.image1.Cursor:=crdefault;
                     form1.StatusBar1.Panels[2].text:=
                     '';
   ajouttexte:=false;
   //form1.monimage1.free;
   form1.activeboutons;
   rafraichit;
      sauvevirtuel;
   exit;
   end;


     if ajoutpoly then begin
     point1fait:=true;   point2fait:=true;
     point1x:=x1; pointrx:=x1; point1xi:=x; pointrxi:=x;
 point1y:=y1; pointry:=y1; point1yi:=y; pointryi:=y;
     inc(nbactsommet);
     toutjustefait:=(nbactsommet=nbpoly-1);
     ttpoly[nbactsommet].ax:=x1;
     ttpoly[nbactsommet].ay:=y1;
     form1.image1.Cursor:=crsuivant;
                     form1.StatusBar1.Panels[2].text:=
                     rsPositionnezL4;
     if nbactsommet=nbpoly then begin
     ReTaillePpolyhedre(Listepolyhedre,nombrepolyhedre,nombrepolyhedre+1);
inc(nombrepolyhedre);
if not (listepolyhedre[-1+nombrepolyhedre].create(nbpoly,ttpoly,1.4,1.8,1.6,
clblack,couleurpolyhedre,jamaisreflechi,jamaisreflechi)) then begin
ReTaillePpolyhedre(Listepolyhedre,nombrepolyhedre,nombrepolyhedre-1);
dec(nombrepolyhedre);
application.messagebox(pchar(rsPointsSaisis),
pchar(rsAttention), mb_ok);
end else  begin
 Listepolyhedre[-1+nombrepolyhedre].dessine(BIBI);
    sauvevirtuel; end;
 form1.image1.Cursor:=crdefault;
                     form1.StatusBar1.Panels[2].text:='';
                      form1.activeboutons;
                     end;


          rafraichit;
  affichecoordonnees(x1,y1);

  exit;
        end;


         if (ajoutpolycercle and not(rectifait)) then begin
     point1x:=x1; pointrx:=x1; point1xi:=x; pointrxi:=x; point1fait:=true;
 point1y:=y1; pointry:=y1; point1yi:=y; pointryi:=y;
     inc(nbactsommet);
     ttpoly[nbactsommet].ax:=x1;
     ttpoly[nbactsommet].ay:=y1;
     toutjustefait:=(nbactsommet=nbpoly-1);
   if nbactsommet<nbpoly then begin
     form1.image1.Cursor:=crsuivant;
                     form1.StatusBar1.Panels[2].text:=
                     rsPositionnezL4;
                    end;
     if nbactsommet=nbpoly then begin
     ttpoly[nbpoly+1].ax:=ttpoly[1].ax;
       ttpoly[nbpoly+1].ay:=ttpoly[1].ay;
       rafraichit;
      BIBI.trait(ttpoly[1].ax,ttpoly[1].ay,ttpoly[nbpoly].ax,
      ttpoly[nbpoly].ay,1,couleurmiroirplan,psdot,pmcopy,true);
      for iiii:=1 to nbpoly-1 do
       BIBI.trait(ttpoly[iiii].ax,ttpoly[iiii].ay,ttpoly[iiii+1].ax,
      ttpoly[iiii+1].ay,1,couleurmiroirplan,psdot,pmcopy,true);


      for iiii:=1 to nbpoly do  begin

      with saisiejoindre
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
    if iiii<nbpoly then
     saisiejoindre.radiogroup1.caption:=Format(rsRelierLesPoi, [inttostr(iiii),
       inttostr(iiii+1)]) else
      saisiejoindre.radiogroup1.caption:=Format(rsRelierLesPoi, [inttostr(iiii
        ), inttostr(1)]);
      saisiejoindre.ShowModal;
      youri[iiii]:=rere;
      end;
      rectifait:=true;  nbrecti:=0;    nbrectiact:=0;
       form1.image1.Cursor:=crsommet;
                     form1.StatusBar1.Panels[2].text:=
    rsPositionnezL5;
              for iiii:=1 to nbpoly do
              if not(youri[iiii]) then begin
              inc(nbrecti);
              listerecti[nbrecti]:=iiii;
              end;

   if nbrecti=0  then begin
    ReTaillePpolycercle(Listepolycercle,nombrepolycercle,nombrepolycercle+1);
inc(nombrepolycercle);
if not (listepolycercle[-1+nombrepolycercle].create(nbpoly,ttpoly,tts,youri,1.4,1.8,1.6,
clblack,couleurpolycercle,jamaisreflechi,jamaisreflechi)) then begin
ReTaillePpolycercle(Listepolycercle,nombrepolycercle,nombrepolycercle-1);
dec(nombrepolycercle);
application.messagebox(pchar(rsPointsSaisis),
pchar(rsAttention), mb_ok);
end else     begin
   sauvevirtuel;
 Listepolycercle[-1+nombrepolycercle].dessine(BIBI);
 end;
 form1.image1.Cursor:=crdefault;
                     form1.StatusBar1.Panels[2].text:='';
                      form1.activeboutons;

                      end;
                     end;

 //form1.monimage1.free;
             rafraichit;
  affichecoordonnees(x1,y1);
  exit;
        end;


 if (ajoutpolycercle and (rectifait)) then begin
     point1x:=x1; pointrx:=x1; point1xi:=x; pointrxi:=x;
 point1y:=y1; pointry:=y1; point1yi:=y; pointryi:=y;
     inc(nbrectiact);

   AiAj:=sqrt(sqr(ttpoly[listerecti[nbrectiact]].ax-ttpoly[listerecti[nbrectiact]+1].ax)+
  sqr(ttpoly[listerecti[nbrectiact]].ay-ttpoly[listerecti[nbrectiact]+1].ay));
  ux:=(ttpoly[listerecti[nbrectiact]+1].ax-ttpoly[listerecti[nbrectiact]].ax)/AiAj;
  uy:=(ttpoly[listerecti[nbrectiact]+1].ay-ttpoly[listerecti[nbrectiact]].ay)/AiAj;
  lambda:=(
  (ttpoly[listerecti[nbrectiact]].ax+ttpoly[listerecti[nbrectiact]+1].ax)/2-
  x1)*ux+
  (
  (ttpoly[listerecti[nbrectiact]].ay+ttpoly[listerecti[nbrectiact]+1].ay)/2-
  y1)*uy ;
  tts[listerecti[nbrectiact]].ax:=x1+lambda*ux;
   tts[listerecti[nbrectiact]].ay:=y1+lambda*uy;
   {********************************}
 ix:=(ttpoly[listerecti[nbrectiact]].ax+ttpoly[listerecti[nbrectiact]+1].ax)/2;
  iy:=(ttpoly[listerecti[nbrectiact]].ay+ttpoly[listerecti[nbrectiact]+1].ay)/2;
    tt_rayon[listerecti[nbrectiact]]:=(sqr(tts[listerecti[nbrectiact]].ax-ix)+
    sqr(tts[listerecti[nbrectiact]].ay-iy)+sqr(ttpoly[listerecti[nbrectiact]].ax-ix)+
    sqr(ttpoly[listerecti[nbrectiact]].ay-iy))/2/sqrt(sqr(tts[listerecti[nbrectiact]].ax
    -ix)+
    sqr(tts[listerecti[nbrectiact]].ay-iy));
  ttc[listerecti[nbrectiact]].ax:=tts[listerecti[nbrectiact]].ax
  -tt_rayon[listerecti[nbrectiact]]
  *(ttpoly[listerecti[nbrectiact]+1].ay-ttpoly[listerecti[nbrectiact]].ay)/AiAj;
  ttc[listerecti[nbrectiact]].ay:=tts[listerecti[nbrectiact]].ay
  +tt_rayon[listerecti[nbrectiact]]
  *(ttpoly[listerecti[nbrectiact]+1].ax-ttpoly[listerecti[nbrectiact]].ax)/AiAj;
  if (tts[listerecti[nbrectiact]].ax-ix)
  *(ttc[listerecti[nbrectiact]].ax-ix)+(tts[listerecti[nbrectiact]].ay-iy)
  *(ttc[listerecti[nbrectiact]].ay-iy)>0 then begin
     ttc[listerecti[nbrectiact]].ax:=tts[listerecti[nbrectiact]].ax
     +tt_rayon[listerecti[nbrectiact]]
     *(ttpoly[listerecti[nbrectiact]+1].ay-ttpoly[listerecti[nbrectiact]].ay)/AiAj;
  ttc[listerecti[nbrectiact]].ay:=tts[listerecti[nbrectiact]].ay
  -tt_rayon[listerecti[nbrectiact]]
  *(ttpoly[listerecti[nbrectiact]+1].ax-ttpoly[listerecti[nbrectiact]].ax)/AiAj;
     end;

  cctheta1[listerecti[nbrectiact]]
  :=donneanglepolairedans02pi(ttpoly[listerecti[nbrectiact]].ax
  -ttc[listerecti[nbrectiact]].ax,
  ttpoly[listerecti[nbrectiact]].ay-ttc[listerecti[nbrectiact]].ay);
  cctheta2[listerecti[nbrectiact]]:=donneanglepolairedans02pi
  (ttpoly[listerecti[nbrectiact]+1].ax-
  ttc[listerecti[nbrectiact]].ax,
  ttpoly[listerecti[nbrectiact]+1].ay-ttc[listerecti[nbrectiact]].ay);

  if  (tts[listerecti[nbrectiact]].ax-ttc[listerecti[nbrectiact]].ax)
  *(ttpoly[listerecti[nbrectiact]+1].ay-ttpoly[listerecti[nbrectiact]].ay)-
  (tts[listerecti[nbrectiact]].ay-ttc[listerecti[nbrectiact]].ay)
  *(ttpoly[listerecti[nbrectiact]+1].ax-ttpoly[listerecti[nbrectiact]].ax)>0 then
  begin
  if cctheta2[listerecti[nbrectiact]]<cctheta1[listerecti[nbrectiact]]
   then cctheta1[listerecti[nbrectiact]]:=cctheta1[listerecti[nbrectiact]]-2*pi;
  end else begin
  if cctheta2[listerecti[nbrectiact]]>cctheta1[listerecti[nbrectiact]]
   then cctheta2[listerecti[nbrectiact]]:=cctheta2[listerecti[nbrectiact]]-2*pi;
  tt:=cctheta1[listerecti[nbrectiact]];
  cctheta1[listerecti[nbrectiact]]:=cctheta2[listerecti[nbrectiact]];
  cctheta2[listerecti[nbrectiact]]:=tt;
  end;

  {********************************}
  rafraichit;
  BIBI.trait(ttpoly[1].ax,ttpoly[1].ay,ttpoly[nbpoly].ax,
      ttpoly[nbpoly].ay,1,couleurmiroirplan,psdot,pmcopy,true);
      for iiii:=1 to nbpoly-1 do
       BIBI.trait(ttpoly[iiii].ax,ttpoly[iiii].ay,ttpoly[iiii+1].ax,
      ttpoly[iiii+1].ay,1,couleurmiroirplan,psdot,pmcopy,true);
  BIBI.cercle(tts[listerecti[nbrectiact]].ax,tts[listerecti[nbrectiact]].ay,3,
  1,clblack,pmcopy,true);
   for iiii:=1 to nbrectiact do
   begin
   BIBI.cercle(tts[listerecti[iiii]].ax,
   tts[listerecti[iiii]].ay,3,1,clblack,pmcopy,true);
   BIBI.arcdecercle(ttc[listerecti[iiii]].ax,ttc[listerecti[iiii]].ay,
   tt_rayon[listerecti[iiii]],1,cctheta1[listerecti[iiii]],cctheta2[listerecti[iiii]],
   clblack,pmcopy,psdot,true);
   end;
 for iiii:=1 to nbpoly do
   BIBI.trait(ttpoly[iiii].ax,ttpoly[iiii].ay,ttpoly[iiii+1].ax,ttpoly[iiii+1].ay,1,
     couleurmiroirplan,psdot,pmcopy,true);
     if nbrecti=nbrectiact then begin
    ReTaillePpolycercle(Listepolycercle,nombrepolycercle,nombrepolycercle+1);
inc(nombrepolycercle);
if not (listepolycercle[-1+nombrepolycercle].create(nbpoly,ttpoly,tts,youri,1.4,1.8,1.6,
clblack,couleurpolycercle,jamaisreflechi,jamaisreflechi)) then begin
ReTaillePpolycercle(Listepolycercle,nombrepolycercle,nombrepolycercle-1);
dec(nombrepolycercle);
application.messagebox(pchar(rsPointsSaisis),
pchar(rsAttention), mb_ok);
end else  begin
 Listepolycercle[-1+nombrepolycercle].dessine(BIBI);
    sauvevirtuel;
    end;
 form1.image1.Cursor:=crdefault;
                     form1.StatusBar1.Panels[2].text:='';
                      form1.activeboutons;
                      end;
 //form1.monimage1.free;
   rafraichit;
  affichecoordonnees(x1,y1);
  exit;
        end;



    if (not(point1fait) and not(ajoutpoly or ajoutpolycercle)) then begin
if (ajoutmiroirplan or ajoutreseau or ajoutecran or ajoutlsr or ajoutconiqueconcave or ajoutconiqueconvexe
or ajoutdiaphragme or ajoutlec or ajoutfleche or ajoutoeil)
 then BIBI.cercle(x1,y1,2*epaisseurmiroirplan,
epaisseurmiroirplan,couleurmiroirplan,pmcopy,true);
if ajoutsphere then  BIBI.cercle(x1,y1,2*epaisseursphere,
epaisseursphere,couleursphere,pmcopy,true);
if (ajoutmscopa or ajoutmscepa)then BIBI.cercle(x1,y1,2*epaisseurmscopa,
epaisseurmscopa,couleurmscopa,pmcopy,true);
if (ajoutmscore or ajoutmscere)then BIBI.cercle(x1,y1,2*epaisseurmscore,
epaisseurmscore,couleurmscore,pmcopy,true);
if ajoutrayon then BIBI.cercle(x1,y1,2*epaisseurrayon,
epaisseurrayon,couleurrayon,pmcopy,true);
if ajoutsourceponctuelle then BIBI.cercle(x1,y1,2*epaisseursourceponctuelle,
epaisseursourceponctuelle,couleursourceponctuelle,pmcopy,true);
if ajoutondeplane then BIBI.cercle(x1,y1,2*epaisseurondeplane,
epaisseurondeplane,couleurondeplane,pmcopy,true);
if ajoutlmc then BIBI.cercle(x1,y1,2*epaisseurlmc,
epaisseurlmc,couleurlmc,pmcopy,true);
if ajoutprisme then BIBI.cercle(x1,y1,2*epaisseurprisme,
epaisseurprisme,couleurprisme,pmcopy,true);
if ajoutlmd then BIBI.cercle(x1,y1,2*epaisseurlmd,
epaisseurlmd,couleurlmd,pmcopy,true);
point1x:=x1; pointrx:=x1; point1xi:=x; pointrxi:=x;
point1y:=y1; pointry:=y1; point1yi:=y; pointryi:=y;
if ajoutsourceponctuelle then begin
sourcex:=x1; sourcey:=y1;
end;
point1fait:=true;
 if ajoutsourceponctuelle then
 form1.image1.Cursor:=crPoint1 else
 if ajoutondeplane then
  form1.image1.Cursor:=crDirection else
form1.image1.Cursor:=crPoint2;
if ajoutrayon then form1.StatusBar1.Panels[2].text:=rsPositionnezL6;
if ajoutondeplane then form1.StatusBar1.Panels[2].text:=rsPositionnezL7;
if ajoutsourceponctuelle then
form1.StatusBar1.Panels[2].text:=rsPositionnezL8;
if ajoutsphere then   begin
form1.StatusBar1.Panels[2].text:=rsPositionnezU;
  form1.image1.Cursor:=crdefault;
  end;
  if ajoutlec then   begin
form1.StatusBar1.Panels[2].text:=rsPositionnezL9;
  form1.image1.Cursor:=crsommet2;
  end;
  if ajoutdiaphragme then begin
  form1.image1.Cursor:=crPoint1;
 form1.StatusBar1.Panels[2].text:=rsPositionnezL10;
 end;
if (ajoutmiroirplan or ajoutmscopa or ajoutmscepa
or ajoutmscere or ajoutmscore )
 then form1.StatusBar1.Panels[2].text:=
rsPositionnezL11;

        if (ajoutreseau )
         then form1.StatusBar1.Panels[2].text:=
         rsPositionnezL54;  ;

if ajoutfleche then
form1.StatusBar1.Panels[2].text:=
rsPositionnezL12;
if (ajoutconiqueconcave or ajoutconiqueconvexe)then begin
form1.StatusBar1.Panels[2].text:=
rsPositionnezL13;
 form1.image1.Cursor:=crfoyer;
 end;

if ajoutecran
then form1.StatusBar1.Panels[2].text:=
rsPositionnezL14;
 if ajoutoeil
then form1.StatusBar1.Panels[2].text:=
rsPositionnezL15;
if ajoutlsr
then form1.StatusBar1.Panels[2].text:=
rsPositionnezL16;

if ajoutprisme
then begin
form1.StatusBar1.Panels[2].text:=
rsPositionnezL17;
 form1.image1.Cursor:=crsommet;
               end;

if (ajoutlmc or ajoutlmd) then form1.StatusBar1.Panels[2].text:=
rsPositionnezL18;

             rafraichit;
  affichecoordonnees(x1,y1);
  exit;
  end;

  {2eme clic lec}
  if (ajoutlec and point1fait and not(point2fait)) then begin
  point2x:=x1; point2y:=y1;
  if sqrt(sqr(point1x-point2x)+sqr(point2y-point1y))<form1.taillemini then begin
  application.messagebox(pchar(rsSommetsTropP),pchar( rsAttention2), mb_ok);

  form1.activeboutons;
  form1.image1.Cursor:=crdefault;
                     form1.StatusBar1.Panels[2].text:='';
    rafraichit;
     BIBI.cercle(point1x,point1y,2,1,clblack,pmcopy,true);
    BIBI.cercle(point2x,point2y,2,1,clblack,pmcopy,true);
     bibi.trait(point1x,point1y,point2x,point2y,1,clblack,psdot,pmcopy,true);

  affichecoordonnees(x1,y1);
 exit;
 end;
 form1.image1.Cursor:=crcentre1;
 form1.StatusBar1.Panels[2].text:=
                     rsPositionnezL19;
                     point2fait:=true;   toutjustefait:=true;
 rafraichit;

 BIBI.cercle(x1,y1,2*epaisseurmiroirplan,
epaisseurmiroirplan,couleurpoignees,pmcopy,true);
    exit;
 end;

  {3eme clic lec}
  if (ajoutlec and point2fait and not(point3fait)) then begin
  point3x:=point1x+((x1-point1x)*(point2x-point1x)+(y1-point1y)*(point2y-point1y))*
  (point2x-point1x)/(sqr(point2x-point1x)+sqr(point2y-point1y));
   point3y:=point1y+((x1-point1x)*(point2x-point1x)+(y1-point1y)*(point2y-point1y))*
  (point2y-point1y)/(sqr(point2x-point1x)+sqr(point2y-point1y));
   form1.image1.Cursor:=crcentre2;
                     form1.StatusBar1.Panels[2].text:=
                     rsPositionnezL20;
      rafraichit;
      bibi.trait(point1x,point1y,point2x,point2y,1,clblack,psdot,pmcopy,true);
  BIBI.cercle(point3x,point3y,2*epaisseurmiroirplan,
epaisseurmiroirplan,couleurpoignees,pmcopy,true);
 BIBI.arcdecercle(point3x,point3y,sqrt(sqr(point3x-point1x)+sqr(point3y-point1y)),
epaisseurmiroirplan,0,2*pi-0.01,couleurmiroirplan,pmcopy,psdot,true);

                     point3fait:=true;    toutjustefait:=true;
                     //form1.monimage1.free;
  exit;
 end;

 {4eme clic lec}
  if (ajoutlec and point3fait ) then begin

  point4fait:=true;
   point4x:=point1x+((x1-point1x)*(point2x-point1x)+(y1-point1y)*(point2y-point1y))*
  (point2x-point1x)/(sqr(point2x-point1x)+sqr(point2y-point1y));
   point4y:=point1y+((x1-point1x)*(point2x-point1x)+(y1-point1y)*(point2y-point1y))*
  (point2y-point1y)/(sqr(point2x-point1x)+sqr(point2y-point1y));
   rafraichit;
  BIBI.cercle(point4x,point4y,2*epaisseurmiroirplan,
epaisseurmiroirplan,couleurpoignees,pmcopy,true);
 BIBI.arcdecercle(point4x,point4y,sqrt(sqr(point4x-point2x)+sqr(point4y-point2y)),
epaisseurmiroirplan,0,2*pi,couleurmiroirplan,pmcopy,psdot,true);
  BIBI.cercle(point3x,point3y,2*epaisseurmiroirplan,
epaisseurmiroirplan,couleurpoignees,pmcopy,true);
 BIBI.arcdecercle(point3x,point3y,sqrt(sqr(point3x-point1x)+sqr(point3y-point1y)),
epaisseurmiroirplan,0,2*pi-0.01,couleurmiroirplan,pmcopy,psdot,true);

  form1.image1.Cursor:=crdefault;
                     form1.StatusBar1.Panels[2].text:=
                     rsSaisissezLeD;

      with saisiediametre
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
    saisiediametre.log1.Caption:=unitelongueur;
   repeat
   until saisiediametre.showmodal=mrok;
if ((point3x-point1x)*(point2x-point1x)+(point3y-point1y)*(point2y-point1y)<0)
then
riri1:=sqrt(sqr(point3x-point1x)+sqr(point3y-point1y)) else
 riri1:=-sqrt(sqr(point3x-point1x)+sqr(point3y-point1y));

if ((point4x-point2x)*(point2x-point1x)+(point4y-point2y)*(point2y-point1y)<0)
then
riri2:=sqrt(sqr(point4x-point2x)+sqr(point4y-point2y)) else
 riri2:=-sqrt(sqr(point4x-point2x)+sqr(point4y-point2y));

 ReTaillePlec(Listelec,nombrelec,nombrelec+1);
inc(nombrelec);
if not
(Listelec[-1+nombrelec].create(point1x,point1y,point2x,point2y,riri1,riri2,diadia,
couleurlec,couleuraxelec,true,1.4,1.8,1.6,jamaisreflechi,jamaisreflechi)) then begin
ReTaillePlec(Listelec,nombrelec,nombrelec-1);
dec(nombrelec);
application.messagebox(pchar(rsDonnEsAberra),
pchar(rsAttention), mb_ok);
end else    sauvevirtuel;

             rafraichit;
  affichecoordonnees(x1,y1);
  form1.activeboutons;
  form1.image1.Cursor:=crdefault;
                     form1.StatusBar1.Panels[2].text:='';
 exit;
 end;






{clic exterieur sphere};
if ajoutsphere then begin

ReTaillePsphere(Listesphere,nombresphere,nombresphere+1);
inc(nombresphere);
if not
(Listesphere[-1+nombresphere].create(point1x,point1y,sqrt(sqr(x1-point1x)+sqr(y1-point1y)),
1.4,1.8,1.6,clblack,couleursphere,jamaisreflechi,jamaisreflechi)) then begin
ReTaillePsphere(Listesphere,nombresphere,nombresphere-1);
dec(nombresphere);
application.messagebox(pchar(rsRayonDeLaSph),
pchar(rsAttention), mb_ok);
end else  begin
 Listesphere[-1+nombresphere].dessine(BIBI);
    sauvevirtuel; end;
 //form1.monimage1.free;
             rafraichit;
  affichecoordonnees(x1,y1);
  form1.activeboutons;
  form1.image1.Cursor:=crdefault;
                     form1.StatusBar1.Panels[2].text:='';
 exit;
 end;



{clic sur le point 2 du miroir plan}
if ajoutmiroirplan  then begin
ReTaillePMiroirPlan(ListeMiroirPlan,nombremiroirplan,nombremiroirplan+1);
inc(nombremiroirplan);
if not
(ListeMiroirPlan[-1+nombremiroirplan].create(point1x,point1y,x1,y1,epaisseurmiroirplan,
couleurmiroirplan,true)) then begin
ReTaillePMiroirPlan(ListeMiroirPlan,nombremiroirplan,nombremiroirplan-1);
dec(nombremiroirplan);
application.messagebox(pchar(rsTailleDuMiro),
pchar(rsAttention), mb_ok);
end else begin
 ListeMiroirPlan[-1+nombremiroirplan].dessine(BIBI);
    sauvevirtuel;
 end;
 //form1.monimage1.free;
             rafraichit;
  affichecoordonnees(x1,y1);
  form1.activeboutons;
  form1.image1.Cursor:=crdefault;
                     form1.StatusBar1.Panels[2].text:='';
 exit;
 end;



{clic sur le point 2 du reseau}
if ajoutreseau  then begin
ReTaillePreseau(Listereseau,nombrereseau,nombrereseau+1);
inc(nombrereseau);
if not
(Listereseau[-1+nombrereseau].create(point1x,point1y,x1,y1,epaisseurreseau,
couleurreseau,1e-3/500,true,false,-1,1)) then begin
ReTaillePreseau(Listereseau,nombrereseau,nombrereseau-1);
dec(nombrereseau);
application.messagebox(pchar(rsTailleDuRSea2),
pchar(rsAttention), mb_ok);
end else begin
 Listereseau[-1+nombrereseau].dessine(BIBI);
    sauvevirtuel;

       with fsaisiereseau
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
     fsaisiereseau.boutonsup.enabled:=true;
    fsaisiereseau.editx1.text:=floattostr(listeReseau[-1+nombrereseau].a1x);
    fsaisiereseau.editx2.text:=floattostr(listeReseau[-1+nombrereseau].a2x);
    fsaisiereseau.edity1.text:=floattostr(listeReseau[-1+nombrereseau].a1y);
    fsaisiereseau.edity2.text:=floattostr(listeReseau[-1+nombrereseau].a2y);
    fsaisiereseau.cochehachures.checked:=listeReseau[-1+nombrereseau].hachures;
    fsaisiereseau.editepaisseur.Value:=listeReseau[-1+nombrereseau].epaisseur;
     fsaisiereseau.spinedit_ordre_min.value:=listeReseau[-1+nombrereseau].ordre_min;
      fsaisiereseau.spinedit_ordre_max.value:=listeReseau[-1+nombrereseau].ordre_max;
      fsaisiereseau.FloatSpinEdit1.value:=listeReseau[-1+nombrereseau].nombre_trait_par_mm;
      if listeReseau[-1+nombrereseau].entransmission then
      fsaisiereseau.radiogroup1.itemindex:=0 else fsaisiereseau.radiogroup1.itemindex:=1;
fsaisiereseau.colorgrid1.selected:=(
listeReseau[-1+nombrereseau].couleur);
    fsaisiereseau.caption:=
    rsPropriTSDeCe24;
     fsaisiereseau.log1.Caption:=unitelongueur;
     fsaisiereseau.log2.Caption:=unitelongueur;
      fsaisiereseau.log3.Caption:=unitelongueur;
       fsaisiereseau.log4.Caption:=unitelongueur;

 if fsaisiereseau.showmodal=mrok then
  begin
 listeReseau[-2+nombreReseau].create(
 listeReseau[-1+nombreReseau].a1x,
 listeReseau[-1+nombreReseau].a1y,
 listeReseau[-1+nombreReseau].a2x,
 listeReseau[-1+nombreReseau].a2y,
 listeReseau[-1+nombreReseau].epaisseur,
 listeReseau[-1+nombreReseau].couleur,
 listeReseau[-1+nombreReseau].pas,
 listeReseau[-1+nombreReseau].entransmission,
 listeReseau[-1+nombreReseau].hachures,
 listeReseau[-1+nombreReseau].ordre_min,
 listeReseau[-1+nombreReseau].ordre_max);
 retaillepReseau(listeReseau,nombreReseau,nombreReseau-1);
 dec(nombreReseau);
   sauvevirtuel;
 end;




 end;
 //form1.monimage1.free;
             rafraichit;
  affichecoordonnees(x1,y1);
  form1.activeboutons;
  form1.image1.Cursor:=crdefault;
                     form1.StatusBar1.Panels[2].text:='';
 exit;
 end;



 if ajoutfleche  then begin
ReTaillePfleche(Listefleche,nombrefleche,nombrefleche+1);
inc(nombrefleche);

 with saisiefleche
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisiefleche.showmodal;
if not
(Listefleche[-1+nombrefleche].create(point1x,point1y,x1,y1,saisiefle.ra_epaisseur,
saisiefle.ra_couleur,saisiefle.ra_ext1,saisiefle.ra_ext2,saisiefle.ra_styletrait,
saisiefle.ra_taille1,saisiefle.ra_taille2)) then begin
ReTaillePfleche(Listefleche,nombrefleche,nombrefleche-1);
dec(nombrefleche);
application.messagebox(pchar(rsTailleDuTrai),
pchar(rsAttention), mb_ok);
end else   begin    sauvevirtuel;
 Listefleche[-1+nombrefleche].dessine(BIBI);
 end;
 //form1.monimage1.free;
             rafraichit;
  affichecoordonnees(x1,y1);
  form1.activeboutons;
  form1.image1.Cursor:=crdefault;
                     form1.StatusBar1.Panels[2].text:='';
 exit;
 end;

 {clic sur le point 2 de ecran}
if ajoutecran  then begin
ReTaillePecran(Listeecran,nombreecran,nombreecran+1);
inc(nombreecran);
if not
(Listeecran[-1+nombreecran].create(point1x,point1y,x1,y1,epaisseurecran,
couleurecran)) then begin
ReTaillePecran(Listeecran,nombreecran,nombreecran-1);
dec(nombreecran);
application.messagebox(pchar(rsTailleDeLCra),
pchar(rsAttention), mb_ok);
end else    begin
 Listeecran[-1+nombreecran].dessine(BIBI);
    sauvevirtuel; end;
 //form1.monimage1.free;
             rafraichit;
  affichecoordonnees(x1,y1);
  form1.activeboutons;
  form1.image1.Cursor:=crdefault;
                     form1.StatusBar1.Panels[2].text:='';
 exit;
 end;

 {clic sur le point 2 de oeil}
if ajoutoeil  then begin
ReTaillePoeil(Listeoeil,nombreoeil,nombreoeil+1);
inc(nombreoeil);
if not
(Listeoeil[-1+nombreoeil].create(point1x,point1y,x1,y1,epaisseuroeil,
couleuroeil)) then begin
ReTaillePoeil(Listeoeil,nombreoeil,nombreoeil-1);
dec(nombreoeil);
application.messagebox(pchar(rsTailleDeLOei),
pchar(rsAttention), mb_ok);
end else   begin
 Listeoeil[-1+nombreoeil].dessine(BIBI);
    sauvevirtuel; end;
 //form1.monimage1.free;
             rafraichit;
  affichecoordonnees(x1,y1);
  form1.activeboutons;
  form1.image1.Cursor:=crdefault;
                     form1.StatusBar1.Panels[2].text:='';
 exit;
 end;

 {clic sur le point 2 de lsr}
if ajoutlsr  then begin
ReTaillePlsr(Listelsr,nombrelsr,nombrelsr+1);
inc(nombrelsr);
if not
(Listelsr[-1+nombrelsr].create(point1x,point1y,x1,y1,epaisseurlsr,
couleurlsr)) then begin
ReTaillePlsr(Listelsr,nombrelsr,nombrelsr-1);
dec(nombrelsr);
application.messagebox(pchar(rsTailleDeLaLa),
pchar(rsAttention), mb_ok);
end else       begin
 Listelsr[-1+nombrelsr].dessine(BIBI);      sauvevirtuel; end;
 //form1.monimage1.free;
             rafraichit;
  affichecoordonnees(x1,y1);
  form1.activeboutons;
  form1.image1.Cursor:=crdefault;
                     form1.StatusBar1.Panels[2].text:='';
 exit;
 end;


 {clic sur le point 2 du rayon}
if ajoutrayon  then begin
ReTaillePrayon(Listerayon,nombrerayon,nombrerayon+1);
inc(nombrerayon);
if not(
listerayon[-1+nombrerayon].create(point1x,point1y,x1-point1x,y1-point1y,
couleurrayon,epaisseurrayon,'','',10,maxmaxsegment)) then begin
ReTaillePrayon(Listerayon,nombrerayon,nombrerayon-1);
dec(nombrerayon);
application.messagebox(pchar(rsPointsTropPr),
pchar(rsAttention), mb_ok);
end else       begin    sauvevirtuel;

 listerayon[-1+nombrerayon].dessine(BIBI);   end;
 //form1.monimage1.free;
             rafraichit;
 affichecoordonnees(x1,y1);
  form1.activeboutons;
  form1.image1.Cursor:=crdefault;
                     form1.StatusBar1.Panels[2].text:='';
  exit;
 end;

 {clic sur le point 1 de la source ponctuelle}
if (ajoutsourceponctuelle and not(point2fait) and point1fait)  then begin
point1x:=x1; point1y:=y1;
if sqrt(sqr(sourcex-point1x)+sqr(sourcey-point1y))<form1.taillemini
 then begin
 application.messagebox(pchar(rsPointsTropPr),
pchar(rsAttention), mb_ok);
  //form1.monimage1.free;
             rafraichit;
             exit; end;

 affichecoordonnees(x1,y1);
  form1.image1.Cursor:=crpoint2;
                     form1.StatusBar1.Panels[2].text:=
                     rsPositionnezL21;
                     point2fait:=true;
                     rafraichit;
                     BIBI.trait(sourcex,sourcey,point1x,point1y,1,couleursourceponctuelle,pssolid,pmcopy,true);
  exit;
 end;


 {clic sur le point D de l'onde plane}
if (ajoutondeplane and not(point2fait) and point1fait)  then begin
directionx:=x1; directiony:=y1;
if sqrt(sqr(directionx-point1x)+sqr(directiony-point1y))<form1.taillemini
 then begin
 application.messagebox(pchar(rsPointsTropPr),
pchar(rsAttention), mb_ok);
  //form1.monimage1.free;
             rafraichit;
             exit; end;

 affichecoordonnees(x1,y1);
  form1.image1.Cursor:=crpoint2;
                     form1.StatusBar1.Panels[2].text:=
                     rsPositionnezL22;
                     point2fait:=true;
                      rafraichit;
                      BIBI.trait(point1x,point1y,directionx,directiony,1,couleurondeplane,pssolid,pmcopy,true);
  exit;
 end;


 {clic sur le point 2 de l'onde plane}
if (ajoutondeplane and point2fait)  then begin
if ((sqrt(sqr(x1-point1x)+sqr(y1-point1y))<form1.taillemini) or
  (sqrt(sqr(x1-directionx)+sqr(y1-directiony))<form1.taillemini))
 then begin
 application.messagebox(pchar(rsPointsTropPr),
pchar(rsAttention), mb_ok);
  //form1.monimage1.free;
             rafraichit;
             exit; end;

   with saisienombrerayons
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
rafraichit;
BIBI.trait(point1x,point1y,directionx,directiony,1,couleurondeplane,pssolid,pmcopy,true);
    BIBI.trait(point1x,point1y,x1,y1,1,couleurondeplane,pssolid,pmcopy,true);
    BIBI.cercle(point1x,point1y,2*epaisseurondeplane,epaisseurondeplane,
    couleurondeplane,pmcopy,true);
     BIBI.cercle(directionx,directiony,2*epaisseurondeplane,epaisseurondeplane,
    couleurondeplane,pmcopy,true);
      BIBI.cercle(x1,y1,2*epaisseurondeplane,epaisseurondeplane,
    couleurondeplane,pmcopy,true);

 saisienombrerayons.showmodal;
 ReTaillePondeplane(Listeondeplane,nombreondeplane,nombreondeplane+1);
inc(nombreondeplane);
if not(listeondeplane[-1+nombreondeplane].create(nbsaisie,directionx,directiony,point1x,
point1y,x1,y1,couleurondeplane,epaisseurondeplane,'','',10,false,'',maxmaxsegment)) then begin
ReTaillePondeplane(Listeondeplane,nombreondeplane,nombreondeplane-1);
dec(nombreondeplane);
application.messagebox(pchar(rsPointsTropPr),
pchar(rsAttention), mb_ok);
  //form1.monimage1.free;
             rafraichit;
             exit; end;
                sauvevirtuel;
 listeondeplane[-1+nombreondeplane].dessine(BIBI);
 //form1.monimage1.free;
             rafraichit;
  affichecoordonnees(x1,y1);
  form1.image1.Cursor:=crdefault;
  form1.activeboutons;
                     form1.StatusBar1.Panels[2].text:='';
  exit;
 end;


  {clic sur le point 2 de la source ponctuelle}
if (ajoutsourceponctuelle and point2fait)  then begin
if ((sqrt(sqr(sourcex-x1)+sqr(sourcey-y1))<form1.taillemini) or
    (sqrt(sqr(point1x-x1)+sqr(point1y-y1))<form1.taillemini))
 then begin
 application.messagebox(pchar(rsPointsTropPr),
pchar(rsAttention), mb_ok);

             rafraichit;
             exit; end;
        rafraichit;
        BIBI.trait(sourcex,sourcey,point1x,point1y,1,couleursourceponctuelle,pssolid,pmcopy,true);

    BIBI.trait(sourcex,sourcey,x1,y1,1,couleursourceponctuelle,pssolid,pmcopy,true);
    BIBI.cercle(sourcex,sourcey,2*epaisseursourceponctuelle,epaisseursourceponctuelle,
    couleursourceponctuelle,pmcopy,true);
     BIBI.cercle(point1x,point1y,2*epaisseursourceponctuelle,epaisseursourceponctuelle,
    couleursourceponctuelle,pmcopy,true);
     BIBI.cercle(x1,y1,2*epaisseursourceponctuelle,epaisseursourceponctuelle,
    couleursourceponctuelle,pmcopy,true);
  with saisienombrerayons
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
if genrevirtuel then StaticText1.Caption:=
rsNombreDeRayo
else StaticText1.Caption:=
rsNombreDeRayo2;   ;
        end;

          saisienombrerayons.showmodal;

 ReTaillePSourcePonctuelle(listesourceponctuelle,nombresourceponctuelle,
 nombresourceponctuelle+1);
inc(nombresourceponctuelle);
if not(genrevirtuel) then
if not(listesourceponctuelle[-1+nombresourceponctuelle].create(nbsaisie,sourcex,sourcey,point1x,
point1y,x1,y1,couleursourceponctuelle,epaisseursourceponctuelle,'','',10,false,false,'',maxmaxsegment)) then begin
ReTaillePSourcePonctuelle(listesourceponctuelle,nombresourceponctuelle,
 nombresourceponctuelle-1);
dec(nombresourceponctuelle);
application.messagebox(pchar(rsPointsTropPr),
pchar(rsAttention), mb_ok);
  //form1.monimage1.free;
             rafraichit;
             exit; end;

           if  (genrevirtuel) then
if not(listesourceponctuelle[-1+nombresourceponctuelle].create(nbsaisie,sourcex,sourcey,point1x,
point1y,x1,y1,couleursourceponctuelle,epaisseursourceponctuelle,'(1)','',10,true,false,'',maxmaxsegment)) then begin
ReTaillePSourcePonctuelle(listesourceponctuelle,nombresourceponctuelle,
 nombresourceponctuelle-1);
dec(nombresourceponctuelle);
application.messagebox(pchar(rsPointsTropPr),
pchar(rsAttention), mb_ok);
  //form1.monimage1.free;
             rafraichit;
             exit; end;
                sauvevirtuel;
 listesourceponctuelle[-1+nombresourceponctuelle].dessine(BIBI);
 //form1.monimage1.free;
             rafraichit;
  affichecoordonnees(x1,y1);
  form1.activeboutons;
  form1.image1.Cursor:=crdefault;
                     form1.StatusBar1.Panels[2].text:='';
  exit;
 end;

     {clic point 1 du diaphragme}
 if ((ajoutdiaphragme) and not( point2fait)) then begin
 point2x:=x1; point2y:=y1;
 if sqrt((point2x-point1x)*(point2x-point1x)+(point2y-point1y)*(point2y-point1y))<form1.taillemini
 then begin
 application.messagebox(pchar(rsRayonIntRieu),
pchar(rsAttention), mb_ok);
  //form1.monimage1.free;
             rafraichit;
             exit; end;

 point2fait:=true;
  BIBI.trait(point1x,point1y,point2x,point2y,1,clblack,pssolid,pmcopy,true);
   form1.StatusBar1.Panels[2].text:=rsPositionnezL23;
   form1.image1.Cursor:=crpoint2;
    axefocal_x1:=point2x;
   axefocal_y1:=point2y;
   axefocal_anglepolaire:=donneanglepolairedanspipi(point2x-point1x,point2y-point1y);

  axefocal_normalex:=cos(axefocal_anglepolaire);
  axefocal_normaley:=sin(axefocal_anglepolaire);

  if axefocal_normalex=0 then
  if axefocal_normaley>0 then begin
  axefocal_x2:=axefocal_x1;
   axefocal_y2:=yymax;
   end else begin
    axefocal_x2:=axefocal_x1;
   axefocal_y2:=yymin;
   end;

  if ((axefocal_normaley=0) and (axefocal_normalex<>0)) then
  if axefocal_normalex>0 then begin
  axefocal_x2:=xxmax;
   axefocal_y2:=axefocal_y1;
   end else begin
    axefocal_x2:=xxmin;
   axefocal_y2:=axefocal_y1;
   end;

   if ((axefocal_normaley<>0) and (axefocal_normalex<>0)) then
    if ( (axefocal_y1+(xxmin-axefocal_x1)*axefocal_normaley/axefocal_normalex<=yymax)
    and  (axefocal_y1+(xxmin-axefocal_x1)*axefocal_normaley/axefocal_normalex>=yymin) and
    (axefocal_normalex<0))
    then begin
    axefocal_y2:=axefocal_y1+(xxmin-axefocal_x1)*axefocal_normaley/axefocal_normalex;
    axefocal_x2:=xxmin;
    end else
     if ( (axefocal_y1+(xxmax-axefocal_x1)*axefocal_normaley/axefocal_normalex<=yymax)
    and  (axefocal_y1+(xxmax-axefocal_x1)*axefocal_normaley/axefocal_normalex>=yymin) and
    (axefocal_normalex>0))
    then begin
    axefocal_y2:=axefocal_y1+(xxmax-axefocal_x1)*axefocal_normaley/axefocal_normalex;
    axefocal_x2:=xxmax;
    end else
     if ( (axefocal_x1+(yymin-axefocal_y1)*axefocal_normalex/axefocal_normaley<=xxmax)
    and  (axefocal_x1+(yymin-axefocal_y1)*axefocal_normalex/axefocal_normaley>=xxmin) and
    (axefocal_normaley<0))
    then begin
    axefocal_x2:=axefocal_x1+(yymin-axefocal_y1)*axefocal_normalex/axefocal_normaley;
    axefocal_y2:=yymin;
    end else
     if ( (axefocal_x1+(yymax-axefocal_y1)*axefocal_normalex/axefocal_normaley<=xxmax)
    and  (axefocal_x1+(yymax-axefocal_y1)*axefocal_normalex/axefocal_normaley>=xxmin) and
    (axefocal_normaley>0))
    then begin
    axefocal_x2:=axefocal_x1+(yymax-axefocal_y1)*axefocal_normalex/axefocal_normaley;
    axefocal_y2:=yymax;
    end;
  BIBI.trait(axefocal_x1,axefocal_y1,axefocal_x2,axefocal_y2,1,
  clblack,psdashdot,pmcopy,true);
    //form1.monimage1.free;
             rafraichit;
 affichecoordonnees(x1,y1);
  exit;
  end;



      {clic point 2 du miroir spherique paraxial}
 if ((ajoutmscopa or ajoutmscepa) and not( point2fait)) then begin
 point2x:=x1; point2y:=y1;
 if sqrt((point2x-point1x)*(point2x-point1x)+(point2y-point1y)*(point2y-point1y))<form1.taillemini
 then begin
 application.messagebox(pchar(rsTailleDuMiro2),
pchar(rsAttention), mb_ok);
  //form1.monimage1.free;
             rafraichit;
             exit; end;

 point2fait:=true;
  BIBI.trait(point1x,point1y,point2x,point2y,epaisseurmscopa,couleurmscopa,pssolid,pmcopy,true);
   form1.StatusBar1.Panels[2].text:=rsPositionnezL24;
   form1.image1.Cursor:=crfoyer;
   axefocal_x1:=(point1x+point2x)/2;
   axefocal_y1:=(point1y+point2y)/2;
   if (point1x=point2x) then
  if (point2y>point1y) then axefocal_anglepolaire:=Pi/2 else axefocal_anglepolaire:=-Pi/2
  else begin
  axefocal_anglepolaire:=arctan((point2y-point1y)/(point2x-point1x));
  if (point2x<point1x) then axefocal_anglepolaire:=axefocal_anglepolaire+Pi;
  end;
  if ajoutmscepa then axefocal_anglepolaire:=axefocal_anglepolaire+Pi;
  if axefocal_anglepolaire>Pi then axefocal_anglepolaire:=axefocal_anglepolaire-Pi*2;
  axefocal_normalex:=cos(axefocal_anglepolaire+Pi/2);
  axefocal_normaley:=sin(axefocal_anglepolaire+Pi/2);

  if axefocal_normalex=0 then
  if axefocal_normaley>0 then begin
  axefocal_x2:=axefocal_x1;
   axefocal_y2:=yymax;
   end else begin
    axefocal_x2:=axefocal_x1;
   axefocal_y2:=yymin;
   end;

  if ((axefocal_normaley=0) and (axefocal_normalex<>0)) then
  if axefocal_normalex>0 then begin
  axefocal_x2:=xxmax;
   axefocal_y2:=axefocal_y1;
   end else begin
    axefocal_x2:=xxmin;
   axefocal_y2:=axefocal_y1;
   end;

   if ((axefocal_normaley<>0) and (axefocal_normalex<>0)) then
    if ( (axefocal_y1+(xxmin-axefocal_x1)*axefocal_normaley/axefocal_normalex<=yymax)
    and  (axefocal_y1+(xxmin-axefocal_x1)*axefocal_normaley/axefocal_normalex>=yymin) and
    (axefocal_normalex<0))
    then begin
    axefocal_y2:=axefocal_y1+(xxmin-axefocal_x1)*axefocal_normaley/axefocal_normalex;
    axefocal_x2:=xxmin;
    end else
     if ( (axefocal_y1+(xxmax-axefocal_x1)*axefocal_normaley/axefocal_normalex<=yymax)
    and  (axefocal_y1+(xxmax-axefocal_x1)*axefocal_normaley/axefocal_normalex>=yymin) and
    (axefocal_normalex>0))
    then begin
    axefocal_y2:=axefocal_y1+(xxmax-axefocal_x1)*axefocal_normaley/axefocal_normalex;
    axefocal_x2:=xxmax;
    end else
     if ( (axefocal_x1+(yymin-axefocal_y1)*axefocal_normalex/axefocal_normaley<=xxmax)
    and  (axefocal_x1+(yymin-axefocal_y1)*axefocal_normalex/axefocal_normaley>=xxmin) and
    (axefocal_normaley<0))
    then begin
    axefocal_x2:=axefocal_x1+(yymin-axefocal_y1)*axefocal_normalex/axefocal_normaley;
    axefocal_y2:=yymin;
    end else
     if ( (axefocal_x1+(yymax-axefocal_y1)*axefocal_normalex/axefocal_normaley<=xxmax)
    and  (axefocal_x1+(yymax-axefocal_y1)*axefocal_normalex/axefocal_normaley>=xxmin) and
    (axefocal_normaley>0))
    then begin
    axefocal_x2:=axefocal_x1+(yymax-axefocal_y1)*axefocal_normalex/axefocal_normaley;
    axefocal_y2:=yymax;
    end;
  BIBI.trait(axefocal_x1,axefocal_y1,axefocal_x2,axefocal_y2,epaisseurmscopa,
  couleuraxemscopa,psdashdot,pmcopy,true);
   //form1.monimage1.free;
             rafraichit;
 affichecoordonnees(x1,y1);
  exit;
  end;


   {clic point 2 du miroir spherique reel}
 if ((ajoutmscore or ajoutmscere) and not( point2fait)) then begin
 point2x:=x1; point2y:=y1;
 if sqrt((point2x-point1x)*(point2x-point1x)+(point2y-point1y)*(point2y-point1y))<form1.taillemini
 then begin
 application.messagebox(pchar(rsTailleDuMiro2),
pchar(rsAttention), mb_ok);
  //form1.monimage1.free;
             rafraichit;
             exit; end;
      toutjustefait:=true;
 point2fait:=true;
  {BIBI.trait(point1x,point1y,point2x,point2y,epaisseurmscore,couleurmscore,pssolid,pmcopy,true); }
  pointrx:=(point1x+point2x)/2;
  pointry:=(point1y+point2y)/2;
   form1.StatusBar1.Panels[2].text:=rsPositionnezL25;
   form1.image1.Cursor:=crcentre;
     axefocal_ox:=(point1x+point2x)/2;
   axefocal_oy:=(point1y+point2y)/2;
   if (point1x=point2x) then
  if (point2y>point1y) then axefocal_anglepolaire:=Pi/2 else axefocal_anglepolaire:=-Pi/2
  else begin
  axefocal_anglepolaire:=arctan((point2y-point1y)/(point2x-point1x));
  if (point2x<point1x) then axefocal_anglepolaire:=axefocal_anglepolaire+Pi;
  end;

  if axefocal_anglepolaire>Pi then axefocal_anglepolaire:=axefocal_anglepolaire-Pi*2;
  axefocal_normalex:=cos(axefocal_anglepolaire+Pi/2);
  axefocal_normaley:=sin(axefocal_anglepolaire+Pi/2);

  if axefocal_normalex=0 then
  if axefocal_normaley>0 then begin
  axefocal_x1:=axefocal_ox;
   axefocal_y1:=yymax;
   end else begin
    axefocal_x1:=axefocal_ox;
   axefocal_y1:=yymin;
   end;

  if ((axefocal_normaley=0) and (axefocal_normalex<>0)) then
  if axefocal_normalex>0 then begin
  axefocal_x1:=xxmax;
   axefocal_y1:=axefocal_oy;
   end else begin
    axefocal_x1:=xxmin;
   axefocal_y1:=axefocal_oy;
   end;

   if ((axefocal_normaley<>0) and (axefocal_normalex<>0)) then
    if ( (axefocal_oy+(xxmin-axefocal_ox)*axefocal_normaley/axefocal_normalex<=yymax)
    and  (axefocal_oy+(xxmin-axefocal_ox)*axefocal_normaley/axefocal_normalex>=yymin) and
    (axefocal_normalex<0))
    then begin
    axefocal_y1:=axefocal_oy+(xxmin-axefocal_ox)*axefocal_normaley/axefocal_normalex;
    axefocal_x1:=xxmin;
    end else
     if ( (axefocal_oy+(xxmax-axefocal_ox)*axefocal_normaley/axefocal_normalex<=yymax)
    and  (axefocal_oy+(xxmax-axefocal_ox)*axefocal_normaley/axefocal_normalex>=yymin) and
    (axefocal_normalex>0))
    then begin
    axefocal_y1:=axefocal_oy+(xxmax-axefocal_ox)*axefocal_normaley/axefocal_normalex;
    axefocal_x1:=xxmax;
    end else
     if ( (axefocal_ox+(yymin-axefocal_oy)*axefocal_normalex/axefocal_normaley<=xxmax)
    and  (axefocal_ox+(yymin-axefocal_oy)*axefocal_normalex/axefocal_normaley>=xxmin) and
    (axefocal_normaley<0))
    then begin
    axefocal_x1:=axefocal_ox+(yymin-axefocal_oy)*axefocal_normalex/axefocal_normaley;
    axefocal_y1:=yymin;
    end else
     if ( (axefocal_ox+(yymax-axefocal_oy)*axefocal_normalex/axefocal_normaley<=xxmax)
    and  (axefocal_ox+(yymax-axefocal_oy)*axefocal_normalex/axefocal_normaley>=xxmin) and
    (axefocal_normaley>0))
    then begin
    axefocal_x1:=axefocal_ox+(yymax-axefocal_oy)*axefocal_normalex/axefocal_normaley;
    axefocal_y1:=yymax;
    end;

    axefocal_anglepolaire:=axefocal_anglepolaire+Pi;
  if axefocal_anglepolaire>Pi then axefocal_anglepolaire:=axefocal_anglepolaire-Pi*2;
  axefocal_normalex:=cos(axefocal_anglepolaire+Pi/2);
  axefocal_normaley:=sin(axefocal_anglepolaire+Pi/2);

  if axefocal_normalex=0 then
  if axefocal_normaley>0 then begin
  axefocal_x2:=axefocal_ox;
   axefocal_y2:=yymax;
   end else begin
    axefocal_x2:=axefocal_ox;
   axefocal_y2:=yymin;
   end;

  if ((axefocal_normaley=0) and (axefocal_normalex<>0)) then
  if axefocal_normalex>0 then begin
  axefocal_x2:=xxmax;
   axefocal_y2:=axefocal_oy;
   end else begin
    axefocal_x2:=xxmin;
   axefocal_y2:=axefocal_oy;
   end;

   if ((axefocal_normaley<>0) and (axefocal_normalex<>0)) then
    if ( (axefocal_oy+(xxmin-axefocal_ox)*axefocal_normaley/axefocal_normalex<=yymax)
    and  (axefocal_oy+(xxmin-axefocal_ox)*axefocal_normaley/axefocal_normalex>=yymin) and
    (axefocal_normalex<0))
    then begin
    axefocal_y2:=axefocal_oy+(xxmin-axefocal_ox)*axefocal_normaley/axefocal_normalex;
    axefocal_x2:=xxmin;
    end else
     if ( (axefocal_oy+(xxmax-axefocal_ox)*axefocal_normaley/axefocal_normalex<=yymax)
    and  (axefocal_oy+(xxmax-axefocal_ox)*axefocal_normaley/axefocal_normalex>=yymin) and
    (axefocal_normalex>0))
    then begin
    axefocal_y2:=axefocal_oy+(xxmax-axefocal_ox)*axefocal_normaley/axefocal_normalex;
    axefocal_x2:=xxmax;
    end else
     if ( (axefocal_ox+(yymin-axefocal_oy)*axefocal_normalex/axefocal_normaley<=xxmax)
    and  (axefocal_ox+(yymin-axefocal_oy)*axefocal_normalex/axefocal_normaley>=xxmin) and
    (axefocal_normaley<0))
    then begin
    axefocal_x2:=axefocal_ox+(yymin-axefocal_oy)*axefocal_normalex/axefocal_normaley;
    axefocal_y2:=yymin;
    end else
     if ( (axefocal_ox+(yymax-axefocal_oy)*axefocal_normalex/axefocal_normaley<=xxmax)
    and  (axefocal_ox+(yymax-axefocal_oy)*axefocal_normalex/axefocal_normaley>=xxmin) and
    (axefocal_normaley>0))
    then begin
    axefocal_x2:=axefocal_ox+(yymax-axefocal_oy)*axefocal_normalex/axefocal_normaley;
    axefocal_y2:=yymax;
    end;

  BIBI.trait(axefocal_x1,axefocal_y1,axefocal_x2,axefocal_y2,epaisseurmscore,
  couleuraxemscore,psdashdot,pmcopy,true);
   //form1.monimage1.free;
             rafraichit;
 affichecoordonnees(x1,y1);
  exit;
  end;

   {clic foyer du miroir conique}
 if ((ajoutconiqueconcave or ajoutconiqueconvexe) and point1fait and not( foyerfait)) then begin
 foyerx:=x1; foyery:=y1;
 if sqrt(sqr(foyerx-point1x)+sqr(foyery-point1y))<form1.taillemini
 then begin
 application.messagebox(pchar(rsPointsTropPr),
pchar(rsAttention), mb_ok);
  //form1.monimage1.free;
             rafraichit;
             exit; end;

 foyerfait:=true;
   BIBI.cercle(foyerx,foyery,3,1,clblack,pmcopy,true);
   form1.StatusBar1.Panels[2].text:=rsPositionnezL26;
   form1.image1.Cursor:=crsommet;
   //form1.monimage1.free;
             rafraichit;
  affichecoordonnees(x1,y1);
  exit;
             end;

  {clic sommet miroir conique}
  if ((ajoutconiqueconcave or ajoutconiqueconvexe) and  foyerfait and not(sommetfait))
    then begin
    sommetx:=x1; sommety:=y1;  sommetfait:=true;
    if (   (sqrt(sqr(sommetx-point1x)+sqr(sommety-point1y))<form1.taillemini) or
      (sqrt(sqr(foyerx-sommetx)+sqr(foyery-sommety))<form1.taillemini) )
 then begin
 application.messagebox(pchar(rsPointsTropPr),
pchar(rsAttention), mb_ok);
form1.StatusBar1.Panels[2].text:='';
   form1.image1.Cursor:=crdefault;
  //form1.monimage1.free;      form1.activeboutons;
             rafraichit;
             exit; end;
    co_theta0:=donneanglepolairedans02pi(sommetx-foyerx,sommety-foyery);
    co_fs:=sqrt(sqr(sommetx-foyerx)+sqr(sommety-foyery));
    co_thetaM:=donneanglepolairedans02pi(point1x-foyerx,point1y-foyery);
    co_rm:=sqrt(sqr(point1x-foyerx)+sqr( point1y-foyery));
    if (co_fs=co_rm*cos(co_thetaM-co_theta0)) then begin
    application.messagebox(pchar(rsDonnEsDeCons),
pchar(rsAttention), mb_ok);
  //form1.monimage1.free;
  form1.StatusBar1.Panels[2].text:='';
   form1.image1.Cursor:=crdefault;
   form1.activeboutons;
             rafraichit;
             exit; end;

             co_e:=(co_rm-co_fs)/(co_fs-co_rm*cos(co_thetaM-co_theta0));
             co_p:=co_fs*(co_rm-co_rm*cos(co_thetaM-co_theta0))/(co_fs-co_rm*cos(co_thetaM-co_theta0));
         if ((co_e<0) or (co_p<=0))
          then begin
    application.messagebox(pchar(rsDonnEsDeCons),
pchar(rsAttention), mb_ok);
  //form1.monimage1.free;   form1.activeboutons;
             rafraichit;
             exit; end;

  BIBI.cercle(foyerx,foyery,3,1,clblack,pmcopy,true);
 // form1.monimage1.traceconique(co_theta0,-pi,pi,foyerx,foyery,co_e,co_p,clblack);
   form1.StatusBar1.Panels[2].text:=rsPourFinirPos;
   form1.image1.Cursor:=crpoint2;
   //form1.monimage1.free;
             rafraichit;
  affichecoordonnees(x1,y1);
  exit;
  end;

   {clic point1 miroirconique}
    if ((ajoutconiqueconcave or ajoutconiqueconvexe) and  sommetfait) then begin


   ReTaillePmiroirconique(listemiroirconique,nombremiroirconique,nombremiroirconique+1);
   inc(nombremiroirconique);


if ajoutconiqueconcave then
if not(listemiroirconique[-1+nombremiroirconique].create(foyerx,foyery,sommetx,sommety,
point1x,point1y,
foyerx+cos(donneanglepolairedanspipi(x1-foyerx,y1-foyery))*co_p/(1+co_e*cos(-co_theta0+
donneanglepolairedanspipi(x1-foyerx,y1-foyery))),
foyery+sin(donneanglepolairedanspipi(x1-foyerx,y1-foyery))*co_p/(1+co_e*cos(-co_theta0+
donneanglepolairedanspipi(x1-foyerx,y1-foyery))),true,couleurmiroirconique,couleuraxemiroirconique,true,true))then begin
ReTaillePmiroirconique(listemiroirconique,nombremiroirconique,nombremiroirconique-1);
dec(nombremiroirconique);
application.messagebox(pchar(rsDonnEsDeCons2),
pchar(rsAttention), mb_ok);
end else    begin
 listemiroirconique[-1+nombremiroirconique].dessine(BIBI);
    sauvevirtuel; end;

 if ajoutconiqueconvexe then
if not(listemiroirconique[-1+nombremiroirconique].create(foyerx,foyery,sommetx,sommety,
point1x,point1y,
foyerx+cos(donneanglepolairedanspipi(x1-foyerx,y1-foyery))*co_p/(1+co_e*cos(-co_theta0+
donneanglepolairedanspipi(x1-foyerx,y1-foyery))),
foyery+sin(donneanglepolairedanspipi(x1-foyerx,y1-foyery))*co_p/(1+co_e*cos(-co_theta0+
donneanglepolairedanspipi(x1-foyerx,y1-foyery))),false,couleurmiroirconique,couleuraxemiroirconique,true,true)) then begin
ReTaillePmiroirconique(listemiroirconique,nombremiroirconique,nombremiroirconique-1);
dec(nombremiroirconique);
application.messagebox(pchar(rsDonnEsDeCons2),
pchar(rsAttention), mb_ok);
end else       begin
 listemiroirconique[-1+nombremiroirconique].dessine(BIBI);
        sauvevirtuel; end;
 //form1.monimage1.free;
             rafraichit;
  affichecoordonnees(x1,y1);
  form1.activeboutons;
  form1.image1.Cursor:=crdefault;
                     form1.StatusBar1.Panels[2].text:='';
 exit;

  end;

  {clic sommet prisme}
 if ((ajoutprisme)and not( point2fait)) then begin
 point2x:=x1; point2y:=y1;  point3x:=x1; point3y:=x1; point4x:=x1; point4y:=y1;
 if sqrt((point2x-point1x)*(point2x-point1x)+(point2y-point1y)*(point2y-point1y))<form1.taillemini
 then begin
 application.messagebox(pchar(rsTailleDuPris),
pchar(rsAttention), mb_ok);
  //form1.monimage1.free;
             rafraichit;
             exit; end;
    form1.image1.Cursor:=crfin;
                     form1.StatusBar1.Panels[2].text:=rsDernierPoint;
   point2fait:=true;
   BIBI.trait(point1x,point1y,point2x,point2y,1,clblack,psdot,pmcopy,true);
    rafraichit;
  affichecoordonnees(x1,y1);
  exit;

 end;


  {clic point 2 de la lentille}
 if ((ajoutlmc or ajoutlmd) and not( point2fait)) then begin
 point2x:=x1; point2y:=y1;
 if sqrt((point2x-point1x)*(point2x-point1x)+(point2y-point1y)*(point2y-point1y))<form1.taillemini
 then begin
 application.messagebox(pchar(rsTailleDeLaLe),
pchar(rsAttention), mb_ok);
  //form1.monimage1.free;
             rafraichit;
             exit; end;


 point2fait:=true;
   if ajoutlmc then BIBI.trait(point1x,point1y,point2x,point2y,epaisseurlmc,couleurlmc,pssolid,pmcopy,true);
   if ajoutlmd then BIBI.trait(point1x,point1y,point2x,point2y,epaisseurlmd,couleurlmd,pssolid,pmcopy,true);
   form1.StatusBar1.Panels[2].text:=rsPositionnezU2;
   form1.image1.Cursor:=crfoyer;
   axefocal_ox:=(point1x+point2x)/2;
   axefocal_oy:=(point1y+point2y)/2;
   if (point1x=point2x) then
  if (point2y>point1y) then axefocal_anglepolaire:=Pi/2 else axefocal_anglepolaire:=-Pi/2
  else begin
  axefocal_anglepolaire:=arctan((point2y-point1y)/(point2x-point1x));
  if (point2x<point1x) then axefocal_anglepolaire:=axefocal_anglepolaire+Pi;
  end;

  if axefocal_anglepolaire>Pi then axefocal_anglepolaire:=axefocal_anglepolaire-Pi*2;
  axefocal_normalex:=cos(axefocal_anglepolaire+Pi/2);
  axefocal_normaley:=sin(axefocal_anglepolaire+Pi/2);

  if axefocal_normalex=0 then
  if axefocal_normaley>0 then begin
  axefocal_x1:=axefocal_ox;
   axefocal_y1:=yymax;
   end else begin
    axefocal_x1:=axefocal_ox;
   axefocal_y1:=yymin;
   end;

  if ((axefocal_normaley=0) and (axefocal_normalex<>0)) then
  if axefocal_normalex>0 then begin
  axefocal_x1:=xxmax;
   axefocal_y1:=axefocal_oy;
   end else begin
    axefocal_x1:=xxmin;
   axefocal_y1:=axefocal_oy;
   end;

   if ((axefocal_normaley<>0) and (axefocal_normalex<>0)) then
    if ( (axefocal_oy+(xxmin-axefocal_ox)*axefocal_normaley/axefocal_normalex<=yymax)
    and  (axefocal_oy+(xxmin-axefocal_ox)*axefocal_normaley/axefocal_normalex>=yymin) and
    (axefocal_normalex<0))
    then begin
    axefocal_y1:=axefocal_oy+(xxmin-axefocal_ox)*axefocal_normaley/axefocal_normalex;
    axefocal_x1:=xxmin;
    end else
     if ( (axefocal_oy+(xxmax-axefocal_ox)*axefocal_normaley/axefocal_normalex<=yymax)
    and  (axefocal_oy+(xxmax-axefocal_ox)*axefocal_normaley/axefocal_normalex>=yymin) and
    (axefocal_normalex>0))
    then begin
    axefocal_y1:=axefocal_oy+(xxmax-axefocal_ox)*axefocal_normaley/axefocal_normalex;
    axefocal_x1:=xxmax;
    end else
     if ( (axefocal_ox+(yymin-axefocal_oy)*axefocal_normalex/axefocal_normaley<=xxmax)
    and  (axefocal_ox+(yymin-axefocal_oy)*axefocal_normalex/axefocal_normaley>=xxmin) and
    (axefocal_normaley<0))
    then begin
    axefocal_x1:=axefocal_ox+(yymin-axefocal_oy)*axefocal_normalex/axefocal_normaley;
    axefocal_y1:=yymin;
    end else
     if ( (axefocal_ox+(yymax-axefocal_oy)*axefocal_normalex/axefocal_normaley<=xxmax)
    and  (axefocal_ox+(yymax-axefocal_oy)*axefocal_normalex/axefocal_normaley>=xxmin) and
    (axefocal_normaley>0))
    then begin
    axefocal_x1:=axefocal_ox+(yymax-axefocal_oy)*axefocal_normalex/axefocal_normaley;
    axefocal_y1:=yymax;
    end;

    axefocal_anglepolaire:=axefocal_anglepolaire+Pi;
  if axefocal_anglepolaire>Pi then axefocal_anglepolaire:=axefocal_anglepolaire-Pi*2;
  axefocal_normalex:=cos(axefocal_anglepolaire+Pi/2);
  axefocal_normaley:=sin(axefocal_anglepolaire+Pi/2);

  if axefocal_normalex=0 then
  if axefocal_normaley>0 then begin
  axefocal_x2:=axefocal_ox;
   axefocal_y2:=yymax;
   end else begin
    axefocal_x2:=axefocal_ox;
   axefocal_y2:=yymin;
   end;

  if ((axefocal_normaley=0) and (axefocal_normalex<>0)) then
  if axefocal_normalex>0 then begin
  axefocal_x2:=xxmax;
   axefocal_y2:=axefocal_oy;
   end else begin
    axefocal_x2:=xxmin;
   axefocal_y2:=axefocal_oy;
   end;

   if ((axefocal_normaley<>0) and (axefocal_normalex<>0)) then
    if ( (axefocal_oy+(xxmin-axefocal_ox)*axefocal_normaley/axefocal_normalex<=yymax)
    and  (axefocal_oy+(xxmin-axefocal_ox)*axefocal_normaley/axefocal_normalex>=yymin) and
    (axefocal_normalex<0))
    then begin
    axefocal_y2:=axefocal_oy+(xxmin-axefocal_ox)*axefocal_normaley/axefocal_normalex;
    axefocal_x2:=xxmin;
    end else
     if ( (axefocal_oy+(xxmax-axefocal_ox)*axefocal_normaley/axefocal_normalex<=yymax)
    and  (axefocal_oy+(xxmax-axefocal_ox)*axefocal_normaley/axefocal_normalex>=yymin) and
    (axefocal_normalex>0))
    then begin
    axefocal_y2:=axefocal_oy+(xxmax-axefocal_ox)*axefocal_normaley/axefocal_normalex;
    axefocal_x2:=xxmax;
    end else
     if ( (axefocal_ox+(yymin-axefocal_oy)*axefocal_normalex/axefocal_normaley<=xxmax)
    and  (axefocal_ox+(yymin-axefocal_oy)*axefocal_normalex/axefocal_normaley>=xxmin) and
    (axefocal_normaley<0))
    then begin
    axefocal_x2:=axefocal_ox+(yymin-axefocal_oy)*axefocal_normalex/axefocal_normaley;
    axefocal_y2:=yymin;
    end else
     if ( (axefocal_ox+(yymax-axefocal_oy)*axefocal_normalex/axefocal_normaley<=xxmax)
    and  (axefocal_ox+(yymax-axefocal_oy)*axefocal_normalex/axefocal_normaley>=xxmin) and
    (axefocal_normaley>0))
    then begin
    axefocal_x2:=axefocal_ox+(yymax-axefocal_oy)*axefocal_normalex/axefocal_normaley;
    axefocal_y2:=yymax;
    end;

  if ajoutlmc then BIBI.trait(axefocal_x2,axefocal_y2,axefocal_x1,axefocal_y1,epaisseurlmc,
  couleuraxelmc,psdashdot,pmcopy,true);
  if ajoutlmd then BIBI.trait(axefocal_x2,axefocal_y2,axefocal_x1,axefocal_y1,epaisseurlmd,
  couleuraxelmd,psdashdot,pmcopy,true);
   //form1.monimage1.free;
             rafraichit;
  affichecoordonnees(x1,y1);
  exit;
  end;

  if (ajoutdiaphragme and point2fait) then begin
  lambda:=((x1-axefocal_x1)*(axefocal_x2-axefocal_x1)+(y1-axefocal_y1)*
      (axefocal_y2-axefocal_y1))/((axefocal_x2-axefocal_x1)*(axefocal_x2-axefocal_x1)+
      (axefocal_y2-axefocal_y1)*(axefocal_y2-axefocal_y1));
      if lambda<0 then lambda:=0;
       pointrx:=axefocal_x1+lambda*(axefocal_x2-axefocal_x1);
        pointry:=axefocal_y1+lambda*(axefocal_y2-axefocal_y1);
         foyerfait:=true;
       retaillepdiaphragme(listediaphragme,nombrediaphragme,nombrediaphragme+1);
       inc(nombrediaphragme);
       if not(listediaphragme[-1+nombrediaphragme].create(point1x,point1y,point2x,point2y,
       pointrx,pointry,couleurdiaphragme,epaisseurdiaphragme)) then begin
         retaillepdiaphragme(listediaphragme,nombrediaphragme,nombrediaphragme-1);
       dec(nombrediaphragme);
       application.messagebox(pchar(rsEpaisseurDuD),
pchar(rsAttention), mb_ok);
end else     begin
 listediaphragme[-1+nombrediaphragme].dessine(BIBI);
    sauvevirtuel;
    end;
   //form1.monimage1.free;
             rafraichit;
  affichecoordonnees(x1,y1);
  form1.activeboutons;
  form1.image1.Cursor:=crdefault;
                     form1.StatusBar1.Panels[2].text:='';
 exit;

  end;


  if ((ajoutmscopa or ajoutmscepa) and point2fait) then begin
  {clic foyer miroir spherique}
      lambda:=((x1-axefocal_x1)*(axefocal_x2-axefocal_x1)+(y1-axefocal_y1)*
      (axefocal_y2-axefocal_y1))/((axefocal_x2-axefocal_x1)*(axefocal_x2-axefocal_x1)+
      (axefocal_y2-axefocal_y1)*(axefocal_y2-axefocal_y1));
      if lambda<0 then lambda:=0;
       pointrx:=axefocal_x1+lambda*(axefocal_x2-axefocal_x1);
        pointry:=axefocal_y1+lambda*(axefocal_y2-axefocal_y1);
  foyerfait:=true;
   if ajoutmscopa then begin
   ReTaillePMscopa(listemscopa,nombremscopa,nombremscopa+1);
   inc(nombremscopa);
   end;
    if ajoutmscepa then begin
    ReTaillePMscepa(listemscepa,nombremscepa,nombremscepa+1);
    inc(nombremscepa);
    end;
if ajoutmscopa then if not(listemscopa[-1+nombremscopa].create(point1x,point1y,point2x,point2y,pointrx,pointry,
epaisseurmscopa,couleurmscopa,couleuraxemscopa,true,true,false,0))then begin
ReTaillePMscopa(listemscopa,nombremscopa,nombremscopa-1);
dec(nombremscopa);
application.messagebox(pchar(rsTailleDuMiro3),
pchar(rsAttention), mb_ok);
end else         begin
 listemscopa[-1+nombremscopa].dessine(BIBI);
           sauvevirtuel; end;
 if ajoutmscepa then if not(listemscepa[-1+nombremscepa].create(point1x,point1y,point2x,point2y,pointrx,pointry,
epaisseurmscepa,couleurmscepa,couleuraxemscepa,true,true,false,0))then begin
ReTaillePMscepa(listemscepa,nombremscepa,nombremscepa-1);
dec(nombremscepa);
application.messagebox(pchar(rsTailleDuMiro4),
pchar(rsAttention), mb_ok);
end else    begin
 listemscepa[-1+nombremscepa].dessine(BIBI);
    sauvevirtuel; end;
//form1.monimage1.free;
             rafraichit;
  affichecoordonnees(x1,y1);
  form1.activeboutons;
  form1.image1.Cursor:=crdefault;
                     form1.StatusBar1.Panels[2].text:='';
 exit;

  end;

      if ((ajoutmscore or ajoutmscere) and point2fait) then begin
    {clic centre miroir spheriquereel}
      lambda:=((x1-axefocal_x1)*(axefocal_x2-axefocal_x1)+(y1-axefocal_y1)*
      (axefocal_y2-axefocal_y1))/((axefocal_x2-axefocal_x1)*(axefocal_x2-axefocal_x1)+
      (axefocal_y2-axefocal_y1)*(axefocal_y2-axefocal_y1));
       pointrx:=axefocal_x1+lambda*(axefocal_x2-axefocal_x1);
        pointry:=axefocal_y1+lambda*(axefocal_y2-axefocal_y1);
       { if ((pointrx+sqrt((pointrx-point1x)*(pointrx-point1x)+(pointry-point1y)*
        (pointry-point1y))>xxmax) or
        (pointrx-sqrt((pointrx-point1x)*(pointrx-point1x)+(pointry-point1y)*
        (pointry-point1y))<xxmin) or
         (pointry+sqrt((pointrx-point1x)*(pointrx-point1x)+(pointry-point1y)*
        (pointry-point1y))>yymax) or
        (pointry-sqrt((pointrx-point1x)*(pointrx-point1x)+(pointry-point1y)*
        (pointry-point1y))<yymin)) then begin
        lambda:=0;
         application.messagebox('Le miroir déborde de l''espace de travail, repositionnez le centre de courbure !',
pchar(rsattention),mb_ok);
exit;
end;  }
        pointrx:=axefocal_x1+lambda*(axefocal_x2-axefocal_x1);
        pointry:=axefocal_y1+lambda*(axefocal_y2-axefocal_y1);
  foyerfait:=true;
   if ajoutmscore then begin
   ReTaillePMscore(listemscore,nombremscore,nombremscore+1);
   inc(nombremscore);
   end;
    if ajoutmscere then begin
    ReTaillePMscere(listemscere,nombremscere,nombremscere+1);
    inc(nombremscere);
    end;
if ajoutmscore then if not(listemscore[-1+nombremscore].create(point1x,point1y,point2x,point2y,pointrx,pointry,
epaisseurmscore,couleurmscore,couleuraxemscore,true,true,false,0))then begin
ReTaillePMscore(listemscore,nombremscore,nombremscore-1);
dec(nombremscore);
application.messagebox(pchar(rsTailleDuMiro5),
pchar(rsAttention), mb_ok);
end else      begin
 listemscore[-1+nombremscore].dessine(BIBI);
    sauvevirtuel; end;

 if ajoutmscere then if not(listemscere[-1+nombremscere].create(point1x,point1y,point2x,point2y,pointrx,pointry,
epaisseurmscere,couleurmscere,couleuraxemscere,true,true,false,0))then begin
ReTaillePMscere(listemscere,nombremscere,nombremscere-1);
dec(nombremscere);
application.messagebox(pchar(rsTailleDuMiro6),
pchar(rsAttention), mb_ok);
end else   begin
 listemscere[-1+nombremscere].dessine(BIBI);
    sauvevirtuel; end;
 //form1.monimage1.free;
             rafraichit;
  affichecoordonnees(x1,y1);
  form1.activeboutons;
  form1.image1.Cursor:=crdefault;
                     form1.StatusBar1.Panels[2].text:='';
 exit;

  end;

  {clic 3eme point prisme}
    if ((ajoutprisme) and point2fait) then begin
      ux:=(point2x-point1x)/sqrt(sqr(point2x-point1x)+sqr(point2y-point1y));
       uy:=(point2y-point1y)/sqrt(sqr(point2x-point1x)+sqr(point2y-point1y));
      hx:=point1x-(point2x-point1x)/2;
      hy:=point1y-(point2y-point1y)/2;
      lambda:=(x1-hx)*ux+(y1-hy)*uy;

     ReTaillePprisme(Listeprisme,nombreprisme,nombreprisme+1);
inc(nombreprisme);
       if not(listeprisme[-1+nombreprisme].create(point1x,point1y,point2x,point2y,
       x1-lambda*ux,y1-lambda*uy,1.4,1.8,1.6,
clblack,couleurprisme,jamaisreflechi,jamaisreflechi))then begin
ReTaillePprisme(listeprisme,nombreprisme,nombreprisme-1);
dec(nombreprisme);
application.messagebox(pchar(rsDonnEsAberra2),
pchar(rsAttention), mb_ok);
end else  begin
 listeprisme[-1+nombreprisme].dessine(BIBI);
    sauvevirtuel; end;
//form1.monimage1.free;
             rafraichit;
  affichecoordonnees(x1,y1);
  form1.activeboutons;
  form1.image1.Cursor:=crdefault;
                     form1.StatusBar1.Panels[2].text:='';
 exit;




    end;


    if ((ajoutlmc or ajoutlmd) and point2fait) then begin
  {clic foyer lentille}
      lambda:=((x1-axefocal_x1)*(axefocal_x2-axefocal_x1)+(y1-axefocal_y1)*
      (axefocal_y2-axefocal_y1))/((axefocal_x2-axefocal_x1)*(axefocal_x2-axefocal_x1)+
      (axefocal_y2-axefocal_y1)*(axefocal_y2-axefocal_y1));
       pointrx:=axefocal_x1+lambda*(axefocal_x2-axefocal_x1);
        pointry:=axefocal_y1+lambda*(axefocal_y2-axefocal_y1);
  foyerfait:=true;
   if ajoutlmc then begin
   ReTaillePLmc(listeLmc,nombreLmc,nombreLmc+1);
   inc(nombrelmc);
   end;
    if ajoutlmd then begin
    ReTaillePLmd(listeLmd,nombreLmd,nombreLmd+1);
    inc(nombrelmd);
    end;
if ajoutlmc then if not(listelmc[-1+nombrelmc].create(point1x,point1y,point2x,point2y,pointrx,pointry,
epaisseurlmc,couleurlmc,couleuraxelmc,true))then begin
ReTaillePLmc(listeLmc,nombreLmc,nombreLmc-1);
dec(nombrelmc);
application.messagebox(pchar(rsDonnEsAberra2),
pchar(rsAttention), mb_ok);
end else  begin
 listelmc[-1+nombrelmc].dessine(BIBI);
      sauvevirtuel; end;
 if ajoutlmd then if not(listelmd[-1+nombrelmd].create(point1x,point1y,point2x,point2y,pointrx,pointry,
epaisseurlmd,couleurlmd,couleuraxelmd,true))then begin
ReTaillePLmd(listeLmd,nombreLmd,nombreLmd-1);
dec(nombrelmd);
application.messagebox(pchar(rsDonnEsAberra2),
pchar(rsAttention), mb_ok);
end else  begin
 listelmd[-1+nombrelmd].dessine(BIBI);
    sauvevirtuel; end;
//form1.monimage1.free;
             rafraichit;
  affichecoordonnees(x1,y1);
  form1.activeboutons;
  form1.image1.Cursor:=crdefault;
                     form1.StatusBar1.Panels[2].text:='';
 exit;

  end;


end;

  procedure TForm1.Image1MouseMove(Sender: TObject; Shift: TShiftState; X,
    Y: Integer);

  var

     x1,y1,lambda,tata1,tata2,tx,ty,rra,tt1,tt2,ux,uy,hx,hy,incertitudex,incertitudey:extended;
     i,j,xec,yec,iii:integer;
     ppoint:tpoint; posi:tpoint;
  label 667,668,666;
begin
{sous gtk2, ca ne suit pas le mouvement de la souris: on rerecupere la
position du curseur, pas actualisee assez rapidement}
 getcursorpos(posi);
  image1.OnMouseMove:=nil;
  application.ProcessMessages;
   image1.OnMouseMove:=@image1mousemove;
    posi:=image1.ScreenToClient(posi);
    x:=posi.x; y:=posi.y;

  if creation then exit;
if modegroupement then begin
    traitegroupement(sender,shift,x,y);
    exit;
    end;
if modemodif then begin
    traitemodif(sender,shift,x,y);
    exit;
    end;
if modesuppression then begin
traitesuppression(sender,shift,x,y);
    exit;
    end;



 if (ssalt in shift) then  x:=lastxf;
if (ssctrl in shift)  then  y:=lastyf;


{  BIBI.LimitesEtAxes(xxmin,xxmax,yymin,yymax,yymin,yymax,form1.image1.width,
form1.image1.height,'',policedefaut,true,clwhite,false,0,clblack,false,0,0,clblack,
policedefaut,false,false,{grillex,grilley,}1,clblack,clblack,'','','','','','',false,false,false);
BIBI.background(couleurfondsimulation);
if tracergrille then BIBI.dessinergrille(grillex,grilley);}
//application.ProcessMessages;
graphepresent:=true;
    BIBI.convert(x,y,x1,y1,true);





    if ((modedistance or modeangle) and not(point1fait)) then begin
    pointrx:=x1; pointry:=y1;
     affichecoordonnees(pointrx,pointry);
  //form1.monimage1.free;
  exit;
  end;

  if (modedistance and point1fait) then begin
    BIBI.trait(point1x,point1y,pointrx,pointry,1,clblack,psdot,pmxor,true);
     BIBI.trait(point1x,point1y,x1,y1,1,clblack,psdot,pmxor,true);

  pointrx:=x1; pointry:=y1;
   affichecoordonnees(pointrx,pointry);

 goto 666;
       end;

        if (modeangle and point1fait and not(point2fait)) then begin
        BIBI.cercle(point1x,point1y,3,1,clblack,pmcopy,true);
    BIBI.trait(point1x,point1y,pointrx,pointry,1,clblack,psdot,pmxor,true);
     BIBI.trait(point1x,point1y,x1,y1,1,clblack,psdot,pmxor,true);

  pointrx:=x1; pointry:=y1;
   affichecoordonnees(pointrx,pointry);

  goto 666;
       end;


     if (modeangle and point2fait) then begin
     BIBI.cercle(point1x,point1y,3,1,clblack,pmcopy,true);
     BIBI.cercle(point2x,point2y,3,1,clblack,pmcopy,true);
     BIBI.trait(point1x,point1y,point2x,point2y,1,clblack,pssolid,pmcopy,true);
    BIBI.trait(point1x,point1y,pointrx,pointry,1,clblack,psdot,pmxor,true);
     BIBI.trait(point1x,point1y,x1,y1,1,clblack,psdot,pmxor,true);

  pointrx:=x1; pointry:=y1;
   affichecoordonnees(pointrx,pointry);
goto 666;
       end;


 if (not(form1.ajoutencours) and
 (nombremiroirplan+nombrereseau+nombremscopa+nombremscepa+nombremscore+nombremscere+
 nombrelmc+nombrelmd+nombrerayon+nombreondeplane+nombresourceponctuelle+nombreecran+
 nombrelsr+nombrepolyhedre+nombresphere+nombremiroirconique+nombretexte
 +nombrediaphragme+nombrelec+nombremetre+nombreangle+nombrepolycercle+nombrefleche+nombreoeil+nombreprisme<>0)
 and (modeinfo)) then begin
 pointrx:=x1; pointry:=y1;
       BIBI.convert(3,form1.image1.height-3,tx,ty,true);
        tx:=tx-xxmin; ty:=ty-yymin;
 //form1.monimage1.free;
   affichecoordonnees(pointrx,pointry);


 if not(modeduplication) then begin

  if nombremetre>0 then
 for i:=1 to nombremetre do
   if ((abs(listemetre[-1+i].cx-x1)<=tx*1.1) and
    (abs(listemetre[-1+i].cy-y1)<=ty*1.1)) then goto 667;

     if nombrefleche>0 then
 for i:=1 to nombrefleche do
   if ((abs(listefleche[-1+i].cx-x1)<=tx*1.1) and
    (abs(listefleche[-1+i].cy-y1)<=ty*1.1)) then goto 667;


    if nombreangle>0 then
 for i:=1 to nombreangle do
   if ((abs(listeangle[-1+i].cx-x1)<=tx*1.1) and
    (abs(listeangle[-1+i].cy-y1)<=ty*1.1)) then goto 667;



 if nombretexte>0 then
 for i:=1 to nombretexte do
   if ((abs(listetexteaffiche[-1+i].x-x1)<=tx*1.1) and
    (abs(listetexteaffiche[-1+i].y-y1)<=ty*1.1)) then goto 667;


 if nombresphere>0 then
 for i:=1 to nombresphere do
   if ((abs(listesphere[-1+i].cx-x1)<=tx*1.1) and
    (abs(listesphere[-1+i].cy-y1)<=ty*1.1)) then goto 667;

    if nombresphere>0 then
 for i:=1 to nombresphere do
   if ((abs(listesphere[-1+i].cx+listesphere[-1+i].rayon-x1)<=tx*1.1) and
    (abs(listesphere[-1+i].cy-y1)<=ty*1.1)) then goto 667;

     if nombresphere>0 then
 for i:=1 to nombresphere do
   if ((abs(listesphere[-1+i].cx-x1)<=tx*1.1) and
    (abs(listesphere[-1+i].cy+listesphere[-1+i].rayon-y1)<=ty*1.1)) then goto 667;

     if nombresphere>0 then
 for i:=1 to nombresphere do
   if ((abs(listesphere[-1+i].cx-listesphere[-1+i].rayon-x1)<=tx*1.1) and
    (abs(listesphere[-1+i].cy-y1)<=ty*1.1)) then goto 667;

     if nombresphere>0 then
 for i:=1 to nombresphere do
   if ((abs(listesphere[-1+i].cx-x1)<=tx*1.1) and
    (abs(listesphere[-1+i].cy-listesphere[-1+i].rayon-y1)<=ty*1.1)) then goto 667;

    if nombremiroirconique>0 then
 for i:=1 to nombremiroirconique do
   if ((abs(listemiroirconique[-1+i].sx-x1)<=tx*1.1) and
    (abs(listemiroirconique[-1+i].sy-y1)<=ty*1.1)) then goto 667;

    if nombremiroirconique>0 then
 for i:=1 to nombremiroirconique do
   if ((abs(listemiroirconique[-1+i].fx-x1)<=tx*1.1) and
    (abs(listemiroirconique[-1+i].fy-y1)<=ty*1.1)) then goto 667;

    if nombremiroirconique>0 then
 for i:=1 to nombremiroirconique do
   if ((abs(listemiroirconique[-1+i].a1x-x1)<=tx*1.1) and
    (abs(listemiroirconique[-1+i].a1y-y1)<=ty*1.1)) then goto 667;

    if nombremiroirconique>0 then
 for i:=1 to nombremiroirconique do
   if ((abs(listemiroirconique[-1+i].a2x-x1)<=tx*1.1) and
    (abs(listemiroirconique[-1+i].a2y-y1)<=ty*1.1)) then goto 667;



 if nombremiroirplan>0 then
 for i:=1 to nombremiroirplan do
   if ((abs(listemiroirplan[-1+i].cx-x1)<=tx*1.1) and
    (abs(listemiroirplan[-1+i].cy-y1)<=ty*1.1)) then goto 667;

     if nombremiroirplan>0 then
 for i:=1 to nombremiroirplan do
   if ((abs(listemiroirplan[-1+i].a1x-x1)<=tx*1.1) and
    (abs(listemiroirplan[-1+i].a1y-y1)<=ty*1.1)) then goto 667;

     if nombremiroirplan>0 then
 for i:=1 to nombremiroirplan do
   if ((abs(listemiroirplan[-1+i].a2x-x1)<=tx*1.1) and
    (abs(listemiroirplan[-1+i].a2y-y1)<=ty*1.1)) then goto 667;

      if nombreReseau>0 then
 for i:=1 to nombreReseau do
   if ((abs(listeReseau[-1+i].cx-x1)<=tx*1.1) and
    (abs(listeReseau[-1+i].cy-y1)<=ty*1.1)) then goto 667;

     if nombreReseau>0 then
 for i:=1 to nombreReseau do
   if ((abs(listeReseau[-1+i].a1x-x1)<=tx*1.1) and
    (abs(listeReseau[-1+i].a1y-y1)<=ty*1.1)) then goto 667;

     if nombreReseau>0 then
 for i:=1 to nombreReseau do
   if ((abs(listeReseau[-1+i].a2x-x1)<=tx*1.1) and
    (abs(listeReseau[-1+i].a2y-y1)<=ty*1.1)) then goto 667;

     if nombrepolyhedre>0 then
 for i:=1 to nombrepolyhedre do
 for j:=1 to listepolyhedre[-1+i].nombresommet do
   if ((abs(listepolyhedre[-1+i].messommets[j].ax-x1)<=tx*1.1) and
    (abs(listepolyhedre[-1+i].messommets[j].ay-y1)<=ty*1.1)) then goto 667;

     if nombreprisme>0 then
 for i:=1 to nombreprisme do
 for j:=1 to listeprisme[-1+i].nombresommet do
   if ((abs(listeprisme[-1+i].messommets[j].ax-x1)<=tx*1.1) and
    (abs(listeprisme[-1+i].messommets[j].ay-y1)<=ty*1.1)) then goto 667;


     if nombrepolycercle>0 then
 for i:=1 to nombrepolycercle do
 for j:=1 to listepolycercle[-1+i].nombresommet do
   if ((abs(listepolycercle[-1+i].messommets[j].ax-x1)<=tx*1.1) and
    (abs(listepolycercle[-1+i].messommets[j].ay-y1)<=ty*1.1)) then goto 667;


     if nombreoeil>0 then
 for i:=1 to nombreoeil do
   if ((abs(listeoeil[-1+i].cx-x1)<=tx*1.1) and
    (abs(listeoeil[-1+i].cy-y1)<=ty*1.1)) then goto 667;

     if nombreoeil>0 then
 for i:=1 to nombreoeil do
   if ((abs(listeoeil[-1+i].ax-x1)<=tx*1.1) and
    (abs(listeoeil[-1+i].ay-y1)<=ty*1.1)) then goto 667;


     if nombreecran>0 then
 for i:=1 to nombreecran do
   if ((abs(listeecran[-1+i].cx-x1)<=tx*1.1) and
    (abs(listeecran[-1+i].cy-y1)<=ty*1.1)) then goto 667;

     if nombreecran>0 then
 for i:=1 to nombreecran do
   if ((abs(listeecran[-1+i].a1x-x1)<=tx*1.1) and
    (abs(listeecran[-1+i].a1y-y1)<=ty*1.1)) then goto 667;

     if nombreecran>0 then
 for i:=1 to nombreecran do
   if ((abs(listeecran[-1+i].a2x-x1)<=tx*1.1) and
    (abs(listeecran[-1+i].a2y-y1)<=ty*1.1)) then goto 667;

    if nombrediaphragme>0 then
 for i:=1 to nombrediaphragme do
   if ((abs(listediaphragme[-1+i].cx-x1)<=tx*1.1) and
    (abs(listediaphragme[-1+i].cy-y1)<=ty*1.1)) then goto 667;

     if nombrediaphragme>0 then
 for i:=1 to nombrediaphragme do
   if ((abs(listediaphragme[-1+i].a1x-x1)<=tx*1.1) and
    (abs(listediaphragme[-1+i].a1y-y1)<=ty*1.1)) then goto 667;

     if nombrediaphragme>0 then
 for i:=1 to nombrediaphragme do
   if ((abs(listediaphragme[-1+i].a2x-x1)<=tx*1.1) and
    (abs(listediaphragme[-1+i].a2y-y1)<=ty*1.1)) then goto 667;

    if nombrediaphragme>0 then
 for i:=1 to nombrediaphragme do
   if ((abs(listediaphragme[-1+i].ap1x-x1)<=tx*1.1) and
    (abs(listediaphragme[-1+i].ap1y-y1)<=ty*1.1)) then goto 667;

     if nombrediaphragme>0 then
 for i:=1 to nombrediaphragme do
   if ((abs(listediaphragme[-1+i].ap2x-x1)<=tx*1.1) and
    (abs(listediaphragme[-1+i].ap2y-y1)<=ty*1.1)) then goto 667;




     if nombrelsr>0 then
 for i:=1 to nombrelsr do
   if ((abs(listelsr[-1+i].cx-x1)<=tx*1.1) and
    (abs(listelsr[-1+i].cy-y1)<=ty*1.1)) then goto 667;

     if nombrelsr>0 then
 for i:=1 to nombrelsr do
   if ((abs(listelsr[-1+i].a1x-x1)<=tx*1.1) and
    (abs(listelsr[-1+i].a1y-y1)<=ty*1.1)) then goto 667;

     if nombrelsr>0 then
 for i:=1 to nombrelsr do
   if ((abs(listelsr[-1+i].a2x-x1)<=tx*1.1) and
    (abs(listelsr[-1+i].a2y-y1)<=ty*1.1)) then goto 667;
    if nombremscopa>0 then
  for i:=1 to nombremscopa do
   if ((abs(listemscopa[-1+i].sx-x1)<=tx*1.1) and
    (abs(listemscopa[-1+i].sy-y1)<=ty*1.1)) then goto 667;

     if nombremscopa>0 then
  for i:=1 to nombremscopa do
   if ((abs(listemscopa[-1+i].fx-x1)<=tx*1.1) and
    (abs(listemscopa[-1+i].fy-y1)<=ty*1.1)) then goto 667;

     if nombremscopa>0 then
  for i:=1 to nombremscopa do
   if ((abs(listemscopa[-1+i].a1x-x1)<=tx*1.1) and
    (abs(listemscopa[-1+i].a1y-y1)<=ty*1.1)) then goto 667;

    if nombremscopa>0 then
  for i:=1 to nombremscopa do
   if ((abs(listemscopa[-1+i].a2x-x1)<=tx*1.1) and
    (abs(listemscopa[-1+i].a2y-y1)<=ty*1.1)) then goto 667;


    if nombremscepa>0 then
    for i:=1 to nombremscepa do
   if ((abs(listemscepa[-1+i].sx-x1)<=tx*1.1) and
    (abs(listemscepa[-1+i].sy-y1)<=ty*1.1)) then goto 667;

    if nombremscepa>0 then
    for i:=1 to nombremscepa do
   if ((abs(listemscepa[-1+i].fx-x1)<=tx*1.1) and
    (abs(listemscepa[-1+i].fy-y1)<=ty*1.1)) then goto 667;

    if nombremscepa>0 then
    for i:=1 to nombremscepa do
   if ((abs(listemscepa[-1+i].a1x-x1)<=tx*1.1) and
    (abs(listemscepa[-1+i].a1y-y1)<=ty*1.1)) then goto 667;

    if nombremscepa>0 then
    for i:=1 to nombremscepa do
   if ((abs(listemscepa[-1+i].a2x-x1)<=tx*1.1) and
    (abs(listemscepa[-1+i].a2y-y1)<=ty*1.1)) then goto 667;

    if nombremscore>0 then
    for i:=1 to nombremscore do
   if ((abs(listemscore[-1+i].sx-x1)<=tx*1.1) and
    (abs(listemscore[-1+i].sy-y1)<=ty*1.1)) then goto 667;

    if nombremscore>0 then
    for i:=1 to nombremscore do
   if ((abs(listemscore[-1+i].cx-x1)<=tx*1.1) and
    (abs(listemscore[-1+i].cy-y1)<=ty*1.1)) then goto 667;

    if nombremscore>0 then
    for i:=1 to nombremscore do
   if ((abs(listemscore[-1+i].a1x-x1)<=tx*1.1) and
    (abs(listemscore[-1+i].a1y-y1)<=ty*1.1)) then goto 667;

    if nombremscore>0 then
    for i:=1 to nombremscore do
   if ((abs(listemscore[-1+i].a2x-x1)<=tx*1.1) and
    (abs(listemscore[-1+i].a2y-y1)<=ty*1.1)) then goto 667;


    if nombremscere>0 then
    for i:=1 to nombremscere do
   if ((abs(listemscere[-1+i].sx-x1)<=tx*1.1) and
    (abs(listemscere[-1+i].sy-y1)<=ty*1.1)) then goto 667;

    if nombremscere>0 then
    for i:=1 to nombremscere do
   if ((abs(listemscere[-1+i].cx-x1)<=tx*1.1) and
    (abs(listemscere[-1+i].cy-y1)<=ty*1.1)) then goto 667;

    if nombremscere>0 then
    for i:=1 to nombremscere do
   if ((abs(listemscere[-1+i].a1x-x1)<=tx*1.1) and
    (abs(listemscere[-1+i].a1y-y1)<=ty*1.1)) then goto 667;

    if nombremscere>0 then
    for i:=1 to nombremscere do
   if ((abs(listemscere[-1+i].a2x-x1)<=tx*1.1) and
    (abs(listemscere[-1+i].a2y-y1)<=ty*1.1)) then goto 667;

    if nombrelmc>0 then
    for i:=1 to nombrelmc do
   if ((abs(listelmc[-1+i].ox-x1)<=tx*1.1) and
    (abs(listelmc[-1+i].oy-y1)<=ty*1.1)) then goto 667;

    if nombrelmc>0 then
    for i:=1 to nombrelmc do
   if ((abs(listelmc[-1+i].f1x-x1)<=tx*1.1) and
    (abs(listelmc[-1+i].f1y-y1)<=ty*1.1)) then goto 667;

    if nombrelmc>0 then
    for i:=1 to nombrelmc do
   if ((abs(listelmc[-1+i].f2x-x1)<=tx*1.1) and
    (abs(listelmc[-1+i].f2y-y1)<=ty*1.1)) then goto 667;

    if nombrelmc>0 then
    for i:=1 to nombrelmc do
   if ((abs(listelmc[-1+i].a1x-x1)<=tx*1.1) and
    (abs(listelmc[-1+i].a1y-y1)<=ty*1.1)) then goto 667;

    if nombrelmc>0 then
    for i:=1 to nombrelmc do
   if ((abs(listelmc[-1+i].a2x-x1)<=tx*1.1) and
    (abs(listelmc[-1+i].a2y-y1)<=ty*1.1)) then goto 667;


     if nombrelec>0 then
    for i:=1 to nombrelec do
   if ((abs(listelec[-1+i].ox-x1)<=tx*1.1) and
    (abs(listelec[-1+i].oy-y1)<=ty*1.1)) then goto 667;

    if nombrelec>0 then
    for i:=1 to nombrelec do
   if ((abs(listelec[-1+i].s1x-x1)<=tx*1.1) and
    (abs(listelec[-1+i].s1y-y1)<=ty*1.1)) then goto 667;

    if nombrelec>0 then
    for i:=1 to nombrelec do
   if ((abs(listelec[-1+i].s2x-x1)<=tx*1.1) and
    (abs(listelec[-1+i].s2y-y1)<=ty*1.1)) then goto 667;


    if nombrelmd>0 then
    for i:=1 to nombrelmd do
   if ((abs(listelmd[-1+i].ox-x1)<=tx*1.1) and
    (abs(listelmd[-1+i].oy-y1)<=ty*1.1)) then goto 667;

     if nombrelmd>0 then
    for i:=1 to nombrelmd do
   if ((abs(listelmd[-1+i].f1x-x1)<=tx*1.1) and
    (abs(listelmd[-1+i].f1y-y1)<=ty*1.1)) then goto 667;

     if nombrelmd>0 then
    for i:=1 to nombrelmd do
   if ((abs(listelmd[-1+i].f2x-x1)<=tx*1.1) and
    (abs(listelmd[-1+i].f2y-y1)<=ty*1.1)) then goto 667;

     if nombrelmd>0 then
    for i:=1 to nombrelmd do
   if ((abs(listelmd[-1+i].a1x-x1)<=tx*1.1) and
    (abs(listelmd[-1+i].a1y-y1)<=ty*1.1)) then goto 667;

     if nombrelmd>0 then
    for i:=1 to nombrelmd do
   if ((abs(listelmd[-1+i].a2x-x1)<=tx*1.1) and
    (abs(listelmd[-1+i].a2y-y1)<=ty*1.1)) then goto 667;

    if nombrerayon>0 then
    for i:=1 to nombrerayon do
   if ((abs(listerayon[-1+i].cx-x1)<=tx*1.1) and
    (abs(listerayon[-1+i].cy-y1)<=ty*1.1)) then goto 667;

    if nombrerayon>0 then
    for i:=1 to nombrerayon do
   if ((abs(listerayon[-1+i].ax-x1)<=tx*1.1) and
    (abs(listerayon[-1+i].ay-y1)<=ty*1.1)) then goto 667;

    if nombrerayon>0 then
    for i:=1 to nombrerayon do
   if ((abs(listerayon[-1+i].bx-x1)<=tx*1.1) and
    (abs(listerayon[-1+i].by-y1)<=ty*1.1)) then goto 667;


  if nombresourceponctuelle>0 then
    for i:=1 to nombresourceponctuelle do
   if ((abs(listesourceponctuelle[-1+i].sx-x1)<=tx*1.1) and
    (abs(listesourceponctuelle[-1+i].sy-y1)<=ty*1.1)) then goto 667;

     if nombresourceponctuelle>0 then
    for i:=1 to nombresourceponctuelle do
   if ((abs(listesourceponctuelle[-1+i].a1x-x1)<=tx*1.1) and
    (abs(listesourceponctuelle[-1+i].a1y-y1)<=ty*1.1)) then goto 667;

   if nombresourceponctuelle>0 then
    for i:=1 to nombresourceponctuelle do
   if ((abs(listesourceponctuelle[-1+i].a2x-x1)<=tx*1.1) and
    (abs(listesourceponctuelle[-1+i].a2y-y1)<=ty*1.1)) then goto 667;

  if nombreondeplane>0 then
    for i:=1 to nombreondeplane do
   if ((abs(listeondeplane[-1+i].a1x-x1)<=tx*1.1) and
    (abs(listeondeplane[-1+i].a1y-y1)<=ty*1.1)) then goto 667;

    if nombreondeplane>0 then
    for i:=1 to nombreondeplane do
   if ((abs(listeondeplane[-1+i].sx-x1)<=tx*1.1) and
    (abs(listeondeplane[-1+i].sy-y1)<=ty*1.1)) then goto 667;

    if nombreondeplane>0 then
    for i:=1 to nombreondeplane do
   if ((abs(listeondeplane[-1+i].a2x-x1)<=tx*1.1) and
    (abs(listeondeplane[-1+i].a2y-y1)<=ty*1.1)) then goto 667;

 if nombreondeplane>0 then
    for i:=1 to nombreondeplane do
   if ((abs(listeondeplane[-1+i].ray[listeondeplane[-1+i].nombrederayon].bx-x1)<=tx*1.1) and
    (abs(listeondeplane[-1+i].ray[listeondeplane[-1+i].nombrederayon].by-y1)<=ty*1.1)) then goto 667;

  form1.image1.Cursor:=crprop;
  form1.StatusBar1.Panels[2].text:='';
  exit;
 667:
 form1.image1.Cursor:=crInfo;
 form1.StatusBar1.Panels[2].text:=rsCliquezPourA;
  exit;
 end;







 if modeduplication then begin

    if nombremetre>0 then
 for i:=1 to nombremetre do
   if ((abs(listemetre[-1+i].cx-x1)<=tx*1.1) and
    (abs(listemetre[-1+i].cy-y1)<=ty*1.1)) then goto 668;

  if nombreangle>0 then
 for i:=1 to nombreangle do
   if ((abs(listeangle[-1+i].cx-x1)<=tx*1.1) and
    (abs(listeangle[-1+i].cy-y1)<=ty*1.1)) then goto 668;


 if nombretexte>0 then
 for i:=1 to nombretexte do
   if ((abs(listetexteaffiche[-1+i].x-x1)<=tx*1.1) and
    (abs(listetexteaffiche[-1+i].y-y1)<=ty*1.1)) then goto 668;

 if nombrefleche>0 then
 for i:=1 to nombrefleche do
   if ((abs(listefleche[-1+i].cx-x1)<=tx*1.1) and
    (abs(listefleche[-1+i].cy-y1)<=ty*1.1)) then goto 668;


 if nombremiroirconique>0 then
 for i:=1 to nombremiroirconique do
   if ((abs(listemiroirconique[-1+i].sx-x1)<=tx*1.1) and
    (abs(listemiroirconique[-1+i].sy-y1)<=ty*1.1)) then goto 668;


 if nombresphere>0 then
 for i:=1 to nombresphere do
   if ((abs(listesphere[-1+i].cx-x1)<=tx*1.1) and
    (abs(listesphere[-1+i].cy-y1)<=ty*1.1)) then goto 668;


 if nombremiroirplan>0 then
 for i:=1 to nombremiroirplan do
   if ((abs(listemiroirplan[-1+i].cx-x1)<=tx*1.1) and
    (abs(listemiroirplan[-1+i].cy-y1)<=ty*1.1)) then goto 668;

  if nombreReseau>0 then
 for i:=1 to nombreReseau do
   if ((abs(listeReseau[-1+i].cx-x1)<=tx*1.1) and
    (abs(listeReseau[-1+i].cy-y1)<=ty*1.1)) then goto 668;


  if nombrepolyhedre>0 then
 for i:=1 to nombrepolyhedre do
 for j:=1 to listepolyhedre[-1+i].nombresommet do
   if ((abs((listepolyhedre[-1+i].messommets[j].ax+listepolyhedre[-1+i].messommets[j+1].ax)/2-x1)<=tx*1.1) and
    (abs((listepolyhedre[-1+i].messommets[j].ay+listepolyhedre[-1+i].messommets[j+1].ay)/2-y1)<=ty*1.1) )
    then goto 668;

     if nombreprisme>0 then
 for i:=1 to nombreprisme do
 for j:=1 to listeprisme[-1+i].nombresommet do
   if ((abs((listeprisme[-1+i].messommets[j].ax+listeprisme[-1+i].messommets[j+1].ax)/2-x1)<=tx*1.1) and
    (abs((listeprisme[-1+i].messommets[j].ay+listeprisme[-1+i].messommets[j+1].ay)/2-y1)<=ty*1.1) )
    then goto 668;


      if nombrepolycercle>0 then
 for i:=1 to nombrepolycercle do
 for j:=1 to listepolycercle[-1+i].nombresommet do
   if ((abs((listepolycercle[-1+i].messommets[j].ax+listepolycercle[-1+i].messommets[j+1].ax)/2-x1)<=tx*1.1) and
    (abs((listepolycercle[-1+i].messommets[j].ay+listepolycercle[-1+i].messommets[j+1].ay)/2-y1)<=ty*1.1)
     and not((listepolycercle[-1+i].nombresommet=2) and (j=2)))
    then goto 668;


     if nombreecran>0 then
 for i:=1 to nombreecran do
   if ((abs(listeecran[-1+i].cx-x1)<=tx*1.1) and
    (abs(listeecran[-1+i].cy-y1)<=ty*1.1)) then goto 668;

     if nombreoeil>0 then
 for i:=1 to nombreoeil do
   if ((abs(listeoeil[-1+i].cx-x1)<=tx*1.1) and
    (abs(listeoeil[-1+i].cy-y1)<=ty*1.1)) then goto 668;


    if nombrediaphragme>0 then
 for i:=1 to nombrediaphragme do
   if ((abs(listediaphragme[-1+i].cx-x1)<=tx*1.1) and
    (abs(listediaphragme[-1+i].cy-y1)<=ty*1.1)) then goto 668;


     if nombrelsr>0 then
 for i:=1 to nombrelsr do
   if ((abs(listelsr[-1+i].cx-x1)<=tx*1.1) and
    (abs(listelsr[-1+i].cy-y1)<=ty*1.1)) then goto 668;

     if nombremscopa>0 then
    for i:=1 to nombremscopa do
   if ((abs(listemscopa[-1+i].sx-x1)<=tx*1.1) and
    (abs(listemscopa[-1+i].sy-y1)<=ty*1.1)) then goto 668;


    if nombremscepa>0 then
    for i:=1 to nombremscepa do
   if ((abs(listemscepa[-1+i].sx-x1)<=tx*1.1) and
    (abs(listemscepa[-1+i].sy-y1)<=ty*1.1)) then goto 668;

    if nombremscore>0 then
    for i:=1 to nombremscore do
   if ((abs(listemscore[-1+i].sx-x1)<=tx*1.1) and
    (abs(listemscore[-1+i].sy-y1)<=ty*1.1)) then goto 668;


    if nombremscere>0 then
    for i:=1 to nombremscere do
   if ((abs(listemscere[-1+i].sx-x1)<=tx*1.1) and
    (abs(listemscere[-1+i].sy-y1)<=ty*1.1)) then goto 668;



    if nombrelmc>0 then
    for i:=1 to nombrelmc do
   if ((abs(listelmc[-1+i].ox-x1)<=tx*1.1) and
    (abs(listelmc[-1+i].oy-y1)<=ty*1.1)) then goto 668;



    if nombrelec>0 then
    for i:=1 to nombrelec do
   if ((abs(listelec[-1+i].ox-x1)<=tx*1.1) and
    (abs(listelec[-1+i].oy-y1)<=ty*1.1)) then goto 668;


    if nombrelmd>0 then
    for i:=1 to nombrelmd do
   if ((abs(listelmd[-1+i].ox-x1)<=tx*1.1) and
    (abs(listelmd[-1+i].oy-y1)<=ty*1.1)) then goto 668;



    if nombrerayon>0 then
    for i:=1 to nombrerayon do
   if ((abs(listerayon[-1+i].cx-x1)<=tx*1.1) and
    (abs(listerayon[-1+i].cy-y1)<=ty*1.1)) then goto 668;



  if nombresourceponctuelle>0 then
    for i:=1 to nombresourceponctuelle do
   if ((abs((listesourceponctuelle[-1+i].a1x+listesourceponctuelle[-1+i].a2x)/2-x1)<=tx*1.1) and
    (abs((listesourceponctuelle[-1+i].a1y+listesourceponctuelle[-1+i].a2y)/2-y1)<=ty*1.1)) then goto 668;

  if nombreondeplane>0 then
    for i:=1 to nombreondeplane do
   if ((abs(listeondeplane[-1+i].a1x-x1)<=tx*1.1) and
    (abs(listeondeplane[-1+i].a1y-y1)<=ty*1.1)) then goto 668;


  if not(modeduplication) then
  form1.image1.Cursor:=crprop
  else
   form1.image1.Cursor:=crdefault;
  form1.StatusBar1.Panels[2].text:='';
  exit;
 668:
  form1.image1.Cursor:=crdupliquer;
 form1.StatusBar1.Panels[2].text:=rsCliquezPourD;
  exit;
 end; end;




if (not(form1.ajoutencours) or
(form1.ajoutencours and not(point1fait or ((ajoutpoly or ajoutpolycercle) and (nbactsommet>0))))) then begin
   pointrx:=x1; pointry:=y1;

             end;

    if ((ajoutpoly) and (nbactsommet>=1)) then begin
    if nbactsommet>1 then for i:=1 to nbactsommet-1 do
     BIBI.trait(ttpoly[i].ax,ttpoly[i].ay,ttpoly[i+1].ax,ttpoly[i+1].ay,1,
     clblack,pssolid,pmcopy,true);
   BIBI.trait(ttpoly[nbactsommet].ax,ttpoly[nbactsommet].ay,pointrx,pointry,1,
    clblack,psdot,pmxor,true);
     BIBI.trait(ttpoly[nbactsommet].ax,ttpoly[nbactsommet].ay,x1,y1,1,
     clblack,psdot,pmxor,true);
     if nbactsommet=nbpoly-1 then begin
   if not(toutjustefait) then
    BIBI.trait(ttpoly[1].ax,ttpoly[1].ay,pointrx,pointry,1,
    clblack,psdot,pmxor,true) else toutjustefait:=false;
     BIBI.trait(ttpoly[1].ax,ttpoly[1].ay,x1,y1,1,
     clblack,psdot,pmxor,true);

     end;
       pointrxi:=x; pointryi:=y; pointrx:=x1; pointry:=y1;
                       end;


          if (((ajoutpolycercle and not(rectifait))) and (nbactsommet>=1)) then begin

   if nbactsommet>1 then for i:=1 to nbactsommet-1 do
     BIBI.trait(ttpoly[i].ax,ttpoly[i].ay,ttpoly[i+1].ax,ttpoly[i+1].ay,1,
     clblack,pssolid,pmcopy,true);
   BIBI.trait(ttpoly[nbactsommet].ax,ttpoly[nbactsommet].ay,pointrx,pointry,1,
    clblack,psdot,pmxor,true);
     BIBI.trait(ttpoly[nbactsommet].ax,ttpoly[nbactsommet].ay,x1,y1,1,
     clblack,psdot,pmxor,true);
     if nbactsommet=nbpoly-1 then begin
   if not(toutjustefait) then
    BIBI.trait(ttpoly[1].ax,ttpoly[1].ay,pointrx,pointry,1,
    clblack,psdot,pmxor,true) else toutjustefait:=false;
     BIBI.trait(ttpoly[1].ax,ttpoly[1].ay,x1,y1,1,
     clblack,psdot,pmxor,true);       end;
       pointrxi:=x; pointryi:=y; pointrx:=x1; pointry:=y1;
                         end;



                       if (((ajoutpolycercle and (rectifait))) ) then begin
    for i:=1 to nbpoly do  begin
   BIBI.trait(ttpoly[i].ax,ttpoly[i].ay,ttpoly[i+1].ax,ttpoly[i+1].ay,1,
     couleurmiroirplan,psdot,pmcopy,true);
     BIBI.invconvert(xec,yec,ttpoly[i].ax,ttpoly[i].ay,true);

  form1.image1.Picture.Bitmap.Canvas.font.Color:=clblack;
  form1.image1.Picture.Bitmap.Canvas.font.Name:='Arial Narrow';
  form1.image1.Picture.Bitmap.Canvas.font.Charset:=DEFAULT_CHARSET;
  form1.image1.Picture.Bitmap.Canvas.font.size:=10;
  form1.image1.Picture.Bitmap.Canvas.font.Style:=[];
     form1.image1.Picture.Bitmap.Canvas.textout(xec+5,yec,inttostr(i));
     end;
   for i:=1 to nbrectiact do
   begin
   BIBI.cercle(tts[listerecti[i]].ax,
   tts[listerecti[i]].ay,3,1,clblack,pmcopy,true);
   BIBI.arcdecercle(ttc[listerecti[i]].ax,ttc[listerecti[i]].ay,
   tt_rayon[listerecti[i]],1,cctheta1[listerecti[i]],cctheta2[listerecti[i]],
   clblack,pmcopy,psdot,true);
   end;
   BIBI.cercle(ttpoly[listerecti[nbrectiact+1]].ax, ttpoly[listerecti[nbrectiact+1]].ay,
   3,1,clblue,pmcopy,true);
   BIBI.cercle(ttpoly[listerecti[nbrectiact+1]+1].ax, ttpoly[listerecti[nbrectiact+1]+1].ay,
   3,1,clblue,pmcopy,true);
   if listerecti[nbrectiact+1]+1<=nbpoly then
   form1.StatusBar1.Panels[2].text:=Format(rsPositionnezL27, [inttostr(
     listerecti[nbrectiact+1]), inttostr(listerecti[nbrectiact+1]+1)]) else
   form1.StatusBar1.Panels[2].text:=Format(rsPositionnezL28, [inttostr(
     listerecti[nbrectiact+1])]);
       pointrxi:=x; pointryi:=y; pointrx:=x1; pointry:=y1;
                       end;

    if (ajoutlec and point1fait and not(point2fait)) then begin
    BIBI.cercle(point1x,point1y,2,1,clblack,pmcopy,true);
    bibi.trait(point1x,point1y,pointrx,pointry,1,clblack,psdot,pmxor,true);
    bibi.trait(point1x,point1y,x1,y1,1,clblack,psdot,pmxor,true);
         pointrxi:=x; pointryi:=y; pointrx:=x1; pointry:=y1;
                       end;

      if (ajoutlec and point2fait and not(point3fait)) then begin
    BIBI.cercle(point1x,point1y,2,1,clblack,pmcopy,true);
    BIBI.cercle(point2x,point2y,2,1,clblack,pmcopy,true);
      bibi.trait(point1x,point1y,point2x,point2y,1,clblack,psdot,pmcopy,true);
   if not(toutjustefait) then
     BIBI.arcdecercle(point3x,point3y,sqrt(sqr(point3x-point1x)+sqr(point3y-point1y)),
epaisseurmiroirplan,0,2*pi-0.01,couleurmiroirplan,pmnotxor,psdot,true) else toutjustefait:=false;
        point3x:=point1x+((x1-point1x)*(point2x-point1x)+(y1-point1y)*(point2y-point1y))*
  (point2x-point1x)/(sqr(point2x-point1x)+sqr(point2y-point1y));
   point3y:=point1y+((x1-point1x)*(point2x-point1x)+(y1-point1y)*(point2y-point1y))*
  (point2y-point1y)/(sqr(point2x-point1x)+sqr(point2y-point1y));
  BIBI.arcdecercle(point3x,point3y,sqrt(sqr(point3x-point1x)+sqr(point3y-point1y)),
epaisseurmiroirplan,0,2*pi-0.01,couleurmiroirplan,pmnotxor,psdot,true);
         pointrxi:=x; pointryi:=y; pointrx:=x1; pointry:=y1;
                       end;

      if (ajoutlec and point3fait) then begin
    BIBI.cercle(point1x,point1y,2,1,clblack,pmcopy,true);
    BIBI.cercle(point2x,point2y,2,1,clblack,pmcopy,true);
     bibi.trait(point1x,point1y,point2x,point2y,1,clblack,psdot,pmcopy,true);
     BIBI.arcdecercle(point3x,point3y,sqrt(sqr(point3x-point1x)+sqr(point3y-point1y)),1,0,
     2*pi-0.001,clblack,pmcopy,psdot,true);
   if not(toutjustefait) then
      BIBI.arcdecercle(point4x,point4y,sqrt(sqr(point4x-point2x)+sqr(point4y-point2y)),
epaisseurmiroirplan,0,2*pi-0.001,couleurmiroirplan,pmnotxor,psdot,true)  else toutjustefait:=false;
      point4x:=point1x+((x1-point1x)*(point2x-point1x)+(y1-point1y)*(point2y-point1y))*
  (point2x-point1x)/(sqr(point2x-point1x)+sqr(point2y-point1y));
   point4y:=point1y+((x1-point1x)*(point2x-point1x)+(y1-point1y)*(point2y-point1y))*
  (point2y-point1y)/(sqr(point2x-point1x)+sqr(point2y-point1y));
  BIBI.arcdecercle(point4x,point4y,sqrt(sqr(point4x-point2x)+sqr(point4y-point2y)),
epaisseurmiroirplan,0,2*pi-0.001,couleurmiroirplan,pmnotxor,psdot,true);
         pointrxi:=x; pointryi:=y; pointrx:=x1; pointry:=y1;
                       end;

       if (ajoutlec and point4fait) then begin
    BIBI.cercle(point1x,point1y,2,1,clblack,pmcopy,true);
    BIBI.cercle(point2x,point2y,2,1,clblack,pmcopy,true);
     BIBI.arcdecercle(point3x,point3y,sqrt(sqr(point3x-point1x)+sqr(point3y-point1y)),1,0,
     2*pi-0.001,clblack,pmxor,psdot,true);
     BIBI.arcdecercle(point4x,point4y,sqrt(sqr(point4x-point2x)+sqr(point4y-point2y)),1,0,
     2*pi-0.001,clblack,pmxor,psdot,true);
         pointrxi:=x; pointryi:=y; pointrx:=x1; pointry:=y1;
                       end;



    if ((ajoutmiroirplan or ajoutreseau or ajoutecran or ajoutlsr or ajoutfleche or ajoutoeil) and point1fait) then begin
         BIBI.trait(point1x,point1y,pointrx,pointry,1,couleurmiroirplan,psdot,pmxor,true);
    BIBI.trait(point1x,point1y,x1,y1,1,couleurmiroirplan,psdot,pmxor,true);
    BIBI.cercle(point1x,point1y,2*epaisseurmiroirplan,epaisseurmiroirplan,
    couleurmiroirplan,pmcopy,true);
                       pointrxi:=x; pointryi:=y; pointrx:=x1; pointry:=y1;
                       end;

       if ((ajoutconiqueconcave or ajoutconiqueconvexe) and (point1fait)) then begin
          BIBI.cercle(point1x,point1y,2,1,
    clblue,pmcopy,true);
                       pointrxi:=x; pointryi:=y; pointrx:=x1; pointry:=y1;
                       end;

     if ((ajoutconiqueconcave or ajoutconiqueconvexe) and (foyerfait)) then begin
          BIBI.cercle(foyerx,foyery,2,1,
    clblue,pmcopy,true);
                       pointrxi:=x; pointryi:=y; pointrx:=x1; pointry:=y1;
                       end;

                        if ((ajoutconiqueconcave or ajoutconiqueconvexe) and (sommetfait)) then begin
          BIBI.cercle(sommetx,sommety,2,1,
    clblue,pmcopy,true);
    bibi.traceconique(co_theta0,-pi,pi,foyerx,foyery,co_e,co_p,clblack,true);
                       pointrxi:=x; pointryi:=y; pointrx:=x1; pointry:=y1;

                       end;


       if ((ajoutsphere) and point1fait) then begin
         BIBI.arcdecercle(point1x,point1y,sqrt(sqr(pointrx-point1x)+sqr(pointry-point1y)),1,0,2*Pi-0.001,
         clblack,pmnotxor,psdot,true);
         BIBI.arcdecercle(point1x,point1y,sqrt(sqr(x1-point1x)+sqr(y1-point1y)),1,0,2*Pi-0.001,
         clblack,pmnotxor,psdot,true);

                       pointrxi:=x; pointryi:=y; pointrx:=x1; pointry:=y1;
                       end;

                       if (ajoutsourceponctuelle and point1fait and not(point2fait)) then begin
    BIBI.trait(sourcex,sourcey,pointrx,pointry,1,couleursourceponctuelle,psdot,pmnotxor,true);
    BIBI.trait(sourcex,sourcey,x1,y1,1,couleursourceponctuelle,psdot,pmnotxor,true);
    BIBI.cercle(sourcex,sourcey,2*epaisseursourceponctuelle,epaisseursourceponctuelle,
    couleursourceponctuelle,pmcopy,true);
                             pointrxi:=x; pointryi:=y; pointrx:=x1; pointry:=y1;
                       end;

                  if (ajoutondeplane and point1fait and not(point2fait)) then begin
                    BIBI.trait(point1x,point1y,pointrx,pointry,1,couleurondeplane,psdot,pmnotxor,true);
    BIBI.trait(point1x,point1y,x1,y1,1,couleurondeplane,psdot,pmnotxor,true);
    BIBI.cercle(point1x,point1y,2*epaisseurondeplane,epaisseurondeplane,
    couleurondeplane,pmcopy,true);
                             pointrxi:=x; pointryi:=y; pointrx:=x1; pointry:=y1;
                       end;

             if (ajoutondeplane and point1fait and point2fait) then begin
           BIBI.trait(point1x,point1y,directionx,directiony,1,couleurondeplane,pssolid,pmcopy,true);
           BIBI.trait(point1x,point1y,pointrx,pointry,1,couleurondeplane,psdot,pmnotxor,true);
    BIBI.trait(point1x,point1y,x1,y1,1,couleurondeplane,psdot,pmnotxor,true);
    BIBI.cercle(point1x,point1y,2*epaisseurondeplane,epaisseurondeplane,
    couleurondeplane,pmcopy,true);
                             pointrxi:=x; pointryi:=y; pointrx:=x1; pointry:=y1;
                       end;




                    if (ajoutsourceponctuelle and point1fait and point2fait) then begin
                     BIBI.trait(sourcex,sourcey,point1x,point1y,1,couleursourceponctuelle,pssolid,pmcopy,true);
                       BIBI.trait(sourcex,sourcey,pointrx,pointry,1,couleursourceponctuelle,psdot,pmnotxor,true);
    BIBI.trait(sourcex,sourcey,x1,y1,1,couleursourceponctuelle,psdot,pmnotxor,true);
    BIBI.cercle(sourcex,sourcey,2*epaisseursourceponctuelle,epaisseursourceponctuelle,
    couleursourceponctuelle,pmcopy,true);
                             pointrxi:=x; pointryi:=y; pointrx:=x1; pointry:=y1;
                       end;

    if ((ajoutmscopa or ajoutmscepa) and point1fait and not(point2fait)) then begin
    BIBI.trait(point1x,point1y,pointrx,pointry,1,couleurmscopa,psdot,pmxor,true);
    BIBI.trait(point1x,point1y,x1,y1,1,couleurmscopa,psdot,pmxor,true);
    BIBI.cercle(point1x,point1y,2*epaisseurmscopa,epaisseurmscopa,
    couleurmscopa,pmcopy,true);
                       pointrxi:=x; pointryi:=y; pointrx:=x1; pointry:=y1;
                       end;

     if (ajoutdiaphragme and point1fait and not(point2fait)) then begin
    BIBI.trait(point1x,point1y,pointrx,pointry,1,clblack,psdot,pmxor,true);
    BIBI.trait(point1x,point1y,x1,y1,1,clblack,psdot,pmxor,true);
    BIBI.cercle(point1x,point1y,2,1,
    clblack,pmcopy,true);
                       pointrxi:=x; pointryi:=y; pointrx:=x1; pointry:=y1;
                       end;

    if ((ajoutmscore or ajoutmscere) and point1fait and not(point2fait)) then begin
    BIBI.trait(point1x,point1y,pointrx,pointry,1,couleurmscore,psdot,pmxor,true);
    BIBI.trait(point1x,point1y,x1,y1,1,couleurmscore,psdot,pmxor,true);
    BIBI.cercle(point1x,point1y,2*epaisseurmscore,epaisseurmscore,
    couleurmscore,pmcopy,true);
                       pointrxi:=x; pointryi:=y; pointrx:=x1; pointry:=y1;
                       end;

                    if ((ajoutlmc) and point1fait and not(point2fait)) then begin
    BIBI.trait(point1x,point1y,pointrx,pointry,1,couleurlmc,psdot,pmxor,true);
    BIBI.trait(point1x,point1y,x1,y1,1,couleurlmc,psdot,pmxor,true);
    BIBI.cercle(point1x,point1y,2*epaisseurlmc,epaisseurlmc,
    couleurlmc,pmcopy,true);
                       pointrxi:=x; pointryi:=y; pointrx:=x1; pointry:=y1;
                       end;

                        if ((ajoutprisme) and point1fait and not(point2fait)) then begin
    BIBI.trait(point1x,point1y,pointrx,pointry,1,clblack,psdot,pmxor,true);
    BIBI.trait(point1x,point1y,x1,y1,1,clblack,psdot,pmxor,true);
    BIBI.cercle(point1x,point1y,2*epaisseurprisme,epaisseurprisme,
    clred,pmcopy,true);
                       pointrxi:=x; pointryi:=y; pointrx:=x1; pointry:=y1;
                       end;

                       if ((ajoutlmd) and point1fait and not(point2fait)) then begin
    BIBI.trait(point1x,point1y,pointrx,pointry,1,couleurlmd,psdot,pmxor,true);
    BIBI.trait(point1x,point1y,x1,y1,1,couleurlmd,psdot,pmxor,true);
    BIBI.cercle(point1x,point1y,2*epaisseurlmd,epaisseurlmd,couleurlmd,pmcopy,true);
                       pointrxi:=x; pointryi:=y; pointrx:=x1; pointry:=y1;
                       end;


      if (ajoutrayon and point1fait) then begin
    BIBI.trait(point1x,point1y,pointrx,pointry,1,couleurrayon,psdot,pmnotxor,true);
    BIBI.trait(point1x,point1y,x1,y1,1,couleurrayon,psdot,pmnotxor,true);
    BIBI.cercle(point1x,point1y,2*epaisseurrayon,epaisseurrayon,
    couleurrayon,pmcopy,true);
                       pointrxi:=x; pointryi:=y; pointrx:=x1; pointry:=y1;
                       end;

      if (ajoutmscopa and point1fait and point2fait) then begin
      BIBI.trait(point1x,point1y,point2x,point2y,epaisseurmscopa,couleurmscopa,pssolid,pmcopy,true);
      BIBI.trait(axefocal_x1,axefocal_y1,axefocal_x2,axefocal_y2,epaisseurmscopa,
  couleuraxemscopa,psdashdot,pmcopy,true);
      BIBI.cercle(pointrx,pointry,2*epaisseurmscopa,
      epaisseurmscopa,couleurmscopa,pmnotxor,true);
      lambda:=((x1-axefocal_x1)*(axefocal_x2-axefocal_x1)+(y1-axefocal_y1)*
      (axefocal_y2-axefocal_y1))/((axefocal_x2-axefocal_x1)*(axefocal_x2-axefocal_x1)+
      (axefocal_y2-axefocal_y1)*(axefocal_y2-axefocal_y1));
      if lambda<0 then lambda:=0;
       pointrx:=axefocal_x1+lambda*(axefocal_x2-axefocal_x1);
        pointry:=axefocal_y1+lambda*(axefocal_y2-axefocal_y1);
         BIBI.cercle(pointrx,pointry,2*epaisseurmscopa,
         epaisseurmscopa,couleurmscopa,pmnotxor,true);

                       end;


                        if (ajoutdiaphragme and point1fait and point2fait) then begin
      BIBI.trait(point1x,point1y,point2x,point2y,1,clblack,psdot,pmcopy,true);
      BIBI.trait(axefocal_x1,axefocal_y1,axefocal_x2,axefocal_y2,1,
  clblack,psdashdot,pmcopy,true);
      BIBI.cercle(pointrx,pointry,2,
      1,clblack,pmnotxor,true);
      BIBI.cercle(2*point1x-pointrx,2*point1y-pointry,2,
         1,clblack,pmnotxor,true);
      lambda:=((x1-axefocal_x1)*(axefocal_x2-axefocal_x1)+(y1-axefocal_y1)*
      (axefocal_y2-axefocal_y1))/((axefocal_x2-axefocal_x1)*(axefocal_x2-axefocal_x1)+
      (axefocal_y2-axefocal_y1)*(axefocal_y2-axefocal_y1));
      if lambda<0 then lambda:=0;
       pointrx:=axefocal_x1+lambda*(axefocal_x2-axefocal_x1);
        pointry:=axefocal_y1+lambda*(axefocal_y2-axefocal_y1);
         BIBI.cercle(pointrx,pointry,2,
         1,clblack,pmnotxor,true);
          BIBI.cercle(2*point1x-pointrx,2*point1y-pointry,2,
         1,clblack,pmnotxor,true);
                       end;



                       if (ajoutmscore and point1fait and point2fait) then begin

      BIBI.trait(axefocal_x1,axefocal_y1,axefocal_x2,axefocal_y2,epaisseurmscore,
  couleuraxemscore,psdashdot,pmcopy,true);
      BIBI.cercle(pointrx,pointry,2*epaisseurmscore,
      epaisseurmscore,couleurmscore,pmnotxor,true);
      if (point2x=pointrx) then
  if (point2y>pointry) then tata2:=Pi/2 else tata2:=-Pi/2
  else begin
  tata2:=arctan((point2y-pointry)/(point2x-pointrx));
  if (point2x<pointrx) then tata2:=tata2+Pi;
  end;
    if (point1x=pointrx) then
  if (point1y>pointry) then tata1:=Pi/2 else tata1:=-Pi/2
  else begin
  tata1:=arctan((point1y-pointry)/(point1x-pointrx));
  if (point1x<pointrx) then tata1:=tata1+Pi;
  end;
     if tata1<0 then tata1:=tata1+Pi*2;
  if tata1>2*Pi then tata1:=tata1-Pi*2;
    if tata1>tata2 then tata2:=tata2+2*Pi;
    if tata1>tata2 then tata2:=tata2+2*Pi;
    if not(toutjustefait) then  BIBI.arcdecercle(pointrx,pointry,sqrt(sqr(point1x-pointrx)+sqr(point1y-pointry)),
      epaisseurmscore,tata1,tata2,couleurmscore,pmnotxor,psdot,true)
     else toutjustefait:=false;

      lambda:=((x1-axefocal_x1)*(axefocal_x2-axefocal_x1)+(y1-axefocal_y1)*
      (axefocal_y2-axefocal_y1))/((axefocal_x2-axefocal_x1)*(axefocal_x2-axefocal_x1)+
      (axefocal_y2-axefocal_y1)*(axefocal_y2-axefocal_y1));
       pointrx:=axefocal_x1+lambda*(axefocal_x2-axefocal_x1);
        pointry:=axefocal_y1+lambda*(axefocal_y2-axefocal_y1);

        if (point2x=pointrx) then
  if (point2y>pointry) then tata2:=Pi/2 else tata2:=-Pi/2
  else begin
  tata2:=arctan((point2y-pointry)/(point2x-pointrx));
  if (point2x<pointrx) then tata2:=tata2+Pi;
  end;
    if (point1x=pointrx) then
  if (point1y>pointry) then tata1:=Pi/2 else tata1:=-Pi/2
  else begin
  tata1:=arctan((point1y-pointry)/(point1x-pointrx));
  if (point1x<pointrx) then tata1:=tata1+Pi;
  end;
     if tata1<0 then tata1:=tata1+Pi*2;
  if tata1>2*Pi then tata1:=tata1-Pi*2;
    if tata1>tata2 then tata2:=tata2+2*Pi;
    if tata1>tata2 then tata2:=tata2+2*Pi;
     BIBI.arcdecercle(pointrx,pointry,sqrt(sqr(point1x-pointrx)+sqr(point1y-pointry)),
      epaisseurmscore,tata1,tata2,couleurmscore,pmnotxor,psdot,true);
         BIBI.cercle(pointrx,pointry,2*epaisseurmscore,
         epaisseurmscore,couleurmscore,pmnotxor,true);

                       end;

         if (ajoutmscere and point1fait and point2fait) then begin
      BIBI.trait(axefocal_x1,axefocal_y1,axefocal_x2,axefocal_y2,epaisseurmscere,
  couleuraxemscere,psdashdot,pmcopy,true);
      BIBI.cercle(pointrx,pointry,2*epaisseurmscere,
      epaisseurmscere,couleurmscere,pmnotxor,true);
      if (point2x=pointrx) then
  if (point2y>pointry) then tata2:=Pi/2 else tata2:=-Pi/2
  else begin
  tata2:=arctan((point2y-pointry)/(point2x-pointrx));
  if (point2x<pointrx) then tata2:=tata2+Pi;
  end;
    if (point1x=pointrx) then
  if (point1y>pointry) then tata1:=Pi/2 else tata1:=-Pi/2
  else begin
  tata1:=arctan((point1y-pointry)/(point1x-pointrx));
  if (point1x<pointrx) then tata1:=tata1+Pi;
  end;
     if tata1<0 then tata1:=tata1+Pi*2;
  if tata1>2*Pi then tata1:=tata1-Pi*2;
    if tata1>tata2 then tata2:=tata2+2*Pi;
    if tata1>tata2 then tata2:=tata2+2*Pi;
    if not(toutjustefait) then
     BIBI.arcdecercle(pointrx,pointry,sqrt(sqr(point1x-pointrx)+sqr(point1y-pointry)),
      epaisseurmscere,tata1,tata2,couleurmscere,pmnotxor,psdot,true)
   else   toutjustefait:=false;
          lambda:=((x1-axefocal_x1)*(axefocal_x2-axefocal_x1)+(y1-axefocal_y1)*
      (axefocal_y2-axefocal_y1))/((axefocal_x2-axefocal_x1)*(axefocal_x2-axefocal_x1)+
      (axefocal_y2-axefocal_y1)*(axefocal_y2-axefocal_y1));
       pointrx:=axefocal_x1+lambda*(axefocal_x2-axefocal_x1);
        pointry:=axefocal_y1+lambda*(axefocal_y2-axefocal_y1);
        if (point2x=pointrx) then
  if (point2y>pointry) then tata2:=Pi/2 else tata2:=-Pi/2
  else begin
  tata2:=arctan((point2y-pointry)/(point2x-pointrx));
  if (point2x<pointrx) then tata2:=tata2+Pi;
  end;
    if (point1x=pointrx) then
  if (point1y>pointry) then tata1:=Pi/2 else tata1:=-Pi/2
  else begin
  tata1:=arctan((point1y-pointry)/(point1x-pointrx));
  if (point1x<pointrx) then tata1:=tata1+Pi;
  end;
     if tata1<0 then tata1:=tata1+Pi*2;
  if tata1>2*Pi then tata1:=tata1-Pi*2;
    if tata1>tata2 then tata2:=tata2+2*Pi;
    if tata1>tata2 then tata2:=tata2+2*Pi;
     BIBI.arcdecercle(pointrx,pointry,sqrt(sqr(point1x-pointrx)+sqr(point1y-pointry)),
      epaisseurmscere,tata1,tata2,couleurmscere,pmnotxor,psdot,true);
         BIBI.cercle(pointrx,pointry,2*epaisseurmscere,
         epaisseurmscere,couleurmscere,pmnotxor,true);


                       end;

                       if (ajoutlmc and point1fait and point2fait) then begin
      BIBI.trait(point1x,point1y,point2x,point2y,epaisseurlmc,couleurlmc,pssolid,pmcopy,true);
      BIBI.trait(axefocal_x1,axefocal_y1,axefocal_x2,axefocal_y2,epaisseurlmc,
  couleuraxelmc,psdashdot,pmcopy,true);
      BIBI.cercle(pointrx,pointry,2*epaisseurlmc,
      epaisseurlmc,couleurlmc,pmnotxor,true);
      lambda:=((x1-axefocal_x1)*(axefocal_x2-axefocal_x1)+(y1-axefocal_y1)*
      (axefocal_y2-axefocal_y1))/((axefocal_x2-axefocal_x1)*(axefocal_x2-axefocal_x1)+
      (axefocal_y2-axefocal_y1)*(axefocal_y2-axefocal_y1));
       pointrx:=axefocal_x1+lambda*(axefocal_x2-axefocal_x1);
        pointry:=axefocal_y1+lambda*(axefocal_y2-axefocal_y1);
         BIBI.cercle(pointrx,pointry,2*epaisseurlmc,
         epaisseurlmc,couleurlmc,pmnotxor,true);

                       end;


                       if (ajoutprisme and point1fait and point2fait) then begin
                       BIBI.trait(point2x,point2y,point3x,point3y,epaisseurprisme,
  clblack,psdot,pmxor,true);
  BIBI.trait(point2x,point2y,point4x,point4y,epaisseurprisme,
  clblack,psdot,pmxor,true);
  BIBI.trait(point4x,point4y,point3x,point3y,epaisseurprisme,
  clblack,psdot,pmxor,true);
                     ux:=(point2x-point1x)/sqrt(sqr(point2x-point1x)+sqr(point2y-point1y));
       uy:=(point2y-point1y)/sqrt(sqr(point2x-point1x)+sqr(point2y-point1y));
      hx:=point1x-(point2x-point1x)/2;
      hy:=point1y-(point2y-point1y)/2;
      lambda:=(x1-hx)*ux+(y1-hy)*uy;
      point3x:=x1-lambda*ux; point3y:=y1-lambda*uy;
      point4x:=-point3x+2*hx;
      point4y:=-point3y+2*hy;
       BIBI.trait(point2x,point2y,point3x,point3y,epaisseurprisme,
  clblack,psdot,pmxor,true);
  BIBI.trait(point2x,point2y,point4x,point4y,epaisseurprisme,
  clblack,psdot,pmxor,true);
  BIBI.trait(point4x,point4y,point3x,point3y,epaisseurprisme,
  clblack,psdot,pmxor,true);
  BIBI.cercle(point1x,point1y,2*epaisseurprisme,
      epaisseurlmd,clred,pmcopy,true);
    BIBI.cercle(point2x,point2y,2*epaisseurprisme,
      epaisseurlmd,clred,pmcopy,true);
      BIBI.cercle(hx,hy,2*epaisseurprisme,
      epaisseurlmd,clred,pmcopy,true);
      {BIBI.trait(hx,hy,point2x,point2y,epaisseurprisme,
  clblack,psdot,pmcopy,true);}
                       end;


                       if (ajoutlmd and point1fait and point2fait) then begin
      BIBI.trait(point1x,point1y,point2x,point2y,epaisseurlmd,couleurlmd,pssolid,pmcopy,true);
      BIBI.trait(axefocal_x1,axefocal_y1,axefocal_x2,axefocal_y2,epaisseurlmd,
  couleuraxelmd,psdashdot,pmcopy,true);
      BIBI.cercle(pointrx,pointry,2*epaisseurlmd,
      epaisseurlmd,couleurlmd,pmnotxor,true);
      lambda:=((x1-axefocal_x1)*(axefocal_x2-axefocal_x1)+(y1-axefocal_y1)*
      (axefocal_y2-axefocal_y1))/((axefocal_x2-axefocal_x1)*(axefocal_x2-axefocal_x1)+
      (axefocal_y2-axefocal_y1)*(axefocal_y2-axefocal_y1));
       pointrx:=axefocal_x1+lambda*(axefocal_x2-axefocal_x1);
        pointry:=axefocal_y1+lambda*(axefocal_y2-axefocal_y1);
         BIBI.cercle(pointrx,pointry,2*epaisseurlmd,
         epaisseurlmd,couleurlmd,pmnotxor,true);

                       end;
                       if (ajoutmscepa and point1fait and point2fait) then begin
      BIBI.trait(point1x,point1y,point2x,point2y,epaisseurmscepa,couleurmscepa,pssolid,pmcopy,true);
      BIBI.trait(axefocal_x1,axefocal_y1,axefocal_x2,axefocal_y2,epaisseurmscepa,
  couleuraxemscepa,psdashdot,pmcopy,true);
      BIBI.cercle(pointrx,pointry,2*epaisseurmscepa,
      epaisseurmscepa,couleurmscepa,pmnotxor,true);
      lambda:=((x1-axefocal_x1)*(axefocal_x2-axefocal_x1)+(y1-axefocal_y1)*
      (axefocal_y2-axefocal_y1))/((axefocal_x2-axefocal_x1)*(axefocal_x2-axefocal_x1)+
      (axefocal_y2-axefocal_y1)*(axefocal_y2-axefocal_y1));
      if lambda<0 then lambda:=0;
       pointrx:=axefocal_x1+lambda*(axefocal_x2-axefocal_x1);
        pointry:=axefocal_y1+lambda*(axefocal_y2-axefocal_y1);
         BIBI.cercle(pointrx,pointry,2*epaisseurmscepa,
         epaisseurmscepa,couleurmscepa,pmnotxor,true);

                       end;
666: if (modemodif  or modeduplication) then begin

if nombregroupe>0 then for i:=1 to nombregroupe do listegroupe[-1+i].recalculeiso;
if nombrepolycercle>0 then for i:=1 to nombrepolycercle do
listepolycercle[-1+i].dessine(bibi);
if nombrepolyhedre>0 then for i:=1 to nombrepolyhedre do
listepolyhedre[-1+i].dessine(bibi);
 if nombresphere>0 then for i:=1 to nombresphere do
listesphere[-1+i].dessine(bibi);
if nombrelec>0 then for i:=1 to nombrelec do
listelec[-1+i].dessine(bibi);
if nombremiroirplan>0 then for i:=1 to nombremiroirplan do
ListeMiroirPlan[-1+i].dessine(bibi);
if nombreReseau>0 then for i:=1 to nombreReseau do
ListeReseau[-1+i].dessine(bibi);
if nombrefleche>0 then for i:=1 to nombrefleche do
ListeFleche[-1+i].dessine(bibi);
if nombremiroirconique>0 then for i:=1 to nombremiroirconique do
ListeMiroirconique[-1+i].dessine(bibi);
if nombrerayon>0 then for i:=1 to nombrerayon do
listerayon[-1+i].dessine(bibi);
if nombresourceponctuelle>0 then for i:=1 to nombresourceponctuelle do
listesourceponctuelle[-1+i].dessine(bibi);
if nombreondeplane>0 then for i:=1 to nombreondeplane do
listeondeplane[-1+i].dessine(bibi);
if nombremscopa>0 then for i:=1 to nombremscopa do
listemscopa[-1+i].dessine(bibi);
if nombremscore>0 then for i:=1 to nombremscore do
listemscore[-1+i].dessine(bibi);
if nombremscere>0 then for i:=1 to nombremscere do
listemscere[-1+i].dessine(bibi);
if nombremscepa>0 then for i:=1 to nombremscepa do
listemscepa[-1+i].dessine(bibi);
if nombrelmc>0 then for i:=1 to nombrelmc do
listelmc[-1+i].dessine(bibi);
if nombrelmd>0 then for i:=1 to nombrelmd do
listelmd[-1+i].dessine(bibi);
if nombreecran>0 then for i:=1 to nombreecran do
listeecran[-1+i].dessine(bibi);
if nombreoeil>0 then for i:=1 to nombreoeil do
listeoeil[-1+i].dessine(bibi);
if nombrediaphragme>0 then for i:=1 to nombrediaphragme do
listediaphragme[-1+i].dessine(bibi);
if nombrelsr>0 then for i:=1 to nombrelsr do
listelsr[-1+i].dessine(bibi);
if nombremetre>0 then for i:=1 to nombremetre do
listemetre[-1+i].dessine(bibi);
if nombreangle>0 then for i:=1 to nombreangle do
listeangle[-1+i].dessine(bibi);
if nombretexte>0 then for i:=1 to nombretexte do
listetexteaffiche[-1+i].dessine(bibi);
if nombreanglesatracer>0 then for i:=1 to nombreanglesatracer do
listeanglesatracer[-1+i].dessinebis(bibi);
 if nombregroupe>0 then for i:=1 to nombregroupe do
 listegroupe[-1+i].dessine(bibi);
 if nombreprisme>0 then for i:=1 to nombreprisme do
 listeprisme[-1+i].dessine(bibi);
 DessineTousLesSegments(bibi);
  // form1.image1.Picture.Assign(bibi);
   end;

  //   if Form1.ajoutencours  then form1.image1.Picture.Assign(bibi);

affichecoordonnees(pointrx,pointry);
  //form1.statusbar1.Panels[0].Text:=floattostr(pointrx);
   //form1.statusbar1.Panels[1].Text:=floattostr(pointry);
   //exit;
end;



function TForm1.ajoutencours:boolean;
begin
ajoutencours:=(ajoutrayon or ajoutmiroirplan or ajoutreseau or ajoutmscopa or ajoutmscepa
 or ajoutlmc or ajoutlmd or ajoutmscore or ajoutmscere or
 ajoutsourceponctuelle or ajoutondeplane or ajoutecran or ajoutlsr or
 ajoutpoly or ajoutsphere or ajoutconiqueconcave or ajoutconiqueconvexe or ajouttexte or ajoutprisme
 or ajoutdiaphragme or ajoutlec or ajoutpolycercle or ajoutfleche or ajoutoeil);
end;

 procedure TForm1.FormActivate(Sender: TObject);

  begin

end;


procedure TForm1.FormCreate(Sender: TObject);

var f1:textfile; s1:string;   i,il:integer;

      f:textfile;   newmenuitem:tmenuitem;




begin
encreation:=true;
//application.MessageBox('kjbkjbkjb','kjkjbkjbkjb',mb_ok);
    if liste_langues.Count>0 then
                   for il:=1 to  liste_langues.Count do begin
                   newmenuitem:=tmenuitem.Create(mainmenu1);
                   newmenuitem.Caption:=liste_langues[il-1];
                   newmenuitem.Tag:=1000+il;
                   newmenuitem.OnClick:=@menuitem6click;
                   if  liste_langues[il-1]=fallbacklang then newmenuitem.Checked:=true;
                   menuitem6.add(newmenuitem);
                   end;



DoubleBuffered:=true;
  scrollbox1.DoubleBuffered:=true;

  AFFICHERLAGRILLE1.CAPTION:=rsAfficherLaGr;
  AFFICHERLESANGLES1.CAPTION:=rsAfficherLesA;
  AIDE1.CAPTION:=rsAide;
    AIDESUROPTGEO1.CAPTION:=rsAideSurOptGe;
    AJOUTER1.CAPTION:=rsAjouter;
    AJUSTERAUTOMATIQUEMENT1.CAPTION:=rsAjusterAutom;
    APROPOS2.CAPTION:=rsAPropos;
    ASSOCIERLESFICHIERSOPTAVECOPTGEO1.CAPTION:=rsAssocierLesF;
    ATTRACTIONDELAGRILLE1.caption:=
rsAttractionDe;

BASEDEREGISTRE1.caption:=
rsBaseDeRegist;


BOUTONANGLE.HINT:=
rsMesurerUnAng;


BOUTONCONIQUECONCAVE.HINT:=
rsAjouterUnMir;


BOUTONCONIQUECONVEXE.HINT:=
rsAjouterUnMir2;


BOUTONDIA.HINT:=
rsAjouterUnDia;


BOUTONDISTANCE.HINT:=
rsMesurerLaDis;


BOUTONECRAN.HINT:=
rsAjouterUnCra;


BOUTONFANTOME.HINT:=
rsCacherDesSeg;


BOUTONFLECHE.HINT:=
rsAjouterUnTra;


BOUTONLEC.HINT:=
rsAjouterUneLe;


BOUTONLMC.HINT:=
rsAjouterUneLe2;


BOUTONLMD.HINT:=
rsAjouterUneLe3;


BOUTONLSR.HINT:=
rsAjouterUneLa;


BOUTONMIROIRPLAN.HINT:=
rsAjouterUnMir3;


BOUTONMSCEPA.HINT:=
rsAjouterUnMir4;


BOUTONMSCERE.HINT:=
rsAjouterUnMir5;


BOUTONMSCOPA.HINT:=
rsAjouterUnMir6;


BOUTONMSCORE.HINT:=
rsAjouterUnMir7;


BOUTONOEIL.HINT:=
rsAjouterUnOei;


BOUTONONDEPLANE.HINT:=
rsAjouterUneOn;


BOUTONPOLY.HINT:=
rsAjouterUnPol;


BOUTONPOLYCERCLE.HINT:=
rsAjouterUnePo;

boutonreseau.Hint:=rsAjouterUnRSe;
BOUTONPRISME.HINT:=
rsAjouterUnPri;
MenuItemreseau.Caption:=rsRSeauDiffrac;

BOUTONRAYON.HINT:=
rsAjouterUnRay;


BOUTONSOURCEPONCTUELLE.HINT:=
rsAjouterUnObj;


BOUTONSPHERE.HINT:=
rsAjouterUneSp;


BOUTONSPV.HINT:=
rsAjouterUnObj2;


BOUTONTEXTE.caption:=
rsA;


BOUTONTEXTE.HINT:=
rsAjouterUnTex;


CAPTION:=
rsOptGeo;


COCHEANGLES.caption:=
rsAnglesDIncid;


COCHEATTRACTION.caption:=
rsAttraction;


COCHEGRILLE.caption:=
rsGrille;


COCHENORMALE.caption:=
rsNormales;


COMBOZOOM.HINT:=
rsFacteurDeZoo;


COMMENATIRESURLASIMULATION1.caption:=
rsCommentaireS2;


CONTACTERLAUTEUR1.caption:=
rsContacterLAu;


COPIER1.caption:=
rsCopierSimula2;


COULEURDEFOND1.caption:=
rsCouleurDeFon;


DFINIRLERPERTOIREPERSONNEL1.caption:=
rsDFinirLeRPer;


DIAPHRAGME1.caption:=
rsDiaphragme;


DIVERS1.caption:=
rsDivers;


DPLACERUNLMENT1.caption:=
rsDPlacerUnLMe2;


DSASSOCIER1.caption:=
rsDSassocier;


DUDERNIERRPERTOIREUTILIS1.caption:=
rsDuDernierRPe;


DUPLIQUERUNLMENT1.caption:=
rsDupliquerUnL2;


DURPERTOIREEXEMPLES1.caption:=
rsDeMonRPertoi;


DURPERTOIREEXEMPLES2.caption:=
Format(rsDuRPertoireE, ['"', '"']);


ECRAN1.caption:=
rsEcran;


EDITERLECOMMENTAIREDELASIMULATION1.caption:=
rsEditerLeComm;


EDITION1.caption:=
rsEdition;


ELEMENTSCHOISIR2.caption:=
rsElMentsChois;


EMPENNAGEDESRAYONS1.caption:=
rsEmpennageDes;


ENREGISTRERSIMULATION1.caption:=
rsEnregistrerS;


ENREGISTRERSIMULATION2.caption:=
rsEnregistrerS2;


FICHIER1.caption:=
rsFichier;


GRILLE1.caption:=
rsOptions;


GROUPERDESLMENTS1.caption:=
rsGrouperDesLM;


IMPRIMER1.caption:=
rsImprimer;


JOINDREPARUNARCDECERCLE1.caption:=
rsJoindreParUn;


JOINDREPARUNSEGMENT1.caption:=
rsJoindreParUn2;


LAMESEMIRFLCHISSANTE1.caption:=
rsLameSemiRFlC;


LENTILLE1.caption:=
rsLentille;


LENTILLEMINCECONVERGENTE1.caption:=
rsLentilleMinc;


LENTILLEMINCEDIVERGENTE1.caption:=
rsLentilleMinc2;


LICENSEGPL1.caption:=
rsLicenseGPL;


MANUELLEMENT1.caption:=
rsManuellement;


MENUITEM1.caption:=
rsExporterGrap;

menuitem7.Caption:=rsIndicesDuMil3;


MENUITEM3.caption:=
rsPNG;


MENUITEM4.caption:=
rsJPG;



MENUITEM6.caption:=
rsLangue;


MENUPRISME.caption:=
rsPrisme;


MESUREDANGLE1.caption:=
rsMesureDAngle;


MESUREDEDISTANCE1.caption:=
rsMesureDeDist;


MIROIRCNIQUE1.caption:=
rsMiroirCNique;


MIROIRPLAN2.caption:=
rsMiroirPlan;


MIROIRSPHRIQUECONCAVE1.caption:=
rsMiroirSphRiq;


MIROIRSPHRIQUECONVERGENT1.caption:=
rsMiroirSphRiq;


MIROIRSPHRIQUECONVEXE1.caption:=
rsMiroirSphRiq2;


MIROIRSPHRIQUECONVEXE2.caption:=
rsMiroirSphRiq2;


MODIFIERDIMENSIONSESPACEDETRAVAIL1.caption:=
rsModifierLesD;




N7.caption:=
rsAfficherLesN;



NOUVEAU1.caption:=
rsNouvelleSimu;

menuitem10.Caption:=rsDeMonRPertoi;
menuitem11.Caption:=Format(rsDuRPertoireE, ['"', '"']);
menuitem12.Caption:=rsDuDernierRPe2;
menuitem9.Caption:=rsSuperposerSi;

OEILSTYLIS1.caption:=
rsOeilStylis;


ONDEPLANE1.caption:=
rsOndePlane;


OPTIONSDELAGRILLE1.caption:=
rsOptionsDeLaG;


OPTIONSDESNORMALES1.caption:=
rsOptionsDesNo;


OUVRIRSIMULATION1.caption:=
rsOuvrirSimula;


POLYHDRERFRACTANT1.caption:=
rsPolyhDreRFra;


POLYSPHRERFRACTANTE1.caption:=
rsPolysphReRFr;


PRFRENCES1.caption:=
rsPrFRences;


PROPRITSDUNLMENT1.caption:=
rsPropriTSDUnL2;


QUITTER1.caption:=
rsQuitter;


RAYONSFANTOMES1.caption:=
rsRayonsFantom;


RAYONUNIQUE1.caption:=
rsRayonUnique;


REFAIRE1.caption:=
rsRefaire;


RTABLIR1.caption:=
rsdefaire;


SITEWEB1.caption:=
rsSiteWeb;


SOURCE2.caption:=
rsSource;


SOURCEPONCTUELLE1.caption:=
rsObjetPonctue;


SPEEDBUTTON1.HINT:=
rsAjouterOuDit;


SPEEDBUTTON2.caption:=
rsCoord;


SPEEDBUTTON2.HINT:=
rsAfficherLesC;


SPHRERFRACTANTE1.caption:=
rsSphReRFracta;


STATICTEXT1.caption:=
rsZoom;


STATICTEXT2.caption:=
rsMiroirs;


STATICTEXT3.caption:=
rsSources;


STATICTEXT4.caption:=
rsDioptres;


STATICTEXT5.caption:=
rsEcrans;


STATICTEXT6.caption:=
rsDivers2;


STATICTEXT7.caption:=
rsAfficher;


SUPPRIMERUNLMENT1.caption:=
rsSupprimerUnL2;


SURFACESDONDE1.caption:=
rsSurfacesDOnd;


SYSTMEOPTIQUE1.caption:=
rsSystMeOptiqu;


SYSTMEOPTIQUEREL1.caption:=
rsSystMeOptiqu2;


TEXTESURLEDESSIN1.caption:=
rsTexteSurLeDe;




TOUSLESLMENTS1.caption:=
rsTousLesLMent;


TRAITFLCHE1.caption:=
rsTraitFlChe;



 // BIBI:=tmonbitmap.Create;
      screen.Cursors[crDepla]:= LoadCursorFromLazarusResource('DEPLA');
      screen.Cursors[crFin]:= LoadCursorFromLazarusResource('FIN');
        screen.Cursors[crDeplacement]:= LoadCursorFromLazarusResource('DEPLACEMENT');
      screen.Cursors[crDebut]:= LoadCursorFromLazarusResource('DEBUT');
      screen.Cursors[crancre]:= LoadCursorFromLazarusResource('ANCRE');
     screen.Cursors[crPoint1]:=  LoadCursorFromLazarusResource('POINT1');
     screen.Cursors[crPoint2]:=  LoadCursorFromLazarusResource('POINT2');
     screen.Cursors[crFoyer]:=  LoadCursorFromLazarusResource('FOYER');
     screen.Cursors[crDepFoyer]:=  LoadCursorFromLazarusResource('DEPFOYER');
     screen.Cursors[crCentre]:=  LoadCursorFromLazarusResource('CENTRE');
     screen.Cursors[crmarteau]:=  LoadCursorFromLazarusResource('MARTEAU');
     screen.Cursors[crCentre1]:=  LoadCursorFromLazarusResource('CENTRE1');
     screen.Cursors[crCentre2]:=  LoadCursorFromLazarusResource('CENTRE2');
     screen.Cursors[crDepCentre]:=  LoadCursorFromLazarusResource('DEPCENTRE');
     screen.Cursors[crSource]:=  LoadCursorFromLazarusResource('SOURCE');
     screen.Cursors[crDirection]:=  LoadCursorFromLazarusResource('DIRECTION');
     screen.Cursors[crViseur]:=  LoadCursorFromLazarusResource('VISEUR');
     screen.Cursors[crtranslation]:=  LoadCursorFromLazarusResource('TRANSLATION');
     screen.Cursors[crrotation]:=  LoadCursorFromLazarusResource('ROTATION');
     screen.Cursors[crinfo]:=  LoadCursorFromLazarusResource('INFO');
     screen.Cursors[crprop]:=  LoadCursorFromLazarusResource('PROP');
      screen.Cursors[crsuivant]:=  LoadCursorFromLazarusResource('SUIVANT');
       screen.Cursors[crdupliquer]:= LoadCursorFromLazarusResource('DUPLIQUER');
       screen.Cursors[crsommet]:=  LoadCursorFromLazarusResource('SOMMET');
       screen.Cursors[crsommet1]:=  LoadCursorFromLazarusResource('SOMMET1');
       screen.Cursors[crsommet2]:=  LoadCursorFromLazarusResource('SOMMET2');
       screen.Cursors[crtexte]:= LoadCursorFromLazarusResource('TEXTE');
       screen.Cursors[crtaille]:=  LoadCursorFromLazarusResource('TAILLE');
        screen.Cursors[crdepsommet]:=  LoadCursorFromLazarusResource('DEPSOMMET');
          screen.Cursors[crgroupe]:=  LoadCursorFromLazarusResource('GROUPE');
          screen.Cursors[crcentreg]:=  LoadCursorFromLazarusResource('CENTREG');
     creation:=true;
     Combozoom.itemindex:=0;
   toolbar1.ButtonHeight:=25;
   toolbar2.ButtonHeight:=25;

for i:=1 to maxannulation do   etatactuel[i]:=tstringlist.Create;

  policedefaut:=tfont.Create;
      policedefaut.Assign(form1.font);
  end;

procedure tform1.desactiveboutons;
var i:integer;
begin
     for i:=0 to 4 do
mainmenu1.Items.items[i].enabled:=false;
cochegrille.Enabled:=false;
cocheattraction .enabled:=false;
cochenormale.Enabled:=false;
cocheangles.enabled:=false;
combozoom.Enabled:=false;
boutontexte.enabled:=false;   form_palette.boutongroupe.Enabled:=false;
boutonangle.Enabled:=false;
  boutonconiqueconcave.enabled:=false;
  boutonconiqueconvexe.enabled:=false;
 boutonmiroirplan.enabled:=false;    boutonreseau.enabled:=false;
 boutonfleche.enabled:=false;
 form_palette.boutondupliquer.enabled:=false;
 boutonpoly.enabled:=false;
 boutonprisme.Enabled:=false;
 boutonpolycercle.enabled:=false;
  boutonecran.enabled:=false;  boutondia.Enabled:=false;
  boutonlsr.enabled:=false;  boutonoeil.enabled:=false;
 boutonrayon.enabled:=false;
 boutonmscopa.enabled:=false;
 boutonmscepa.enabled:=false;
 boutonmscere.enabled:=false;
 boutonmscore.enabled:=false;
 boutonlmc.enabled:=false;
 boutonlmd.enabled:=false;
 boutonlec.enabled:=false;
  form_palette.speedbutton3.Enabled:=false;
 form_palette.speedbutton4.Enabled:=false;
 form_palette.speedbutton5.Enabled:=false;
 form_palette.speedbutton2.Enabled:=false;
 form_palette.speedbutton6.Enabled:=false;
 form_palette.speedbutton7.Enabled:=false;
 boutonsourceponctuelle.enabled:=false;
 boutonspv.enabled:=false;
 boutonondeplane.enabled:=false;
 form_palette.boutondeplacement.enabled:=false;
 form_palette.boutonsuppression.enabled:=false;
 form_palette.boutonprop.enabled:=false;  boutonsphere.enabled:=false;
 boutondistance.enabled:=false;  
 point1fait:=false;
 point2fait:=false; point3fait:=false;  point4fait:=false;
 foyerfait:=false; sommetfait:=false;
 if ajoutrayon then StatusBar1.Panels[3].text:=rsModeAjoutDUn;
 if ajoutmiroirplan then StatusBar1.Panels[3].text:=rsModeAjoutDUn2;
   if ajoutreseau then StatusBar1.Panels[3].text:=rsModeAjoutDUn25; ;
  if ajoutfleche then StatusBar1.Panels[3].text:=rsModeAjoutDUn3;
 if ajoutconiqueconcave then StatusBar1.Panels[3].text:=rsModeAjoutDUn4;
  if ajoutconiqueconvexe then StatusBar1.Panels[3].text:=rsModeAjoutDUn5;
 if ajoutpoly then StatusBar1.Panels[3].text:=rsModeAjoutDUn6;
  if ajoutprisme then StatusBar1.Panels[3].text:=rsModeAjoutDUn7;
  if ajoutpolycercle then StatusBar1.Panels[3].text:=rsModeAjoutDUn8;
  if ajoutecran then StatusBar1.Panels[3].text:=rsModeAjoutDUn9;
   if ajoutoeil then StatusBar1.Panels[3].text:=rsModeAjoutDUn10;
   if ajoutlsr then StatusBar1.Panels[3].text:=rsModeAjoutDUn11;
 if ajoutmscopa then StatusBar1.Panels[3].text:=rsModeAjoutDUn12;
 if ajoutmscepa then StatusBar1.Panels[3].text:=rsModeAjoutDUn13;
 if ajoutlmc then StatusBar1.Panels[3].text:=rsModeAjoutDUn14;
 if ajoutlec then StatusBar1.Panels[3].text:=rsModeAjoutDUn15;
 if ajoutsphere then StatusBar1.Panels[3].text:=rsModeAjoutDUn16;
 if ajoutlmd then StatusBar1.Panels[3].text:=rsModeAjoutDUn17;
 if ajoutmscore then StatusBar1.Panels[3].text:=rsModeAjoutDUn18;
 if ajoutmscere then StatusBar1.Panels[3].text:=rsModeAjoutDUn19;
 if ajoutsourceponctuelle  then StatusBar1.Panels[3].text:=rsModeAjoutDUn20;
 if ajoutondeplane then  StatusBar1.Panels[3].text:=rsModeAjoutDUn21;
if modemodif then StatusBar1.Panels[3].text:=rsModeDPlaceme;
if modeinfo then  StatusBar1.Panels[3].text:=rsModePropriTS;
if modeduplication then  StatusBar1.Panels[3].text:=rsModeDuplicat;
if modesuppression then  StatusBar1.Panels[3].text:=rsModeSuppress;
if modedistance then  StatusBar1.Panels[3].text:=rsModeMesureDe;
if modeangle then  StatusBar1.Panels[3].text:=rsModeMesureDA;
if ajouttexte then StatusBar1.Panels[3].text:=rsModeAjoutDUn22;
if ajoutdiaphragme then StatusBar1.Panels[3].text:=rsModeAjoutDUn23;
if modegroupement then StatusBar1.Panels[3].text:=Format(rsModeAjoutDUn24, [
  '"', '"']);
      end;

  procedure tform1.activeboutons;
  var i:integer;  toto:string;
  begin
   for i:=0 to 4 do
mainmenu1.Items.items[i].enabled:=true;
cochegrille.Enabled:=true;
cocheattraction.Enabled:=true;
cochenormale.Enabled:=true;
cocheangles.enabled:=true;
combozoom.Enabled:=true;
  StatusBar1.Panels[3].text:='';
   boutonconiqueconcave.enabled:=true;
   boutontexte.Enabled:=true;       form_palette.boutongroupe.Enabled:=true;
   boutonangle.Enabled:=true;
   boutonconiqueconvexe.enabled:=true;
 boutonmiroirplan.enabled:=true;   boutonreseau.enabled:=true;
 boutonfleche.enabled:=true;
 form_palette.boutondupliquer.enabled:=true;
 form_palette.boutondupliquer.caption:=rsDupliquer;
 boutonecran.enabled:=true;     boutondia.Enabled:=true;
 boutonlsr.enabled:=true;   boutonoeil.Enabled:=true;
 boutonpoly.enabled:=true; boutonprisme.Enabled:=true;
 boutonpolycercle.enabled:=true;
 boutonrayon.enabled:=true;
 boutonmscopa.enabled:=true;
 boutonmscore.enabled:=true;
 boutonmscere.enabled:=true;
 boutonmscepa.enabled:=true;
 boutonlmc.enabled:=true;   boutonlec.enabled:=true;
 boutonlmd.enabled:=true;
 boutonsourceponctuelle.enabled:=true;
 boutonspv.enabled:=true;
 boutonondeplane.enabled:=true;
 form_palette.boutondeplacement.enabled:=true;
 form_palette.boutonprop.enabled:=true;  boutonsphere.enabled:=true;
 boutondistance.enabled:=true;
 form_palette.speedbutton3.Enabled:=true;
 form_palette.speedbutton4.Enabled:=true;
 form_palette.speedbutton5.Enabled:=true;
 form_palette.speedbutton2.Enabled:=true;
 form_palette.speedbutton6.Enabled:=true;
 form_palette.speedbutton7.Enabled:=true;
  form_palette.boutonsuppression.enabled:=true;
  form_palette.boutondeplacement.caption:=rsDPlacer;
  toto:=form_palette.boutondeplacement.caption;

   form_palette.boutonsuppression.caption:=rsSupprimer;
   form_palette.boutonprop.caption:=rsPropriTS;
 ajoutmiroirplan:=false;   ajoutreseau:=false;  ajoutfleche:=false;
ajoutpoly:=false;   ajoutpolycercle:=false;     ajoutprisme:=false;
 ajoutecran:=false; ajoutdiaphragme:=false;
 ajoutlsr:=false;    ajoutoeil:=false;
 ajoutrayon:=false;
 ajoutmscopa:=false;
 ajoutmscore:=false;
 ajoutmscere:=false;
 ajoutmscepa:=false;
 ajoutlmc:=false;  ajoutsphere:=false;   ajoutlec:=false;
 ajoutlmd:=false;   ajouttexte:=false;
 ajoutsourceponctuelle:=false;
 ajoutondeplane:=false;    ajoutconiqueconcave:=false; ajoutconiqueconvexe:=false;
 modifencours:=false;
 modemodif:=false;    modegroupement:=false;
 modeinfo:=false;  modeduplication:=false;  modedistance:=false; modeangle:=false;
 modesuppression:=false;
      end;

      procedure tform1.retaille_image1;
      begin

       if not(respectrapport) then begin
 form1.image1.width:=largeurinitialeimage1*zoomfactor;
 form1.image1.height:=hauteurinitialeimage1*zoomfactor;
end;
if respectrapport then
 if ((yymax-yymin)/(xxmax-xxmin)>rapportideal) then begin
  form1.image1.height:=hauteurinitialeimage1*zoomfactor;
 form1.image1.width:=trunc(hauteurinitialeimage1/rapportactuel)*zoomfactor;
 end else begin
 form1.image1.width:=largeurinitialeimage1*zoomfactor;
  form1.image1.height:=trunc(largeurinitialeimage1*rapportactuel)*zoomfactor;
   end;
   form1.Image1.Picture.Bitmap.Width:=form1.Image1.Width;
   form1.Image1.Picture.Bitmap.height:=form1.Image1.height;
   //form1.Image1.Refresh;
   form1.ScrollBox1.HorzScrollBar.Visible:=false;
  form1.ScrollBox1.VertScrollBar.Visible:=false;
  form1.ScrollBox1.Refresh;
  form1.ScrollBox1.HorzScrollBar.Visible:=(form1.image1.width>form1.ScrollBox1.ClientWidth);
  form1.ScrollBox1.VertScrollBar.Visible:=(form1.image1.height>form1.ScrollBox1.ClientHeight);
 if form1.ScrollBox1.HorzScrollBar.Visible then
   form1.ScrollBox1.HorzScrollBar.Range:=form1.image1.width;
    if form1.ScrollBox1.VertScrollBar.Visible then
   form1.ScrollBox1.VertScrollBar.Range:=form1.image1.height;

      end;

      procedure TForm1.MenuItem3Click(Sender: TObject);
      var png : TPortableNetworkGraphic; PngFileName:string;
begin
 savedialog2.FileName:='';
 savedialog2.InitialDir:=repertoire_simul_perso;
if savedialog2.Execute then
PngFileName:=(savedialog2.FileName);
if pngfilename='' then exit;
 if fileexistsutf8(PngFileName) then
 if application.MessageBox(pchar(rsVoulezVousLC), pchar(rsCeFichierExi), mb_yesno
   )=idno then exit;
  png := TPortableNetworkGraphic.Create;
  try
    png.Assign(form1.image1.Picture.bitmap);
    png.SaveToFile((PngFileName));
  finally
    png.Free;
  end;

      end;



      procedure TForm1.MenuItem4Click(Sender: TObject);
      var jpg : TJPEGImage; JpgFileName:string;
begin
  savedialog3.FileName:='';
 savedialog3.InitialDir:=repertoire_simul_perso;
if savedialog3.Execute then
JpgFileName:=(savedialog3.FileName);
if jpgfilename='' then exit;
 if fileexistsutf8(JpgFileName) then
 if application.MessageBox(pchar(rsVoulezVousLC2), pchar(rsCeFichierExi),
   mb_yesno)=idno then exit;
 jpg := TJPEGImage.Create;
  try
    jpg.Assign(form1.Image1.Picture.bitmap);
    jpg.SaveToFile((JpgFileName));
  finally
    jpg.Free;
      end;  end;

      procedure TForm1.MenuItem6Click(Sender: TObject);
      var n:integer; nom_ini_file:string; f:textfile;
begin
n:=(Sender as TComponent).Tag;
if n<=1000 then exit;
if liste_langues[n-1000-1]=fallbacklang then exit;

application.MessageBox(pchar(rsPourPrendreE), pchar(rsChangementDe), mb_ok);

  nom_ini_file:=repertoire_config_perso+'optgeo.ini';
              assignfile(f,(nom_ini_file));
        rewrite(f);
        writeln(f,liste_langues[n-1000-1]+'_'+uppercase(liste_langues[n-1000-1]));
               writeln(f,liste_langues[n-1000-1]);
        closefile(f);

 application.Terminate;

      end;

      procedure TForm1.MenuItem7Click(Sender: TObject);
      begin
         with saisie_indices_milieu_ambiant
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
Edit_indice_bleu_par_defaut.text:=floattostr(indice_bleu_par_defaut);
Edit_indice_vert_par_defaut.text:=floattostr(indice_vert_par_defaut);
Edit_indice_rouge_par_defaut.text:=floattostr(indice_rouge_par_defaut);
FloatSpinEditrouge.value:=longueur_donde_red*1e9;
   FloatSpinEditbleu.value:=longueur_donde_blue*1e9;
    FloatSpinEditvert.value:=longueur_donde_green*1e9;
end;
saisie_indices_milieu_ambiant.ShowModal;
Rafraichit;
      end;

      procedure TForm1.MenuItem8Click(Sender: TObject);
       var  PSFileName:string;
       mps:TMonPostScript;
         label 3012,628;
var iiii:integer;
 i,j,iii,nn1,nn2:integer;
 tt1,tt2,rra:extended;
   bord_b,bord_h,bord_g,bord_d:integer;
   rap:extended;
dede:string;
hhhh,llll:integer;
begin
if not(graphepresent) then exit;
savedialog4.FileName:='';
savedialog4.InitialDir:=repertoire_simul_perso;
if savedialog4.Execute then
PSFileName:=(savedialog4.FileName);
if psfilename='' then exit;
 if fileexistsutf8(PsFileName) then
 if application.MessageBox(pchar(rsVoulezVousLC), pchar(rsCeFichierExi), mb_yesno
   )=idno then exit;



 case  graphehorizontal of
tgauche: FormConfigPostScript.radiogroupehorizontal.itemindex:=1;
tdroite: FormConfigPostScript.radiogroupehorizontal.itemindex:=0;
tcentreh: FormConfigPostScript.radiogroupehorizontal.itemindex:=2;
end;
case graphevertical of
thaut: FormConfigPostScript.radiogroupevertical.itemindex:=0;
tbas: FormConfigPostScript.radiogroupevertical.itemindex:=1;
tcentrev: FormConfigPostScript.radiogroupevertical.itemindex:=2;
end;
FormConfigPostScript.spinpourcentagehorizontal.value:=taillegraphehorizontal;
FormConfigPostScript.spinpourcentagevertical.value:=taillegraphevertical;

 with  FormConfigPostScript do
begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
showmodal;
end;

 Donne_Hauteur_Largeur(p_ps,or_ps,res_ps_x,res_ps_y,hhhh,llll);


 if not(respect_ps) then begin
 case graphehorizontal of
 tgauche: begin
 bord_g:=0;
 bord_d:=trunc(llll-llll/100*taillegraphehorizontal);
 end;
 tdroite: begin
 bord_d:=0;
 bord_g:=trunc(llll-llll/100*taillegraphehorizontal);
 end;
 tcentreh: begin
 bord_g:=trunc((llll-llll/100*taillegraphehorizontal)/2);
 bord_d:=bord_g;
 end;
 end;

 case graphevertical of
 thaut: begin
 bord_h:=0;
 bord_b:=trunc(hhhh-hhhh/100*taillegraphevertical);
 end;
 tbas: begin
 bord_b:=0;
 bord_h:=trunc(hhhh-hhhh/100*taillegraphevertical);
 end;
 tcentrev: begin
 bord_b:=trunc((hhhh-hhhh/100*taillegraphevertical)/2);
 bord_h:=bord_b;
 end;
 end;

 end
 else
 begin
 rap:=(yymax-yymin)/(xxmax-xxmin);
 if rap<taillegraphevertical/taillegraphehorizontal*hhhh/llll then
 begin
  case graphehorizontal of
 tgauche: begin
 bord_g:=0;
 bord_d:=trunc(llll-llll/100*taillegraphehorizontal);
 end;
 tdroite: begin
 bord_d:=0;
 bord_g:=trunc(llll-llll/100*taillegraphehorizontal);
 end;
 tcentreh: begin
 bord_g:=trunc((llll-llll/100*taillegraphehorizontal)/2);
 bord_d:=bord_g;
 end;
 end;

 case graphevertical of
 thaut: begin
 bord_h:=0;
 bord_b:=trunc(hhhh-llll/100*taillegraphevertical);
 end;
 tbas: begin
 bord_b:=0;
 bord_h:=trunc(hhhh-rap*llll/100*taillegraphevertical);
 end;
 tcentrev: begin
 bord_b:=trunc((hhhh-rap*llll/100*taillegraphevertical)/2);
 bord_h:=bord_b;
 end;
 end;


 end else
 begin
  case graphehorizontal of
 tgauche: begin
 bord_g:=0;
 bord_d:=trunc(llll-hhhh/100/rap*taillegraphehorizontal);
 end;
 tdroite: begin
 bord_d:=0;
 bord_g:=trunc(llll-hhhh/100/rap*taillegraphehorizontal);
 end;
 tcentreh: begin
 bord_g:=trunc((llll-hhhh/100/rap*taillegraphehorizontal)/2);
 bord_d:=bord_g;
 end;
 end;

 case graphevertical of
 thaut: begin
 bord_h:=0;
 bord_b:=trunc(hhhh-hhhh/100*taillegraphevertical);
 end;
 tbas: begin
 bord_b:=0;
 bord_h:=trunc(hhhh-hhhh/100*taillegraphevertical);
 end;
 tcentrev: begin
 bord_b:=trunc((hhhh-hhhh/100*taillegraphevertical)/2);
 bord_h:=bord_b;
 end;
 end;
 end;
 end;







  ps := TMonPostScriptCanvas.Create;
  try







 optionsurfacesonde:=false;
retaillepsegment(listesegment,nombretotalsegment,0);
retaillepsegment(listenormale,nombrenormale,0);

   if nombreanglesatracer>0 then begin
 retaillepangle(listeanglesatracer,nombreanglesatracer,0);
 nombreanglesatracer:=0;
 end;
  nombretotalsegment:=0; nombreenfant:=0;   nombrenormale:=0;
  creeprayon(listerayonenfant,0);
  if nombrerayon>0 then for i:=1 to nombrerayon do
  if not( (listerayon[-1+i].couleur=clblack) and (unit222.unmilieudispersif or (nombrereseau>0))) then calculesegment0(listerayon[-1+i].ax,
  listerayon[-1+i].ay,listerayon[-1+i].kx,listerayon[-1+i].ky,listerayon[-1+i].anglepolaire,listerayon[-1+i].epaisseur,
  listerayon[-1+i].couleur,listerayon[-1+i].vav,
  listerayon[-1+i].vaa,listerayon[-1+i].nombremaxenfant,listerayon[-1+i].maxsegment) else
   begin
   retailleprayon(listerayonenfant,nombreenfant,nombreenfant+1);
   inc(nombreenfant);
  listerayonenfant[-1+nombreenfant].createenfant(listerayon[-1+i].ax,listerayon[-1+i].ay
  ,listerayon[-1+i].kx,listerayon[-1+i].ky,clred,listerayon[-1+i].epaisseur,
    listerayon[-1+i].vav,listerayon[-1+i].vaa,listerayon[-1+i].nombremaxenfant,listerayon[-1+i].maxsegment,0);
   retailleprayon(listerayonenfant,nombreenfant,nombreenfant+1);
   inc(nombreenfant);
  listerayonenfant[-1+nombreenfant].createenfant(listerayon[-1+i].ax,listerayon[-1+i].ay
  ,listerayon[-1+i].kx,listerayon[-1+i].ky,clgreen,listerayon[-1+i].epaisseur,
    listerayon[-1+i].vav,listerayon[-1+i].vaa,listerayon[-1+i].nombremaxenfant,listerayon[-1+i].maxsegment,0);
    retailleprayon(listerayonenfant,nombreenfant,nombreenfant+1);
   inc(nombreenfant);
  listerayonenfant[-1+nombreenfant].createenfant(listerayon[-1+i].ax,listerayon[-1+i].ay
  ,listerayon[-1+i].kx,listerayon[-1+i].ky,clblue,listerayon[-1+i].epaisseur,
    listerayon[-1+i].vav,listerayon[-1+i].vaa,listerayon[-1+i].nombremaxenfant,listerayon[-1+i].maxsegment,0);
  end;

  if nombresourceponctuelle>0 then for i:=1 to nombresourceponctuelle do
  listesourceponctuelle[-1+i].calculesegment;
  if nombreondeplane>0 then for i:=1 to nombreondeplane do listeondeplane[-1+i].calculesegment;
  if nombreenfant=0 then goto 3012;
  i:=0;
    628:   inc(i);
  calculesegment0(listerayonenfant[-1+i].ax,
  listerayonenfant[-1+i].ay,listerayonenfant[-1+i].kx,listerayonenfant[-1+i].ky,listerayonenfant[-1+i].anglepolaire,
  listerayonenfant[-1+i].epaisseur,
  listerayonenfant[-1+i].couleur,listerayonenfant[-1+i].vav,
  listerayonenfant[-1+i].vaa,listerayonenfant[-1+i].nombremaxenfant,listerayonenfant[-1+i].maxsegment);
  if i<nombreenfant then goto 628;
 3012: freeprayon(listerayonenfant,nombreenfant);
   nombreenfant:=0;   nn1:=nombretotalsegment;


 mps.LimitesEtAxes(@ps,res_ps_x,res_ps_y,p_ps,or_ps,xxmin,xxmax,yymin,yymax,yymin,yymax,policedefaut,'',policedefaut,true,couleurfondsimulation,false,
 0,clblack,false,0,0,clblack,
policedefaut,false,false,{grillex,grilley,}1,clblack,clblack,'','','','','','',false,false,false
,bord_g,bord_d,bord_b,bord_h);
mps.background(couleurfondsimulation);
if tracergrille then mps.dessinergrille(grillex,grilley);
if nombregroupe>0 then for i:=1 to nombregroupe do listegroupe[-1+i].recalculeiso;
if nombreprisme>0 then for i:=1 to nombreprisme do
listeprisme[-1+i].dessinePS(mps);
if nombrepolycercle>0 then for i:=1 to nombrepolycercle do
listepolycercle[-1+i].dessinePS(mps);
if nombrepolyhedre>0 then for i:=1 to nombrepolyhedre do
listepolyhedre[-1+i].dessinePS(mps);
 if nombresphere>0 then for i:=1 to nombresphere do
listesphere[-1+i].dessinePS(mps);
if nombrelec>0 then for i:=1 to nombrelec do
listelec[-1+i].dessinePS(mps);
if nombremiroirplan>0 then for i:=1 to nombremiroirplan do
ListeMiroirPlan[-1+i].dessinePS(mps);
  if nombreReseau>0 then for i:=1 to nombreReseau do
  ListeReseau[-1+i].dessinePS(mps);
if nombrefleche>0 then for i:=1 to nombrefleche do
ListeFleche[-1+i].dessinePS(mps);
if nombremiroirconique>0 then for i:=1 to nombremiroirconique do
ListeMiroirconique[-1+i].dessinePS(mps);
if nombrerayon>0 then for i:=1 to nombrerayon do
listerayon[-1+i].dessinePS(mps);
if nombresourceponctuelle>0 then for i:=1 to nombresourceponctuelle do
listesourceponctuelle[-1+i].dessinePS(mps);
if nombreondeplane>0 then for i:=1 to nombreondeplane do
listeondeplane[-1+i].dessinePS(mps);
if nombremscopa>0 then for i:=1 to nombremscopa do
listemscopa[-1+i].dessinePS(mps);
if nombremscore>0 then for i:=1 to nombremscore do
listemscore[-1+i].dessinePS(mps);
if nombremscere>0 then for i:=1 to nombremscere do
listemscere[-1+i].dessinePS(mps);
if nombremscepa>0 then for i:=1 to nombremscepa do
listemscepa[-1+i].dessinePS(mps);
if nombrelmc>0 then for i:=1 to nombrelmc do
listelmc[-1+i].dessinePS(mps);
if nombrelmd>0 then for i:=1 to nombrelmd do
listelmd[-1+i].dessinePS(mps);
if nombreecran>0 then for i:=1 to nombreecran do
listeecran[-1+i].dessinePS(mps);
if nombreoeil>0 then for i:=1 to nombreoeil do
listeoeil[-1+i].dessinePS(mps);
if nombrediaphragme>0 then for i:=1 to nombrediaphragme do
listediaphragme[-1+i].dessinePS(mps);
if nombrelsr>0 then for i:=1 to nombrelsr do
listelsr[-1+i].dessinePS(mps);
if nombremetre>0 then for i:=1 to nombremetre do
listemetre[-1+i].dessinePS(mps);
if nombreangle>0 then for i:=1 to nombreangle do
listeangle[-1+i].dessinePS(mps);
if nombretexte>0 then for i:=1 to nombretexte do
listetexteaffiche[-1+i].dessinePS(mps);
if nombreanglesatracer>0 then for i:=1 to nombreanglesatracer do
listeanglesatracer[-1+i].dessinebisPS(mps);
 if nombregroupe>0 then for i:=1 to nombregroupe do
 listegroupe[-1+i].dessinePS(mps);
  if (nombrelmc+nombrelmd+nombremscopa+nombremscepa=0) then
 for i:=1 to nombresourceponctuelle do
 if listesourceponctuelle[-1+i].tracersurfacesonde then
 if not(listesourceponctuelle[-1+i].sourcevirtuelle) then
 if  listesourceponctuelle[-1+i].nombrecheminsoptiques>0 then
 begin
   tt1:=listesourceponctuelle[-1+i].ray[1].anglepolaire;
        tt2:=listesourceponctuelle[-1+i].ray[listesourceponctuelle[-1+i].nombrederayon].anglepolaire;
       if tt2<tt1 then tt2:=tt2+2*Pi;
  if tt2<tt1 then tt2:=tt2+2*Pi;
       rra:=sqrt(sqr(listesourceponctuelle[-1+i].a1x-listesourceponctuelle[-1+i].sx)
       +sqr(listesourceponctuelle[-1+i].a1y-listesourceponctuelle[-1+i].sy));
  if (sqrt(sqr(listesourceponctuelle[-1+i].a2x-listesourceponctuelle[-1+i].sx)
  +sqr(listesourceponctuelle[-1+i].a2y-listesourceponctuelle[-1+i].sy))<rra) then
  rra:=sqrt(sqr(listesourceponctuelle[-1+i].a2x-listesourceponctuelle[-1+i].sx)
  +sqr(listesourceponctuelle[-1+i].a2y-listesourceponctuelle[-1+i].sy));

 for j:=1 to   listesourceponctuelle[-1+i].nombrecheminsoptiques do
 begin

        optionsurfacesonde:=true;
         premierpointsurfacesonde:=true;
        cheminactuelaobtenir:=listesourceponctuelle[-1+i].listecheminsoptiques[j];

       for iii:=1 to maxrayonsrecherchesurfacesonde do  begin
       calculesegment0(listesourceponctuelle[-1+i].sx,listesourceponctuelle[-1+i].sy,
    rra*cos(tt1+(iii-1)*(tt2-tt1)/(maxrayonsrecherchesurfacesonde-1)),
  rra*sin(tt1+(iii-1)*(tt2-tt1)/(maxrayonsrecherchesurfacesonde-1)),tt1+(iii-1)*(tt2-tt1)/
  (maxrayonsrecherchesurfacesonde-1)
  ,1,listesourceponctuelle[-1+i].couleur,'','',0,listesourceponctuelle[-1+i].maxsegment);
     mps.trait(listesegment[-1+nombretotalsegment].a1x,
     listesegment[-1+nombretotalsegment].a1y,
     listesegment[-1+nombretotalsegment].a2x,
 listesegment[-1+nombretotalsegment].a2y,1,
 listesegment[-1+nombretotalsegment].couleur,psdot,pmcopy,true);
                end;
 end;  end;

    if (nombrelmc+nombrelmd+nombremscopa+nombremscepa=0) then
 for i:=1 to nombreondeplane do
 if listeondeplane[-1+i].tracersurfacesonde then
 if  listeondeplane[-1+i].nombrecheminsoptiques>0 then
  begin
 for j:=1 to   listeondeplane[-1+i].nombrecheminsoptiques do
 begin

        optionsurfacesonde:=true;
         premierpointsurfacesonde:=true;
        cheminactuelaobtenir:=listeondeplane[-1+i].listecheminsoptiques[j];

       for iii:=1 to maxrayonsrecherchesurfacesonde do  begin
       calculesegment0(listeondeplane[-1+i].a1x+
       (iii-1)*(listeondeplane[-1+i].a2x-listeondeplane[-1+i].a1x)/
       (maxrayonsrecherchesurfacesonde-1),
       listeondeplane[-1+i].a1y+
       (iii-1)*(listeondeplane[-1+i].a2y-listeondeplane[-1+i].a1y)/
       (maxrayonsrecherchesurfacesonde-1),
    listeondeplane[-1+i].ray[1].kx,
  listeondeplane[-1+i].ray[1].ky,
  listeondeplane[-1+i].ray[1].anglepolaire
  ,1,listeondeplane[-1+i].couleur,'','',0,listeondeplane[-1+i].maxsegment);
     mps.trait(listesegment[-1+nombretotalsegment].a1x,
     listesegment[-1+nombretotalsegment].a1y,
     listesegment[-1+nombretotalsegment].a2x,
 listesegment[-1+nombretotalsegment].a2y,1,
 listesegment[-1+nombretotalsegment].couleur,psdot,pmcopy,true);
                end;
 end;  end;
  nn2:=nombretotalsegment;
  nombretotalsegment:=nn1;
 DessineTousLesSegmentsPS(mps);






    ps.SaveToFile((PsFileName));
  finally
    ps.Free;
  end;

      end;

procedure TForm1.MenuItemreseauClick(Sender: TObject);
begin
 if modequelquechose then exit;
 if NombreReseau=maxReseau then begin
     application.MessageBox(pchar(rsNombreMaxima37), pchar(rsAttention3), mb_ok
       );
 exit;
 end;

 with Fsaisiereseau
 do begin
 if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
 if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
 end;
  fsaisiereseau.boutonsup.enabled:=false;
     fsaisiereseau.editx1.text:='';
     fsaisiereseau.editx2.text:='';
     fsaisiereseau.edity1.text:='';
     fsaisiereseau.edity2.text:='';
     fsaisiereseau.cochehachures.checked:=false;
     fsaisiereseau.editepaisseur.Value:=epaisseurReseau;
     fsaisiereseau.radiogroup1.itemindex:=0;
     fsaisiereseau.floatspinedit1.value:=500;
     fsaisiereseau.spinedit_ordre_min.value:=-1;
      fsaisiereseau.spinedit_ordre_max.value:=1;
 fsaisiereseau.colorgrid1.Selected :=(couleurReseau);
     fsaisiereseau.caption:=
     rsAjoutDUnMiro;
      fsaisiereseau.log1.Caption:=unitelongueur;
      fsaisiereseau.log2.Caption:=unitelongueur;
       fsaisiereseau.log3.Caption:=unitelongueur;
        fsaisiereseau.log4.Caption:=unitelongueur;
   if fsaisiereseau.showmodal=mrok then sauvevirtuel;
end;

      procedure TForm1.MenuPrismeClick(Sender: TObject);
      begin
         if modequelquechose then exit;
 if NombrePrisme=maxPrisme then begin
    application.MessageBox(pchar(rsNombreMaxima2), pchar(rsAttention3), mb_ok);
exit;
end;

saisiePrisme.combomateriaux.items:=listemateriaux;
saisiePrisme.combomateriaux.itemindex:=0;
saisiePrisme.editnrouge.text:=listenrouge.strings[0];
saisiePrisme.editnbleu.text:=listenbleu.strings[0];
saisiePrisme.editnvert.text:=listenvert.strings[0];
  with saisiePrisme
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisiePrisme.editnrouge.text:=floattostr(1.5);
  saisiePrisme.editnvert.text:=floattostr(1.6);
  saisiePrisme.editnbleu.text:=floattostr(1.7);
  saisiePrisme.log1.Caption:=unitelongueur;
     saisiePrisme.log2.Caption:=unitelongueur;
     saisiePrisme.log3.Caption:=unitelongueur;
      saisiePrisme.colorgrid1.selected:= (couleurPrisme);
     saisiePrisme.radiosortant.ItemIndex:=1;
          saisiePrisme.radioentrant.ItemIndex:=1;
          saisiePrisme.boutonsup.enabled:=false;
   if  saisiePrisme.showmodal=mrok then sauvevirtuel;
      end;

  procedure TForm1.FormShow(Sender: TObject);
  var f1:textfile; s1:string; _n1:extended;  si:integer;
   label 3204,3555,159;
  begin

  if encreation then begin {scalefont(self);} encreation:=false;

  form1.WindowState:=wsmaximized;
  form1.Refresh;
{$ifdef linux}
 form1.Top:=0;
  form1.Left:=0;
  form1.Width:=screen.Width;
  form1.Height:=screen.Height;
  form1.Refresh;
  {$endif}

  application.ProcessMessages;
  form1.ScrollBox1.Refresh;
  application.ProcessMessages;

  form1.Image1.Width:=form1.ScrollBox1.ClientWidth;
  form1.Image1.Height:=form1.ScrollBox1.ClientHeight;

 form1.Image1.Refresh;
  creation:=false;
     ConfigurationInitiale;
     {     application.MessageBox('','',mb_ok);
       application.ProcessMessages;
        form1.StatusBar1.Update;
       form1.Refresh;
       application.ProcessMessages;}

  largeurinitialeimage1:=form1.scrollbox1.clientwidth;
      hauteurinitialeimage1:=form1.scrollbox1.clientheight;

      rapportideal:=hauteurinitialeimage1/largeurinitialeimage1;

     if parametrelignecommande then begin
    // decimalseparator:=',';
     goto 159;
     end;

    with saisiechoix
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
edit_indice_vert_par_defaut.text:='1';
edit_indice_bleu_par_defaut.text:='1';
edit_indice_rouge_par_defaut.text:='1';
end;
  saisiechoix.showmodal;


  try
    indice_vert_par_defaut:=abs(strtofloat(saisiechoix.Edit_indice_vert_par_defaut.Text));
    except
    application.MessageBox(pchar(rsAttention), pchar(rsValeurIllGal), mb_ok);
    indice_vert_par_defaut:=1;
    end;
    try
    indice_bleu_par_defaut:=abs(strtofloat(saisiechoix.Edit_indice_bleu_par_defaut.Text));
    except
    application.MessageBox(pchar(rsAttention), pchar(rsValeurIllGal2), mb_ok);
    indice_bleu_par_defaut:=1;
    end;
  try
    indice_rouge_par_defaut:=abs(strtofloat(saisiechoix.Edit_indice_rouge_par_defaut.Text));
    except
    application.MessageBox(pchar(rsAttention), pchar(rsValeurIllGal3), mb_ok);
    indice_rouge_par_defaut:=1;
    end;








 159:form_palette.show;
 xxmin:=0;xxmax:=largeurinitialeimage1; yymin:=0;yymax:=hauteurinitialeimage1;
  BIBI.LimitesEtAxes(xxmin,xxmax,yymin,yymax,yymin,yymax,form1.image1.width,
form1.image1.height,'',policedefaut,true,clwhite,false,0,clblack,false,0,0,clblack,
policedefaut,false,false,{grillex,grilley,}1,clblack,clblack,'','','','','','',false,false,false);
BIBI.background(couleurfondsimulation);
if tracergrille then BIBI.dessinergrille(grillex,grilley);
listemateriaux:=tstringlist.Create;
listenrouge:=tstringlist.create;
listenbleu:=tstringlist.create;
listenvert:=tstringlist.create;
listemateriaux.add('...');
listenrouge.add(floattostr(1.5));
listenvert.add(floattostr(1.6));
listenbleu.add(floattostr(1.7));
if not fileexistsutf8(repertoire_executable+'listen.lst') then begin
application.messagebox(pchar(rsFichierDesIn),
pchar(rsAttention3), mb_ok);
goto 3555;
end;


  assignfile(f1,(repertoire_executable+'listen.lst'));
  reset(f1);
  while not(eof(f1)) do begin
  readln(f1,s1);
  listemateriaux.add(s1);
   readln(f1,_n1);
  listenbleu.add(floattostr(_n1));
  readln(f1,_n1);
  listenvert.add(floattostr(_n1));
  readln(f1,_n1);
  listenrouge.add(floattostr(_n1));
  end;
  closefile(f1);
  3555:
  if parametrelignecommande then begin
    sa_hauteur:=hauteurinitialeimage1;
   sa_largeur:=largeurinitialeimage1;
   sa_xxmin:=0; sa_yymin:=0; sa_yymax:=sa_hauteur;
   sa_xxmax:=sa_largeur;
   sa_respect:=true;
   goto 3204;
   end;

  case saisiechoix.radiochoix.itemindex of
  0: begin
  sa_hauteur:=hauteurinitialeimage1;
   sa_largeur:=largeurinitialeimage1;
   sa_xxmin:=0; sa_yymin:=0; sa_yymax:=sa_hauteur;
   sa_xxmax:=sa_largeur;
   sa_respect:=true;
   goto 3204;
  end;
  1: begin
  end;
  2: begin
  fichierouvert:=false;
    Form1.Ouvrirsimulation(Sender,repertoire_exemples);
  if fichierouvert then begin
    exit; end else begin
  sa_hauteur:=hauteurinitialeimage1;
   sa_largeur:=largeurinitialeimage1;
   sa_xxmin:=0; sa_yymin:=0; sa_yymax:=sa_hauteur;
   sa_xxmax:=sa_largeur;
   sa_respect:=true;
  goto 3204;
  end;
  end;
  end;



       with saisieespacetravail
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
     saisieespacetravail.editxxmax.Text:=floattostr(largeurinitialeimage1);
     saisieespacetravail.edityymax.Text:=floattostr(hauteurinitialeimage1);
   saisieespacetravail.statrapportideal.caption:=floattostr(rapportideal);
   sa_hauteur:=hauteurinitialeimage1;   sa_xxmin:=0; sa_yymin:=0;
   sa_largeur:=largeurinitialeimage1;   sa_xxmax:= largeurinitialeimage1;
   sa_yymax:= hauteurinitialeimage1;
   saisieespacetravail.statrapportactuel.caption:=floattostr(rapportideal);
      saisieespacetravail.showmodal;
  3204:    xxmin:=sa_xxmin; xxmax:=sa_xxmax;
  yymin:=sa_yymin;
yymax:=sa_yymax;  rapportactuel:=(yymax-yymin)/(xxmax-xxmin);
grillex:=(xxmax-xxmin)/20; grilley:=(yymax-yymin)/20;
grillex:=int(grillex*1.001/puissance(10,partieentiere(log10(grillex))))*
puissance(10,partieentiere(log10(grillex)));
grilley:=int(grilley*1.001/puissance(10,partieentiere(log10(grilley))))*
puissance(10,partieentiere(log10(grilley)));
respectrapport:=sa_respect;

 form1.retaille_image1;

form1.Caption:=Format(rsEspaceDeTrav, [floattostr(xxmin), floattostr(xxmax),
  floattostr(yymin), floattostr(yymax)]);

application.ProcessMessages;

  Rafraichit;

 if parametrelignecommande then  Form1.Ouvrirsimulation(Sender,repertoire_exemples);
   nombre_etats_sauvegardes:=0;  niveau_max_sauvegarde:=0;
  sauvevirtuel;

  end;
  end;

function tform1.taillemini:extended;
var taille1,taille2:extended;
begin
if ((form1.scrollbox1.ClientWidth=0) or (form1.scrollbox1.ClientHeight=0)) then begin
taillemini:=1;
exit;
end;
taille1:=2/form1.scrollbox1.clientwidth*(xxmax-xxmin);
taille2:=2/form1.scrollbox1.clientheight*(yymax-yymin);
if taille1<taille2 then taillemini:=taille1 else taillemini:=taille2;
taillemini:=taillemini/10;
  end;



procedure TForm1.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
var i:integer;
begin
if ((nombrequelquechose<>0) and  (modifie or  commentaire_change))
 then
 if application.MessageBox( pchar(rsLaSimulation),
 pchar(rsAttention4), MB_YESNO)=IDNO then begin
 canclose:=false;
 exit;
 end;

canclose:=true;

end;

procedure TForm1.FormKeyPress(Sender: TObject; var Key: char);
begin
  if key=chr(27) then begin
 form1.activeboutons;
 form_palette.boutongroupe.caption:=rsGrouper;
form_palette.boutongroupe.Hint:=rsGrouperDesLM;
 Rafraichit;
 form1.StatusBar1.Panels[1].text:='';
 form1.StatusBar1.Panels[2].text:='';
 form1.image1.Cursor:=crdefault;
 end;



end;

procedure TForm1.FormResize(Sender: TObject);
begin
 //if not(creation) then  form1.retaille_image1;
//if (not(fermeture) and not(creation)) then Rafraichit;

end;


procedure TForm1.boutonmiroirplanClick(Sender: TObject);
begin
if NombreMiroirPlan=maxmiroirplan then begin
    application.MessageBox(pchar(rsNombreMaxima3), pchar(rsAttention3), mb_ok);
exit;
end;
boutonmiroirplan.Enabled:=false;
 boutonmiroirplan.OnClick:=nil;
form1.image1.Cursor:=crPoint1;

form1.StatusBar1.Panels[2].text:=rsPositionnezL29;
ajoutmiroirplan:=true;
application.ProcessMessages;
                      boutonmiroirplan.OnClick:=@boutonmiroirplanclick;
desactiveboutons;
end;

procedure TForm1.boutonflecheClick(Sender: TObject);
begin
if Nombrefleche=maxfleche then begin
    application.MessageBox(pchar(rsNombreMaxima4), pchar(rsAttention3), mb_ok);
exit;
end;
boutonfleche.Enabled:=false;
 boutonfleche.OnClick:=nil;
form1.image1.Cursor:=crPoint1;

form1.StatusBar1.Panels[2].text:=rsPositionnezL30;
ajoutfleche:=true;
application.ProcessMessages;
                      boutonfleche.OnClick:=@boutonflecheclick;
desactiveboutons;
end;

procedure TForm1.boutonlsrClick(Sender: TObject);
begin
 if Nombrelsr=maxlsr then begin
    application.MessageBox(pchar(rsNombreMaxima5), pchar(rsAttention3), mb_ok);

exit;
end;
boutonlsr.Enabled:=false;
 boutonlsr.OnClick:=nil;
form1.image1.Cursor:=crPoint1;

form1.StatusBar1.Panels[2].text:=rsPositionnezL31;
ajoutlsr:=true;
application.ProcessMessages;
                      boutonlsr.OnClick:=@boutonlsrclick;
desactiveboutons;
end;





procedure TForm1.boutondiaClick(Sender: TObject);
begin
if Nombrediaphragme=maxdiaphragme then begin
    application.MessageBox(pchar(rsNombreMaxima6), pchar(rsAttention3), mb_ok);
exit;
end;
boutondia.Enabled:=false;
 boutondia.OnClick:=nil;
form1.image1.Cursor:=crcentre;

form1.StatusBar1.Panels[2].text:=rsPositionnezL32;
ajoutdiaphragme:=true;
application.ProcessMessages;
                      boutondia.OnClick:=@boutondiaclick;
desactiveboutons;
end;

procedure TForm1.boutonecranClick(Sender: TObject);
begin
 if Nombreecran=maxecran then begin
    application.MessageBox(pchar(rsNombreMaxima7), pchar(rsAttention3), mb_ok);
exit;
end;
boutonecran.Enabled:=false;
 boutonecran.OnClick:=nil;
form1.image1.Cursor:=crPoint1;

form1.StatusBar1.Panels[2].text:=rsPositionnezL33;
ajoutecran:=true;
application.ProcessMessages;
                      boutonecran.OnClick:=@boutonecranclick;
desactiveboutons;
end;
 procedure TForm1.ComboZoomChange(Sender: TObject);
begin
if modequelquechose then begin
    case zoomfactor of
    1:    combozoom.itemindex:=0;
    2:    combozoom.itemindex:=1;
    5:    combozoom.itemindex:=2;
    10:    combozoom.itemindex:=3;
    20:    combozoom.itemindex:=4;
    50:    combozoom.itemindex:=5;
    100:    combozoom.itemindex:=6;
    end;
exit;
end;
 if   combozoom.items[combozoom.itemindex]=rsX1 then begin
 zoomfactor:=1;
 end;

 if   combozoom.items[combozoom.itemindex]=rsX2 then begin
 zoomfactor:=2;
 end;

 if   combozoom.items[combozoom.itemindex]=rsX5 then begin
 zoomfactor:=5;
 end;

 if   combozoom.items[combozoom.itemindex]=rsX10 then begin
 zoomfactor:=10;
 end;

 if   combozoom.items[combozoom.itemindex]=rsX20 then begin
 zoomfactor:=20;
 end;

 if   combozoom.items[combozoom.itemindex]=rsX50 then begin
 zoomfactor:=50;
 end;

  if   combozoom.items[combozoom.itemindex]=rsX100 then begin
 zoomfactor:=100;
 end;

  form1.ScrollBox1.VertScrollBar.Position:=0;
  form1.ScrollBox1.HorzScrollBar.Position:=0;
{ if ((zoomfactor*form1.scrollbox1.clientheight) or (zoomfactor*form1.scrollbox1.clientwidth))
 >32766 then begin
  application.MessageBox('Zoom trop important !! !','Attention',mb_ok);
  zoomfactor:=1;
  combozoom.itemindex:=0;
  end;}
  form1.retaille_image1;

 Rafraichit;
 end;


 procedure TForm1.Copier1Click(Sender: TObject);
begin
if not(graphepresent) then exit;
clipboard.assign(form1.image1.picture);
end;



procedure TForm1.Imprimer1Click(Sender: TObject);
  label 3012,628;
var iiii:integer;
 i,j,iii,nn1,nn2:integer;
 tt1,tt2,rra:extended;
   bord_b,bord_h,bord_g,bord_d:integer;
  hPrinter   : TMyimprimante;
  rap:extended;
dede:string;
begin
if not(graphepresent) then exit;
 abandonimpression:=true;
case  graphehorizontal of
tgauche: configimpression.radiogroupehorizontal.itemindex:=1;
tdroite: configimpression.radiogroupehorizontal.itemindex:=0;
tcentreh: configimpression.radiogroupehorizontal.itemindex:=2;
end;
case graphevertical of
thaut: configimpression.radiogroupevertical.itemindex:=0;
tbas: configimpression.radiogroupevertical.itemindex:=1;
tcentrev: configimpression.radiogroupevertical.itemindex:=2;
end;
configimpression.spinpourcentagehorizontal.value:=taillegraphehorizontal;
configimpression.spinpourcentagevertical.value:=taillegraphevertical;
 configimpression.spinnombrecopies.value:=1;
 with  configimpression do
begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
showmodal;
end;

if abandonimpression then exit;


       printer.orientation:=orientation_impression;

   if not(printDialog1.Execute) then begin
        exit;
   end;


   imprimante:=printer;


 if not(respect_imprimante) then begin
 case graphehorizontal of
 tgauche: begin
 bord_g:=0;
 bord_d:=trunc(imprimante.pagewidth-imprimante.pagewidth/100*taillegraphehorizontal);
 end;
 tdroite: begin
 bord_d:=0;
 bord_g:=trunc(imprimante.pagewidth-imprimante.pagewidth/100*taillegraphehorizontal);
 end;
 tcentreh: begin
 bord_g:=trunc((imprimante.pagewidth-imprimante.pagewidth/100*taillegraphehorizontal)/2);
 bord_d:=bord_g;
 end;
 end;

 case graphevertical of
 thaut: begin
 bord_h:=0;
 bord_b:=trunc(imprimante.pageheight-imprimante.pageheight/100*taillegraphevertical);
 end;
 tbas: begin
 bord_b:=0;
 bord_h:=trunc(imprimante.pageheight-imprimante.pageheight/100*taillegraphevertical);
 end;
 tcentrev: begin
 bord_b:=trunc((imprimante.pageheight-imprimante.pageheight/100*taillegraphevertical)/2);
 bord_h:=bord_b;
 end;
 end;

 end
 else
 begin
 rap:=(yymax-yymin)/(xxmax-xxmin);
 if rap<taillegraphevertical/taillegraphehorizontal*imprimante.pageheight/imprimante.pagewidth then
 begin
  case graphehorizontal of
 tgauche: begin
 bord_g:=0;
 bord_d:=trunc(imprimante.pagewidth-imprimante.pagewidth/100*taillegraphehorizontal);
 end;
 tdroite: begin
 bord_d:=0;
 bord_g:=trunc(imprimante.pagewidth-imprimante.pagewidth/100*taillegraphehorizontal);
 end;
 tcentreh: begin
 bord_g:=trunc((imprimante.pagewidth-imprimante.pagewidth/100*taillegraphehorizontal)/2);
 bord_d:=bord_g;
 end;
 end;

 case graphevertical of
 thaut: begin
 bord_h:=0;
 bord_b:=trunc(imprimante.pageheight-imprimante.pagewidth/100*taillegraphevertical);
 end;
 tbas: begin
 bord_b:=0;
 bord_h:=trunc(imprimante.pageheight-rap*imprimante.pagewidth/100*taillegraphevertical);
 end;
 tcentrev: begin
 bord_b:=trunc((imprimante.pageheight-rap*imprimante.pagewidth/100*taillegraphevertical)/2);
 bord_h:=bord_b;
 end;
 end;


 end else
 begin
  case graphehorizontal of
 tgauche: begin
 bord_g:=0;
 bord_d:=trunc(imprimante.pagewidth-imprimante.pageheight/100/rap*taillegraphehorizontal);
 end;
 tdroite: begin
 bord_d:=0;
 bord_g:=trunc(imprimante.pagewidth-imprimante.pageheight/100/rap*taillegraphehorizontal);
 end;
 tcentreh: begin
 bord_g:=trunc((imprimante.pagewidth-imprimante.pageheight/100/rap*taillegraphehorizontal)/2);
 bord_d:=bord_g;
 end;
 end;

 case graphevertical of
 thaut: begin
 bord_h:=0;
 bord_b:=trunc(imprimante.pageheight-imprimante.pageheight/100*taillegraphevertical);
 end;
 tbas: begin
 bord_b:=0;
 bord_h:=trunc(imprimante.pageheight-imprimante.pageheight/100*taillegraphevertical);
 end;
 tcentrev: begin
 bord_b:=trunc((imprimante.pageheight-imprimante.pageheight/100*taillegraphevertical)/2);
 bord_h:=bord_b;
 end;
 end;
 end;
 end;
 for iiii:=1 to nombrecopies do begin
imprimante.begindoc;



 optionsurfacesonde:=false;
retaillepsegment(listesegment,nombretotalsegment,0);
retaillepsegment(listenormale,nombrenormale,0);

   if nombreanglesatracer>0 then begin
 retaillepangle(listeanglesatracer,nombreanglesatracer,0);
 nombreanglesatracer:=0;
 end;
  nombretotalsegment:=0; nombreenfant:=0;   nombrenormale:=0;
  creeprayon(listerayonenfant,0);
  if nombrerayon>0 then for i:=1 to nombrerayon do
  if not( (listerayon[-1+i].couleur=clblack) and (unit222.unmilieudispersif or (nombrereseau>0))) then calculesegment0(listerayon[-1+i].ax,
  listerayon[-1+i].ay,listerayon[-1+i].kx,listerayon[-1+i].ky,listerayon[-1+i].anglepolaire,listerayon[-1+i].epaisseur,
  listerayon[-1+i].couleur,listerayon[-1+i].vav,
  listerayon[-1+i].vaa,listerayon[-1+i].nombremaxenfant,listerayon[-1+i].maxsegment) else
   begin
   retailleprayon(listerayonenfant,nombreenfant,nombreenfant+1);
   inc(nombreenfant);
  listerayonenfant[-1+nombreenfant].createenfant(listerayon[-1+i].ax,listerayon[-1+i].ay
  ,listerayon[-1+i].kx,listerayon[-1+i].ky,clred,listerayon[-1+i].epaisseur,
    listerayon[-1+i].vav,listerayon[-1+i].vaa,listerayon[-1+i].nombremaxenfant,listerayon[-1+i].maxsegment,0);
   retailleprayon(listerayonenfant,nombreenfant,nombreenfant+1);
   inc(nombreenfant);
  listerayonenfant[-1+nombreenfant].createenfant(listerayon[-1+i].ax,listerayon[-1+i].ay
  ,listerayon[-1+i].kx,listerayon[-1+i].ky,clgreen,listerayon[-1+i].epaisseur,
    listerayon[-1+i].vav,listerayon[-1+i].vaa,listerayon[-1+i].nombremaxenfant,listerayon[-1+i].maxsegment,0);
    retailleprayon(listerayonenfant,nombreenfant,nombreenfant+1);
   inc(nombreenfant);
  listerayonenfant[-1+nombreenfant].createenfant(listerayon[-1+i].ax,listerayon[-1+i].ay
  ,listerayon[-1+i].kx,listerayon[-1+i].ky,clblue,listerayon[-1+i].epaisseur,
    listerayon[-1+i].vav,listerayon[-1+i].vaa,listerayon[-1+i].nombremaxenfant,listerayon[-1+i].maxsegment,0);
  end;

  if nombresourceponctuelle>0 then for i:=1 to nombresourceponctuelle do
  listesourceponctuelle[-1+i].calculesegment;
  if nombreondeplane>0 then for i:=1 to nombreondeplane do listeondeplane[-1+i].calculesegment;
  if nombreenfant=0 then goto 3012;
  i:=0;
    628:   inc(i);
  calculesegment0(listerayonenfant[-1+i].ax,
  listerayonenfant[-1+i].ay,listerayonenfant[-1+i].kx,listerayonenfant[-1+i].ky,listerayonenfant[-1+i].anglepolaire,
  listerayonenfant[-1+i].epaisseur,
  listerayonenfant[-1+i].couleur,listerayonenfant[-1+i].vav,
  listerayonenfant[-1+i].vaa,listerayonenfant[-1+i].nombremaxenfant,listerayonenfant[-1+i].maxsegment);
  if i<nombreenfant then goto 628;
 3012: freeprayon(listerayonenfant,nombreenfant);
   nombreenfant:=0;   nn1:=nombretotalsegment;


 hprinter.LimitesEtAxes(xxmin,xxmax,yymin,yymax,yymin,yymax,imprimante.pagewidth,
imprimante.pageHeight,'',policedefaut,true,couleurfondsimulation,false,0,clblack,false,0,0,clblack,
policedefaut,false,false,{grillex,grilley,}1,clblack,clblack,'','','','','','',false,false,false,
bord_g,bord_d,bord_b,bord_h);
hprinter.background(couleurfondsimulation);
if tracergrille then hprinter.dessinergrille(grillex,grilley);
if nombregroupe>0 then for i:=1 to nombregroupe do listegroupe[-1+i].recalculeiso;
if nombreprisme>0 then for i:=1 to nombreprisme do
listeprisme[-1+i].imprime(hprinter);
if nombrepolycercle>0 then for i:=1 to nombrepolycercle do
listepolycercle[-1+i].imprime(hprinter);
if nombrepolyhedre>0 then for i:=1 to nombrepolyhedre do
listepolyhedre[-1+i].imprime(hprinter);
 if nombresphere>0 then for i:=1 to nombresphere do
listesphere[-1+i].imprime(hprinter);
if nombrelec>0 then for i:=1 to nombrelec do
listelec[-1+i].imprime(hprinter);
if nombremiroirplan>0 then for i:=1 to nombremiroirplan do
ListeMiroirPlan[-1+i].imprime(hprinter);
 if nombreReseau>0 then for i:=1 to nombreReseau do
 ListeReseau[-1+i].imprime(hprinter);
if nombrefleche>0 then for i:=1 to nombrefleche do
ListeFleche[-1+i].imprime(hprinter);
if nombremiroirconique>0 then for i:=1 to nombremiroirconique do
ListeMiroirconique[-1+i].imprime(hprinter);
if nombrerayon>0 then for i:=1 to nombrerayon do
listerayon[-1+i].imprime(hprinter);
if nombresourceponctuelle>0 then for i:=1 to nombresourceponctuelle do
listesourceponctuelle[-1+i].imprime(hprinter);
if nombreondeplane>0 then for i:=1 to nombreondeplane do
listeondeplane[-1+i].imprime(hprinter);
if nombremscopa>0 then for i:=1 to nombremscopa do
listemscopa[-1+i].imprime(hprinter);
if nombremscore>0 then for i:=1 to nombremscore do
listemscore[-1+i].imprime(hprinter);
if nombremscere>0 then for i:=1 to nombremscere do
listemscere[-1+i].imprime(hprinter);
if nombremscepa>0 then for i:=1 to nombremscepa do
listemscepa[-1+i].imprime(hprinter);
if nombrelmc>0 then for i:=1 to nombrelmc do
listelmc[-1+i].imprime(hprinter);
if nombrelmd>0 then for i:=1 to nombrelmd do
listelmd[-1+i].imprime(hprinter);
if nombreecran>0 then for i:=1 to nombreecran do
listeecran[-1+i].imprime(hprinter);
if nombreoeil>0 then for i:=1 to nombreoeil do
listeoeil[-1+i].imprime(hprinter);
if nombrediaphragme>0 then for i:=1 to nombrediaphragme do
listediaphragme[-1+i].imprime(hprinter);
if nombrelsr>0 then for i:=1 to nombrelsr do
listelsr[-1+i].imprime(hprinter);
if nombremetre>0 then for i:=1 to nombremetre do
listemetre[-1+i].imprime(hprinter);
if nombreangle>0 then for i:=1 to nombreangle do
listeangle[-1+i].imprime(hprinter);
if nombretexte>0 then for i:=1 to nombretexte do
listetexteaffiche[-1+i].imprime(hprinter);
if nombreanglesatracer>0 then for i:=1 to nombreanglesatracer do
listeanglesatracer[-1+i].imprimebis(hprinter);
 if nombregroupe>0 then for i:=1 to nombregroupe do
 listegroupe[-1+i].imprime(hprinter);
  if (nombrelmc+nombrelmd+nombremscopa+nombremscepa=0) then
 for i:=1 to nombresourceponctuelle do
 if listesourceponctuelle[-1+i].tracersurfacesonde then
 if not(listesourceponctuelle[-1+i].sourcevirtuelle) then
 if  listesourceponctuelle[-1+i].nombrecheminsoptiques>0 then
 begin
   tt1:=listesourceponctuelle[-1+i].ray[1].anglepolaire;
        tt2:=listesourceponctuelle[-1+i].ray[listesourceponctuelle[-1+i].nombrederayon].anglepolaire;
       if tt2<tt1 then tt2:=tt2+2*Pi;
  if tt2<tt1 then tt2:=tt2+2*Pi;
       rra:=sqrt(sqr(listesourceponctuelle[-1+i].a1x-listesourceponctuelle[-1+i].sx)
       +sqr(listesourceponctuelle[-1+i].a1y-listesourceponctuelle[-1+i].sy));
  if (sqrt(sqr(listesourceponctuelle[-1+i].a2x-listesourceponctuelle[-1+i].sx)
  +sqr(listesourceponctuelle[-1+i].a2y-listesourceponctuelle[-1+i].sy))<rra) then
  rra:=sqrt(sqr(listesourceponctuelle[-1+i].a2x-listesourceponctuelle[-1+i].sx)
  +sqr(listesourceponctuelle[-1+i].a2y-listesourceponctuelle[-1+i].sy));

 for j:=1 to   listesourceponctuelle[-1+i].nombrecheminsoptiques do
 begin

        optionsurfacesonde:=true;
         premierpointsurfacesonde:=true;
        cheminactuelaobtenir:=listesourceponctuelle[-1+i].listecheminsoptiques[j];

       for iii:=1 to maxrayonsrecherchesurfacesonde do  begin
       calculesegment0(listesourceponctuelle[-1+i].sx,listesourceponctuelle[-1+i].sy,
    rra*cos(tt1+(iii-1)*(tt2-tt1)/(maxrayonsrecherchesurfacesonde-1)),
  rra*sin(tt1+(iii-1)*(tt2-tt1)/(maxrayonsrecherchesurfacesonde-1)),tt1+(iii-1)*(tt2-tt1)/
  (maxrayonsrecherchesurfacesonde-1)
  ,1,listesourceponctuelle[-1+i].couleur,'','',0,listesourceponctuelle[-1+i].maxsegment);
     hprinter.trait(listesegment[-1+nombretotalsegment].a1x,
     listesegment[-1+nombretotalsegment].a1y,
     listesegment[-1+nombretotalsegment].a2x,
 listesegment[-1+nombretotalsegment].a2y,1,
 listesegment[-1+nombretotalsegment].couleur,psdot,pmcopy,true);
                end;
 end;  end;

    if (nombrelmc+nombrelmd+nombremscopa+nombremscepa=0) then
 for i:=1 to nombreondeplane do
 if listeondeplane[-1+i].tracersurfacesonde then
 if  listeondeplane[-1+i].nombrecheminsoptiques>0 then
  begin
 for j:=1 to   listeondeplane[-1+i].nombrecheminsoptiques do
 begin

        optionsurfacesonde:=true;
         premierpointsurfacesonde:=true;
        cheminactuelaobtenir:=listeondeplane[-1+i].listecheminsoptiques[j];

       for iii:=1 to maxrayonsrecherchesurfacesonde do  begin
       calculesegment0(listeondeplane[-1+i].a1x+
       (iii-1)*(listeondeplane[-1+i].a2x-listeondeplane[-1+i].a1x)/
       (maxrayonsrecherchesurfacesonde-1),
       listeondeplane[-1+i].a1y+
       (iii-1)*(listeondeplane[-1+i].a2y-listeondeplane[-1+i].a1y)/
       (maxrayonsrecherchesurfacesonde-1),
    listeondeplane[-1+i].ray[1].kx,
  listeondeplane[-1+i].ray[1].ky,
  listeondeplane[-1+i].ray[1].anglepolaire
  ,1,listeondeplane[-1+i].couleur,'','',0,listeondeplane[-1+i].maxsegment);
     hprinter.trait(listesegment[-1+nombretotalsegment].a1x,
     listesegment[-1+nombretotalsegment].a1y,
     listesegment[-1+nombretotalsegment].a2x,
 listesegment[-1+nombretotalsegment].a2y,1,
 listesegment[-1+nombretotalsegment].couleur,psdot,pmcopy,true);
                end;
 end;  end;
  nn2:=nombretotalsegment;
  nombretotalsegment:=nn1;
 ImprimeTousLesSegments(hprinter);

printer.EndDoc;


 end;

end;




 procedure TForm1.Miroirplan2Click(Sender: TObject);
begin
if modequelquechose then exit;
if NombreMiroirPlan=maxmiroirplan then begin
    application.MessageBox(pchar(rsNombreMaxima3), pchar(rsAttention3), mb_ok);
exit;
end;

with saisiemiroirplan
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
 saisiemiroirplan.boutonsup.enabled:=false;
    saisiemiroirplan.editx1.text:='';
    saisiemiroirplan.editx2.text:='';
    saisiemiroirplan.edity1.text:='';
    saisiemiroirplan.edity2.text:='';
    saisiemiroirplan.cochehachures.checked:=true;
    saisiemiroirplan.editepaisseur.Value:=epaisseurmiroirplan;

saisiemiroirplan.colorgrid1.Selected :=(couleurmiroirplan);
    saisiemiroirplan.caption:=
    rsAjoutDUnMiro;
     saisiemiroirplan.log1.Caption:=unitelongueur;
     saisiemiroirplan.log2.Caption:=unitelongueur;
      saisiemiroirplan.log3.Caption:=unitelongueur;
       saisiemiroirplan.log4.Caption:=unitelongueur;
  if saisiemiroirplan.showmodal=mrok then sauvevirtuel;

  end;



 

procedure TForm1.Dupliquerunlment1Click(Sender: TObject);
begin
form_palette.boutondupliquerClick(Sender);
end;






procedure TForm1.boutondistanceClick(Sender: TObject);
begin
boutondistance.OnClick:=nil;
form1.image1.Cursor:=crdefault;
modedistance:=true;  point1fait:=false;
application.ProcessMessages;
boutondistance.OnClick:=@boutondistanceclick;
form1.image1.Cursor:=crpoint1;
StatusBar1.Panels[2].text:=rsCliquerSurLO;
desactiveboutons;
Rafraichit;
end;


procedure TForm1.boutonangleClick(Sender: TObject);
begin
boutonangle.OnClick:=nil;
form1.image1.Cursor:=crdefault;
modeangle:=true;  point1fait:=false;
application.ProcessMessages;
boutonangle.OnClick:=@boutonangleclick;
form1.image1.Cursor:=crcentre;
StatusBar1.Panels[2].text:=rsCliquerSurLe;
desactiveboutons;
Rafraichit;
end;




procedure TForm1.boutonrayonClick(Sender: TObject);
begin
if Nombrerayon=maxrayon then begin
    application.MessageBox(pchar(rsNombreMaxima8), pchar(rsAttention3), mb_ok);
exit;
end;
boutonrayon.Enabled:=false;
 boutonrayon.OnClick:=nil;
form1.image1.Cursor:=crPoint1;
form1.StatusBar1.Panels[2].text:=rsPositionnezL34;
ajoutrayon:=true;
application.ProcessMessages;
boutonrayon.OnClick:=@boutonrayonclick;
desactiveboutons;
end;


procedure TForm1.boutonsphereClick(Sender: TObject);
begin
if Nombresphere=maxsphere then begin
    application.MessageBox(pchar(rsNombreMaxima9), pchar(rsAttention3), mb_ok);
exit;
end;
boutonsphere.Enabled:=false;
 boutonsphere.OnClick:=nil;
form1.image1.Cursor:=crcentre;
form1.StatusBar1.Panels[2].text:=rsPositionnezL35;
ajoutsphere:=true;
application.ProcessMessages;
boutonsphere.OnClick:=@boutonsphereclick;
desactiveboutons;
end;



procedure TForm1.boutonprismeClick(Sender: TObject);
      begin
 if Nombreprisme=maxprisme then begin
    application.MessageBox(pchar(rsNombreMaxima2), pchar(rsAttention3), mb_ok);
exit;
end;
boutonprisme.Enabled:=false;
 boutonprisme.OnClick:=nil;
form1.image1.Cursor:=crcentreG;
form1.StatusBar1.Panels[2].text:=rsPositionnezL36;
ajoutprisme:=true;
application.ProcessMessages;
boutonprisme.OnClick:=@boutonprismeclick;
desactiveboutons;
      end;

procedure TForm1.cocheanglesChange(Sender: TObject);
begin

end;

procedure TForm1.FormDeactivate(Sender: TObject);
begin

end;

procedure TForm1.MenuItem10Click(Sender: TObject);
begin
   form1.superposesimulation(sender,repertoire_simul_perso);
end;

procedure TForm1.MenuItem11Click(Sender: TObject);
begin
   form1.superposesimulation(sender,repertoire_exemples);
end;

procedure TForm1.MenuItem12Click(Sender: TObject);
begin
  form1.superposesimulation(sender,repertoire_actuel);
end;

procedure TForm1.MenuItem14Click(Sender: TObject);
var
 v: THTMLBrowserHelpViewer;
 BrowserPath, BrowserParams: string;
 p: LongInt;
 URL: String;
 BrowserProcess: TProcessUTF8;
begin
 v:=THTMLBrowserHelpViewer.Create(nil);
 try
   v.FindDefaultBrowser(BrowserPath,BrowserParams);
   //debugln(['Path=',BrowserPath,' Params=',BrowserParams]);

   url:='file:///'+repertoire_executable+'historique.html';
   p:=System.Pos('%s', BrowserParams);
   System.Delete(BrowserParams,p,2);
   System.Insert(URL,BrowserParams,p);

   // start browser
   BrowserProcess:=TProcessUTF8.Create(nil);
   try
     BrowserProcess.CommandLine:=BrowserPath+' '+BrowserParams;
     BrowserProcess.Execute;
   finally
     BrowserProcess.Free;
   end;
 finally
   v.Free;
 end;

end;

procedure TForm1.boutonpolyClick(Sender: TObject);
begin
if Nombrepolyhedre=maxpolyhedre then begin
    application.MessageBox(pchar(rsNombreMaxima10), pchar(rsAttention3), mb_ok);
exit;
end;
boutonpoly.Enabled:=false;
 boutonpoly.OnClick:=nil;
form1.image1.Cursor:=crPoint1;
form1.StatusBar1.Panels[2].text:=rsPositionnezL37;
ajoutpoly:=true;
application.ProcessMessages;
boutonpoly.OnClick:=@boutonpolyclick;
desactiveboutons;

with saisienombresommet
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
groupbox1.Caption:=rsSommetsDeCeP;
spinedit1.MinValue:=3;
end;
 if saisienombresommet.showmodal=mrcancel then begin
 form1.activeboutons;
  form1.image1.Cursor:=crdefault;
                     form1.StatusBar1.Panels[2].text:='';
 exit;
 end;
 nbactsommet:=0;
end;




procedure TForm1.boutonmscopaClick(Sender: TObject);
begin
if NombreMscopa=maxMscopa then begin
    application.MessageBox(pchar(rsNombreMaxima11), pchar(rsAttention3), mb_ok);
exit;
end;
boutonMscopa.Enabled:=false;
 boutonMscopa.OnClick:=nil;
form1.image1.Cursor:=crPoint1;
form1.StatusBar1.Panels[2].text:=rsPositionnezL38;
ajoutMscopa:=true;
application.ProcessMessages;
                      boutonMscopa.OnClick:=@boutonMscopaclick;
desactiveboutons;
end;

procedure TForm1.boutonmscoreClick(Sender: TObject);
begin
 if Nombremscore=maxmscore then begin
    application.MessageBox(pchar(rsNombreMaxima12), pchar(rsAttention3), mb_ok);
exit;
end;
boutonmscore.Enabled:=false;
 boutonmscore.OnClick:=nil;
form1.image1.Cursor:=crPoint1;
form1.StatusBar1.Panels[2].text:=rsPositionnezL39;
ajoutmscore:=true;
application.ProcessMessages;
                      boutonmscore.OnClick:=@boutonmscoreclick;
desactiveboutons;
end;

 procedure TForm1.boutonmscereClick(Sender: TObject);
begin
 if Nombremscere=maxmscere then begin
    application.MessageBox(pchar(rsNombreMaxima13), pchar(rsAttention3), mb_ok);
exit;
end;
boutonmscere.Enabled:=false;
 boutonmscere.OnClick:=nil;
form1.image1.Cursor:=crPoint1;
form1.StatusBar1.Panels[2].text:=rsPositionnezL40;
ajoutmscere:=true;
application.ProcessMessages;
                      boutonmscere.OnClick:=@boutonmscereclick;
desactiveboutons;
end;



procedure TForm1.boutonmscepaClick(Sender: TObject);
begin
if Nombremscepa=maxmscepa then begin
    application.MessageBox(pchar(rsNombreMaxima14), pchar(rsAttention3), mb_ok);
exit;
end;
boutonmscepa.Enabled:=false;
 boutonmscepa.OnClick:=nil;
form1.image1.Cursor:=crPoint1;
form1.StatusBar1.Panels[2].text:=rsPositionnezL41;
ajoutmscepa:=true;
application.ProcessMessages;
                      boutonmscepa.OnClick:=@boutonmscepaclick;
desactiveboutons;
end;



procedure TForm1.boutonlmcClick(Sender: TObject);
begin
if Nombrelmc=maxlmc then begin
    application.MessageBox(pchar(rsNombreMaxima15), pchar(rsAttention3), mb_ok);
exit;
end;
boutonlmc.Enabled:=false;
 boutonlmc.OnClick:=nil;
form1.image1.Cursor:=crPoint1;
form1.StatusBar1.Panels[2].text:=rsPositionnezL42;
ajoutlmc:=true;
application.ProcessMessages;
                      boutonlmc.OnClick:=@boutonlmcclick;
desactiveboutons;
end;



procedure TForm1.boutonlmdClick(Sender: TObject);
begin
 if Nombrelmd=maxlmd then begin
    application.MessageBox(pchar(rsNombreMaxima16), pchar(rsAttention3), mb_ok);
exit;
end;
boutonlmd.Enabled:=false;
 boutonlmd.OnClick:=nil;
form1.image1.Cursor:=crPoint1;
form1.StatusBar1.Panels[2].text:=rsPositionnezL43;
ajoutlmd:=true;
application.ProcessMessages;
                      boutonlmd.OnClick:=@boutonlmdclick;
desactiveboutons;
end;

procedure TForm1.boutonspvClick(Sender: TObject);
begin
 if Nombresourceponctuelle=maxsourceponctuelle then begin
    application.MessageBox(pchar(rsNombreMaxima17), pchar(rsAttention3), mb_ok);
exit;
end;
boutonspv.Enabled:=false;
 boutonspv.OnClick:=nil;
form1.image1.Cursor:=crSource;
form1.StatusBar1.Panels[2].text:=rsPositionnezL44;
ajoutsourceponctuelle:=true;  genrevirtuel:=true;
application.ProcessMessages;
                      boutonspv.OnClick:=@boutonspvclick;
desactiveboutons;
end;

procedure TForm1.boutonsourceponctuelleClick(Sender: TObject);
begin
 if Nombresourceponctuelle=maxsourceponctuelle then begin
    application.MessageBox(pchar(rsNombreMaxima17), pchar(rsAttention3), mb_ok);
exit;
end;
boutonsourceponctuelle.Enabled:=false;
 boutonsourceponctuelle.OnClick:=nil;
form1.image1.Cursor:=crSource;
form1.StatusBar1.Panels[2].text:=rsPositionnezL45;
ajoutsourceponctuelle:=true;   genrevirtuel:=false;
application.ProcessMessages;
                      boutonsourceponctuelle.OnClick:=@boutonsourceponctuelleclick;
desactiveboutons;
end;

procedure TForm1.boutonondeplaneClick(Sender: TObject);
begin
 if Nombreondeplane=maxondeplane then begin
    application.MessageBox(pchar(rsNombreMaxima18), pchar(rsAttention3), mb_ok);
exit;
end;
boutonondeplane.Enabled:=false;
 boutonondeplane.OnClick:=nil;
form1.image1.Cursor:=crPoint1;
form1.StatusBar1.Panels[2].text:=rsPositionnezL46;
ajoutondeplane:=true;
application.ProcessMessages;
                      boutonondeplane.OnClick:=@boutonondeplaneclick;
desactiveboutons;
end;

procedure TForm1.RayonUnique1Click(Sender: TObject);
begin
if modequelquechose then exit;
if Nombrerayon=maxrayon then begin
    application.MessageBox(pchar(rsNombreMaxima8), pchar(rsAttention3), mb_ok);
exit;
end;

  with saisierayon
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
  saisierayon.boutonsup.enabled:=false;
    saisierayon.Spinenfantmax.value:=10;
    saisierayon.editx1.text:='';
    saisierayon.editx2.text:='';
    saisierayon.edity1.text:='';
    saisierayon.edity2.text:='';
     saisierayon.editvav.text:='';
    saisierayon.editvaa.text:='';
    saisierayon.editepaisseur.Value:=epaisseurrayon;
     saisierayon.radiocouleur.ItemIndex:=0;
    saisierayon.caption:=
    rsAjoutDUnRayo;
    saisierayon.log1.Caption:=unitelongueur;
     saisierayon.log2.Caption:=unitelongueur;
      saisierayon.log3.Caption:=unitelongueur;
      saisierayon.log4.Caption:=unitelongueur;
if saisierayon.showmodal=mrok then sauvevirtuel;
   end;


 procedure TForm1.Miroirsphriqueconvergent1Click(Sender: TObject);
begin
  if modequelquechose then exit;
if Nombremscopa=maxmscopa then begin
    application.MessageBox(pchar(rsNombreMaxima19), pchar(rsAttention3), mb_ok);
exit;
end;

  with saisiemiroirscopa
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
 saisiemiroirscopa.checktrou.checked:=false;
    saisiemiroirscopa.editdiametretrou.enabled:=false;
   saisiemiroirscopa.editdiametretrou.text:='0';
    saisiemiroirscopa.boutonsup.enabled:=false;
    saisiemiroirscopa.editx1.text:='';
    saisiemiroirscopa.editx2.text:='';
    saisiemiroirscopa.edity1.text:='';
    saisiemiroirscopa.edity2.text:='';
    saisiemiroirscopa.editfocale.text:='';
    saisiemiroirscopa.cochehachures.checked:=true;
    saisiemiroirscopa.cocheaxe.checked:=true;
    saisiemiroirscopa.editepaisseur.Value:=epaisseurmscopa;

saisiemiroirscopa.colorgrid1.selected:=couleurmscopa ;


saisiemiroirscopa.gridaxe.selected:=(couleuraxemscopa) ;
    saisiemiroirscopa.caption:=
    rsAjoutDUnMiro2;
    saisiemiroirscopa.log1.Caption:=unitelongueur;
     saisiemiroirscopa.log2.Caption:=unitelongueur;
      saisiemiroirscopa.log3.Caption:=unitelongueur;
       saisiemiroirscopa.log4.Caption:=unitelongueur;
        saisiemiroirscopa.log5.Caption:=unitelongueur;
       saisiemiroirscopa.log6.Caption:=unitelongueur;
 if saisiemiroirscopa.showmodal=mrok then sauvevirtuel;
end;

procedure TForm1.Miroirsphriqueconvexe1Click(Sender: TObject);
begin
 if modequelquechose then exit;
if Nombremscepa=maxmscepa then begin
    application.MessageBox(pchar(rsNombreMaxima3), pchar(rsAttention3), mb_ok);
exit;
end;

   with saisiemiroirscepa
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
    saisiemiroirscepa.checktrou.checked:=false;
    saisiemiroirscepa.editdiametretrou.enabled:=false;
   saisiemiroirscepa.editdiametretrou.text:='0';
    saisiemiroirscepa.boutonsup.enabled:=false;
    saisiemiroirscepa.editx1.text:='';
    saisiemiroirscepa.editx2.text:='';
    saisiemiroirscepa.edity1.text:='';
    saisiemiroirscepa.edity2.text:='';
    saisiemiroirscepa.editfocale.text:='';
    saisiemiroirscepa.cochehachures.checked:=true;
    saisiemiroirscepa.cocheaxe.checked:=true;
    saisiemiroirscepa.editepaisseur.Value:=epaisseurmscepa;

saisiemiroirscepa.colorgrid1.selected:=(couleurmscepa) ;


saisiemiroirscepa.gridaxe.selected:=(couleuraxemscepa) ;
    saisiemiroirscepa.caption:=
    rsAjoutDUnMiro3;
    saisiemiroirscepa.log1.Caption:=unitelongueur;
     saisiemiroirscepa.log2.Caption:=unitelongueur;
      saisiemiroirscepa.log3.Caption:=unitelongueur;
       saisiemiroirscepa.log4.Caption:=unitelongueur;
        saisiemiroirscepa.log5.Caption:=unitelongueur;
       saisiemiroirscepa.log6.Caption:=unitelongueur;

if saisiemiroirscepa.showmodal=mrok then sauvevirtuel;
end;


 procedure TForm1.Lentilleminceconvergente1Click(Sender: TObject);
begin
  if modequelquechose then exit;
if Nombrelmc=maxlmc then begin
    application.MessageBox(pchar(rsNombreMaxima20), pchar(rsAttention3), mb_ok);
exit;
end;

   with saisielentillemc
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
  saisielentillemc.boutonsup.enabled:=false;
    saisielentillemc.editx1.text:='';
    saisielentillemc.editx2.text:='';
    saisielentillemc.edity1.text:='';
    saisielentillemc.edity2.text:='';
    saisielentillemc.editfocale.text:='';
    saisielentillemc.cocheaxe.checked:=true;
    saisielentillemc.editepaisseur.Value:=epaisseurlmc;

saisielentillemc.colorgrid1.selected:=(couleurlmc) ;


saisielentillemc.gridaxe.selected:=(couleuraxelmc) ;
    saisielentillemc.caption:=
    rsAjoutDUneLen;
     saisielentillemc.log1.Caption:=unitelongueur;
     saisielentillemc.log2.Caption:=unitelongueur;
      saisielentillemc.log3.Caption:=unitelongueur;
       saisielentillemc.log4.Caption:=unitelongueur;
        saisielentillemc.log5.Caption:=unitelongueur;
if saisielentillemc.showmodal=mrok then sauvevirtuel;
end;

 procedure TForm1.Lentillemincedivergente1Click(Sender: TObject);
begin
  if modequelquechose then exit;
if Nombrelmd=maxlmd then begin
    application.MessageBox(pchar(rsNombreMaxima20), pchar(rsAttention3), mb_ok);
exit;
end;

   with saisielentillemd
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
  saisielentillemd.boutonsup.enabled:=false;
    saisielentillemd.editx1.text:='';
    saisielentillemd.editx2.text:='';
    saisielentillemd.edity1.text:='';
    saisielentillemd.edity2.text:='';
    saisielentillemd.editfocale.text:='';
    saisielentillemd.cocheaxe.checked:=true;
    saisielentillemd.editepaisseur.Value:=epaisseurlmc;

saisielentillemd.colorgrid1.selected:=(couleurlmc );


saisielentillemd.gridaxe.selected:=(couleuraxelmc) ;
    saisielentillemd.caption:=
    rsAjoutDUneLen2;
     saisielentillemd.log1.Caption:=unitelongueur;
     saisielentillemd.log2.Caption:=unitelongueur;
      saisielentillemd.log3.Caption:=unitelongueur;
       saisielentillemd.log4.Caption:=unitelongueur;
        saisielentillemd.log5.Caption:=unitelongueur;
if saisielentillemd.showmodal=mrok then sauvevirtuel;
end;

procedure TForm1.Lamesemirflchissante1Click(Sender: TObject);
begin
  if modequelquechose then exit;
if Nombrelsr=maxlsr then begin
    application.MessageBox(pchar(rsNombreMaxima21), pchar(rsAttention3), mb_ok);
exit;
end;

 with saisielsr
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
   saisielsr.boutonsup.enabled:=false;
    saisielsr.editx1.text:='';
    saisielsr.editx2.text:='';
    saisielsr.edity1.text:='';
    saisielsr.edity2.text:='';
    saisielsr.editepaisseur.Value:=epaisseurlsr;

saisielsr.colorgrid1.selected:=(couleurlsr) ;
    saisielsr.caption:=
    rsAjoutDUneLam;
    saisielsr.log1.Caption:=unitelongueur;
     saisielsr.log2.Caption:=unitelongueur;
      saisielsr.log3.Caption:=unitelongueur;
       saisielsr.log4.Caption:=unitelongueur;
if saisielsr.showmodal=mrok then sauvevirtuel;
end;


 procedure TForm1.Miroirsphriqueconcave1Click(Sender: TObject);
begin
 if modequelquechose then exit;
if Nombremscore=maxmscore then begin
    application.MessageBox(pchar(rsNombreMaxima22), pchar(rsAttention3), mb_ok);
exit;
end;

  with saisiemiroirscore
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
    saisiemiroirscore.boutonsup.enabled:=false;
    saisiemiroirscore.checkobtu.Checked:=false;
    saisiemiroirscore.editx1.text:='';
    saisiemiroirscore.editx2.text:='';
    saisiemiroirscore.edity1.text:='';
    saisiemiroirscore.edity2.text:='';
    saisiemiroirscore.editrayoncourbure.text:='';
    saisiemiroirscore.cochehachures.checked:=true;
    saisiemiroirscore.cocheaxe.checked:=true;
    saisiemiroirscore.editepaisseur.Value:=epaisseurmscore;

saisiemiroirscore.colorgrid1.selected:=(couleurmscore) ;


saisiemiroirscore.gridaxe.selected:=(couleuraxemscore) ;
    saisiemiroirscore.caption:=
    rsAjoutDUnMiro4;
     saisiemiroirscore.log1.Caption:=unitelongueur;
     saisiemiroirscore.log2.Caption:=unitelongueur;
      saisiemiroirscore.log3.Caption:=unitelongueur;
       saisiemiroirscore.log4.Caption:=unitelongueur;
        saisiemiroirscore.log5.Caption:=unitelongueur;

    saisiemiroirscore.cochetrou.checked:=false;
   saisiemiroirscore.editdiametretrou.text:='0';

if saisiemiroirscore.showmodal=mrok then sauvevirtuel;
end;

procedure TForm1.Lentille1Click(Sender: TObject);

 begin
 if modequelquechose then exit;
if Nombrelec=maxlec then begin
    application.MessageBox(pchar(rsNombreMaxima20),pchar( rsAttention3), mb_ok);
exit;
end;

  saisielentille.combomateriaux.items:=listemateriaux;
saisielentille.combomateriaux.itemindex:=0;
saisielentille.editnrouge.text:=listenrouge.strings[0];
saisielentille.editnbleu.text:=listenbleu.strings[0];
saisielentille.editnvert.text:=listenvert.strings[0];
  with saisielentille
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;

    saisielentille.boutonsup.enabled:=false;
    saisielentille.edits1x.text:='';
    saisielentille.edits2x.text:='';
    saisielentille.edits1y.text:='';
    saisielentille.edits2y.text:='';
    saisielentille.editr1.text:='';
     saisielentille.editr2.text:='';
     saisielentille.editdiametre.text:='';

    saisielentille.caption:=
    rsAjoutDUneLen3;

saisielentille.colorgrid1.selected:=(couleurlec);
    saisielentille.log1.Caption:=unitelongueur;
     saisielentille.log2.Caption:=unitelongueur;
      saisielentille.log3.Caption:=unitelongueur;
       saisielentille.log4.Caption:=unitelongueur;
        saisielentille.log5.Caption:=unitelongueur;
         saisielentille.log6.Caption:=unitelongueur;
          saisielentille.log7.Caption:=unitelongueur;
if saisielentille.showmodal=mrok then sauvevirtuel;
end;


procedure TForm1.Miroirsphriqueconvexe2Click(Sender: TObject);
begin
 if modequelquechose then exit;
if Nombremscere=maxmscere then begin
    application.MessageBox(pchar(rsNombreMaxima22), pchar(rsAttention3), mb_ok);
exit;
end;

    with saisiemiroirscere
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
    saisiemiroirscere.boutonsup.enabled:=false;
    saisiemiroirscere.checkobtu.Checked:=false;
    saisiemiroirscere.editx1.text:='';
    saisiemiroirscere.editx2.text:='';
    saisiemiroirscere.edity1.text:='';
    saisiemiroirscere.edity2.text:='';
    saisiemiroirscere.editrayoncourbure.text:='';
    saisiemiroirscere.cochehachures.checked:=true;
    saisiemiroirscere.cocheaxe.checked:=true;
    saisiemiroirscere.editepaisseur.Value:=epaisseurmscere;

saisiemiroirscere.colorgrid1.selected:=(couleurmscere) ;


saisiemiroirscere.gridaxe.selected:=(couleuraxemscere) ;
    saisiemiroirscere.caption:=
    rsAjoutDUnMiro5;
     saisiemiroirscere.log1.Caption:=unitelongueur;
     saisiemiroirscere.log2.Caption:=unitelongueur;
      saisiemiroirscere.log3.Caption:=unitelongueur;
       saisiemiroirscere.log4.Caption:=unitelongueur;
        saisiemiroirscere.log5.Caption:=unitelongueur;

    saisiemiroirscere.cochetrou.checked:=false;
   saisiemiroirscere.editdiametretrou.text:='0';
if saisiemiroirscere.showmodal=mrok then sauvevirtuel;
end;

procedure TForm1.boutonreseauClick(Sender: TObject);
begin

if Nombrereseau=maxreseau then begin
    application.MessageBox(pchar(rsNombreMaxima36), pchar(rsAttention3), mb_ok);
exit;
end;
boutonreseau.Enabled:=false;
 boutonreseau.OnClick:=nil;
form1.image1.Cursor:=crPoint1;

form1.StatusBar1.Panels[2].text:=rsPositionnezL53;
ajoutreseau:=true;
application.ProcessMessages;
                      boutonreseau.OnClick:=@boutonreseauclick;
desactiveboutons;
end;







procedure TForm1.Miroircnique1Click(Sender: TObject);
begin
 if modequelquechose then exit;
if Nombremiroirconique=maxmiroirconique then begin
    application.MessageBox(pchar(rsNombreMaxima23), pchar(rsAttention3), mb_ok);
exit;
end;

    with saisiemiroirconique
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
  saisiemiroirconique.boutonsup.enabled:=false;
    saisiemiroirconique.editfx.text:='';
    saisiemiroirconique.editfy.text:='';
    saisiemiroirconique.edite.text:='';
    saisiemiroirconique.editp.text:='';
    saisiemiroirconique.editt0.text:='';
    saisiemiroirconique.editt1.text:='';
    saisiemiroirconique.editt2.text:='';
    saisiemiroirconique.checkhachures.checked:=true;
    saisiemiroirconique.checkaxefocal.checked:=true;
    saisiemiroirconique.checkconcave.checked:=true;

saisiemiroirconique.colorgrid1.selected:=(couleurmiroirconique);


saisiemiroirconique.gridaxe.selected:=(couleuraxemiroirconique) ;
saisiemiroirconique.caption:=
    rsAjoutDUnMiro6;
   saisiemiroirconique.log1.Caption:=unitelongueur;
     saisiemiroirconique.log2.Caption:=unitelongueur;
      saisiemiroirconique.log3.Caption:=unitelongueur;
if saisiemiroirconique.showmodal=mrok then sauvevirtuel;
end;



procedure TForm1.Ecran1Click(Sender: TObject);
begin
if modequelquechose then exit;
if Nombreecran=maxecran then begin
    application.MessageBox(pchar(rsNombreMaxima24), pchar(rsAttention3), mb_ok);
exit;
end;

     with saisieecran
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
    saisieecran.boutonsup.enabled:=false;
    saisieecran.editx1.text:='';
    saisieecran.editx2.text:='';
    saisieecran.edity1.text:='';
    saisieecran.edity2.text:='';
    saisieecran.editepaisseur.Value:=epaisseurecran;

saisieecran.colorgrid1.selected:=(couleurecran) ;
    saisieecran.caption:=
    rsAjoutDUnCran;
     saisieecran.log1.Caption:=unitelongueur;
     saisieecran.log2.Caption:=unitelongueur;
      saisieecran.log3.Caption:=unitelongueur;
      saisieecran.log4.Caption:=unitelongueur;
if saisieecran.showmodal=mrok then sauvevirtuel;
end;

procedure TForm1.Diaphragme1Click(Sender: TObject);
begin
 if modequelquechose then exit;
if Nombrediaphragme=maxdiaphragme then begin
    application.MessageBox(pchar(rsNombreMaxima25), pchar(rsAttention3), mb_ok);
exit;
end;

    with saisiediaphragme
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
 saisiediaphragme.boutonsup.enabled:=false;
    saisiediaphragme.editcx.text:='';
    saisiediaphragme.editcy.text:='';
    saisiediaphragme.editt.text:='';
    saisiediaphragme.editrint.text:='';
    saisiediaphragme.editrext.text:='';
    saisiediaphragme.spinedit1.Value:=epaisseurdiaphragme;
saisiediaphragme.colorgrid1.selected:=(couleurdiaphragme) ;
    saisiediaphragme.caption:=
    rsAjoutDUnDiap;
     saisiediaphragme.log1.Caption:=unitelongueur;
     saisiediaphragme.log2.Caption:=unitelongueur;
      saisiediaphragme.log3.Caption:=unitelongueur;
      saisiediaphragme.log4.Caption:=unitelongueur;
if saisiediaphragme.showmodal=mrok then sauvevirtuel;
end;

procedure TForm1.Sphrerfractante1Click(Sender: TObject);
begin
  if modequelquechose then exit;
 if Nombresphere=maxsphere then begin
    application.MessageBox(pchar(rsNombreMaxima9), pchar(rsAttention3), mb_ok);
exit;
end;

saisiesph.combomateriaux.items:=listemateriaux;
saisiesph.combomateriaux.itemindex:=0;
saisiesph.editnrouge.text:=listenrouge.strings[0];
saisiesph.editnbleu.text:=listenbleu.strings[0];
saisiesph.editnvert.text:=listenvert.strings[0];
  with saisiesph
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisiesph.editnrouge.text:=floattostr(1.5);
  saisiesph.editnvert.text:=floattostr(1.6);
  saisiesph.editnbleu.text:=floattostr(1.7);
  saisiesph.log1.Caption:=unitelongueur;
     saisiesph.log2.Caption:=unitelongueur;
     saisiesph.log3.Caption:=unitelongueur;
      saisiesph.colorgrid1.selected:= (couleursphere);
     saisiesph.radiosortant.ItemIndex:=1;
          saisiesph.radioentrant.ItemIndex:=1;
          saisiesph.boutonsup.enabled:=false;
   if  saisiesph.showmodal=mrok then sauvevirtuel;

end;



procedure TForm1.Polysphrerfractante1Click(Sender: TObject);
var i:integer;
begin
 if modequelquechose then exit;
  if Nombrepolycercle=maxpolycercle then begin
    application.MessageBox(pchar(rsNombreMaxima26), pchar(rsAttention3), mb_ok);
exit;
end;

 saisienombresommet.groupbox1.caption:=rsSommetsDeCet;
   with saisienombresommet
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
spinedit1.MinValue:=2;
end;
 saisienombresommet.showmodal;

for i:=1 to maxsommet do begin
ttpoly[i].ax:=0;
ttpoly[i].ay:=0;
youri[i]:=false;
helena[i]:=true;
selene[i]:=0;
  end;

saisiepolycercle.combomateriaux.items:=listemateriaux;
saisiepolycercle.combomateriaux.itemindex:=0;
saisiepolycercle.editnrouge.text:=listenrouge.strings[0];
saisiepolycercle.editnbleu.text:=listenbleu.strings[0];
saisiepolycercle.editnvert.text:=listenvert.strings[0];
  with saisiepolycercle
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;

saisiepolycercle.log1.Caption:=unitelongueur;
     saisiepolycercle.log2.Caption:=unitelongueur;
     saisiepolycercle.log3.Caption:=unitelongueur;
saisiepolycercle.editnrouge.text:=floattostr(1.5);
  saisiepolycercle.editnvert.text:=floattostr(1.6);
  saisiepolycercle.editnbleu.text:=floattostr(1.7);
    saisiepolycercle.ComboBox1.Items.Clear;
    for i:=1 to nbpoly do
    saisiepolycercle.ComboBox1.Items.Add(Format(rsSommet, [inttostr(i)]));
saisiepolycercle.ComboBox1.Itemindex:=0;
     saisiepolycercle.editx.text:=floattostr(ttpoly[1].ax);
     saisiepolycercle.edity.text:=floattostr(ttpoly[1].ay);
     saisiepolycercle.editrayon.text:=floattostr(selene[1]);
if youri[1] then saisiepolycercle.radiorectiligne.ItemIndex:=0 else
 saisiepolycercle.radiorectiligne.ItemIndex:=1;
 if helena[1] then saisiepolycercle.radiorentrant.ItemIndex:=0 else
 saisiepolycercle.radiorectiligne.ItemIndex:=1;
 if tavarich[1] then  saisiepolycercle.radiogroupaigu.ItemIndex:=0 else
 saisiepolycercle.radiogroupaigu.ItemIndex:=1;
 saisiepolycercle.colorgrid1.selected:=(couleurpolycercle);
     saisiepolycercle.radiosortant.ItemIndex:=1;
          saisiepolycercle.radioentrant.ItemIndex:=1;
          saisiepolycercle.boutonsup.enabled:=false;
  if   saisiepolycercle.showmodal=mrok then sauvevirtuel;

end;





procedure TForm1.Polyhdrerfractant1Click(Sender: TObject);
var i:integer;
begin
 if modequelquechose then exit;
  if Nombrepolyhedre=maxpolyhedre then begin
    application.MessageBox(pchar(rsNombreMaxima27), pchar(rsAttention3), mb_ok);
exit;
end;

    with saisienombresommet
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
spinedit1.MinValue:=3;
groupbox1.Caption:=rsSommetsDeCeP;
end;
 saisienombresommet.showmodal;

for i:=1 to maxsommet do begin
ttpoly[i].ax:=0;
ttpoly[i].ay:=0;  end;

saisiepolyhedre.combomateriaux.items:=listemateriaux;
saisiepolyhedre.combomateriaux.itemindex:=0;
saisiepolyhedre.editnrouge.text:=listenrouge.strings[0];
saisiepolyhedre.editnbleu.text:=listenbleu.strings[0];
saisiepolyhedre.editnvert.text:=listenvert.strings[0];
  with saisiepolyhedre
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
    saisiepolyhedre.boutonsup.enabled:=false;
    saisiepolyhedre.ComboBox1.Items.Clear;
    for i:=1 to nbpoly do
    saisiepolyhedre.ComboBox1.Items.Add(Format(rsSommet, [inttostr(i)]));
saisiepolyhedre.ComboBox1.Itemindex:=0;
     saisiepolyhedre.editx.text:=floattostr(ttpoly[1].ax);
     saisiepolyhedre.edity.text:=floattostr(ttpoly[1].ay);
     saisiepolyhedre.log1.Caption:=unitelongueur;
     saisiepolyhedre.log2.Caption:=unitelongueur;
     saisiepolyhedre.colorgrid1.selected:=(couleurpolyhedre);
     saisiepolyhedre.radiosortant.ItemIndex:=1;
          saisiepolyhedre.radioentrant.ItemIndex:=1;
  if   saisiepolyhedre.showmodal=mrok then sauvevirtuel;
end;

procedure TForm1.Sourceponctuelle1Click(Sender: TObject);
begin
 if modequelquechose then exit;
if Nombresourceponctuelle=maxsourceponctuelle then begin
    application.MessageBox(pchar(rsNombreMaxima28), pchar(rsAttention3), mb_ok);
exit;
end;

    with saisiespp
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
   saisiespp.spinmaxenfant.value:=10;
    saisiespp.boutonsup.enabled:=false;
    saisiespp.spinnombrerayons.Value:=5;
      saisiespp.editlistechemins.text:='';
    saisiespp.cochesurfacesonde.checked:=false;
    saisiespp.editx1.text:='';
    saisiespp.editx2.text:='';
     saisiespp.edity1.text:='';
    saisiespp.edity2.text:='';
     saisiespp.editsx.text:='';
    saisiespp.editsy.text:='';
    saisiespp.editvaa.text:='';
    saisiespp.editvav.text:='';
    saisiespp.editepaisseur.Value:=epaisseursourceponctuelle;
         saisiespp.radiovirtuelle.ItemIndex:=0;
    saisiespp.radiocouleur.ItemIndex:=0;
    saisiespp.caption:=
    rsAjoutDUneSou;
    saisiespp.log1.Caption:=unitelongueur;
     saisiespp.log2.Caption:=unitelongueur;
      saisiespp.log3.Caption:=unitelongueur;
      saisiespp.log4.Caption:=unitelongueur;
     saisiespp.log5.Caption:=unitelongueur;
      saisiespp.log6.Caption:=unitelongueur;
 if saisiespp.showmodal=mrok then sauvevirtuel;
end;

procedure TForm1.Ondeplane1Click(Sender: TObject);
begin
 if modequelquechose then exit;
if Nombreondeplane=maxondeplane then begin
    application.MessageBox(pchar(rsNombreMaxima29), pchar(rsAttention3), mb_ok);
exit;
end;

     with saisieopp
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
   saisieopp.spinmaxenfant.value:=10;
    saisieopp.boutonsup.enabled:=false;
    saisieopp.spinnombrerayons.Value:=5;
    saisieopp.editx1.text:='';
    saisieopp.editx2.text:='';
     saisieopp.edity1.text:='';
    saisieopp.edity2.text:='';
     saisieopp.editsx.text:='';
    saisieopp.editsy.text:='';
     saisieopp.editvaa.text:='';
    saisieopp.editvav.text:='';
    saisieopp.editepaisseur.Value:=epaisseurondeplane;
    saisieopp.cochesurfaces.Checked:=false;
    saisieopp.editlistechemins.text:='';
  saisieopp.radiocouleur.ItemIndex:=0;
    saisieopp.caption:=
    rsAjoutDUneCet;
    saisieopp.log1.Caption:=unitelongueur;
     saisieopp.log2.Caption:=unitelongueur;
      saisieopp.log3.Caption:=unitelongueur;
      saisieopp.log4.Caption:=unitelongueur;
     saisieopp.log5.Caption:=unitelongueur;
      saisieopp.log6.Caption:=unitelongueur;
if saisieopp.showmodal=mrok then sauvevirtuel;
end;


procedure tform1.metazero;

begin
if modequelquechose then exit;
couleurfondsimulation:=clwhite;
indice_vert_par_defaut:=1;
 indice_bleu_par_defaut:=1;
  indice_rouge_par_defaut:=1;
positionempennage:=500;
 tailleempennage:=5;
FreePMiroirPlan(ListeMiroirPlan,nombremiroirplan); FreePreseau(Listereseau,nombrereseau);  freepfleche(listefleche,nombrefleche);
FreePecran(Listeecran,nombreecran);   freepdiaphragme(listediaphragme,nombrediaphragme);
freeplsr(listelsr,nombrelsr); freeppolyhedre(listepolyhedre,nombrepolyhedre);   freepprisme(listeprisme,nombreprisme);
freeppolycercle(listepolycercle,nombrepolycercle);    freepoeil(listeoeil,nombreoeil);
 freePMscopa(ListeMscopa,nombreMscopa); freepgroupe(listegroupe,nombregroupe);
 freePMscepa(ListeMscepa,nombreMscepa);
 freePMscere(ListeMscere,nombreMscere);
 freepMscore(ListeMscore,nombreMscore);
 freepLmc(ListeLmc,nombreLmc);  freeplec(listelec,nombrelec);
 freepmiroirconique(listemiroirconique,nombremiroirconique);
 freepLmd(ListeLmd,nombreLmd);  freeptexteaffiche(listetexteaffiche,nombretexte);
 freepRayon(ListeRayon,nombreRayon);
 freepSourcePonctuelle(ListeSourcePonctuelle,nombreSourcePonctuelle);
 freepOndePlane(ListeOndePlane,nombreOndePlane);
  freepsphere(listesphere,nombresphere);  freepmetre(listemetre,nombremetre);
  freepangle(listeangle,nombreangle);
freepangle(listeanglesatracer,nombreanglesatracer);
 creePMiroirPlan(listemiroirplan,0);   creePreseau(listereseau,0);creepfleche(listefleche,0);
 creePEcran(listeecran,0);  CreePpolyhedre(listepolyhedre,0);   CreePprisme(listeprisme,0);
 creeppolycercle(listepolycercle,0); CreePOeil(listeoeil,0);
 creeplsr(listelsr,0);  creepsphere(listesphere,0);
  creePMscopa(ListeMscopa,0);  creepmetre(listemetre,0);  creepangle(listeangle,0);
  creePMscepa(ListeMscepa,0);  creepmiroirconique(listemiroirconique,0);
  creePMscore(ListeMscore,0);  creeptexteaffiche(listetexteaffiche,0);
  creePMscere(ListeMscere,0);  creepdiaphragme(listediaphragme,0);
  creePlmc(Listelmc,0);  creeplec(listelec,0);
  creePlmd(Listelmd,0);  creepangle(listeanglesatracer,0);
  creePrayon(Listerayon,0);   creepgroupe(listegroupe,0);
  creePSourcePonctuelle(ListeSourcePonctuelle,0);
  creePOndePlane(ListeOndePlane,0);
   maxrayonsrecherchesurfacesonde:=20;
  nomsauvegarde:='';    unitelongueur:=rsMm;
   fantomespresents:=false;     modifie:=false;
  formediteur.memo1.Lines.clear;
   commentaire_change:=false;
   formediteur.WindowState:=wsminimized;
   formediteur.Caption:=rsCommentaires3;
 nombrerayon:=0; nombremiroirplan:=0;  nombrereseau:=0; nombreecran:=0;  nombremiroirconique:=0;
     nombreMscopa:=0;  nombreMscepa:=0; nombrelmc:=0; nombrelec:=0;
     nombrelmd:=0;    nombrefleche:=0;    nombreoeil:=0;  nombregroupe:=0;
     nombreMscore:=0;  nombreMscere:=0;  nombresourceponctuelle:=0;
     nombreondeplane:=0;    nombrelsr:=0;   nombrepolyhedre:=0;  nombrepolycercle:=0;
     nombresphere:=0;   nombremetre:=0;   nombretexte:=0;  nombrediaphragme:=0;
       nombreprisme:=0;                       nombreangle:=0;  decimalesangles:=0;
    nom_fichier_image_fond_simulation:='';
    fond_simulation_est_image:=false;

      sa_hauteur:=hauteurinitialeimage1;
   sa_largeur:=largeurinitialeimage1;
   sa_respect:=true;
    xxmin:=0; xxmax:=abs(sa_largeur); yymin:=0;
yymax:=abs(sa_hauteur);  rapportactuel:=(yymax-yymin)/(xxmax-xxmin);
tracergrille:=false; attractiongrille:=false; optionnormale:=true;
optionangles:=false;
N7.Checked:=optionnormale; cochegrille.Checked:=tracergrille;
cocheattraction.checked:=attractiongrille;
cochenormale.checked:=optionnormale; afficherlagrille1.Checked:=tracergrille;
Afficherlesangles1.checked:=optionangles;
cocheangles.checked:=optionangles;
grillex:=xxmax/20; grilley:=yymax/20;
grillex:=int(grillex*1.001/puissance(10,partieentiere(log10(grillex))))*
puissance(10,partieentiere(log10(grillex)));
grilley:=int(grilley*1.001/puissance(10,partieentiere(log10(grilley))))*
puissance(10,partieentiere(log10(grilley)));
respectrapport:=sa_respect;



form1.retaille_image1;
form1.Caption:=Format(rsEspaceDeTrav2, [floattostr(xxmin), floattostr(xxmax),
  floattostr(yymin), floattostr(yymax)]);

  
     Rafraichit;
end;

procedure TForm1.Nouveau1Click(Sender: TObject);
label 3205;
begin
if modequelquechose then exit;
if ((nombrequelquechose<>0) and  (modifie or  commentaire_change))
 then
 if application.MessageBox( pchar(rsLaSimulation2),
 pchar(rsAttention4), MB_YESNO)=IDNO then  exit;
 nombre_etats_sauvegardes:=0;  niveau_max_sauvegarde:=0;
 rtablir1.enabled:=false;  refaire1.Enabled:=false;
FreePMiroirPlan(ListeMiroirPlan,nombremiroirplan); FreePreseau(Listereseau,nombrereseau); freepfleche(listefleche,nombrefleche);
FreePecran(Listeecran,nombreecran);   freepdiaphragme(listediaphragme,nombrediaphragme);
freeplsr(listelsr,nombrelsr); freeppolyhedre(listepolyhedre,nombrepolyhedre);
freepprisme(listeprisme,nombreprisme);
freeppolycercle(listepolycercle,nombrepolycercle);    freepoeil(listeoeil,nombreoeil);
 freePMscopa(ListeMscopa,nombreMscopa); freepgroupe(listegroupe,nombregroupe);
 freePMscepa(ListeMscepa,nombreMscepa);
 freePMscere(ListeMscere,nombreMscere);
 freepMscore(ListeMscore,nombreMscore);
 freepLmc(ListeLmc,nombreLmc);  freeplec(listelec,nombrelec);
 freepmiroirconique(listemiroirconique,nombremiroirconique);
 freepLmd(ListeLmd,nombreLmd);  freeptexteaffiche(listetexteaffiche,nombretexte);
 freepRayon(ListeRayon,nombreRayon);
 freepSourcePonctuelle(ListeSourcePonctuelle,nombreSourcePonctuelle);
 freepOndePlane(ListeOndePlane,nombreOndePlane);
  freepsphere(listesphere,nombresphere);  freepmetre(listemetre,nombremetre);
  freepangle(listeangle,nombreangle);
freepangle(listeanglesatracer,nombreanglesatracer);
 creePMiroirPlan(listemiroirplan,0); creePreseau(listereseau,0); creepfleche(listefleche,0);
 creePEcran(listeecran,0);  CreePpolyhedre(listepolyhedre,0); CreePprisme(listeprisme,0);
 creeppolycercle(listepolycercle,0); CreePOeil(listeoeil,0);
 creeplsr(listelsr,0);  creepsphere(listesphere,0);
  creePMscopa(ListeMscopa,0);  creepmetre(listemetre,0);  creepangle(listeangle,0);
  creePMscepa(ListeMscepa,0);  creepmiroirconique(listemiroirconique,0);
  creePMscore(ListeMscore,0);  creeptexteaffiche(listetexteaffiche,0);
  creePMscere(ListeMscere,0);  creepdiaphragme(listediaphragme,0);
  creePlmc(Listelmc,0);  creeplec(listelec,0);
  creePlmd(Listelmd,0);  creepangle(listeanglesatracer,0);
  creePrayon(Listerayon,0);   creepgroupe(listegroupe,0);
  creePSourcePonctuelle(ListeSourcePonctuelle,0);
  creePOndePlane(ListeOndePlane,0);
   maxrayonsrecherchesurfacesonde:=20;
  nomsauvegarde:='';    unitelongueur:=rsMm;
   fantomespresents:=false;     modifie:=false;
   formediteur.memo1.Lines.clear;
   commentaire_change:=false;
   formediteur.WindowState:=wsminimized;
   formediteur.Caption:=rsCommentaires3;
 nombrerayon:=0; nombremiroirplan:=0;  nombrereseau:=0; nombreecran:=0;  nombremiroirconique:=0;
     nombreMscopa:=0;  nombreMscepa:=0; nombrelmc:=0; nombrelec:=0;
     nombrelmd:=0;    nombrefleche:=0;    nombreoeil:=0;  nombregroupe:=0;
     nombreMscore:=0;  nombreMscere:=0;  nombresourceponctuelle:=0;
     nombreondeplane:=0;    nombrelsr:=0;   nombrepolyhedre:=0;  nombrepolycercle:=0;
     nombresphere:=0;   nombremetre:=0;   nombretexte:=0;  nombrediaphragme:=0;
                             nombreangle:=0;  decimalesangles:=0; nombreprisme:=0;
                             form1.WindowState:=wsmaximized;
    nom_fichier_image_fond_simulation:='';
    fond_simulation_est_image:=false;

        with saisiechoix
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
edit_indice_vert_par_defaut.text:='1';
edit_indice_bleu_par_defaut.text:='1';
edit_indice_rouge_par_defaut.text:='1';
end;
      try
      saisiechoix.radiochoix.Items.Delete(2);
      except
      end;

      saisiechoix.showmodal;
      try
    indice_vert_par_defaut:=abs(strtofloat(saisiechoix.Edit_indice_vert_par_defaut.Text));
    except
    application.MessageBox(pchar(rsAttention), pchar(rsValeurIllGal), mb_ok);
    indice_vert_par_defaut:=1;
    end;
    try
    indice_bleu_par_defaut:=abs(strtofloat(saisiechoix.Edit_indice_bleu_par_defaut.Text));
    except
    application.MessageBox(pchar(rsAttention), pchar(rsValeurIllGal2), mb_ok);
    indice_bleu_par_defaut:=1;
    end;
  try
    indice_rouge_par_defaut:=abs(strtofloat(saisiechoix.Edit_indice_rouge_par_defaut.Text));
    except
    application.MessageBox(pchar(rsAttention), pchar(rsValeurIllGal3), mb_ok);
    indice_rouge_par_defaut:=1;
    end;
      case saisiechoix.radiochoix.ItemIndex of
      0: begin
      sa_hauteur:=hauteurinitialeimage1;
   sa_largeur:=largeurinitialeimage1;
   sa_xxmin:=0; sa_yymin:=0; sa_xxmax:=sa_largeur;  sa_yymax:=sa_hauteur;
   sa_respect:=true;
   goto 3205;
      end;
      1: begin
      end;
      end;



      with saisieespacetravail
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
     saisieespacetravail.editxxmax.Text:=floattostr(largeurinitialeimage1);
     saisieespacetravail.edityymax.Text:=floattostr(hauteurinitialeimage1);
   saisieespacetravail.statrapportideal.caption:=floattostr(rapportideal);
   sa_hauteur:=hauteurinitialeimage1;
   sa_largeur:=largeurinitialeimage1;
   saisieespacetravail.statrapportactuel.caption:=floattostr(rapportideal);
      saisieespacetravail.showmodal;
 3205:     xxmin:=sa_xxmin; xxmax:=sa_xxmax; yymin:=sa_yymin;
yymax:=sa_yymax;  rapportactuel:=(yymax-yymin)/(xxmax-xxmin);
tracergrille:=false; attractiongrille:=false; optionnormale:=true;
optionangles:=false;
N7.Checked:=optionnormale; cochegrille.Checked:=tracergrille;
cocheattraction.checked:=attractiongrille;
cochenormale.checked:=optionnormale; afficherlagrille1.Checked:=tracergrille;
Afficherlesangles1.checked:=optionangles;
cocheangles.checked:=optionangles;
grillex:=(xxmax-xxmin)/20; grilley:=(yymax-yymin)/20;
grillex:=int(grillex*1.001/puissance(10,partieentiere(log10(grillex))))*
puissance(10,partieentiere(log10(grillex)));
grilley:=int(grilley*1.001/puissance(10,partieentiere(log10(grilley))))*
puissance(10,partieentiere(log10(grilley)));
respectrapport:=sa_respect;
 form1.retaille_image1;
form1.Caption:=Format(rsEspaceDeTrav, [floattostr(xxmin), floattostr(xxmax),
  floattostr(yymin), floattostr(yymax)]);

application.ProcessMessages;
 form1.image1.refresh;
 application.ProcessMessages;

 nombre_etats_sauvegardes:=0;  niveau_max_sauvegarde:=0;
  sauvevirtuel;
     Rafraichit;
end;





procedure enregistrer(nomfichier:string);
 var f:textfile; i,j:integer;
  begin
 AssignFile(F, (nomfichier));
  rewrite(f);
  writeln(f,'NE PAS MODIFIER CE FICHIER');
 writeln(f,'VERSION');
 writeln(f,versionprogramme);
 { so on modifie le format des fichiers, il faut modifier le numero de version
 du programme; lors de la lecture
 des fichiuers, il faut adapter la lecture au numero de version, de facon que les
 anciens fichiers puissent etre relus avec les nouvelles versions de programme}

 writeln(f,xxmin,' ',xxmax,' ',yymin,' ',yymax);
 writeln(f,respectrapport);
 writeln(f,'UNITE');
 writeln(f,unitelongueur);
 writeln(f,'OPTIONS GRILLE');
 writeln(f,tracergrille);
 writeln(f,attractiongrille);
 writeln(f,grillex,grilley);
 writeln(f,'OPTIONS NORMALES');
 writeln(f,optionnormale);
 writeln(f,optionangles);
 writeln(f,pourcentagenormale);
 writeln(f,couleurnormale);
 writeln(f,epaisseurnormale);
 writeln(f,decimalesangles);
 writeln(f,'OPTIONS SEGMENTS FANTOMES');
 writeln(f,fantomespresents);
 writeln(f,listefantomes);
 writeln(f,'NOMBRE POINTS TRACE SURFACE ONDE');
 writeln(f,maxrayonsrecherchesurfacesonde);
  writeln(f,'INDICES PAR DEFAUT');
    writeln(f,indice_vert_par_defaut);
    writeln(f,indice_bleu_par_defaut);
    writeln(f,indice_rouge_par_defaut);
    writeln(f,'LAMBDA');
    writeln(f,longueur_donde_red);
     writeln(f,longueur_donde_blue);
     writeln(f,longueur_donde_green);


 writeln(f,'Nombre de miroirs plans:');

  writeln(f,nombremiroirplan);
  if nombremiroirplan>0 then
  for i:=1 to nombremiroirplan do begin
  writeln(f,'Numero '+inttostr(i));
  writeln(f,listemiroirplan[-1+i].a1x,' ',listemiroirplan[-1+i].a1y,' ',listemiroirplan[-1+i].a2x,' ',
  listemiroirplan[-1+i].a2y,' ',listemiroirplan[-1+i].epaisseur,' ',listemiroirplan[-1+i].couleur);
  writeln(f,listemiroirplan[-1+i].hachures);
  end;


  writeln(f,'Nombre de reseaux:');

  writeln(f,nombreReseau);
  if nombreReseau>0 then
  for i:=1 to nombreReseau do begin
  writeln(f,'Numero '+inttostr(i));
  writeln(f,listeReseau[-1+i].a1x,listeReseau[-1+i].a1y,listeReseau[-1+i].a2x,
  listeReseau[-1+i].a2y);
  writeln(f,listeReseau[-1+i].epaisseur);
  writeln(f,listeReseau[-1+i].couleur);
  writeln(f,listeReseau[-1+i].pas);
   writeln(f,listeReseau[-1+i].entransmission);
  writeln(f,listeReseau[-1+i].hachures);
  writeln(f,listeReseau[-1+i].ordre_min);
  writeln(f,listeReseau[-1+i].ordre_max);
  end;



  writeln(f,'Nombre de miroirs concaves paraxiaux:');
  writeln(f,nombremscopa);
  if nombremscopa>0 then
  for i:=1 to nombremscopa do begin
  writeln(f,'Numero '+inttostr(i));
  writeln(f,listemscopa[-1+i].a1x,' ',listemscopa[-1+i].a1y,' ',listemscopa[-1+i].a2x,' ',
  listemscopa[-1+i].a2y,' ',listemscopa[-1+i].focale,' ',
  listemscopa[-1+i].epaisseur,' ',listemscopa[-1+i].couleur,' ',
  listemscopa[-1+i].couleuraxe);
  writeln(f,listemscopa[-1+i].hachures);
  writeln(f,listemscopa[-1+i].axefocal);
  writeln(f,listemscopa[-1+i].trou);
  writeln(f,listemscopa[-1+i].diametretrou);
  end;

    writeln(f,'Nombre de miroirs convexes paraxiaux:');
   writeln(f,nombremscepa);
  if nombremscepa>0 then
  for i:=1 to nombremscepa do begin
  writeln(f,'Numero '+inttostr(i));
  writeln(f,listemscepa[-1+i].a1x,' ',listemscepa[-1+i].a1y,' ',listemscepa[-1+i].a2x,' ',
  listemscepa[-1+i].a2y,' ',listemscepa[-1+i].focale,' ',
  listemscepa[-1+i].epaisseur,' ',listemscepa[-1+i].couleur,' ',
  listemscepa[-1+i].couleuraxe);
  writeln(f,listemscepa[-1+i].hachures);
  writeln(f,listemscepa[-1+i].axefocal);
  writeln(f,listemscepa[-1+i].trou);
  writeln(f,listemscepa[-1+i].diametretrou);
  end;

   writeln(f,'Nombre de miroirs concaves reels:');
  writeln(f,nombremscore);
  if nombremscore>0 then
  for i:=1 to nombremscore do begin
  writeln(f,'Numero '+inttostr(i));
  writeln(f,listemscore[-1+i].a1x,' ',listemscore[-1+i].a1y,' ',listemscore[-1+i].a2x,' ',
  listemscore[-1+i].a2y,' ',listemscore[-1+i].rayoncourbure,' ',
  listemscore[-1+i].epaisseur,' ',listemscore[-1+i].couleur,' ',
  listemscore[-1+i].couleuraxe);
  writeln(f,listemscore[-1+i].hachures);
  writeln(f,listemscore[-1+i].axefocal);
  writeln(f,listemscore[-1+i].aigu);
   writeln(f,listemscore[-1+i].trou);
    writeln(f,listemscore[-1+i].diametretrou);
  end;

    writeln(f,'Nombre de miroirs convexes reels:');
   writeln(f,nombremscere);
  if nombremscere>0 then
  for i:=1 to nombremscere do begin
  writeln(f,'Numero '+inttostr(i));
  writeln(f,listemscere[-1+i].a1x,' ',listemscere[-1+i].a1y,' ',listemscere[-1+i].a2x,' ',
  listemscere[-1+i].a2y,' ',listemscere[-1+i].rayoncourbure,' ',
  listemscere[-1+i].epaisseur,' ',listemscere[-1+i].couleur,' ',
  listemscere[-1+i].couleuraxe);
  writeln(f,listemscere[-1+i].hachures);
  writeln(f,listemscere[-1+i].axefocal);
  writeln(f,listemscere[-1+i].aigu);
   writeln(f,listemscere[-1+i].trou);
    writeln(f,listemscere[-1+i].diametretrou);
  end;

   writeln(f,'Nombre de lentiles minces convergentes:');
   writeln(f,nombrelmc);
  if nombrelmc>0 then
  for i:=1 to nombrelmc do begin
  writeln(f,'Numero '+inttostr(i));
  writeln(f,listelmc[-1+i].a1x,' ',listelmc[-1+i].a1y,' ',listelmc[-1+i].a2x,' ',
  listelmc[-1+i].a2y,' ',listelmc[-1+i].focale,' ',
  listelmc[-1+i].epaisseur,' ',listelmc[-1+i].couleur,' ',
  listelmc[-1+i].couleuraxe);
  writeln(f,listelmc[-1+i].axefocal);
  end;

  writeln(f,'Nombre de lentiles minces divergentes:');
  writeln(f,nombrelmd);
  if nombrelmd>0 then
  for i:=1 to nombrelmd do begin
  writeln(f,'Numero '+inttostr(i));
  writeln(f,listelmd[-1+i].a1x,' ',listelmd[-1+i].a1y,' ',listelmd[-1+i].a2x,' ',
  listelmd[-1+i].a2y,' ',listelmd[-1+i].focale,' ',
  listelmd[-1+i].epaisseur,' ',listelmd[-1+i].couleur,' ',
  listelmd[-1+i].couleuraxe);
  writeln(f,listelmd[-1+i].axefocal);
  end;

   writeln(f,'Nombre de rayons:');
   writeln(f,nombrerayon);
  if nombrerayon>0 then
  for i:=1 to nombrerayon do begin
  writeln(f,'Numero '+inttostr(i));
  writeln(f,listerayon[-1+i].ax,' ',listerayon[-1+i].ay,' ',listerayon[-1+i].kx,' ',
  listerayon[-1+i].ky,' ',listerayon[-1+i].couleur,' ',listerayon[-1+i].epaisseur,' ',
  listerayon[-1+i].nombremaxenfant);
  writeln(f,listerayon[-1+i].vav);
  writeln(f,listerayon[-1+i].vaa);
  writeln(f,'Nombre maximal de segments:');
  writeln(f,listerayon[-1+i].maxsegment);
  end;

  writeln(f,'Nombre de sources ponctuelles:');
  writeln(f,nombresourceponctuelle);
  if nombresourceponctuelle>0 then
  for i:=1 to nombresourceponctuelle do begin
  writeln(f,'Numero '+inttostr(i));
  writeln(f,listesourceponctuelle[-1+i].nombrederayon,' ',
  listesourceponctuelle[-1+i].sx,' ',
   listesourceponctuelle[-1+i].sy,' ',
  listesourceponctuelle[-1+i].a1x,' ',
  listesourceponctuelle[-1+i].a1y,' ',
  listesourceponctuelle[-1+i].a2x,' ',
  listesourceponctuelle[-1+i].a2y,' ',
   listesourceponctuelle[-1+i].couleur,' ',
   listesourceponctuelle[-1+i].epaisseur,' ',
   listesourceponctuelle[-1+i].maxenfantparrayon);
  writeln(f,listesourceponctuelle[-1+i].vav);
  writeln(f,listesourceponctuelle[-1+i].vaa);
   writeln(f,listesourceponctuelle[-1+i].sourcevirtuelle);
    writeln(f,listesourceponctuelle[-1+i].tracersurfacesonde);
     writeln(f,listesourceponctuelle[-1+i].listechemins);
     writeln(f,'Nombre maximal de segments par rayon:');
     writeln(f,listesourceponctuelle[-1+i].maxsegment);
  end;


   writeln(f,'Nombre d''ondes planes:');
   writeln(f,nombreondeplane);
  if nombreondeplane>0 then
  for i:=1 to nombreondeplane do begin
  writeln(f,'Numero '+inttostr(i));
  writeln(f,listeondeplane[-1+i].nombrederayon,' ',
  listeondeplane[-1+i].sx,' ',
   listeondeplane[-1+i].sy,' ',
  listeondeplane[-1+i].a1x,' ',
  listeondeplane[-1+i].a1y,' ',
  listeondeplane[-1+i].a2x,' ',
  listeondeplane[-1+i].a2y,' ',
   listeondeplane[-1+i].couleur,' ',
   listeondeplane[-1+i].epaisseur,' ',
   listeondeplane[-1+i].maxenfantparrayon);
  writeln(f,listeondeplane[-1+i].vav);
  writeln(f,listeondeplane[-1+i].vaa);
  writeln(f,listeondeplane[-1+i].tracersurfacesonde);
  writeln(f,listeondeplane[-1+i].listechemins);
  writeln(f,'Nombre maximal de segments par rayon:');
     writeln(f,listeondeplane[-1+i].maxsegment);
  end;

   writeln(f,'Nombre d''ecrans:');
   writeln(f,nombreecran);
  if nombreecran>0 then
  for i:=1 to nombreecran do begin
  writeln(f,'Numero '+inttostr(i));
  writeln(f,listeecran[-1+i].a1x,' ',listeecran[-1+i].a1y,' ',listeecran[-1+i].a2x,' ',
  listeecran[-1+i].a2y,' ',listeecran[-1+i].epaisseur,' ',listeecran[-1+i].couleur);
  end;

  writeln(f,'Nombre de diaphragmes:');
  writeln(f,nombrediaphragme);
  if nombrediaphragme>0 then
  for i:=1 to nombrediaphragme do begin
  writeln(f,'Numero '+inttostr(i));
  writeln(f,listediaphragme[-1+i].cx,' ',listediaphragme[-1+i].cy,' ',listediaphragme[-1+i].anglepolaire,' ',
  listediaphragme[-1+i].rint,listediaphragme[-1+i].rext,
  ' ',listediaphragme[-1+i].epaisseur,' ',listediaphragme[-1+i].couleur);
  end;


   writeln(f,'Nombre de lames semi reflechissantes:');
   writeln(f,nombrelsr);
  if nombrelsr>0 then
  for i:=1 to nombrelsr do begin
  writeln(f,'Numero '+inttostr(i));
  writeln(f,listelsr[-1+i].a1x,' ',listelsr[-1+i].a1y,' ',listelsr[-1+i].a2x,' ',
  listelsr[-1+i].a2y,' ',listelsr[-1+i].epaisseur,' ',listelsr[-1+i].couleur);
  end;

  writeln(f,'Nombre de polyhedres:');
  writeln(f,nombrepolyhedre);
  if nombrepolyhedre>0 then
  for i:=1 to nombrepolyhedre do begin
  writeln(f,'Numero '+inttostr(i));
  writeln(f,listepolyhedre[-1+i].nombresommet);
  for j:=1 to listepolyhedre[-1+i].nombresommet do
  writeln(f,listepolyhedre[-1+i].messommets[j].ax,' ',listepolyhedre[-1+i].messommets[j].ay);
  writeln(f,listepolyhedre[-1+i].indicerouge,' ',listepolyhedre[-1+i].indicebleu,' ',
  listepolyhedre[-1+i].indicevert);
  writeln(f,listepolyhedre[-1+i].couleurbord,' ',listepolyhedre[-1+i].couleurfond);
  case listepolyhedre[-1+i].reflechientrant of
  toujoursreflechi: writeln(f,'1');
  jamaisreflechi: writeln(f,'2');
  reflechisirefracte:writeln(f,'3');
  reflechisipasrefracte:writeln(f,'4');
  end;
  case listepolyhedre[-1+i].reflechisortant of
  toujoursreflechi: writeln(f,'1');
  jamaisreflechi: writeln(f,'2');
  reflechisirefracte:writeln(f,'3');
  reflechisipasrefracte:writeln(f,'4');
  end;
  end;



  writeln(f,'Nombre de prismes:');
  writeln(f,nombreprisme);
  if nombreprisme>0 then
  for i:=1 to nombreprisme do begin
  writeln(f,'Numero '+inttostr(i));
  writeln(f,listeprisme[-1+i].gx,' ',listeprisme[-1+i].gy);
  writeln(f,listeprisme[-1+i].ax,' ',listeprisme[-1+i].ay);
  writeln(f,listeprisme[-1+i].bx,' ',listeprisme[-1+i].by);
  writeln(f,listeprisme[-1+i].indicerouge,' ',listeprisme[-1+i].indicebleu,' ',
  listeprisme[-1+i].indicevert);
  writeln(f,listeprisme[-1+i].couleurbord,' ',listeprisme[-1+i].couleurfond);
  case listeprisme[-1+i].reflechientrant of
  toujoursreflechi: writeln(f,'1');
  jamaisreflechi: writeln(f,'2');
  reflechisirefracte:writeln(f,'3');
  reflechisipasrefracte:writeln(f,'4');
  end;
  case listeprisme[-1+i].reflechisortant of
  toujoursreflechi: writeln(f,'1');
  jamaisreflechi: writeln(f,'2');
  reflechisirefracte:writeln(f,'3');
  reflechisipasrefracte:writeln(f,'4');
  end;
  end;




  writeln(f,'Nombre de polycercles:');
  writeln(f,nombrepolycercle);
  if nombrepolycercle>0 then
  for i:=1 to nombrepolycercle do begin
  writeln(f,'Numero '+inttostr(i));
  writeln(f,listepolycercle[-1+i].nombresommet);
  for j:=1 to listepolycercle[-1+i].nombresommet do
  writeln(f,listepolycercle[-1+i].messommets[j].ax,' ',listepolycercle[-1+i].messommets[j].ay);
   for j:=1 to listepolycercle[-1+i].nombresommet do
 writeln(f,listepolycercle[-1+i].mess[j].ax,' ',listepolycercle[-1+i].mess[j].ay);
   for j:=1 to listepolycercle[-1+i].nombresommet do
  writeln(f,listepolycercle[-1+i].rectiligne[j]);
  writeln(f,listepolycercle[-1+i].indicerouge,' ',listepolycercle[-1+i].indicebleu,' ',
  listepolycercle[-1+i].indicevert);
  writeln(f,listepolycercle[-1+i].couleurbord,' ',listepolycercle[-1+i].couleurfond);
  case listepolycercle[-1+i].reflechientrant of
  toujoursreflechi: writeln(f,'1');
  jamaisreflechi: writeln(f,'2');
  reflechisirefracte:writeln(f,'3');
  reflechisipasrefracte:writeln(f,'4');
  end;
  case listepolycercle[-1+i].reflechisortant of
  toujoursreflechi: writeln(f,'1');
  jamaisreflechi: writeln(f,'2');
  reflechisirefracte:writeln(f,'3');
  reflechisipasrefracte:writeln(f,'4');
  end;
  end;

  writeln(f,'Nombre de spheres:');
  writeln(f,nombresphere);
    if nombresphere>0 then
  for i:=1 to nombresphere do begin
  writeln(f,'Numero '+inttostr(i));
  writeln(f,listesphere[-1+i].cx,' ',listesphere[-1+i].cy,' ',listesphere[-1+i].rayon,' ',
  listesphere[-1+i].indicerouge,' ',listesphere[-1+i].indicebleu,' ',listesphere[-1+i].indicevert);
  writeln(f,listesphere[-1+i].couleurbord,' ',listesphere[-1+i].couleurfond);
  case listesphere[-1+i].reflechientrant of
  toujoursreflechi: writeln(f,'1');
  jamaisreflechi: writeln(f,'2');
  reflechisirefracte:writeln(f,'3');
  reflechisipasrefracte:writeln(f,'4');
  end;
  case listesphere[-1+i].reflechisortant of
  toujoursreflechi: writeln(f,'1');
  jamaisreflechi: writeln(f,'2');
  reflechisirefracte:writeln(f,'3');
  reflechisipasrefracte:writeln(f,'4');
  end;
  end;

  writeln(f,'Nombre de miroirs coniques:');
  writeln(f,nombremiroirconique);
  if nombremiroirconique>0 then
  for i:=1 to nombremiroirconique do begin
  writeln(f,'Numero '+inttostr(i));
  writeln(f,listemiroirconique[-1+i].fx,' ',listemiroirconique[-1+i].fy,' ',
  listemiroirconique[-1+i].excentricite,' ',listemiroirconique[-1+i].parametre,' ',
  listemiroirconique[-1+i].theta0,' ',listemiroirconique[-1+i].theta1,' ',
  listemiroirconique[-1+i].theta2);
  writeln(f,listemiroirconique[-1+i].couleur,' ',listemiroirconique[-1+i].couleuraxe);
  writeln(f,listemiroirconique[-1+i].hachures);
  writeln(f,listemiroirconique[-1+i].axefocal);
  writeln(f,listemiroirconique[-1+i].concave);
  end;

  writeln(f,'Nombre de textes:');
  writeln(f,nombretexte);
  if nombretexte>0 then
   for i:=1 to nombretexte do begin
   writeln(f,'Numero '+inttostr(i));
  writeln(f,listetexteaffiche[-1+i].texteluimeme);
  writeln(f,listetexteaffiche[-1+i].x,' ',listetexteaffiche[-1+i].y);
   writeln(f,listetexteaffiche[-1+i].policecharset,' ',
   listetexteaffiche[-1+i].policecolor);
   writeln(f,listetexteaffiche[-1+i].policename);
   writeln(f,listetexteaffiche[-1+i].policesize);
   if listetexteaffiche[-1+i].policefsbold then
   writeln(f,'fsbold') else writeln(f,'no');
   if listetexteaffiche[-1+i].policefsitalic  then
   writeln(f,'fsitalic') else writeln(f,'no');
   if listetexteaffiche[-1+i].policefsunderline  then
   writeln(f,'fsunderline') else writeln(f,'no');
   if listetexteaffiche[-1+i].policefsstrikeout  then
   writeln(f,'fsstrikeout') else writeln(f,'no');
   if listetexteaffiche[-1+i].policepitch=fpDefault then writeln(f,'fpDefault');
   if  listetexteaffiche[-1+i].policepitch=fpVariable then writeln(f,'fpVariable');
   if  listetexteaffiche[-1+i].policepitch=fpFixed then writeln(f,'fpFixed');

   end;


    writeln(f,'Nombre de lentilles epaisses:');
    writeln(f,nombrelec);
  if nombrelec>0 then
  for i:=1 to nombrelec do begin
  writeln(f,'Numero '+inttostr(i));
  writeln(f,listelec[-1+i].s1x,' ',listelec[-1+i].s1y,' ',
  listelec[-1+i].s2x,' ',listelec[-1+i].s2y,' ',
  listelec[-1+i].r1,' ',listelec[-1+i].r2,' ',
  listelec[-1+i].diametre);
  writeln(f,listelec[-1+i].couleurfond,' ',listelec[-1+i].couleuraxe);
  writeln(f,listelec[-1+i].axefocal);
  writeln(f,listelec[-1+i].indicerouge,' ',listelec[-1+i].indicebleu,' ',
  listelec[-1+i].indicevert);
  case listelec[-1+i].reflechientrant of
  toujoursreflechi: writeln(f,'1');
  jamaisreflechi: writeln(f,'2');
  reflechisirefracte:writeln(f,'3');
  reflechisipasrefracte:writeln(f,'4');
  end;
  case listelec[-1+i].reflechisortant of
  toujoursreflechi: writeln(f,'1');
  jamaisreflechi: writeln(f,'2');
  reflechisirefracte:writeln(f,'3');
  reflechisipasrefracte:writeln(f,'4');
  end;
  end;


    writeln(f,'Nombre d''angles:');
    writeln(f,nombreangle);
  if nombreangle>0 then
  for i:=1 to nombreangle do begin
  writeln(f,'Numero '+inttostr(i));
  writeln(f,listeangle[-1+i].cx,' ',listeangle[-1+i].cy,' ',
  listeangle[-1+i].a1x,' ',listeangle[-1+i].a1y,' ',
  listeangle[-1+i].a2x,' ',listeangle[-1+i].a2y);
  end;


  { a partir de la version 3}
   writeln(f,'Nombre de traits:');
    writeln(f,nombrefleche);
  if nombrefleche>0 then
  for i:=1 to nombrefleche do begin
  writeln(f,'Numero '+inttostr(i));
  writeln(f,listefleche[-1+i].a1x,' ',listefleche[-1+i].a1y,' ',
  listefleche[-1+i].a2x,' ',listefleche[-1+i].a2y);
  writeln(f,listefleche[-1+i].epaisseur,'  ',listefleche[-1+i].couleur);
  case listefleche[-1+i].ext1 of
  trien: writeln(f,'trien');
  ttrait: writeln(f,'ttrait');
  tffleche: writeln(f,'tffleche');
  trond: writeln(f,'trond');
  tcroix: writeln(f,'tcroix');
  tcroixx: writeln(f,'tcroixx');
  tdisque: writeln(f,'tdisque');
  end;
 case listefleche[-1+i].ext2 of
  trien: writeln(f,'trien');
   ttrait: writeln(f,'ttrait');
  tffleche: writeln(f,'tffleche');
  trond: writeln(f,'trond');
  tcroix: writeln(f,'tcroix');
  tcroixx: writeln(f,'tcroixx');
  tdisque: writeln(f,'tdisque');
  end;
 case listefleche[-1+i].styletrait of
 pssolid: writeln(f,'pssolid');
 psdot:writeln(f,'psdot');
 psdash: writeln(f,'psdash');
 psdashdot: writeln(f,'psdashdot');
 psdashdotdot: writeln(f,'psdashdotdot');
 end;
 writeln(f,listefleche[-1+i].taille1,'  ',listefleche[-1+i].taille2);

  end;

  { a partir de a version 4}

  writeln(f,'Nombre d''oeils:');
   writeln(f,nombreoeil);
  if nombreoeil>0 then
  for i:=1 to nombreoeil do begin
  writeln(f,'Numero '+inttostr(i));
  writeln(f,listeoeil[-1+i].cx,' ',listeoeil[-1+i].cy,' ',listeoeil[-1+i].ax,' ',
  listeoeil[-1+i].ay,' ',listeoeil[-1+i].epaisseur,' ',listeoeil[-1+i].couleur);
  end;

    { a partir de la version 5}

    writeln(f,'Nombre de mesures de distances');
    writeln(f,nombremetre);
    if nombremetre>0 then for i:=1 to nombremetre do begin
    writeln(f,'Numero '+inttostr(i));
    writeln(f,listemetre[-1+i].a1x,'  ',listemetre[-1+i].a1y,'  ',
    listemetre[-1+i].a2x,'  ',listemetre[-1+i].a2y);
    end;


  writeln(f,'Nombre de groupes:');
   writeln(f,nombregroupe);
  if nombregroupe>0 then
  for i:=1 to nombregroupe do begin
  writeln(f,'Numero groupe'+inttostr(i));
  writeln(f,listegroupe[-1+i].nombreelements);
  for j:=1 to listegroupe[-1+i].nombreelements do begin
  writeln(f,listegroupe[-1+i].listeelements[j].indiceelement);
  case listegroupe[-1+i].listeelements[j].typeelementgroupe of
  mdoeil: writeln(f,'mdoeil');
  mddiaphragme: writeln(f,'mddiaphragme');
  mdtexteaffiche: writeln(f,'mdtexteaffiche');
  mdmiroirconique: writeln(f,'mdmiroirconique');
  mdlec: writeln(f,'mdlec');
  mdfleche:writeln(f,'mdfleche');
  mdmiroirplan: writeln(f,'mdmiroirplan');
   mdreseau: writeln(f,'mdreseau');
  mdecran: writeln(f,'mdecran');
  mdangle: writeln(f,'mdangle');
  mdmetre: writeln(f,'mdmetre');
  mdlsr: writeln(f,'mdlsr');
  mdmscopa: writeln(f,'mdmscopa');
  mdlmc: writeln(f,'mdlmc');
  mdlmd: writeln(f,'mdlmd');
  mdmscepa: writeln(f,'mdmscepa');
  mdmscore: writeln(f,'mdmscore');
  mdmscere: writeln(f,'mdmscere');
   mdpolyhedre: writeln(f,'mdpolyhedre');
    mdrayon: writeln(f,'mdrayon');
     mdsourceponctuelle: writeln(f,'mdsourceponctuelle');
      mdondeplane: writeln(f,'mdondeplane');
       mdpolycercle: writeln(f,'mdpolycercle');
        mdsphere: writeln(f,'mdsphere');
        mdprisme: writeln(f,'mdprisme');
        else writeln(f,'mdneant');
  end;
  end;


  end;

      writeln(f,'couleur fond simulation');
  writeln(f,couleurfondsimulation);
  writeln(f,'empennage');
  writeln(f,'position');
  writeln(f,positionempennage);
  writeln(f,'taille');
  writeln(f,tailleempennage);

    closefile(f);
        modifie:=false;

     try
    formediteur.memo1.Lines.SaveToFile((copy(nomfichier,1,length(nomfichier)-3)+
    'txt'));
    except
    application.MessageBox(pchar(rsImpossibleDe3),
      pchar(rsErreurDCritu), mb_ok);
    end;
    formediteur.caption:=Format(rsCommentaires4, [nomfichier]);
     commentaire_change:=false;
    end;




 procedure TForm1.Enregistrersimulation2Click(Sender: TObject);
begin
if nomsauvegarde<>'' then enregistrer(nomsauvegarde) else
 Form1.Enregistrersimulation1Click(Sender);
end;

 procedure TForm1.Enregistrersimulation1Click(Sender: TObject);
 var f:textfile; i,j:integer;
label 1010;
 begin
 if modequelquechose then exit;
1010:
savedialog1.FileName:='';
 savedialog1.InitialDir:=repertoire_simul_perso;
 if saveDialog1.Execute then { affiche la boîte de dialogue d'ouverture }
  begin
  if fileexistsutf8((saveDialog1.FileName)) then
 if application.messagebox(pchar(rsUnFichierPor),
  pchar(rsAttention3), MB_YESNO)=IDNO then goto 1010;
   enregistrer(savedialog1.filename);
   nomsauvegarde:=savedialog1.filename;
  form1.Caption:=Format(rsEspaceDeTrav3, [floattostr(xxmin), floattostr(xxmax),
    floattostr(yymin), floattostr(yymax), extractfilename(nomsauvegarde)]);
  end;
end;


procedure TForm1.Ouvrirsimulation(Sender: TObject;repertoiredepart:string);
var f:textfile; i,j:integer;   s,s1:string;
newnombremiroirplan,newnombrereseau,newnombremscopa,newnombremscepa,newnombremscore,
newnombremscere,newnombrelmc,newnombrelmd,newnombrerayon,newnombrelsr,
newnombresourceponctuelle,newnombreondeplane,newnombreecran,toto,
newnombrepolyhedre,newnombresphere,newnombremiroirconique,
newnombretexte,newnombrediaphragme,newnombrelec,newnombrepolycercle,
newnombreangle,newnombrefleche,newnombreoeil,newnombregroupe,
newnombremetre,newnombreprisme:integer;
coco:tcolor;      aze:ttypeelementmodif;
chacha,papa:integer;    versionfichier:longint;
nerina:string;

label 9852,789,12345;

begin
if modequelquechose then exit;
if (parametrelignecommande) then
begin
nerina:=paramstr(1);
parametrelignecommande:=false;
goto 789;
end;
OpenDialog1.FileName := '';
 opendialog1.InitialDir:=repertoiredepart;
if not(OpenDialog1.Execute) then exit
else nerina:=(OpenDialog1.FileName); { affiche la boîte de dialogue d'ouverture }
  if  ((pos(repertoire_exemples,extractfilepath(nerina))=0) and
  (pos(repertoire_simul_perso,extractfilepath(nerina))=0)) then
    repertoire_actuel:=extractfilepath(nerina);
    if nom_fichier_config_perso<>'' then begin
    try
if not(directoryexistsUTF8(repertoire_config_perso)) then
   forcedirectories((repertoire_config_perso));
assignfile(f,(nom_fichier_config_perso));
rewrite(f);
closefile(f);
saisiepreferences.BitBtn2Click(Sender);
except

end;
 end;


 789:   try

    Assignfile(F,(nerina));
    except
    Application.messagebox(pchar(rsFichierInacc), pchar(rsAttention), mb_ok);
    exit;
    end;

// application.MessageBox(pchar(nerina),pchar((nerina)),mb_ok);
   try
  reset(f);
  except
    Application.messagebox(pchar(rsFichierInacc2), pchar(rsAttention), mb_ok);
    exit;
    end;

    metazero;   versionfichier:=221;
    try  {suivi du bloc de lecture; si une erreur se produit, elle est traitee a la fin}
   while not(eof(f)) do begin
   readln(f,s1);
   if s1='NE PAS MODIFIER CE FICHIER' then goto  12345;

   if s1='VERSION' then begin
   readln(f,versionfichier);
   readln(f,xxmin,xxmax,yymin,yymax);
   readln(f,s);
   respectrapport:=(s='TRUE');
   goto 12345;
   end;



  if s1= 'UNITE' then begin
   readln(f,unitelongueur);
   goto 12345; end;

   if s1='OPTIONS GRILLE' then begin
    readln(f,s);
   tracergrille:=(s='TRUE');
    readln(f,s);
   attractiongrille:=(s='TRUE');
   readln(f,grillex,grilley);
   goto 12345; end;

     if s1= 'OPTIONS NORMALES' then begin
     readln(f,s);
optionnormale:=(s='TRUE');
readln(f,s);
optionangles:=(s='TRUE');
 readln(f,pourcentagenormale);
 readln(f,couleurnormale);
 readln(f,epaisseurnormale);
  readln(f,decimalesangles);
  goto 12345; end;

   if s1='OPTIONS SEGMENTS FANTOMES' then begin
  readln(f,s);
  fantomespresents:=(s='TRUE');
  readln(f,listefantomes);
  goto 12345; end;

  if s1= 'NOMBRE POINTS TRACE SURFACE ONDE' then begin
      readln(f,maxrayonsrecherchesurfacesonde);
        if fantomespresents then
for i:=1 to maxtotalsegment do
  fantomes[i]:=(pos(','+inttostr(i)+',',listefantomes)<>0) or (pos('('+inttostr(i)+',',listefantomes)<>0) or
  (pos(','+inttostr(i)+')',listefantomes)<>0) or
  (pos('('+inttostr(i)+')',listefantomes)<>0);
      goto 12345; end;

  if s1='INDICES PAR DEFAUT' then begin
    readln(f,indice_vert_par_defaut);
    readln(f,indice_bleu_par_defaut);
    readln(f,indice_rouge_par_defaut);
    goto 12345;
   end;


  if s1='LAMBDA' then begin
    readln(f,longueur_donde_red);
    readln(f,longueur_donde_blue);
    readln(f,longueur_donde_green);
    goto 12345;
   end;





  if s1='Nombre de miroirs plans:' then begin
  readln(f,newnombremiroirplan);
  retaillepmiroirplan(listemiroirplan,nombremiroirplan,newnombremiroirplan);
  nombremiroirplan:=newnombremiroirplan;
  if nombremiroirplan>0 then
     for i:=1 to nombremiroirplan do begin
     readln(f,s);
  readln(f,listemiroirplan[-1+i].a1x,listemiroirplan[-1+i].a1y,listemiroirplan[-1+i].a2x,
  listemiroirplan[-1+i].a2y,listemiroirplan[-1+i].epaisseur,listemiroirplan[-1+i].couleur);
  readln(f,s);
  listemiroirplan[-1+i].hachures:=(s='TRUE');
 if not( listemiroirplan[-1+i].create(listemiroirplan[-1+i].a1x,listemiroirplan[-1+i].a1y,
  listemiroirplan[-1+i].a2x,listemiroirplan[-1+i].a2y,listemiroirplan[-1+i].epaisseur,
  listemiroirplan[-1+i].couleur,listemiroirplan[-1+i].hachures)) then
  raise einouterror.Create('erreur');
  end;
  goto 12345; end;


   if s1='Nombre de reseaux:' then begin
  readln(f,newnombrereseau);
  retaillepreseau(listereseau,nombrereseau,newnombrereseau);
  nombrereseau:=newnombreReseau;
  if nombreReseau>0 then
     for i:=1 to nombreReseau do begin
     readln(f,s);
  readln(f,listeReseau[-1+i].a1x,listeReseau[-1+i].a1y,listeReseau[-1+i].a2x,
  listeReseau[-1+i].a2y);
  readln(f,listeReseau[-1+i].epaisseur);
  readln(f,listeReseau[-1+i].couleur);
  readln(f,listeReseau[-1+i].pas);
  readln(f,s);
  listeReseau[-1+i].entransmission:=(s='TRUE');
  readln(f,s);
  listeReseau[-1+i].hachures:=(s='TRUE');
   readln(f,listeReseau[-1+i].ordre_min);
   readln(f,listeReseau[-1+i].ordre_max);
 if not( listeReseau[-1+i].create(listeReseau[-1+i].a1x,listeReseau[-1+i].a1y,
  listeReseau[-1+i].a2x,listeReseau[-1+i].a2y,listeReseau[-1+i].epaisseur,
  listeReseau[-1+i].couleur,listeReseau[-1+i].pas,listeReseau[-1+i].entransmission,listeReseau[-1+i].hachures,
  listeReseau[-1+i].ordre_min,listeReseau[-1+i].ordre_max)) then
  raise einouterror.Create('erreur');
  end;
  goto 12345; end;




  if s1='Nombre de miroirs concaves paraxiaux:' then begin
   readln(f,newnombremscopa);
  retaillepmscopa(listemscopa,nombremscopa,newnombremscopa);
  nombremscopa:=newnombremscopa;
  if nombremscopa>0 then
     for i:=1 to nombremscopa do begin
     readln(f,s);
  readln(f,listemscopa[-1+i].a1x,listemscopa[-1+i].a1y,listemscopa[-1+i].a2x,
  listemscopa[-1+i].a2y,listemscopa[-1+i].focale,listemscopa[-1+i].epaisseur,
  listemscopa[-1+i].couleur,listemscopa[-1+i].couleuraxe);
  readln(f,s);
  listemscopa[-1+i].hachures:=(s='TRUE');
  readln(f,s);
  listemscopa[-1+i].axefocal:=(s='TRUE');
  readln(f,s);
  listemscopa[-1+i].trou:=(s='TRUE');
   readln(f,listemscopa[-1+i].diametretrou);
 if not( listemscopa[-1+i].create2(listemscopa[-1+i].a1x,listemscopa[-1+i].a1y,
  listemscopa[-1+i].a2x,listemscopa[-1+i].a2y,listemscopa[-1+i].focale,
  listemscopa[-1+i].epaisseur,
  listemscopa[-1+i].couleur,listemscopa[-1+i].couleuraxe,
  listemscopa[-1+i].hachures,listemscopa[-1+i].axefocal,
  listemscopa[-1+i].trou,listemscopa[-1+i].diametretrou))then
  raise einouterror.Create('erreur');
  end;
    goto 12345; end;

    if s1='Nombre de miroirs convexes paraxiaux:'
  then begin
   readln(f,newnombremscepa);
  retaillepmscepa(listemscepa,nombremscepa,newnombremscepa);
  nombremscepa:=newnombremscepa;
  if nombremscepa>0 then
     for i:=1 to nombremscepa do begin
     readln(f,s);
  readln(f,listemscepa[-1+i].a1x,listemscepa[-1+i].a1y,listemscepa[-1+i].a2x,
  listemscepa[-1+i].a2y,listemscepa[-1+i].focale,listemscepa[-1+i].epaisseur,
  listemscepa[-1+i].couleur,listemscepa[-1+i].couleuraxe);
  readln(f,s);
  listemscepa[-1+i].hachures:=(s='TRUE');
  readln(f,s);
  listemscepa[-1+i].axefocal:=(s='TRUE');
   readln(f,s);
  listemscepa[-1+i].trou:=(s='TRUE');
  readln(f,listemscepa[-1+i].diametretrou);
 if not( listemscepa[-1+i].create2(listemscepa[-1+i].a1x,listemscepa[-1+i].a1y,
  listemscepa[-1+i].a2x,listemscepa[-1+i].a2y,listemscepa[-1+i].focale,
  listemscepa[-1+i].epaisseur,
  listemscepa[-1+i].couleur,listemscepa[-1+i].couleuraxe,
  listemscepa[-1+i].hachures,listemscepa[-1+i].axefocal,
  listemscepa[-1+i].trou,listemscepa[-1+i].diametretrou))then
  raise einouterror.Create('erreur');
  end;
   goto 12345; end;

   if s1='Nombre de miroirs concaves reels:' then begin
  readln(f,newnombremscore);
  retaillepmscore(listemscore,nombremscore,newnombremscore);
  nombremscore:=newnombremscore;
  if nombremscore>0 then
     for i:=1 to nombremscore do begin
     readln(f,s);
  readln(f,listemscore[-1+i].a1x,listemscore[-1+i].a1y,listemscore[-1+i].a2x,
  listemscore[-1+i].a2y,listemscore[-1+i].rayoncourbure,listemscore[-1+i].epaisseur,
  listemscore[-1+i].couleur,listemscore[-1+i].couleuraxe);
  readln(f,s);
  listemscore[-1+i].hachures:=(s='TRUE');
  readln(f,s);
  listemscore[-1+i].axefocal:=(s='TRUE');
  readln(f,s);
  listemscore[-1+i].aigu:=(s='TRUE');
  readln(f,s);
  listemscore[-1+i].trou:=(s='TRUE');
  readln(f,listemscore[-1+i].diametretrou);
if not(  listemscore[-1+i].create2(listemscore[-1+i].a1x,listemscore[-1+i].a1y,
  listemscore[-1+i].a2x,listemscore[-1+i].a2y,listemscore[-1+i].rayoncourbure,
  listemscore[-1+i].epaisseur,
  listemscore[-1+i].couleur,listemscore[-1+i].couleuraxe,
  listemscore[-1+i].hachures,listemscore[-1+i].axefocal,listemscore[-1+i].aigu,
  listemscore[-1+i].trou,listemscore[-1+i].diametretrou))then
  raise einouterror.Create('erreur');
  end;
   goto 12345; end;

   if s1='Nombre de miroirs convexes reels:' then begin
    readln(f,newnombremscere);
  retaillepmscere(listemscere,nombremscere,newnombremscere);
  nombremscere:=newnombremscere;
  if nombremscere>0 then
     for i:=1 to nombremscere do begin
     readln(f,s);
  readln(f,listemscere[-1+i].a1x,listemscere[-1+i].a1y,listemscere[-1+i].a2x,
  listemscere[-1+i].a2y,listemscere[-1+i].rayoncourbure,listemscere[-1+i].epaisseur,
  listemscere[-1+i].couleur,listemscere[-1+i].couleuraxe);
  readln(f,s);
  listemscere[-1+i].hachures:=(s='TRUE');
  readln(f,s);
  listemscere[-1+i].axefocal:=(s='TRUE');
  readln(f,s);
  listemscere[-1+i].aigu:=(s='TRUE');
  readln(f,s);
  listemscere[-1+i].trou:=(s='TRUE');
  readln(f,listemscere[-1+i].diametretrou);
 if not( listemscere[-1+i].create2(listemscere[-1+i].a1x,listemscere[-1+i].a1y,
  listemscere[-1+i].a2x,listemscere[-1+i].a2y,listemscere[-1+i].rayoncourbure,
  listemscere[-1+i].epaisseur,
  listemscere[-1+i].couleur,listemscere[-1+i].couleuraxe,
  listemscere[-1+i].hachures,listemscere[-1+i].axefocal,listemscere[-1+i].aigu,
  listemscere[-1+i].trou,listemscere[-1+i].diametretrou))then
  raise einouterror.Create('erreur');
  end;
  goto 12345; end;

   if s1='Nombre de lentiles minces convergentes:' then begin
   readln(f,newnombrelmc);
  retailleplmc(listelmc,nombrelmc,newnombrelmc);
  nombrelmc:=newnombrelmc;
  if nombrelmc>0 then
     for i:=1 to nombrelmc do begin
     readln(f,s);
  readln(f,listelmc[-1+i].a1x,listelmc[-1+i].a1y,listelmc[-1+i].a2x,
  listelmc[-1+i].a2y,listelmc[-1+i].focale,listelmc[-1+i].epaisseur,
  listelmc[-1+i].couleur,listelmc[-1+i].couleuraxe);
  readln(f,s);
  listelmc[-1+i].axefocal:=(s='TRUE');
  if not(listelmc[-1+i].create2(listelmc[-1+i].a1x,listelmc[-1+i].a1y,
  listelmc[-1+i].a2x,listelmc[-1+i].a2y,listelmc[-1+i].focale,
  listelmc[-1+i].epaisseur,
  listelmc[-1+i].couleur,listelmc[-1+i].couleuraxe,
  listelmc[-1+i].axefocal))then
  raise einouterror.Create('erreur');
  end;
  goto 12345; end;

  if s1='Nombre de lentiles minces divergentes:' then begin
  readln(f,newnombrelmd);
  retailleplmd(listelmd,nombrelmd,newnombrelmd);
  nombrelmd:=newnombrelmd;
  if nombrelmd>0 then
     for i:=1 to nombrelmd do begin
     readln(f,s);
  readln(f,listelmd[-1+i].a1x,listelmd[-1+i].a1y,listelmd[-1+i].a2x,
  listelmd[-1+i].a2y,listelmd[-1+i].focale,listelmd[-1+i].epaisseur,
  listelmd[-1+i].couleur,listelmd[-1+i].couleuraxe);
  readln(f,s);
  listelmd[-1+i].axefocal:=(s='TRUE');
 if not( listelmd[-1+i].create2(listelmd[-1+i].a1x,listelmd[-1+i].a1y,
  listelmd[-1+i].a2x,listelmd[-1+i].a2y,listelmd[-1+i].focale,
  listelmd[-1+i].epaisseur,
  listelmd[-1+i].couleur,listelmd[-1+i].couleuraxe,
  listelmd[-1+i].axefocal))then
  raise einouterror.Create('erreur');
  end;
   goto 12345; end;

   if s1='Nombre de rayons:' then begin
    readln(f,newnombrerayon);
  retailleprayon(listerayon,nombrerayon,newnombrerayon);
  nombrerayon:=newnombrerayon;
  if nombrerayon>0 then
     for i:=1 to nombrerayon do begin
     readln(f,s);
  readln(f,listerayon[-1+i].ax,listerayon[-1+i].ay,listerayon[-1+i].kx,
  listerayon[-1+i].ky,listerayon[-1+i].couleur,listerayon[-1+i].epaisseur,
  listerayon[-1+i].nombremaxenfant);
  readln(f,s);
  listerayon[-1+i].vav:=s;
   readln(f,s);
  listerayon[-1+i].vaa:=s;
  if versionfichier>=200 then begin
  readln(f,s);
   readln(f,listerayon[-1+i].maxsegment);
   end else
   listerayon[-1+i].maxsegment:=maxmaxsegment;
if not(  listerayon[-1+i].create(listerayon[-1+i].ax,listerayon[-1+i].ay,
  listerayon[-1+i].kx,listerayon[-1+i].ky, listerayon[-1+i].couleur,
  listerayon[-1+i].epaisseur,
  listerayon[-1+i].vav,listerayon[-1+i].vaa,
 listerayon[-1+i].nombremaxenfant,listerayon[-1+i].maxsegment  ))then
  raise einouterror.Create('erreur');
    end;
  goto 12345; end;



   if s1='Nombre de sources ponctuelles:' then begin
   readln(f,newnombresourceponctuelle);
  retaillepsourceponctuelle(listesourceponctuelle,nombresourceponctuelle,newnombresourceponctuelle);
  nombresourceponctuelle:=newnombresourceponctuelle;
  if nombresourceponctuelle>0 then
  for i:=1 to nombresourceponctuelle do begin
  readln(f,s);
  readln(f,listesourceponctuelle[-1+i].nombrederayon,
  listesourceponctuelle[-1+i].sx,
   listesourceponctuelle[-1+i].sy,
  listesourceponctuelle[-1+i].a1x,
  listesourceponctuelle[-1+i].a1y,
  listesourceponctuelle[-1+i].a2x,
  listesourceponctuelle[-1+i].a2y,
   listesourceponctuelle[-1+i].couleur,
   listesourceponctuelle[-1+i].epaisseur,
   listesourceponctuelle[-1+i].maxenfantparrayon);
   readln(f,s);
listesourceponctuelle[-1+i].vav:=s;
readln(f,s);
listesourceponctuelle[-1+i].vaa:=s;
readln(f,s);
listesourceponctuelle[-1+i].sourcevirtuelle:=(s='TRUE');
readln(f,s);
listesourceponctuelle[-1+i].tracersurfacesonde:=(s='TRUE');
readln(f,s);
listesourceponctuelle[-1+i].listechemins:=s;
if versionfichier>=200 then begin
 readln(f,s);
  readln(f,listesourceponctuelle[-1+i].maxsegment);
  end else
  listesourceponctuelle[-1+i].maxsegment:=maxmaxsegment;
if not(listesourceponctuelle[-1+i].create(listesourceponctuelle[-1+i].nombrederayon,
  listesourceponctuelle[-1+i].sx,
   listesourceponctuelle[-1+i].sy,
  listesourceponctuelle[-1+i].a1x,
  listesourceponctuelle[-1+i].a1y,
  listesourceponctuelle[-1+i].a2x,
  listesourceponctuelle[-1+i].a2y,
   listesourceponctuelle[-1+i].couleur,
   listesourceponctuelle[-1+i].epaisseur,listesourceponctuelle[-1+i].vav,
   listesourceponctuelle[-1+i].vaa,
   listesourceponctuelle[-1+i].maxenfantparrayon,listesourceponctuelle[-1+i].sourcevirtuelle,
   listesourceponctuelle[-1+i].tracersurfacesonde,listesourceponctuelle[-1+i].listechemins,
   listesourceponctuelle[-1+i].maxsegment))then
  raise einouterror.Create('erreur');
    end;
     goto 12345; end;


     if s1='Nombre d''ondes planes:' then begin
     readln(f,newnombreondeplane);
  retaillepondeplane(listeondeplane,nombreondeplane,newnombreondeplane);
  nombreondeplane:=newnombreondeplane;
  if nombreondeplane>0 then
  for i:=1 to nombreondeplane do begin
  readln(f,s);
  readln(f,listeondeplane[-1+i].nombrederayon,
  listeondeplane[-1+i].sx,
   listeondeplane[-1+i].sy,
  listeondeplane[-1+i].a1x,
  listeondeplane[-1+i].a1y,
  listeondeplane[-1+i].a2x,
  listeondeplane[-1+i].a2y,
   listeondeplane[-1+i].couleur,
   listeondeplane[-1+i].epaisseur,
   listeondeplane[-1+i].maxenfantparrayon);
   readln(f,s);
listeondeplane[-1+i].vav:=s;
 readln(f,s);
listeondeplane[-1+i].vaa:=s;
readln(f,s);
listeondeplane[-1+i].tracersurfacesonde:=(s='TRUE');
readln(f,s);
listeondeplane[-1+i].listechemins:=s;
if versionfichier>=200 then begin
 readln(f,s);
  readln(f,listeondeplane[-1+i].maxsegment);
  end else
  listeondeplane[-1+i].maxsegment:=maxmaxsegment;
if not(listeondeplane[-1+i].create(listeondeplane[-1+i].nombrederayon,
  listeondeplane[-1+i].sx,
   listeondeplane[-1+i].sy,
  listeondeplane[-1+i].a1x,
  listeondeplane[-1+i].a1y,
  listeondeplane[-1+i].a2x,
  listeondeplane[-1+i].a2y,
   listeondeplane[-1+i].couleur,
   listeondeplane[-1+i].epaisseur,listeondeplane[-1+i].vav,
   listeondeplane[-1+i].vaa,listeondeplane[-1+i].maxenfantparrayon,
   listeondeplane[-1+i].tracersurfacesonde,
   listeondeplane[-1+i].listechemins,
   listeondeplane[-1+i].maxsegment))then
  raise einouterror.Create('erreur');
end;
  goto 12345; end;


 if s1='Nombre d''ecrans:' then begin
 readln(f,newnombreecran);
  retaillepecran(listeecran,nombreecran,newnombreecran);
  nombreecran:=newnombreecran;
  if nombreecran>0 then
     for i:=1 to nombreecran do begin
     readln(f,s);
  readln(f,listeecran[-1+i].a1x,listeecran[-1+i].a1y,listeecran[-1+i].a2x,
  listeecran[-1+i].a2y,listeecran[-1+i].epaisseur,listeecran[-1+i].couleur);
 if not( listeecran[-1+i].create(listeecran[-1+i].a1x,listeecran[-1+i].a1y,
  listeecran[-1+i].a2x,listeecran[-1+i].a2y,listeecran[-1+i].epaisseur,
  listeecran[-1+i].couleur))then
  raise einouterror.Create('erreur');

  end;
    goto 12345; end;


 if s1='Nombre de diaphragmes:' then begin
  readln(f,newnombrediaphragme);
  retaillepdiaphragme(listediaphragme,nombrediaphragme,newnombrediaphragme);
  nombrediaphragme:=newnombrediaphragme;
  if nombrediaphragme>0 then
     for i:=1 to nombrediaphragme do begin
     readln(f,s);
  readln(f,listediaphragme[-1+i].cx,listediaphragme[-1+i].cy,listediaphragme[-1+i].anglepolaire,
  listediaphragme[-1+i].rint,listediaphragme[-1+i].rext,
  listediaphragme[-1+i].epaisseur,listediaphragme[-1+i].couleur);
 if not( listediaphragme[-1+i].create2(listediaphragme[-1+i].cx,
 listediaphragme[-1+i].cy,listediaphragme[-1+i].anglepolaire,
  listediaphragme[-1+i].rint,listediaphragme[-1+i].rext,
  listediaphragme[-1+i].couleur,listediaphragme[-1+i].epaisseur))then
  raise einouterror.Create('erreur');

  end;
   goto 12345; end;



  if s1='Nombre de lames semi reflechissantes:' then begin
  readln(f,newnombrelsr);
  retailleplsr(listelsr,nombrelsr,newnombrelsr);
  nombrelsr:=newnombrelsr;
  if nombrelsr>0 then
     for i:=1 to nombrelsr do begin
     readln(f,s);
  readln(f,listelsr[-1+i].a1x,listelsr[-1+i].a1y,listelsr[-1+i].a2x,
  listelsr[-1+i].a2y,listelsr[-1+i].epaisseur,listelsr[-1+i].couleur);
 if not( listelsr[-1+i].create(listelsr[-1+i].a1x,listelsr[-1+i].a1y,
  listelsr[-1+i].a2x,listelsr[-1+i].a2y,listelsr[-1+i].epaisseur,
  listelsr[-1+i].couleur))then
  raise einouterror.Create('erreur');
  end;
   goto 12345; end;


     if s1='Nombre de polyhedres:' then begin
   readln(f,newnombrepolyhedre);
   retailleppolyhedre(listepolyhedre,nombrepolyhedre,newnombrepolyhedre);
  nombrepolyhedre:=newnombrepolyhedre;
  if nombrepolyhedre>0 then
  for i:=1 to nombrepolyhedre do begin
  readln(f,s);
  readln(f,listepolyhedre[-1+i].nombresommet);
  for j:=1 to listepolyhedre[-1+i].nombresommet do
  readln(f,listepolyhedre[-1+i].messommets[j].ax,listepolyhedre[-1+i].messommets[j].ay);
  readln(f,listepolyhedre[-1+i].indicerouge,listepolyhedre[-1+i].indicebleu,listepolyhedre[-1+i].indicevert);
  readln(f,listepolyhedre[-1+i].couleurbord,listepolyhedre[-1+i].couleurfond);
  readln(f,s);
  if s='1' then  listepolyhedre[-1+i].reflechientrant:=toujoursreflechi;
   if s='2' then  listepolyhedre[-1+i].reflechientrant:=jamaisreflechi;
   if s='3' then  listepolyhedre[-1+i].reflechientrant:=reflechisirefracte;
   if s='4' then  listepolyhedre[-1+i].reflechientrant:=reflechisipasrefracte;
  readln(f,s);
  if s='1' then  listepolyhedre[-1+i].reflechisortant:=toujoursreflechi;
   if s='2' then  listepolyhedre[-1+i].reflechisortant:=jamaisreflechi;
   if s='3' then  listepolyhedre[-1+i].reflechisortant:=reflechisirefracte;
   if s='4' then  listepolyhedre[-1+i].reflechisortant:=reflechisipasrefracte;
   if not(listepolyhedre[-1+i].create(listepolyhedre[-1+i].nombresommet,listepolyhedre[-1+i].messommets,
   listepolyhedre[-1+i].indicerouge,listepolyhedre[-1+i].indicebleu,listepolyhedre[-1+i].indicevert,
 listepolyhedre[-1+i].couleurbord,listepolyhedre[-1+i].couleurfond,
  listepolyhedre[-1+i].reflechientrant,listepolyhedre[-1+i].reflechisortant))then
  raise einouterror.Create('erreur');
  end;
   goto 12345; end;




   if s1='Nombre de prismes:' then begin
   readln(f,newnombreprisme);
   retaillepprisme(listeprisme,nombreprisme,newnombreprisme);
  nombreprisme:=newnombreprisme;
  if nombreprisme>0 then
  for i:=1 to nombreprisme do begin
  readln(f,s);
  readln(f,listeprisme[-1+i].gx,listeprisme[-1+i].gy);
   readln(f,listeprisme[-1+i].ax,listeprisme[-1+i].ay);
    readln(f,listeprisme[-1+i].bx,listeprisme[-1+i].by);
  readln(f,listeprisme[-1+i].indicerouge,listeprisme[-1+i].indicebleu,listeprisme[-1+i].indicevert);
  readln(f,listeprisme[-1+i].couleurbord,listeprisme[-1+i].couleurfond);
  readln(f,s);
  if s='1' then  listeprisme[-1+i].reflechientrant:=toujoursreflechi;
   if s='2' then  listeprisme[-1+i].reflechientrant:=jamaisreflechi;
   if s='3' then  listeprisme[-1+i].reflechientrant:=reflechisirefracte;
   if s='4' then  listeprisme[-1+i].reflechientrant:=reflechisipasrefracte;
  readln(f,s);
  if s='1' then  listeprisme[-1+i].reflechisortant:=toujoursreflechi;
   if s='2' then  listeprisme[-1+i].reflechisortant:=jamaisreflechi;
   if s='3' then  listeprisme[-1+i].reflechisortant:=reflechisirefracte;
   if s='4' then  listeprisme[-1+i].reflechisortant:=reflechisipasrefracte;
   if not(listeprisme[-1+i].create(listeprisme[-1+i].gx,listeprisme[-1+i].gy,
   listeprisme[-1+i].ax,listeprisme[-1+i].ay,listeprisme[-1+i].bx,listeprisme[-1+i].by,
   listeprisme[-1+i].indicerouge,listeprisme[-1+i].indicebleu,listeprisme[-1+i].indicevert,
 listeprisme[-1+i].couleurbord,listeprisme[-1+i].couleurfond,
  listeprisme[-1+i].reflechientrant,listeprisme[-1+i].reflechisortant))then
  raise einouterror.Create('erreur');
  end;
   goto 12345; end;




 if s1='Nombre de polycercles:' then begin
  readln(f,newnombrepolycercle);
   retailleppolycercle(listepolycercle,nombrepolycercle,newnombrepolycercle);
  nombrepolycercle:=newnombrepolycercle;
  if nombrepolycercle>0 then
  for i:=1 to nombrepolycercle do begin
  readln(f,s);
  readln(f,listepolycercle[-1+i].nombresommet);
  for j:=1 to listepolycercle[-1+i].nombresommet do
  readln(f,listepolycercle[-1+i].messommets[j].ax,listepolycercle[-1+i].messommets[j].ay);
   for j:=1 to listepolycercle[-1+i].nombresommet do
  readln(f,listepolycercle[-1+i].mess[j].ax,listepolycercle[-1+i].mess[j].ay);
   for j:=1 to listepolycercle[-1+i].nombresommet do  begin
   readln(f,s);
  listepolycercle[-1+i].rectiligne[j]:=(s='TRUE');
  end;
  readln(f,listepolycercle[-1+i].indicerouge,listepolycercle[-1+i].indicebleu,listepolycercle[-1+i].indicevert);
  readln(f,listepolycercle[-1+i].couleurbord,listepolycercle[-1+i].couleurfond);
  readln(f,s);
  if s='1' then  listepolycercle[-1+i].reflechientrant:=toujoursreflechi;
   if s='2' then  listepolycercle[-1+i].reflechientrant:=jamaisreflechi;
   if s='3' then  listepolycercle[-1+i].reflechientrant:=reflechisirefracte;
   if s='4' then  listepolycercle[-1+i].reflechientrant:=reflechisipasrefracte;
  readln(f,s);
  if s='1' then  listepolycercle[-1+i].reflechisortant:=toujoursreflechi;
   if s='2' then  listepolycercle[-1+i].reflechisortant:=jamaisreflechi;
   if s='3' then  listepolycercle[-1+i].reflechisortant:=reflechisirefracte;
   if s='4' then  listepolycercle[-1+i].reflechisortant:=reflechisipasrefracte;
   if not(listepolycercle[-1+i].create(listepolycercle[-1+i].nombresommet,
   listepolycercle[-1+i].messommets,listepolycercle[-1+i].mess,listepolycercle[-1+i].rectiligne,
   listepolycercle[-1+i].indicerouge,listepolycercle[-1+i].indicebleu,listepolycercle[-1+i].indicevert,
 listepolycercle[-1+i].couleurbord,listepolycercle[-1+i].couleurfond,
  listepolycercle[-1+i].reflechientrant,listepolycercle[-1+i].reflechisortant))then
  raise einouterror.Create('erreur');
  end;
    goto 12345; end;




  if s1='Nombre de spheres:' then begin
  readln(f,newnombresphere);
  retaillepsphere(listesphere,nombresphere,newnombresphere);
  nombresphere:=newnombresphere;
  if nombresphere>0 then
     for i:=1 to nombresphere do begin
     readln(f,s);
  readln(f,listesphere[-1+i].cx,listesphere[-1+i].cy,listesphere[-1+i].rayon,
  listesphere[-1+i].indicerouge,listesphere[-1+i].indicebleu,listesphere[-1+i].indicevert);
   readln(f,listesphere[-1+i].couleurbord,listesphere[-1+i].couleurfond);
   readln(f,s);
  if s='1' then  listesphere[-1+i].reflechientrant:=toujoursreflechi;
   if s='2' then  listesphere[-1+i].reflechientrant:=jamaisreflechi;
   if s='3' then  listesphere[-1+i].reflechientrant:=reflechisirefracte;
   if s='4' then  listesphere[-1+i].reflechientrant:=reflechisipasrefracte;
  readln(f,s);
  if s='1' then  listesphere[-1+i].reflechisortant:=toujoursreflechi;
   if s='2' then  listesphere[-1+i].reflechisortant:=jamaisreflechi;
   if s='3' then  listesphere[-1+i].reflechisortant:=reflechisirefracte;
   if s='4' then  listesphere[-1+i].reflechisortant:=reflechisipasrefracte;
 if not(listesphere [-1+i].create(listesphere[-1+i].cx,listesphere[-1+i].cy,listesphere[-1+i].rayon,
   listesphere[-1+i].indicerouge,listesphere[-1+i].indicebleu,listesphere[-1+i].indicevert,
 listesphere[-1+i].couleurbord,listesphere[-1+i].couleurfond,
  listesphere[-1+i].reflechientrant,listesphere[-1+i].reflechisortant))then
  raise einouterror.Create('erreur');
    end;
     goto 12345; end;




    if s1='Nombre de miroirs coniques:' then begin
    readln(f,newnombremiroirconique);
    retaillepmiroirconique(listemiroirconique,nombremiroirconique,newnombremiroirconique);
    nombremiroirconique:=newnombremiroirconique;
    if nombremiroirconique>0 then
    for i:=1 to nombremiroirconique do begin
    readln(f,s);
    readln(f,listemiroirconique[-1+i].fx,listemiroirconique[-1+i].fy,
    listemiroirconique[-1+i].excentricite,listemiroirconique[-1+i].parametre,
    listemiroirconique[-1+i].theta0,listemiroirconique[-1+i].theta1,listemiroirconique[-1+i].theta2);
    readln(f,listemiroirconique[-1+i].couleur,listemiroirconique[-1+i].couleuraxe);
    readln(f,s);
  listemiroirconique[-1+i].hachures:=(s='TRUE');
  readln(f,s);
  listemiroirconique[-1+i].axefocal:=(s='TRUE');
  readln(f,s);
  listemiroirconique[-1+i].concave:=(s='TRUE');
 if not( listemiroirconique[-1+i].create2(listemiroirconique[-1+i].fx,
  listemiroirconique[-1+i].fy,listemiroirconique[-1+i].theta0,
  listemiroirconique[-1+i].theta1,listemiroirconique[-1+i].theta2,
  listemiroirconique[-1+i].excentricite,listemiroirconique[-1+i].parametre,
  listemiroirconique[-1+i].concave,listemiroirconique[-1+i].couleur,
  listemiroirconique[-1+i].couleuraxe,listemiroirconique[-1+i].hachures,
  listemiroirconique[-1+i].axefocal)) then
  raise einouterror.Create('erreur');
   end;
    goto 12345; end;





   if s1='Nombre de textes:' then begin
   readln(f,newnombretexte);
   retailleptexteaffiche(listetexteaffiche,nombretexte,newnombretexte);
   nombretexte:=newnombretexte;
  if nombretexte>0 then

   for i:=1 to nombretexte do begin
   readln(f,s);
  readln(f,listetexteaffiche[-1+i].texteluimeme);
  readln(f,listetexteaffiche[-1+i].x,listetexteaffiche[-1+i].y);

   readln(f,listetexteaffiche[-1+i].policecharset,
    listetexteaffiche[-1+i].policecolor);
   readln(f,listetexteaffiche[-1+i].policename);
   readln(f,listetexteaffiche[-1+i].policesize);

   readln(f,s);
   listetexteaffiche[-1+i].policefsbold:=(s='fsbold');

   readln(f,s);
   listetexteaffiche[-1+i].policefsitalic:=(s='fsitalic');

   readln(f,s);
   listetexteaffiche[-1+i].policefsunderline:=(s='fsunderline');

   readln(f,s);
   listetexteaffiche[-1+i].policefsstrikeout:=(s='fsstrikeout');
   readln(f,s);
   if s='fpDefault' then listetexteaffiche[-1+i].policepitch:=fpDefault;
   if s='fpVariable' then listetexteaffiche[-1+i].policepitch:=fpVariable;
   if s='fpFixed'   then listetexteaffiche[-1+i].policepitch:=fpFixed;

if not(listetexteaffiche[-1+i].create(listetexteaffiche[-1+i].texteluimeme,
listetexteaffiche[-1+i].policename,listetexteaffiche[-1+i].policecharset,
listetexteaffiche[-1+i].policecolor,
listetexteaffiche[-1+i].policesize,
listetexteaffiche[-1+i].policepitch,
listetexteaffiche[-1+i].policefsbold,
listetexteaffiche[-1+i].policefsitalic,
listetexteaffiche[-1+i].policefsunderline,
listetexteaffiche[-1+i].policefsstrikeout,listetexteaffiche[-1+i].x,
listetexteaffiche[-1+i].y)) then
  raise einouterror.Create('erreur');
   end;
     goto 12345; end;






   if s1='Nombre de lentilles epaisses:' then begin
   readln(f,newnombrelec);
  retailleplec(listelec,nombrelec,newnombrelec);
  nombrelec:=newnombrelec;
  if nombrelec>0 then
     for i:=1 to nombrelec do begin
     readln(f,s);
  readln(f,listelec[-1+i].s1x,listelec[-1+i].s1y,listelec[-1+i].s2x,
  listelec[-1+i].s2y,listelec[-1+i].r1,listelec[-1+i].r2,listelec[-1+i].diametre);
  readln(f,listelec[-1+i].couleurfond,listelec[-1+i].couleuraxe);
  readln(f,s);
  listelec[-1+i].axefocal:=(s='TRUE');
  readln(f,listelec[-1+i].indicerouge,listelec[-1+i].indicebleu,listelec[-1+i].indicevert);

   readln(f,s);
  if s='1' then  listelec[-1+i].reflechientrant:=toujoursreflechi;
   if s='2' then  listelec[-1+i].reflechientrant:=jamaisreflechi;
   if s='3' then  listelec[-1+i].reflechientrant:=reflechisirefracte;
   if s='4' then  listelec[-1+i].reflechientrant:=reflechisipasrefracte;
  readln(f,s);
  if s='1' then  listelec[-1+i].reflechisortant:=toujoursreflechi;
   if s='2' then  listelec[-1+i].reflechisortant:=jamaisreflechi;
   if s='3' then  listelec[-1+i].reflechisortant:=reflechisirefracte;
   if s='4' then  listelec[-1+i].reflechisortant:=reflechisipasrefracte;
 if not(listelec [-1+i].create(listelec[-1+i].s1x,listelec[-1+i].s1y,listelec[-1+i].s2x,
 listelec[-1+i].s2y,listelec[-1+i].r1,listelec[-1+i].r2,listelec[-1+i].diametre,
 listelec[-1+i].couleurfond,listelec[-1+i].couleuraxe,listelec[-1+i].axefocal,
   listelec[-1+i].indicerouge,listelec[-1+i].indicebleu,listelec[-1+i].indicevert,
    listelec[-1+i].reflechientrant,listelec[-1+i].reflechisortant))then
  raise einouterror.Create('erreur');
    end;
      goto 12345; end;



    if s1='Nombre d''angles:' then begin
    readln(f,newnombreangle);
  retaillepangle(listeangle,nombreangle,newnombreangle);
  nombreangle:=newnombreangle;
  if nombreangle>0 then
     for i:=1 to nombreangle do begin
     readln(f,s);
  readln(f,listeangle[-1+i].cx,listeangle[-1+i].cy,listeangle[-1+i].a1x,
  listeangle[-1+i].a1y,listeangle[-1+i].a2x,listeangle[-1+i].a2y);

 if not(listeangle [-1+i].create(listeangle[-1+i].cx,listeangle[-1+i].cy,listeangle[-1+i].a1x,
 listeangle[-1+i].a1y,listeangle[-1+i].a2x,listeangle[-1+i].a2y))then
  raise einouterror.Create('erreur');
    end;
      goto 12345; end;


if s1='Nombre de traits:' then begin
    readln(f,newnombrefleche);
  retaillepfleche(listefleche,nombrefleche,newnombrefleche);
  nombrefleche:=newnombrefleche;
  if nombrefleche>0 then
     for i:=1 to nombrefleche do begin
     readln(f,s);
    readln(f,listefleche[-1+i].a1x,listefleche[-1+i].a1y,listefleche[-1+i].a2x,listefleche[-1+i].a2y);
    readln(f,listefleche[-1+i].epaisseur,listefleche[-1+i].couleur);
    readln(f,s);
    if s='trien' then listefleche[-1+i].ext1:=trien;
     if s='tffleche' then listefleche[-1+i].ext1:=tffleche;
      if s='ttrait' then listefleche[-1+i].ext1:=ttrait;
       if s='trond' then listefleche[-1+i].ext1:=trond;
        if s='tcroix' then listefleche[-1+i].ext1:=tcroix;
         if s='tcroixx' then listefleche[-1+i].ext1:=tcroixx;
          if s='tdisque' then listefleche[-1+i].ext1:=tdisque;
     readln(f,s);
     if s='trien' then listefleche[-1+i].ext2:=trien;
     if s='tffleche' then listefleche[-1+i].ext2:=tffleche;
      if s='ttrait' then listefleche[-1+i].ext2:=ttrait;
       if s='trond' then listefleche[-1+i].ext2:=trond;
        if s='tcroix' then listefleche[-1+i].ext2:=tcroix;
         if s='tcroixx' then listefleche[-1+i].ext2:=tcroixx;
          if s='tdisque' then listefleche[-1+i].ext2:=tdisque;
          readln(f,s);
          if s='pssolid' then listefleche[-1+i].styletrait:=pssolid;
        if s='psdot' then listefleche[-1+i].styletrait:=psdot;
        if s='psdash' then listefleche[-1+i].styletrait:=psdash;
        if s='psdashdot' then listefleche[-1+i].styletrait:=psdashdot;
        if s='psdashdotdot' then listefleche[-1+i].styletrait:=psdashdotdot;
     readln(f,listefleche[-1+i].taille1,listefleche[-1+i].taille2);
     listefleche[-1+i].create(listefleche[-1+i].a1x,listefleche[-1+i].a1y,
     listefleche[-1+i].a2x,listefleche[-1+i].a2y,
     listefleche[-1+i].epaisseur,listefleche[-1+i].couleur,
     listefleche[-1+i].ext1,listefleche[-1+i].ext2,
     listefleche[-1+i].styletrait,
     listefleche[-1+i].taille1,listefleche[-1+i].taille2);
     end;
     goto 12345; end;





if s1='Nombre d''oeils:' then begin
 readln(f,newnombreoeil);
  retaillepoeil(listeoeil,nombreoeil,newnombreoeil);
  nombreoeil:=newnombreoeil;
  if nombreoeil>0 then
     for i:=1 to nombreoeil do begin
     readln(f,s);
  readln(f,listeoeil[-1+i].cx,listeoeil[-1+i].cy,listeoeil[-1+i].ax,
  listeoeil[-1+i].ay,listeoeil[-1+i].epaisseur,listeoeil[-1+i].couleur);
 if not( listeoeil[-1+i].create(listeoeil[-1+i].cx,listeoeil[-1+i].cy,
  listeoeil[-1+i].ax,listeoeil[-1+i].ay,listeoeil[-1+i].epaisseur,
  listeoeil[-1+i].couleur))then
  raise einouterror.Create('erreur');
      end;
    goto 12345; end;

     if s1='Nombre de mesures de distances' then begin
 readln(f,newnombremetre);
 retaillepmetre(listemetre,nombremetre,newnombremetre);
 nombremetre:=newnombremetre;
 if nombremetre>0 then for i:=1 to nombremetre do begin
 readln(f,s);
 readln(f,listemetre[-1+i].a1x, listemetre[-1+i].a1y,
 listemetre[-1+i].a2x,listemetre[-1+i].a2y);
 if not( listemetre[-1+i].create(listemetre[-1+i].a1x,listemetre[-1+i].a1y,
  listemetre[-1+i].a2x,listemetre[-1+i].a2y))then
  raise einouterror.Create('erreur');
      end;
       goto 12345; end;


   if s1='Nombre de groupes:' then begin
 readln(f,newnombregroupe);
 retaillepgroupe(listegroupe,nombregroupe,newnombregroupe);
 nombregroupe:=newnombregroupe;
 if nombregroupe> 0 then for i:=1 to nombregroupe do begin
 listegroupe[-1+i].create;
   readln(f,s); readln(f,chacha);
  for j:=1 to  chacha do begin
  readln(f,papa); readln(f,s);
      aze:=mdneant;
   if s='mdoeil' then aze:=mdoeil;
  if s='mddiaphragme' then aze:=mddiaphragme;
  if s='mdtexteaffiche'then aze:=mdtexteaffiche;
  if s='mdmiroirconique' then aze:=mdmiroirconique;
  if s='mdlec' then aze:=mdlec;
  if s='mdfleche' then aze:=mdfleche;
  if s='mdmiroirplan' then aze:=mdmiroirplan;
   if s='mdreseau' then aze:=mdreseau;
  if s='mdecran' then aze:=mdecran;
  if s='mdangle' then aze:=mdangle;
  if s='mdmetre' then aze:=mdmetre;
  if s='mdlsr' then aze:=mdlsr;
  if s='mdmscopa' then aze:=mdmscopa;
  if s='mdlmc' then aze:=mdlmc;
  if s='mdlmd' then aze:=mdlmd;
  if s='mdmscepa' then aze:=mdmscepa;
  if s='mdmscore' then aze:=mdmscore;
  if s='mdmscere' then aze:=mdmscere;
   if s='mdpolyhedre' then aze:=mdpolyhedre;
    if s='mdrayon' then aze:=mdrayon;
     if s='mdsourceponctuelle' then aze:=mdsourceponctuelle;
      if s='mdondeplane' then aze:=mdondeplane;
       if s='mdpolycercle' then aze:=mdpolycercle;
        if s='mdsphere' then aze:=mdsphere;
         if s='mdprisme' then aze:=mdprisme;
        listegroupe[-1+i].add(aze,papa);
         end; {de la boucle chacha}
                end; {de la boucle nombregroupe}
                goto 12345; end;

      

  if s1='couleur fond simulation' then begin
  readln(f,couleurfondsimulation);
    goto 12345; end;

    if s1='empennage' then begin
    readln(f,s);
    readln(f,positionempennage);
    readln(f,s);
     readln(f,tailleempennage);
     goto 12345; end;

     
 12345:
 end; {du while not(eof(f))}

   {gestion des erreurs de tout le bloc de lecture}
9852: except
application.messagebox(pchar(rsFichierEndom),
pchar(rsErreur), mb_ok);
 Nouveau1Click(sender);
end;

  closefile(f);

  if fileexistsutf8(copy(nerina,1,length(nerina)-3)+'txt') then
     nerina:=copy(nerina,1,length(nerina)-3)+'txt' else
    if fileexistsutf8(copy(nerina,1,length(nerina)-3)+'rtf') then
     nerina:=copy(nerina,1,length(nerina)-3)+'rtf' else
      nerina:=copy(nerina,1,length(nerina)-3)+'txt';

         try
         assignfile(f,(nerina));
         reset(f);
         closefile(f);

        formediteur.caption:=Format(rsCommentaires5, [copy(nerina, 1, length(
          nerina)-3)]);
  formediteur.show;
  formediteur.WindowState:=wsnormal;
  formediteur.fichierouvrir(nerina);
  if  formediteur.memo1.lines.count=0 then  formediteur.WindowState:=wsminimized;
  except

 formediteur.caption:=Format(rsCommentaires5, [copy(nerina, 1, length(nerina)-3)
   ]);
  formediteur.show;
  formediteur.WindowState:=wsminimized;
  formediteur.memo1.lines.Clear;
  end;

rapportactuel:=(yymax-yymin)/(xxmax-xxmin);
    afficherlagrille1.checked:=tracergrille;
    attractiondelagrille1.Checked:=attractiongrille;
    cochegrille.checked:=tracergrille;
    cocheattraction.Checked:=attractiongrille;
    N7.checked:=optionnormale; cochenormale.checked:=optionnormale;
    Afficherlesangles1.checked:=optionangles;
    cocheangles.checked:=optionangles;
  Rafraichit;
  fichierouvert:=true;
  modifie:=false;
  nomsauvegarde:=copy(nerina,1,length(nerina)-3)+'opt';
  {opendialog1.FileName;}
  form1.Caption:=Format(rsEspaceDeTrav4, [floattostr(xxmin), floattostr(xxmax),
    floattostr(yymin), floattostr(yymax), extractfilename(nomsauvegarde)]);
   nombre_etats_sauvegardes:=0;  niveau_max_sauvegarde:=0;
    commentaire_change:=false;
   sauvevirtuel;
    rtablir1.enabled:=false; refaire1.Enabled:=false;
end;







 procedure TForm1.SuperposeSimulation(Sender: TObject;repertoiredepart:string);
var f:textfile; i,j:integer;   s,s1:string;
newnombremiroirplan,newnombrereseau,newnombremscopa,newnombremscepa,newnombremscore,
newnombremscere,newnombrelmc,newnombrelmd,newnombrerayon,newnombrelsr,
newnombresourceponctuelle,newnombreondeplane,newnombreecran,toto,
newnombrepolyhedre,newnombresphere,newnombremiroirconique,
newnombretexte,newnombrediaphragme,newnombrelec,newnombrepolycercle,
newnombreangle,newnombrefleche,newnombreoeil,newnombregroupe,
newnombremetre,newnombreprisme:integer;
coco:tcolor;      aze:ttypeelementmodif;
chacha,papa:integer;    versionfichier:longint;
nerina:widestring;    newunitelongueur:shortstring;
 newxxmin,newxxmax,newyymin,newyymax,newgrillex,newgrilley:extended;
 ecraser_parametres_actuels:boolean;
 newpourcentagenormale:extended;
 newcouleurnormale:tcolor;
  newepaisseurnormale:integer;
   newdecimalesangles:integer;
   newlistefantomes:string;
     newmaxrayonsrecherchesurfacesonde:integer;
  newindice_vert_par_defaut,newindice_rouge_par_defaut,newindice_bleu_par_defaut:extended;
    newcouleurfondsimulation:tcolor;
    newpositionempennage:integer;
    newtailleempennage:integer;
 newlongueur_donde_red,newlongueur_donde_blue,newlongueur_donde_green:extended;
label 9852,789,12345;

begin
if modequelquechose then exit;

if nombrequelquechose=0 then begin
application.MessageBox(pchar(rsPasDeSimulat), pchar(rsHLas2), mb_ok);
exit;
end;

newnombremiroirplan:=0;newnombrereseau:=0;newnombremscopa:=0;newnombremscepa:=0;newnombremscore:=0;
newnombremscere:=0;newnombrelmc:=0;newnombrelmd:=0;newnombrerayon:=0;newnombrelsr:=0;
newnombresourceponctuelle:=0;newnombreondeplane:=0;newnombreecran:=0;
newnombrepolyhedre:=0;newnombresphere:=0;newnombremiroirconique:=0;
newnombretexte:=0;newnombrediaphragme:=0;newnombrelec:=0;newnombrepolycercle:=0;
newnombreangle:=0;newnombrefleche:=0;newnombreoeil:=0;newnombregroupe:=0;
newnombremetre:=0;newnombreprisme:=0;



OpenDialog1.FileName := '';
 opendialog1.InitialDir:=repertoiredepart;
if not(OpenDialog1.Execute) then exit
else nerina:=(OpenDialog1.FileName); { affiche la boîte de dialogue d'ouverture }
  if  ((pos(repertoire_exemples,extractfilepath(nerina))=0) and
  (pos(repertoire_simul_perso,extractfilepath(nerina))=0)) then
    repertoire_actuel:=extractfilepath(nerina);
    if nom_fichier_config_perso<>'' then begin
    try
if not(directoryexistsUTF8(repertoire_config_perso)) then
   forcedirectories((repertoire_config_perso));
assignfile(f,(nom_fichier_config_perso));
rewrite(f);
closefile(f);
saisiepreferences.BitBtn2Click(Sender);
except

end;
 end;


 789:   try

    Assignfile(F,(nerina));
    except
    Application.messagebox(pchar(rsFichierInacc), pchar(rsAttention), mb_ok);
    exit;
    end;

// application.MessageBox(pchar(nerina),pchar((nerina)),mb_ok);
   try
  reset(f);
  except
    Application.messagebox(pchar(rsFichierInacc2), pchar(rsAttention), mb_ok);
    exit;
    end;


ecraser_parametres_actuels:=application.MessageBox(pchar(rsLesParamTres),
 pchar(rsAttention), mb_yesno)=idyes;


    //metazero;
    versionfichier:=1;
    try  {suivi du bloc de lecture; si une erreur se produit, elle est traitee a la fin}
   while not(eof(f)) do begin
   readln(f,s1);
   if s1='NE PAS MODIFIER CE FICHIER' then goto  12345;

   if s1='VERSION' then begin
   readln(f,versionfichier);
   readln(f,newxxmin,newxxmax,newyymin,newyymax);
   if newxxmin<xxmin then xxmin:=newxxmin;
   if newxxmax>xxmax then xxmax:=newxxmax;
    if newyymin<yymin then yymin:=newyymin;
   if newyymax>yymax then yymax:=newyymax;
   readln(f,s);
   if ecraser_parametres_actuels then respectrapport:=(s='TRUE');
   goto 12345;
   end;



  if s1= 'UNITE' then begin
   readln(f,newunitelongueur);
   if ecraser_parametres_actuels then  unitelongueur:=newunitelongueur;
   goto 12345; end;

   if s1='OPTIONS GRILLE' then begin
    readln(f,s);
if ecraser_parametres_actuels then    tracergrille:=(s='TRUE');
    readln(f,s);
if ecraser_parametres_actuels then    attractiongrille:=(s='TRUE');
   readln(f,newgrillex,newgrilley);
   if ecraser_parametres_actuels then begin
    grillex:=newgrillex;
    grilley:=newgrilley;
   end;
   goto 12345; end;

     if s1= 'OPTIONS NORMALES' then begin
     readln(f,s);
 if ecraser_parametres_actuels then optionnormale:=(s='TRUE');
readln(f,s);
 if ecraser_parametres_actuels then optionangles:=(s='TRUE');
 readln(f,newpourcentagenormale);
 readln(f,newcouleurnormale);
 readln(f,newepaisseurnormale);
  readln(f,newdecimalesangles);
  if ecraser_parametres_actuels then
  begin
   pourcentagenormale:=newpourcentagenormale;
   couleurnormale:=newcouleurnormale;
   epaisseurnormale:=newepaisseurnormale;
   decimalesangles:=newdecimalesangles;
  end;
  goto 12345; end;

   if s1='OPTIONS SEGMENTS FANTOMES' then begin
  readln(f,s);
  if ecraser_parametres_actuels then fantomespresents:=(s='TRUE');
  readln(f,newlistefantomes);
  if ecraser_parametres_actuels then listefantomes:=newlistefantomes;
  goto 12345; end;

  if s1= 'NOMBRE POINTS TRACE SURFACE ONDE' then begin
      readln(f,newmaxrayonsrecherchesurfacesonde);
  if ecraser_parametres_actuels then maxrayonsrecherchesurfacesonde:=newmaxrayonsrecherchesurfacesonde;
        if fantomespresents then
for i:=1 to maxtotalsegment do
  fantomes[i]:=(pos(','+inttostr(i)+',',listefantomes)<>0) or (pos('('+inttostr(i)+',',listefantomes)<>0) or
  (pos(','+inttostr(i)+')',listefantomes)<>0) or
  (pos('('+inttostr(i)+')',listefantomes)<>0);

      goto 12345; end;

  if s1='INDICES PAR DEFAUT' then begin
    readln(f,newindice_vert_par_defaut);
    readln(f,newindice_bleu_par_defaut);
    readln(f,newindice_rouge_par_defaut);
     if ecraser_parametres_actuels then
     begin
    indice_vert_par_defaut:=newindice_vert_par_defaut;
    indice_bleu_par_defaut:=newindice_bleu_par_defaut;
    indice_rouge_par_defaut:=newindice_rouge_par_defaut;
     end;

    goto 12345;
   end;


   if s1='LAMBDA' then begin
    readln(f,newlongueur_donde_red);
    readln(f,newlongueur_donde_blue);
    readln(f,newlongueur_donde_green);
     if ecraser_parametres_actuels then
     begin
    longueur_donde_red:=newlongueur_donde_red;
    longueur_donde_blue:=newlongueur_donde_blue;
    longueur_donde_green:=newlongueur_donde_green;
     end;
    goto 12345;
   end;





  if s1='Nombre de miroirs plans:' then begin
  readln(f,newnombremiroirplan);
  retaillepmiroirplan(listemiroirplan,nombremiroirplan,nombremiroirplan+newnombremiroirplan);
  nombremiroirplan:=nombremiroirplan+newnombremiroirplan;
  if newnombremiroirplan>0 then
     for i:=nombremiroirplan-newnombremiroirplan+1 to nombremiroirplan do begin
     readln(f,s);
  readln(f,listemiroirplan[-1+i].a1x,listemiroirplan[-1+i].a1y,listemiroirplan[-1+i].a2x,
  listemiroirplan[-1+i].a2y,listemiroirplan[-1+i].epaisseur,listemiroirplan[-1+i].couleur);
  readln(f,s);
  listemiroirplan[-1+i].hachures:=(s='TRUE');
 if not( listemiroirplan[-1+i].create(listemiroirplan[-1+i].a1x,listemiroirplan[-1+i].a1y,
  listemiroirplan[-1+i].a2x,listemiroirplan[-1+i].a2y,listemiroirplan[-1+i].epaisseur,
  listemiroirplan[-1+i].couleur,listemiroirplan[-1+i].hachures)) then
  raise einouterror.Create('erreur');
  end;
  goto 12345; end;

   if s1='Nombre de reseaux:' then begin
  readln(f,newnombreReseau);
  retaillepReseau(listeReseau,nombreReseau,nombreReseau+newnombreReseau);
  nombreReseau:=nombreReseau+newnombreReseau;
  if newnombreReseau>0 then
     for i:=nombreReseau-newnombreReseau+1 to nombreReseau do begin
     readln(f,s);
  readln(f,listeReseau[-1+i].a1x,listeReseau[-1+i].a1y,listeReseau[-1+i].a2x,
  listeReseau[-1+i].a2y);
  readln(f,listeReseau[-1+i].epaisseur);
  readln(f,listeReseau[-1+i].couleur);
  readln(f,listeReseau[-1+i].pas);
  readln(f,s);
  listeReseau[-1+i].entransmission:=(s='TRUE');
  readln(f,s);
  listeReseau[-1+i].hachures:=(s='TRUE');
   readln(f,listeReseau[-1+i].ordre_min);
   readln(f,listeReseau[-1+i].ordre_max);
 if not( listeReseau[-1+i].create(listeReseau[-1+i].a1x,listeReseau[-1+i].a1y,
  listeReseau[-1+i].a2x,listeReseau[-1+i].a2y,listeReseau[-1+i].epaisseur,
  listeReseau[-1+i].couleur,listeReseau[-1+i].pas,listeReseau[-1+i].entransmission,listeReseau[-1+i].hachures,
  listeReseau[-1+i].ordre_min,listeReseau[-1+i].ordre_max)) then
  raise einouterror.Create('erreur');
  end;
  goto 12345; end;




  if s1='Nombre de miroirs concaves paraxiaux:' then begin
   readln(f,newnombremscopa);
  retaillepmscopa(listemscopa,nombremscopa,nombremscopa+newnombremscopa);
  nombremscopa:=nombremscopa+newnombremscopa;
  if newnombremscopa>0 then
     for i:=nombremscopa-newnombremscopa+1 to nombremscopa do begin
     readln(f,s);
  readln(f,listemscopa[-1+i].a1x,listemscopa[-1+i].a1y,listemscopa[-1+i].a2x,
  listemscopa[-1+i].a2y,listemscopa[-1+i].focale,listemscopa[-1+i].epaisseur,
  listemscopa[-1+i].couleur,listemscopa[-1+i].couleuraxe);
  readln(f,s);
  listemscopa[-1+i].hachures:=(s='TRUE');
  readln(f,s);
  listemscopa[-1+i].axefocal:=(s='TRUE');
  readln(f,s);
  listemscopa[-1+i].trou:=(s='TRUE');
   readln(f,listemscopa[-1+i].diametretrou);
 if not( listemscopa[-1+i].create2(listemscopa[-1+i].a1x,listemscopa[-1+i].a1y,
  listemscopa[-1+i].a2x,listemscopa[-1+i].a2y,listemscopa[-1+i].focale,
  listemscopa[-1+i].epaisseur,
  listemscopa[-1+i].couleur,listemscopa[-1+i].couleuraxe,
  listemscopa[-1+i].hachures,listemscopa[-1+i].axefocal,
  listemscopa[-1+i].trou,listemscopa[-1+i].diametretrou))then
  raise einouterror.Create('erreur');
  end;
    goto 12345; end;

    if s1='Nombre de miroirs convexes paraxiaux:'
  then begin
   readln(f,newnombremscepa);
  retaillepmscepa(listemscepa,nombremscepa,nombremscepa+newnombremscepa);
  nombremscepa:=nombremscepa+newnombremscepa;
  if newnombremscepa>0 then
     for i:=nombremscepa-newnombremscepa+1 to nombremscepa do begin
     readln(f,s);
  readln(f,listemscepa[-1+i].a1x,listemscepa[-1+i].a1y,listemscepa[-1+i].a2x,
  listemscepa[-1+i].a2y,listemscepa[-1+i].focale,listemscepa[-1+i].epaisseur,
  listemscepa[-1+i].couleur,listemscepa[-1+i].couleuraxe);
  readln(f,s);
  listemscepa[-1+i].hachures:=(s='TRUE');
  readln(f,s);
  listemscepa[-1+i].axefocal:=(s='TRUE');
   readln(f,s);
  listemscepa[-1+i].trou:=(s='TRUE');
  readln(f,listemscepa[-1+i].diametretrou);
 if not( listemscepa[-1+i].create2(listemscepa[-1+i].a1x,listemscepa[-1+i].a1y,
  listemscepa[-1+i].a2x,listemscepa[-1+i].a2y,listemscepa[-1+i].focale,
  listemscepa[-1+i].epaisseur,
  listemscepa[-1+i].couleur,listemscepa[-1+i].couleuraxe,
  listemscepa[-1+i].hachures,listemscepa[-1+i].axefocal,
  listemscepa[-1+i].trou,listemscepa[-1+i].diametretrou))then
  raise einouterror.Create('erreur');
  end;
   goto 12345; end;

   if s1='Nombre de miroirs concaves reels:' then begin
  readln(f,newnombremscore);
  retaillepmscore(listemscore,nombremscore,nombremscore+newnombremscore);
  nombremscore:=nombremscore+newnombremscore;
  if newnombremscore>0 then
     for i:=nombremscore-newnombremscore+1 to nombremscore do begin
     readln(f,s);
  readln(f,listemscore[-1+i].a1x,listemscore[-1+i].a1y,listemscore[-1+i].a2x,
  listemscore[-1+i].a2y,listemscore[-1+i].rayoncourbure,listemscore[-1+i].epaisseur,
  listemscore[-1+i].couleur,listemscore[-1+i].couleuraxe);
  readln(f,s);
  listemscore[-1+i].hachures:=(s='TRUE');
  readln(f,s);
  listemscore[-1+i].axefocal:=(s='TRUE');
  readln(f,s);
  listemscore[-1+i].aigu:=(s='TRUE');
  readln(f,s);
  listemscore[-1+i].trou:=(s='TRUE');
  readln(f,listemscore[-1+i].diametretrou);
if not(  listemscore[-1+i].create2(listemscore[-1+i].a1x,listemscore[-1+i].a1y,
  listemscore[-1+i].a2x,listemscore[-1+i].a2y,listemscore[-1+i].rayoncourbure,
  listemscore[-1+i].epaisseur,
  listemscore[-1+i].couleur,listemscore[-1+i].couleuraxe,
  listemscore[-1+i].hachures,listemscore[-1+i].axefocal,listemscore[-1+i].aigu,
  listemscore[-1+i].trou,listemscore[-1+i].diametretrou))then
  raise einouterror.Create('erreur');
  end;
   goto 12345; end;

   if s1='Nombre de miroirs convexes reels:' then begin
    readln(f,newnombremscere);
  retaillepmscere(listemscere,nombremscere,nombremscere+newnombremscere);
  nombremscere:=nombremscere+newnombremscere;
  if newnombremscere>0 then
     for i:=nombremscere-newnombremscere+1 to nombremscere do begin
     readln(f,s);
  readln(f,listemscere[-1+i].a1x,listemscere[-1+i].a1y,listemscere[-1+i].a2x,
  listemscere[-1+i].a2y,listemscere[-1+i].rayoncourbure,listemscere[-1+i].epaisseur,
  listemscere[-1+i].couleur,listemscere[-1+i].couleuraxe);
  readln(f,s);
  listemscere[-1+i].hachures:=(s='TRUE');
  readln(f,s);
  listemscere[-1+i].axefocal:=(s='TRUE');
  readln(f,s);
  listemscere[-1+i].aigu:=(s='TRUE');
  readln(f,s);
  listemscere[-1+i].trou:=(s='TRUE');
  readln(f,listemscere[-1+i].diametretrou);
 if not( listemscere[-1+i].create2(listemscere[-1+i].a1x,listemscere[-1+i].a1y,
  listemscere[-1+i].a2x,listemscere[-1+i].a2y,listemscere[-1+i].rayoncourbure,
  listemscere[-1+i].epaisseur,
  listemscere[-1+i].couleur,listemscere[-1+i].couleuraxe,
  listemscere[-1+i].hachures,listemscere[-1+i].axefocal,listemscere[-1+i].aigu,
  listemscere[-1+i].trou,listemscere[-1+i].diametretrou))then
  raise einouterror.Create('erreur');
  end;
  goto 12345; end;

   if s1='Nombre de lentiles minces convergentes:' then begin
   readln(f,newnombrelmc);
  retailleplmc(listelmc,nombrelmc,nombrelmc+newnombrelmc);
  nombrelmc:=nombrelmc+newnombrelmc;
  if newnombrelmc>0 then
     for i:=nombrelmc-newnombrelmc+1 to nombrelmc do begin
     readln(f,s);
  readln(f,listelmc[-1+i].a1x,listelmc[-1+i].a1y,listelmc[-1+i].a2x,
  listelmc[-1+i].a2y,listelmc[-1+i].focale,listelmc[-1+i].epaisseur,
  listelmc[-1+i].couleur,listelmc[-1+i].couleuraxe);
  readln(f,s);
  listelmc[-1+i].axefocal:=(s='TRUE');
  if not(listelmc[-1+i].create2(listelmc[-1+i].a1x,listelmc[-1+i].a1y,
  listelmc[-1+i].a2x,listelmc[-1+i].a2y,listelmc[-1+i].focale,
  listelmc[-1+i].epaisseur,
  listelmc[-1+i].couleur,listelmc[-1+i].couleuraxe,
  listelmc[-1+i].axefocal))then
  raise einouterror.Create('erreur');
  end;
  goto 12345; end;

  if s1='Nombre de lentiles minces divergentes:' then begin
  readln(f,newnombrelmd);
  retailleplmd(listelmd,nombrelmd,nombrelmd+newnombrelmd);
  nombrelmd:=nombrelmd+newnombrelmd;
  if newnombrelmd>0 then
     for i:=nombrelmd-newnombrelmd+1 to nombrelmd do begin
     readln(f,s);
  readln(f,listelmd[-1+i].a1x,listelmd[-1+i].a1y,listelmd[-1+i].a2x,
  listelmd[-1+i].a2y,listelmd[-1+i].focale,listelmd[-1+i].epaisseur,
  listelmd[-1+i].couleur,listelmd[-1+i].couleuraxe);
  readln(f,s);
  listelmd[-1+i].axefocal:=(s='TRUE');
 if not( listelmd[-1+i].create2(listelmd[-1+i].a1x,listelmd[-1+i].a1y,
  listelmd[-1+i].a2x,listelmd[-1+i].a2y,listelmd[-1+i].focale,
  listelmd[-1+i].epaisseur,
  listelmd[-1+i].couleur,listelmd[-1+i].couleuraxe,
  listelmd[-1+i].axefocal))then
  raise einouterror.Create('erreur');
  end;
   goto 12345; end;

   if s1='Nombre de rayons:' then begin
    readln(f,newnombrerayon);
  retailleprayon(listerayon,nombrerayon,nombrerayon+newnombrerayon);
  nombrerayon:=nombrerayon+newnombrerayon;
  if newnombrerayon>0 then
     for i:=nombrerayon-newnombrerayon+1 to nombrerayon do begin
     readln(f,s);
  readln(f,listerayon[-1+i].ax,listerayon[-1+i].ay,listerayon[-1+i].kx,
  listerayon[-1+i].ky,listerayon[-1+i].couleur,listerayon[-1+i].epaisseur,
  listerayon[-1+i].nombremaxenfant);
  readln(f,s);
  listerayon[-1+i].vav:=s;
   readln(f,s);
  listerayon[-1+i].vaa:=s;
  if versionfichier>=200 then begin
  readln(f,s);
   readln(f,listerayon[-1+i].maxsegment);
   end else
   listerayon[-1+i].maxsegment:=maxmaxsegment;
if not(  listerayon[-1+i].create(listerayon[-1+i].ax,listerayon[-1+i].ay,
  listerayon[-1+i].kx,listerayon[-1+i].ky, listerayon[-1+i].couleur,
  listerayon[-1+i].epaisseur,
  listerayon[-1+i].vav,listerayon[-1+i].vaa,
 listerayon[-1+i].nombremaxenfant,listerayon[-1+i].maxsegment  ))then
  raise einouterror.Create('erreur');
    end;
  goto 12345; end;



   if s1='Nombre de sources ponctuelles:' then begin
   readln(f,newnombresourceponctuelle);
  retaillepsourceponctuelle(listesourceponctuelle,nombresourceponctuelle,nombresourceponctuelle+newnombresourceponctuelle);
  nombresourceponctuelle:=nombresourceponctuelle+newnombresourceponctuelle;
  if newnombresourceponctuelle>0 then
  for i:=nombresourceponctuelle-newnombresourceponctuelle+1 to nombresourceponctuelle do begin
  readln(f,s);
  readln(f,listesourceponctuelle[-1+i].nombrederayon,
  listesourceponctuelle[-1+i].sx,
   listesourceponctuelle[-1+i].sy,
  listesourceponctuelle[-1+i].a1x,
  listesourceponctuelle[-1+i].a1y,
  listesourceponctuelle[-1+i].a2x,
  listesourceponctuelle[-1+i].a2y,
   listesourceponctuelle[-1+i].couleur,
   listesourceponctuelle[-1+i].epaisseur,
   listesourceponctuelle[-1+i].maxenfantparrayon);
   readln(f,s);
listesourceponctuelle[-1+i].vav:=s;
readln(f,s);
listesourceponctuelle[-1+i].vaa:=s;
readln(f,s);
listesourceponctuelle[-1+i].sourcevirtuelle:=(s='TRUE');
readln(f,s);
listesourceponctuelle[-1+i].tracersurfacesonde:=(s='TRUE');
readln(f,s);
listesourceponctuelle[-1+i].listechemins:=s;
if versionfichier>=200 then begin
 readln(f,s);
  readln(f,listesourceponctuelle[-1+i].maxsegment);
  end else
  listesourceponctuelle[-1+i].maxsegment:=maxmaxsegment;
if not(listesourceponctuelle[-1+i].create(listesourceponctuelle[-1+i].nombrederayon,
  listesourceponctuelle[-1+i].sx,
   listesourceponctuelle[-1+i].sy,
  listesourceponctuelle[-1+i].a1x,
  listesourceponctuelle[-1+i].a1y,
  listesourceponctuelle[-1+i].a2x,
  listesourceponctuelle[-1+i].a2y,
   listesourceponctuelle[-1+i].couleur,
   listesourceponctuelle[-1+i].epaisseur,listesourceponctuelle[-1+i].vav,
   listesourceponctuelle[-1+i].vaa,
   listesourceponctuelle[-1+i].maxenfantparrayon,listesourceponctuelle[-1+i].sourcevirtuelle,
   listesourceponctuelle[-1+i].tracersurfacesonde,listesourceponctuelle[-1+i].listechemins,
   listesourceponctuelle[-1+i].maxsegment))then
  raise einouterror.Create('erreur');
    end;
     goto 12345; end;


     if s1='Nombre d''ondes planes:' then begin
     readln(f,newnombreondeplane);
  retaillepondeplane(listeondeplane,nombreondeplane,nombreondeplane+newnombreondeplane);
  nombreondeplane:=nombreondeplane+newnombreondeplane;
  if newnombreondeplane>0 then
  for i:=nombreondeplane-newnombreondeplane+1 to nombreondeplane do begin
  readln(f,s);
  readln(f,listeondeplane[-1+i].nombrederayon,
  listeondeplane[-1+i].sx,
   listeondeplane[-1+i].sy,
  listeondeplane[-1+i].a1x,
  listeondeplane[-1+i].a1y,
  listeondeplane[-1+i].a2x,
  listeondeplane[-1+i].a2y,
   listeondeplane[-1+i].couleur,
   listeondeplane[-1+i].epaisseur,
   listeondeplane[-1+i].maxenfantparrayon);
   readln(f,s);
listeondeplane[-1+i].vav:=s;
 readln(f,s);
listeondeplane[-1+i].vaa:=s;
readln(f,s);
listeondeplane[-1+i].tracersurfacesonde:=(s='TRUE');
readln(f,s);
listeondeplane[-1+i].listechemins:=s;
if versionfichier>=200 then begin
 readln(f,s);
  readln(f,listeondeplane[-1+i].maxsegment);
  end else
  listeondeplane[-1+i].maxsegment:=maxmaxsegment;
if not(listeondeplane[-1+i].create(listeondeplane[-1+i].nombrederayon,
  listeondeplane[-1+i].sx,
   listeondeplane[-1+i].sy,
  listeondeplane[-1+i].a1x,
  listeondeplane[-1+i].a1y,
  listeondeplane[-1+i].a2x,
  listeondeplane[-1+i].a2y,
   listeondeplane[-1+i].couleur,
   listeondeplane[-1+i].epaisseur,listeondeplane[-1+i].vav,
   listeondeplane[-1+i].vaa,listeondeplane[-1+i].maxenfantparrayon,
   listeondeplane[-1+i].tracersurfacesonde,
   listeondeplane[-1+i].listechemins,
   listeondeplane[-1+i].maxsegment))then
  raise einouterror.Create('erreur');
end;
  goto 12345; end;


 if s1='Nombre d''ecrans:' then begin
 readln(f,newnombreecran);
  retaillepecran(listeecran,nombreecran,nombreecran+newnombreecran);
  nombreecran:=nombreecran+newnombreecran;
  if newnombreecran>0 then
     for i:=nombreecran-newnombreecran+1 to nombreecran do begin
     readln(f,s);
  readln(f,listeecran[-1+i].a1x,listeecran[-1+i].a1y,listeecran[-1+i].a2x,
  listeecran[-1+i].a2y,listeecran[-1+i].epaisseur,listeecran[-1+i].couleur);
 if not( listeecran[-1+i].create(listeecran[-1+i].a1x,listeecran[-1+i].a1y,
  listeecran[-1+i].a2x,listeecran[-1+i].a2y,listeecran[-1+i].epaisseur,
  listeecran[-1+i].couleur))then
  raise einouterror.Create('erreur');

  end;
    goto 12345; end;


 if s1='Nombre de diaphragmes:' then begin
  readln(f,newnombrediaphragme);
  retaillepdiaphragme(listediaphragme,nombrediaphragme,nombrediaphragme+newnombrediaphragme);
  nombrediaphragme:=nombrediaphragme+newnombrediaphragme;
  if newnombrediaphragme>0 then
     for i:=nombrediaphragme-newnombrediaphragme+1 to nombrediaphragme do begin
     readln(f,s);
  readln(f,listediaphragme[-1+i].cx,listediaphragme[-1+i].cy,listediaphragme[-1+i].anglepolaire,
  listediaphragme[-1+i].rint,listediaphragme[-1+i].rext,
  listediaphragme[-1+i].epaisseur,listediaphragme[-1+i].couleur);
 if not( listediaphragme[-1+i].create2(listediaphragme[-1+i].cx,
 listediaphragme[-1+i].cy,listediaphragme[-1+i].anglepolaire,
  listediaphragme[-1+i].rint,listediaphragme[-1+i].rext,
  listediaphragme[-1+i].couleur,listediaphragme[-1+i].epaisseur))then
  raise einouterror.Create('erreur');

  end;
   goto 12345; end;



  if s1='Nombre de lames semi reflechissantes:' then begin
  readln(f,newnombrelsr);
  retailleplsr(listelsr,nombrelsr,nombrelsr+newnombrelsr);
  nombrelsr:=nombrelsr+newnombrelsr;
  if newnombrelsr>0 then
     for i:=nombrelsr-newnombrelsr+1 to nombrelsr do begin
     readln(f,s);
  readln(f,listelsr[-1+i].a1x,listelsr[-1+i].a1y,listelsr[-1+i].a2x,
  listelsr[-1+i].a2y,listelsr[-1+i].epaisseur,listelsr[-1+i].couleur);
 if not( listelsr[-1+i].create(listelsr[-1+i].a1x,listelsr[-1+i].a1y,
  listelsr[-1+i].a2x,listelsr[-1+i].a2y,listelsr[-1+i].epaisseur,
  listelsr[-1+i].couleur))then
  raise einouterror.Create('erreur');
  end;
   goto 12345; end;


     if s1='Nombre de polyhedres:' then begin
   readln(f,newnombrepolyhedre);
   retailleppolyhedre(listepolyhedre,nombrepolyhedre,nombrepolyhedre+newnombrepolyhedre);
  nombrepolyhedre:=nombrepolyhedre+newnombrepolyhedre;
  if newnombrepolyhedre>0 then
  for i:=nombrepolyhedre-newnombrepolyhedre+1 to nombrepolyhedre do begin
  readln(f,s);
  readln(f,listepolyhedre[-1+i].nombresommet);
  for j:=1 to listepolyhedre[-1+i].nombresommet do
  readln(f,listepolyhedre[-1+i].messommets[j].ax,listepolyhedre[-1+i].messommets[j].ay);
  readln(f,listepolyhedre[-1+i].indicerouge,listepolyhedre[-1+i].indicebleu,listepolyhedre[-1+i].indicevert);
  readln(f,listepolyhedre[-1+i].couleurbord,listepolyhedre[-1+i].couleurfond);
  readln(f,s);
  if s='1' then  listepolyhedre[-1+i].reflechientrant:=toujoursreflechi;
   if s='2' then  listepolyhedre[-1+i].reflechientrant:=jamaisreflechi;
   if s='3' then  listepolyhedre[-1+i].reflechientrant:=reflechisirefracte;
   if s='4' then  listepolyhedre[-1+i].reflechientrant:=reflechisipasrefracte;
  readln(f,s);
  if s='1' then  listepolyhedre[-1+i].reflechisortant:=toujoursreflechi;
   if s='2' then  listepolyhedre[-1+i].reflechisortant:=jamaisreflechi;
   if s='3' then  listepolyhedre[-1+i].reflechisortant:=reflechisirefracte;
   if s='4' then  listepolyhedre[-1+i].reflechisortant:=reflechisipasrefracte;
   if not(listepolyhedre[-1+i].create(listepolyhedre[-1+i].nombresommet,listepolyhedre[-1+i].messommets,
   listepolyhedre[-1+i].indicerouge,listepolyhedre[-1+i].indicebleu,listepolyhedre[-1+i].indicevert,
 listepolyhedre[-1+i].couleurbord,listepolyhedre[-1+i].couleurfond,
  listepolyhedre[-1+i].reflechientrant,listepolyhedre[-1+i].reflechisortant))then
  raise einouterror.Create('erreur');
  end;
   goto 12345; end;




   if s1='Nombre de prismes:' then begin
   readln(f,newnombreprisme);
   retaillepprisme(listeprisme,nombreprisme,nombreprisme+newnombreprisme);
  nombreprisme:=nombreprisme+newnombreprisme;
  if newnombreprisme>0 then
  for i:=nombreprisme-newnombreprisme+1 to nombreprisme do begin
  readln(f,s);
  readln(f,listeprisme[-1+i].gx,listeprisme[-1+i].gy);
   readln(f,listeprisme[-1+i].ax,listeprisme[-1+i].ay);
    readln(f,listeprisme[-1+i].bx,listeprisme[-1+i].by);
  readln(f,listeprisme[-1+i].indicerouge,listeprisme[-1+i].indicebleu,listeprisme[-1+i].indicevert);
  readln(f,listeprisme[-1+i].couleurbord,listeprisme[-1+i].couleurfond);
  readln(f,s);
  if s='1' then  listeprisme[-1+i].reflechientrant:=toujoursreflechi;
   if s='2' then  listeprisme[-1+i].reflechientrant:=jamaisreflechi;
   if s='3' then  listeprisme[-1+i].reflechientrant:=reflechisirefracte;
   if s='4' then  listeprisme[-1+i].reflechientrant:=reflechisipasrefracte;
  readln(f,s);
  if s='1' then  listeprisme[-1+i].reflechisortant:=toujoursreflechi;
   if s='2' then  listeprisme[-1+i].reflechisortant:=jamaisreflechi;
   if s='3' then  listeprisme[-1+i].reflechisortant:=reflechisirefracte;
   if s='4' then  listeprisme[-1+i].reflechisortant:=reflechisipasrefracte;
   if not(listeprisme[-1+i].create(listeprisme[-1+i].gx,listeprisme[-1+i].gy,
   listeprisme[-1+i].ax,listeprisme[-1+i].ay,listeprisme[-1+i].bx,listeprisme[-1+i].by,
   listeprisme[-1+i].indicerouge,listeprisme[-1+i].indicebleu,listeprisme[-1+i].indicevert,
 listeprisme[-1+i].couleurbord,listeprisme[-1+i].couleurfond,
  listeprisme[-1+i].reflechientrant,listeprisme[-1+i].reflechisortant))then
  raise einouterror.Create('erreur');
  end;
   goto 12345; end;




 if s1='Nombre de polycercles:' then begin
  readln(f,newnombrepolycercle);
   retailleppolycercle(listepolycercle,nombrepolycercle,nombrepolycercle+newnombrepolycercle);
  nombrepolycercle:=nombrepolycercle+newnombrepolycercle;
  if newnombrepolycercle>0 then
  for i:=nombrepolycercle-newnombrepolycercle+1 to nombrepolycercle do begin
  readln(f,s);
  readln(f,listepolycercle[-1+i].nombresommet);
  for j:=1 to listepolycercle[-1+i].nombresommet do
  readln(f,listepolycercle[-1+i].messommets[j].ax,listepolycercle[-1+i].messommets[j].ay);
   for j:=1 to listepolycercle[-1+i].nombresommet do
  readln(f,listepolycercle[-1+i].mess[j].ax,listepolycercle[-1+i].mess[j].ay);
   for j:=1 to listepolycercle[-1+i].nombresommet do  begin
   readln(f,s);
  listepolycercle[-1+i].rectiligne[j]:=(s='TRUE');
  end;
  readln(f,listepolycercle[-1+i].indicerouge,listepolycercle[-1+i].indicebleu,listepolycercle[-1+i].indicevert);
  readln(f,listepolycercle[-1+i].couleurbord,listepolycercle[-1+i].couleurfond);
  readln(f,s);
  if s='1' then  listepolycercle[-1+i].reflechientrant:=toujoursreflechi;
   if s='2' then  listepolycercle[-1+i].reflechientrant:=jamaisreflechi;
   if s='3' then  listepolycercle[-1+i].reflechientrant:=reflechisirefracte;
   if s='4' then  listepolycercle[-1+i].reflechientrant:=reflechisipasrefracte;
  readln(f,s);
  if s='1' then  listepolycercle[-1+i].reflechisortant:=toujoursreflechi;
   if s='2' then  listepolycercle[-1+i].reflechisortant:=jamaisreflechi;
   if s='3' then  listepolycercle[-1+i].reflechisortant:=reflechisirefracte;
   if s='4' then  listepolycercle[-1+i].reflechisortant:=reflechisipasrefracte;
   if not(listepolycercle[-1+i].create(listepolycercle[-1+i].nombresommet,
   listepolycercle[-1+i].messommets,listepolycercle[-1+i].mess,listepolycercle[-1+i].rectiligne,
   listepolycercle[-1+i].indicerouge,listepolycercle[-1+i].indicebleu,listepolycercle[-1+i].indicevert,
 listepolycercle[-1+i].couleurbord,listepolycercle[-1+i].couleurfond,
  listepolycercle[-1+i].reflechientrant,listepolycercle[-1+i].reflechisortant))then
  raise einouterror.Create('erreur');
  end;
    goto 12345; end;




  if s1='Nombre de spheres:' then begin
  readln(f,newnombresphere);
  retaillepsphere(listesphere,nombresphere,nombresphere+newnombresphere);
  nombresphere:=nombresphere+newnombresphere;
  if newnombresphere>0 then
     for i:=nombresphere-newnombresphere+1 to nombresphere do begin
     readln(f,s);
  readln(f,listesphere[-1+i].cx,listesphere[-1+i].cy,listesphere[-1+i].rayon,
  listesphere[-1+i].indicerouge,listesphere[-1+i].indicebleu,listesphere[-1+i].indicevert);
   readln(f,listesphere[-1+i].couleurbord,listesphere[-1+i].couleurfond);
   readln(f,s);
  if s='1' then  listesphere[-1+i].reflechientrant:=toujoursreflechi;
   if s='2' then  listesphere[-1+i].reflechientrant:=jamaisreflechi;
   if s='3' then  listesphere[-1+i].reflechientrant:=reflechisirefracte;
   if s='4' then  listesphere[-1+i].reflechientrant:=reflechisipasrefracte;
  readln(f,s);
  if s='1' then  listesphere[-1+i].reflechisortant:=toujoursreflechi;
   if s='2' then  listesphere[-1+i].reflechisortant:=jamaisreflechi;
   if s='3' then  listesphere[-1+i].reflechisortant:=reflechisirefracte;
   if s='4' then  listesphere[-1+i].reflechisortant:=reflechisipasrefracte;
 if not(listesphere [-1+i].create(listesphere[-1+i].cx,listesphere[-1+i].cy,listesphere[-1+i].rayon,
   listesphere[-1+i].indicerouge,listesphere[-1+i].indicebleu,listesphere[-1+i].indicevert,
 listesphere[-1+i].couleurbord,listesphere[-1+i].couleurfond,
  listesphere[-1+i].reflechientrant,listesphere[-1+i].reflechisortant))then
  raise einouterror.Create('erreur');
    end;
     goto 12345; end;




    if s1='Nombre de miroirs coniques:' then begin
    readln(f,newnombremiroirconique);
    retaillepmiroirconique(listemiroirconique,nombremiroirconique,nombremiroirconique+newnombremiroirconique);
    nombremiroirconique:=nombremiroirconique+newnombremiroirconique;
    if newnombremiroirconique>0 then
    for i:=nombremiroirconique-newnombremiroirconique+1 to nombremiroirconique do begin
    readln(f,s);
    readln(f,listemiroirconique[-1+i].fx,listemiroirconique[-1+i].fy,
    listemiroirconique[-1+i].excentricite,listemiroirconique[-1+i].parametre,
    listemiroirconique[-1+i].theta0,listemiroirconique[-1+i].theta1,listemiroirconique[-1+i].theta2);
    readln(f,listemiroirconique[-1+i].couleur,listemiroirconique[-1+i].couleuraxe);
    readln(f,s);
  listemiroirconique[-1+i].hachures:=(s='TRUE');
  readln(f,s);
  listemiroirconique[-1+i].axefocal:=(s='TRUE');
  readln(f,s);
  listemiroirconique[-1+i].concave:=(s='TRUE');
 if not( listemiroirconique[-1+i].create2(listemiroirconique[-1+i].fx,
  listemiroirconique[-1+i].fy,listemiroirconique[-1+i].theta0,
  listemiroirconique[-1+i].theta1,listemiroirconique[-1+i].theta2,
  listemiroirconique[-1+i].excentricite,listemiroirconique[-1+i].parametre,
  listemiroirconique[-1+i].concave,listemiroirconique[-1+i].couleur,
  listemiroirconique[-1+i].couleuraxe,listemiroirconique[-1+i].hachures,
  listemiroirconique[-1+i].axefocal)) then
  raise einouterror.Create('erreur');
   end;
    goto 12345; end;





   if s1='Nombre de textes:' then begin
   readln(f,newnombretexte);
   retailleptexteaffiche(listetexteaffiche,nombretexte,nombretexte+newnombretexte);
   nombretexte:=nombretexte+newnombretexte;
  if newnombretexte>0 then

   for i:=nombretexte-newnombretexte+1 to nombretexte do begin
   readln(f,s);
  readln(f,listetexteaffiche[-1+i].texteluimeme);
  readln(f,listetexteaffiche[-1+i].x,listetexteaffiche[-1+i].y);

   readln(f,listetexteaffiche[-1+i].policecharset,
    listetexteaffiche[-1+i].policecolor);
   readln(f,listetexteaffiche[-1+i].policename);
   readln(f,listetexteaffiche[-1+i].policesize);

   readln(f,s);
   listetexteaffiche[-1+i].policefsbold:=(s='fsbold');

   readln(f,s);
   listetexteaffiche[-1+i].policefsitalic:=(s='fsitalic');

   readln(f,s);
   listetexteaffiche[-1+i].policefsunderline:=(s='fsunderline');

   readln(f,s);
   listetexteaffiche[-1+i].policefsstrikeout:=(s='fsstrikeout');
   readln(f,s);
   if s='fpDefault' then listetexteaffiche[-1+i].policepitch:=fpDefault;
   if s='fpVariable' then listetexteaffiche[-1+i].policepitch:=fpVariable;
   if s='fpFixed'   then listetexteaffiche[-1+i].policepitch:=fpFixed;

if not(listetexteaffiche[-1+i].create(listetexteaffiche[-1+i].texteluimeme,
listetexteaffiche[-1+i].policename,listetexteaffiche[-1+i].policecharset,
listetexteaffiche[-1+i].policecolor,
listetexteaffiche[-1+i].policesize,
listetexteaffiche[-1+i].policepitch,
listetexteaffiche[-1+i].policefsbold,
listetexteaffiche[-1+i].policefsitalic,
listetexteaffiche[-1+i].policefsunderline,
listetexteaffiche[-1+i].policefsstrikeout,listetexteaffiche[-1+i].x,
listetexteaffiche[-1+i].y)) then
  raise einouterror.Create('erreur');
   end;
     goto 12345; end;






   if s1='Nombre de lentilles epaisses:' then begin
   readln(f,newnombrelec);
  retailleplec(listelec,nombrelec,nombrelec+newnombrelec);
  nombrelec:=nombrelec+newnombrelec;
  if newnombrelec>0 then
     for i:=nombrelec-newnombrelec+1 to nombrelec do begin
     readln(f,s);
  readln(f,listelec[-1+i].s1x,listelec[-1+i].s1y,listelec[-1+i].s2x,
  listelec[-1+i].s2y,listelec[-1+i].r1,listelec[-1+i].r2,listelec[-1+i].diametre);
  readln(f,listelec[-1+i].couleurfond,listelec[-1+i].couleuraxe);
  readln(f,s);
  listelec[-1+i].axefocal:=(s='TRUE');
  readln(f,listelec[-1+i].indicerouge,listelec[-1+i].indicebleu,listelec[-1+i].indicevert);

   readln(f,s);
  if s='1' then  listelec[-1+i].reflechientrant:=toujoursreflechi;
   if s='2' then  listelec[-1+i].reflechientrant:=jamaisreflechi;
   if s='3' then  listelec[-1+i].reflechientrant:=reflechisirefracte;
   if s='4' then  listelec[-1+i].reflechientrant:=reflechisipasrefracte;
  readln(f,s);
  if s='1' then  listelec[-1+i].reflechisortant:=toujoursreflechi;
   if s='2' then  listelec[-1+i].reflechisortant:=jamaisreflechi;
   if s='3' then  listelec[-1+i].reflechisortant:=reflechisirefracte;
   if s='4' then  listelec[-1+i].reflechisortant:=reflechisipasrefracte;
 if not(listelec [-1+i].create(listelec[-1+i].s1x,listelec[-1+i].s1y,listelec[-1+i].s2x,
 listelec[-1+i].s2y,listelec[-1+i].r1,listelec[-1+i].r2,listelec[-1+i].diametre,
 listelec[-1+i].couleurfond,listelec[-1+i].couleuraxe,listelec[-1+i].axefocal,
   listelec[-1+i].indicerouge,listelec[-1+i].indicebleu,listelec[-1+i].indicevert,
    listelec[-1+i].reflechientrant,listelec[-1+i].reflechisortant))then
  raise einouterror.Create('erreur');
    end;
      goto 12345; end;



    if s1='Nombre d''angles:' then begin
    readln(f,newnombreangle);
  retaillepangle(listeangle,nombreangle,nombreangle+newnombreangle);
  nombreangle:=nombreangle+newnombreangle;
  if newnombreangle>0 then
     for i:=nombreangle-newnombreangle+1 to nombreangle do begin
     readln(f,s);
  readln(f,listeangle[-1+i].cx,listeangle[-1+i].cy,listeangle[-1+i].a1x,
  listeangle[-1+i].a1y,listeangle[-1+i].a2x,listeangle[-1+i].a2y);

 if not(listeangle [-1+i].create(listeangle[-1+i].cx,listeangle[-1+i].cy,listeangle[-1+i].a1x,
 listeangle[-1+i].a1y,listeangle[-1+i].a2x,listeangle[-1+i].a2y))then
  raise einouterror.Create('erreur');
    end;
      goto 12345; end;


if s1='Nombre de traits:' then begin
    readln(f,newnombrefleche);
  retaillepfleche(listefleche,nombrefleche,nombrefleche+newnombrefleche);
  nombrefleche:=nombrefleche+newnombrefleche;
  if newnombrefleche>0 then
     for i:=nombrefleche-newnombrefleche+1 to nombrefleche do begin
     readln(f,s);
    readln(f,listefleche[-1+i].a1x,listefleche[-1+i].a1y,listefleche[-1+i].a2x,listefleche[-1+i].a2y);
    readln(f,listefleche[-1+i].epaisseur,listefleche[-1+i].couleur);
    readln(f,s);
    if s='trien' then listefleche[-1+i].ext1:=trien;
     if s='tffleche' then listefleche[-1+i].ext1:=tffleche;
      if s='ttrait' then listefleche[-1+i].ext1:=ttrait;
       if s='trond' then listefleche[-1+i].ext1:=trond;
        if s='tcroix' then listefleche[-1+i].ext1:=tcroix;
         if s='tcroixx' then listefleche[-1+i].ext1:=tcroixx;
          if s='tdisque' then listefleche[-1+i].ext1:=tdisque;
     readln(f,s);
     if s='trien' then listefleche[-1+i].ext2:=trien;
     if s='tffleche' then listefleche[-1+i].ext2:=tffleche;
      if s='ttrait' then listefleche[-1+i].ext2:=ttrait;
       if s='trond' then listefleche[-1+i].ext2:=trond;
        if s='tcroix' then listefleche[-1+i].ext2:=tcroix;
         if s='tcroixx' then listefleche[-1+i].ext2:=tcroixx;
          if s='tdisque' then listefleche[-1+i].ext2:=tdisque;
          readln(f,s);
          if s='pssolid' then listefleche[-1+i].styletrait:=pssolid;
        if s='psdot' then listefleche[-1+i].styletrait:=psdot;
        if s='psdash' then listefleche[-1+i].styletrait:=psdash;
        if s='psdashdot' then listefleche[-1+i].styletrait:=psdashdot;
        if s='psdashdotdot' then listefleche[-1+i].styletrait:=psdashdotdot;
     readln(f,listefleche[-1+i].taille1,listefleche[-1+i].taille2);
     listefleche[-1+i].create(listefleche[-1+i].a1x,listefleche[-1+i].a1y,
     listefleche[-1+i].a2x,listefleche[-1+i].a2y,
     listefleche[-1+i].epaisseur,listefleche[-1+i].couleur,
     listefleche[-1+i].ext1,listefleche[-1+i].ext2,
     listefleche[-1+i].styletrait,
     listefleche[-1+i].taille1,listefleche[-1+i].taille2);
     end;
     goto 12345; end;





if s1='Nombre d''oeils:' then begin
 readln(f,newnombreoeil);
  retaillepoeil(listeoeil,nombreoeil,nombreoeil+newnombreoeil);
  nombreoeil:=nombreoeil+newnombreoeil;
  if newnombreoeil>0 then
     for i:=nombreoeil-newnombreoeil+1 to nombreoeil do begin
     readln(f,s);
  readln(f,listeoeil[-1+i].cx,listeoeil[-1+i].cy,listeoeil[-1+i].ax,
  listeoeil[-1+i].ay,listeoeil[-1+i].epaisseur,listeoeil[-1+i].couleur);
 if not( listeoeil[-1+i].create(listeoeil[-1+i].cx,listeoeil[-1+i].cy,
  listeoeil[-1+i].ax,listeoeil[-1+i].ay,listeoeil[-1+i].epaisseur,
  listeoeil[-1+i].couleur))then
  raise einouterror.Create('erreur');
      end;
    goto 12345; end;

     if s1='Nombre de mesures de distances' then begin
 readln(f,newnombremetre);
 retaillepmetre(listemetre,nombremetre,nombremetre+newnombremetre);
 nombremetre:=nombremetre+newnombremetre;
 if newnombremetre>0 then for i:=nombremetre-newnombremetre+1 to nombremetre do begin
 readln(f,s);
 readln(f,listemetre[-1+i].a1x, listemetre[-1+i].a1y,
 listemetre[-1+i].a2x,listemetre[-1+i].a2y);
 if not( listemetre[-1+i].create(listemetre[-1+i].a1x,listemetre[-1+i].a1y,
  listemetre[-1+i].a2x,listemetre[-1+i].a2y))then
  raise einouterror.Create('erreur');
      end;
       goto 12345; end;


   if s1='Nombre de groupes:' then begin
 readln(f,newnombregroupe);
 retaillepgroupe(listegroupe,nombregroupe,nombregroupe+newnombregroupe);
 nombregroupe:=nombregroupe+newnombregroupe;
 if newnombregroupe> 0 then for i:=nombregroupe-newnombregroupe+1 to nombregroupe do begin
 listegroupe[-1+i].create;
   readln(f,s); readln(f,chacha);
  for j:=1 to  chacha do begin
  readln(f,papa); readln(f,s);
      aze:=mdneant;
   if s='mdoeil' then begin aze:=mdoeil;  papa:=papa+nombreoeil-newnombreoeil; end;
  if s='mddiaphragme' then begin  aze:=mddiaphragme; papa:=papa+nombrediaphragme-newnombrediaphragme; end;
  if s='mdtexteaffiche'then begin  aze:=mdtexteaffiche; papa:=papa+nombretexte-newnombretexte; end;
  if s='mdmiroirconique' then begin  aze:=mdmiroirconique; papa:=papa+nombremiroirconique-newnombremiroirconique; end;
  if s='mdlec' then begin  aze:=mdlec; papa:=papa+nombrelec-newnombrelec; end;
  if s='mdfleche' then begin  aze:=mdfleche; papa:=papa+nombrefleche-newnombrefleche; end;
  if s='mdmiroirplan' then begin  aze:=mdmiroirplan; papa:=papa+nombremiroirplan-newnombremiroirplan; end;
   if s='mdreseau' then begin  aze:=mdreseau; papa:=papa+nombrereseau-newnombrereseau; end;
  if s='mdecran' then begin  aze:=mdecran; papa:=papa+nombreecran-newnombreecran; end;
  if s='mdangle' then begin  aze:=mdangle; papa:=papa+nombreangle-newnombreangle; end;
  if s='mdmetre' then begin  aze:=mdmetre; papa:=papa+nombremetre-newnombremetre; end;
  if s='mdlsr' then begin  aze:=mdlsr;  papa:=papa+nombrelsr-newnombrelsr; end;
  if s='mdmscopa' then begin  aze:=mdmscopa; papa:=papa+nombremscopa-newnombremscopa; end;
  if s='mdlmc' then begin  aze:=mdlmc; papa:=papa+nombrelmc-newnombrelmc; end;
  if s='mdlmd' then begin  aze:=mdlmd; papa:=papa+nombrelmd-newnombrelmd; end;
  if s='mdmscepa' then begin  aze:=mdmscepa; papa:=papa+nombremscepa-newnombremscepa; end;
  if s='mdmscore' then begin  aze:=mdmscore; papa:=papa+nombremscore-newnombremscore; end;
  if s='mdmscere' then begin  aze:=mdmscere; papa:=papa+nombremscere-newnombremscere; end;
   if s='mdpolyhedre' then begin  aze:=mdpolyhedre; papa:=papa+nombrepolyhedre-newnombrepolyhedre; end;
    if s='mdrayon' then begin  aze:=mdrayon;   papa:=papa+nombrerayon-newnombrerayon; end;
     if s='mdsourceponctuelle' then begin  aze:=mdsourceponctuelle; papa:=papa+nombresourceponctuelle-newnombresourceponctuelle; end;
      if s='mdondeplane' then begin  aze:=mdondeplane;   papa:=papa+nombreondeplane-newnombreondeplane; end;
       if s='mdpolycercle' then begin  aze:=mdpolycercle;  papa:=papa+nombrepolycercle-newnombrepolycercle; end;
        if s='mdsphere' then begin  aze:=mdsphere;    papa:=papa+nombresphere-newnombresphere; end;
         if s='mdprisme' then begin  aze:=mdprisme;   papa:=papa+nombreprisme-newnombreprisme; end;
        listegroupe[-1+i].add(aze,papa);
         end; {de la boucle chacha}
                end; {de la boucle nombregroupe}
                goto 12345; end;



  if s1='couleur fond simulation' then begin
  readln(f,newcouleurfondsimulation);
  if ecraser_parametres_actuels then  couleurfondsimulation:=newcouleurfondsimulation;
    goto 12345; end;

    if s1='empennage' then begin
    readln(f,s);
    readln(f,newpositionempennage);
    if ecraser_parametres_actuels then  positionempennage:=newpositionempennage;
    readln(f,s);
     readln(f,newtailleempennage);
     if ecraser_parametres_actuels then  tailleempennage:=newtailleempennage;
     goto 12345; end;


 12345:
 end; {du while not(eof(f))}

   {gestion des erreurs de tout le bloc de lecture}
9852: except
application.messagebox(pchar(rsFichierEndom),
pchar(rsErreur), mb_ok);
 Nouveau1Click(sender);
end;

  closefile(f);

  retaillepgroupe(listegroupe,nombregroupe,nombregroupe+1);
 nombregroupe:=nombregroupe+1;
   listegroupe[-1+nombregroupe].create;
   if newnombremiroirplan>0 then
   for i:=1 to newnombremiroirplan do listegroupe[-1+nombregroupe].add(mdmiroirplan,nombremiroirplan-newnombremiroirplan+i);

   if newnombreReseau>0 then
   for i:=1 to newnombreReseau do listegroupe[-1+nombregroupe].add(mdReseau,nombreReseau-newnombreReseau+i);

   if newnombremscopa>0 then
   for i:=1 to newnombremscopa do listegroupe[-1+nombregroupe].add(mdmscopa,nombremscopa-newnombremscopa+i);

    if newnombremscepa>0 then
   for i:=1 to newnombremscepa do listegroupe[-1+nombregroupe].add(mdmscepa,nombremscepa-newnombremscepa+i);

    if newnombremscore>0 then
   for i:=1 to newnombremscore do listegroupe[-1+nombregroupe].add(mdmscore,nombremscore-newnombremscore+i);

    if newnombremscere>0 then
   for i:=1 to newnombremscere do listegroupe[-1+nombregroupe].add(mdmscere,nombremscere-newnombremscere+i);

    if newnombrelmc>0 then
   for i:=1 to newnombrelmc do listegroupe[-1+nombregroupe].add(mdlmc,nombrelmc-newnombrelmc+i);

    if newnombrelmd>0 then
   for i:=1 to newnombrelmd do listegroupe[-1+nombregroupe].add(mdlmd,nombrelmd-newnombrelmd+i);

    if newnombrerayon>0 then
   for i:=1 to newnombrerayon do listegroupe[-1+nombregroupe].add(mdrayon,nombrerayon-newnombrerayon+i);

    if newnombrelsr>0 then
   for i:=1 to newnombrelsr do listegroupe[-1+nombregroupe].add(mdlsr,nombrelsr-newnombrelsr+i);

    if newnombresourceponctuelle>0 then
   for i:=1 to newnombresourceponctuelle do listegroupe[-1+nombregroupe].add(mdsourceponctuelle,nombresourceponctuelle-newnombresourceponctuelle+i);

    if newnombreondeplane>0 then
   for i:=1 to newnombreondeplane do listegroupe[-1+nombregroupe].add(mdondeplane,nombreondeplane-newnombreondeplane+i);

    if newnombreecran>0 then
   for i:=1 to newnombreecran do listegroupe[-1+nombregroupe].add(mdecran,nombreecran-newnombreecran+i);

    if newnombrepolyhedre>0 then
   for i:=1 to newnombrepolyhedre do listegroupe[-1+nombregroupe].add(mdpolyhedre,nombrepolyhedre-newnombrepolyhedre+i);

    if newnombresphere>0 then
   for i:=1 to newnombresphere do listegroupe[-1+nombregroupe].add(mdsphere,nombresphere-newnombresphere+i);

    if newnombremiroirconique>0 then
   for i:=1 to newnombremiroirconique do listegroupe[-1+nombregroupe].add(mdmiroirconique,nombremiroirconique-newnombremiroirconique+i);

    if newnombretexte>0 then
   for i:=1 to newnombretexte do listegroupe[-1+nombregroupe].add(mdtexteaffiche,nombretexte-newnombretexte+i);

    if newnombrediaphragme>0 then
   for i:=1 to newnombrediaphragme do listegroupe[-1+nombregroupe].add(mddiaphragme,nombrediaphragme-newnombrediaphragme+i);

    if newnombrelec>0 then
   for i:=1 to newnombrelec do listegroupe[-1+nombregroupe].add(mdlec,nombrelec-newnombrelec+i);

    if newnombrepolycercle>0 then
   for i:=1 to newnombrepolycercle do listegroupe[-1+nombregroupe].add(mdpolycercle,nombrepolycercle-newnombrepolycercle+i);

    if newnombreangle>0 then
   for i:=1 to newnombreangle do listegroupe[-1+nombregroupe].add(mdangle,nombreangle-newnombreangle+i);

    if newnombrefleche>0 then
   for i:=1 to newnombrefleche do listegroupe[-1+nombregroupe].add(mdfleche,nombrefleche-newnombrefleche+i);

    if newnombreoeil>0 then
   for i:=1 to newnombreoeil do listegroupe[-1+nombregroupe].add(mdoeil,nombreoeil-newnombreoeil+i);

    if newnombremetre>0 then
   for i:=1 to newnombremetre do listegroupe[-1+nombregroupe].add(mdmetre,nombremetre-newnombremetre+i);

    if newnombreprisme>0 then
   for i:=1 to newnombreprisme do listegroupe[-1+nombregroupe].add(mdprisme,nombreprisme-newnombreprisme+i);

application.MessageBox(pchar(rsUnNouveauGro),
pchar(rsImportationR), mb_ok);


   if ecraser_parametres_actuels then begin
  if fileexistsutf8(copy(nerina,1,length(nerina)-3)+'txt') then
     nerina:=copy(nerina,1,length(nerina)-3)+'txt' else
    if fileexistsutf8(copy(nerina,1,length(nerina)-3)+'rtf') then
     nerina:=copy(nerina,1,length(nerina)-3)+'rtf' else
      nerina:=copy(nerina,1,length(nerina)-3)+'txt';

         try
         assignfile(f,(nerina));
         reset(f);
         closefile(f);
         formediteur.memo1.Lines.clear;
        formediteur.caption:=Format(rsCommentaires5, [copy(nerina, 1, length(
          nerina)-3)]);
  formediteur.show;
  formediteur.WindowState:=wsnormal;
  formediteur.fichierouvrir(nerina);
  if  formediteur.memo1.lines.count=0 then  formediteur.WindowState:=wsminimized;
  except

 formediteur.caption:=Format(rsCommentaires5, [copy(nerina, 1, length(nerina)-3)
   ]);
  formediteur.show;
  formediteur.WindowState:=wsminimized;
  formediteur.memo1.lines.Clear;
  end;
     end;
rapportactuel:=(yymax-yymin)/(xxmax-xxmin);
    afficherlagrille1.checked:=tracergrille;
    attractiondelagrille1.Checked:=attractiongrille;
    cochegrille.checked:=tracergrille;
    cocheattraction.Checked:=attractiongrille;
    N7.checked:=optionnormale; cochenormale.checked:=optionnormale;
    Afficherlesangles1.checked:=optionangles;
    cocheangles.checked:=optionangles;
  Rafraichit;
  fichierouvert:=true;
  modifie:=false;
 if ecraser_parametres_actuels then  nomsauvegarde:=copy(nerina,1,length(nerina)-3)+'opt';
  {opendialog1.FileName;}
  form1.Caption:=Format(rsEspaceDeTrav4, [floattostr(xxmin), floattostr(xxmax),
    floattostr(yymin), floattostr(yymax), extractfilename(nomsauvegarde)]);
   nombre_etats_sauvegardes:=0;  niveau_max_sauvegarde:=0;
    commentaire_change:=false;
   sauvevirtuel;
    rtablir1.enabled:=false; refaire1.Enabled:=false;
end;







 procedure TForm1.Quitter1Click(Sender: TObject);
begin
close;
end;


procedure TForm1.boutonconiqueconcaveClick(Sender: TObject);
begin
if NombreMiroirconique=maxmiroirconique then begin
    application.MessageBox(pchar(rsNombreMaxima23), pchar(rsAttention3), mb_ok);
exit;
end;
boutonconiqueconcave.Enabled:=false;
 boutonconiqueconcave.OnClick:=nil;
form1.image1.Cursor:=crpoint1;

form1.StatusBar1.Panels[2].text:=rsPositionnezL47;
ajoutconiqueconcave:=true;
application.ProcessMessages;
                      boutonconiqueconcave.OnClick:=@boutonconiqueconcaveclick;
desactiveboutons;

end;


procedure TForm1.boutonconiqueconvexeClick(Sender: TObject);
begin
 if NombreMiroirconique=maxmiroirconique then begin
    application.MessageBox(pchar(rsNombreMaxima23), pchar(rsAttention3), mb_ok);
exit;
end;
boutonconiqueconvexe.Enabled:=false;
 boutonconiqueconvexe.OnClick:=nil;
form1.image1.Cursor:=crpoint1;

form1.StatusBar1.Panels[2].text:=rsPositionnezL48;
ajoutconiqueconvexe:=true;
application.ProcessMessages;
                      boutonconiqueconvexe.OnClick:=@boutonconiqueconvexeclick;
desactiveboutons;
end;

procedure TForm1.boutontexteClick(Sender: TObject);
begin
 if modequelquechose then exit;
if Nombretexte=maxtexte then begin
    application.MessageBox(pchar(rsNombreMaxima30), pchar(rsAttention3), mb_ok);
exit;
end;
boutontexte.Enabled:=false;
 boutontexte.OnClick:=nil;

application.ProcessMessages;
boutontexte.OnClick:=@boutontexteclick;
desactiveboutons;


  with saisietexte
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
fontdialog1.font.Assign(policedefaut);
 Edit1.Text:='';
end;
unit9.fontetexte.Assign(policedefaut);

 if saisietexte.showmodal=mrcancel then begin
     activeboutons;
 exit;
 end;
 form1.image1.Cursor:=crtexte;
form1.StatusBar1.Panels[2].text:=rsPositionnezL49;
ajouttexte:=true;
end;


procedure TForm1.Textesurledessin1Click(Sender: TObject);
begin
if Nombretexte=maxtexte then begin
    application.MessageBox(pchar(rsNombreMaxima31), pchar(rsAttention3), mb_ok);
exit;
end;

  with saisietexte2
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
fontetexte2:=policedefaut;
  saisietexte2.fontdialog1.font:=policedefaut;
   saisietexte2.log2.Caption:=unitelongueur;
    saisietexte2.log1.Caption:=unitelongueur;
     saisietexte2.edit1.text:='';
  saisietexte2.editx.text:='';
  saisietexte2.edity.text:='';
 saisietexte2.boutonsup.enabled:=false;
 saisietexte2.caption:=rsAjoutDUnText;
 if saisietexte2.showmodal=mrok then sauvevirtuel;

end;

 procedure TForm1.Afficherlagrille1Click(Sender: TObject);
begin
afficherlagrille1.checked:=not(afficherlagrille1.checked);
cochegrille.Checked:=afficherlagrille1.checked;
tracergrille:=afficherlagrille1.checked;
Rafraichit;
end;

procedure TForm1.cochegrilleClick(Sender: TObject);
begin
 afficherlagrille1.checked:=cochegrille.checked;
tracergrille:=cochegrille.checked;
Rafraichit;
end;

procedure TForm1.cocheattractionClick(Sender: TObject);
begin
 Attractiondelagrille1.checked:=cocheattraction.checked;
 attractiongrille:=cocheattraction.checked;
 Rafraichit;
end;


procedure TForm1.Attractiondelagrille1Click(Sender: TObject);
begin
Attractiondelagrille1.checked:=not(Attractiondelagrille1.checked);
cocheattraction.Checked:=Attractiondelagrille1.checked;
attractiongrille:=Attractiondelagrille1.checked;
end;

procedure TForm1.Optionsdelagrille1Click(Sender: TObject);
begin

      with saisiegrille
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
    saisiegrille.log1.caption:=unitelongueur;
     saisiegrille.log2.caption:=unitelongueur;
 saisiegrille.editpash.text:=floattostr(grillex);
 saisiegrille.editpasv.text:=floattostr(grilley);
 saisiegrille.cocheattraction.checked:=attractiongrille;
 saisiegrille.cocheafficher.checked:=tracergrille;
 repeat
 until saisiegrille.showmodal<>mrno;
 cochegrille.checked:=tracergrille;
 cocheattraction.checked:=attractiongrille;
 Rafraichit;
end;

procedure TForm1.boutonlecClick(Sender: TObject);
begin
if Nombrelec=maxlec then begin
    application.MessageBox(pchar(rsNombreMaxima32), pchar(rsAttention3), mb_ok);
exit;
end;
boutonlec.Enabled:=false;
 boutonlec.OnClick:=nil;
form1.image1.Cursor:=crsommet1;
form1.StatusBar1.Panels[2].text:=rsPositionnezL50;
ajoutlec:=true;
application.ProcessMessages;
boutonlec.OnClick:=@boutonlecclick;
desactiveboutons;
end;


procedure TForm1.N7Click(Sender: TObject);
begin
N7.checked:=not(N7.checked);
cochenormale.checked:=N7.checked;
optionnormale:=N7.checked;
Rafraichit;
end;

procedure TForm1.Afficherlesangles1Click(Sender: TObject);
begin
Afficherlesangles1.checked:=not(Afficherlesangles1.checked);
cocheangles.checked:=Afficherlesangles1.checked;
optionangles:=Afficherlesangles1.checked;
Rafraichit;
end;


 procedure TForm1.cochenormaleClick(Sender: TObject);
begin
if optionangles and not (cochenormale.checked) then begin
application.messagebox(pchar(rsLAffichageDe), pchar(rsAttention3), mb_ok);
 cochenormale.checked:=true;
 end;
 N7.checked:=(cochenormale.checked);
optionnormale:=cochenormale.checked;
Rafraichit;
end;

procedure TForm1.cocheanglesClick(Sender: TObject);
begin

 afficherlesangles1.checked:=(cocheangles.checked);
optionangles:=cocheangles.checked;
if optionangles then begin
cochenormale.checked:=true;
N7.Checked:=true;
end;
Rafraichit;

end;



procedure TForm1.Optionsdesnormales1Click(Sender: TObject);
begin

   with saisienormale
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
 saisienormale.spinepaisseur.value:=epaisseurnormale;
 saisienormale.checknormale.checked:=optionnormale;
 saisienormale.checkangles.Checked:=optionangles;
 saisienormale.spindecimalesangles.value:=decimalesangles;                                  
 saisienormale.spinpourcentage.value:=trunc(pourcentagenormale*100);

saisienormale.colorgrid1.selected:=(couleurnormale) ;

        saisienormale.ShowModal;
        N7.Checked:=optionnormale;
        afficherlesangles1.checked:=optionangles;
        cocheangles.checked:=optionangles;
        cochenormale.checked:=optionnormale;
      Rafraichit;

end;
procedure TForm1.boutonpolycercleClick(Sender: TObject);
begin
if Nombrepolycercle=maxpolycercle then begin
    application.MessageBox(pchar(rsNombreMaxima26), pchar(rsAttention3), mb_ok);
exit;
end;
boutonpolycercle.Enabled:=false;
 boutonpolycercle.OnClick:=nil;
form1.image1.Cursor:=crPoint1;
form1.StatusBar1.Panels[2].text:=rsPositionnezL51;
ajoutpolycercle:=true;
application.ProcessMessages;
boutonpolycercle.OnClick:=@boutonpolycercleclick;
desactiveboutons;
youri[1]:=false;  rectifait:=false;

  with saisienombresommet
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
spinedit1.MinValue:=2;
end;
saisienombresommet.groupbox1.Caption:=rsSommetsDeCet2;
 if saisienombresommet.showmodal=mrcancel then begin
 form1.activeboutons;
  form1.image1.Cursor:=crdefault;
                     form1.StatusBar1.Panels[2].text:='';
 exit;
 end;
 nbactsommet:=0;

end;

procedure TForm1.boutonfantomeClick(Sender: TObject);
begin
montrernumerossegments:=true;
Rafraichit;

  with saisiefantomes
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisiefantomes.cochefantomes.Checked:=fantomespresents;
saisiefantomes.editfantomes.Text:=listefantomes;
saisiefantomes.showmodal;
montrernumerossegments:=false;
Rafraichit;
end;

procedure TForm1.SpeedButton1Click(Sender: TObject);
begin
formediteur.show;
formediteur.WindowState:=wsnormal;
end;



procedure TForm1.Contacterlauteur1Click(Sender: TObject);
begin
{$ifdef windows}ShellExecute( 0, Nil, PChar('mailto:jeanmarie.biansan@free.fr'),Nil, Nil, SW_NORMAL ); {$endif}
{$ifndef windows} application.MessageBox('jeanmarie.biansan@free.fr','Adresse de courriel:',mb_ok);              {$endif}
end;


procedure TForm1.Apropos2Click(Sender: TObject);
begin


   with form4
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
if form4.showmodal=mrno then application.Terminate;

end;

procedure TForm1.AidesurOptGeo1Click(Sender: TObject);

 var
  v: THTMLBrowserHelpViewer;
  BrowserPath, BrowserParams: string;
  p: LongInt;
  URL: String;
  BrowserProcess: TProcessUTF8;
begin
  v:=THTMLBrowserHelpViewer.Create(nil);
  try
    v.FindDefaultBrowser(BrowserPath,BrowserParams);


    url:='file:///'+repertoire_executable+'aide/'+FallbackLang+'/optgeo.html';
    p:=System.Pos('%s', BrowserParams);
    System.Delete(BrowserParams,p,2);
    System.Insert(URL,BrowserParams,p);


    BrowserProcess:=TProcessUTF8.Create(nil);
    try
      BrowserProcess.CommandLine:=BrowserPath+' '+BrowserParams;
      BrowserProcess.Execute;
    finally
      BrowserProcess.Free;
    end;
  finally
    v.Free;
  end;

end ;

procedure TForm1.Surfacesdonde1Click(Sender: TObject);
begin

    with saisiepointssurfaceonde
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
  saisiepointssurfaceonde.spinpoints.value:=maxrayonsrecherchesurfacesonde;
  saisiepointssurfaceonde.showmodal;
  Rafraichit;
end;

procedure TForm1.Editerlecommentairedelasimulation1Click(Sender: TObject);
begin
formediteur.show;
formediteur.WindowState:=wsnormal;
end;

procedure TForm1.Supprimerunlment1Click(Sender: TObject);
begin
form_palette.boutonsuppressionClick(Sender);
end;

procedure TForm1.Propritsdunlment1Click(Sender: TObject);
begin
form_palette.boutonpropClick(Sender);
end;

procedure TForm1.Dplacerunlment1Click(Sender: TObject);
begin
form_palette.boutondeplacementClick(Sender);
end;

procedure TForm1.Commenatiresurlasimulation1Click(Sender: TObject);
begin
Form1.SpeedButton1Click(Sender);
end;


procedure TForm1.Mesurededistance1Click(Sender: TObject);
begin
{Form1.boutondistanceClick(Sender); }
 with saisiemetre
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisiemetre.boutonsup.enabled:=false;
saisiemetre.editx1.text:='';
saisiemetre.editx2.text:='';
saisiemetre.edity1.text:='';
saisiemetre.edity2.text:='';
saisiemetre.caption:=rsAjoutDUneMes;
saisiemetre.log1.Caption:=unitelongueur;
     saisiemetre.log2.Caption:=unitelongueur;
     saisiemetre.log3.Caption:=unitelongueur;
      saisiemetre.log4.Caption:=unitelongueur;
  if    saisiemetre.ShowModal=mrok then sauvevirtuel;
end;

procedure TForm1.Mesuredangle1Click(Sender: TObject);
begin
{Form1.boutonangleClick(Sender); }
 with saisieangle
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisieangle.boutonsup.enabled:=false;
saisieangle.caption:=rsAjoutDUneMes2;
saisieangle.editx1.text:='';
saisieangle.editx2.text:='';
saisieangle.editcx.text:='';
saisieangle.editcy.text:='';
saisieangle.edity1.text:='';
saisieangle.edity2.text:='';
saisieangle.log1.Caption:=unitelongueur;
     saisieangle.log2.Caption:=unitelongueur;
     saisieangle.log3.Caption:=unitelongueur;
      saisieangle.log4.Caption:=unitelongueur;
      saisieangle.log5.Caption:=unitelongueur;
      saisieangle.log6.Caption:=unitelongueur;
if saisieangle.showmodal=mrok then sauvevirtuel;
end;

procedure TForm1.SiteWeb1Click(Sender: TObject);
   var
  v: THTMLBrowserHelpViewer;
  BrowserPath, BrowserParams: string;
  p: LongInt;
  URL: String;
  BrowserProcess: TProcessUTF8;
begin
  v:=THTMLBrowserHelpViewer.Create(nil);
  try
    v.FindDefaultBrowser(BrowserPath,BrowserParams);
    //debugln(['Path=',BrowserPath,' Params=',BrowserParams]);

   url:='http://jeanmarie.biansan.free.fr/logiciel.html';
    p:=System.Pos('%s', BrowserParams);
    System.Delete(BrowserParams,p,2);
    System.Insert(URL,BrowserParams,p);

    // start browser
    BrowserProcess:=TProcessUTF8.Create(nil);
    try
      BrowserProcess.CommandLine:=BrowserPath+' '+BrowserParams;
      BrowserProcess.Execute;
    finally
      BrowserProcess.Free;
    end;
  finally
    v.Free;
end; end;



procedure TForm1.Rayonsfantomes1Click(Sender: TObject);
begin
Form1.boutonfantomeClick(Sender);
end;


procedure TForm1.Traitflche1Click(Sender: TObject);
begin
if modequelquechose then exit;
if Nombrefleche=maxfleche then begin
    application.MessageBox(pchar(rsNombreMaxima3), pchar(rsAttention3), mb_ok);
exit;
end;

with saisiefle1
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisiefle1.boutonsup.enabled:=false;
saisiefle1.caption:=rsAjoutDUnTrai;
saisiefle1.editx1.text:='';
saisiefle1.editx2.text:='';
saisiefle1.edity1.text:='';
saisiefle1.edity2.text:='';
saisiefle1.spinepaisseur.value:=epaisseurfleche;
saisiefle1.spintaille1.value:=10;
saisiefle1.spintaille2.value:=10;
saisiefle1.radioext1.itemindex:=0;
saisiefle1.radioext2.itemindex:=0;
saisiefle1.radiostyletrait.itemindex:=0;
 saisiefle1.colorgrid1.selected:=(couleurfleche) ;
 saisiefle1.log1.Caption:=unitelongueur;
     saisiefle1.log2.Caption:=unitelongueur;
     saisiefle1.log3.Caption:=unitelongueur;
      saisiefle1.log4.Caption:=unitelongueur;
 if saisiefle1.showmodal=mrok then sauvevirtuel;
end;

procedure TForm1.FormDestroy(Sender: TObject);
var i:integer;

begin
  fermeture:=true;   for i:=1 to maxannulation do etatactuel[i].Free;
 FreePMiroirPlan(ListeMiroirPlan,nombremiroirplan);  FreePreseau(Listereseau,nombrereseau);  freepfleche(listefleche,nombrefleche);
 FreePecran(Listeecran,nombreecran);  freeppolyhedre(listepolyhedre,nombrepolyhedre);
 freeppolycercle(listepolycercle,nombrepolycercle);    freepprisme(listeprisme,nombreprisme);
 freeplsr(listelsr,nombrelsr);      freepsphere(listesphere,nombresphere);
 freePMscopa(ListeMscopa,nombreMscopa); freepmiroirconique(listemiroirconique,nombremiroirconique);
 freePMscepa(ListeMscepa,nombreMscepa); freeptexteaffiche(listetexteaffiche,nombretexte);
 freePMscere(ListeMscere,nombreMscere);
 freepMscore(ListeMscore,nombreMscore); freepsegment(listesegment,nombretotalsegment);
 freepsegment(listenormale,nombrenormale);  freepgroupe(listegroupe,nombregroupe);
 freepoeil(listeoeil,nombreoeil);
 freepLmc(ListeLmc,nombreLmc);  freeplec(listelec,nombrelec);
 freepmetre(listemetre,nombremetre);   freepangle(listeangle,nombreangle);
 freepLmd(ListeLmd,nombreLmd);   freepdiaphragme(listediaphragme,nombrediaphragme);
 freepRayon(ListeRayon,nombreRayon);    freepangle(listeanglesatracer,nombreanglesatracer);
 freepSourcePonctuelle(ListeSourcePonctuelle,nombreSourcePonctuelle);
 freepOndePlane(ListeOndePlane,nombreOndePlane);
 listemateriaux.free;
listenrouge.free;
listenbleu.free;
listenvert.free;
//if bibi<>nil then BIBI.free;
end;


procedure TForm1.boutonoeilClick(Sender: TObject);
begin
 if Nombreoeil=maxoeil then begin
    application.MessageBox(pchar(rsNombreMaxima33), pchar(rsAttention3), mb_ok);
exit;
end;
boutonoeil.Enabled:=false;
 boutonoeil.OnClick:=nil;
form1.image1.Cursor:=crPoint1;

form1.StatusBar1.Panels[2].text:=rsPositionnezL52;
ajoutoeil:=true;
application.ProcessMessages;
                      boutonoeil.OnClick:=@boutonoeilclick;
desactiveboutons;
end;

procedure TForm1.Oeilstylis1Click(Sender: TObject);

begin
if modequelquechose then exit;
if NombreOeil=maxOeil then begin
    application.MessageBox(pchar(rsNombreMaxima34), pchar(rsAttention3), mb_ok);
exit;
end;

     with saisieOeil
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
  saisieoeil.boutonsup.enabled:=false;
    saisieoeil.editx1.text:='';
    saisieoeil.editx2.text:='';
    saisieoeil.edity1.text:='';
    saisieoeil.edity2.text:='';
    saisieoeil.editepaisseur.Value:=epaisseuroeil;

saisieoeil.colorgrid1.selected:=(couleuroeil) ;
    saisieoeil.caption:=
    rsAjoutDUnOeil;
     saisieoeil.log1.Caption:=unitelongueur;
     saisieoeil.log2.Caption:=unitelongueur;
      saisieoeil.log3.Caption:=unitelongueur;
      saisieoeil.log4.Caption:=unitelongueur;
if saisieOeil.showmodal=mrok then sauvevirtuel;
end;




procedure TForm1.Elementschoisir2Click(Sender: TObject);
begin
  form_palette.boutongroupeClick(Sender);
end;

procedure TForm1.Tousleslments1Click(Sender: TObject);

  var i:integer;
  begin
if Nombregroupe=maxgroupe then begin
    application.MessageBox(pchar(rsNombreMaxima), pchar(rsAttention3), mb_ok);
exit;
end;
if Nombrequelquechose<2 then begin
    application.MessageBox(pchar(rsImpossibleDe2), pchar(rsAttention3), mb_ok);
exit;
end;
if application.messagebox(pchar(Format(rsDoisJeCrErLe, [inttostr(nombregroupe+1)
  ])), pchar(rsCrAtion), mb_yesno)= idno then exit;
retaillepgroupe(listegroupe, nombregroupe,nombregroupe+1);
inc(nombregroupe);
listegroupe[-1+nombregroupe].create;

if nombreoeil>0 then for  i:=1 to nombreoeil do
listegroupe[-1+nombregroupe].add(mdoeil,i);

if nombrediaphragme>0 then for  i:=1 to nombrediaphragme do
listegroupe[-1+nombregroupe].add(mddiaphragme,i);

if nombretexte>0 then for  i:=1 to nombretexte do
listegroupe[-1+nombregroupe].add(mdtexteaffiche,i);

 if nombremiroirconique>0 then for  i:=1 to nombremiroirconique do
listegroupe[-1+nombregroupe].add(mdmiroirconique,i);

if nombrelec>0 then for  i:=1 to nombrelec do
listegroupe[-1+nombregroupe].add(mdlec,i);

if nombrefleche>0 then for  i:=1 to nombrefleche do
listegroupe[-1+nombregroupe].add(mdfleche,i);

if nombremiroirplan>0 then for  i:=1 to nombremiroirplan do
listegroupe[-1+nombregroupe].add(mdmiroirplan,i);

  if nombreReseau>0 then for  i:=1 to nombreReseau do
  listegroupe[-1+nombregroupe].add(mdReseau,i);

if nombreecran>0 then for  i:=1 to nombreecran do
listegroupe[-1+nombregroupe].add(mdecran,i);

if nombreangle>0 then for  i:=1 to nombreangle do
listegroupe[-1+nombregroupe].add(mdangle,i);


if nombremetre>0 then for  i:=1 to nombremetre do
listegroupe[-1+nombregroupe].add(mdmetre,i);

if nombrelsr>0 then for  i:=1 to nombrelsr do
listegroupe[-1+nombregroupe].add(mdlsr,i);

  if nombremscopa>0 then for  i:=1 to nombremscopa do
listegroupe[-1+nombregroupe].add(mdmscopa,i);

if nombrelmc>0 then for  i:=1 to nombrelmc do
listegroupe[-1+nombregroupe].add(mdlmc,i);

if nombrelmd>0 then for  i:=1 to nombrelmd do
listegroupe[-1+nombregroupe].add(mdlmd,i);

if nombremscepa>0 then for  i:=1 to nombremscepa do
listegroupe[-1+nombregroupe].add(mdmscepa,i);

 if nombremscore>0 then for  i:=1 to nombremscore do
listegroupe[-1+nombregroupe].add(mdmscore,i);

if nombremscere>0 then for  i:=1 to nombremscere do
listegroupe[-1+nombregroupe].add(mdmscere,i);


if nombrepolyhedre>0 then for  i:=1 to nombrepolyhedre do
listegroupe[-1+nombregroupe].add(mdpolyhedre,i);

if nombrerayon>0 then for  i:=1 to nombrerayon do
listegroupe[-1+nombregroupe].add(mdrayon,i);

if nombresourceponctuelle>0 then for  i:=1 to nombresourceponctuelle do
listegroupe[-1+nombregroupe].add(mdsourceponctuelle,i);

if nombreondeplane>0 then for  i:=1 to nombreondeplane do
listegroupe[-1+nombregroupe].add(mdondeplane,i);

 if nombrepolycercle>0 then for  i:=1 to nombrepolycercle do
listegroupe[-1+nombregroupe].add(mdpolycercle,i);

 if nombresphere>0 then for  i:=1 to nombresphere do
listegroupe[-1+nombregroupe].add(mdsphere,i);
  sauvevirtuel;
  Rafraichit;
end;

procedure TForm1.AssocierlesfichiersoptavecOptGeo1Click(Sender: TObject);
{$ifdef windows}var Registre:TRegistry; {$endif}
begin
  {$ifdef windows}  Registre:= TRegistry.Create;
    With Registre do try
    RootKey := HKEY_CLASSES_ROOT;
    OpenKey('OptGeoFile',True);
    WriteString('','Fichier de simulation de OptGeo');
    CloseKey;
    OpenKey('OptGeoFile\shell\open\command',True);
    WriteString('','"'+repertoire_executable+nomexecutable+'" "%1"');
    CloseKey;
    OpenKey('.opt',True);
    WriteString('','OptGeoFile');
    CloseKey;
    finally
    Free;
    end; {$endif}
end;

 procedure TForm1.Dsassocier1Click(Sender: TObject);
 {$ifdef windows}var Registre:TRegistry; {$endif}
begin
  {$ifdef windows}  Registre:= TRegistry.Create;
    With Registre do try
    RootKey := HKEY_CLASSES_ROOT;
    DeleteKey('OpGeoFile');
    DeleteKey('.opt');
    finally
    Free;
    end;  {$endif}
end;

procedure TForm1.Manuellement1Click(Sender: TObject);
begin
Form1.Modifierdimensionsespacedetravail11Click(Sender);
end;

procedure TForm1.Ajusterautomatiquement1Click(Sender: TObject);
var iii:integer;
begin
if nombrequelquechose=0 then exit;

iii:=0;
while retaille_espace_possible(xxmin+iii*(xxmax-xxmin)/100,xxmax,yymin,yymax)
do inc(iii);
xxmin:=xxmin+(iii-1)*(xxmax-xxmin)/100;

iii:=0;
while retaille_espace_possible(xxmin,xxmax-iii*(xxmax-xxmin)/100,yymin,yymax)
do inc(iii);
xxmax:=xxmax-(iii-1)*(xxmax-xxmin)/100;

iii:=0;
while retaille_espace_possible(xxmin,xxmax,yymin+iii*(yymax-yymin)/100,yymax)
do inc(iii);
yymin:=yymin+(iii-1)*(yymax-yymin)/100;

iii:=0;
while retaille_espace_possible(xxmin,xxmax,yymin,yymax-iii*(yymax-yymin)/100)
do inc(iii);
yymax:=yymax-(iii-1)*(yymax-yymin)/100;


rapportactuel:=(yymax-yymin)/(xxmax-xxmin);
form1.Caption:=Format(rsEspaceDeTrav, [floattostr(xxmin), floattostr(xxmax),
  floattostr(yymin), floattostr(yymax)]);
Rafraichit;
sauvevirtuel;

end;


procedure TForm1.Modifierdimensionsespacedetravail11Click(Sender: TObject);
begin

saisieretailleespace.Edit_xxmin_m.Text:=floattostr(xxmin);
saisieretailleespace.Edit_yymin_m.Text:=floattostr(yymin);
saisieretailleespace.Edit_xxmax_m.Text:=floattostr(xxmax);
saisieretailleespace.Edit_yymax_m.Text:=floattostr(yymax);
 saisieretailleespace.log1.Caption:=unitelongueur;
 saisieretailleespace.log2.Caption:=unitelongueur;
 saisieretailleespace.log3.Caption:=unitelongueur;
 saisieretailleespace.log4.Caption:=unitelongueur;
  saisieretailleespace.cocherespect_m.checked:=respectrapport;
  with saisieretailleespace
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
 if saisieretailleespace.showmodal=mrcancel then exit;
 {puis traitement: voir si nouvelles valeurs compatibles, etc...}
if retaille_espace_possible(xxmin_m,xxmax_m,yymin_m,yymax_m) then begin
grillex:=1/(xxmax-xxmin)*(xxmax_m-xxmin_m)*grillex;
grilley:=1/(yymax-yymin)*(yymax_m-yymin_m)*grilley;
grillex:=int(grillex*1.001/puissance(10,partieentiere(log10(grillex))))*
puissance(10,partieentiere(log10(grillex)));
grilley:=int(grilley*1.001/puissance(10,partieentiere(log10(grilley))))*
puissance(10,partieentiere(log10(grilley)));
xxmin:=xxmin_m;
xxmax:=xxmax_m;
yymin:=yymin_m;
yymax:=yymax_m;
respectrapport:=respect_m;
rapportactuel:=(yymax-yymin)/(xxmax-xxmin);
form1.Caption:=Format(rsEspaceDeTrav, [floattostr(xxmin), floattostr(xxmax),
  floattostr(yymin), floattostr(yymax)]);
Rafraichit;
sauvevirtuel;
end else begin
  application.messagebox(pchar(rsUnAuMoinsDes),
  pchar(rsRedimensionn), mb_ok);
    exit;
end;
end;

procedure TForm1.durpertoireexemples1Click(Sender: TObject);
begin
 form1.Ouvrirsimulation(sender,repertoire_simul_perso);
end;

procedure TForm1.durpertoireexemples2Click(Sender: TObject);
begin
 form1.Ouvrirsimulation(sender,repertoire_exemples);
end;

procedure TForm1.Couleurdefond1Click(Sender: TObject);
begin
form_palette.Hide;
   formediteur.Hide;
 case QuestionDlg (rsChoixDuFond, rsChoisissez, mtCustom, [mrYes,
   rsCouleurUnie, mrNo, rsImageDeFond, 'IsDefault'], '') of
         mrYes: begin
           colordialog1.Color:=couleurfondsimulation;

if colordialog1.Execute then begin
couleurfondsimulation:=colordialog1.Color;
fond_simulation_est_image:=false;
sauvevirtuel;
end;
form_palette.Show;
Rafraichit;
end;

         mrNo:
         begin
   OpenDialogImageFonds.FileName := '';
 OpenDialogImageFonds.InitialDir:=repertoire_exemples;
if not(OpenDialogImageFonds.Execute) then begin
form_palette.Show;
exit;
end
else nom_fichier_image_fond_simulation:=(OpenDialogImageFonds.FileName);
  fond_simulation_est_image:=true;
  form_palette.Show;
  Rafraichit;
         end;
         mrCancel:
         begin
         form_palette.Show;
         exit;
     end;
         end;
end;

procedure TForm1.Prfrences1Click(Sender: TObject);
begin

 with saisiepreferences
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
combobox1.itemindex:=0;
boutoncouleur.enabled:=true;
labelcouleur.width:=89;
labelcouleur.height:=25;
 labelcouleur.Color:=couleurmiroirplan;
 labelcouleur1.width:=89;
labelcouleur1.height:=25;
 labelcouleur1.Color:=couleurglobale;
 labelcouleuraxe.width:=113;
labelcouleuraxe.height:=25;
 labelcouleuraxe.color:=saisiepreferences.Color;
 labelcouleuraxe1.width:=113;
labelcouleuraxe1.height:=25;
 labelcouleuraxe1.color:=couleurglobaleaxe;
 labecouleurrayon.width:=113;
labecouleurrayon.height:=25;
 labecouleurrayon.color:=couleurglobalerayon;
 spinedit1.enabled:=true;
 spinedit1.Value:=epaisseurmiroirplan;
 spinedit2.Value:=epaisseurglobale;
 boutoncouleuraxe.enabled:=false;
  boutonpolice.Enabled:=false;
  memo1.Lines.Clear;
memo1.lines.Add('Nom: '+policedefaut.name);
memo1.lines.Add('Taille: '+inttostr(policedefaut.size));
if (fsunderline in policedefaut.style) then memo1.lines.add('Souligné');
if (fsbold in policedefaut.style) then memo1.lines.add('Gras');
if (fsitalic in policedefaut.style) then memo1.lines.add('Italique');
if (fsstrikeout in policedefaut.style) then memo1.lines.add('Barré');
memo2.Lines.Clear;
memo2.lines.Add('Nom: '+policedefaut.name);
memo2.lines.Add('Taille: '+inttostr(policedefaut.size));
if (fsunderline in policedefaut.style) then memo2.lines.add('Souligné');
if (fsbold in policedefaut.style) then memo2.lines.add('Gras');
if (fsitalic in policedefaut.style) then memo2.lines.add('Italique');
if (fsstrikeout in policedefaut.style) then memo2.lines.add('Barré');
end;

saisiepreferences.showmodal;
end;

procedure TForm1.Empennagedesrayons1Click(Sender: TObject);
begin

   saisieempennage.spinposition.value:=positionempennage;
   saisieempennage.SpinEdit1.Value:=tailleempennage;
    with saisieempennage
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
 if saisieempennage.showmodal=mrok then begin

   positionempennage:=saisieempennage.spinposition.value;
   tailleempennage:=saisieempennage.SpinEdit1.Value;
    sauvevirtuel;
   Rafraichit;
   end;
end;

 procedure TForm1.charge_virtuel;
 var indice,i,j:integer;   s,s1:string;
newnombremiroirplan,newnombrereseau,newnombremscopa,newnombremscepa,newnombremscore,
newnombremscere,newnombrelmc,newnombrelmd,newnombrerayon,newnombrelsr,
newnombresourceponctuelle,newnombreondeplane,newnombreecran,toto,
newnombrepolyhedre,newnombresphere,newnombremiroirconique,
newnombretexte,newnombrediaphragme,newnombrelec,newnombrepolycercle,
newnombreangle,newnombrefleche,newnombreoeil,newnombregroupe,
newnombremetre,newnombreprisme:integer;
coco:tcolor;      aze:ttypeelementmodif;
chacha,papa:integer;    versionfichier:longint;
nerina,old_nom_sauvegarde:string;

label 9852,789,12345;

procedure monmetazero;
begin
couleurfondsimulation:=clwhite;
indice_vert_par_defaut:=1;
 indice_bleu_par_defaut:=1;
  indice_rouge_par_defaut:=1;
positionempennage:=500;
 tailleempennage:=5;
FreePMiroirPlan(ListeMiroirPlan,nombremiroirplan); FreePReseau(ListeReseau,nombreReseau);  freepfleche(listefleche,nombrefleche);
FreePecran(Listeecran,nombreecran);   freepdiaphragme(listediaphragme,nombrediaphragme);
freeplsr(listelsr,nombrelsr); freeppolyhedre(listepolyhedre,nombrepolyhedre);  freepprisme(listeprisme,nombreprisme);
freeppolycercle(listepolycercle,nombrepolycercle);    freepoeil(listeoeil,nombreoeil);
 freePMscopa(ListeMscopa,nombreMscopa); freepgroupe(listegroupe,nombregroupe);
 freePMscepa(ListeMscepa,nombreMscepa);
 freePMscere(ListeMscere,nombreMscere);
 freepMscore(ListeMscore,nombreMscore);
 freepLmc(ListeLmc,nombreLmc);  freeplec(listelec,nombrelec);
 freepmiroirconique(listemiroirconique,nombremiroirconique);
 freepLmd(ListeLmd,nombreLmd);  freeptexteaffiche(listetexteaffiche,nombretexte);
 freepRayon(ListeRayon,nombreRayon);
 freepSourcePonctuelle(ListeSourcePonctuelle,nombreSourcePonctuelle);
 freepOndePlane(ListeOndePlane,nombreOndePlane);
  freepsphere(listesphere,nombresphere);  freepmetre(listemetre,nombremetre);
  freepangle(listeangle,nombreangle);
freepangle(listeanglesatracer,nombreanglesatracer);
 creePMiroirPlan(listemiroirplan,0);  creePreseau(listereseau,0); creepfleche(listefleche,0);
 creePEcran(listeecran,0);  CreePpolyhedre(listepolyhedre,0); CreePprisme(listeprisme,0);
 creeppolycercle(listepolycercle,0); CreePOeil(listeoeil,0);
 creeplsr(listelsr,0);  creepsphere(listesphere,0);
  creePMscopa(ListeMscopa,0);  creepmetre(listemetre,0);  creepangle(listeangle,0);
  creePMscepa(ListeMscepa,0);  creepmiroirconique(listemiroirconique,0);
  creePMscore(ListeMscore,0);  creeptexteaffiche(listetexteaffiche,0);
  creePMscere(ListeMscere,0);  creepdiaphragme(listediaphragme,0);
  creePlmc(Listelmc,0);  creeplec(listelec,0);
  creePlmd(Listelmd,0);  creepangle(listeanglesatracer,0);
  creePrayon(Listerayon,0);   creepgroupe(listegroupe,0);
  creePSourcePonctuelle(ListeSourcePonctuelle,0);
  creePOndePlane(ListeOndePlane,0);
   maxrayonsrecherchesurfacesonde:=20;
  nomsauvegarde:='';    unitelongueur:=rsMm;
   fantomespresents:=false;     modifie:=false;  commentaire_change:=false;
  { mainform.Editor.Lines.clear;
   mainform.WindowState:=wsminimized;
   mainform.Caption:='Commentaires sur la simulation de nom non précisé...';  }
 nombrerayon:=0; nombremiroirplan:=0; nombrereseau:=0;   nombreecran:=0;  nombremiroirconique:=0;
     nombreMscopa:=0;  nombreMscepa:=0; nombrelmc:=0; nombrelec:=0;
     nombrelmd:=0;    nombrefleche:=0;    nombreoeil:=0;  nombregroupe:=0;
     nombreMscore:=0;  nombreMscere:=0;  nombresourceponctuelle:=0;
     nombreondeplane:=0;    nombrelsr:=0;   nombrepolyhedre:=0;  nombrepolycercle:=0;
     nombresphere:=0;   nombremetre:=0;   nombretexte:=0;  nombrediaphragme:=0;
                             nombreangle:=0;  decimalesangles:=0;  nombreprisme:=0;


      sa_hauteur:=hauteurinitialeimage1;
   sa_largeur:=largeurinitialeimage1;
   sa_respect:=true;
    xxmin:=0; xxmax:=abs(sa_largeur); yymin:=0;
yymax:=abs(sa_hauteur);  rapportactuel:=(yymax-yymin)/(xxmax-xxmin);
tracergrille:=false; attractiongrille:=false; optionnormale:=true;
optionangles:=false;
N7.Checked:=optionnormale; cochegrille.Checked:=tracergrille;
cocheattraction.checked:=attractiongrille;
cochenormale.checked:=optionnormale; afficherlagrille1.Checked:=tracergrille;
Afficherlesangles1.checked:=optionangles;
cocheangles.checked:=optionangles;
grillex:=xxmax/20; grilley:=yymax/20;
grillex:=int(grillex*1.001/puissance(10,partieentiere(log10(grillex))))*
puissance(10,partieentiere(log10(grillex)));
grilley:=int(grilley*1.001/puissance(10,partieentiere(log10(grilley))))*
puissance(10,partieentiere(log10(grilley)));
respectrapport:=sa_respect;
 form1.retaille_image1;
form1.Caption:=Format(rsEspaceDeTrav2, [floattostr(xxmin), floattostr(xxmax),
  floattostr(yymin), floattostr(yymax)]);

  

     end;
 procedure litdansstring_string(var s:string);
 begin
  s:=etatactuel[nombre_etats_sauvegardes][indice-1];
  inc(indice); end;
   procedure litdansstring_shortstring(var s:shortstring);
 begin
  s:=etatactuel[nombre_etats_sauvegardes][indice-1];
  inc(indice); end;
   procedure litdansstring_entier(var s:integer);
 begin
  s:=strtoint(etatactuel[nombre_etats_sauvegardes][indice-1]);
  inc(indice); end;
   procedure litdansstring_reel(var s:extended);
 begin
  s:=strtofloat(etatactuel[nombre_etats_sauvegardes][indice-1]);
  inc(indice); end;
   procedure litdansstring_couleur(var s:tcolor);
 begin
  s:=strtoint(etatactuel[nombre_etats_sauvegardes][indice-1]);
  inc(indice); end;
 begin
 if nombre_etats_sauvegardes<=1 then exit;
  if nombre_etats_sauvegardes=2 then rtablir1.Enabled:=false;
   old_nom_sauvegarde:=nomsauvegarde;
   monmetazero;
  nomsauvegarde:=old_nom_sauvegarde;
     indice:=1;
                 dec(nombre_etats_sauvegardes);
   while indice<etatactuel[nombre_etats_sauvegardes].Count do begin
   litdansstring_string(s1);
   if s1='NE PAS MODIFIER CE FICHIER' then goto  12345;

   if s1='VERSION' then begin
   litdansstring_entier(versionfichier);
   litdansstring_reel(xxmin);
   litdansstring_reel(xxmax);
   litdansstring_reel(yymin);
   litdansstring_reel(yymax);
   litdansstring_string(s);
   respectrapport:=(s='TRUE');
   goto 12345;
   end;




  if s1= 'UNITE' then begin
   litdansstring_shortstring(unitelongueur);
   goto 12345; end;

   if s1='OPTIONS GRILLE' then begin
    litdansstring_string(s);
   tracergrille:=(s='TRUE');
    litdansstring_string(s);
   attractiongrille:=(s='TRUE');
    litdansstring_reel(grillex);
     litdansstring_reel(grilley);
   goto 12345; end;

     if s1= 'OPTIONS NORMALES' then begin
     litdansstring_string(s);
optionnormale:=(s='TRUE');
litdansstring_string(s);
optionangles:=(s='TRUE');
 litdansstring_reel(pourcentagenormale);
 litdansstring_couleur(couleurnormale);
 litdansstring_entier(epaisseurnormale);
  litdansstring_entier(decimalesangles);
  goto 12345; end;

   if s1='OPTIONS SEGMENTS FANTOMES' then begin
  litdansstring_string(s);
  fantomespresents:=(s='TRUE');
  litdansstring_string(listefantomes);
  goto 12345; end;

  if s1= 'NOMBRE POINTS TRACE SURFACE ONDE' then begin
      litdansstring_entier(maxrayonsrecherchesurfacesonde);
        if fantomespresents then
for i:=1 to maxtotalsegment do
  fantomes[i]:=(pos(','+inttostr(i)+',',listefantomes)<>0) or (pos('('+inttostr(i)+',',listefantomes)<>0) or
  (pos(','+inttostr(i)+')',listefantomes)<>0) or
  (pos('('+inttostr(i)+')',listefantomes)<>0);
      goto 12345; end;

   if s1='INDICES PAR DEFAUT' then begin
    litdansstring_reel(indice_vert_par_defaut);
    litdansstring_reel(indice_bleu_par_defaut);
    litdansstring_reel(indice_rouge_par_defaut);
    goto 12345;
   end;


   if s1='LAMBDA' then begin
    litdansstring_reel(longueur_donde_red);
    litdansstring_reel(longueur_donde_blue);
    litdansstring_reel(longueur_donde_green);
    goto 12345;
   end;




  if s1='Nombre de miroirs plans:' then begin
  litdansstring_entier(newnombremiroirplan);
  retaillepmiroirplan(listemiroirplan,nombremiroirplan,newnombremiroirplan);
  nombremiroirplan:=newnombremiroirplan;
  if nombremiroirplan>0 then
     for i:=1 to nombremiroirplan do begin
     litdansstring_string(s);
  litdansstring_reel(listemiroirplan[-1+i].a1x);
  litdansstring_reel(listemiroirplan[-1+i].a1y);
  litdansstring_reel(listemiroirplan[-1+i].a2x);
  litdansstring_reel(listemiroirplan[-1+i].a2y);
  litdansstring_entier(listemiroirplan[-1+i].epaisseur);
  litdansstring_couleur(listemiroirplan[-1+i].couleur);
  litdansstring_string(s);
  listemiroirplan[-1+i].hachures:=(s='TRUE');
 if not( listemiroirplan[-1+i].create(listemiroirplan[-1+i].a1x,listemiroirplan[-1+i].a1y,
  listemiroirplan[-1+i].a2x,listemiroirplan[-1+i].a2y,listemiroirplan[-1+i].epaisseur,
  listemiroirplan[-1+i].couleur,listemiroirplan[-1+i].hachures)) then
  raise einouterror.Create('erreur');
  end;
  goto 12345; end;


   if s1='Nombre de reseaux:' then begin
  litdansstring_entier(newnombreReseau);
  retaillepReseau(listeReseau,nombreReseau,newnombreReseau);
  nombreReseau:=newnombreReseau;
  if nombreReseau>0 then
     for i:=1 to nombreReseau do begin
     litdansstring_string(s);
  litdansstring_reel(listeReseau[-1+i].a1x);
  litdansstring_reel(listeReseau[-1+i].a1y);
  litdansstring_reel(listeReseau[-1+i].a2x);
  litdansstring_reel(listeReseau[-1+i].a2y);
  litdansstring_entier(listeReseau[-1+i].epaisseur);
  litdansstring_couleur(listeReseau[-1+i].couleur);
  litdansstring_reel(listeReseau[-1+i].pas);
   litdansstring_string(s);
  listeReseau[-1+i].entransmission:=(s='TRUE');
  litdansstring_string(s);
  listeReseau[-1+i].hachures:=(s='TRUE');
  litdansstring_entier(listeReseau[-1+i].ordre_min);
  litdansstring_entier(listeReseau[-1+i].ordre_max);
 if not( listeReseau[-1+i].create(listeReseau[-1+i].a1x,listeReseau[-1+i].a1y,
  listeReseau[-1+i].a2x,listeReseau[-1+i].a2y,listeReseau[-1+i].epaisseur,
  listeReseau[-1+i].couleur,listeReseau[-1+i].pas,listeReseau[-1+i].entransmission,listeReseau[-1+i].hachures,
  listeReseau[-1+i].ordre_min,listeReseau[-1+i].ordre_max)) then
  raise einouterror.Create('erreur');
  end;
  goto 12345; end;




  if s1='Nombre de miroirs concaves paraxiaux:' then begin
   litdansstring_entier(newnombremscopa);
  retaillepmscopa(listemscopa,nombremscopa,newnombremscopa);
  nombremscopa:=newnombremscopa;
  if nombremscopa>0 then
     for i:=1 to nombremscopa do begin
     litdansstring_string(s);
  litdansstring_reel(listemscopa[-1+i].a1x);
  litdansstring_reel(listemscopa[-1+i].a1y);
  litdansstring_reel(listemscopa[-1+i].a2x);
  litdansstring_reel(listemscopa[-1+i].a2y);
  litdansstring_reel(listemscopa[-1+i].focale);
  litdansstring_entier(listemscopa[-1+i].epaisseur);
  litdansstring_couleur(listemscopa[-1+i].couleur);
  litdansstring_couleur(listemscopa[-1+i].couleuraxe);
  litdansstring_string(s);
  listemscopa[-1+i].hachures:=(s='TRUE');
  litdansstring_string(s);
  listemscopa[-1+i].axefocal:=(s='TRUE');
  litdansstring_string(s);
  listemscopa[-1+i].trou:=(s='TRUE');
   litdansstring_reel(listemscopa[-1+i].diametretrou);
 if not( listemscopa[-1+i].create2(listemscopa[-1+i].a1x,listemscopa[-1+i].a1y,
  listemscopa[-1+i].a2x,listemscopa[-1+i].a2y,listemscopa[-1+i].focale,
  listemscopa[-1+i].epaisseur,
  listemscopa[-1+i].couleur,listemscopa[-1+i].couleuraxe,
  listemscopa[-1+i].hachures,listemscopa[-1+i].axefocal,
  listemscopa[-1+i].trou,listemscopa[-1+i].diametretrou))then
  raise einouterror.Create('erreur');
  end;
    goto 12345; end;

    if s1='Nombre de miroirs convexes paraxiaux:'
  then begin
   litdansstring_entier(newnombremscepa);
  retaillepmscepa(listemscepa,nombremscepa,newnombremscepa);
  nombremscepa:=newnombremscepa;
  if nombremscepa>0 then
     for i:=1 to nombremscepa do begin
     litdansstring_string(s);
  litdansstring_reel(listemscepa[-1+i].a1x);
  litdansstring_reel(listemscepa[-1+i].a1y);
  litdansstring_reel(listemscepa[-1+i].a2x);
  litdansstring_reel(  listemscepa[-1+i].a2y);
  litdansstring_reel(listemscepa[-1+i].focale);
  litdansstring_entier(listemscepa[-1+i].epaisseur);
  litdansstring_couleur( listemscepa[-1+i].couleur);
  litdansstring_couleur(listemscepa[-1+i].couleuraxe);
  litdansstring_string(s);
  listemscepa[-1+i].hachures:=(s='TRUE');
  litdansstring_string(s);
  listemscepa[-1+i].axefocal:=(s='TRUE');
   litdansstring_string(s);
  listemscepa[-1+i].trou:=(s='TRUE');
  litdansstring_reel(listemscepa[-1+i].diametretrou);
 if not( listemscepa[-1+i].create2(listemscepa[-1+i].a1x,listemscepa[-1+i].a1y,
  listemscepa[-1+i].a2x,listemscepa[-1+i].a2y,listemscepa[-1+i].focale,
  listemscepa[-1+i].epaisseur,
  listemscepa[-1+i].couleur,listemscepa[-1+i].couleuraxe,
  listemscepa[-1+i].hachures,listemscepa[-1+i].axefocal,
  listemscepa[-1+i].trou,listemscepa[-1+i].diametretrou))then
  raise einouterror.Create('erreur');
  end;
   goto 12345; end;

   if s1='Nombre de miroirs concaves reels:' then begin
  litdansstring_entier(newnombremscore);
  retaillepmscore(listemscore,nombremscore,newnombremscore);
  nombremscore:=newnombremscore;
  if nombremscore>0 then
     for i:=1 to nombremscore do begin
     litdansstring_string(s);
  litdansstring_reel(listemscore[-1+i].a1x);
  litdansstring_reel(listemscore[-1+i].a1y);
  litdansstring_reel(listemscore[-1+i].a2x);
  litdansstring_reel(listemscore[-1+i].a2y);
  litdansstring_reel(listemscore[-1+i].rayoncourbure);
  litdansstring_entier(listemscore[-1+i].epaisseur);
  litdansstring_couleur(listemscore[-1+i].couleur);
  litdansstring_couleur(listemscore[-1+i].couleuraxe);
  litdansstring_string(s);
  listemscore[-1+i].hachures:=(s='TRUE');
  litdansstring_string(s);
  listemscore[-1+i].axefocal:=(s='TRUE');
  litdansstring_string(s);
  listemscore[-1+i].aigu:=(s='TRUE');
  litdansstring_string(s);
  listemscore[-1+i].trou:=(s='TRUE');
  litdansstring_reel(listemscore[-1+i].diametretrou);
if not(  listemscore[-1+i].create2(listemscore[-1+i].a1x,listemscore[-1+i].a1y,
  listemscore[-1+i].a2x,listemscore[-1+i].a2y,listemscore[-1+i].rayoncourbure,
  listemscore[-1+i].epaisseur,
  listemscore[-1+i].couleur,listemscore[-1+i].couleuraxe,
  listemscore[-1+i].hachures,listemscore[-1+i].axefocal,listemscore[-1+i].aigu,
  listemscore[-1+i].trou,listemscore[-1+i].diametretrou))then
  raise einouterror.Create('erreur');
  end;
   goto 12345; end;

   if s1='Nombre de miroirs convexes reels:' then begin
    litdansstring_entier(newnombremscere);
  retaillepmscere(listemscere,nombremscere,newnombremscere);
  nombremscere:=newnombremscere;
  if nombremscere>0 then
     for i:=1 to nombremscere do begin
     litdansstring_string(s);
  litdansstring_reel(listemscere[-1+i].a1x);
  litdansstring_reel(listemscere[-1+i].a1y);
  litdansstring_reel(listemscere[-1+i].a2x);
  litdansstring_reel( listemscere[-1+i].a2y);
  litdansstring_reel(listemscere[-1+i].rayoncourbure);
  litdansstring_entier(listemscere[-1+i].epaisseur);
  litdansstring_couleur(  listemscere[-1+i].couleur);
  litdansstring_couleur(listemscere[-1+i].couleuraxe);
  litdansstring_string(s);
  listemscere[-1+i].hachures:=(s='TRUE');
  litdansstring_string(s);
  listemscere[-1+i].axefocal:=(s='TRUE');
  litdansstring_string(s);
  listemscere[-1+i].aigu:=(s='TRUE');
  litdansstring_string(s);
  listemscere[-1+i].trou:=(s='TRUE');
  litdansstring_reel(listemscere[-1+i].diametretrou);
 if not( listemscere[-1+i].create2(listemscere[-1+i].a1x,listemscere[-1+i].a1y,
  listemscere[-1+i].a2x,listemscere[-1+i].a2y,listemscere[-1+i].rayoncourbure,
  listemscere[-1+i].epaisseur,
  listemscere[-1+i].couleur,listemscere[-1+i].couleuraxe,
  listemscere[-1+i].hachures,listemscere[-1+i].axefocal,listemscere[-1+i].aigu,
  listemscere[-1+i].trou,listemscere[-1+i].diametretrou))then
  raise einouterror.Create('erreur');
  end;
  goto 12345; end;

   if s1='Nombre de lentiles minces convergentes:' then begin
   litdansstring_entier(newnombrelmc);
  retailleplmc(listelmc,nombrelmc,newnombrelmc);
  nombrelmc:=newnombrelmc;
  if nombrelmc>0 then
     for i:=1 to nombrelmc do begin
     litdansstring_string(s);
  litdansstring_reel(listelmc[-1+i].a1x);
  litdansstring_reel(listelmc[-1+i].a1y);
  litdansstring_reel(listelmc[-1+i].a2x);
  litdansstring_reel( listelmc[-1+i].a2y);
  litdansstring_reel(listelmc[-1+i].focale);
  litdansstring_entier(listelmc[-1+i].epaisseur);
  litdansstring_couleur(listelmc[-1+i].couleur);
  litdansstring_couleur(listelmc[-1+i].couleuraxe);
  litdansstring_string(s);
  listelmc[-1+i].axefocal:=(s='TRUE');
  if not(listelmc[-1+i].create2(listelmc[-1+i].a1x,listelmc[-1+i].a1y,
  listelmc[-1+i].a2x,listelmc[-1+i].a2y,listelmc[-1+i].focale,
  listelmc[-1+i].epaisseur,
  listelmc[-1+i].couleur,listelmc[-1+i].couleuraxe,
  listelmc[-1+i].axefocal))then
  raise einouterror.Create('erreur');
  end;
  goto 12345; end;

  if s1='Nombre de lentiles minces divergentes:' then begin
  litdansstring_entier(newnombrelmd);
  retailleplmd(listelmd,nombrelmd,newnombrelmd);
  nombrelmd:=newnombrelmd;
  if nombrelmd>0 then
     for i:=1 to nombrelmd do begin
     litdansstring_string(s);
  litdansstring_reel(listelmd[-1+i].a1x);
  litdansstring_reel(listelmd[-1+i].a1y);
  litdansstring_reel(listelmd[-1+i].a2x);
  litdansstring_reel( listelmd[-1+i].a2y);
  litdansstring_reel(listelmd[-1+i].focale);
  litdansstring_entier(listelmd[-1+i].epaisseur);
  litdansstring_couleur( listelmd[-1+i].couleur);
  litdansstring_couleur(listelmd[-1+i].couleuraxe);
  litdansstring_string(s);
  listelmd[-1+i].axefocal:=(s='TRUE');
 if not( listelmd[-1+i].create2(listelmd[-1+i].a1x,listelmd[-1+i].a1y,
  listelmd[-1+i].a2x,listelmd[-1+i].a2y,listelmd[-1+i].focale,
  listelmd[-1+i].epaisseur,
  listelmd[-1+i].couleur,listelmd[-1+i].couleuraxe,
  listelmd[-1+i].axefocal))then
  raise einouterror.Create('erreur');
  end;
   goto 12345; end;

   if s1='Nombre de rayons:' then begin
    litdansstring_entier(newnombrerayon);
  retailleprayon(listerayon,nombrerayon,newnombrerayon);
  nombrerayon:=newnombrerayon;
  if nombrerayon>0 then
     for i:=1 to nombrerayon do begin
     litdansstring_string(s);
  litdansstring_reel(listerayon[-1+i].ax);
  litdansstring_reel(listerayon[-1+i].ay);
  litdansstring_reel(listerayon[-1+i].kx);
  litdansstring_reel( listerayon[-1+i].ky);
  litdansstring_couleur(listerayon[-1+i].couleur);
  litdansstring_entier(listerayon[-1+i].epaisseur);
  litdansstring_entier(listerayon[-1+i].nombremaxenfant);
  litdansstring_string(s);
  listerayon[-1+i].vav:=s;
   litdansstring_string(s);
  listerayon[-1+i].vaa:=s;
  litdansstring_string(s);
   litdansstring_entier(listerayon[-1+i].maxsegment);
if not(  listerayon[-1+i].create(listerayon[-1+i].ax,listerayon[-1+i].ay,
  listerayon[-1+i].kx,listerayon[-1+i].ky, listerayon[-1+i].couleur,
  listerayon[-1+i].epaisseur,
  listerayon[-1+i].vav,listerayon[-1+i].vaa,
 listerayon[-1+i].nombremaxenfant,listerayon[-1+i].maxsegment  ))then
  raise einouterror.Create('erreur');
    end;
  goto 12345; end;



   if s1='Nombre de sources ponctuelles:' then begin
   litdansstring_entier(newnombresourceponctuelle);
  retaillepsourceponctuelle(listesourceponctuelle,nombresourceponctuelle,newnombresourceponctuelle);
  nombresourceponctuelle:=newnombresourceponctuelle;
  if nombresourceponctuelle>0 then
  for i:=1 to nombresourceponctuelle do begin
  litdansstring_string(s);
  litdansstring_entier(listesourceponctuelle[-1+i].nombrederayon);
  litdansstring_reel(  listesourceponctuelle[-1+i].sx);
  litdansstring_reel(   listesourceponctuelle[-1+i].sy);
  litdansstring_reel(  listesourceponctuelle[-1+i].a1x);
  litdansstring_reel(listesourceponctuelle[-1+i].a1y);
  litdansstring_reel(listesourceponctuelle[-1+i].a2x);
  litdansstring_reel( listesourceponctuelle[-1+i].a2y);
   litdansstring_couleur(listesourceponctuelle[-1+i].couleur);
   litdansstring_entier(listesourceponctuelle[-1+i].epaisseur);
   litdansstring_entier(listesourceponctuelle[-1+i].maxenfantparrayon);
   litdansstring_string(s);
listesourceponctuelle[-1+i].vav:=s;
litdansstring_string(s);
listesourceponctuelle[-1+i].vaa:=s;
litdansstring_string(s);
listesourceponctuelle[-1+i].sourcevirtuelle:=(s='TRUE');
litdansstring_string(s);
listesourceponctuelle[-1+i].tracersurfacesonde:=(s='TRUE');
litdansstring_string(s);
listesourceponctuelle[-1+i].listechemins:=s;
litdansstring_string(s);
   litdansstring_entier(listesourceponctuelle[-1+i].maxsegment);
if not(listesourceponctuelle[-1+i].create(listesourceponctuelle[-1+i].nombrederayon,
  listesourceponctuelle[-1+i].sx,
   listesourceponctuelle[-1+i].sy,
  listesourceponctuelle[-1+i].a1x,
  listesourceponctuelle[-1+i].a1y,
  listesourceponctuelle[-1+i].a2x,
  listesourceponctuelle[-1+i].a2y,
   listesourceponctuelle[-1+i].couleur,
   listesourceponctuelle[-1+i].epaisseur,listesourceponctuelle[-1+i].vav,
   listesourceponctuelle[-1+i].vaa,
   listesourceponctuelle[-1+i].maxenfantparrayon,listesourceponctuelle[-1+i].sourcevirtuelle,
   listesourceponctuelle[-1+i].tracersurfacesonde,listesourceponctuelle[-1+i].listechemins,
   listesourceponctuelle[-1+i].maxsegment))then
  raise einouterror.Create('erreur');
    end;
     goto 12345; end;


     if s1='Nombre d''ondes planes:' then begin
     litdansstring_entier(newnombreondeplane);
  retaillepondeplane(listeondeplane,nombreondeplane,newnombreondeplane);
  nombreondeplane:=newnombreondeplane;
  if nombreondeplane>0 then
  for i:=1 to nombreondeplane do begin
  litdansstring_string(s);
  litdansstring_entier(listeondeplane[-1+i].nombrederayon);
  litdansstring_reel(listeondeplane[-1+i].sx);
   litdansstring_reel(listeondeplane[-1+i].sy);
  litdansstring_reel(listeondeplane[-1+i].a1x);
   litdansstring_reel(listeondeplane[-1+i].a1y);
   litdansstring_reel(listeondeplane[-1+i].a2x);
   litdansstring_reel(listeondeplane[-1+i].a2y);
    litdansstring_couleur(listeondeplane[-1+i].couleur);
    litdansstring_entier(listeondeplane[-1+i].epaisseur);
    litdansstring_entier(listeondeplane[-1+i].maxenfantparrayon);
   litdansstring_string(s);
listeondeplane[-1+i].vav:=s;
 litdansstring_string(s);
listeondeplane[-1+i].vaa:=s;
litdansstring_string(s);
listeondeplane[-1+i].tracersurfacesonde:=(s='TRUE');
litdansstring_string(s);
listeondeplane[-1+i].listechemins:=s;
litdansstring_string(s);
   litdansstring_entier(listeondeplane[-1+i].maxsegment);
if not(listeondeplane[-1+i].create(listeondeplane[-1+i].nombrederayon,
  listeondeplane[-1+i].sx,
   listeondeplane[-1+i].sy,
  listeondeplane[-1+i].a1x,
  listeondeplane[-1+i].a1y,
  listeondeplane[-1+i].a2x,
  listeondeplane[-1+i].a2y,
   listeondeplane[-1+i].couleur,
   listeondeplane[-1+i].epaisseur,listeondeplane[-1+i].vav,
   listeondeplane[-1+i].vaa,listeondeplane[-1+i].maxenfantparrayon,
   listeondeplane[-1+i].tracersurfacesonde,
   listeondeplane[-1+i].listechemins,
   listeondeplane[-1+i].maxsegment))then
  raise einouterror.Create('erreur');
end;
  goto 12345; end;


 if s1='Nombre d''ecrans:' then begin
 litdansstring_entier(newnombreecran);
  retaillepecran(listeecran,nombreecran,newnombreecran);
  nombreecran:=newnombreecran;
  if nombreecran>0 then
     for i:=1 to nombreecran do begin
     litdansstring_string(s);
   litdansstring_reel(listeecran[-1+i].a1x);
    litdansstring_reel(listeecran[-1+i].a1y);
     litdansstring_reel(listeecran[-1+i].a2x);
      litdansstring_reel(listeecran[-1+i].a2y);
       litdansstring_entier(listeecran[-1+i].epaisseur);
        litdansstring_couleur(listeecran[-1+i].couleur);
 if not( listeecran[-1+i].create(listeecran[-1+i].a1x,listeecran[-1+i].a1y,
  listeecran[-1+i].a2x,listeecran[-1+i].a2y,listeecran[-1+i].epaisseur,
  listeecran[-1+i].couleur))then
  raise einouterror.Create('erreur');

  end;
    goto 12345; end;


 if s1='Nombre de diaphragmes:' then begin
  litdansstring_entier(newnombrediaphragme);
  retaillepdiaphragme(listediaphragme,nombrediaphragme,newnombrediaphragme);
  nombrediaphragme:=newnombrediaphragme;
  if nombrediaphragme>0 then
     for i:=1 to nombrediaphragme do begin
     litdansstring_string(s);
   litdansstring_reel(listediaphragme[-1+i].cx);
    litdansstring_reel(listediaphragme[-1+i].cy);
     litdansstring_reel(listediaphragme[-1+i].anglepolaire);
      litdansstring_reel( listediaphragme[-1+i].rint);
       litdansstring_reel(listediaphragme[-1+i].rext);
        litdansstring_entier( listediaphragme[-1+i].epaisseur);
         litdansstring_couleur(listediaphragme[-1+i].couleur);
 if not( listediaphragme[-1+i].create2(listediaphragme[-1+i].cx,
 listediaphragme[-1+i].cy,listediaphragme[-1+i].anglepolaire,
  listediaphragme[-1+i].rint,listediaphragme[-1+i].rext,
  listediaphragme[-1+i].couleur,listediaphragme[-1+i].epaisseur))then
  raise einouterror.Create('erreur');

  end;
   goto 12345; end;



  if s1='Nombre de lames semi reflechissantes:' then begin
  litdansstring_entier(newnombrelsr);
  retailleplsr(listelsr,nombrelsr,newnombrelsr);
  nombrelsr:=newnombrelsr;
  if nombrelsr>0 then
     for i:=1 to nombrelsr do begin
     litdansstring_string(s);
   litdansstring_reel(listelsr[-1+i].a1x);
    litdansstring_reel(listelsr[-1+i].a1y);
     litdansstring_reel(listelsr[-1+i].a2x);
      litdansstring_reel( listelsr[-1+i].a2y);
       litdansstring_entier(listelsr[-1+i].epaisseur);
        litdansstring_couleur(listelsr[-1+i].couleur);
 if not( listelsr[-1+i].create(listelsr[-1+i].a1x,listelsr[-1+i].a1y,
  listelsr[-1+i].a2x,listelsr[-1+i].a2y,listelsr[-1+i].epaisseur,
  listelsr[-1+i].couleur))then
  raise einouterror.Create('erreur');
  end;
   goto 12345; end;


     if s1='Nombre de polyhedres:' then begin
   litdansstring_entier(newnombrepolyhedre);
   retailleppolyhedre(listepolyhedre,nombrepolyhedre,newnombrepolyhedre);
  nombrepolyhedre:=newnombrepolyhedre;
  if nombrepolyhedre>0 then
  for i:=1 to nombrepolyhedre do begin
  litdansstring_string(s);
  litdansstring_entier(listepolyhedre[-1+i].nombresommet);
  for j:=1 to listepolyhedre[-1+i].nombresommet do  begin
   litdansstring_reel(listepolyhedre[-1+i].messommets[j].ax);
    litdansstring_reel(listepolyhedre[-1+i].messommets[j].ay);
    end;
   litdansstring_reel(listepolyhedre[-1+i].indicerouge);
    litdansstring_reel(listepolyhedre[-1+i].indicebleu);
     litdansstring_reel(listepolyhedre[-1+i].indicevert);
  litdansstring_couleur(listepolyhedre[-1+i].couleurbord);
   litdansstring_couleur(listepolyhedre[-1+i].couleurfond);
  litdansstring_string(s);
  if s='1' then  listepolyhedre[-1+i].reflechientrant:=toujoursreflechi;
   if s='2' then  listepolyhedre[-1+i].reflechientrant:=jamaisreflechi;
   if s='3' then  listepolyhedre[-1+i].reflechientrant:=reflechisirefracte;
   if s='4' then  listepolyhedre[-1+i].reflechientrant:=reflechisipasrefracte;
  litdansstring_string(s);
  if s='1' then  listepolyhedre[-1+i].reflechisortant:=toujoursreflechi;
   if s='2' then  listepolyhedre[-1+i].reflechisortant:=jamaisreflechi;
   if s='3' then  listepolyhedre[-1+i].reflechisortant:=reflechisirefracte;
   if s='4' then  listepolyhedre[-1+i].reflechisortant:=reflechisipasrefracte;
   if not(listepolyhedre[-1+i].create(listepolyhedre[-1+i].nombresommet,listepolyhedre[-1+i].messommets,
   listepolyhedre[-1+i].indicerouge,listepolyhedre[-1+i].indicebleu,listepolyhedre[-1+i].indicevert,
 listepolyhedre[-1+i].couleurbord,listepolyhedre[-1+i].couleurfond,
  listepolyhedre[-1+i].reflechientrant,listepolyhedre[-1+i].reflechisortant))then
  raise einouterror.Create('erreur');
  end;
   goto 12345; end;


   if s1='Nombre de prismes:' then begin
   litdansstring_entier(newnombreprisme);
   retaillepprisme(listeprisme,nombreprisme,newnombreprisme);
  nombreprisme:=newnombreprisme;
  if nombreprisme>0 then
  for i:=1 to nombreprisme do begin
  litdansstring_string(s);
   litdansstring_reel(listeprisme[-1+i].gx);
    litdansstring_reel(listeprisme[-1+i].gy);
   litdansstring_reel(listeprisme[-1+i].ax);
    litdansstring_reel(listeprisme[-1+i].ay);
    litdansstring_reel(listeprisme[-1+i].bx);
    litdansstring_reel(listeprisme[-1+i].by);
   litdansstring_reel(listeprisme[-1+i].indicerouge);
    litdansstring_reel(listeprisme[-1+i].indicebleu);
     litdansstring_reel(listeprisme[-1+i].indicevert);
  litdansstring_couleur(listeprisme[-1+i].couleurbord);
   litdansstring_couleur(listeprisme[-1+i].couleurfond);
  litdansstring_string(s);
  if s='1' then  listeprisme[-1+i].reflechientrant:=toujoursreflechi;
   if s='2' then  listeprisme[-1+i].reflechientrant:=jamaisreflechi;
   if s='3' then  listeprisme[-1+i].reflechientrant:=reflechisirefracte;
   if s='4' then  listeprisme[-1+i].reflechientrant:=reflechisipasrefracte;
  litdansstring_string(s);
  if s='1' then  listeprisme[-1+i].reflechisortant:=toujoursreflechi;
   if s='2' then  listeprisme[-1+i].reflechisortant:=jamaisreflechi;
   if s='3' then  listeprisme[-1+i].reflechisortant:=reflechisirefracte;
   if s='4' then  listeprisme[-1+i].reflechisortant:=reflechisipasrefracte;
   if not(listeprisme[-1+i].create(listeprisme[-1+i].gx,listeprisme[-1+i].gy,
   listeprisme[-1+i].ax,listeprisme[-1+i].ay,listeprisme[-1+i].bx,listeprisme[-1+i].by,
   listeprisme[-1+i].indicerouge,listeprisme[-1+i].indicebleu,listeprisme[-1+i].indicevert,
 listeprisme[-1+i].couleurbord,listeprisme[-1+i].couleurfond,
  listeprisme[-1+i].reflechientrant,listeprisme[-1+i].reflechisortant))then
  raise einouterror.Create('erreur');
  end;
   goto 12345; end;






 if s1='Nombre de polycercles:' then begin
  litdansstring_entier(newnombrepolycercle);
   retailleppolycercle(listepolycercle,nombrepolycercle,newnombrepolycercle);
  nombrepolycercle:=newnombrepolycercle;
  if nombrepolycercle>0 then
  for i:=1 to nombrepolycercle do begin
  litdansstring_string(s);
  litdansstring_entier(listepolycercle[-1+i].nombresommet);
  for j:=1 to listepolycercle[-1+i].nombresommet do begin
   litdansstring_reel(listepolycercle[-1+i].messommets[j].ax);
    litdansstring_reel(listepolycercle[-1+i].messommets[j].ay);
    end;
   for j:=1 to listepolycercle[-1+i].nombresommet do  begin
  litdansstring_reel(listepolycercle[-1+i].mess[j].ax);
   litdansstring_reel(listepolycercle[-1+i].mess[j].ay);
   end;
   for j:=1 to listepolycercle[-1+i].nombresommet do  begin
   litdansstring_string(s);
  listepolycercle[-1+i].rectiligne[j]:=(s='TRUE');
  end;
   litdansstring_reel(listepolycercle[-1+i].indicerouge);
    litdansstring_reel(listepolycercle[-1+i].indicebleu);
     litdansstring_reel(listepolycercle[-1+i].indicevert);
   litdansstring_couleur(listepolycercle[-1+i].couleurbord);
    litdansstring_couleur(listepolycercle[-1+i].couleurfond);
  litdansstring_string(s);
  if s='1' then  listepolycercle[-1+i].reflechientrant:=toujoursreflechi;
   if s='2' then  listepolycercle[-1+i].reflechientrant:=jamaisreflechi;
   if s='3' then  listepolycercle[-1+i].reflechientrant:=reflechisirefracte;
   if s='4' then  listepolycercle[-1+i].reflechientrant:=reflechisipasrefracte;
  litdansstring_string(s);
  if s='1' then  listepolycercle[-1+i].reflechisortant:=toujoursreflechi;
   if s='2' then  listepolycercle[-1+i].reflechisortant:=jamaisreflechi;
   if s='3' then  listepolycercle[-1+i].reflechisortant:=reflechisirefracte;
   if s='4' then  listepolycercle[-1+i].reflechisortant:=reflechisipasrefracte;
   if not(listepolycercle[-1+i].create(listepolycercle[-1+i].nombresommet,
   listepolycercle[-1+i].messommets,listepolycercle[-1+i].mess,listepolycercle[-1+i].rectiligne,
   listepolycercle[-1+i].indicerouge,listepolycercle[-1+i].indicebleu,listepolycercle[-1+i].indicevert,
 listepolycercle[-1+i].couleurbord,listepolycercle[-1+i].couleurfond,
  listepolycercle[-1+i].reflechientrant,listepolycercle[-1+i].reflechisortant))then
  raise einouterror.Create('erreur');
  end;
    goto 12345; end;




  if s1='Nombre de spheres:' then begin
  litdansstring_entier(newnombresphere);
  retaillepsphere(listesphere,nombresphere,newnombresphere);
  nombresphere:=newnombresphere;
  if nombresphere>0 then
     for i:=1 to nombresphere do begin
     litdansstring_string(s);
  litdansstring_reel(listesphere[-1+i].cx);
   litdansstring_reel(listesphere[-1+i].cy);
    litdansstring_reel(listesphere[-1+i].rayon);
     litdansstring_reel(  listesphere[-1+i].indicerouge);
      litdansstring_reel(listesphere[-1+i].indicebleu);
       litdansstring_reel(listesphere[-1+i].indicevert);
   litdansstring_couleur(listesphere[-1+i].couleurbord);
    litdansstring_couleur(listesphere[-1+i].couleurfond);
   litdansstring_string(s);
  if s='1' then  listesphere[-1+i].reflechientrant:=toujoursreflechi;
   if s='2' then  listesphere[-1+i].reflechientrant:=jamaisreflechi;
   if s='3' then  listesphere[-1+i].reflechientrant:=reflechisirefracte;
   if s='4' then  listesphere[-1+i].reflechientrant:=reflechisipasrefracte;
  litdansstring_string(s);
  if s='1' then  listesphere[-1+i].reflechisortant:=toujoursreflechi;
   if s='2' then  listesphere[-1+i].reflechisortant:=jamaisreflechi;
   if s='3' then  listesphere[-1+i].reflechisortant:=reflechisirefracte;
   if s='4' then  listesphere[-1+i].reflechisortant:=reflechisipasrefracte;
 if not(listesphere [-1+i].create(listesphere[-1+i].cx,listesphere[-1+i].cy,listesphere[-1+i].rayon,
   listesphere[-1+i].indicerouge,listesphere[-1+i].indicebleu,listesphere[-1+i].indicevert,
 listesphere[-1+i].couleurbord,listesphere[-1+i].couleurfond,
  listesphere[-1+i].reflechientrant,listesphere[-1+i].reflechisortant))then
  raise einouterror.Create('erreur');
    end;
     goto 12345; end;




    if s1='Nombre de miroirs coniques:' then begin
    litdansstring_entier(newnombremiroirconique);
    retaillepmiroirconique(listemiroirconique,nombremiroirconique,newnombremiroirconique);
    nombremiroirconique:=newnombremiroirconique;
    if nombremiroirconique>0 then
    for i:=1 to nombremiroirconique do begin
    litdansstring_string(s);
     litdansstring_reel(listemiroirconique[-1+i].fx);
      litdansstring_reel(listemiroirconique[-1+i].fy);
       litdansstring_reel( listemiroirconique[-1+i].excentricite);
        litdansstring_reel(listemiroirconique[-1+i].parametre);
         litdansstring_reel(  listemiroirconique[-1+i].theta0);
          litdansstring_reel(listemiroirconique[-1+i].theta1);
           litdansstring_reel(listemiroirconique[-1+i].theta2);
    litdansstring_couleur(listemiroirconique[-1+i].couleur);
     litdansstring_couleur(listemiroirconique[-1+i].couleuraxe);
    litdansstring_string(s);
  listemiroirconique[-1+i].hachures:=(s='TRUE');
  litdansstring_string(s);
  listemiroirconique[-1+i].axefocal:=(s='TRUE');
  litdansstring_string(s);
  listemiroirconique[-1+i].concave:=(s='TRUE');
 if not( listemiroirconique[-1+i].create2(listemiroirconique[-1+i].fx,
  listemiroirconique[-1+i].fy,listemiroirconique[-1+i].theta0,
  listemiroirconique[-1+i].theta1,listemiroirconique[-1+i].theta2,
  listemiroirconique[-1+i].excentricite,listemiroirconique[-1+i].parametre,
  listemiroirconique[-1+i].concave,listemiroirconique[-1+i].couleur,
  listemiroirconique[-1+i].couleuraxe,listemiroirconique[-1+i].hachures,
  listemiroirconique[-1+i].axefocal)) then
  raise einouterror.Create('erreur');
   end;
    goto 12345; end;





   if s1='Nombre de textes:' then begin
   litdansstring_entier(newnombretexte);
   retailleptexteaffiche(listetexteaffiche,nombretexte,newnombretexte);
   nombretexte:=newnombretexte;
  if nombretexte>0 then

   for i:=1 to nombretexte do begin
   litdansstring_string(s);
  litdansstring_shortstring(listetexteaffiche[-1+i].texteluimeme);
   litdansstring_reel(listetexteaffiche[-1+i].x);
    litdansstring_reel(listetexteaffiche[-1+i].y);

   litdansstring_entier(listetexteaffiche[-1+i].policecharset);

     litdansstring_couleur(listetexteaffiche[-1+i].policecolor);
   litdansstring_shortstring(listetexteaffiche[-1+i].policename);
   litdansstring_entier(listetexteaffiche[-1+i].policesize);

   litdansstring_string(s);
   listetexteaffiche[-1+i].policefsbold:=(s='fsbold');

   litdansstring_string(s);
   listetexteaffiche[-1+i].policefsitalic:=(s='fsitalic');

   litdansstring_string(s);
   listetexteaffiche[-1+i].policefsunderline:=(s='fsunderline');

   litdansstring_string(s);
   listetexteaffiche[-1+i].policefsstrikeout:=(s='fsstrikeout');
   litdansstring_string(s);
   if s='fpDefault' then listetexteaffiche[-1+i].policepitch:=fpDefault;
   if s='fpVariable' then listetexteaffiche[-1+i].policepitch:=fpVariable;
   if s='fpFixed'   then listetexteaffiche[-1+i].policepitch:=fpFixed;

if not(listetexteaffiche[-1+i].create(listetexteaffiche[-1+i].texteluimeme,
listetexteaffiche[-1+i].policename,listetexteaffiche[-1+i].policecharset,
listetexteaffiche[-1+i].policecolor,
listetexteaffiche[-1+i].policesize,
listetexteaffiche[-1+i].policepitch,
listetexteaffiche[-1+i].policefsbold,
listetexteaffiche[-1+i].policefsitalic,
listetexteaffiche[-1+i].policefsunderline,
listetexteaffiche[-1+i].policefsstrikeout,listetexteaffiche[-1+i].x,
listetexteaffiche[-1+i].y)) then
  raise einouterror.Create('erreur');
   end;
     goto 12345; end;






   if s1='Nombre de lentilles epaisses:' then begin
   litdansstring_entier(newnombrelec);
  retailleplec(listelec,nombrelec,newnombrelec);
  nombrelec:=newnombrelec;
  if nombrelec>0 then
     for i:=1 to nombrelec do begin
     litdansstring_string(s);
   litdansstring_reel(listelec[-1+i].s1x);
    litdansstring_reel(listelec[-1+i].s1y);
     litdansstring_reel(listelec[-1+i].s2x);
      litdansstring_reel(listelec[-1+i].s2y);
       litdansstring_reel(listelec[-1+i].r1);
        litdansstring_reel(listelec[-1+i].r2);
         litdansstring_reel(listelec[-1+i].diametre);
  litdansstring_couleur(listelec[-1+i].couleurfond);
   litdansstring_couleur(listelec[-1+i].couleuraxe);
  litdansstring_string(s);
  listelec[-1+i].axefocal:=(s='TRUE');
   litdansstring_reel(listelec[-1+i].indicerouge);
    litdansstring_reel(listelec[-1+i].indicebleu);
     litdansstring_reel(listelec[-1+i].indicevert);

   litdansstring_string(s);
  if s='1' then  listelec[-1+i].reflechientrant:=toujoursreflechi;
   if s='2' then  listelec[-1+i].reflechientrant:=jamaisreflechi;
   if s='3' then  listelec[-1+i].reflechientrant:=reflechisirefracte;
   if s='4' then  listelec[-1+i].reflechientrant:=reflechisipasrefracte;
  litdansstring_string(s);
  if s='1' then  listelec[-1+i].reflechisortant:=toujoursreflechi;
   if s='2' then  listelec[-1+i].reflechisortant:=jamaisreflechi;
   if s='3' then  listelec[-1+i].reflechisortant:=reflechisirefracte;
   if s='4' then  listelec[-1+i].reflechisortant:=reflechisipasrefracte;
 if not(listelec [-1+i].create(listelec[-1+i].s1x,listelec[-1+i].s1y,listelec[-1+i].s2x,
 listelec[-1+i].s2y,listelec[-1+i].r1,listelec[-1+i].r2,listelec[-1+i].diametre,
 listelec[-1+i].couleurfond,listelec[-1+i].couleuraxe,listelec[-1+i].axefocal,
   listelec[-1+i].indicerouge,listelec[-1+i].indicebleu,listelec[-1+i].indicevert,
    listelec[-1+i].reflechientrant,listelec[-1+i].reflechisortant))then
  raise einouterror.Create('erreur');
    end;
      goto 12345; end;



    if s1='Nombre d''angles:' then begin
    litdansstring_entier(newnombreangle);
  retaillepangle(listeangle,nombreangle,newnombreangle);
  nombreangle:=newnombreangle;
  if nombreangle>0 then
     for i:=1 to nombreangle do begin
     litdansstring_string(s);
  litdansstring_reel(listeangle[-1+i].cx);
   litdansstring_reel(listeangle[-1+i].cy);
    litdansstring_reel(listeangle[-1+i].a1x);
     litdansstring_reel( listeangle[-1+i].a1y);
      litdansstring_reel(listeangle[-1+i].a2x);
       litdansstring_reel(listeangle[-1+i].a2y);

 if not(listeangle [-1+i].create(listeangle[-1+i].cx,listeangle[-1+i].cy,listeangle[-1+i].a1x,
 listeangle[-1+i].a1y,listeangle[-1+i].a2x,listeangle[-1+i].a2y))then
  raise einouterror.Create('erreur');
    end;
      goto 12345; end;


if s1='Nombre de traits:' then begin
    litdansstring_entier(newnombrefleche);
  retaillepfleche(listefleche,nombrefleche,newnombrefleche);
  nombrefleche:=newnombrefleche;
  if nombrefleche>0 then
     for i:=1 to nombrefleche do begin
     litdansstring_string(s);
     litdansstring_reel(listefleche[-1+i].a1x);
      litdansstring_reel(listefleche[-1+i].a1y);
       litdansstring_reel(listefleche[-1+i].a2x);
        litdansstring_reel(listefleche[-1+i].a2y);
    litdansstring_entier(listefleche[-1+i].epaisseur);
     litdansstring_couleur(listefleche[-1+i].couleur);
    litdansstring_string(s);
    if s='trien' then listefleche[-1+i].ext1:=trien;
     if s='tffleche' then listefleche[-1+i].ext1:=tffleche;
      if s='ttrait' then listefleche[-1+i].ext1:=ttrait;
       if s='trond' then listefleche[-1+i].ext1:=trond;
        if s='tcroix' then listefleche[-1+i].ext1:=tcroix;
         if s='tcroixx' then listefleche[-1+i].ext1:=tcroixx;
          if s='tdisque' then listefleche[-1+i].ext1:=tdisque;
     litdansstring_string(s);
     if s='trien' then listefleche[-1+i].ext2:=trien;
     if s='tffleche' then listefleche[-1+i].ext2:=tffleche;
      if s='ttrait' then listefleche[-1+i].ext2:=ttrait;
       if s='trond' then listefleche[-1+i].ext2:=trond;
        if s='tcroix' then listefleche[-1+i].ext2:=tcroix;
         if s='tcroixx' then listefleche[-1+i].ext2:=tcroixx;
          if s='tdisque' then listefleche[-1+i].ext2:=tdisque;
          litdansstring_string(s);
          if s='pssolid' then listefleche[-1+i].styletrait:=pssolid;
        if s='psdot' then listefleche[-1+i].styletrait:=psdot;
        if s='psdash' then listefleche[-1+i].styletrait:=psdash;
        if s='psdashdot' then listefleche[-1+i].styletrait:=psdashdot;
        if s='psdashdotdot' then listefleche[-1+i].styletrait:=psdashdotdot;
     litdansstring_entier(listefleche[-1+i].taille1);
      litdansstring_entier(listefleche[-1+i].taille2);
     listefleche[-1+i].create(listefleche[-1+i].a1x,listefleche[-1+i].a1y,
     listefleche[-1+i].a2x,listefleche[-1+i].a2y,
     listefleche[-1+i].epaisseur,listefleche[-1+i].couleur,
     listefleche[-1+i].ext1,listefleche[-1+i].ext2,
     listefleche[-1+i].styletrait,
     listefleche[-1+i].taille1,listefleche[-1+i].taille2);
     end;
     goto 12345; end;





if s1='Nombre d''oeils:' then begin
 litdansstring_entier(newnombreoeil);
  retaillepoeil(listeoeil,nombreoeil,newnombreoeil);
  nombreoeil:=newnombreoeil;
  if nombreoeil>0 then
     for i:=1 to nombreoeil do begin
     litdansstring_string(s);
   litdansstring_reel(listeoeil[-1+i].cx);
    litdansstring_reel(listeoeil[-1+i].cy);
     litdansstring_reel(listeoeil[-1+i].ax);
      litdansstring_reel( listeoeil[-1+i].ay);
       litdansstring_entier(listeoeil[-1+i].epaisseur);
        litdansstring_couleur(listeoeil[-1+i].couleur);
 if not( listeoeil[-1+i].create(listeoeil[-1+i].cx,listeoeil[-1+i].cy,
  listeoeil[-1+i].ax,listeoeil[-1+i].ay,listeoeil[-1+i].epaisseur,
  listeoeil[-1+i].couleur))then
  raise einouterror.Create('erreur');
      end;
    goto 12345; end;

     if s1='Nombre de mesures de distances' then begin
 litdansstring_entier(newnombremetre);
 retaillepmetre(listemetre,nombremetre,newnombremetre);
 nombremetre:=newnombremetre;
 if nombremetre>0 then for i:=1 to nombremetre do begin
 litdansstring_string(s);
  litdansstring_reel(listemetre[-1+i].a1x);
   litdansstring_reel(listemetre[-1+i].a1y);
    litdansstring_reel( listemetre[-1+i].a2x);
     litdansstring_reel(listemetre[-1+i].a2y);
 if not( listemetre[-1+i].create(listemetre[-1+i].a1x,listemetre[-1+i].a1y,
  listemetre[-1+i].a2x,listemetre[-1+i].a2y))then
  raise einouterror.Create('erreur');
      end;
       goto 12345; end;


   if s1='Nombre de groupes:' then begin
 litdansstring_entier(newnombregroupe);
 retaillepgroupe(listegroupe,nombregroupe,newnombregroupe);
 nombregroupe:=newnombregroupe;
 if nombregroupe> 0 then for i:=1 to nombregroupe do begin
 listegroupe[-1+i].create;
   litdansstring_string(s); litdansstring_entier(chacha);
  for j:=1 to  chacha do begin
  litdansstring_entier(papa); litdansstring_string(s);
      aze:=mdneant;
   if s='mdoeil' then aze:=mdoeil;
  if s='mddiaphragme' then aze:=mddiaphragme;
  if s='mdtexteaffiche'then aze:=mdtexteaffiche;
  if s='mdmiroirconique' then aze:=mdmiroirconique;
  if s='mdlec' then aze:=mdlec;
  if s='mdfleche' then aze:=mdfleche;
  if s='mdmiroirplan' then aze:=mdmiroirplan;
    if s='mdreseau' then aze:=mdreseau;
  if s='mdecran' then aze:=mdecran;
  if s='mdangle' then aze:=mdangle;
  if s='mdmetre' then aze:=mdmetre;
  if s='mdlsr' then aze:=mdlsr;
  if s='mdmscopa' then aze:=mdmscopa;
  if s='mdlmc' then aze:=mdlmc;
  if s='mdlmd' then aze:=mdlmd;
  if s='mdmscepa' then aze:=mdmscepa;
  if s='mdmscore' then aze:=mdmscore;
  if s='mdmscere' then aze:=mdmscere;
   if s='mdpolyhedre' then aze:=mdpolyhedre;
    if s='mdrayon' then aze:=mdrayon;
     if s='mdsourceponctuelle' then aze:=mdsourceponctuelle;
      if s='mdondeplane' then aze:=mdondeplane;
       if s='mdpolycercle' then aze:=mdpolycercle;
        if s='mdsphere' then aze:=mdsphere;
        if s='mdprisme' then aze:=mdprisme;
        listegroupe[-1+i].add(aze,papa);
         end; {de la boucle chacha}
                end; {de la boucle nombregroupe}
                goto 12345; end;



  if s1='couleur fond simulation' then begin
  litdansstring_couleur(couleurfondsimulation);
    goto 12345; end;

    if s1='empennage' then begin
    litdansstring_string(s);
    litdansstring_entier(positionempennage);
    litdansstring_string(s);
     litdansstring_entier(tailleempennage);
     goto 12345; end;


 12345:
 end; {du while not(eof(f))}


rapportactuel:=(yymax-yymin)/(xxmax-xxmin);
    afficherlagrille1.checked:=tracergrille;
    attractiondelagrille1.Checked:=attractiongrille;
    cochegrille.checked:=tracergrille;
    cocheattraction.Checked:=attractiongrille;
    N7.checked:=optionnormale; cochenormale.checked:=optionnormale;
    Afficherlesangles1.checked:=optionangles;
    cocheangles.checked:=optionangles;
  Rafraichit;
  form1.Caption:=Format(rsEspaceDeTrav2, [floattostr(xxmin), floattostr(xxmax),
    floattostr(yymin), floattostr(yymax)]);


  end;







  procedure sauvevirtuel;
 var  i,j:integer;
   TempStream : TMemoryStream;
 procedure ecritdanststring(s:string);
  begin
  etatactuel[nombre_etats_sauvegardes].Add(s); end;
   procedure ecritdanststring_couleur(s:tcolor);
  begin
  etatactuel[nombre_etats_sauvegardes].Add(inttostr(s)); end;
  procedure ecritdanststring_entier(s:integer);
  begin
  etatactuel[nombre_etats_sauvegardes].Add(inttostr(s)); end;
  procedure ecritdanststring_reel(s:extended);
  begin
  etatactuel[nombre_etats_sauvegardes].Add(floattostr(s)); end;
   procedure ecritdanststring_boolean(s:boolean);
  begin
  if s then etatactuel[nombre_etats_sauvegardes].Add('TRUE') else
  etatactuel[nombre_etats_sauvegardes].add('FALSE'); end;

begin


  form1.refaire1.enabled:=false;
if  nombre_etats_sauvegardes=maxannulation then for i:=2 to  maxannulation do begin
  TempStream := TMemoryStream.Create;

  etatactuel[i].SaveToStream(TempStream);

  TempStream.Position := 0;
  etatactuel[i-1].LoadFromStream( TempStream);


  TempStream.Free;
 end else  begin
 inc(nombre_etats_sauvegardes);

 end;
 niveau_max_sauvegarde:=nombre_etats_sauvegardes;
     if nombre_etats_sauvegardes>=2 then form1.rtablir1.enabled:=true;
    etatactuel[nombre_etats_sauvegardes].Clear;
  ecritdanststring('NE PAS MODIFIER CE FICHIER');
 ecritdanststring('VERSION');
 ecritdanststring_entier(versionprogramme);
 { so on modifie le format des fichiers, il faut modifier le numero de version
 du programme; lors de la lecture
 des fichiuers, il faut adapter la lecture au numero de version, de facon que les
 anciens fichiers puissent etre relus avec les nouvelles versions de programme}
 ecritdanststring_reel(xxmin);
 ecritdanststring_reel(xxmax);
 ecritdanststring_reel(yymin);
 ecritdanststring_reel(yymax);
 ecritdanststring_boolean(respectrapport);
 ecritdanststring('UNITE');
 ecritdanststring(unitelongueur);
 ecritdanststring('OPTIONS GRILLE');
 ecritdanststring_boolean(tracergrille);
 ecritdanststring_boolean(attractiongrille);
 ecritdanststring_reel(grillex);
 ecritdanststring_reel(grilley);
 ecritdanststring('OPTIONS NORMALES');
 ecritdanststring_boolean(optionnormale);
 ecritdanststring_boolean(optionangles);
 ecritdanststring_reel(pourcentagenormale);
 ecritdanststring_couleur(couleurnormale);
 ecritdanststring_entier(epaisseurnormale);
 ecritdanststring_entier(decimalesangles);
 ecritdanststring('OPTIONS SEGMENTS FANTOMES');
 ecritdanststring_boolean(fantomespresents);
 ecritdanststring(listefantomes);
 ecritdanststring('NOMBRE POINTS TRACE SURFACE ONDE');
 ecritdanststring_entier(maxrayonsrecherchesurfacesonde);
 ecritdanststring('INDICES PAR DEFAUT');
    ecritdanststring_reel(indice_vert_par_defaut);
    ecritdanststring_reel(indice_bleu_par_defaut);
    ecritdanststring_reel(indice_rouge_par_defaut);

    ecritdanststring('LAMBDA');
    ecritdanststring_reel(longueur_donde_red);
    ecritdanststring_reel(longueur_donde_blue);
    ecritdanststring_reel(longueur_donde_green);



 ecritdanststring('Nombre de miroirs plans:');

  ecritdanststring_entier(nombremiroirplan);
  if nombremiroirplan>0 then
  for i:=1 to nombremiroirplan do begin
  ecritdanststring('Numero '+inttostr(i));
  ecritdanststring_reel(listemiroirplan[-1+i].a1x);
   ecritdanststring_reel(listemiroirplan[-1+i].a1y);
    ecritdanststring_reel(listemiroirplan[-1+i].a2x);
     ecritdanststring_reel(listemiroirplan[-1+i].a2y);
      ecritdanststring_entier(listemiroirplan[-1+i].epaisseur);
       ecritdanststring_couleur(listemiroirplan[-1+i].couleur);
  ecritdanststring_boolean(listemiroirplan[-1+i].hachures);
  end;

   ecritdanststring('Nombre de reseaux:');

  ecritdanststring_entier(nombreReseau);
  if nombreReseau>0 then
  for i:=1 to nombreReseau do begin
  ecritdanststring('Numero '+inttostr(i));
  ecritdanststring_reel(listeReseau[-1+i].a1x);
   ecritdanststring_reel(listeReseau[-1+i].a1y);
    ecritdanststring_reel(listeReseau[-1+i].a2x);
     ecritdanststring_reel(listeReseau[-1+i].a2y);
      ecritdanststring_entier(listeReseau[-1+i].epaisseur);
       ecritdanststring_couleur(listeReseau[-1+i].couleur);
       ecritdanststring_reel(listeReseau[-1+i].pas);
       ecritdanststring_boolean(listeReseau[-1+i].entransmission);
  ecritdanststring_boolean(listeReseau[-1+i].hachures);
   ecritdanststring_entier(listeReseau[-1+i].ordre_min);
    ecritdanststring_entier(listeReseau[-1+i].ordre_max);
  end;



  ecritdanststring('Nombre de miroirs concaves paraxiaux:');
  ecritdanststring_entier(nombremscopa);
  if nombremscopa>0 then
  for i:=1 to nombremscopa do begin
  ecritdanststring('Numero '+inttostr(i));
  ecritdanststring_reel(listemscopa[-1+i].a1x);
   ecritdanststring_reel(listemscopa[-1+i].a1y);
    ecritdanststring_reel(listemscopa[-1+i].a2x);
     ecritdanststring_reel( listemscopa[-1+i].a2y);
      ecritdanststring_reel(listemscopa[-1+i].focale);
       ecritdanststring_entier(listemscopa[-1+i].epaisseur);
        ecritdanststring_couleur(listemscopa[-1+i].couleur);
         ecritdanststring_couleur( listemscopa[-1+i].couleuraxe);
  ecritdanststring_boolean(listemscopa[-1+i].hachures);
  ecritdanststring_boolean(listemscopa[-1+i].axefocal);
  ecritdanststring_boolean(listemscopa[-1+i].trou);
   ecritdanststring_reel(listemscopa[-1+i].diametretrou);
  end;

    ecritdanststring('Nombre de miroirs convexes paraxiaux:');
   ecritdanststring_entier(nombremscepa);
  if nombremscepa>0 then
  for i:=1 to nombremscepa do begin
  ecritdanststring('Numero '+inttostr(i));
   ecritdanststring_reel(listemscepa[-1+i].a1x);
    ecritdanststring_reel(listemscepa[-1+i].a1y);
     ecritdanststring_reel(listemscepa[-1+i].a2x);
      ecritdanststring_reel( listemscepa[-1+i].a2y);
       ecritdanststring_reel(listemscepa[-1+i].focale);
        ecritdanststring_entier( listemscepa[-1+i].epaisseur);
         ecritdanststring_couleur(listemscepa[-1+i].couleur);
          ecritdanststring_couleur( listemscepa[-1+i].couleuraxe);
  ecritdanststring_boolean(listemscepa[-1+i].hachures);
  ecritdanststring_boolean(listemscepa[-1+i].axefocal);
  ecritdanststring_boolean(listemscepa[-1+i].trou);
   ecritdanststring_reel(listemscepa[-1+i].diametretrou);
  end;

   ecritdanststring('Nombre de miroirs concaves reels:');
  ecritdanststring_entier(nombremscore);
  if nombremscore>0 then
  for i:=1 to nombremscore do begin
  ecritdanststring('Numero '+inttostr(i));
   ecritdanststring_reel(listemscore[-1+i].a1x);
    ecritdanststring_reel(listemscore[-1+i].a1y);
     ecritdanststring_reel(listemscore[-1+i].a2x);
      ecritdanststring_reel(listemscore[-1+i].a2y);
       ecritdanststring_reel(listemscore[-1+i].rayoncourbure);
        ecritdanststring_entier(listemscore[-1+i].epaisseur);
         ecritdanststring_couleur(listemscore[-1+i].couleur);
          ecritdanststring_couleur( listemscore[-1+i].couleuraxe);
  ecritdanststring_boolean(listemscore[-1+i].hachures);
  ecritdanststring_boolean(listemscore[-1+i].axefocal);
  ecritdanststring_boolean(listemscore[-1+i].aigu);
   ecritdanststring_boolean(listemscore[-1+i].trou);
    ecritdanststring_reel(listemscore[-1+i].diametretrou);
  end;

    ecritdanststring('Nombre de miroirs convexes reels:');
   ecritdanststring_entier(nombremscere);
  if nombremscere>0 then
  for i:=1 to nombremscere do begin
  ecritdanststring('Numero '+inttostr(i));
   ecritdanststring_reel(listemscere[-1+i].a1x);
    ecritdanststring_reel(listemscere[-1+i].a1y);
     ecritdanststring_reel(listemscere[-1+i].a2x);
      ecritdanststring_reel(listemscere[-1+i].a2y);
       ecritdanststring_reel(listemscere[-1+i].rayoncourbure);
        ecritdanststring_entier(listemscere[-1+i].epaisseur);
         ecritdanststring_couleur(listemscere[-1+i].couleur);
          ecritdanststring_couleur(listemscere[-1+i].couleuraxe);
  ecritdanststring_boolean(listemscere[-1+i].hachures);
  ecritdanststring_boolean(listemscere[-1+i].axefocal);
  ecritdanststring_boolean(listemscere[-1+i].aigu);
   ecritdanststring_boolean(listemscere[-1+i].trou);
    ecritdanststring_reel(listemscere[-1+i].diametretrou);
  end;

   ecritdanststring('Nombre de lentiles minces convergentes:');
   ecritdanststring_entier(nombrelmc);
  if nombrelmc>0 then
  for i:=1 to nombrelmc do begin
  ecritdanststring('Numero '+inttostr(i));
   ecritdanststring_reel(listelmc[-1+i].a1x);
    ecritdanststring_reel(listelmc[-1+i].a1y);
     ecritdanststring_reel(listelmc[-1+i].a2x);
      ecritdanststring_reel(listelmc[-1+i].a2y);
       ecritdanststring_reel(listelmc[-1+i].focale);
        ecritdanststring_entier(listelmc[-1+i].epaisseur);
         ecritdanststring_couleur(listelmc[-1+i].couleur);
          ecritdanststring_couleur(listelmc[-1+i].couleuraxe);
  ecritdanststring_boolean(listelmc[-1+i].axefocal);
  end;

  ecritdanststring('Nombre de lentiles minces divergentes:');
  ecritdanststring_entier(nombrelmd);
  if nombrelmd>0 then
  for i:=1 to nombrelmd do begin
  ecritdanststring('Numero '+inttostr(i));
   ecritdanststring_reel(listelmd[-1+i].a1x);
    ecritdanststring_reel(listelmd[-1+i].a1y);
     ecritdanststring_reel(listelmd[-1+i].a2x);
      ecritdanststring_reel( listelmd[-1+i].a2y);
       ecritdanststring_reel(listelmd[-1+i].focale);
        ecritdanststring_entier(listelmd[-1+i].epaisseur);
         ecritdanststring_couleur(listelmd[-1+i].couleur);
          ecritdanststring_couleur(listelmd[-1+i].couleuraxe);
  ecritdanststring_boolean(listelmd[-1+i].axefocal);
  end;

   ecritdanststring('Nombre de rayons:');
   ecritdanststring_entier(nombrerayon);
  if nombrerayon>0 then
  for i:=1 to nombrerayon do begin
  ecritdanststring('Numero '+inttostr(i));
   ecritdanststring_reel(listerayon[-1+i].ax);
    ecritdanststring_reel(listerayon[-1+i].ay);
     ecritdanststring_reel(listerayon[-1+i].kx);
      ecritdanststring_reel(listerayon[-1+i].ky);
       ecritdanststring_couleur(listerayon[-1+i].couleur);
        ecritdanststring_entier(listerayon[-1+i].epaisseur);
         ecritdanststring_entier(listerayon[-1+i].nombremaxenfant);
  ecritdanststring(listerayon[-1+i].vav);
  ecritdanststring(listerayon[-1+i].vaa);
  ecritdanststring('Nombre maximal de segments:');
  ecritdanststring_entier(listerayon[-1+i].maxsegment);
  end;

  ecritdanststring('Nombre de sources ponctuelles:');
  ecritdanststring_entier(nombresourceponctuelle);
  if nombresourceponctuelle>0 then
  for i:=1 to nombresourceponctuelle do begin
  ecritdanststring('Numero '+inttostr(i));
  ecritdanststring_entier(listesourceponctuelle[-1+i].nombrederayon);
   ecritdanststring_reel(listesourceponctuelle[-1+i].sx);
    ecritdanststring_reel( listesourceponctuelle[-1+i].sy);
     ecritdanststring_reel( listesourceponctuelle[-1+i].a1x);
      ecritdanststring_reel(listesourceponctuelle[-1+i].a1y);
       ecritdanststring_reel(listesourceponctuelle[-1+i].a2x);
        ecritdanststring_reel(listesourceponctuelle[-1+i].a2y);
         ecritdanststring_couleur( listesourceponctuelle[-1+i].couleur);
          ecritdanststring_entier( listesourceponctuelle[-1+i].epaisseur);
           ecritdanststring_entier(  listesourceponctuelle[-1+i].maxenfantparrayon);
  ecritdanststring(listesourceponctuelle[-1+i].vav);
  ecritdanststring(listesourceponctuelle[-1+i].vaa);
   ecritdanststring_boolean(listesourceponctuelle[-1+i].sourcevirtuelle);
    ecritdanststring_boolean(listesourceponctuelle[-1+i].tracersurfacesonde);
     ecritdanststring(listesourceponctuelle[-1+i].listechemins);
     ecritdanststring('Nombre maximal de segments par rayon:');
  ecritdanststring_entier(listesourceponctuelle[-1+i].maxsegment);
  end;


   ecritdanststring('Nombre d''ondes planes:');
   ecritdanststring_entier(nombreondeplane);
  if nombreondeplane>0 then
  for i:=1 to nombreondeplane do begin
  ecritdanststring('Numero '+inttostr(i));
  ecritdanststring_entier(listeondeplane[-1+i].nombrederayon);
   ecritdanststring_reel( listeondeplane[-1+i].sx);
    ecritdanststring_reel( listeondeplane[-1+i].sy);
     ecritdanststring_reel(listeondeplane[-1+i].a1x);
      ecritdanststring_reel(listeondeplane[-1+i].a1y);
       ecritdanststring_reel(listeondeplane[-1+i].a2x);
        ecritdanststring_reel(listeondeplane[-1+i].a2y);
         ecritdanststring_couleur(listeondeplane[-1+i].couleur);
          ecritdanststring_entier( listeondeplane[-1+i].epaisseur);
           ecritdanststring_entier( listeondeplane[-1+i].maxenfantparrayon);
  ecritdanststring(listeondeplane[-1+i].vav);
  ecritdanststring(listeondeplane[-1+i].vaa);
  ecritdanststring_boolean(listeondeplane[-1+i].tracersurfacesonde);
  ecritdanststring(listeondeplane[-1+i].listechemins);
   ecritdanststring('Nombre maximal de segments par rayon:');
  ecritdanststring_entier(listeondeplane[-1+i].maxsegment);
  end;

   ecritdanststring('Nombre d''ecrans:');
   ecritdanststring_entier(nombreecran);
  if nombreecran>0 then
  for i:=1 to nombreecran do begin
  ecritdanststring('Numero '+inttostr(i));
   ecritdanststring_reel(listeecran[-1+i].a1x);
    ecritdanststring_reel(listeecran[-1+i].a1y);
     ecritdanststring_reel(listeecran[-1+i].a2x);
      ecritdanststring_reel(listeecran[-1+i].a2y);
       ecritdanststring_entier(listeecran[-1+i].epaisseur);
        ecritdanststring_couleur(listeecran[-1+i].couleur);
  end;

  ecritdanststring('Nombre de diaphragmes:');
  ecritdanststring_entier(nombrediaphragme);
  if nombrediaphragme>0 then
  for i:=1 to nombrediaphragme do begin
  ecritdanststring('Numero '+inttostr(i));
   ecritdanststring_reel(listediaphragme[-1+i].cx);
    ecritdanststring_reel(listediaphragme[-1+i].cy);
     ecritdanststring_reel(listediaphragme[-1+i].anglepolaire);
      ecritdanststring_reel(listediaphragme[-1+i].rint);
       ecritdanststring_reel(listediaphragme[-1+i].rext);
        ecritdanststring_entier(listediaphragme[-1+i].epaisseur);
         ecritdanststring_couleur(listediaphragme[-1+i].couleur);
  end;


   ecritdanststring('Nombre de lames semi reflechissantes:');
   ecritdanststring_entier(nombrelsr);
  if nombrelsr>0 then
  for i:=1 to nombrelsr do begin
  ecritdanststring('Numero '+inttostr(i));
   ecritdanststring_reel(listelsr[-1+i].a1x);
    ecritdanststring_reel(listelsr[-1+i].a1y);
     ecritdanststring_reel(listelsr[-1+i].a2x);
      ecritdanststring_reel(listelsr[-1+i].a2y);
       ecritdanststring_entier(listelsr[-1+i].epaisseur);
        ecritdanststring_couleur(listelsr[-1+i].couleur);
  end;

  ecritdanststring('Nombre de polyhedres:');
  ecritdanststring_entier(nombrepolyhedre);
  if nombrepolyhedre>0 then
  for i:=1 to nombrepolyhedre do begin
  ecritdanststring('Numero '+inttostr(i));
  ecritdanststring_entier(listepolyhedre[-1+i].nombresommet);
  for j:=1 to listepolyhedre[-1+i].nombresommet do begin
   ecritdanststring_reel(listepolyhedre[-1+i].messommets[j].ax);
    ecritdanststring_reel(listepolyhedre[-1+i].messommets[j].ay);
    end;
  ecritdanststring_reel(listepolyhedre[-1+i].indicerouge);
   ecritdanststring_reel(listepolyhedre[-1+i].indicebleu);
    ecritdanststring_reel(listepolyhedre[-1+i].indicevert);
  ecritdanststring_couleur(listepolyhedre[-1+i].couleurbord);
   ecritdanststring_couleur(listepolyhedre[-1+i].couleurfond);
  case listepolyhedre[-1+i].reflechientrant of
  toujoursreflechi: ecritdanststring('1');
  jamaisreflechi: ecritdanststring('2');
  reflechisirefracte:ecritdanststring('3');
  reflechisipasrefracte:ecritdanststring('4');
  end;
  case listepolyhedre[-1+i].reflechisortant of
  toujoursreflechi: ecritdanststring('1');
  jamaisreflechi: ecritdanststring('2');
  reflechisirefracte:ecritdanststring('3');
  reflechisipasrefracte:ecritdanststring('4');
  end;
  end;





  ecritdanststring('Nombre de prismes:');
  ecritdanststring_entier(nombreprisme);
  if nombreprisme>0 then
  for i:=1 to nombreprisme do begin
  ecritdanststring('Numero '+inttostr(i));
      ecritdanststring_reel(listeprisme[-1+i].gx);
    ecritdanststring_reel(listeprisme[-1+i].gy);
   ecritdanststring_reel(listeprisme[-1+i].ax);
    ecritdanststring_reel(listeprisme[-1+i].ay);
     ecritdanststring_reel(listeprisme[-1+i].bx);
    ecritdanststring_reel(listeprisme[-1+i].by);
  ecritdanststring_reel(listeprisme[-1+i].indicerouge);
   ecritdanststring_reel(listeprisme[-1+i].indicebleu);
    ecritdanststring_reel(listeprisme[-1+i].indicevert);
  ecritdanststring_couleur(listeprisme[-1+i].couleurbord);
   ecritdanststring_couleur(listeprisme[-1+i].couleurfond);
  case listeprisme[-1+i].reflechientrant of
  toujoursreflechi: ecritdanststring('1');
  jamaisreflechi: ecritdanststring('2');
  reflechisirefracte:ecritdanststring('3');
  reflechisipasrefracte:ecritdanststring('4');
  end;
  case listeprisme[-1+i].reflechisortant of
  toujoursreflechi: ecritdanststring('1');
  jamaisreflechi: ecritdanststring('2');
  reflechisirefracte:ecritdanststring('3');
  reflechisipasrefracte:ecritdanststring('4');
  end;
  end;


  ecritdanststring('Nombre de polycercles:');
  ecritdanststring_entier(nombrepolycercle);
  if nombrepolycercle>0 then
  for i:=1 to nombrepolycercle do begin
  ecritdanststring('Numero '+inttostr(i));
  ecritdanststring_entier(listepolycercle[-1+i].nombresommet);
  for j:=1 to listepolycercle[-1+i].nombresommet do   begin
   ecritdanststring_reel(listepolycercle[-1+i].messommets[j].ax);
    ecritdanststring_reel(listepolycercle[-1+i].messommets[j].ay);
    end;
   for j:=1 to listepolycercle[-1+i].nombresommet do  begin
  ecritdanststring_reel(listepolycercle[-1+i].mess[j].ax);
   ecritdanststring_reel(listepolycercle[-1+i].mess[j].ay);
   end;
   for j:=1 to listepolycercle[-1+i].nombresommet do
  ecritdanststring_boolean(listepolycercle[-1+i].rectiligne[j]);
   ecritdanststring_reel(listepolycercle[-1+i].indicerouge);
    ecritdanststring_reel(listepolycercle[-1+i].indicebleu);
     ecritdanststring_reel(listepolycercle[-1+i].indicevert);
  ecritdanststring_couleur(listepolycercle[-1+i].couleurbord);
   ecritdanststring_couleur(listepolycercle[-1+i].couleurfond);
  case listepolycercle[-1+i].reflechientrant of
  toujoursreflechi: ecritdanststring('1');
  jamaisreflechi: ecritdanststring('2');
  reflechisirefracte:ecritdanststring('3');
  reflechisipasrefracte:ecritdanststring('4');
  end;
  case listepolycercle[-1+i].reflechisortant of
  toujoursreflechi: ecritdanststring('1');
  jamaisreflechi: ecritdanststring('2');
  reflechisirefracte:ecritdanststring('3');
  reflechisipasrefracte:ecritdanststring('4');
  end;
  end;

  ecritdanststring('Nombre de spheres:');
  ecritdanststring_entier(nombresphere);
    if nombresphere>0 then
  for i:=1 to nombresphere do begin
  ecritdanststring('Numero '+inttostr(i));
   ecritdanststring_reel(listesphere[-1+i].cx);
    ecritdanststring_reel(listesphere[-1+i].cy);
     ecritdanststring_reel(listesphere[-1+i].rayon);
      ecritdanststring_reel(listesphere[-1+i].indicerouge);
       ecritdanststring_reel(listesphere[-1+i].indicebleu);
        ecritdanststring_reel(listesphere[-1+i].indicevert);
  ecritdanststring_couleur(listesphere[-1+i].couleurbord);
   ecritdanststring_couleur(listesphere[-1+i].couleurfond);
  case listesphere[-1+i].reflechientrant of
  toujoursreflechi: ecritdanststring('1');
  jamaisreflechi: ecritdanststring('2');
  reflechisirefracte:ecritdanststring('3');
  reflechisipasrefracte:ecritdanststring('4');
  end;
  case listesphere[-1+i].reflechisortant of
  toujoursreflechi: ecritdanststring('1');
  jamaisreflechi: ecritdanststring('2');
  reflechisirefracte:ecritdanststring('3');
  reflechisipasrefracte:ecritdanststring('4');
  end;
  end;

  ecritdanststring('Nombre de miroirs coniques:');
  ecritdanststring_entier(nombremiroirconique);
  if nombremiroirconique>0 then
  for i:=1 to nombremiroirconique do begin
  ecritdanststring('Numero '+inttostr(i));
   ecritdanststring_reel(listemiroirconique[-1+i].fx);
    ecritdanststring_reel(listemiroirconique[-1+i].fy);
     ecritdanststring_reel(listemiroirconique[-1+i].excentricite);
      ecritdanststring_reel(listemiroirconique[-1+i].parametre);
       ecritdanststring_reel(listemiroirconique[-1+i].theta0);
        ecritdanststring_reel(listemiroirconique[-1+i].theta1);
         ecritdanststring_reel(listemiroirconique[-1+i].theta2);
  ecritdanststring_couleur(listemiroirconique[-1+i].couleur);
   ecritdanststring_couleur(listemiroirconique[-1+i].couleuraxe);
  ecritdanststring_boolean(listemiroirconique[-1+i].hachures);
  ecritdanststring_boolean(listemiroirconique[-1+i].axefocal);
  ecritdanststring_boolean(listemiroirconique[-1+i].concave);
  end;

  ecritdanststring('Nombre de textes:');
  ecritdanststring_entier(nombretexte);
  if nombretexte>0 then
   for i:=1 to nombretexte do begin
   ecritdanststring('Numero '+inttostr(i));
  ecritdanststring(listetexteaffiche[-1+i].texteluimeme);
   ecritdanststring_reel(listetexteaffiche[-1+i].x);
    ecritdanststring_reel(listetexteaffiche[-1+i].y);
   ecritdanststring_entier(listetexteaffiche[-1+i].policecharset);
    ecritdanststring_couleur(listetexteaffiche[-1+i].policecolor);
   ecritdanststring(listetexteaffiche[-1+i].policename);
   ecritdanststring_entier(listetexteaffiche[-1+i].policesize);
   if listetexteaffiche[-1+i].policefsbold then
   ecritdanststring('fsbold') else ecritdanststring('no');
   if listetexteaffiche[-1+i].policefsitalic  then
   ecritdanststring('fsitalic') else ecritdanststring('no');
   if listetexteaffiche[-1+i].policefsunderline  then
   ecritdanststring('fsunderline') else ecritdanststring('no');
   if listetexteaffiche[-1+i].policefsstrikeout  then
   ecritdanststring('fsstrikeout') else ecritdanststring('no');
   if listetexteaffiche[-1+i].policepitch=fpDefault then ecritdanststring('fpDefault');
   if  listetexteaffiche[-1+i].policepitch=fpVariable then ecritdanststring('fpVariable');
   if  listetexteaffiche[-1+i].policepitch=fpFixed then ecritdanststring('fpFixed');

   end;


    ecritdanststring('Nombre de lentilles epaisses:');
    ecritdanststring_entier(nombrelec);
  if nombrelec>0 then
  for i:=1 to nombrelec do begin
  ecritdanststring('Numero '+inttostr(i));
   ecritdanststring_reel(listelec[-1+i].s1x);
    ecritdanststring_reel(listelec[-1+i].s1y);
     ecritdanststring_reel(listelec[-1+i].s2x);
      ecritdanststring_reel(listelec[-1+i].s2y);
       ecritdanststring_reel(listelec[-1+i].r1);
        ecritdanststring_reel(listelec[-1+i].r2);
         ecritdanststring_reel(listelec[-1+i].diametre);
  ecritdanststring_couleur(listelec[-1+i].couleurfond);
   ecritdanststring_couleur(listelec[-1+i].couleuraxe);
  ecritdanststring_boolean(listelec[-1+i].axefocal);
  ecritdanststring_reel(listelec[-1+i].indicerouge);
   ecritdanststring_reel(listelec[-1+i].indicebleu);
    ecritdanststring_reel(listelec[-1+i].indicevert);
  case listelec[-1+i].reflechientrant of
  toujoursreflechi: ecritdanststring('1');
  jamaisreflechi: ecritdanststring('2');
  reflechisirefracte:ecritdanststring('3');
  reflechisipasrefracte:ecritdanststring('4');
  end;
  case listelec[-1+i].reflechisortant of
  toujoursreflechi: ecritdanststring('1');
  jamaisreflechi: ecritdanststring('2');
  reflechisirefracte:ecritdanststring('3');
  reflechisipasrefracte:ecritdanststring('4');
  end;
  end;


    ecritdanststring('Nombre d''angles:');
    ecritdanststring_reel(nombreangle);
  if nombreangle>0 then
  for i:=1 to nombreangle do begin
  ecritdanststring('Numero '+inttostr(i));
   ecritdanststring_reel(listeangle[-1+i].cx);
    ecritdanststring_reel(listeangle[-1+i].cy);
     ecritdanststring_reel(listeangle[-1+i].a1x);
      ecritdanststring_reel(listeangle[-1+i].a1y);
       ecritdanststring_reel(listeangle[-1+i].a2x);
        ecritdanststring_reel(listeangle[-1+i].a2y);
  end;


  { a partir de la version 3}
   ecritdanststring('Nombre de traits:');
    ecritdanststring_entier(nombrefleche);
  if nombrefleche>0 then
  for i:=1 to nombrefleche do begin
  ecritdanststring('Numero '+inttostr(i));
   ecritdanststring_reel(listefleche[-1+i].a1x);
    ecritdanststring_reel(listefleche[-1+i].a1y);
     ecritdanststring_reel(listefleche[-1+i].a2x);
      ecritdanststring_reel(listefleche[-1+i].a2y);
  ecritdanststring_entier(listefleche[-1+i].epaisseur);
   ecritdanststring_couleur(listefleche[-1+i].couleur);
  case listefleche[-1+i].ext1 of
  trien: ecritdanststring('trien');
  ttrait: ecritdanststring('ttrait');
  tffleche: ecritdanststring('tffleche');
  trond: ecritdanststring('trond');
  tcroix: ecritdanststring('tcroix');
  tcroixx: ecritdanststring('tcroixx');
  tdisque: ecritdanststring('tdisque');
  end;
 case listefleche[-1+i].ext2 of
  trien: ecritdanststring('trien');
   ttrait: ecritdanststring('ttrait');
  tffleche: ecritdanststring('tffleche');
  trond: ecritdanststring('trond');
  tcroix: ecritdanststring('tcroix');
  tcroixx: ecritdanststring('tcroixx');
  tdisque: ecritdanststring('tdisque');
  end;
 case listefleche[-1+i].styletrait of
 pssolid: ecritdanststring('pssolid');
 psdot:ecritdanststring('psdot');
 psdash: ecritdanststring('psdash');
 psdashdot: ecritdanststring('psdashdot');
 psdashdotdot: ecritdanststring('psdashdotdot');
 end;
  ecritdanststring_reel(listefleche[-1+i].taille1);
   ecritdanststring_reel(listefleche[-1+i].taille2);

  end;

  { a partir de a version 4}

  ecritdanststring('Nombre d''oeils:');
   ecritdanststring_entier(nombreoeil);
  if nombreoeil>0 then
  for i:=1 to nombreoeil do begin
  ecritdanststring('Numero '+inttostr(i));
   ecritdanststring_reel(listeoeil[-1+i].cx);
    ecritdanststring_reel(listeoeil[-1+i].cy);
     ecritdanststring_reel(listeoeil[-1+i].ax);
      ecritdanststring_reel(listeoeil[-1+i].ay);
       ecritdanststring_entier(listeoeil[-1+i].epaisseur);
        ecritdanststring_couleur(listeoeil[-1+i].couleur);
  end;

    { a partir de la version 5}

    ecritdanststring('Nombre de mesures de distances');
    ecritdanststring_entier(nombremetre);
    if nombremetre>0 then for i:=1 to nombremetre do begin
    ecritdanststring('Numero '+inttostr(i));
     ecritdanststring_reel(listemetre[-1+i].a1x);
      ecritdanststring_reel(listemetre[-1+i].a1y);
       ecritdanststring_reel(listemetre[-1+i].a2x);
        ecritdanststring_reel(listemetre[-1+i].a2y);
    end;


  ecritdanststring('Nombre de groupes:');
   ecritdanststring_entier(nombregroupe);
  if nombregroupe>0 then
  for i:=1 to nombregroupe do begin
  ecritdanststring('Numero groupe'+inttostr(i));
  ecritdanststring_entier(listegroupe[-1+i].nombreelements);
  for j:=1 to listegroupe[-1+i].nombreelements do begin
  ecritdanststring_entier(listegroupe[-1+i].listeelements[j].indiceelement);
  case listegroupe[-1+i].listeelements[j].typeelementgroupe of
  mdoeil: ecritdanststring('mdoeil');
  mddiaphragme: ecritdanststring('mddiaphragme');
  mdtexteaffiche: ecritdanststring('mdtexteaffiche');
  mdmiroirconique: ecritdanststring('mdmiroirconique');
  mdlec: ecritdanststring('mdlec');
  mdfleche:ecritdanststring('mdfleche');
  mdmiroirplan: ecritdanststring('mdmiroirplan');
  mdecran: ecritdanststring('mdecran');
  mdangle: ecritdanststring('mdangle');
  mdmetre: ecritdanststring('mdmetre');
  mdlsr: ecritdanststring('mdlsr');
  mdmscopa: ecritdanststring('mdmscopa');
  mdlmc: ecritdanststring('mdlmc');
  mdlmd: ecritdanststring('mdlmd');
  mdmscepa: ecritdanststring('mdmscepa');
  mdmscore: ecritdanststring('mdmscore');
  mdmscere: ecritdanststring('mdmscere');
   mdpolyhedre: ecritdanststring('mdpolyhedre');
    mdrayon: ecritdanststring('mdrayon');
     mdsourceponctuelle: ecritdanststring('mdsourceponctuelle');
      mdondeplane: ecritdanststring('mdondeplane');
       mdpolycercle: ecritdanststring('mdpolycercle');
        mdsphere: ecritdanststring('mdsphere');
        mdprisme: ecritdanststring('mdprisme');
        else ecritdanststring('mdneant');
  end;
  end;


  end;

      ecritdanststring('couleur fond simulation');
  ecritdanststring_couleur(couleurfondsimulation);
  ecritdanststring('empennage');
  ecritdanststring('position');
  ecritdanststring_entier(positionempennage);
  ecritdanststring('taille');
  ecritdanststring_entier(tailleempennage);


    end;

 procedure TForm1.Sauver1Click(Sender: TObject);
begin
 sauvevirtuel;
end;
procedure TForm1.Rtablir1Click(Sender: TObject);
begin
 form1.charge_virtuel;
 refaire1.Enabled:=true;
end;

procedure TForm1.Refaire1Click(Sender: TObject);
begin
 if nombre_etats_sauvegardes>=niveau_max_sauvegarde-1
then refaire1.Enabled:=false;
if nombre_etats_sauvegardes<=niveau_max_sauvegarde-1 then begin
 inc(nombre_etats_sauvegardes,2);
  form1.charge_virtuel;
  end;
  rtablir1.Enabled:=true;
end;

 procedure TForm1.Dfinirlerpertoirepersonnel1Click(Sender: TObject);
 var fc:textfile;
 begin
 with form16
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
   form16.rep1.text:=repertoire_executable;
   form16.rep1.Enabled:=false;
    form16.directoryedit1.Directory:=repertoire_exemples;
     form16.directoryedit2.Directory:=repertoire_config_perso;
      form16.directoryedit3.Directory:=repertoire_simul_perso;


   form16.directoryedit1.enabled:=true;
   form16.directoryedit2.enabled:=false;
   form16.directoryedit3.enabled:=true;
   form16.showmodal;

    repertoire_simul_perso:=form16.directoryedit3.Directory;
    repertoire_exemples:=form16.directoryedit1.Directory;


   if not(directoryexistsutf8(repertoire_simul_perso)) then
   try
   forcedirectories((repertoire_simul_perso));
   except
   end;
   if not(directoryexistsUTF8(repertoire_simul_perso)) then begin
   application.messagebox(pchar(rsJeNeParviens4), pchar(rsMaisEuh), mb_ok);
    repertoire_simul_perso:=repertoire_simul_perso_par_defaut;
        end;

    if not(directoryexistsUTF8(repertoire_exemples)) then
   try
   forcedirectories((repertoire_exemples));
   except
   end;
   if not(directoryexistsUTF8(repertoire_exemples)) then begin
   application.messagebox(pchar(rsJeNeParviens4), pchar(rsMaisEuh), mb_ok);
    repertoire_exemples:=repertoire_exemples_par_defaut;
        end;

   try
   AssignFile(Fc,(nom_fichier_config_optgeo));
  rewrite(fc);
  writeln(fc,'repertoire exemples');
    writeln(fc,repertoire_exemples);
    writeln(fc,'repertoire simulation perso');
    writeln(fc,repertoire_simul_perso);
    writeln(fc,'repertoire config perso');
    writeln(fc,repertoire_config_perso);
    closefile(fc);
  except
  end;
        saisiepreferences.BitBtn2Click(Sender);

end;

 procedure TForm1.dudernierrpertoireutilis1Click(Sender: TObject);
begin
form1.Ouvrirsimulation(sender,repertoire_actuel);
end;


procedure TForm1.LicenseGPL1Click(Sender: TObject);
begin
formgpl:=tformgpl.create(self);
 with formgpl
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
formgpl.Memogpl.Clear;
formgpl.Memogpl.Lines.LoadFromFile((repertoire_executable+'gpl.txt'));
formgpl.ShowModal;
end;

procedure TForm1.SpeedButton2Click(Sender: TObject);
var i:integer;
begin
if not(nombretotalsegment)>0 then exit;

fiche_coordonnees_segments:=tfiche_coordonnees_segments.create(self);
fiche_coordonnees_segments.Memo1.Lines.Clear;
if nombretotalsegment>0  then

 for i:=1 to   nombretotalsegment do
 if not (fantomespresents and fantomes[i]) then
 fiche_coordonnees_segments.Memo1.Lines.Add('('+floattostr(listesegment[-1+i].a1x)+' ; '+floattostr(listesegment[-1+i].a1y)+
 ') ; ('+floattostr(listesegment[-1+i].a2x)+' ; '+floattostr(listesegment[-1+i].a2y)+' )')
  else
  fiche_coordonnees_segments.Memo1.Lines.Add('('+floattostr(listesegment[-1+i].a1x)+' ; '+floattostr(listesegment[-1+i].a1y)+
 ') ; ('+floattostr(listesegment[-1+i].a2x)+' ; '+floattostr(listesegment[-1+i].a2y)+' )...fantôme !');
  fiche_coordonnees_segments.ShowModal;
end;

Function MyGetAppConfigDirUTF8: String;
{il est necessaire de modifier GetAppConfigDir acr sur les systemes
windows renvoie local setting\application data au lieu de application data}
var
  I: Integer;
  EnvVars: TStringList;
begin
{$ifndef windows}
result:=(SysUtils.GetAppConfigDir(false));
{$endif}
 {$ifdef windows}
  EnvVars := TStringList.Create;
try
       for I := 0 to GetEnvironmentVariableCount - 1 do
      EnvVars.Add(GetEnvironmentString(I));
        result:=(AppendPathDelim(EnvVars.Values['APPDATA'])+'optgeo\');

  finally
    EnvVars.Free;
  end;
  {$endif}
end;


Function ExpandEnvironmentString(s: String): String;
    Var i, k: Integer;
        sl: TStringList;
        s2: String;
        bP, bD: Boolean;
    Begin
      Result := s;
      bP := Pos('%', s) > 0;
      bD := Pos('$', s) > 0;
      If Not(bP Or bD) Then Exit; // Nichts zu konvertieren
      sl := TStringList.Create;
      sl.Sorted := bD;
      For i := GetEnvironmentVariableCount - 1 DownTo 0 Do // Alle Variablen in eine Liste merken
      Begin
        s2 := GetEnvironmentString(i);
        If Length(s2) >= 2 Then
          If s2[1] <> '=' Then // Bei Windows kommt sowas auch vor, daher wegoptimieren
            sl.Add(s2);
      end;

      k := 0;
      Repeat // Durchlaufe so oft bis alle Env-Variablen aufgelöst sind
        s2 := Result;
        For i := sl.Count - 1 DownTo 0 Do
        Begin
          If bP Then
          Begin
            Result := StringReplace(Result, '%' + sl.Names[i] + '%', sl.ValueFromIndex[i], [rfReplaceAll, rfIgnoreCase]);
            If Pos('%', Result) = 0 Then bP := False;
          end;
          If bD Then
          Begin
            Result := StringReplace(Result, '$' + sl.Names[i], sl.ValueFromIndex[i], [rfReplaceAll]);
            If Pos('$', Result) = 0 Then bD := False;
          end;
          If Not(bP Or bD) Then Break;
        end;
        If Not(bP Or bD) Then Break;
        If SameText(s2, Result) Then Break; // % $ nicht konvertierbar, da im System nicht vorhanden
        Inc(k);
        If k > sl.Count - 2 Then Break; // Max Durchläufe
      Until True;
      sl.Free;
    end;




 {$ifdef windows}
Function MyGetMesDocumentsUTF8: String;
var
    Reg: TRegistry;

begin

  Reg := TRegistry.Create;
  try
    Reg.RootKey := HKEY_CURRENT_USER;
    Reg.OpenKey('Software\Microsoft\Windows\CurrentVersion\'
      + 'Explorer\User Shell Folders\', False);
    result:=ExpandEnvironmentString(Reg.ReadString('Personal'));
  finally
    Reg.Free;

  end;
    end;
     {$endif}


initialization
  {$i ray1.lrs}
 


 fermeture:=false; creation:=true;
 modepage:=true;
  versionprogramme:=220;









   {$IFDEF WINDOWS}parametrelignecommande:=(paramcount>0);{$ELSE}
       parametrelignecommande:=false;    {$ENDIF}


  nombre_etats_sauvegardes:=0;  niveau_max_sauvegarde:=0;





end.
