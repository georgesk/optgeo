{  This file is part of OptGeo, optical simulation software
    Copyright (C) 2004  Jean-Marie Biansan
        Contact: jeanmarie.biansan@free.fr
    Web site: http://jeanmarie.biansan.free.fr/logiciel.html
 From version 1.25, OptGeo is distribued under the terms of the
 GNU General Public License.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA}
    unit Unit6;

{$mode objfpc}{$H+}

interface

uses
  LCLIntf, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Spin, ExtCtrls,unit222,Unit8,ColorBox, LResources,LCLType,
  UnitScaleFont,UChaines;

type

  { Tsaisiepolyhedre }

  Tsaisiepolyhedre = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Colorgrid1: TColorBox;
    radiosortant: TRadioGroup;
    radioentrant: TRadioGroup;
    GroupBox2: TGroupBox;
    StaticText2: TLabel;
    StaticText3: TLabel;
    StaticText4: TLabel;
    editnrouge: TEdit;
    editnvert: TEdit;
    editnbleu: TEdit;
    ComboBox1: TComboBox;
    GroupBox3: TGroupBox;
    StaticText5: TLabel;
    StaticText6: TLabel;
    editx: TEdit;
    edity: TEdit;
    boutonsup: TBitBtn;
    GroupBox1: TGroupBox;
    log1: TLabel;
    log2: TLabel;
    StaticText1: TLabel;
    combomateriaux: TComboBox;
    procedure BitBtn1Click(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
    procedure combomateriauxChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    
  private  encreation:boolean;
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

var
  saisiepolyhedre: Tsaisiepolyhedre;
   oldindex:integer;
  nrouge,nvert,nbleu:extended;
  tentr,tsort:treflechipolyhedre;
implementation


procedure Tsaisiepolyhedre.BitBtn1Click(Sender: TObject);
begin
try
 strtofloat(editx.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce2),
 pchar(rsattention),mb_ok);
 ComboBox1.itemindex:=oldindex;
 exit;
 end;  end;
ttpoly[combobox1.ItemIndex+1].ax:=strtofloat(editx.text);

try
 strtofloat(edity.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce3),
 pchar(rsattention),mb_ok);
 ComboBox1.itemindex:=oldindex;
 exit;
 end;  end;
ttpoly[combobox1.ItemIndex+1].ay:=strtofloat(edity.text);


case radioentrant.ItemIndex of
0: tentr:=toujoursreflechi;
1: tentr:=jamaisreflechi;
2: tentr:=reflechisirefracte;
3: tentr:=reflechisipasrefracte;
end;

case radiosortant.ItemIndex of
0: tsort:=toujoursreflechi;
1: tsort:=jamaisreflechi;
2: tsort:=reflechisirefracte;
3: tsort:=reflechisipasrefracte;
end;

try
nrouge:=strtofloat(editnrouge.text);
except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce4),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;
 
try
nvert:=strtofloat(editnvert.text);
except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce5),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;


try
nbleu:=strtofloat(editnbleu.text);
except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce6),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;


ReTaillePpolyhedre(Listepolyhedre,nombrepolyhedre,nombrepolyhedre+1);

 inc(nombrepolyhedre);
if not
(Listepolyhedre[-1+nombrepolyhedre].create(nbpoly,ttpoly,nrouge,nbleu,nvert,clblack,colorgrid1.selected,
tentr,tsort)) then begin
ReTaillePpolyhedre(Listepolyhedre,nombrepolyhedre,nombrepolyhedre-1);
dec(nombrepolyhedre);
application.messagebox(pchar(rsPointsAberra),
pchar(rsattention),mb_ok);

end else
self.modalresult:=mrOk;
Rafraichit;
end;

procedure Tsaisiepolyhedre.ComboBox1Change(Sender: TObject);
begin

 try
 strtofloat(editx.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce2),
 pchar(rsattention),mb_ok);
 ComboBox1.itemindex:=oldindex;
 exit;
 end;  end;
ttpoly[oldindex+1].ax:=strtofloat(editx.text);

try
 strtofloat(edity.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce3),
 pchar(rsattention),mb_ok);
 ComboBox1.itemindex:=oldindex;
 exit;
 end;  end;
ttpoly[oldindex+1].ay:=strtofloat(edity.text);

editx.text:=floattostr(ttpoly[ComboBox1.itemindex+1].ax);
edity.text:=floattostr(ttpoly[ComboBox1.itemindex+1].ay);
oldindex:=ComboBox1.itemindex;
end;

procedure Tsaisiepolyhedre.combomateriauxChange(Sender: TObject);
begin
if combomateriaux.itemindex=0 then exit;
editnrouge.text:=listenrouge.strings[combomateriaux.itemindex];
editnvert.text:=listenvert.strings[combomateriaux.itemindex];
editnbleu.text:=listenbleu.strings[combomateriaux.itemindex];
end;

procedure Tsaisiepolyhedre.FormCreate(Sender: TObject);
begin
   encreation:=true;
   BITBTN1.CAPTION
// msgctxt ';TSAISIEPOLYHEDRE.BITBTN1.CAPTION';
:=rsOK;


BITBTN2.CAPTION
// msgctxt ';TSAISIEPOLYHEDRE.BITBTN2.CAPTION';
:=rsAnnuler;


BOUTONSUP.CAPTION
:=rsSupprimerCeP3;


CAPTION
:=rsPropriTSDeCe3;


GROUPBOX1.CAPTION
// msgctxt ';TSAISIEPOLYHEDRE.GROUPBOX1.CAPTION';
:=rsCouleurLCran7;


GROUPBOX2.CAPTION
// msgctxt ';TSAISIEPOLYHEDRE.GROUPBOX2.CAPTION';
:=rsIndiceDeRFra;


GROUPBOX3.CAPTION
// msgctxt ';TSAISIEPOLYHEDRE.GROUPBOX3.CAPTION';
:=rsCoordonnEsDe2;


LOG1.CAPTION
// msgctxt ';TSAISIEPOLYHEDRE.LOG1.CAPTION';
:=rsLog1;


LOG2.CAPTION
// msgctxt ';TSAISIEPOLYHEDRE.LOG2.CAPTION';
:=rsLog2;


RADIOENTRANT.CAPTION
// msgctxt ';TSAISIEPOLYHEDRE.RADIOENTRANT.CAPTION';
:=rsRayonRFlChiS;


RADIOSORTANT.CAPTION
// msgctxt ';TSAISIEPOLYHEDRE.RADIOSORTANT.CAPTION';
:=rsRayonRFlChiS2;


STATICTEXT1.CAPTION
// msgctxt ';TSAISIEPOLYHEDRE.STATICTEXT1.CAPTION';
:=rsMatRiau;


STATICTEXT2.CAPTION
// msgctxt ';TSAISIEPOLYHEDRE.STATICTEXT2.CAPTION';
:=rsPourLeRouge;


STATICTEXT3.CAPTION
// msgctxt ';TSAISIEPOLYHEDRE.STATICTEXT3.CAPTION';
:=rsPourLeVert;


STATICTEXT4.CAPTION
// msgctxt ';TSAISIEPOLYHEDRE.STATICTEXT4.CAPTION';
:=rsPourLeBleu;


STATICTEXT5.CAPTION
// msgctxt ';TSAISIEPOLYHEDRE.STATICTEXT5.CAPTION';
:=rsX;


STATICTEXT6.CAPTION
// msgctxt ';TSAISIEPOLYHEDRE.STATICTEXT6.CAPTION';
:=rsY;

radiosortant.Items[0]:=rsToujours;
radiosortant.Items[1]:=rsJamais;
radiosortant.Items[2]:=rsSeulementSiL;
radiosortant.Items[3]:=rsSeulementSiL8;

radioentrant.Items[0]:=rsToujours;
radioentrant.Items[1]:=rsJamais;
radioentrant.Items[2]:=rsSeulementSiL;
radioentrant.Items[3]:=rsSeulementSiL9;
end;

procedure Tsaisiepolyhedre.FormShow(Sender: TObject);
begin
//  if encreation then begin scalefont(self); encreation:=false; end;
end;


initialization
  {$i Unit6.lrs}


end.
