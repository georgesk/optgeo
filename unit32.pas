{  This file is part of OptGeo, optical simulation software
    Copyright (C) 2004  Jean-Marie Biansan
        Contact: jeanmarie.biansan@free.fr
    Web site: http://jeanmarie.biansan.free.fr/logiciel.html
 From version 1.25, OptGeo is distribued under the terms of the
 GNU General Public License.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA}

unit Unit32;

{$mode objfpc}{$H+}

interface

    uses
   LCLIntf, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, Buttons,{$ifdef windows}shellapi,{$endif}RTFTOPLAINTEXT,unit222,
  ComCtrls, LResources,UnitScaleFont,UChaines,LazFileUtils;

type

  { TFormEditeur }

  TFormEditeur = class(TForm)
    Memo1: TMemo;
    StatusBar1: TStatusBar;
    procedure fichiernouveau(Sender: TObject);
    procedure fichierouvrir(nom:string);
    procedure fichiersauver(Sender: TObject);
  procedure FormCreate(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormShow(Sender: TObject);
    procedure Memo1Change(Sender: TObject);
  private  encreation:boolean;
    { private  encreation:boolean; declarations }
  public
    { Public declarations }
    nomfichier: string;
  end;

var
  FormEditeur: TFormEditeur;

implementation


procedure TFormEditeur.fichiernouveau(Sender: TObject);
begin
  Memo1.Clear;
  nomfichier := '';
  StatusBar1.Panels[0].Text := nomfichier;
end;

procedure TFormEditeur.fichierouvrir(nom:string);
var f:textfile;   s,s1:string;
begin
memo1.Lines.Clear;
  nomfichier:=nom;
  if ansiuppercase(extractfileext(nomfichier))='.RTF' then begin
      assignfile(f,(nomfichier));
      reset(f);
      while not(eof(f)) do begin
      readln(f,s);
      s1:=rtf2plaintext(s);
  if s='' then Memo1.Lines.Add('');
  if s1<>'' then  Memo1.Lines.Add(s1);
  end;
  closefile(f);
    end else
    memo1.Lines.LoadFromFile((nomfichier));
    StatusBar1.Panels[0].Text := nomfichier;
  end;


procedure TFormEditeur.fichiersauver(Sender: TObject);
begin
if ansiuppercase(extractfileext(nomfichier))='.RTF' then
changefileext(nomfichier,'.txt');
    Memo1.Lines.SaveToFile((nomfichier));
end;







procedure TFormEditeur.FormCreate(Sender: TObject);
begin
encreation:=true;
CAPTION:=rsEditeurDeTex;

  nomfichier := '';
  StatusBar1.Panels[0].Text := nomfichier;
  Memo1.Clear;
end;

procedure TFormEditeur.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
 canclose:=false;
 self.WindowState:=wsminimized;
end;

procedure TFormEditeur.FormShow(Sender: TObject);
begin
 // if encreation then begin scalefont(self); encreation:=false; end;
end;

procedure TFormEditeur.Memo1Change(Sender: TObject);
begin
if memo1.Modified then commentaire_change:=true;
end;

initialization
  {$i UNIT32.lrs}


end.
