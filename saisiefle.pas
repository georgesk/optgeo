{  This file is part of OptGeo, optical simulation software
    Copyright (C) 2004  Jean-Marie Biansan
        Contact: jeanmarie.biansan@free.fr
    Web site: http://jeanmarie.biansan.free.fr/logiciel.html
 From version 1.25, OptGeo is distribued under the terms of the
 GNU General Public License.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA}


    unit saisiefle;

{$mode objfpc}{$H+}

interface

uses
  LCLIntf, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Colorbox, Spin, ExtCtrls,unit222, LResources,UnitScaleFont;

type

  { Tsaisiefleche }

  Tsaisiefleche = class(TForm)
    BitBtn1: TBitBtn;
    Colorgrid1: TColorBox;
    radioext1: TRadioGroup;
    radioext2: TRadioGroup;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    spintaille1: TSpinEdit;
    spintaille2: TSpinEdit;
    StaticText1: TLabel;
    StaticText3: TLabel;
    StaticText2: TLabel;
    StaticText4: TLabel;
    GroupBox3: TGroupBox;
    spinepaisseur: TSpinEdit;
    radiostyletrait: TRadioGroup;
    GroupBox4: TGroupBox;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure radiostyletraitClick(Sender: TObject);
  private  encreation:boolean;
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

var
  saisiefleche: Tsaisiefleche;
   ra_ext1,ra_ext2:textremite;
   ra_epaisseur:integer;
   ra_styletrait:tpenstyle;
   ra_couleur:tcolor;
    ra_taille1,ra_taille2:integer;
implementation





procedure Tsaisiefleche.BitBtn1Click(Sender: TObject);
begin
 ra_taille1:=spintaille1.value;
 ra_taille2:=spintaille2.value;
 ra_epaisseur:=spinepaisseur.value;
 case radioext1.itemindex of
 0: ra_ext1:=trien;
 1: ra_ext1:=tffleche;
 2: ra_ext1:=ttrait;
 3: ra_ext1:=trond;
 4: ra_ext1:=tcroix;
 5: ra_ext1:=tcroixx;
 6: ra_ext1:=tdisque;
 end;

 case radioext2.itemindex of
 0: ra_ext2:=trien;
 1: ra_ext2:=tffleche;
 2: ra_ext2:=ttrait;
 3: ra_ext2:=trond;
 4: ra_ext2:=tcroix;
 5: ra_ext2:=tcroixx;
 6: ra_ext2:=tdisque;
 end;

 case radiostyletrait.itemindex of
 0: ra_styletrait:=pssolid;
 1: ra_styletrait:=psdot;
 2: ra_styletrait:=psdash;
 3: ra_styletrait:=psdashdot;
 4: ra_styletrait:=psdashdotdot;
 end;

 ra_couleur:=colorgrid1.selected;

end;

procedure Tsaisiefleche.FormCreate(Sender: TObject);
begin
   encreation:=true;
end;

procedure Tsaisiefleche.FormShow(Sender: TObject);
begin
 // if encreation then begin scalefont(self); encreation:=false; end;
end;

procedure Tsaisiefleche.radiostyletraitClick(Sender: TObject);
begin

end;

initialization
  {$i saisiefle.lrs}


end.
