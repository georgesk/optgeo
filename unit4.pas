{  This file is part of OptGeo, optical simulation software
    Copyright (C) 2004  Jean-Marie Biansan
        Contact: jeanmarie.biansan@free.fr
    Web site: http://jeanmarie.biansan.free.fr/logiciel.html
 From version 1.25, OptGeo is distribued under the terms of the
 GNU General Public License.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA}

    unit Unit4;

{$mode objfpc}{$H+}

interface

uses
  LCLIntf, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, Buttons,{$ifdef windows}shellapi,windows,{$endif }LResources,UnitScaleFont,
  LazHelpHTML,UTF8Process,UChaines;

type

  { TForm4 }

  TForm4 = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    GroupBox1: TGroupBox;
    StaticText1: TLabel;
    StaticText2: TLabel;
    StaticText3: TLabel;
    StaticText4: TLabel;
    StaticText5: TLabel;
    StaticText6: TLabel;
    StaticText7: TLabel;
    StaticText8: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure StaticText5Click(Sender: TObject);
    procedure StaticText8Click(Sender: TObject);


    
  private  encreation:boolean;
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

var
  Form4: TForm4;

implementation








procedure TForm4.BitBtn1Click(Sender: TObject);
begin
close;
end;

procedure TForm4.FormCreate(Sender: TObject);
begin
   encreation:=true;
   BITBTN1.CAPTION:=rsOK;


BITBTN2.CAPTION:=rsAnnuler;


CAPTION:=rsAProposDeCeL;


LABEL1.CAPTION:=Format(rsEnCliquantSu, ['"', '"']);


LABEL2.CAPTION:=rsDeLaLicenseG;


STATICTEXT1.CAPTION:=rsOptGeoLogici;


STATICTEXT2.CAPTION:=rsDeuxDimensio;


STATICTEXT3.CAPTION:=rsVersion200Be;


STATICTEXT4.CAPTION:=rsCopyrightJea;


STATICTEXT5.CAPTION:=rsJeanmarieBia;


STATICTEXT6.CAPTION:=rsPourTouteRem;


STATICTEXT7.CAPTION:=rsVRifierQueVo;


STATICTEXT8.CAPTION:=rsSiteWebDeLAu;

end;

procedure TForm4.FormShow(Sender: TObject);
begin
 // if encreation then begin scalefont(self); encreation:=false; end;
end;

procedure TForm4.StaticText5Click(Sender: TObject);
begin
{$ifdef windows}
ShellExecute( 0, Nil, PChar('mailto:jeanmarie.biansan@free.fr'),Nil, Nil, SW_NORMAL );
{$endif}
end;

procedure TForm4.StaticText8Click(Sender: TObject);
var
  v: THTMLBrowserHelpViewer;
  BrowserPath, BrowserParams: string;
  p: LongInt;
  URL: String;
  BrowserProcess: TProcessUTF8;
begin
  v:=THTMLBrowserHelpViewer.Create(nil);
  try
    v.FindDefaultBrowser(BrowserPath,BrowserParams);
    //debugln(['Path=',BrowserPath,' Params=',BrowserParams]);

   url:='http://jeanmarie.biansan.free.fr/logiciel.html';
    p:=System.Pos('%s', BrowserParams);
    System.Delete(BrowserParams,p,2);
    System.Insert(URL,BrowserParams,p);

    // start browser
    BrowserProcess:=TProcessUTF8.Create(nil);
    try
      BrowserProcess.CommandLine:=BrowserPath+' '+BrowserParams;
      BrowserProcess.Execute;
    finally
      BrowserProcess.Free;
    end;
  finally
    v.Free;
end; end;

initialization
  {$i Unit4.lrs}


end.
