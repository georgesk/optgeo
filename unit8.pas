{  This file is part of OptGeo, optical simulation software
    Copyright (C) 2004  Jean-Marie Biansan
        Contact: jeanmarie.biansan@free.fr
    Web site: http://jeanmarie.biansan.free.fr/logiciel.html
 From version 1.25, OptGeo is distribued under the terms of the
 GNU General Public License.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA}

    unit Unit8;

{$mode objfpc}{$H+}

interface

uses
  LCLIntf, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Spin, LResources,UnitScaleFont,UChaines;

type

  { Tsaisienombresommet }

  Tsaisienombresommet = class(TForm)
    BitBtn1: TBitBtn;
    GroupBox1: TGroupBox;
    StaticText1: TLabel;
    SpinEdit1: TSpinEdit;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private  encreation:boolean;
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

var
  saisienombresommet: Tsaisienombresommet;
   nbpoly:integer;
implementation


procedure Tsaisienombresommet.BitBtn1Click(Sender: TObject);
begin
nbpoly:=spinedit1.value;
end;

procedure Tsaisienombresommet.FormCreate(Sender: TObject);
begin
   encreation:=true;
   BITBTN1.CAPTION:=rsOK;

CAPTION:=rsNombreDeSomm;

GROUPBOX1.CAPTION:=rsSommetsDeCeP;

STATICTEXT1.CAPTION:=rsNombre;
end;

procedure Tsaisienombresommet.FormShow(Sender: TObject);
begin
 // if encreation then begin scalefont(self); encreation:=false; end;
end;

initialization

  {$i Unit8.lrs}

end.
