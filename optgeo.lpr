{  This file is part of OptGeo, optical simulation software
    Copyright (C) 2004  Jean-Marie Biansan
        Contact: jeanmarie.biansan@free.fr
    Web site: http://jeanmarie.biansan.free.fr/logiciel.html
 From version 1.25, OptGeo is distribued under the terms of the
 GNU General Public License.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA}


    program OptGeo;

{$mode objfpc}{$H+}

uses
{$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Forms, LResources,Interfaces, Unit222, UnitScaleFont,
   MonImprimante,
  ray1 in 'ray1.pas' {Form1},
  Unit_imp in 'Unit_imp.pas' {configimpression},
  Unit3 in 'Unit3.pas' {proprietespoints},
  Unit4 in 'Unit4.pas' {Form4},
  saisiemp in 'saisiemp.pas' {saisiemiroirplan},
  saisieray in 'saisieray.pas' {saisierayon},
  saisiemscopa in 'saisiemscopa.pas' {saisiemiroirscopa},
  saisiemscepa in 'saisiemscepa.pas' {saisiemiroirscepa},
  saisielmc in 'saisielmc.pas' {saisielentillemc},
  saisielmd in 'saisielmd.pas' {saisielentillemd},
  saisiemscore in 'saisiemscore.pas' {saisiemiroirscore},
  saisiemscere in 'saisiemscere.pas' {saisiemiroirscere},
  Unit5 in 'Unit5.pas' {saisienombrerayons},
  saisiesp1 in 'saisiesp1.pas' {saisiespp},
  saisieop1 in 'saisieop1.pas' {saisieopp},
  saisieec in 'saisieec.pas' {saisieecran},
  saisielamesr in 'saisielamesr.pas' {saisielsr},
  Unit6 in 'Unit6.pas' {saisiepolyhedre},
  Unit8 in 'Unit8.pas' {saisienombresommet},
  saisiesphere in 'saisiesphere.pas' {saisiesph},
  saisiemconique in 'saisiemconique.pas' {saisiemiroirconique},
  Unit9 in 'Unit9.pas' {saisietexte},
  Unit10 in 'Unit10.pas' {saisietexte2},
  saisiedia in 'saisiedia.pas' {saisiediaphragme},
  Unit11 in 'Unit11.pas' {saisieespacetravail},
  saisiech in 'saisiech.pas' {saisiechoix},
  saisiegr in 'saisiegr.pas' {saisiegrille},
  saisielec in 'saisielec.pas' {saisielentille},
  unit12 in 'unit12.pas' {saisiediametre},
  Unit13 in 'Unit13.pas' {saisiemetre},
  saisienor in 'saisienor.pas' {saisienormale},
  Unit14 in 'Unit14.pas' {saisieangle},
  Unit15 in 'Unit15.pas' {saisiejoindre},
  saisiefant in 'saisiefant.pas' {saisiefantomes},
  saisiecercle in 'saisiecercle.pas' {saisiepolycercle},
  saisieptssurfaceonde in 'saisieptssurfaceonde.pas' {saisiepointssurfaceonde},
  saisiefle in 'saisiefle.pas' {saisiefleche},
  saisief1 in 'saisief1.pas' {saisiefle1},
  saisieoe in 'saisieoe.pas' {saisieoeil},
  modif_espace_travail in 'modif_espace_travail.pas' {saisieretailleespace},
  Unit16 in 'Unit16.pas' {Form16},
  Unit17 in 'Unit17.pas' {saisiepreferences},
  Unit18 in 'Unit18.pas' {saisiecouleurrayondefaut},
  Unit19 in 'Unit19.pas' {saisieempennage},
  Unitgpl in 'Unitgpl.pas' {Formgpl},
  Unit32 in 'UNIT32.PAS' {FormEditeur},
saisiereseau in 'saisiereseau.pas',
  Monbitmap in 'monbitmap.pas', RTFtoPlainText in 'RTFTOPLAINTEXT.PAS',
  Printer4Lazarus, fiche_affiche_coordonees in 'fiche_affiche_coordonees.pas',
  saisiepri, unit_quelobjet, Unit1_palette, UChaines,
  Unit_indices_milieu_ambiant, MonPostScript, UnitConfigPostScript,
  MonPostScriptCanvas, Definitions, Unit17b, Unit_commune,crt;



{$IFDEF WINDOWS}{$R OptGeo.rc}{$ENDIF}

begin
    {$I OptGeo.lrs}
    {$i curseurs.lrs}

  Application.Initialize;
     SplashScreen := TSplashScreen.Create(Application);
  with SplashScreen do begin
  if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
  end;
  SplashScreen.Show;
  SplashScreen.Update;
  delay(1000);
  application.ProcessMessages;
     Application.Title:='Optgeo';
     Application.CreateForm(TForm1, Form1);
     Application.CreateForm(TFormediteur, Formediteur);
     Application.CreateForm(Tproprietespoints, proprietespoints);
     Application.CreateForm(TForm4, Form4);
     Application.CreateForm(Tsaisiemiroirplan, saisiemiroirplan);
     Application.CreateForm(Tsaisierayon, saisierayon);
     Application.CreateForm(Tsaisiemiroirscopa, saisiemiroirscopa);
     Application.CreateForm(Tsaisiemiroirscepa, saisiemiroirscepa);
     Application.CreateForm(Tsaisielentillemc, saisielentillemc);
     Application.CreateForm(Tsaisielentillemd, saisielentillemd);
     Application.CreateForm(Tsaisiemiroirscore, saisiemiroirscore);
     Application.CreateForm(Tsaisiemiroirscere, saisiemiroirscere);
     Application.CreateForm(Tsaisienombrerayons, saisienombrerayons);
     Application.CreateForm(Tsaisieecran, saisieecran);
     Application.CreateForm(Tsaisielsr, saisielsr);
     Application.CreateForm(Tsaisiepolyhedre, saisiepolyhedre);
     Application.CreateForm(Tsaisienombresommet, saisienombresommet);
     Application.CreateForm(Tsaisiesph, saisiesph);
     Application.CreateForm(Tsaisiemiroirconique, saisiemiroirconique);
     Application.CreateForm(Tsaisietexte, saisietexte);
     Application.CreateForm(Tsaisietexte2, saisietexte2);
     Application.CreateForm(Tsaisiediaphragme, saisiediaphragme);
     Application.CreateForm(Tsaisieespacetravail, saisieespacetravail);
     Application.CreateForm(Tsaisiechoix, saisiechoix);
     Application.CreateForm(Tsaisiegrille, saisiegrille);
     Application.CreateForm(Tsaisielentille, saisielentille);
     Application.CreateForm(Tsaisiediametre, saisiediametre);
     Application.CreateForm(Tsaisiemetre, saisiemetre);
     Application.CreateForm(Tsaisienormale, saisienormale);
     Application.CreateForm(Tsaisieangle, saisieangle);
     Application.CreateForm(Tsaisiejoindre, saisiejoindre);
     Application.CreateForm(Tsaisiefantomes, saisiefantomes);
     Application.CreateForm(Tsaisiepolycercle, saisiepolycercle);
     Application.CreateForm(Tsaisiepointssurfaceonde, saisiepointssurfaceonde);
     Application.CreateForm(Tsaisiefleche, saisiefleche);
     Application.CreateForm(Tsaisiefle1, saisiefle1);
     Application.CreateForm(Tsaisieoeil, saisieoeil);
     Application.CreateForm(Tsaisieretailleespace, saisieretailleespace);
     Application.CreateForm(TForm16, Form16);
     Application.CreateForm(Tsaisiepreferences, saisiepreferences);
     Application.CreateForm(Tsaisiecouleurrayondefaut, saisiecouleurrayondefaut
       );
     Application.CreateForm(Tsaisieempennage, saisieempennage);
     Application.CreateForm(Tfiche_coordonnees_segments,
       fiche_coordonnees_segments);
     Application.CreateForm(Tconfigimpression, configimpression);
     Application.CreateForm(Tsaisieprisme, saisieprisme);
     Application.CreateForm(TForm_quelobjet, Form_quelobjet);
     Application.CreateForm(Tform_palette, form_palette);
     Application.CreateForm(TFormgpl, Formgpl);
     Application.CreateForm(Tsaisie_indices_milieu_ambiant,
       saisie_indices_milieu_ambiant);
     Application.CreateForm(TFormConfigPostScript, FormConfigPostScript);
     Application.CreateForm(Tsaisieop, saisieopp);
     Application.CreateForm(Tsaisiesp,saisiespp);
     Application.CreateForm(Tsaisiereseau, fsaisiereseau);


  formediteur.caption:=rsCommentaires;
   Application.Run;
    if (SplashScreen<>nil) then begin
    SplashScreen.Free;
    SplashScreen:=nil;
  end;
end.
