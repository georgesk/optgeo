{  This file is part of OptGeo, optical simulation software
    Copyright (C) 2004  Jean-Marie Biansan
        Contact: jeanmarie.biansan@free.fr
    Web site: http://jeanmarie.biansan.free.fr/logiciel.html
 From version 1.25, OptGeo is distribued under the terms of the
 GNU General Public License.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA}

    unit saisiedia;

{$mode objfpc}{$H+}

interface

uses
  LCLIntf, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, Buttons, Spin,ColorBox, LResources,LCLType,UnitScaleFont,
  UChaines;

type

  { Tsaisiediaphragme }

  Tsaisiediaphragme = class(TForm)
    BitBtn2: TBitBtn;
    ColorGrid1: TColorBox;
    Image1: TImage;
    GroupBox1: TGroupBox;
    StaticText1: TLabel;
    StaticText2: TLabel;
    editcx: TEdit;
    editcy: TEdit;
    GroupBox3: TGroupBox;
    BitBtn1: TBitBtn;
    GroupBox6: TGroupBox;
    StaticText5: TLabel;
    editrint: TEdit;
    boutonsup: TBitBtn;
    GroupBox2: TGroupBox;
    StaticText3: TLabel;
    editrext: TEdit;
    GroupBox4: TGroupBox;
    StaticText4: TLabel;
    editt: TEdit;
    StaticText10: TLabel;
    GroupBox5: TGroupBox;
    SpinEdit1: TSpinEdit;
    log1: TLabel;
    log2: TLabel;
    log3: TLabel;
    log4: TLabel;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private  encreation:boolean;
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

var
  saisiediaphragme: Tsaisiediaphragme;

  dia_epaisseur:integer;
  dia_couleur:tcolor;
  dia_cx,dia_cy,dia_t,dia_rint,
  dia_rext:extended;
implementation

uses unit222;


procedure Tsaisiediaphragme.BitBtn1Click(Sender: TObject);
begin

dia_couleur:=colorgrid1.selected;

try
dia_cx:=strtofloat(editcx.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce8),
 pchar(rsAttention), mb_ok);
 exit;
 end;  end;
 if ((dia_cx<xxmin) or (dia_cx>xxmax)) then begin
 application.messagebox(pchar(rsValeurInacce8),
 pchar(rsAttention),mb_ok);
 exit;
 end;

 try
dia_cy:=strtofloat(editcy.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce9),
 pchar(rsAttention),mb_ok);
 exit;
 end;  end;
 if ((dia_cy<yymin) or (dia_cy>yymax)) then begin
 application.messagebox(pchar(rsValeurInacce9),
 pchar(rsAttention),mb_ok);
 exit;
 end;

 try
dia_rint:=strtofloat(editrint.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce10),
 pchar(rsAttention),mb_ok);
 exit;
 end;  end;
 dia_rint:=abs(dia_rint);

 try
dia_rext:=strtofloat(editrext.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce11),
 pchar(rsAttention),mb_ok);
 exit;
 end;  end;
  dia_rext:=abs(dia_rext);


 try
dia_t:=strtofloat(editt.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce12),
 pchar(rsAttention),mb_ok);
 exit;
 end;  end;

 dia_epaisseur:=spinedit1.value;
  ReTaillePdiaphragme(Listediaphragme,nombrediaphragme,nombrediaphragme+1);
 inc(nombrediaphragme);
if not
(Listediaphragme[-1+nombrediaphragme].create2(dia_cx,dia_cy,
dia_t,dia_rint,dia_rext,
dia_couleur,dia_epaisseur)) then begin
ReTaillePdiaphragme(Listediaphragme,nombrediaphragme,nombrediaphragme-1);
dec(nombrediaphragme);
application.messagebox(pchar(rsDonnEsAberra2),
pchar(rsAttention),mb_ok);
end else
self.modalresult:=mrok;
Rafraichit;


end;

procedure Tsaisiediaphragme.FormCreate(Sender: TObject);
begin
   encreation:=true;
   BITBTN1.CAPTION:=rsOK2;


BITBTN2.CAPTION:=rsAnnuler;


BOUTONSUP.CAPTION:=rsSupprimerCeD;


CAPTION:=rsAjoutDUnDiap2;


GROUPBOX1.CAPTION:=rsCentreC;


GROUPBOX2.CAPTION:=rsRayonExtRieu;


GROUPBOX3.CAPTION:=rsCouleurDuDia;


GROUPBOX4.CAPTION:=rsAnglePolaire;


GROUPBOX5.CAPTION:=rsEpaisseurDuT;


GROUPBOX6.CAPTION:=rsRayonIntRieu2;


LOG1.CAPTION:=rsLog1;


LOG2.CAPTION:=rsLog2;


LOG3.CAPTION:=rsLog3;


LOG4.CAPTION:=rsLog4;


STATICTEXT1.CAPTION:=rsX;


STATICTEXT10.CAPTION:=rsRad;


STATICTEXT2.CAPTION:=rsY;


STATICTEXT3.CAPTION:=rsRext;


STATICTEXT4.CAPTION:=rsT;


STATICTEXT5.CAPTION:=rsRint;

end;

procedure Tsaisiediaphragme.FormShow(Sender: TObject);
begin
 // if encreation then begin scalefont(self); encreation:=false; end;
end;

initialization
  {$i saisiedia.lrs}


end.
