 {  This file is part of OptGeo, optical simulation software
    Copyright (C) 2004  Jean-Marie Biansan
        Contact: jeanmarie.biansan@free.fr
    Web site: http://jeanmarie.biansan.free.fr/logiciel.html
 From version 1.25, OptGeo is distribued under the terms of the
 GNU General Public License.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA}


    unit saisienor;

{$mode objfpc}{$H+}

interface

uses
  LCLIntf, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Colorbox, Spin,unit222, LResources,UnitScaleFont,
  UChaines;

type

  { Tsaisienormale }

  Tsaisienormale = class(TForm)
    BitBtn1: TBitBtn;
        GroupBox1: TGroupBox;
    checknormale: TCheckBox;
    GroupBox2: TGroupBox;
    spinepaisseur: TSpinEdit;
    GroupBox3: TGroupBox;
    ColorGrid1: TColorBox;
    GroupBox4: TGroupBox;
    spinpourcentage: TSpinEdit;
    GroupBox5: TGroupBox;
    checkangles: TCheckBox;
    spindecimalesangles: TSpinEdit;
    StaticText1: TLabel;
    
    procedure BitBtn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private  encreation:boolean;
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

var
  saisienormale: Tsaisienormale;

implementation


procedure Tsaisienormale.BitBtn1Click(Sender: TObject);
begin
pourcentagenormale:=spinpourcentage.value/100;
couleurnormale:=colorgrid1.selected;
optionnormale:=checknormale.checked;
epaisseurnormale:=spinepaisseur.value;
optionangles:=checkangles.checked;
if optionangles then optionnormale:=true;
decimalesangles:=spindecimalesangles.value;
end;

procedure Tsaisienormale.FormCreate(Sender: TObject);
begin
   encreation:=true;
   BITBTN1.CAPTION
// msgctxt ';TSAISIENORMALE.BITBTN1.CAPTION';
:=rsOK;


CAPTION
:=rsOptionsPourL;


CHECKANGLES.CAPTION
// msgctxt ';TSAISIENORMALE.CHECKANGLES.CAPTION';
:=rsAfficher2;


CHECKNORMALE.CAPTION
:=rsTracer;


GROUPBOX1.CAPTION
:=rsTracerLesNor;


GROUPBOX2.CAPTION
:=rsEpaisseurDeL;


GROUPBOX3.CAPTION
:=rsCouleurDeLaT;


GROUPBOX4.CAPTION
:=rsLongueurTrac;


GROUPBOX5.CAPTION
:=rsAfficherVale;


STATICTEXT1.CAPTION
:=rsNombreDeDCim;


end;

procedure Tsaisienormale.FormShow(Sender: TObject);
begin
  //if encreation then begin scalefont(self); encreation:=false; end;
end;


initialization
  {$i saisienor.lrs}


end.
