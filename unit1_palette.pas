unit Unit1_palette;

{$mode objfpc}

interface

uses
  Classes, SysUtils, LazFileUtils, LResources, Forms, Controls, Graphics, Dialogs,
  Buttons,LCLType, ExtCtrls,UnitScaleFont,UChaines;

type

  { Tform_palette }

  Tform_palette = class(TForm)
    boutondeplacement: TSpeedButton;
    boutondupliquer: TSpeedButton;
    boutonprop: TSpeedButton;
    boutonsuppression: TSpeedButton;
    boutongroupe: TSpeedButton;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    SpeedButton6: TSpeedButton;
    SpeedButton5: TSpeedButton;
    SpeedButton4: TSpeedButton;
    SpeedButton3: TSpeedButton;
    SpeedButton7: TSpeedButton;
    procedure boutondeplacementClick(Sender: TObject);
    procedure boutondupliquerClick(Sender: TObject);
    procedure boutonpropClick(Sender: TObject);
      procedure boutonsuppressionClick(Sender: TObject);
    procedure boutongroupeClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);

    procedure SpeedButton1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure SpeedButton1MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure SpeedButton1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure SpeedButton7Click(Sender: TObject);
  private
    encreation, debut_deplacement :boolean;
    { private declarations }
  public
    { public declarations }
  end; 

var
  form_palette: Tform_palette;
  MouseX ,MouseY:integer;
implementation
         uses ray1,unit222;
{ Tform_palette }

procedure Tform_palette.FormCreate(Sender: TObject);
begin
 encreation :=true;
  debut_deplacement:=false;
  BOUTONDEPLACEMENT.CAPTION:=rsDPlacer;
  BOUTONDEPLACEMENT.HINT:=
rsDPlacerUnLMe;
  BOUTONDUPLIQUER.CAPTION:= rsDupliquer;
  BOUTONDUPLIQUER.HINT:=rsDupliquerUnL;
 BOUTONGROUPE.CAPTION:=rsGrouper;
 BOUTONGROUPE.HINT :=rsGrouperDesLM2;
  BOUTONPROP.CAPTION:=rsPropriTS;
   BOUTONPROP.HINT:=rsPropriTSDUnL;
  BOUTONSUPPRESSION.CAPTION:=rsSupprimer;
  BOUTONSUPPRESSION.HINT :=rsSupprimerUnL;
    CAPTION:=rsActions;
  SPEEDBUTTON1.HINT:=rsPourDPlacerC;
  SPEEDBUTTON2.CAPTION :=rsExportPS;
  SPEEDBUTTON2.HINT:=rsExporterLaSi2;
  SPEEDBUTTON3.CAPTION :=rsImprimer;
  SPEEDBUTTON3.HINT:=rsImprimerLaSi;
   SPEEDBUTTON4.CAPTION:=rsExportPNG;
   SPEEDBUTTON4.HINT:=rsExporterLaSi;
   SPEEDBUTTON5.CAPTION:=rsExportJPG;
   SPEEDBUTTON5.HINT:=rsExportDeLaSi;
    SPEEDBUTTON6.CAPTION:=rsCopier;
    SPEEDBUTTON6.HINT:=rsCopierSimula;
    SPEEDBUTTON7.CAPTION:=rsGrouperTout;
    SPEEDBUTTON7.HINT:=rsCrErUnGroupe;





end;

procedure Tform_palette.FormShow(Sender: TObject);
begin
  // if encreation then begin scalefont(self); encreation:=false; end;
end;





procedure Tform_palette.SpeedButton1MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
debut_deplacement:=true;
 MouseX := X;
       MouseY := Y;
end;

procedure Tform_palette.SpeedButton1MouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
 var P : TPoint;

 begin

 if debut_deplacement then begin
  P := ClientToScreen(Point(X, Y));
  SetBounds(P.X-MouseX, P.Y-MouseY, Width, Height);

 end;

end;

procedure Tform_palette.SpeedButton1MouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
 debut_deplacement:=false;
end;

procedure Tform_palette.SpeedButton2Click(Sender: TObject);
begin
  Form1.MenuItem8Click(Sender);
end;

procedure Tform_palette.SpeedButton3Click(Sender: TObject);
begin
  Form1.Imprimer1Click(Sender);
end;

procedure Tform_palette.SpeedButton4Click(Sender: TObject);
begin
  Form1.MenuItem3Click(Sender);
end;

procedure Tform_palette.SpeedButton5Click(Sender: TObject);
begin
  Form1.MenuItem4Click(Sender);
end;

procedure Tform_palette.SpeedButton6Click(Sender: TObject);
begin
    Form1.Copier1Click(Sender);
end;

procedure Tform_palette.SpeedButton7Click(Sender: TObject);
var i:integer;
begin
if form1.nombrequelquechose<2 then begin
application.MessageBox(pchar('Impossible de grouper moins de 2 éléments'),
pchar('Hélas...'),mb_ok);
exit;
end;
  retaillepgroupe(listegroupe,nombregroupe,nombregroupe+1);
 nombregroupe:=nombregroupe+1;
   listegroupe[-1+nombregroupe].create;
   if nombremiroirplan>0 then
   for i:=1 to nombremiroirplan do listegroupe[-1+nombregroupe].add(mdmiroirplan,i);

    if nombreReseau>0 then
   for i:=1 to nombreReseau do listegroupe[-1+nombregroupe].add(mdReseau,i);


   if nombremscopa>0 then
   for i:=1 to nombremscopa do listegroupe[-1+nombregroupe].add(mdmscopa,i);

    if nombremscepa>0 then
   for i:=1 to nombremscepa do listegroupe[-1+nombregroupe].add(mdmscepa,i);

    if nombremscore>0 then
   for i:=1 to nombremscore do listegroupe[-1+nombregroupe].add(mdmscore,i);

    if nombremscere>0 then
   for i:=1 to nombremscere do listegroupe[-1+nombregroupe].add(mdmscere,i);

    if nombrelmc>0 then
   for i:=1 to nombrelmc do listegroupe[-1+nombregroupe].add(mdlmc,i);

    if nombrelmd>0 then
   for i:=1 to nombrelmd do listegroupe[-1+nombregroupe].add(mdlmd,i);

    if nombrerayon>0 then
   for i:=1 to nombrerayon do listegroupe[-1+nombregroupe].add(mdrayon,i);

    if nombrelsr>0 then
   for i:=1 to nombrelsr do listegroupe[-1+nombregroupe].add(mdlsr,i);

    if nombresourceponctuelle>0 then
   for i:=1 to nombresourceponctuelle do listegroupe[-1+nombregroupe].add(mdsourceponctuelle,i);

    if nombreondeplane>0 then
   for i:=1 to nombreondeplane do listegroupe[-1+nombregroupe].add(mdondeplane,i);

    if nombreecran>0 then
   for i:=1 to nombreecran do listegroupe[-1+nombregroupe].add(mdecran,i);

    if nombrepolyhedre>0 then
   for i:=1 to nombrepolyhedre do listegroupe[-1+nombregroupe].add(mdpolyhedre,i);

    if nombresphere>0 then
   for i:=1 to nombresphere do listegroupe[-1+nombregroupe].add(mdsphere,i);

    if nombremiroirconique>0 then
   for i:=1 to nombremiroirconique do listegroupe[-1+nombregroupe].add(mdmiroirconique,i);

    if nombretexte>0 then
   for i:=1 to nombretexte do listegroupe[-1+nombregroupe].add(mdtexteaffiche,i);

    if nombrediaphragme>0 then
   for i:=1 to nombrediaphragme do listegroupe[-1+nombregroupe].add(mddiaphragme,i);

    if nombrelec>0 then
   for i:=1 to nombrelec do listegroupe[-1+nombregroupe].add(mdlec,i);

    if nombrepolycercle>0 then
   for i:=1 to nombrepolycercle do listegroupe[-1+nombregroupe].add(mdpolycercle,i);

    if nombreangle>0 then
   for i:=1 to nombreangle do listegroupe[-1+nombregroupe].add(mdangle,i);

    if nombrefleche>0 then
   for i:=1 to nombrefleche do listegroupe[-1+nombregroupe].add(mdfleche,+i);

    if nombreoeil>0 then
   for i:=1 to nombreoeil do listegroupe[-1+nombregroupe].add(mdoeil,i);

    if nombremetre>0 then
   for i:=1 to nombremetre do listegroupe[-1+nombregroupe].add(mdmetre,i);

    if nombreprisme>0 then
   for i:=1 to nombreprisme do listegroupe[-1+nombregroupe].add(mdprisme,i);

application.MessageBox(pchar(rsUnNouveauGro2),
pchar(rsCrAtionRUssi), mb_ok);
end;

procedure Tform_palette.boutondeplacementClick(Sender: TObject);
begin
  if (form1.nombrequelquechose=0)
 then exit;

if boutondeplacement.caption=rsDPlacer then begin
 boutondeplacement.OnClick:=nil;
form1.image1.Cursor:=crdepla;
modemodif:=true;
modifencours:=false;
application.ProcessMessages;
boutondeplacement.OnClick:=@boutondeplacementclick;
form1.desactiveboutons;
boutondeplacement.Enabled:=true;
boutondeplacement.caption:=rsFinDPl;
Rafraichit;
end else begin
 form1.activeboutons;
modemodif:=false;
modifencours:=false;
boutondeplacement.caption:=rsDPlacer;
form1.image1.Cursor:=crDefault;
form1.StatusBar1.Panels[2].text:='';
form1.StatusBar1.Panels[3].text:='';
Rafraichit;
end;

end;

procedure Tform_palette.boutondupliquerClick(Sender: TObject);
begin
  if (form1.nombrequelquechose=0)
 then exit;
 if boutondupliquer.caption=rsDupliquer then begin
 boutondupliquer.OnClick:=nil;
form1.image1.Cursor:=crdefault;
modeduplication:=true; modeinfo:=true;
application.ProcessMessages;
form1.desactiveboutons;
boutondupliquer.Enabled:=true;
boutondupliquer.caption:=rsFinDupl;
boutondupliquer.OnClick:=@boutondupliquerclick;
Rafraichit;
end else begin
form1.activeboutons;
modeduplication:=false; modeinfo:=false;
boutondupliquer.caption:=rsDupliquer;
form1.image1.Cursor:=crDefault;
form1.StatusBar1.Panels[2].text:='';
form1.StatusBar1.Panels[3].text:='';
Rafraichit;
end;
end;

procedure Tform_palette.boutonpropClick(Sender: TObject);
begin

if boutonprop.caption=rsPropriTS then begin
 boutonprop.OnClick:=nil;
form1.image1.Cursor:=crprop;
modeinfo:=true;
application.ProcessMessages;
boutonprop.OnClick:=@boutonpropclick;
form1.desactiveboutons;
boutonprop.Enabled:=true;
boutonprop.caption:=rsFinProp;
Rafraichit;
end else begin
 form1.activeboutons;
modeinfo:=false;
boutonprop.caption:=rsPropriTS;
form1.image1.Cursor:=crDefault;
form1.StatusBar1.Panels[2].text:='';
form1.StatusBar1.Panels[3].text:='';
Rafraichit;
end;
end;

procedure Tform_palette.boutonsuppressionClick(Sender: TObject);
begin
 if (form1.nombrequelquechose=0)
 then exit;
 if boutonsuppression.caption=rsSupprimer then begin
boutonsuppression.OnClick:=nil;
form1.image1.Cursor:=crdefault;
form1.desactiveboutons;
modesuppression:=true;
boutonsuppression.Enabled:=true;
boutonsuppression.caption:=rsFinSup;
application.ProcessMessages;
boutonsuppression.OnClick:=@boutonsuppressionclick;
 Rafraichit;
end else begin
form1.activeboutons;
modesuppression:=false;
boutonsuppression.caption:=rsSupprimer;
form1.image1.Cursor:=crDefault;
form1.StatusBar1.Panels[2].text:='';
form1.StatusBar1.Panels[3].text:='';
Rafraichit;
end;
end;

procedure Tform_palette.boutongroupeClick(Sender: TObject);
var ii:integer;
begin

if boutongroupe.caption=rsGrouper then begin

if form1.Nombrequelquechose<2 then begin
    application.MessageBox(pchar(rsImpossibleDe2), pchar(rsAttention3), mb_ok);
exit;
end;
 if Nombregroupe=maxgroupe then begin
    application.MessageBox(pchar(rsNombreMaxima), pchar(rsAttention3), mb_ok);
exit;
end;

boutongroupe.caption:=rsFinGroupemen;
boutongroupe.Hint:=rsFinDeLaCrAti;
 boutongroupe.OnClick:=nil;
form1.image1.Cursor:=crPoint1;
form1.StatusBar1.Panels[2].text:=rsCliquezSurLe;
 modegroupement:=true;
 groupeprovisoire.create;
Rafraichit;
application.ProcessMessages;
                      boutongroupe.OnClick:=@boutongroupeclick;
form1.desactiveboutons;
boutongroupe.enabled:=true;
end else
begin
if groupeprovisoire.nombreelements>1 then begin
retaillepgroupe(listegroupe,nombregroupe,nombregroupe+1);
inc(nombregroupe);
listegroupe[-1+nombregroupe].create;
 for ii:=1 to groupeprovisoire.nombreelements do
 listegroupe[-1+nombregroupe].add(groupeprovisoire.listeelements[ii].typeelementgroupe,
 groupeprovisoire.listeelements[ii].indiceelement);
 application.MessageBox(pchar(Format(rsLeGroupeNVie, [inttostr(nombregroupe)])),
  pchar(rsFait), mb_ok);
end;

boutongroupe.caption:=rsGrouper;
boutongroupe.Hint:=rsGrouperDesLM;
form1.activeboutons;
modegroupement:=false;


Rafraichit;
form1.StatusBar1.Panels[2].text:='';
 form1.image1.Cursor:=crdefault;
end;

end;

procedure Tform_palette.FormCloseQuery(Sender: TObject; var CanClose: boolean);
begin
  canclose:=false;
end;

initialization
  {$I unit1_palette.lrs}

end.

