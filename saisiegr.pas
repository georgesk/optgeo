{  This file is part of OptGeo, optical simulation software
    Copyright (C) 2004  Jean-Marie Biansan
        Contact: jeanmarie.biansan@free.fr
    Web site: http://jeanmarie.biansan.free.fr/logiciel.html
 From version 1.25, OptGeo is distribued under the terms of the
 GNU General Public License.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA}


    unit saisiegr;

{$mode objfpc}{$H+}

interface

uses
  LCLIntf, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons,unit222, LResources,LCLType,UnitScaleFont,UChaines;

type

  { Tsaisiegrille }

  Tsaisiegrille = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    GroupBox1: TGroupBox;
    StaticText1: TLabel;
    StaticText2: TLabel;
    editpash: TEdit;
    Editpasv: TEdit;
    GroupBox2: TGroupBox;
    cocheattraction: TCheckBox;
    GroupBox3: TGroupBox;
    cocheafficher: TCheckBox;
    log1: TLabel;
    log2: TLabel;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private  encreation:boolean;
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

var
  saisiegrille: Tsaisiegrille;

implementation

uses ray1;


procedure Tsaisiegrille.BitBtn1Click(Sender: TObject);
begin
try
grillex:=abs(strtofloat(editpash.text));
except
application.messagebox(pchar(rsValeurDuPasH),
pchar(rsAttention3), mb_ok);
modalresult:=mrno;
exit;
end;

if grillex=0 then begin
application.messagebox(pchar(rsValeurDuPasH),
pchar(rsattention),mb_ok);
modalresult:=mrno;
exit;
end;


if ((grillex>(xxmax-xxmin)/3) or (grillex<(xxmax-xxmin)/1000)) then
begin
if application.messagebox(pchar(rsValeurDuPasH2),
pchar(rsattention),mb_yesno)=idno then begin
modalresult:=mrno;
exit; end;
end;

try
grilley:=abs(strtofloat(editpasv.text));
except
application.messagebox(pchar(rsValeurDuPasH),
pchar(rsattention),mb_ok);
modalresult:=mrno;
exit;
end;

if grilley=0 then begin
application.messagebox(pchar(rsValeurDuPasV),
pchar(rsattention),mb_ok);
modalresult:=mrno;
exit;
end;


if ((grilley>(yymax-yymin)/3) or (grilley<(yymax-yymin)/1000)) then
begin
if application.messagebox(pchar(rsValeurDuPasV2),
pchar(rsattention),mb_yesno)=idno then begin
modalresult:=mrno;
exit; end;
end;

tracergrille:=cocheafficher.checked;
attractiongrille:=cocheattraction.checked;
form1.afficherlagrille1.checked:=tracergrille;
form1.attractiondelagrille1.checked:=attractiongrille;
end;

procedure Tsaisiegrille.FormCreate(Sender: TObject);
begin
   encreation:=true;
   BITBTN1.CAPTION
:=rsOK;


BITBTN2.CAPTION
:=rsAnnuler;


CAPTION
:=rsPropriTSDeLa;


COCHEAFFICHER.CAPTION
:=rsAfficher2;


COCHEATTRACTION.CAPTION
:=rsAttraction;


GROUPBOX1.CAPTION
:=rsPasDeLaGrill;


GROUPBOX2.CAPTION
:=rsAttractionDe;


GROUPBOX3.CAPTION
:=rsAfficherLaGr;


LOG1.CAPTION
:=rsLog1;


LOG2.CAPTION
:=rsLog2;


STATICTEXT1.CAPTION
:=rsPasHorizonta;


STATICTEXT2.CAPTION
:=rsPasVertical;

end;

procedure Tsaisiegrille.FormShow(Sender: TObject);
begin
 // if encreation then begin scalefont(self); encreation:=false; end;
end;

initialization
  {$i saisiegr.lrs}


end.
