unit Unit_imp;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, LazFileUtils, LResources, Forms, Controls, Graphics, Dialogs,
  StdCtrls, ExtCtrls, Spin, Buttons,UnitScaleFont,Printers,UChaines;

type

  { Tconfigimpression }
  tgraphehorizontal=(tgauche,tdroite,tcentreh);
tgraphevertical=(thaut,tbas,tcentrev);


  Tconfigimpression = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    CheckBoxrespect: TCheckBox;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    GroupBox4: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    RadioGrouporientation: TRadioGroup;
    RadioGroupevertical: TRadioGroup;
    radiogroupehorizontal: TRadioGroup;
    spinpourcentagevertical: TSpinEdit;
    spinpourcentagehorizontal: TSpinEdit;
    Spinnombrecopies: TSpinEdit;
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private  encreation:boolean;
    { private  encreation:boolean; declarations }
  public
    { public declarations }
  end; 

var
  configimpression: Tconfigimpression;
   abandonimpression,respect_imprimante:boolean;
 graphehorizontal:tgraphehorizontal;
  graphevertical:tgraphevertical;
  taillegraphehorizontal,taillegraphevertical,nombrecopies:integer;

implementation
  uses ray1;
{ Tconfigimpression }

procedure Tconfigimpression.BitBtn1Click(Sender: TObject);
begin
  abandonimpression:=false;
if radiogroupehorizontal.itemindex=1 then graphehorizontal:=tgauche;
if  radiogroupehorizontal.itemindex=0 then graphehorizontal:=tdroite;
if  radiogroupehorizontal.itemindex=2 then graphehorizontal:=tcentreh;
if radiogroupevertical.itemindex=0 then graphevertical:=thaut;
if radiogroupevertical.itemindex=1 then graphevertical:=tbas;
if radiogroupevertical.itemindex=2 then graphevertical:=tcentrev;
taillegraphehorizontal:=spinpourcentagehorizontal.value;
taillegraphevertical:=spinpourcentagevertical.value;
nombrecopies:=spinnombrecopies.value;
if radiogrouporientation.ItemIndex=0 then
orientation_impression:=poportrait else
orientation_impression:=polandscape;
respect_imprimante:=checkboxrespect.Checked;
end;

procedure Tconfigimpression.BitBtn2Click(Sender: TObject);
begin

end;

procedure Tconfigimpression.FormCreate(Sender: TObject);
begin
   encreation:=true;

CHECKBOXRESPECT.CAPTION
:=rsRespecterLes;


GROUPBOX1.CAPTION
:=rsPosition;


GROUPBOX2.CAPTION
:=rsNombreDeCopi;


GROUPBOX3.CAPTION
:=rsTailleEnDeLa;


GROUPBOX4.CAPTION
:=rsRapportHaute;


LABEL1.CAPTION
:=rsNombre;


LABEL2.CAPTION
:=rsHorizontalem;


LABEL3.CAPTION
:=rsVerticalemen;




RADIOGROUPORIENTATION.CAPTION
:=rsOrientation;


  configimpression.Caption :=rsConfiguratio;
  GroupBox1.Caption := rsPosition;
   radiogroupehorizontal.Caption := rsHorizontale;

  // radiogroupehorizontal.Items.clear;
   radiogroupehorizontal.Items[0]:=rsDroite;
     radiogroupehorizontal.Items[1]:=rsGauche;
     radiogroupehorizontal.Items[2]:=rsCentr;

    RadioGroupevertical.Caption := rsVerticale;
    //RadioGroupevertical.Items.clear;
    RadioGroupevertical.Items[0]:=rsHaut;
     RadioGroupevertical.Items[1]:=rsBas;
     RadioGroupevertical.Items[2]:=rsCentr;

     GroupBox2.Caption := rsNombreDeCopi;
     Label1.Caption := rsNombre;
     GroupBox3.Caption := rsTailleEnDeLa;
     Label2.Caption := rsHorizontalem;
     Label3.Caption := rsVerticalemen;
     RadioGrouporientation.Caption := rsOrientation;
     //RadioGrouporientation.Items.clear;
     RadioGrouporientation.Items[0]:=rsPortrait;
      RadioGrouporientation.Items[1]:= rsPaysage;
       BitBtn2.Caption := rsAnnuler;
       BitBtn1.Caption := rsOK;


end;

procedure Tconfigimpression.FormShow(Sender: TObject);
begin
 // if encreation then begin scalefont(self); encreation:=false; end;
end;

initialization
  {$I unit_imp.lrs}
 grapheHorizontal:=tcentreh;
 graphevertical:=tcentrev;
 taillegraphehorizontal:=90;
 taillegraphevertical:=90;
 nombrecopies:=1;
 orientation_impression:=polandscape;
end.

