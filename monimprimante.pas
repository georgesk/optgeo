
unit MonImprimante;

{$mode objfpc}{$H+}

interface

uses
   Classes, SysUtils, LResources, Forms, Controls, Graphics, Dialogs, ExtCtrls,LCLProc,Math,
   printers,UnitScaleFont;
  type
  TMyimprimante=Object


     Xmin,Xmax,Ymin1,Ymax1,Ymin2,Ymax2,GraduationX, GraduationY1,GraduationY2:extended;

  Largeur,Hauteur,BordureBasse,BordureHaute,
  BordureGauche,BordureDroite,EpaisseurGrille,PuissanceDeDixX,
  PuissancedeDixY1,PuissancedeDixY2,epaisseurcadre,epaisseurgraduation:integer;
  longueurgraduationX,longueurgraduationY1,longueurgraduationY2:extended;
  PasGrillex,PasGrilley1,PasGrilley2:extended;
  couleurfondsim,couleurcadre,couleurgraduation,
  couleurgrille1,couleurgrille2:tcolor;
  cadre,gradue,grille1,grille2,fond,borduresverticalessymetriques,echelle_g,echelle_d:boolean;
   titre,labelx,labely1,labely2,unitex,unitey1,unitey2:string;
  fontegraduation:tfont;
  fontetitre:tfont;
  procedure background(couleur:tcolor);


  function LimitesEtAxes(_xmin,_xmax,_ymin1,_ymax1,_ymin2,_ymax2:extended;
 _Largeur,_Hauteur:integer;
 _titre:string;
  _fontetitre:tfont;
  _Fond:boolean;
 _CouleurFondsim:tcolor;
 _Cadre:boolean; _EpaisseurCadre:integer;
 _CouleurCadre:tcolor;
 _Gradue:boolean;
 _EpaisseurGraduation,_LongueurGraduation:integer;
 _couleurgraduation:tcolor;
 _fontegraduation:tfont;
 _Grille1,_Grille2:boolean; _EpaisseurGrille:integer;
 _CouleurGrille1,_CouleurGrille2:tcolor;
 _labelx,_labely1,_labely2,_unitex,_unitey1,_unitey2:string;
  _borduresverticalessymetriques,
 _echelle_g,_echelle_d:boolean;
 b_g,b_d,b_b,b_h:integer):boolean;

procedure  dessinergrille(pas_grillex,pas_grilley:extended);

procedure arronditalagrille(var x1,y1:extended;pas_grillex,pas_grilley:extended);
 function invconvert(var xi,yi:integer; x,y:extended;gauche:boolean):boolean;

 function Convert(xi,yi:integer; var x,y:extended;gauche:boolean):boolean;

 function CroixX(x,y:extended;
 demi_diagonale,epaisseur:integer; couleur:tcolor; penmod:tpenmode;gauche:boolean):boolean;

 function CroixP(x,y:extended;
 demi_longueur,epaisseur:integer; couleur:tcolor; penmod:tpenmode;gauche:boolean):boolean;

 function Carreau(x,y:extended;
 demi_diagonale,epaisseur:integer; couleur:tcolor; penmod:tpenmode;gauche:boolean):boolean;

            function Trait(x1,y1,x2,y2:extended; epaisseur:integer;
 couleur:tcolor; sty:tpenstyle; _penmod:tpenmode;gauche:boolean):boolean;

 function Point(x,y:extended; rayon:integer; couleur:tcolor; _penmod:tpenmode;gauche:boolean):boolean;

 function cercle(x,y:extended; rayon,epaisseur:integer; couleur:tcolor; _penmod:tpenmode;gauche:boolean):boolean;

  function disque(x,y:extended; rayon:extended; couleurbord,couleurf:tcolor;_transparent:boolean;gauche:boolean):
 boolean;

  function disque2(x,y:extended; rayon:integer; couleurbord,couleurf:tcolor;gauche:boolean):
 boolean;

  function arcdecercle(x,y:extended; rayonreel:extended;
     epaisseur:integer; theta1,theta2:extended;
     couleur:tcolor; _penmod:tpenmode;_style:tpenstyle;gauche:boolean):boolean;

     function tracepolygone(nombrepoint:integer;listex,listey:array of extended; 
	 couleurbord,couleurf:tcolor;_transparent:boolean;_mode:tpenmode;gauche:boolean):boolean;

   procedure traceconique(theta0,theta1,theta2,fx,fy,excentricite,parametre:extended;
    couleur:tcolor;gauche:boolean);

     procedure arronditalagrille(var x1,y1:extended;gauche:boolean);

      procedure ecrire(x,y:extended; s:string;gauche:boolean; police:tfont);
       procedure ecrireI(x,y:integer; s:string; police:tfont);
      procedure graduation(xi,xa:extended;
var xci,xca:extended; var graduation:extended;
var PuissanceDeDix:integer; var nombregraduation:integer);

  end;
   procedure echange(var a,b:extended);

       procedure echange_entiers(var a,b:integer);

        function partieentiere(x:extended):extended;

         function dix_pp(p:longint):extended;


implementation

uses ray1,unit222;

procedure TMyimprimante.arronditalagrille(var x1,y1:extended;pas_grillex,pas_grilley:extended);
  var newx,newy,divx,divy,fracx,fracy:extended;
  begin
  divx:=partieentiere((x1-xmin)/pas_grillex);
  divy:=partieentiere((y1-ymin1)/pas_grilley);
  fracx:=x1-xmin-divx*pas_grillex;
  fracy:=y1-ymin1-divy*pas_grilley;
  if fracx>0.5*pas_grillex then newx:=xmin+(divx+1)*pas_grillex else newx:=xmin+divx*pas_grillex;
  if fracy>0.5*pas_grilley then newy:=ymin1+(divy+1)*pas_grilley else newy:=ymin1+divy*pas_grilley;
  x1:=newx; y1:=newy;
                        end;
procedure  TMyimprimante.dessinergrille(pas_grillex,pas_grilley:extended);
 var i,j,x1,y1:integer; x,y:extended;
  begin
 if grille1 then begin


  for i:=1 to trunc((xmax-xmin)/pas_grillex) do
  for j:=1 to trunc((ymax1-ymin1)/pas_grilley) do begin
  x:=xmin+i*pas_grillex; y:=ymin1+j*pas_grilley;
 x1:= trunc((x-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite))+
 BordureGauche;
 y1:=trunc((ymax1-y)/(ymax1-ymin1)*(hauteur-BordureHaute-BordureBasse))+
 BordureHaute;


    imprimante.canvas.pixels[x1,y1]:=couleurgrille1;

    end;   end; end;

procedure TMyimprimante.graduation(xi,xa:extended;
var xci,xca:extended; var graduation:extended;
var PuissanceDeDix:integer; var nombregraduation:integer);

var n,gm,g,d,logd,tht:extended;
p,i,xpa,xpi:longint;
res:array[1..12] of extended;
nb:longint;

begin
if xa<xi then echange(xi,xa);
d:=xa-xi;
 logd:=ln(d)/ln(10);
if (logd>=0) then p :=longint(trunc(logd)) else
p:=longint(trunc(logd)-1);

  PuissanceDeDix:=p;
if xa>=0 then begin
gm:=trunc(xa/dix_pp(p-1))*dix_pp(p-1);
if (gm>=xa) then xpa:=trunc(xa/dix_pp(p-1)) else
xpa:=trunc(xa/dix_pp(p-1))+1;
   end;
if xa<0 then begin
gm:=trunc(xa/dix_pp(p-1))*dix_pp(p-1);
if (gm>=xa) then xpa:=trunc(xa/dix_pp(p-1)) else
xpa:=trunc(xa/dix_pp(p-1));
       end;

if xi>=0 then begin
gm:=trunc(xi/dix_pp(p-1))*dix_pp(p-1);
if (gm<=xi) then xpi:=trunc(xi/dix_pp(p-1)) else
xpi:=trunc(xi/dix_pp(p-1))-1;
           end;
if xi<0 then begin
gm:=trunc(xi/dix_pp(p-1))*dix_pp(p-1);
if (gm<=xi) then xpi:=trunc(xi/dix_pp(p-1)) else
xpi:=trunc(xi/dix_pp(p-1))-1;
               end;


case (xpa-xpi ) of
1: g:=0.1;
2: g:=0.2;
3,4,5: g:=0.5;
6,7,8,9,10: g:=1;
 else
 g:=trunc((xpa-xpi-1)/10+0.001)+1;
end;

        graduation:=g*dix_pp(p-1);


{xca:=xpa*dix_pp(p-1);
nombregraduation:=trunc((xpa-xpi)/g);
xci:=xca-graduation*nombregraduation;
if xci>xi then begin
xci:=xci-graduation;
inc(nombregraduation);
 end;  }
 xci:=xpi*dix_pp(p-1);
nombregraduation:=trunc((xpa-xpi)/g);
xca:=xci+graduation*nombregraduation;
if xca<xa then begin
xca:=xca+graduation;
inc(nombregraduation);
 end;
{for i:=1 to nb do writeln(res[i]);}
end;




 function dix_pp(p:longint):extended;
var i:longint;
inter:extended;
begin

if (p<-1) then begin
inter:=1/10;
for i:=1 to abs(p)-1 do
inter:=inter/10;
result:=inter;
end else


if p=0 then begin
result:=1;
end else

if p=1 then begin
 result:=10;
end else

if (p>1) then begin
inter:=10;
for i:=1 to p-1 do
inter:=inter*10;
result:=inter;
end else

if (p=-1) then begin
result:=1/10;
end;



       end;


function partieentiere(x:extended):extended;
begin
if x>=0 then  begin
partieentiere:=int(x);
exit;
end;
if frac(x)=0 then begin
partieentiere:=x;
exit;
end;
{cas ou x est <0 et non entier}
partieentiere:=int(x)-1;
end;



function TMyimprimante.disque2(x,y:extended; rayon:integer; couleurbord,couleurf:tcolor;gauche:boolean):
 boolean;

            var x1,y1,r1x,r1y:integer;
            ymax,ymin,pasgrilley:extended;
            old_pencolor:tcolor;
old_penmode:tpenmode;
 old_penstyle:tpenstyle;
 old_penwidth:integer;
 old_brushcolor:tcolor;
 old_brushstyle:tbrushstyle;


  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;
            x1:= trunc((x-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite));
 y1:=trunc((ymax-y)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse));
 if ((x1<=-borduregauche) or (x1>=largeur-borduregauche)
 or (y1>hauteur-bordurehaute) or (y1<-bordurehaute))
  then begin
 disque2:=false;
 exit;
 end;
     x1:= trunc((x-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite))+
 BordureGauche;
 y1:=trunc((ymax-y)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse))+
 BordureHaute;
     if rayon=0 then exit;

 old_pencolor:=imprimante.Canvas.Pen.Color;
old_penmode:=imprimante.Canvas.Pen.Mode;
 old_penstyle:=imprimante.Canvas.Pen.Style;
 old_penwidth:=imprimante.Canvas.Pen.Width;
 old_brushcolor:=imprimante.Canvas.Brush.Color;
 old_brushstyle:=imprimante.Canvas.Brush.Style;

       imprimante.canvas.pen.color:=couleurbord;
     imprimante.canvas.pen.style:=pssolid;
     imprimante.canvas.pen.width:=1;
     imprimante.canvas.pen.mode:=pmcopy;
     disque2:=true;

     imprimante.canvas.brush.style:=bssolid;
     imprimante.canvas.brush.color:=couleurf;

imprimante.canvas.ellipse(x1-rayon,y1-rayon,x1+rayon,
y1+rayon);
imprimante.Canvas.Pen.Color:=old_pencolor;
imprimante.Canvas.Pen.Mode:=old_penmode;
 imprimante.Canvas.Pen.Style:=old_penstyle;
 imprimante.Canvas.Pen.Width:=old_penwidth;
 imprimante.Canvas.Brush.Color:=old_brushcolor;
imprimante.Canvas.Brush.Style:=old_brushstyle;

     end;



	 
	 
	 
	 
	 

 function TMyimprimante.Carreau(x,y:extended;
 demi_diagonale,epaisseur:integer; couleur:tcolor; penmod:tpenmode;gauche:boolean):boolean;
 var x1,y1:integer;
ymax,ymin,pasgrilley:extended;
old_pencolor:tcolor;
old_penmode:tpenmode;
 old_penstyle:tpenstyle;
 old_penwidth:integer;
 old_brushcolor:tcolor;
 old_brushstyle:tbrushstyle;


  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;
 x1:= trunc((x-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite));
 y1:=trunc((ymax-y)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse));
 if ((x1<=-borduregauche) or (x1>=largeur-borduregauche)
 or (y1>hauteur-bordurehaute) or (y1<-bordurehaute))
  then begin
 carreau:=false;
 exit;
 end;

 x1:= trunc((x-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite))+
 BordureGauche;
 y1:=trunc((ymax-y)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse))+
 BordureHaute;

old_pencolor:=imprimante.Canvas.Pen.Color;
old_penmode:=imprimante.Canvas.Pen.Mode;
 old_penstyle:=imprimante.Canvas.Pen.Style;
 old_penwidth:=imprimante.Canvas.Pen.Width;
 old_brushcolor:=imprimante.Canvas.Brush.Color;
 old_brushstyle:=imprimante.Canvas.Brush.Style;


 imprimante.canvas.pen.width:=epaisseur;
  imprimante.canvas.pen.mode:=penmod;
  imprimante.canvas.pen.style:=pssolid;
  imprimante.canvas.pen.color:=couleur;
   imprimante.canvas.moveto(x1,y1+demi_diagonale);
   imprimante.canvas.lineto(x1+demi_diagonale,y1);
    imprimante.canvas.lineto(x1,y1-demi_diagonale);
     imprimante.canvas.lineto(x1-demi_diagonale,y1);
      imprimante.canvas.lineto(x1,y1+demi_diagonale);

   carreau:=true;

imprimante.Canvas.Pen.Color:=old_pencolor;
imprimante.Canvas.Pen.Mode:=old_penmode;
 imprimante.Canvas.Pen.Style:=old_penstyle;
 imprimante.Canvas.Pen.Width:=old_penwidth;
 imprimante.Canvas.Brush.Color:=old_brushcolor;
imprimante.Canvas.Brush.Style:=old_brushstyle;

   end;



    procedure echange_entiers(var a,b:integer);
     var c:integer;
    begin
    c:=a; a:=b; b:=c; end;


    procedure echange(var a,b:extended);
    var c:extended;
    begin
    c:=a; a:=b; b:=c; end;






 procedure TMyimprimante.traceconique(theta0,theta1,theta2,fx,fy,excentricite,parametre:extended;
   couleur:tcolor;gauche:boolean);
   var pas,theta,r,ract,thetaact,nx,ny,ttai:extended;
   nombrepas,i:integer;
   ymax,ymin,pasgrilley:extended;


  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;
   nombrepas:=1000;
   pas:=(theta2-theta1)/nombrepas;
   thetaact:=theta1;
   ract:=parametre/(1+excentricite*cos(thetaact-theta0));
   for i:=1 to nombrepas do begin
   theta:=theta1+i*pas;
   r:=parametre/(1+excentricite*cos(theta-theta0));
   if ((r>0) and (ract>0)) then trait(fx+ract*cos(thetaact),fy+ract*sin(thetaact),fx+r*cos(theta),fy+r*sin(theta),
   1,couleur,pssolid,pmcopy,gauche);
   thetaact:=theta;
   ract:=r;
   end;
    end;


function TMyimprimante.tracepolygone(nombrepoint:integer;listex,listey:array of extended;
  couleurbord,couleurf:tcolor;_transparent:boolean;_mode:tpenmode;gauche:boolean):boolean;
   var i,x1,y1:integer;  titi:array of tpoint;
  ymax,ymin,pasgrilley:extended;
 old_pencolor:tcolor;
old_penmode:tpenmode;
 old_penstyle:tpenstyle;
 old_penwidth:integer;
 old_brushcolor:tcolor;
 old_brushstyle:tbrushstyle;


  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;
  setlength(titi,nombrepoint);
 for i:=1 to nombrepoint do begin
    x1:= trunc((listex[i-1]-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite))+
 BordureGauche;
 y1:=trunc((ymax-listey[i-1])/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse))+
 BordureHaute;
 titi[i-1].x:=x1;
 titi[i-1].y:=y1;
 end;

old_pencolor:=imprimante.Canvas.Pen.Color;
old_penmode:=imprimante.Canvas.Pen.Mode;
 old_penstyle:=imprimante.Canvas.Pen.Style;
 old_penwidth:=imprimante.Canvas.Pen.Width;
 old_brushcolor:=imprimante.Canvas.Brush.Color;
 old_brushstyle:=imprimante.Canvas.Brush.Style;
imprimante.canvas.Pen.Color:=couleurbord;
imprimante.canvas.brush.color:=couleurf;


 imprimante.canvas.brush.style:=bsSolid;
 imprimante.canvas.pen.Color:=couleurbord;
 imprimante.canvas.Pen.Style:=pssolid;
 imprimante.canvas.Pen.Mode:=pmcopy;
 imprimante.canvas.pen.Width:=1;


if not(_transparent) then imprimante.canvas.brush.style:=bsSolid else
imprimante.canvas.brush.style:=bsclear;

 imprimante.canvas.Polygon(titi,false,0,nombrepoint);
imprimante.Canvas.Pen.Color:=old_pencolor;
imprimante.Canvas.Pen.Mode:=old_penmode;
 imprimante.Canvas.Pen.Style:=old_penstyle;
 imprimante.Canvas.Pen.Width:=old_penwidth;
 imprimante.Canvas.Brush.Color:=old_brushcolor;
imprimante.Canvas.Brush.Style:=old_brushstyle;

  end;

  
  
  
  
  
  

     function TMyimprimante.arcdecercle(x,y:extended; rayonreel:extended;
     epaisseur:integer; theta1,theta2:extended;
     couleur:tcolor; _penmod:tpenmode;_style:tpenstyle;gauche:boolean): boolean;

            var x1,y1,rax,ray:integer;
           ymax,ymin,pasgrilley,t1,t2:extended;
old_pencolor:tcolor;
old_penmode:tpenmode;
 old_penstyle:tpenstyle;
 old_penwidth:integer;
 old_brushcolor:tcolor;
 old_brushstyle:tbrushstyle;
  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;
      invconvert(x1,y1,x,y,gauche);
   rax:= trunc(rayonreel/(xmax-xmin)*(largeur-BordureGauche-BordureDroite));
   ray:= trunc(rayonreel/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse));
          if rayonreel=0 then exit;
    t1:=donneanglepolairedans02pi(cos(theta1),sin(theta1));
     t2:=donneanglepolairedans02pi(cos(theta2),sin(theta2));
 if t1>t2 then t1:=t1-2*Pi;
old_pencolor:=imprimante.Canvas.Pen.Color;
old_penmode:=imprimante.Canvas.Pen.Mode;
 old_penstyle:=imprimante.Canvas.Pen.Style;
 old_penwidth:=imprimante.Canvas.Pen.Width;
 old_brushcolor:=imprimante.Canvas.Brush.Color;
 old_brushstyle:=imprimante.Canvas.Brush.Style;

   imprimante.Canvas.pen.style:=pssolid;
    imprimante.Canvas.pen.width:=epaisseur;
     imprimante.Canvas.pen.Style:=_style;

    if ((_penmod=pmnot) or (_penmod=pmxor)or (_penmod=pmnotxor)) then begin
 imprimante.Canvas.pen.mode:=pmxor;
 imprimante.Canvas.pen.color:=colortorgb(couleur) xor colortorgb(couleurfondsim);
end else begin
  imprimante.Canvas.pen.color:=couleur;
  imprimante.Canvas.pen.mode:=_penmod;
 end;
     imprimante.canvas.brush.style:=bsclear;


     imprimante.canvas.arc(x1-rax,y1-ray,x1+rax,y1+ray,trunc(t1/Pi*180*16),trunc((t2-t1)/Pi*180*16));
     arcdecercle:=true;

imprimante.Canvas.Pen.Color:=old_pencolor;
imprimante.Canvas.Pen.Mode:=old_penmode;
 imprimante.Canvas.Pen.Style:=old_penstyle;
 imprimante.Canvas.Pen.Width:=old_penwidth;
 imprimante.Canvas.Brush.Color:=old_brushcolor;
imprimante.Canvas.Brush.Style:=old_brushstyle;
     end;




function TMyimprimante.Point(x,y:extended; rayon:integer; couleur:tcolor
 ; _penmod:tpenmode;gauche:boolean):
 boolean;
            var x1,y1:integer;
            ymax,ymin,pasgrilley:extended;
 old_pencolor:tcolor;
old_penmode:tpenmode;
 old_penstyle:tpenstyle;
 old_penwidth:integer;
 old_brushcolor:tcolor;
 old_brushstyle:tbrushstyle;
  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;
         invconvert(x1,y1,x,y,true);



 if ((x<xmin) or (x>xmax)
 or (y>ymax) or (y<ymin))
  then begin
 point:=false;
 exit;
 end;
if rayon=0 then imprimante.canvas.pixels[x1,y1]:=couleur;

     if rayon=0 then exit;

 old_pencolor:=imprimante.Canvas.Pen.Color;
old_penmode:=imprimante.Canvas.Pen.Mode;
 old_penstyle:=imprimante.Canvas.Pen.Style;
 old_penwidth:=imprimante.Canvas.Pen.Width;
 old_brushcolor:=imprimante.Canvas.Brush.Color;
 old_brushstyle:=imprimante.Canvas.Brush.Style;

 imprimante.canvas.pen.color:=couleur;
    imprimante.canvas.pen.style:=pssolid;
    imprimante.canvas.pen.width:=1;
    imprimante.canvas.pen.mode:=pmcopy;
     imprimante.canvas.brush.style:=bssolid;
     imprimante.canvas.brush.Color:=couleur;
     imprimante.canvas.ellipse(x1-rayon,y1-rayon,x1+rayon,y1+rayon);
imprimante.Canvas.Pen.Color:=old_pencolor;
imprimante.Canvas.Pen.Mode:=old_penmode;
 imprimante.Canvas.Pen.Style:=old_penstyle;
 imprimante.Canvas.Pen.Width:=old_penwidth;
 imprimante.Canvas.Brush.Color:=old_brushcolor;
imprimante.Canvas.Brush.Style:=old_brushstyle;
     end;


function TMyimprimante.cercle(x,y:extended; rayon,epaisseur:integer; couleur:tcolor
; _penmod:tpenmode;gauche:boolean):
 boolean;
            var xx1,yy1:integer;
           ymax,ymin,pasgrilley:extended;
old_pencolor:tcolor;
old_penmode:tpenmode;
 old_penstyle:tpenstyle;
 old_penwidth:integer;
 old_brushcolor:tcolor;
 old_brushstyle:tbrushstyle;



  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;
         invconvert(xx1,yy1,x,y,true);



 if ((x<=xmin) or (x>=xmax)
 or (y<ymin) or (y>ymax) or (rayon=0))

  then begin
 cercle:=false;
 exit;
 end;
old_pencolor:=imprimante.Canvas.Pen.Color;
old_penmode:=imprimante.Canvas.Pen.Mode;
 old_penstyle:=imprimante.Canvas.Pen.Style;
 old_penwidth:=imprimante.Canvas.Pen.Width;
 old_brushcolor:=imprimante.Canvas.Brush.Color;
 old_brushstyle:=imprimante.Canvas.Brush.Style;
    imprimante.canvas.pen.color:=couleur;
     imprimante.canvas.pen.style:=pssolid;
     imprimante.canvas.pen.width:=epaisseur;
 if ((_penmod=pmnot) or (_penmod=pmxor)or (_penmod=pmnotxor)) then begin
 imprimante.Canvas.pen.mode:=pmxor;
 imprimante.Canvas.pen.color:=colortorgb(couleur) xor colortorgb(couleurfondsim);
end else begin
 imprimante.Canvas.pen.color:=couleur;
 imprimante.Canvas.pen.mode:=_penmod;
 end;
     cercle:=true;
     imprimante.canvas.brush.style:=bsclear;
   
imprimante.canvas.arc(xx1-rayon,yy1-rayon,xx1+rayon,yy1+rayon,0,
360*16);
  imprimante.Canvas.Pen.Color:=old_pencolor;
imprimante.Canvas.Pen.Mode:=old_penmode;
 imprimante.Canvas.Pen.Style:=old_penstyle;
 imprimante.Canvas.Pen.Width:=old_penwidth;
 imprimante.Canvas.Brush.Color:=old_brushcolor;
imprimante.Canvas.Brush.Style:=old_brushstyle;
     end;

     function TMyimprimante.disque(x,y:extended; rayon:extended; couleurbord,couleurf:tcolor;_transparent:boolean;gauche:boolean):
 boolean;
            var x1,y1,r1x,r1y:integer;
            ymax,ymin,pasgrilley:extended;
 old_pencolor:tcolor;
old_penmode:tpenmode;
 old_penstyle:tpenstyle;
 old_penwidth:integer;
 old_brushcolor:tcolor;
 old_brushstyle:tbrushstyle;


  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;
            x1:= trunc((x-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite));
 y1:=trunc((ymax-y)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse));
 if ((x1<=-borduregauche) or (x1>=largeur-borduregauche)
 or (y1>hauteur-bordurehaute) or (y1<-bordurehaute))
  then begin
 disque:=false;
 exit;
 end;
     x1:= trunc((x-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite))+
 BordureGauche;
 y1:=trunc((ymax-y)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse))+
 BordureHaute;
   r1x:= trunc(rayon/(xmax-xmin)*(largeur-BordureGauche-BordureDroite));
 r1y:=trunc((rayon)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse));
      if rayon=0 then exit;
  old_pencolor:=imprimante.Canvas.Pen.Color;
old_penmode:=imprimante.Canvas.Pen.Mode;
 old_penstyle:=imprimante.Canvas.Pen.Style;
 old_penwidth:=imprimante.Canvas.Pen.Width;
 old_brushcolor:=imprimante.Canvas.Brush.Color;
 old_brushstyle:=imprimante.Canvas.Brush.Style;

    imprimante.canvas.pen.color:=couleurbord;
     imprimante.canvas.pen.style:=pssolid;
     imprimante.canvas.pen.width:=1;
     imprimante.canvas.pen.mode:=pmcopy;
     disque:=true;

     imprimante.canvas.brush.color:=couleurf;
   if not(_transparent) then  imprimante.canvas.brush.style:=bssolid else
   imprimante.canvas.brush.style:=bsclear;


imprimante.canvas.ellipse(x1-r1x,y1-r1y,x1+r1x,
y1+r1y);
imprimante.Canvas.Pen.Color:=old_pencolor;
imprimante.Canvas.Pen.Mode:=old_penmode;
 imprimante.Canvas.Pen.Style:=old_penstyle;
 imprimante.Canvas.Pen.Width:=old_penwidth;
 imprimante.Canvas.Brush.Color:=old_brushcolor;
imprimante.Canvas.Brush.Style:=old_brushstyle;

     end;


function TMyimprimante.Trait(x1,y1,x2,y2:extended; epaisseur:integer;
 couleur:tcolor;sty:tpenstyle; _penmod:tpenmode;gauche:boolean):boolean;
 var ymax,ymin,pasgrilley:extended;
xi1,yi1,xi2,yi2:integer;
old_pencolor:tcolor;
old_penmode:tpenmode;
 old_penstyle:tpenstyle;
 old_penwidth:integer;
 old_brushcolor:tcolor;
 old_brushstyle:tbrushstyle;

  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;
 if xmax=xmin then exit;
       if ymax=ymin then exit;

       if ((x1>xmax) or (x1<xmin) or (y1>ymax) or (y1<ymin) or
        (x2>xmax) or (x2<xmin) or (y2>ymax) or (y2<ymin))
       then begin
       trait:=false;
       exit;
       end;

 invconvert(xi1,yi1,x1,y1,true);
       invconvert(xi2,yi2,x2,y2,true);


old_pencolor:=imprimante.Canvas.Pen.Color;
old_penmode:=imprimante.Canvas.Pen.Mode;
 old_penstyle:=imprimante.Canvas.Pen.Style;
 old_penwidth:=imprimante.Canvas.Pen.Width;
 old_brushcolor:=imprimante.Canvas.Brush.Color;
 old_brushstyle:=imprimante.Canvas.Brush.Style;
 imprimante.canvas.pen.width:=epaisseur;
 imprimante.canvas.pen.Style:=sty;
 if ((_penmod=pmnot) or (_penmod=pmxor) or (_penmod=pmnotxor)) then begin
 imprimante.Canvas.pen.mode:=pmxor;
 imprimante.Canvas.pen.color:=colortorgb(couleur) xor colortorgb(couleurfondsim);
end else begin
 imprimante.Canvas.pen.color:=couleur;
  imprimante.Canvas.pen.mode:=_penmod;
 end;
imprimante.Canvas.Line(xi1,yi1,xi2,yi2);
imprimante.Canvas.Pen.Color:=old_pencolor;
imprimante.Canvas.Pen.Mode:=old_penmode;
 imprimante.Canvas.Pen.Style:=old_penstyle;
 imprimante.Canvas.Pen.Width:=old_penwidth;
 imprimante.Canvas.Brush.Color:=old_brushcolor;
imprimante.Canvas.Brush.Style:=old_brushstyle;

 end;



function TMyimprimante.CroixP(x,y:extended;
 demi_longueur,epaisseur:integer; couleur:tcolor; penmod:tpenmode;gauche:boolean):boolean;
 var x1,y1:integer;
 ymax,ymin,pasgrilley:extended;
old_pencolor:tcolor;
old_penmode:tpenmode;
 old_penstyle:tpenstyle;
 old_penwidth:integer;
 old_brushcolor:tcolor;
 old_brushstyle:tbrushstyle;



  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;
 x1:= trunc((x-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite));
 y1:=trunc((ymax-y)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse));
 if ((x1<=-borduregauche) or (x1>=largeur-borduregauche)
 or (y1>hauteur-bordurehaute) or (y1<-bordurehaute))
  then begin
 croixp:=false;
 exit;
 end;

 x1:= trunc((x-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite))+
 BordureGauche;
 y1:=trunc((ymax-y)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse))+
 BordureHaute;

old_pencolor:=imprimante.Canvas.Pen.Color;
old_penmode:=imprimante.Canvas.Pen.Mode;
 old_penstyle:=imprimante.Canvas.Pen.Style;
 old_penwidth:=imprimante.Canvas.Pen.Width;
 old_brushcolor:=imprimante.Canvas.Brush.Color;
 old_brushstyle:=imprimante.Canvas.Brush.Style;


imprimante.canvas.pen.width:=epaisseur;
  imprimante.canvas.pen.mode:=penmod;
  imprimante.canvas.pen.style:=pssolid;
  imprimante.canvas.pen.color:=couleur;
   imprimante.canvas.moveto(x1,y1);
   imprimante.canvas.lineto(x1,y1+demi_longueur);
       imprimante.canvas.moveto(x1,y1);
   imprimante.canvas.lineto(x1,y1-demi_longueur);
        imprimante.canvas.moveto(x1,y1);
   imprimante.canvas.lineto(x1+demi_longueur,y1);
   imprimante.canvas.moveto(x1,y1);
   imprimante.canvas.lineto(x1-demi_longueur,y1);
   croixp:=true;

imprimante.Canvas.Pen.Color:=old_pencolor;
imprimante.Canvas.Pen.Mode:=old_penmode;
 imprimante.Canvas.Pen.Style:=old_penstyle;
 imprimante.Canvas.Pen.Width:=old_penwidth;
 imprimante.Canvas.Brush.Color:=old_brushcolor;
imprimante.Canvas.Brush.Style:=old_brushstyle;
   end;


   procedure TMyimprimante.arronditalagrille(var x1,y1:extended;gauche:boolean);
  var newx,newy,divx,divy,fracx,fracy:extended;
  ymax,ymin,pasgrilley:extended;
  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;

  divx:=partieentiere((x1-xmin)/pasgrillex);
  divy:=partieentiere((y1-ymin)/pasgrilley);
  fracx:=x1-xmin-divx*pasgrillex;
  fracy:=y1-ymin-divy*pasgrilley;
  if fracx>0.5*pasgrillex then newx:=(divx+1)*pasgrillex else newx:=divx*pasgrillex;
  if fracy>0.5*pasgrilley then newy:=(divy+1)*pasgrilley else newy:=divy*pasgrilley;
  x1:=newx; y1:=newy;
                        end;


  procedure  TMyimprimante.ecrireI(x,y:integer; s:string; police:tfont);
   var xi,yi:integer;
 old_font:tfont;
   old_pencolor:tcolor;
old_penmode:tpenmode;
 old_penstyle:tpenstyle;
 old_penwidth:integer;
 old_brushcolor:tcolor;
 old_brushstyle:tbrushstyle;
   begin
    old_pencolor:=imprimante.Canvas.Pen.Color;
old_penmode:=imprimante.Canvas.Pen.Mode;
 old_penstyle:=imprimante.Canvas.Pen.Style;
 old_penwidth:=imprimante.Canvas.Pen.Width;
 old_brushcolor:=imprimante.Canvas.Brush.Color;
 old_brushstyle:=imprimante.Canvas.Brush.Style;
    imprimante.Canvas.Brush.Color:=couleurfondsimulation;
      imprimante.Canvas.Pen.Color:=couleurfondsimulation;
       imprimante.Canvas.Brush.Style:=bsclear;
old_font:=imprimante.Canvas.Font;
    imprimante.canvas.Font:=police;
    imprimante.canvas.textout(x,y,s);
imprimante.Canvas.Font:=old_font;
    imprimante.Canvas.Pen.Color:=old_pencolor;
imprimante.Canvas.Pen.Mode:=old_penmode;
 imprimante.Canvas.Pen.Style:=old_penstyle;
 imprimante.Canvas.Pen.Width:=old_penwidth;
imprimante.Canvas.Brush.Color:=old_brushcolor;
 imprimante.Canvas.Brush.Style:=old_brushstyle;
      end;

  procedure  TMyimprimante.ecrire(x,y:extended; s:string;gauche:boolean; police:tfont);
   var xi,yi:integer;
 old_font:tfont;
   old_pencolor:tcolor;
old_penmode:tpenmode;
 old_penstyle:tpenstyle;
 old_penwidth:integer;
 old_brushcolor:tcolor;
 old_brushstyle:tbrushstyle;
   begin
    old_pencolor:=imprimante.Canvas.Pen.Color;
old_penmode:=imprimante.Canvas.Pen.Mode;
 old_penstyle:=imprimante.Canvas.Pen.Style;
 old_penwidth:=imprimante.Canvas.Pen.Width;
 old_brushcolor:=imprimante.Canvas.Brush.Color;
 old_brushstyle:=imprimante.Canvas.Brush.Style;
    imprimante.Canvas.Brush.Color:=couleurfondsimulation;
      imprimante.Canvas.Pen.Color:=couleurfondsimulation;
       imprimante.Canvas.Brush.Style:=bsclear;
    invconvert(xi,yi,x,y,gauche);
old_font:=imprimante.Canvas.Font;
    imprimante.canvas.Font:=police;
    imprimante.canvas.textout(xi,yi,s);
imprimante.Canvas.Font:=old_font;
    imprimante.Canvas.Pen.Color:=old_pencolor;
imprimante.Canvas.Pen.Mode:=old_penmode;
 imprimante.Canvas.Pen.Style:=old_penstyle;
 imprimante.Canvas.Pen.Width:=old_penwidth;
imprimante.Canvas.Brush.Color:=old_brushcolor;
 imprimante.Canvas.Brush.Style:=old_brushstyle;
      end;

procedure TMyimprimante.background(couleur:tcolor);
var old_pencolor:tcolor;
old_penmode:tpenmode;
 old_penstyle:tpenstyle;
 old_penwidth:integer;
 old_brushcolor:tcolor;
 old_brushstyle:tbrushstyle;
 LPicture:tpicture;   scale:longint;
begin
old_pencolor:=imprimante.Canvas.Pen.Color;
old_penmode:=imprimante.Canvas.Pen.Mode;
 old_penstyle:=imprimante.Canvas.Pen.Style;
 old_penwidth:=imprimante.Canvas.Pen.Width;
 old_brushcolor:=imprimante.Canvas.Brush.Color;
 old_brushstyle:=imprimante.Canvas.Brush.Style;
 
imprimante.canvas.Brush.Color:=couleur;
imprimante.canvas.Brush.Style:=bssolid;
imprimante.Canvas.Pen.Color:=couleur;
 imprimante.Canvas.Pen.Mode:=pmcopy;


if not(fond_simulation_est_image) then
imprimante.Canvas.Rectangle(0,0,largeur,hauteur)   else
begin
LPicture := TPicture.Create;
    try
       LPicture.LoadFromFile(nom_fichier_image_fond_simulation);
      Scale := Min(
        imprimante.PageWidth div LPicture.Bitmap.Width,
        imprimante.PageHeight div LPicture.Bitmap.Height);
      imprimante.Canvas.StretchDraw(
        Rect(0, 0, LPicture.Bitmap.Width*Scale, LPicture.Bitmap.Height*Scale),
        LPicture.Bitmap);

    finally
       FreeAndNil(LPicture);
           end;
end;


imprimante.Canvas.Pen.Color:=old_pencolor;
imprimante.Canvas.Pen.Mode:=old_penmode;
 imprimante.Canvas.Pen.Style:=old_penstyle;
 imprimante.Canvas.Pen.Width:=old_penwidth;
 imprimante.Canvas.Brush.Color:=old_brushcolor;
imprimante.Canvas.Brush.Style:=old_brushstyle;


end;


 function TMyimprimante.LimitesEtAxes(_xmin,_xmax,_ymin1,_ymax1,_ymin2,_ymax2:extended;
 _Largeur,_Hauteur:integer;
 _titre:string;
  _fontetitre:tfont;
  _Fond:boolean;
 _CouleurFondsim:tcolor;
 _Cadre:boolean; _EpaisseurCadre:integer;
 _CouleurCadre:tcolor;
 _Gradue:boolean;
 _EpaisseurGraduation,_LongueurGraduation:integer;
 _couleurgraduation:tcolor;
 _fontegraduation:tfont;
 _Grille1,_Grille2:boolean; _EpaisseurGrille:integer;
 _CouleurGrille1,_CouleurGrille2:tcolor;
 _labelx,_labely1,_labely2,_unitex,_unitey1,_unitey2:string;
  _borduresverticalessymetriques,
 _echelle_g,_echelle_d:boolean;
  b_g,b_d,b_b,b_h:integer):boolean;


  var xi,xa,yi1,yi2,ya1,ya2,gi,ga:extended;
     t1,t2,i:integer;
     dixx,dixy1,dixy2,tt,aff:string;
     nbgx,nbgy1,nbgy2:integer;
 old_pencolor:tcolor;
old_penmode:tpenmode;
 old_penstyle:tpenstyle;
 old_penwidth:integer;
 old_brushcolor:tcolor;
 old_brushstyle:tbrushstyle;

        old_font:tfont;
 begin
old_pencolor:=imprimante.Canvas.Pen.Color;
old_penmode:=imprimante.Canvas.Pen.Mode;
 old_penstyle:=imprimante.Canvas.Pen.Style;
 old_penwidth:=imprimante.Canvas.Pen.Width;
 old_brushcolor:=imprimante.Canvas.Brush.Color;
 old_brushstyle:=imprimante.Canvas.Brush.Style;
 old_font:=imprimante.Canvas.Font;
 
 fond:=_fond;        echelle_g:=_echelle_g; echelle_d:=_echelle_d;
  borduresverticalessymetriques:=_borduresverticalessymetriques;
 labelx:=_labelx;
 labely1:=_labely1;   labely2:=_labely2;
 unitex:=_unitex; unitey1:=_unitey1; unitey2:=_unitey2;
   fontetitre:=_fontetitre;
  largeur:=_largeur;
  hauteur:=_hauteur;
    titre:=_titre;
  couleurfondsim:=_couleurfondsim;
  cadre:=_cadre; epaisseurcadre:=_epaisseurcadre;
  couleurcadre:=_couleurcadre;
  gradue:=_gradue; epaisseurgraduation:=_epaisseurgraduation;

  fontegraduation:=_fontegraduation;
  couleurgraduation:=_couleurgraduation;
  grille1:=_grille1;   grille2:=_grille2;
  epaisseurgrille:=_epaisseurgrille;
  couleurgrille1:=_couleurgrille1;
  couleurgrille2:=_couleurgrille2;

 if (echelle_g and ( (_ymin1=_ymax1) or (_xmax=_xmin))) then begin
 LimitesEtAxes:=false;
 exit;
 end;
   if (echelle_d and ( (_ymin2=_ymax2) or (_xmax=_xmin))) then begin
 LimitesEtAxes:=false;
 exit;
 end;

 LimitesEtAxes:=true;
 if (_xmin>_xmax ) then begin
 xmin:=_xmax;
 xmax:=_xmin;
 end else begin
 xmin:=_xmin;
 xmax:=_xmax;
 end;

 if (_ymin1>_ymax1 ) then begin
 ymin1:=_ymax1;
 ymax1:=_ymin1;
 end else begin
 ymin1:=_ymin1;
 ymax1:=_ymax1;
 end;

 if (_ymin2>_ymax2 ) then begin
 ymin2:=_ymax2;
 ymax2:=_ymin2;
 end else begin
 ymin2:=_ymin2;
 ymax2:=_ymax2;
 end;



   xi:=xmin; yi1:=ymin1; yi2:=ymin2; xa:=xmax; ya1:=ymax1;  ya2:=ymax2;

 
 imprimante.canvas.font:=fontetitre;
 if (titre<>'') then begin
 t1:=trunc(imprimante.canvas.textheight(titre))+10;
 end else t1:=10;

 if gradue then begin
 imprimante.canvas.font:=fontegraduation;
 if ((dixy1='0') and (dixy2='0')) then t2:=trunc(imprimante.canvas.textheight('10'+unitex+labelx)*1.5)
 else t2:=trunc(imprimante.canvas.textheight('10'+unitex+labelx)*2.2);
 end else t2:=10;

 if (t1>t2)  then bordurehaute:=t1 else bordurehaute:=t2;




 {taille bordure basse}
 if gradue then begin
 imprimante.canvas.font:=fontegraduation;
 t1:=trunc(imprimante.canvas.textheight('x10'))+10;
 end else t1:=10;
 bordurebasse:=t1;


 {taille bordure droite}

 if (gradue and echelle_d) then begin
 imprimante.canvas.font:=fontegraduation;
 str( trunc((ymin2)/dix_pp(puissancededixy2-1)),tt);
 t1:=trunc(imprimante.canvas.textwidth(tt))+10;
  str( trunc((ymin2+nbgy2*graduationy2)/dix_pp(puissancededixy2-1)),tt);
 t2:=trunc(imprimante.canvas.textwidth(tt))+10;
 end else begin
 t1:=10;
 t2:=10;
 end;

 if t1<t2 then t1:=t2;
 if gradue then begin
 imprimante.canvas.font:=fontegraduation;
 str( trunc((xmin+nbgx*graduationx)/dix_pp(puissancededixx-1)),tt);
 t2:=trunc(imprimante.canvas.textwidth(tt)*1.2)div 2;
 end else t2:=10;
 if t1>t2 then borduredroite:=t1 else borduredroite:=t2;

 {taille bordure gauche}

 if (gradue and echelle_g) then begin
 imprimante.canvas.font:=fontegraduation;
 str( trunc((ymin1)/dix_pp(puissancededixy1-1)),tt);
 t1:=trunc(imprimante.canvas.textwidth(tt))+10;
  str( trunc((ymin1+nbgy1*graduationy1)/dix_pp(puissancededixy1-1)),tt);
 t2:=trunc(imprimante.canvas.textwidth(tt))+10;
 end else begin
 t1:=10;
 t2:=10;
 end;

 if t1<t2 then t1:=t2;
 if gradue then begin
 imprimante.canvas.font:=fontegraduation;
 str( trunc((xmin)/dix_pp(puissancededixx-1)),tt);
 t2:=trunc(imprimante.canvas.textwidth(tt)*1.2)div 2;
 end else t2:=10;

 if t1>t2 then borduregauche:=t1 else borduregauche:=t2;


 if borduresverticalessymetriques then begin
  borduregauche:=max(borduregauche,borduredroite);
  borduredroite:=borduregauche;
  end;

  borduregauche:=max(borduregauche,b_g);
  borduredroite:=max(borduredroite,b_d);
  bordurebasse:=max(bordurebasse,b_b);
  bordurehaute:=max(bordurehaute,b_h);

   if fond then background(couleurfondsim);


 {longueurgraduation}
  longueurgraduationX:=_longueurgraduation/largeur*(xmax-xmin);
  if echelle_g then
   longueurgraduationy1:=_longueurgraduation/hauteur*(ymax1-ymin1);
  if echelle_d then
   longueurgraduationy2:=_longueurgraduation/hauteur*(ymax2-ymin2);

 if grille1 then begin
    imprimante.canvas.pen.style:=psdot;
    imprimante.canvas.pen.color:=couleurgrille1;
    imprimante.canvas.pen.width:=epaisseurgrille;
       for i:=0 to nbgx do
          trait(xmin+i*graduationx,ymin1,xmin+i*graduationx,ymax1,
          epaisseurgrille,couleurgrille1,psdot,pmcopy,true);
         for i:=0 to nbgy1 do
          trait(xmin,ymin1+i*graduationy1,xmax,ymin1+i*graduationy1,
          epaisseurgrille,couleurgrille1,psdot,pmcopy,true);
            end;
   if grille2 then begin
    imprimante.canvas.pen.style:=psdot;
    imprimante.canvas.pen.color:=couleurgrille2;
    imprimante.canvas.pen.width:=epaisseurgrille;
       for i:=0 to nbgx do
          trait(xmin+i*graduationx,ymin2,xmin+i*graduationx,ymax2,
          epaisseurgrille,couleurgrille2,psdot,pmcopy,false);
         for i:=0 to nbgy2 do
          trait(xmin,ymin2+i*graduationy2,xmax,ymin2+i*graduationy2,
          epaisseurgrille,couleurgrille2,psdot,pmcopy,false);
            end;
 {cadre}
 if cadre then begin
    imprimante.canvas.pen.width:=epaisseurcadre;
  imprimante.canvas.pen.mode:=pmcopy;
  imprimante.canvas.pen.style:=pssolid;
  imprimante.canvas.pen.color:=couleurcadre;

    imprimante.canvas.moveto(BordureGauche,BordureHaute);
    imprimante.canvas.lineto(largeur-BordureDroite,BordureHaute);
    imprimante.canvas.lineto(largeur-BordureDroite,hauteur-BordureBasse);
    imprimante.canvas.lineto(BordureGauche,hauteur-BordureBasse);
    imprimante.canvas.lineto(BordureGauche,BordureHaute);
    end;

    {affichage du titre}
 imprimante.canvas.font:=fontetitre;
 while   ((imprimante.canvas.textwidth(titre)>largeur-borduredroite-borduregauche) and
 (imprimante.canvas.Font.Size>1)) do
 imprimante.canvas.Font.Size:= imprimante.canvas.Font.Size-1;
 //if  imprimante.canvas.Font.Size<6 then  imprimante.canvas.Font.Size:=6;
 imprimante.canvas.textout(borduregauche+((largeur-borduredroite-borduregauche) div 2) -(imprimante.canvas.textwidth(titre) div 2),
 bordurehaute -2-imprimante.canvas.textheight(titre),titre);
    {graduation}

    if gradue then begin

                 if dixx='0' then aff:='' else begin
                 aff:='10';
                 if dixx<>'1' then for i:=1 to length(dixx) do aff:=aff+' ';
                 aff:=aff+' ';
                 end;
                 aff:=aff+labelx;
                 if ((unitex<>'1') and (unitex<>'')) then aff:=aff+' en '+unitex;
  imprimante.canvas.font:=fontegraduation;
  imprimante.canvas.textout(largeur-borduredroite-trunc(imprimante.canvas.textwidth
  (aff)*1.3),hauteur-bordurebasse-
  trunc(imprimante.canvas.textheight('x10')*1.3),aff);
  if ((dixx<>'0') and (dixx<>'1')) then
  imprimante.canvas.textout(largeur-borduredroite-trunc(imprimante.canvas.textwidth(aff)*1.3)+
  trunc(imprimante.canvas.textwidth('10')),hauteur-bordurebasse
  -trunc(1.9*imprimante.canvas.textheight('x10')),
  dixx);

  for i:=0 to nbgx do begin
  str(round((xmin+i*graduationx)/dix_pp(puissancededixx-1)),tt);
  imprimante.canvas.textout(
  trunc(i*graduationx/(xmax-xmin)*(largeur-borduredroite-borduregauche)+
  borduregauche)-imprimante.canvas.textwidth(tt) div 2,
  hauteur-bordurebasse+2,tt);
              end;


      if echelle_g then begin
       if dixy1='0' then aff:='' else begin
                 aff:='10';
       if dixy1<>'1' then   for i:=1 to length(dixy1) do aff:=aff+' ';
                 aff:=aff+' ';
                 end;
                 aff:=aff+labely1;
                 if ((unitey1<>'1') and (unitey1<>'')) then aff:=aff+' en '+unitey1;
  imprimante.canvas.font:=fontegraduation;
  imprimante.canvas.textout(borduregauche,bordurehaute-trunc(imprimante.canvas.textheight(aff)*1.2),aff);
  if ((dixy1<>'0') and (dixy1<>'1')) then
  imprimante.canvas.textout(borduregauche+imprimante.canvas.textwidth('10'),bordurehaute
  -trunc(imprimante.canvas.textheight(aff)*1.8),
  dixy1); end;

  if echelle_d then begin
       if dixy2='0' then aff:='' else begin
                 aff:='10';
       if dixy2<>'1' then   for i:=1 to length(dixy2) do aff:=aff+' ';
                 aff:=aff+' ';
                 end;
                 aff:=aff+labely2;
                 if ((unitey2<>'1') and (unitey2<>'')) then aff:=aff+' en '+unitey2;
  imprimante.canvas.font:=fontegraduation;
   imprimante.canvas.textout(largeur-borduredroite-trunc(imprimante.canvas.textwidth(aff)*1.2),
  bordurehaute-trunc(imprimante.canvas.textheight(aff)*1.2),aff);
  if ((dixy2<>'0') and (dixy2<>'1')) then
  imprimante.canvas.textout(largeur-borduredroite-trunc(imprimante.canvas.textwidth(aff)*1.2)+imprimante.canvas.textwidth('10'),
  bordurehaute
  -trunc(imprimante.canvas.textheight(aff)*1.8),
  dixy2);  end;



  if echelle_g then
  for i:=0 to nbgy1 do begin
  str( round((ymin1+i*graduationy1)/dix_pp(puissancededixy1-1)),tt);
  if i=0 then
      imprimante.canvas.textout( borduregauche-trunc(imprimante.canvas.textwidth(tt)*1.1),
  trunc(-  bordurebasse+hauteur)-imprimante.canvas.textheight(tt) ,
  tt) else
      imprimante.canvas.textout( borduregauche-trunc(imprimante.canvas.textwidth(tt)*1.1),
  trunc(-i*graduationy1/(ymax1-ymin1)*(hauteur-bordurehaute-bordurebasse)-
  bordurebasse+hauteur)-imprimante.canvas.textheight(tt) div 2,
  tt);
              end;

              if echelle_d then
     for i:=0 to nbgy2 do begin
  str( round((ymin2+i*graduationy2)/dix_pp(puissancededixy2-1)),tt);
  if i=0 then

  imprimante.canvas.textout( largeur-borduredroite+5,
  trunc(-  bordurebasse+hauteur)-imprimante.canvas.textheight(tt) ,
  tt) else

  imprimante.canvas.textout( largeur-borduredroite+5,
  trunc(-i*graduationy2/(ymax2-ymin2)*(hauteur-bordurehaute-bordurebasse)-
  bordurebasse+hauteur)-imprimante.canvas.textheight(tt) div 2,
  tt);
              end;






 if echelle_g then begin
   for i:=0 to nbgx do
          trait(xmin+i*graduationx,ymin1,xmin+i*graduationx,
          ymin1+longueurgraduationy1,epaisseurgraduation,couleurgraduation,pssolid,pmcopy,true);

           for i:=0 to nbgx do
          trait(xmin+i*graduationx,ymax1,xmin+i*graduationx,
          ymax1-longueurgraduationy1,epaisseurgraduation,couleurgraduation,pssolid,pmcopy,true);


   for i:=0 to nbgy1 do
          trait(xmin,ymin1+i*graduationy1,xmin+longueurgraduationx,ymin1+i*graduationy1,
          epaisseurgraduation,couleurgraduation,pssolid,pmcopy,true);

     for i:=0 to nbgy1 do
          trait(xmax,ymin1+i*graduationy1,xmax-longueurgraduationx,ymin1+i*graduationy1,
          epaisseurgraduation,couleurgraduation,pssolid,pmcopy,true);
           end;

    if echelle_d then begin
   for i:=0 to nbgx do
          trait(xmin+i*graduationx,ymin2,xmin+i*graduationx,
          ymin2+longueurgraduationy2,epaisseurgraduation,couleurgraduation,pssolid,pmcopy,false);

           for i:=0 to nbgx do
          trait(xmin+i*graduationx,ymax2,xmin+i*graduationx,
          ymax2-longueurgraduationy2,epaisseurgraduation,couleurgraduation,pssolid,pmcopy,false);


   for i:=0 to nbgy2 do
          trait(xmin,ymin2+i*graduationy2,xmin+longueurgraduationx,ymin2+i*graduationy2,
          epaisseurgraduation,couleurgraduation,pssolid,pmcopy,false);

     for i:=0 to nbgy2 do
          trait(xmax,ymin2+i*graduationy2,xmax-longueurgraduationx,ymin2+i*graduationy2,
          epaisseurgraduation,couleurgraduation,pssolid,pmcopy,false);
           end;
end; 

imprimante.Canvas.Pen.Color:=old_pencolor;
imprimante.Canvas.Pen.Mode:=old_penmode;
 imprimante.Canvas.Pen.Style:=old_penstyle;
 imprimante.Canvas.Pen.Width:=old_penwidth;
 imprimante.Canvas.Brush.Color:=old_brushcolor;
imprimante.Canvas.Brush.Style:=old_brushstyle;
imprimante.Canvas.font:=old_font;
end;






    function TMyimprimante.Convert(xi,yi:integer; var x,y:extended;gauche:boolean):boolean;
   var  ymax,ymin,pasgrilley:extended;
  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;
     x:=(xi-BordureGauche)*(xmax-xmin)/ (largeur-BordureGauche-BordureDroite)
     +xmin;
     y:=-(yi-BordureHaute)*(ymax-ymin)/(hauteur-BordureHaute-BordureBasse)
     +ymax;
     convert:=true;
     end;


function TMyimprimante.invconvert(var xi,yi:integer; x,y:extended;gauche:boolean):boolean;
var ymax,ymin,pasgrilley:extended;
  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;

 xi:= trunc((x-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite))+
 BordureGauche;
 yi:=trunc((ymax-y)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse))+
 BordureHaute;
invconvert:=true;
end;

function TMyimprimante.CroixX(x,y:extended;
 demi_diagonale,epaisseur:integer; couleur:tcolor; penmod:tpenmode;gauche:boolean):boolean;
 var x1,y1:integer;
 ymax,ymin,pasgrilley:extended;
 old_pencolor:tcolor;
old_penmode:tpenmode;
 old_penstyle:tpenstyle;
 old_penwidth:integer;
 old_brushcolor:tcolor;
 old_brushstyle:tbrushstyle;


  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;
 x1:= trunc((x-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite));
 y1:=trunc((ymax-y)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse));
 if ((x1<=-borduregauche) or (x1>=largeur-borduregauche)
 or (y1>hauteur-bordurehaute) or (y1<-bordurehaute))
  then begin
 croixx:=false;
 exit;
 end;

 x1:= trunc((x-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite))+
 BordureGauche;
 y1:=trunc((ymax-y)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse))+
 BordureHaute;
old_pencolor:=imprimante.Canvas.Pen.Color;
old_penmode:=imprimante.Canvas.Pen.Mode;
 old_penstyle:=imprimante.Canvas.Pen.Style;
 old_penwidth:=imprimante.Canvas.Pen.Width;
 old_brushcolor:=imprimante.Canvas.Brush.Color;
 old_brushstyle:=imprimante.Canvas.Brush.Style;


 imprimante.canvas.pen.width:=epaisseur;
  imprimante.canvas.pen.mode:=penmod;
  imprimante.canvas.pen.style:=pssolid;
  imprimante.canvas.pen.color:=couleur;
   imprimante.canvas.moveto(x1,y1);
   imprimante.canvas.lineto(x1+demi_diagonale,y1+demi_diagonale);
       imprimante.canvas.moveto(x1,y1);
   imprimante.canvas.lineto(x1-demi_diagonale,y1+demi_diagonale);
        imprimante.canvas.moveto(x1,y1);
   imprimante.canvas.lineto(x1+demi_diagonale,y1-demi_diagonale);
   imprimante.canvas.moveto(x1,y1);
   imprimante.canvas.lineto(x1-demi_diagonale,y1-demi_diagonale);
   croixx:=true;

imprimante.Canvas.Pen.Color:=old_pencolor;
imprimante.Canvas.Pen.Mode:=old_penmode;
 imprimante.Canvas.Pen.Style:=old_penstyle;
 imprimante.Canvas.Pen.Width:=old_penwidth;
 imprimante.Canvas.Brush.Color:=old_brushcolor;
imprimante.Canvas.Brush.Style:=old_brushstyle;

   end;

end.

