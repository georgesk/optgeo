unit Unit_indices_milieu_ambiant;

{$mode objfpc}

interface

uses
  LCLIntf, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, Buttons,{$ifdef windows}shellapi,windows,{$endif }LResources,UnitScaleFont,
  LazHelpHTML,UTF8Process,UChaines,Unit222,LCLType, Spin;

type

  { Tsaisie_indices_milieu_ambiant }

  Tsaisie_indices_milieu_ambiant = class(TForm)
    BitBtn1: TBitBtn;
    Edit_indice_bleu_par_defaut: TEdit;
    Edit_indice_rouge_par_defaut: TEdit;
    Edit_indice_vert_par_defaut: TEdit;
    FloatSpinEditbleu: TFloatSpinEdit;
    FloatSpinEditrouge: TFloatSpinEdit;
    FloatSpinEditvert: TFloatSpinEdit;
    StaticText1: TStaticText;
    StaticText10: TStaticText;
    StaticText2: TStaticText;
    StaticText3: TLabel;
    StaticText4: TLabel;
    StaticText5: TLabel;
    StaticText6: TStaticText;
    StaticText7: TStaticText;
    StaticText8: TStaticText;
    StaticText9: TStaticText;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    encreation:boolean;
    { private declarations }
  public
    { public declarations }
  end; 

var
  saisie_indices_milieu_ambiant: Tsaisie_indices_milieu_ambiant;

implementation

{ Tsaisie_indices_milieu_ambiant }



procedure Tsaisie_indices_milieu_ambiant.BitBtn1Click(Sender: TObject);
begin
    try
    indice_vert_par_defaut:=abs(strtofloat(saisie_indices_milieu_ambiant.Edit_indice_vert_par_defaut.Text));
    except
    application.MessageBox(pchar(rsAttention), pchar(rsValeurIllGal), mb_ok);
    indice_vert_par_defaut:=1;
    end;
    try
    indice_bleu_par_defaut:=abs(strtofloat(saisie_indices_milieu_ambiant.Edit_indice_bleu_par_defaut.Text));
    except
    application.MessageBox(pchar(rsAttention), pchar(rsValeurIllGal2), mb_ok);
    indice_bleu_par_defaut:=1;
    end;
  try
    indice_rouge_par_defaut:=abs(strtofloat(saisie_indices_milieu_ambiant.Edit_indice_rouge_par_defaut.Text));
    except
    application.MessageBox(pchar(rsAttention), pchar(rsValeurIllGal3), mb_ok);
    indice_rouge_par_defaut:=1;
    end;

    longueur_donde_red:=FloatSpinEditrouge.value*1e-9;
     longueur_donde_blue:=FloatSpinEditbleu.value*1e-9;
     longueur_donde_green:=FloatSpinEditvert.value*1e-9;
end;

procedure Tsaisie_indices_milieu_ambiant.FormCreate(Sender: TObject);
begin
    encreation:=true;
    caption:=rsIndicesDuMil2;
 STATICTEXT3.CAPTION:=rsPourLeVert;
STATICTEXT4.CAPTION:=rsPourLeRouge;
STATICTEXT5.CAPTION:=rsPourLeBleu;
STATICTEXT1.CAPTION:=rsLongueurDOnd;
 STATICTEXT2.CAPTION:=rsLongueurDOnd2;
  STATICTEXT6.CAPTION:=rsLongueurDOnd3;
  STATICTEXT7.CAPTION:=rsLesValeursDe;

end;

procedure Tsaisie_indices_milieu_ambiant.FormShow(Sender: TObject);
begin
   //if encreation then begin scalefont(self); encreation:=false; end;
end;

initialization
  {$I unit_indices_milieu_ambiant.lrs}

end.

