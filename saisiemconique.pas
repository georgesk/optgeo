{  This file is part of OptGeo, optical simulation software
    Copyright (C) 2004  Jean-Marie Biansan
        Contact: jeanmarie.biansan@free.fr
    Web site: http://jeanmarie.biansan.free.fr/logiciel.html
 From version 1.25, OptGeo is distribued under the terms of the
 GNU General Public License.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA}


    unit saisiemconique;

{$mode objfpc}{$H+}

interface

uses
  LCLIntf, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, Buttons, Spin,ColorBox, LResources,LCLType,UnitScaleFont,
  UChaines;

type

  { Tsaisiemiroirconique }

  Tsaisiemiroirconique = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    gridaxe: TColorBox;
    Colorgrid1: TColorBox;
    Image1: TImage;
    GroupBox1: TGroupBox;
    StaticText1: TLabel;
    StaticText2: TLabel;
    editfx: TEdit;
    editfy: TEdit;
    GroupBox3: TGroupBox;
    GroupBox6: TGroupBox;
    StaticText5: TLabel;
    GroupBox7: TGroupBox;
    editp: TEdit;
    boutonsup: TBitBtn;
    GroupBox2: TGroupBox;
    StaticText3: TLabel;
    edite: TEdit;
    GroupBox4: TGroupBox;
    StaticText4: TLabel;
    editt0: TEdit;
    GroupBox5: TGroupBox;
    StaticText6: TLabel;
    StaticText7: TLabel;
    editt1: TEdit;
    editt2: TEdit;
    GroupBox8: TGroupBox;
    checkhachures: TCheckBox;
    checkaxefocal: TCheckBox;
    GroupBox9: TGroupBox;
    checkconcave: TCheckBox;
    StaticText8: TLabel;
    StaticText9: TLabel;
    StaticText10: TLabel;
    log1: TLabel;
    log2: TLabel;
    log3: TLabel;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private  encreation:boolean;
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

var
  saisiemiroirconique: Tsaisiemiroirconique;
   mconique_hachures,mconique_axe,mconique_concave:boolean;
  mconique_epaisseur:integer;
  mconique_couleur,mconique_couleuraxe:tcolor;
  mconique_fx,mconique_fy,mconique_t0,mconique_t1,mconique_t2,mconique_e,
  mconique_p:extended;
implementation

uses unit222;


procedure Tsaisiemiroirconique.BitBtn1Click(Sender: TObject);
begin
mconique_concave:=checkconcave.checked;
mconique_hachures:=checkhachures.Checked;
mconique_axe:=checkaxefocal.Checked;
mconique_couleur:=colorgrid1.selected;
mconique_couleuraxe:=gridaxe.selected;
try
mconique_fx:=strtofloat(editfx.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce25),
 pchar(rsAttention), mb_ok);
 exit;
 end;  end;
 if ((mconique_fx<xxmin) or (mconique_fx>xxmax)) then begin
 application.messagebox(pchar(rsValeurInacce25),
 pchar(rsAttention),mb_ok);
 exit;
 end;

 try
mconique_fy:=strtofloat(editfy.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce26),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;
 if ((mconique_fy<yymin) or (mconique_fy>yymax)) then begin
 application.messagebox(pchar(rsValeurInacce26),
 pchar(rsattention),mb_ok);
 exit;
 end;

 try
mconique_e:=strtofloat(edite.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce27),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;
 mconique_e:=abs(mconique_e);

 try
mconique_p:=strtofloat(editp.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce28),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;
  mconique_p:=abs(mconique_p);


 try
mconique_t0:=strtofloat(editt0.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce29),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;

 try
mconique_t1:=strtofloat(editt1.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce30),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;

 try
mconique_t2:=strtofloat(editt2.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce31),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;


  ReTaillePmiroirconique(Listemiroirconique,nombremiroirconique,nombremiroirconique+1);
 inc(nombremiroirconique);
if not
(Listemiroirconique[-1+nombremiroirconique].create2(mconique_fx,mconique_fy,
mconique_t0,mconique_t1,
mconique_t2,mconique_e,mconique_p,mconique_concave,
mconique_couleur,mconique_couleuraxe,mconique_hachures,mconique_axe)) then begin
ReTaillePmiroirconique(Listemiroirconique,nombremiroirconique,nombremiroirconique-1);
dec(nombremiroirconique);
application.messagebox(pchar(rsDonnEsAberra2),
pchar(rsattention),mb_ok);
end else
self.modalresult:=mrok;
Rafraichit;


end;

procedure Tsaisiemiroirconique.FormCreate(Sender: TObject);
begin
   encreation:=true;
   BITBTN1.CAPTION
:=rsOK;


BITBTN2.CAPTION
:=rsAnnuler;


BOUTONSUP.CAPTION
:=rsSupprimerCeM;


CAPTION
:=rsAjoutDUnMiro7;


CHECKAXEFOCAL.CAPTION
:=rsAxeFocal;


CHECKCONCAVE.CAPTION
:=rsConcave;


CHECKHACHURES.CAPTION
:=rsHachures;


GROUPBOX1.CAPTION
:=rsFoyerF;


GROUPBOX2.CAPTION
:=rsExcentricit;


GROUPBOX3.CAPTION
:=rsCouleurDuMir;


GROUPBOX4.CAPTION
:=rsAnglePolaire2;


GROUPBOX5.CAPTION
:=rsAnglesPolair;


GROUPBOX6.CAPTION
:=rsParamTre;


GROUPBOX7.CAPTION
:=rsCouleurDeLAx3;


GROUPBOX8.CAPTION
:=rsAspectLCran;


GROUPBOX9.CAPTION
:=rsConcaveOuCon;


LOG1.CAPTION
:=rsLog1;


LOG2.CAPTION
:=rsLog2;


LOG3.CAPTION
:=rsLog3;


STATICTEXT1.CAPTION
:=rsX;


STATICTEXT10.CAPTION
:=rsRad;


STATICTEXT2.CAPTION
:=rsY;


STATICTEXT3.CAPTION
:=rsE;


STATICTEXT4.CAPTION
:=rsT0;


STATICTEXT5.CAPTION
:=rsP;


STATICTEXT6.CAPTION
:=rsT1;


STATICTEXT7.CAPTION
:=rsT2;


STATICTEXT8.CAPTION
:=rsRad;


STATICTEXT9.CAPTION
:=rsRad;


end;

procedure Tsaisiemiroirconique.FormShow(Sender: TObject);
begin
 // if encreation then begin scalefont(self); encreation:=false; end;
end;

initialization
  {$i saisiemconique.lrs}


end.
