 {  This file is part of OptGeo, optical simulation software
    Copyright (C) 2004  Jean-Marie Biansan
        Contact: jeanmarie.biansan@free.fr
    Web site: http://jeanmarie.biansan.free.fr/logiciel.html
 From version 1.25, OptGeo is distribued under the terms of the
 GNU General Public License.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA}


    unit saisiereseau;

{$mode objfpc}{$H+}

interface

uses
  LCLIntf, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Spin, Colorbox, ExtCtrls, Buttons,unit222, LResources,LCLType,UnitScaleFont,
  UChaines;

type

  { TsaisieReseau }

  { Tsaisiereseau }

  Tsaisiereseau = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Colorgrid1: TColorBox;
    FloatSpinEdit1: TFloatSpinEdit;
    GroupBox6: TGroupBox;
    GroupBox7: TGroupBox;
    Image1: TImage;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    cochehachures: TCheckBox;
    GroupBox3: TGroupBox;
    editepaisseur: TSpinEdit;
    GroupBox4: TGroupBox;
    RadioGroup1: TRadioGroup;
    SpinEdit_ordre_max: TSpinEdit;
    SpinEdit_ordre_min: TSpinEdit;
    StaticText1: TLabel;
    editx1: TEdit;
    StaticText2: TLabel;
    edity1: TEdit;
    GroupBox5: TGroupBox;
    StaticText3: TLabel;
    StaticText4: TLabel;
    editx2: TEdit;
    edity2: TEdit;
    boutonsup: TBitBtn;
    log1: TLabel;
    log2: TLabel;
    log3: TLabel;
    log4: TLabel;
    StaticText5: TStaticText;
    StaticText6: TStaticText;
    StaticText7: TStaticText;
    StaticText8: TStaticText;
    procedure BitBtn1Click(Sender: TObject);
    procedure boutonsupClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private  encreation:boolean;
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

var
  Fsaisiereseau: Tsaisiereseau;
  mp_hachuresReseau:boolean;
  mp_epaisseurReseau:integer;
  mp_couleurReseau:tcolor;
  mp_x1,mp_x2,mp_y1,mp_y2:extended;
implementation


procedure Tsaisiereseau.BitBtn1Click(Sender: TObject);
begin
mp_hachuresReseau:=cochehachures.Checked;
mp_epaisseurReseau:=editepaisseur.Value;
mp_couleurReseau:=colorgrid1.Selected;
try
mp_x1:=strtofloat(editx1.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce13),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;
 if ((mp_x1<xxmin) or (mp_x1>xxmax)) then begin
 application.messagebox(pchar(rsValeurInacce13),
 pchar(rsattention),mb_ok);
 exit;
 end;

 try
mp_x2:=strtofloat(editx2.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce14),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;
 if ((mp_x2<xxmin) or (mp_x2>xxmax)) then begin
 application.messagebox(pchar(rsValeurInacce14),
 pchar(rsattention),mb_ok);
 exit;
 end;

 try
mp_y1:=strtofloat(edity1.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce15),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;
 if ((mp_y1<yymin) or (mp_y1>yymax)) then begin
 application.messagebox(pchar(rsValeurInacce15),
 pchar(rsattention),mb_ok);
 exit;
 end;

 try
mp_y2:=strtofloat(edity2.text);
 except
 on    EConvertError  do begin
 application.messagebox(pchar(rsValeurInacce16),
 pchar(rsattention),mb_ok);
 exit;
 end;  end;
 if ((mp_y2<yymin) or (mp_y2>yymax)) then begin
 application.messagebox(pchar(rsValeurInacce16),
 pchar(rsattention),mb_ok);
 exit;
 end;

ReTaillePreseau(Listereseau,nombrereseau,nombrereseau+1);

 inc(nombrereseau);
if not
(Listereseau[-1+nombrereseau].create(mp_x1,mp_y1,mp_x2,mp_y2,
mp_epaisseurReseau,
mp_couleurReseau,
1e-3/floatspinedit1.Value,
radiogroup1.ItemIndex=0,
mp_hachuresReseau,spinedit_ordre_min.value,spinedit_ordre_max.Value)) then begin
ReTaillePreseau(Listereseau,nombrereseau,nombrereseau-1);
dec(nombrereseau);
application.messagebox(pchar(rsTailleDuRSea),
pchar(rsattention),mb_ok);

end else
self.modalresult:=mrOk; 
Rafraichit;


end;

procedure Tsaisiereseau.boutonsupClick(Sender: TObject);
begin

end;

procedure Tsaisiereseau.FormCreate(Sender: TObject);
begin
   encreation:=true;
   BITBTN1.CAPTION
:=rsOK;


BITBTN2.CAPTION
:=rsAnnuler;


BOUTONSUP.CAPTION
:=rsSupprimerCeR2;

radiogroup1.caption:=rsTypeDeRSeau;
radiogroup1.Items.Clear;
radiogroup1.Items.Add(rsEnTransmissi);
radiogroup1.Items.Add(rsEnRFlexion);

 groupbox6.Caption:=rsNombreDeTrai;

 statictext8.Caption:=rsLesLongueurs;
CAPTION
:=rsAjoutDUnRSea2;


COCHEHACHURES.CAPTION
:=rsHachuresSiRS;


GROUPBOX1.CAPTION
:=rsCouleurLCran6;


GROUPBOX2.CAPTION
:=rsAspect;


GROUPBOX3.CAPTION
:=rsEpaisseurTra5;


GROUPBOX4.CAPTION
:=rsPoint1;


GROUPBOX5.CAPTION
:=rsPoint2;

 groupbox7.Caption:=rsOrdresPrendr;
 statictext5.Caption:=rsOrdreMinimum;
 statictext6.Caption:=rsOrdreMaximum;

statictext7.Caption:=
rsRemarqueImpo;

LOG1.CAPTION
:=rsLog1;


LOG2.CAPTION
:=rsLog2;


LOG3.CAPTION
:=rsLog3;


LOG4.CAPTION
:=rsLog4;


STATICTEXT1.CAPTION
:=rsX;


STATICTEXT2.CAPTION
:=rsY;


STATICTEXT3.CAPTION
:=rsX;


STATICTEXT4.CAPTION
:=rsY;

end;

procedure Tsaisiereseau.FormShow(Sender: TObject);
begin
 // if encreation then begin scalefont(self); encreation:=false; end;
end;

initialization
  {$i saisiereseau.lrs}


end.
